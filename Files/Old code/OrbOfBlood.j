struct Blood extends Tower
//! runtextmacro InitTower("Blood")
    
    method attFlatBonuses takes nothing returns real
        return s__AbilOrbOfBlood_flatbonus[abil] + super.attFlatBonuses()
    endmethod
    
    method attPercBonuses takes nothing returns real
        return s__AbilOrbOfBlood_percbonus[abil] - 1. + super.attPercBonuses()
    endmethod
    
    method addKill takes nothing returns nothing
        call super.addKill()
        call AbilOrbOfBlood.update(abil)
    endmethod
endstruct

    call AddTAD(     AbilOrbOfBloodId, 'A00P',   0,  10,   0,      function AbilOrbOfBlood.init)

struct AbilOrbOfBlood extends TowerAbility
    static constant integer iconid = 'I00<'
    static constant real incpercbonus = 1.075
    static constant real b1_flatbonus = 3.
    
    real percbonus = 1.
    real flatbonus = 0.
    item icon

    method onDestroy takes nothing returns nothing
        set icon = null
    endmethod
    
    static method init takes nothing returns nothing
        local Tower m = Caster
        local thistype this = allocate(m, AbilOrbOfBloodId)
        set icon = m.addItem(iconid)
        call DebugMsg("AbilOrbOfBlood " + I2S(this) + " is created.")
    endmethod
    
    static method update takes thistype this returns nothing
        set percbonus = percbonus * incpercbonus
        call SetItemCharges(icon, R2Idown(percbonus * 100. - 100.))
        if IsDivisibleByN(caster.kills, data.b1) then
            set flatbonus = flatbonus + b1_flatbonus
        endif
        call caster.updateAttackIcon()
    endmethod
endstruct
