struct HPBar
    static constant real period = 0.01
    
    Minion minion
    texttag hpbar
    
    implement CustomTimer
    
    static method update takes nothing returns nothing
        local thistype updater = GetCustomTimer(GetExpiredTimer())
        local Minion target = updater.minion
        call target.addHP(target.regenhp)
        call updater.updatehpbar()
    endmethod
    
    static method create takes Minion u returns thistype
        local thistype obj = thistype.allocate()
        set obj.minion = u
        set obj.hpbar = CreateTextTag()
        if GetLocalPlayer() != obj.minion.foe.user then
            call SetTextTagVisibility(obj.hpbar, false)
        endif
        call obj.initTimer()
        call TimerStart(obj.t, period, true, function HPBar.update)
        call DebugMsg("HPbar " + I2S(obj) + " is created.")
        return obj
    endmethod
    
    method updatehpbar takes nothing returns nothing
        local real hpp = minion.getHPPerc()
        local integer hpr = R2If(GetUnitState(minion.minion, UNIT_STATE_LIFE))
        local integer red = 204
        local integer green = 51
        if hpp >= 0.67 then
            set red = 51
            set green = 204
        elseif hpp >= 0.33 then
            set green  = 204
        endif
        call SetTextTagColor(hpbar, red, green, 51, 255)
        call SetTextTagText(hpbar, I2S(hpr), 8. * 0.0025)
        call SetTextTagPosUnit(hpbar, minion.minion, 0.)
    endmethod
    
    method onDestroy takes nothing returns nothing
        call DestroyTextTag(hpbar)
        set hpbar = null
    endmethod
endstruct
