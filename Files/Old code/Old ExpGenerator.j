struct FoE extends Tower
//! runtextmacro InitTower("FoE")
endstruct

    call AddTAD(   AbilExpGeneratorId, 'A002',  5, 10,  0,    function AbilExpGenerator.init)

struct AbilExpGenerator extends TowerAbility
    static constant integer iconid = 'I00;'
    static constant integer infoorder = Order_autodispel
    static constant integer orderon = Order_autodispelon
    static constant integer orderoff = Order_autodispeloff
    static constant real period = 3.
    // Defaults
    static constant integer deftickexp = 1
    static constant integer defmaxexp = 10
    static constant integer autogaintick = 2
    // Increments
    static constant integer inctickexp = 1
    static constant integer incmaxexp = 10
    // Additions
    static constant integer b1_maxexp = 40
    static constant integer b2_tickexp = 4
    
    integer exp = 0
    integer tickexp = deftickexp
    integer maxexp = defmaxexp
    boolean auto = false
    integer tick
    CustomPlayer owner
    item icon

//! runtextmacro CustomTimer("t", "Timer", "AbilExpGenerator")

    method onDestroy takes nothing returns nothing
        call DeleteTimer()
        set icon = null
    endmethod
    
    method incLevel takes nothing returns nothing
        set tickexp = tickexp + inctickexp
        set maxexp = maxexp + incmaxexp
        if caster.level == data.b1 then
            set maxexp = maxexp + b1_maxexp
        elseif caster.level == data.b2 then
            set tickexp = tickexp + b2_tickexp
            call caster.removeSelfUpgrade()
        endif
    endmethod
    
    method towerAddExp takes Tower target returns nothing
        call target.addExp(exp)
        set exp = 0
        call SetItemCharges(icon, 0)
    endmethod
    
    static method selfAddExp takes thistype this returns integer
        local integer temp = tickexp
        if exp + tickexp >= maxexp then
            set temp = maxexp - exp
            set exp = maxexp
        else
            set exp = exp + tickexp
        endif
        call SetItemCharges(icon, exp)
        return temp
    endmethod
    
    static method autoAddExp takes thistype this returns nothing
        local integer a = 0
        local integer min = 0
        local Tower target = 0
        local boolean nomin = true
        local TowerArray arr = owner.towers
        loop
            exitwhen a > arr.top
            if arr[a].data.hasexp and (nomin or arr[a].exp < min) then
                set min = arr[a].exp
                set target = arr[a]
                set nomin = false
            endif
            set a = a + 1
        endloop
        if target != 0 then
            call towerAddExp(target)
            call target.addExp(tickexp)
        else
            call thistype.selfAddExp(this)
        endif
    endmethod
    
    static method generateExp takes nothing returns nothing
        local thistype this = GetCustomTimer()
        local integer temp = 0
        if auto then
            set tick = tick + 1
            if tick == autogaintick then
                set tick = 0
                call thistype.autoAddExp(this)
                set temp = tickexp
            endif
        else
            set temp = thistype.selfAddExp(this)
        endif
        if temp > 0 then
            call owner.addExperience(temp)
            call TextUnitSimple("|cff80ccff+" + I2S(temp) + "|r", caster.tower, owner.user)
        endif
    endmethod
    
    static method init takes nothing returns nothing
        local Tower m = Caster
        local thistype this = allocate(m, AbilExpGeneratorId)
        set owner = m.owner
        set icon = m.addItem(iconid)
        call InitTimer()
        call TimerStart(t, period, true, function thistype.generateExp)
        call DebugMsg("AbilExpGenerator " + I2S(this) + " is created.")
    endmethod
    
    static method immediateOrders takes thistype this, integer order returns nothing
        if order == orderon or order == orderoff then
            set auto = not auto
            set tick = 0
        endif
    endmethod
    
    static method towerAddExpSelected takes thistype this, Tower target returns nothing
        local CustomPlayer p = target.owner
        local integer a = 0
        local Tower t
        local thistype gen
        if not target.data.hasexp then
            call p.message(SoundError, "|cffcc3333Error|r: this tower does not use experience.")
            return
        endif
        loop
            set t = p.towers[a]
            set gen = t.abil
            if IsUnitSelected(t.tower, p.user) and t.isReady and t.typ == caster.typ and gen.exp > 0 then
                call gen.towerAddExp(target)
            endif
            set a = a + 1
            exitwhen a > p.towers.top
        endloop
    endmethod
endstruct
