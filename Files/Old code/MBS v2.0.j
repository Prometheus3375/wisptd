//! textmacro MBSArray takes name, Name
    readonly MinionBuff array $name$[MaxBuffsPerType]
    private integer $name$Top = -1

    method add$Name$Buff takes MinionBuff obj returns integer
        set $name$Top = $name$Top + 1
        set $name$[$name$Top] = obj
        return $name$Top
    endmethod
//! endtextmacro

//! textmacro MBSIntegerValues takes name, Name
    private integer array $name$Values[MaxBuffsPerType]
    readonly integer total$Name$Bonus = 0

    method change$Name$Value takes integer i, integer value returns nothing
        set total$Name$Bonus = total$Name$Bonus - $name$Values[i] + value
        set $name$Values[i] = value
    endmethod
//! endtextmacro

//! textmacro MBSBooleanValues takes name, Name
    private integer array $name$Values[MaxBuffsPerType]
    private integer $name$BooleanCounter = 0
    readonly boolean total$Name$Bonus = false

    method change$Name$Value takes integer i, boolean value returns nothing
        local integer v = B2I(value)
        set $name$BooleanCounter = $name$BooleanCounter - $name$Values[i] + v
        set total$Name$Bonus = $name$BooleanCounter > 0
        set $name$Values[i] = v
    endmethod
//! endtextmacro

library MinionBuffStorageLib uses ArrayLibrary
struct MinionBuffStorage//[IndexAmount4]
//! runtextmacro MBSArray("eot", "EoT")
//! runtextmacro MBSArray("shield", "Shield")
//! runtextmacro MBSArray("movespeed", "MoveSpeed")
//! runtextmacro MBSArray("stun", "Stun")
//! runtextmacro MBSArray("persistence", "Persistence")
//! runtextmacro MBSArray("physarm", "PhysArmor")
//! runtextmacro MBSArray("magcarm", "MagcArmor")
//! runtextmacro MBSArray("armor", "Armor")
//! runtextmacro MBSArray("physimm", "PhysImmune")
//! runtextmacro MBSArray("magcimm", "Magcimmune")
//! runtextmacro MBSArray("protection", "Protection")
//! runtextmacro MBSArray("invul", "Invul")

    private real array shieldValues[MaxBuffsPerType]
    readonly real totalShieldBonus = 0.
    
    method changeShieldValue takes integer i, real value returns nothing
        set totalShieldBonus = totalShieldBonus - shieldValues[i] + value
        set shieldValues[i] = value
    endmethod
    
    method takeDamageShield takes real amount returns real
        local integer a = 0
        loop
            exitwhen a > shieldTop
            set amount = amount - shieldValues[a]
            if amount > 0. then
                call changeShieldValue(a, 0.)
            else
                call changeShieldValue(a, - amount)
                return 0.
            endif
            set a = a + 1
        endloop
        return amount
    endmethod
    
    private real array movespeedValues[MaxBuffsPerType]
    readonly real totalMoveSpeedBonus = 0.
    
    method updateMoveSpeedBonus takes nothing returns nothing
        local real max = 0.
        local real min = 0.
        local integer a = 0
        loop
            exitwhen a > movespeedTop
            if movespeedValues[a] > max then
                set max = movespeedValues[a]
            elseif movespeedValues[a] < min then
                set min = movespeedValues[a]
            endif
            set a = a + 1
        endloop
        set totalMoveSpeedBonus = max + min
    endmethod
    
    method changeMoveSpeedValue takes integer i, real value returns nothing
        set movespeedValues[i] = value
    endmethod

//! runtextmacro MBSBooleanValues("stun", "Stun")
//! runtextmacro MBSBooleanValues("persistence", "Persistence")
//! runtextmacro MBSIntegerValues("physarm", "PhysArmor")
//! runtextmacro MBSIntegerValues("magcarm", "MagcArmor")
//! runtextmacro MBSIntegerValues("armor", "Armor")
//! runtextmacro MBSBooleanValues("physimm", "PhysImmune")
//! runtextmacro MBSBooleanValues("magcimm", "MagcImmune")
//! runtextmacro MBSBooleanValues("protection", "Protection")
//! runtextmacro MBSBooleanValues("invul", "Invul")

    method makeUnstoppable takes nothing returns nothing
        local integer a = MaxBuffsPerType - 1
        loop
            if a <= movespeedTop and movespeed[a].buffdata.negative then
                call movespeed[a].stop.evaluate()
                call changeMoveSpeedValue(a, 0.)
            endif
            if a <= stunTop/* and stun[a].buffdata.negative*/ then
                call stun[a].stop.evaluate()
                call changeStunValue(a, false)
            endif
            set a = a - 1
            exitwhen a < 0
        endloop
        call updateMoveSpeedBonus()
    endmethod
    
    method stopAllNegativeBuffs takes nothing returns nothing
        local integer a = MaxBuffsPerType - 1
        loop
            if a <= eotTop and eot[a].buffdata.negative then
                call eot[a].stop.evaluate()
            endif
            if a <= movespeedTop and movespeed[a].buffdata.negative then
                call movespeed[a].stop.evaluate()
                call changeMoveSpeedValue(a, 0.)
            endif
            if a <= stunTop/* and stun[a].buffdata.negative*/ then
                call stun[a].stop.evaluate()
                call changeStunValue(a, false)
            endif
            if a <= physarmTop and physarm[a].buffdata.negative then
                call physarm[a].stop.evaluate()
                call changePhysArmorValue(a, 0)
            endif
            if a <= magcarmTop and magcarm[a].buffdata.negative then
                call magcarm[a].stop.evaluate()
                call changeMagcArmorValue(a, 0)
            endif
            if a <= armorTop and armor[a].buffdata.negative then
                call armor[a].stop.evaluate()
                call changeArmorValue(a, 0)
            endif
            set a = a - 1
            exitwhen a < 0
        endloop
        call updateMoveSpeedBonus()
    endmethod

//! textmacro FlushBuff takes name
            if a <= $name$Top and si__MinionBuff_V[$name$[a]] == -1 then
                call $name$[a].destroy()
            endif
//! endtextmacro

    method onDestroy takes nothing returns nothing
        local integer a = MaxBuffsPerType - 1
        loop
            //! runtextmacro FlushBuff("shield")
            //! runtextmacro FlushBuff("eot")
            //! runtextmacro FlushBuff("movespeed")
            //! runtextmacro FlushBuff("stun")
            //! runtextmacro FlushBuff("persistence")
            //! runtextmacro FlushBuff("physarm")
            //! runtextmacro FlushBuff("magcarm")
            //! runtextmacro FlushBuff("armor")
            //! runtextmacro FlushBuff("physimm")
            //! runtextmacro FlushBuff("magcimm")
            //! runtextmacro FlushBuff("protection")
            //! runtextmacro FlushBuff("invul")
            set a = a - 1
            exitwhen a < 0
        endloop
    endmethod
endstruct

endlibrary
