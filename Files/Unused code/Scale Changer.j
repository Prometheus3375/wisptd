struct ScaleChanger
    static constant real period = 0.05
    
    real desired_scale
    real current_scale
    real shift
    unit target
    
//! runtextmacro CustomTimer("t", "", "ScaleChanger")
//! runtextmacro DeleteCustomTimer("")

    static method create takes unit u returns thistype
        local thistype this = allocate()
        set target = u
        call initTimer()
        return this
    endmethod
    
    static method shiftScale takes nothing returns nothing
        local thistype this = GetCustomTimer(GetExpiredTimer())
        set current_scale = current_scale + shift
        call SetUnitScale(target, current_scale, current_scale, current_scale)
        if current_scale == desired_scale then
            call PauseTimer(t)
        endif
    endmethod
    
    method changeScale takes real oldScale, real newScale, real duration returns nothing
        set desired_scale = newScale
        set current_scale = oldScale
        set shift = (newScale - oldScale) / duration * period
        call TimerStart(t, period, true, function thistype.shiftScale)
    endmethod
endstruct
