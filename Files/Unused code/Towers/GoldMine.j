    call AddTowerData1( GoldMineId, 1200, false,   0,  true, false,      1,      0,    60,  180,     10,            10)
    call AddTowerData2( GoldMineId,  60.,   0.4, 'Al05', 'At05',  false,      AbilGoldMiningId,  function GoldMine.build)

struct GoldMine extends Tower
//! runtextmacro InitTower("GoldMine")
endstruct

    call AddTAD(     AbilGoldMiningId, 'A00K',  5, 10,  0,      function AbilGoldMining.init)

struct AbilGoldMining extends TowerAbility
    static constant real period = 1.
    // Defaults
    static constant integer defincome = 1
    // Increments
    static constant integer incincome = 1
    // Additions
    static constant integer b1_income = 1
    static constant integer b2_income = 3
    
    integer income = defincome
    CustomPlayer owner

//! runtextmacro CustomTimer("t", "Timer", "AbilGoldMining")

    method onDestroy takes nothing returns nothing
        call DeleteTimer()
    endmethod
    
    method incLevel takes nothing returns nothing
        set income = income + incincome
        if caster.level == data.b1 then
            set income = income + b1_income
        elseif caster.level == data.b2 then
            set income = income + b2_income
        endif
    endmethod
    
    static method addGold takes nothing returns nothing
        local thistype this = GetCustomTimer()
        call owner.addGoldGathered(income)
        call owner.createGoldTextUnit(I2S(income), caster.tower)
    endmethod
    
    static method init takes nothing returns nothing
        local Tower m = Caster
        local thistype this = allocate(m, AbilGoldMiningId)
        set owner = m.owner
        call InitTimer()
        call TimerStart(t, period, true, function thistype.addGold)
        call DebugMsg("AbilGoldMining " + I2S(this) + " is created.")
    endmethod
endstruct
