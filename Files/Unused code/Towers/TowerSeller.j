struct TowerSeller
    Tower object

//! runtextmacro CustomTimer("t", "Timer", "TowerSeller")
    
    method onDestroy takes nothing returns nothing
        call DeleteTimer()
        call DebugMsg("TowerSeller " + I2S(this) + " is deleted.")
    endmethod
    
    static method end takes nothing returns nothing
        local thistype this = GetCustomTimer()
        set object.sellratio = SellRatio
        call IncUnitAbilityLevel(object.tower, TowerAbilSell)
        set object.seller = 0
        call deallocate()
    endmethod
    
    static method create takes Tower u returns thistype
        local thistype this = allocate()
        set object = u
        call InitTimer()
        call TimerStart(t, FullCostSellTime, false, function thistype.end)
        //call DebugMsg("TowerSeller " + I2S(this) + " is started.")
        return this
    endmethod
endstruct
