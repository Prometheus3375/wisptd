struct AbilFood extends TowerAbility
    static constant integer infoid = 'A00L'
    static constant real period = 1.
    // Levels
    static constant integer add1 = 5
    static constant integer max = 10
    // Defaults
    static constant integer deffood = 1
    // Increments
    static constant integer incfood = 1
    // Additions
    static constant integer add1food = 1
    static constant integer maxfood = 3
    
    integer food = deffood
    CustomPlayer owner
    
    static method create takes Tower m returns thistype
        local thistype this = thistype.allocate(m, infoid)
        call UnitAddAbility(m.tower, infoid)
        set owner = m.owner
        call owner.addFoodMax(food)
        call DebugMsg("AbilFood " + I2S(this) + " is created.")
        return this
    endmethod
    
    method onDestroy takes nothing returns nothing
        call owner.addFoodMax(-food)
    endmethod
    
    method incLevel takes nothing returns nothing
        local integer old = food
        set level = level + 1
        set food = food + incfood
        if level == add1 then
            set food = food + add1food
        elseif level == max then
            set food = food + maxfood
            call UnitRemoveAbility(caster.tower, TowerAbilUpgrade)
            call RemoveItem(caster.reqicon)
            call DecUnitAbilityLevel(caster.tower, TowerAbilInventory)
        endif
        call owner.addFoodMax(food - old)
    endmethod
endstruct

struct Farm extends Tower
    //! runtextmacro InitTower("Farm", "0")
    
    method makeReady takes nothing returns nothing
        set goldcost = 0
        set abil = AbilFood.create(this)
    endmethod
endstruct
