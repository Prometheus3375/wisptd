    call AddTowerData1(    BurstId,  200,  true,   0,  true, false,      0,      0,     0,    0,     10, MaxTowerLevel)
    call AddTowerData2(    BurstId, 135.,   0.6, 'Al04', 'At04',   true,  AbilEnhancedWeaponId,     function Burst.build)
    call AddTowerAttackData(    BurstId,  5.,          0, 0.5,   Perc,    TARGET_TYPE_ALL,  384., 1, false, 'A00F',      Order_deathcoil)

struct Burst extends Tower
    static constant real DamagePerLevel = 0.5

//! runtextmacro InitTower("Burst")
    
    method getBaseAttackDamage takes nothing returns real
        return attdata.damage + DamagePerLevel * level
    endmethod
    
    method attPercBonuses takes nothing returns real
        return AbilEnhancedWeapon(abil).dmgbonus + super.attPercBonuses()
    endmethod
    
    method attackEffect takes Minion m returns nothing
        call AbilEnhancedWeapon.attackEffect(abil, m)
    endmethod
endstruct

    call AddTAD( AbilEnhancedWeaponId, 'A00J',  5, 10,  0,  function AbilEnhancedWeapon.init)

struct AbilEnhancedWeapon extends TowerAbility
    static constant integer icon1id = 'I00='
    static constant integer icon2id = 'I00>'
    static constant integer spellid = 'A00F'
    static constant integer attackerid = 's000'
    static constant integer infoorder = Order_auraunholy
    static constant integer spellorder = Order_deathcoil
    static constant real launchperiod = 0.125
    static constant real fadetime = 3.
    // Defaults
    static constant integer charges = 4
    // Additions
    static constant real b1_dmgbonus = 0.25
    static constant real b1_maxbonus = 1.

    TowerAttacker d
    unit attacker
    unit target = null
    boolean burstmode = false
    real dmgbonus = 0.
    integer currcharge
    item icon
    
//! runtextmacro CustomTimer("t", "Timer", "AbilEnhancedWeapon")
    
    static method init takes nothing returns nothing
        local Tower m = Caster
        local thistype this = allocate(m, AbilEnhancedWeaponId)
        // Dummy
        //set d = TowerAttacker.create(m, spellid, spellorder)
        // Attacker
        set attacker = CreateUnit(m.owner.user, attackerid, m.x, m.y, 0.)
        call SetUnitPathing(attacker, false)
        call SetUnitFlyHeight(attacker, m.height, 0.)
        call SetUnitUserData(attacker, m)
        // Timer
        call InitTimer()
        // Icon
        set icon = AddItemToUnit(m.tower, icon1id)
        call DebugMsg("AbilEnhancedWeapon " + I2S(this) + " is created.")
    endmethod
    
    method onDestroy takes nothing returns nothing
        call d.destroy()
        call RemoveUnit(attacker)
        call DeleteTimer()
        set attacker = null
        set target = null
        set icon = null
    endmethod
    
    static method toggleMode takes thistype this returns nothing
        // FlushAttack
        set dmgbonus = 0.
        set target = null
        call PauseTimer(t)
        // SwapAttackers
        //set bj_lastCreatedUnit = attacker
        //set attacker = caster.attacker
        //set caster.attacker = bj_lastCreatedUnit
        //call UnitAddAbility(attacker, AttackRemovalId)
        //if caster.autoattack then
        //    call UnitRemoveAbility(bj_lastCreatedUnit, AttackRemovalId)
        //endif
        // SwapMode
        set burstmode = not burstmode
        if burstmode then
            set icon = ReplaceItemForUnit(caster.tower, icon, icon2id)
        else
            set icon = ReplaceItemForUnit(caster.tower, icon, icon1id)
        endif
        call caster.updateAttackIcon()
    endmethod

    static method flushBonus takes nothing returns nothing
        local thistype this = GetCustomTimer()
        set dmgbonus = 0.
        set target = null
        call caster.updateAttackIcon()
    endmethod

    method attackAuto takes Minion m returns nothing
        if caster.level >= data.b1 and target == m.minion then
            set dmgbonus = dmgbonus + b1_dmgbonus
            if dmgbonus > b1_maxbonus then
                set dmgbonus = b1_maxbonus
            endif
            call TimerStart(t, fadetime, false, function thistype.flushBonus)
        else
            set dmgbonus = 0.
        endif
        set target = m.minion
        call caster.attackEffectUpdateIcon(m)
    endmethod

    static method launch takes nothing returns nothing
        local thistype this = GetCustomTimer()
        if UnitAlive(target) then
            call d.castTarget(target)
            set currcharge = currcharge + 1
            if currcharge == charges then
                call PauseTimer(t)
            endif
        else
            call PauseTimer(t)
        endif
    endmethod
    
    method attackBurst takes Minion m returns nothing
        set target = m.minion
        call d.castTarget(target)
        set currcharge = 1
        call TimerStart(t, launchperiod, true, function thistype.launch)
    endmethod
    
    static method attackEffect takes thistype this, Minion m returns nothing
        if burstmode then
            call attackBurst(m)
        else
            call attackAuto(m)
        endif
    endmethod
    
    method startEffect takes Minion m returns nothing
        if caster.attackEffectSimple(m) and caster.level == data.b2 then
            call BuffEnhancedWeaponStun.launch(m, caster)
        endif
    endmethod
    
    static method filterToggleModeSelected takes nothing returns nothing
//        local Tower t = GetUnitUserData(GetFilterUnit())
//        if IsUnitTower(GetFilterUnit()) and t.isReady and t.typ == AbilityEndcastInt1 and thistype(t.abil).burstmode == AbilityEndcastBool1 /*
//        */ and IssueImmediateOrderById(t.tower, infoorder) then
//            call toggleMode(t.abil)
//        endif
    endmethod
endstruct

    // Data                                 type,   neg, ticks, max,    dur, effect.
    call AddMinionBuff( BuffEnhancedWeaponStunId,  true,     1,   1,    0.5,      0) // Stun

struct BuffEnhancedWeaponStun extends MinionBuff
    method onCreate takes nothing returns nothing
        set node1 = target.addStunBuff(this, true)
    endmethod
    
    method onDestroy takes nothing returns nothing
        call target.removeStunBuff(node1)
    endmethod
    
    static method launch takes Minion m, Tower caster returns nothing
        //! runtextmacro InitBuff("m", "caster.tower", "caster.owner", "EnhancedWeaponStun", "true", "true", "false", "")
        call start()
    endmethod
endstruct
