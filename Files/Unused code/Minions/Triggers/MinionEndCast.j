globals
    constant trigger TriggerMinionEndCast = CreateTrigger()
endglobals

function Trig_MinionEndCast_Actions takes nothing returns nothing
    local Minion m = GetUnitUserData(GetSpellAbilityUnit())
    call m.moveToDestination()
endfunction

function InitTrig_MinionEndCast takes nothing returns nothing
    call TriggerRegisterPlayerUnitEvent(TriggerMinionEndCast, MinionPlayer1, EVENT_PLAYER_UNIT_SPELL_ENDCAST, null)
    call TriggerRegisterPlayerUnitEvent(TriggerMinionEndCast, MinionPlayer2, EVENT_PLAYER_UNIT_SPELL_ENDCAST, null)
    call TriggerAddAction(TriggerMinionEndCast, function Trig_MinionEndCast_Actions)
endfunction
