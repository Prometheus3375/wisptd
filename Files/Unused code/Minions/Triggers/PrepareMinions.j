globals
    integer PlayerIndex
endglobals

function Trig_PrepareMinions_Actions takes nothing returns nothing
    local CustomPlayer p = Players[PlayerIndex]
    local integer a = 0
    loop
        exitwhen a > p.forces.top
        call p.forces[a].basicPrepare()
        set a = a + 1
    endloop
endfunction

function InitTrig_PrepareMinions takes nothing returns nothing
    set gg_trg_PrepareMinions = CreateTrigger()
    call TriggerAddAction(gg_trg_PrepareMinions, function Trig_PrepareMinions_Actions)
endfunction
