globals
    constant trigger TriggerMinionAttacked = CreateTrigger()
endglobals

function Trig_MinionAttacked_Actions takes nothing returns nothing
    local Minion victim = GetUnitUserData(GetTriggerUnit())
    local Tower t = GetUnitUserData(GetAttacker())
    call victim.startDelay()
    call FindCaster(victim, t, t.attdata.dmgtype)
endfunction

function Trig_MinionAttacked_Conditions takes nothing returns boolean
    return TimerGetRemaining(Minion(GetUnitUserData(GetTriggerUnit())).delayer) <= 0.
endfunction

function InitTrig_MinionAttacked takes nothing returns nothing
    call TriggerAddCondition(TriggerMinionAttacked, Filter(function Trig_MinionAttacked_Conditions))
    call TriggerAddAction(TriggerMinionAttacked, function Trig_MinionAttacked_Actions)
    call TriggerRegisterPlayerUnitEvent(TriggerMinionAttacked, MinionPlayers[0], EVENT_PLAYER_UNIT_ATTACKED, null)
    call TriggerRegisterPlayerUnitEvent(TriggerMinionAttacked, MinionPlayers[1], EVENT_PLAYER_UNIT_ATTACKED, null)
endfunction
