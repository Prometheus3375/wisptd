struct Gryphon extends Minion
    // Upgrades' indexes
    static constant integer BeneathTheWingsId = 0
    static constant integer WideWingsId = 1
    static constant integer InTheSkyId = 2
    // Upgrades
    static constant integer BeneathTheWings_NewArmor = 20
    static constant real WideWings_NewRange = 256.
    static constant real InTheSky_NewDuration = 1.
    
//! runtextmacro InitMinion("Gryphon")
    
    method kill takes nothing returns nothing
        call KillUnit(minion)
        call delete()
    endmethod
    
    static method upgrade1 takes nothing returns nothing
        local integer i = CustomPlayer(AbilityEffectInt1).number
        set AbilAirSupport.Player_ArmorBonus[i] = BeneathTheWings_NewArmor
    endmethod
    
    static method upgrade2 takes nothing returns nothing
        local CustomPlayer upgrader = AbilityEffectInt1
        local integer a = 0
        local thistype m
        set upgrader.minion_abildata[AbilAirSupportId].range = WideWings_NewRange
        loop
            exitwhen a > upgrader.sentminions.top
            set m = upgrader.sentminions[a]
            if m.typ == GryphonId then
                call AbilAirSupport.upgradeRange(m.abil, WideWings_NewRange)
            endif
            set a = a + 1
        endloop
    endmethod
    
    static method upgrade3 takes nothing returns nothing
        local MinionBuffData d = CustomPlayer(AbilityEffectInt1).minion_buffdata[BuffAirSupportId]
        set d.duration = InTheSky_NewDuration
        set d.period = InTheSky_NewDuration
    endmethod
endstruct
