    // Data                                 type,   neg, ticks, max,    dur, effect.
    call AddMinionBuff(        BuffArcaneArmorId, false,     1,   1,     2.,      0) // MagcArmor
    call AddMinionBuff(       BuffArcaneShieldId, false,     1,   1,    1.5, 'A00@') // Protection
    // Data                             type, iconid, activ, search, periodic,  precd,   cd, range.
    call AddMinionAbility(AbilArcaneShieldId, 'Aa03',  true,   true,    false,     0.,   5.,  512.)
    // Data                     typ, cost, food, freq, li, incm, gold+, exp+, texp, lives-, lives+, maxhp,    reghp,          hpup, msfactor, prm, mrm.
    call AddMinionData1(   WizardId,  400,   2.,    2,  5,   40,    40,   40,    0,    -01,     01,  100., DefRegen, HPFactor1_075,     1.00,   0,   0)
    // Additional Data          typ, unitid,  spawn, infoid,            ability, nhero, ndcay,            init fuction.
    call AddMinionData2(   WizardId, 'm003', 'Am03', 'Ai03', AbilArcaneShieldId,  true, false,    function Wizard.init)
    // Upgrades                    typ, button, minion, rsrchd, res_id, cost,            upgrade function.                            
    call AddMinionUpgrades(   WizardId, 'Ab0<', 'Ac0<', 'Ad0<', 'R00<',    0,                        null,/*
                                     */ 'Ab0=', 'Ac0=', 'Ad0=', 'R00=',    0,    function Wizard.upgrade2,/*
                                     */ 'Ab0>', 'Ac0>', 'Ad0>', 'R00>',    0,                        null)

struct BuffArcaneArmor extends MinionBuff
    method onCreate takes integer magcarmor returns nothing
        set node1 = target.addMagcArmorBuff(this, magcarmor)
    endmethod
    
    method onDestroy takes nothing returns nothing
        call target.removeMagcArmorBuff(node1)
    endmethod
    
    static method launch takes Minion m, unit caster, CustomPlayer p, integer magcarmor returns nothing
        //! runtextmacro InitBuff("m", "caster", "p", "ArcaneArmor", "false", "false", "false", "magcarmor")
        call start()
    endmethod
endstruct

struct BuffArcaneShield extends MinionBuff
    method onCreate takes nothing returns nothing
        set node1 = target.addProtectionBuff(this, true)
    endmethod
    
    method onDestroy takes nothing returns nothing
        call target.removeProtectionBuff(node1)
    endmethod
    
    method tickAction takes nothing returns boolean
        local MinionData data = owner.miniondata[WizardId]
        if data.upgrade[Wizard.ReabsorbtionId] then
            call target.heal(MBSNodeIntNReal(node1).rvalue * Wizard.Reabsorbtion_HealingFactor, EffectRestoreHealth, AttEffectRestoreHealth)
        endif
        //call DebugMsg(R2SX(MBSNodeIntNReal(node1).rvalue))
        if data.upgrade[Wizard.ArcaneArmorId] then
            call BuffArcaneArmor.launch(target, source, owner, Wizard.ArcaneArmor_MagcArmor)
        endif
        return true
    endmethod
    
    static method launch takes Minion m, Minion caster returns nothing
        //! runtextmacro InitBuff("m", "caster.minion", "caster.owner", "ArcaneShield", "false", "false", "false", "")
        call start()
    endmethod
endstruct

struct AbilArcaneShield extends MinionAbility
    static method fieldInit takes nothing returns nothing
        //! runtextmacro MinionAbilityBasicInit("ArcaneShield", "true")
        call DebugMsg("AbilArcaneShield " + I2S(this) + " is created.")
    endmethod
    
    method castEffect takes Minion target, Tower attacker, integer dmgtype returns boolean
        if target.owner == caster.owner then
            call BuffArcaneShield.launch(target, caster)
            call startCDTimer()
            return true
        endif
        return false
    endmethod
endstruct

struct Wizard extends Minion
    // Upgrades' indexes
    static constant integer ArcaneArmorId = 0
    static constant integer WisdomId = 1
    static constant integer ReabsorbtionId = 2
    // Upgrades
    static constant integer ArcaneArmor_MagcArmor = 40
    static constant real Wisdom_AdditionalDuration = 0.5
    static constant real Reabsorbtion_HealingFactor = 0.5
    
//! runtextmacro InitMinion("Wizard")
    
    static method upgrade2 takes nothing returns nothing
        local MinionBuffData d = Upgrader.minion_buffdata[BuffArcaneShieldId]
        set d.duration = d.duration + Wisdom_AdditionalDuration
        set d.period = d.duration
    endmethod
endstruct
