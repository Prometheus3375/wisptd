    // Data                                 type,   neg, ticks, max,    dur, effect.
    call AddMinionBuff(      BuffDivinePurposeId, false,     1,   1,     2.,      0) // MoveSpeed, Persistence

struct BuffDivinePurpose extends MinionBuff
    boolean lowHP = false

    method onCreate takes boolean low, real msbonus returns nothing
        set node1 = target.addMoveSpeedBuff(this, msbonus)
        if low then
            set lowHP = true
            set node2 = target.addPersistenceBuff(this, true)
        endif
    endmethod
    
    method onDestroy takes nothing returns nothing
        call target.removeMoveSpeedBuff(node1)
        if lowHP then
            call target.removePersistenceBuff(node2)
        endif
    endmethod
    
    static method launch takes Minion caster, real msbonus returns nothing
        //! runtextmacro InitBuff("caster", "caster.minion", "caster.owner", "DivinePurpose", "false", "false", "false", "caster.getHPPerc() <= AbilDivineShield.DivinePurpose_HPPercThreshold, msbonus")
        call start()
    endmethod
endstruct

//! textmacro InitBuff takes target, sourceunit, owner, name, negative, disabling, ownercheck, args
        static if $negative$ then
            local thistype this
            static if $disabling$ then
                if $target$.omitsDisablingBuffs() or $target$.isInvulnerable() then
                    return
                endif
            else
                if $target$.isInvulnerable() then
                    return
                endif
            endif
            set this = GetMinionBuff($target$, Buff$name$Id)
        else
            local thistype this = GetMinionBuff($target$, Buff$name$Id)
        endif
        if this == 0 then
            set this = allocate()
            call this.basicInit($target$, $sourceunit$, $owner$, Buff$name$Id)
            call this.onCreate($args$)
        elseif this.source != $sourceunit$ then
            static if $ownercheck$ then
                if this.owner != $owner$ then
                set this.owner = $owner$
                set this.buffdata = $owner$.minion_buffdata[Buff$name$Id]
                endif
            endif
            set this.source = $sourceunit$
        endif
//! endtextmacro
