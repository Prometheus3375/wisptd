struct BuffAirSupport extends MinionBuff
    method onCreate takes integer armor returns nothing
        set node1 = target.addArmorBuff(this)
    endmethod
    
    method onDestroy takes nothing returns nothing
        call target.removeArmorBuff(node1)
    endmethod
    
    static method launch takes Minion m, Minion caster, integer armor returns nothing
        //! runtextmacro InitBuff("m", "caster.minion", "caster.owner", "AirSupport", "false", "false", "false", "armor")
        call start()
    endmethod
endstruct
