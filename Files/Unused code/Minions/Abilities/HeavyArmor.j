struct AbilHeavyArmor extends MinionAbility
    static constant integer BlockingDamageType = Phys
    static constant string EffectBlockHit = "Abilities\\Spells\\Items\\SpellShieldAmulet\\SpellShieldCaster.mdx"
    static constant string AttEffectBlockHit = "origin"

    static constant integer DefaultMaxBlocksAmount = 3
    static integer array Player_MaxBlocksAmount
    
    integer blocks = 0
    
    private static method onInit takes nothing returns nothing
        local integer i = 0
        loop
            set Player_MaxBlocksAmount[i] = DefaultMaxBlocksAmount
            set i = i + 1
            exitwhen i == bj_MAX_PLAYERS
        endloop
    endmethod
    
    static method fieldInit takes nothing returns nothing
        //! runtextmacro MinionAbilityBasicInit("HeavyArmor")
        call DebugMsg("AbilHeavyArmor " + I2S(this) + " is created.")
    endmethod
    
    method cooldownEnd takes nothing returns nothing
        set blocks = Player_MaxBlocksAmount[ownerid]
        //call DebugMsg("Amount of blocks is " + I2S(blocks) + ".")
    endmethod
    
    static method blockPhysDamage takes thistype this returns boolean
        if blocks > 0 then
            if blocks == Player_MaxBlocksAmount[ownerid] then
                call startCD()
            endif
            set blocks = blocks - 1
            call CreateEffectTarget(EffectBlockHit, caster.minion, AttEffectBlockHit)
            return true
        endif
        return false
    endmethod
    
    static method blockAnyDamage takes thistype this returns boolean
        if caster.data.upgrade[Knight.WarVeteranId] then
            return blockPhysDamage(this)
        endif
        return false
    endmethod
    
    static method upgradeMaxBlocksAmount takes integer i, integer inc returns nothing
        set Player_MaxBlocksAmount[i] = Player_MaxBlocksAmount[i] + inc
    endmethod
endstruct
