struct AbilAirSupport extends MinionAbility
    static constant integer DefaultArmorBonus = 10
    static integer array Player_ArmorBonus
    
    MinionAura aura
    
    private static method onInit takes nothing returns nothing
        local integer i = 0
        loop
            set Player_ArmorBonus[i] = DefaultArmorBonus
            set i = i + 1
            exitwhen i == bj_MAX_PLAYERS
        endloop
    endmethod
    
    static method fieldInit takes nothing returns nothing
        //! runtextmacro MinionAbilityBasicInit("AirSupport")
        set aura = MinionAura.createMoving(this, abildata.range)
        call DebugMsg("AbilAirSupport " + I2S(this) + " is created.")
    endmethod
    
    method onDestroy takes nothing returns nothing
        call aura.destroy()
    endmethod
    
    static method upgradeRange takes thistype this, real range returns nothing
        call aura.changeRangeMoving(range)
    endmethod
   
    static method filter takes nothing returns nothing
        local Minion m = GetUnitUserData(GetFilterUnit())
        if IsUnitMinion(GetFilterUnit()) and IsUnitType(m.minion, UNIT_TYPE_GROUND) and m.is_alive and m.owner == TransmittedCaster.owner then
            call BuffAirSupport.launch(m, TransmittedCaster, TransmittedArmor)
        endif
    endmethod
    
    private static Minion TransmittedCaster
    private static integer TransmittedArmor
    
    method cooldownEnd takes nothing returns nothing
        local Minion m = caster
        set TransmittedCaster = m
        set TransmittedArmor = Player_ArmorBonus[ownerid]
        call GroupEnumUnitsInRange(bj_lastCreatedGroup, GetUnitX(m.minion), GetUnitY(m.minion), abildata.range, function thistype.filter)
    endmethod
/*
    method cooldownEndAnother takes nothing returns nothing
        local integer a = 0
        local Minion m = caster
        local real x = GetUnitX(m.minion)
        local real y = GetUnitY(m.minion)
        local CustomPlayer p = m.foe
        loop
            set m = p.minions[a]
            if IsUnitType(m.minion, UNIT_TYPE_GROUND) and IsUnitInRangeXY(m.minion, x, y, abildata.range) amd m.owner == caster.owner then
                call BuffAirSupport.launch(m, caster, armor)
            endif
            set a = a + 1
            exitwhen a > p.minions.top
        endloop
    endmethod*/
endstruct
