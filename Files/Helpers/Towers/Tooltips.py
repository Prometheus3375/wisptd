from Sheets.TowerStats import *
from Colors import *
from Misc import nl, convert_range
from wtsParser import FindByComment, Type_Unit, Type_Abil, Pos_UberTip


def get_damage(typ: str, text: str) -> str:
    if typ == 'Phys':
        return colorize(Color_Physical, text) + ' Physical'
    if typ == 'Magc':
        return colorize(Color_Magical, text) + ' Magical'
    if typ == 'Perc':
        return colorize(Color_Percentage, text) + ' Percentage'
    raise Exception('Invalid damage type!')


def generate():
    tinfo = info[:len(info) - 2]  # drop FoL and Burst
    for data in tinfo:
        # Get old tooltip
        build_tip = FindByComment(Type_Unit, data[UnitRaw], Pos_UberTip)
        tow_tip = FindByComment(Type_Abil, data[InfoRaw], Pos_UberTip)
        # Old tooltip not found -> skip
        if not (build_tip and tow_tip):
            continue
        # Generate tooltip
        build = []
        tow = []
        if data[GoldCost] and int(data[GoldCost]) > 0:
            tow.append('Cost: ' + colorize(Color_Gold, data[GoldCost]) + ' Gold')
        if data[Income] and int(data[Income]) > 0:
            tow.append('Income Gain: ' + colorize(Color_Income, '+' + data[Income]))
        tow.append('Build Limit: ' + colorizeNumber(data[Limit]))
        if data[Damage]:
            tow.append(nl + 'Attack')
            tow.append('Damage: ' + get_damage(data[DmgType], data[Damage]))
            tow.append('Cooldown: ' + colorizeNumber(data[DmgCD]) + ' s')
            tow.append('Range: ' + colorizeNumber(convert_range(data[Range])))
            tow.append('Max. Targets: ' + colorizeNumber(data[MaxTargets]))
            tow.append('Targets Allowed: ' + data[Targets])
            tow.append('Explodes Targets: ' + data[Explosive])
        build += tow
        build.append(nl + 'Ability: ' + colorize(Color_Ability, data[Ability]))
        # Update tooltip
        build_tip.content = nl.join(build) + nl
        tow_tip.content = nl.join(tow) + nl
