from Sheets.TowerUps import *
from Sheets.TowerStats import info as TSinfo, Towers as TSTowers, UpDmg, Ability
from Sheets.TowerAbilities import Pattern_Modes
from Colors import *
from Misc import nl, re, replace
from wtsParser import FindByComment, Type_Abil, Pos_UberTip


ud = table.addRow()
abil = table.addRow()
# Copy data
for data in info:
    for d in TSinfo:
        if d[TSTowers] == data[Towers]:
            data[ud] = d[UpDmg]
            data[abil] = d[Ability]
UpDmg = ud
Ability = abil


def generate():
    pre = ' - '
    sep = ';' + nl
    for data in info:
        # Get old tooltip
        tip = FindByComment(Type_Abil, data[Rawcode], Pos_UberTip)
        if not tip:
            continue
        s = ''
        # Tower Ups
        l = []
        if data[UpDmg]:
            l.append(pre + colorizeNumber(data[UpDmg]) + ' attack damage per level')
        if data[TowerSpec]:
            for temp in data[TowerSpec].split(nl):
                l.append(pre + colorizeAllNumbers(temp))
        if l:
            s += 'Tower:' + nl + sep.join(l) + '.' + nl
        # Ability Ups
        if data[AbilityUps]:
            s += colorize(Color_Ability, data[Ability]) + ':' + nl
            temp = pre + data[AbilityUps].replace(nl, sep + pre)
            s += colorize_desc(temp, Pattern_Modes) + '.' + nl
        tip.content = s
