from Sheets.TowerAbilities import *
from Colors import *
from Misc import nl
from wtsParser import FindByComment, Type_Abil, Pos_UberTip


def process_desc(desc: str) -> str:
    return colorize_desc(desc, Pattern_Modes)


def generate():
    for data in info:
        # Get old tooltip
        tip = FindByComment(Type_Abil, data[Rawcode], Pos_UberTip)
        if not tip:
            continue
        s = ''
        # Active description
        if data[ActiveDesc]:
            s += process_desc(data[ActiveDesc]) + nl * 2
        # Passive description
        if data[PassiveDesc]:
            s += Word_Passive + nl + process_desc(data[PassiveDesc]) + nl * 2
        # Important
        if data[Important]:
            s += colorize(Color_Important, data[Important]) + nl * 2
        tip.content = s.strip() + nl
