from Misc import list2pattern, re, replace


def colorize(color: str, text: str) -> str:
    if text:
        return '|cff' + color + text + '|r'
    return text


# Colors
Color_Header = 'aaaaaa'
Color_Hotkey = 'ffcc00'
Color_Important = 'ffcc80'
Color_Modes = 'cc80cc'
Color_Numbers = '00ccff'
Color_Gold = 'e2b007'
Color_Experience = '80ccff'
Color_Supply = 'd15b8f'
Color_Income = '76a5af'
Color_Lives = '00bb2e'
Color_Ability = '80cccc'
Color_Active = 'ff0080'
Color_Passive = '00ff80'
Color_Physical = 'ff9933'
Color_Magical = '9933cc'
Color_Percentage = 'f1f178'
# Color_MSFactor = '3789c4'
Color_Health = '33cc33'
Color_Regen = '48a43f'
Color_Class = 'ddddcc'
Color_Night = '674ea7'
Color_Day = 'f1f178'
# Words
Word_Active = colorize(Color_Active, 'Active')
Word_Passive = colorize(Color_Passive, 'Passive')
Word_Day = colorize(Color_Day, 'Day')
Word_Night = colorize(Color_Night, 'Night')
# Patterns
Pattern_Numbers = re.compile(r'1st|2nd|3rd|\d+th|[-+x]?(\d*\.)?\d+%?')


def colorizeNumber(text: str) -> str:
    return colorize(Color_Numbers, text)


def colorizeByPattern(text: str, color: str, pattern: re) -> str:
    return replace(text, pattern, lambda x: colorize(color, x))


def colorizeWord(text: str, color: str, word: str) -> str:
    if word:
        return text.replace(word, colorize(color, word))
    return text


def colorizeListElements(text: str, color: str, l: list) -> str:
    return colorizeByPattern(text, color, list2pattern(l))


def colorizeAllNumbers(text: str) -> str:
    return colorizeByPattern(text, Color_Numbers, Pattern_Numbers)


def colorizeDayNight(text: str) -> str:
    return text.replace('Day', Word_Day).replace('Night', Word_Night)


def colorize_desc(desc: str, pattern_modes: re = None, pattern_abils: re = None) -> str:
    desc = colorizeAllNumbers(desc)
    desc = colorizeDayNight(desc)
    desc = colorizeByPattern(desc, Color_Modes, pattern_modes)
    desc = colorizeByPattern(desc, Color_Ability, pattern_abils)
    return desc
