from Colors import colorizeAllNumbers
from Misc import nl, output

with open('input.txt', 'r', encoding='utf-8') as f:
    inp = f.read()
output(colorizeAllNumbers(inp) + nl)
