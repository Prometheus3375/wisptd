import gspread
from gspread.models import Worksheet
from oauth2client.service_account import ServiceAccountCredentials


path_to_creds = 'WispTD-reader.json'
table_name = 'Information'


scope = ['https://www.googleapis.com/auth/drive']
creds = ServiceAccountCredentials.from_json_keyfile_name(path_to_creds, scope)
client = gspread.authorize(creds)
table = client.open(table_name)

sheet_counter = -1


def GetNextSheet() -> Worksheet:
    global sheet_counter
    sheet_counter += 1
    return table.get_worksheet(sheet_counter)


Raws = GetNextSheet()
MinionStats = GetNextSheet()
MinionAbilities = GetNextSheet()
MinionUpgrades = GetNextSheet()
TowerStats = GetNextSheet()
TowerLevelReqs = GetNextSheet()
TowerAbilities = GetNextSheet()
TowerUps = GetNextSheet()
