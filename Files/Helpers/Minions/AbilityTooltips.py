from Sheets.MinionAbilities import *
from Colors import *
from Misc import nl, w2b, convert_range
from wtsParser import FindByComment, Type_Abil, Pos_UberTip


def process_desc(desc: str) -> str:
    return colorize_desc(desc, Pattern_Modes)


def generate():
    h_range = colorize(Color_Header, 'Range:') + ' '
    h_precd = colorize(Color_Header, 'Precooldown:') + ' '
    h_cd = colorize(Color_Header, 'Cooldown:') + ' '
    for data in info:
        # Get old tooltip
        rawcodes = data[Rawcode].split(', ')
        tips = [FindByComment(Type_Abil, raw, Pos_UberTip) for raw in rawcodes]
        if None in tips:
            continue
        # Spawn action description
        if data[SADesc]:
            s = process_desc(data[SADesc]) + nl * 2
        else:
            s = ''
        # Field action description
        if w2b(data[IsActive]):
            s += Word_Active + nl
        else:
            s += Word_Passive + nl
        s += process_desc(data[Desc]) + nl
        # Stats
        if data[Range]:
            s += h_range + colorizeNumber(convert_range(data[Range])) + nl
        if data[PreCD]:
            s += h_precd + colorizeNumber(data[PreCD]) + ' s' + nl
        if data[CD]:
            s += h_cd + colorizeNumber(data[CD]) + ' s' + nl
        # Important
        if data[Important]:
            s += nl + colorize(Color_Important, data[Important]) + nl
        for tip in tips:
            tip.content = s
