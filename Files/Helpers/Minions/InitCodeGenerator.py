from Sheets.MinionStats import *
from Misc import nl, output, w2b


def gen_func(name: str, args: list) -> str:
    return '        call data.' + name + '(' + ', '.join(args) + ')' + nl


def gen_comment(words: str):
    return '        // ' + words + nl


def word2boolstr(word: str) -> str:
    word = word.lower()
    if word == 'yes':
        return 'true'
    if word == 'no':
        return 'false'
    return '<invalid>'


def dot(s: str) -> str:
    if '.' in s:
        last = len(s) - 1
        if s[last] == '0':
            s = s[:last]
        return s
    return s + '.'


def bool2word(b: bool) -> str:
    return str(b).lower()


def raw(s: str) -> str:
    return '\'' + s + '\''


out = []
for data in info:
    s = '    -- ' + data[Name] + ' --    ' + nl
    # Color
    s += gen_comment('Color')
    # Tech
    s += gen_func('setTechInfo', [data[GoldCost], data[ExpCost], data[Supply], data[Income]])
    s += gen_func('setLimit', [data[Limit]])
    if int(data[Amount]) > 1:
        s += gen_func('setAmount', [data[Amount]])
    # Life transfer
    s += gen_func('setLifeTransfer', ['-' + data[LivesConsume], data[LivesGain]])
    # Flags
    b = bool2word(data[MSType].lower() != 'none')
    s += gen_func('setFlags', ['MinionClass_' + data[Class], word2boolstr(data[Special]),
                               word2boolstr(data[Decays]), b])
    # Rewards
    b = data[KGold] == data[LGold] and data[KExp] == data[LExp]
    s += gen_func('setKillRewards', [data[KGold], data[KExp], data[KTowerExp], data[KIncome], data[KLives],
                                     bool2word(b)])
    if not b:
        s += gen_func('setLeakRewards', [data[LGold], data[LExp]])
    # Health
    l = [dot(data[MaxHealth])]
    if data[Regen] == '1':
        l += ['DefaultRegen']
    elif data[Regen] == '0':
        l += ['0.']
    elif data[Regen] == '-1':
        l += ['-DefaultRegen']
    else:
        l += [dot(data[Regen]) + ' * DefaultRegen']
    l.append('Exact1_075')
    s += gen_func('setHealthInfo', l)
    # Stats
    v = data[MSFactor]
    v = str(float(v[:len(v) - 1]) / 100.)
    s += gen_func('setStats', [dot(v), data[PhysArmor], data[MagArmor]])
    # Spawn
    s += gen_func('setSpawnInfo', [raw(data[UnitRaw]), raw(data[SpawnRaw]), raw(data[InfoRaw]), 'create'])
    # Ability
    s += gen_comment('Main Ability')
    # Challenger
    if w2b(data[Challenger]):
        s += gen_func('makeAChallenger', [])
    out.append(s + nl * 2)
output(out)
