from Sheets.MinionStats import *
from Colors import *
from Misc import nl, w2b
from wtsParser import FindByComment, Type_Abil, Pos_UberTip, Pos_Tip, Pos_Hotkey


def reward_tip(res: str, color: str, krew: int, lrew: int = None) -> str:
    if lrew and krew + lrew > 0 and krew != lrew:
        return res + ': kill - ' + colorize(color, '+' + str(krew)) + \
               ', leak - ' + colorize(color, '+' + str(lrew)) + nl
    if krew > 0:
        return res + ': ' + colorize(color, '+' + str(krew)) + nl
    return ''


def generate():
    for data in info:
        chall = w2b(data[Challenger])
        merc = w2b(data[Merc])
        amount = int(data[Amount])
        has_spawn = data[SpawnRaw]
        # Update tip
        if has_spawn:
            spawn_ubertip = FindByComment(Type_Abil, data[SpawnRaw], Pos_UberTip)
            spawn_key = FindByComment(Type_Abil, data[SpawnRaw], Pos_Hotkey)
            spawn_tip = FindByComment(Type_Abil, data[SpawnRaw], Pos_Tip)
            if spawn_tip and spawn_key:
                if chall:
                    s = 'Challenge '
                elif merc:
                    s = 'Hire '
                else:
                    s = 'Spawn '
                if amount > 1:
                    s += colorizeNumber(data[Amount]) + ' ' + data[Plural]
                else:
                    s += data[Name]
                spawn_tip.content = s + ' (' + colorize(Color_Hotkey, spawn_key.content.strip()) + ')' + nl
        # Update tooltip
        info_tip = FindByComment(Type_Abil, data[InfoRaw], Pos_UberTip)
        # Old tooltip not found -> skip
        if not info_tip:
            continue
        if has_spawn:
            # Tech
            newspawn = []
            gold = int(data[GoldCost]) * amount
            exp = int(data[ExpCost]) * amount
            supply = int(float(data[Supply]) * amount)
            income = int(data[Income]) * amount
            limit = int(data[Limit])
            if gold + exp + supply > 0:
                l = []
                if gold > 0:
                    l.append(colorize(Color_Gold, str(gold)) + ' Gold')
                if exp > 0:
                    l.append(colorize(Color_Experience, str(exp)) + ' Experience')
                if supply > 0:
                    l.append(colorize(Color_Supply, str(supply)) + ' Supply')
                newspawn.append('Cost: ' + ', '.join(l))
            if income > 0:
                newspawn.append('Income Gain: ' + colorize(Color_Income, '+' + str(income)))
            if limit > 0:
                newspawn.append('Limit: ' + colorizeNumber(data[Limit]))
        # Stats
        newinfo = ['Class: ' + colorize(Color_Class, data[Class])]
        regen = '%.2f' % (float(data[Regen]) * float(data[MaxHealth]) / FullRegenTimeSec)
        newinfo.append('Health: ' + colorize(Color_Health, data[MaxHealth]) +
                       ' (' + colorize(Color_Regen, '+' + regen) + ' / s)')
        newinfo.append('Armor: ' + colorizeNumber(data[PhysArmor]) + '\\' +
                       colorizeNumber(data[MagArmor]))
        newinfo.append('Movement: ' + colorizeNumber(data[MSFactor]) + '\\' + data[MSType])
        newinfo.append('Life Transfer: ' + colorizeNumber('-' + data[LivesConsume]) + '\\' +
                       colorizeNumber('+' + data[LivesGain]))
        s = 'Health Upgrade: ' + colorizeNumber('x' + data[HealthUp])
        if chall:
            newinfo.append(s + ' per wave after ' + colorizeNumber('10th') + ' wave')
        else:
            newinfo.append(s + ' per level')
        newinfo.append('Decays: ' + data[Decays])
        newinfo.append('Special: ' + data[Special])
        # Rewards
        gold = int(data[KGold])
        exp = int(data[KExp])
        texp = int(data[KTowerExp])
        income = int(data[KIncome])
        lives = int(data[KLives])
        lgold = int(data[LGold])
        lexp = int(data[LExp])
        if gold + exp + texp + income + lives + lgold + lexp > 0:
            s = nl + 'Rewards' + nl
            s += reward_tip('Gold', Color_Gold, gold, lgold)
            s += reward_tip('Experience', Color_Experience, exp, lexp)
            s += reward_tip('Income', Color_Income, income)
            s += reward_tip('Lives', Color_Lives, lives)
            s += reward_tip('Tower exp.', Color_Numbers, texp)
            newinfo.append(s.rstrip())
        if has_spawn:
            newspawn.append(nl + 'Field Stats')
            newspawn += newinfo
            # Ability
            s = nl
            abilities = [colorize(Color_Ability, a) for a in data[Ability].split(', ')]
            if len(abilities) > 1:
                s += 'Abilities: ' + ', '.join(abilities)
            else:
                s += 'Ability: ' + abilities[0]
            newspawn.append(s)

            spawn_ubertip.content = nl.join(newspawn) + nl
        info_tip.content = nl.join(newinfo) + nl
