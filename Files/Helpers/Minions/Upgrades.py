from Sheets.MinionUpgrades import *
from Sheets.MinionAbilities import Pattern_Modes, Pattern_Abilities
from Colors import *
from Misc import nl
from wtsParser import FindByComment, Type_Abil, Pos_UberTip


def process_desc(desc: str) -> str:
    return colorize_desc(desc, Pattern_Modes, Pattern_Abilities)


def generate():
    for data in info:
        # Get old tooltip
        button_tip = FindByComment(Type_Abil, data[ButtonRaw], Pos_UberTip)
        icon_tip = FindByComment(Type_Abil, data[IconRaw], Pos_UberTip)
        # Old tooltip not found -> skip
        if not (button_tip and icon_tip):
            continue
        # Button description
        button = process_desc(data[Desc]) + nl
        # Icon description
        if data[Desc] != data[MinionDesc]:
            icon = process_desc(data[MinionDesc]) + nl
        else:
            icon = button
        # Cost
        if data[GoldCost] or data[ExpCost]:
            l = []
            if data[GoldCost]:
                l.append(colorize(Color_Gold, data[GoldCost]) + ' Gold')
            if data[ExpCost]:
                l.append(colorize(Color_Experience, data[ExpCost]) + ' Experience')
            button = 'Cost: ' + ', '.join(l) + nl + button
        # Update tooltip
        button_tip.content = button
        icon_tip.content = icon
