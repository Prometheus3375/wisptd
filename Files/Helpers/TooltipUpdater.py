import Minions.Tooltips as MinionTooltips
import Minions.AbilityTooltips as MinionAbilityTooltips
import Minions.Upgrades as MinionUpgradeTooltips
import Towers.Tooltips as TowerTooltips
import Towers.AbilityTooltips as TowerAbilityTooltips
import Towers.UpsTooltips as TowerUpTooltips
from wtsParser import wtsUpdate

MinionTooltips.generate()
MinionAbilityTooltips.generate()
MinionUpgradeTooltips.generate()
TowerTooltips.generate()
TowerAbilityTooltips.generate()
TowerUpTooltips.generate()
wtsUpdate()
input('Press Enter to close this window...')
