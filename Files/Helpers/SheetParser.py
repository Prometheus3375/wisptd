from Spreadsheet import Worksheet,\
    Raws, MinionStats, MinionAbilities, MinionUpgrades, TowerStats, TowerLevelReqs, TowerAbilities, TowerUps


class Row:
    def __init__(self, row: int, key: int):
        self.row = row
        self.key = key


class Table:
    def __init__(self):
        self.objects = []
        self.counter = -1
        self.keycounter = -1

    def addRow(self, skip: int = 0) -> int:
        self.counter += 1 + skip
        self.keycounter += 1
        self.objects.append(Row(self.counter, self.keycounter))
        return self.keycounter


def parse_rows(table: Table, inp: list) -> list:
    inp = [l[1:] for l in inp]  # get rid of 1st column
    info = []
    first = table.objects[0].row
    L = len(inp[first])
    # Create dicts
    for i in range(L):
        info.append(dict())
    # Parse rows
    for r in table.objects:
        temp = inp[r.row]
        for j in range(min(L, len(temp))):
            info[j][r.key] = temp[j]
    # print('Parsing completed')
    return info


def ParseByRows(table: Table, sheet: Worksheet) -> list:
    return parse_rows(table, sheet.get_all_values())


def ParseByColumns(table: Table, sheet: Worksheet) -> list:
    return parse_rows(table, list(zip(*sheet.get_all_values())))


def ParseMinionStats(table: Table) -> list:
    return ParseByRows(table, MinionStats)


def ParseMinionAbilities(table: Table) -> list:
    return ParseByColumns(table, MinionAbilities)


def ParseMinionUpgrades(table: Table) -> list:
    return ParseByColumns(table, MinionUpgrades)


def ParseTowerStats(table: Table) -> list:
    return ParseByRows(table, TowerStats)


def ParseTowerAbilities(table: Table) -> list:
    return ParseByColumns(table, TowerAbilities)


def ParseTowerUps(table: Table) -> list:
    return ParseByColumns(table, TowerUps)
