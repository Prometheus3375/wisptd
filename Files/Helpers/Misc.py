import re


nl = '\n'


def input_str() -> str:
    with open('input.txt', 'r', encoding='utf-8') as f:
        return f.read()


def input_list() -> list:
    with open('input.txt', 'r', encoding='utf-8') as f:
        return f.readlines()


def output(out):
    with open('output.txt', 'w', encoding='utf-8') as f:
        if isinstance(out, str):
            f.write(out)
        elif isinstance(out, list):
            f.writelines(out)


def w2b(word: str) -> bool:
    word = word.lower()
    if word == 'yes':
        return True
    if word == 'no':
        return False
    raise ValueError(word + ' is not valid flag')


def convert_range(s: str) -> str:
    r = float(s) / 128
    ir = int(r)
    if r == ir:
        return str(ir)
    return '%.1f' % r


def seconds(s: str) -> str:
    if float(s) == 1:
        return ' second'
    return ' seconds'


def list2pattern(l: list) -> re:
    if l:
        return re.compile('|'.join(l))
    return None


def replace(text: str, pattern: re, replace_func) -> str:
    if pattern:
        it = re.finditer(pattern, text)
        out = ''
        i = 0
        for m in it:
            start = m.start()
            out += text[i:start] + replace_func(m.group())
            i = m.end()
        if i < len(text):
            out += text[i:]
        return out
    return text


def GetAbilsAndModes(info: list, namekey: int, modekey: int) -> tuple:
    abils = []
    modes = []
    for data in info:
        if not (data[namekey] in abils):
            abils.append(data[namekey])
        if data[modekey] and not (data[modekey] in modes):
            modes.append(data[modekey])
    pabils = list2pattern(abils)
    pmodes = list2pattern(modes)

    def modes_of_abil(name: str) -> list:
        if not (name in abils):
            raise ValueError('Ability "' + name + '" is not in the ability list')
        return [v[modekey] for v in info if v[modekey] and v[namekey] == name]
    return abils, modes, pabils, pmodes, modes_of_abil
