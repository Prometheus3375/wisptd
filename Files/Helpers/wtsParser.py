from Misc import nl
import re

path = '..\\Strings.wts'
encoding = 'utf-8-sig'
Type_Unit = 'Units'
Type_Item = 'Items'
Type_Abil = 'Abilities'

Pos_Name = 'Name'
Pos_Tip = 'Tip'
Pos_UberTip = 'Ubertip'
Pos_Hotkey = 'Hotkey'


class WC3Str:
    strings = []

    def __init__(self, id: str, comment: str, content: str):
        self.id = id
        self.comment = comment
        self.content = content
        WC3Str.strings.append(self)

    def __str__(self):
        return self.id +\
               self.comment +\
               '{' + nl +\
               self.content +\
               '}' + nl * 2


def FindByComment(typ: str, raw: str, pos: str) -> WC3Str:
    pattern = re.compile('// ' + typ + ': ' + re.escape(raw) + r' \([a-zA-Z0-9 ]+\), ' + pos)
    for s in WC3Str.strings:
        if pattern.match(s.comment):
            return s
    print('String with typ = ' + typ + ', raw = ' + raw + ', pos = ' + pos + ' is not found.')
    return None


with open(path, 'r', encoding=encoding) as f:
    wts = f.readlines()
id = ''
comment = ''
content = ''
outside = True
for line in wts:
    if outside:
        if line.startswith('STRING '):
            id = line
        elif line.startswith('// '):
            comment += line
        elif line.startswith('{'):
            outside = False
    elif line.startswith('}'):
        WC3Str(id, comment, content)
        id = ''
        comment = ''
        content = ''
        outside = True
    else:
        content += line
print('Strings.wts parsing is complete. The file contains', len(WC3Str.strings), 'string(s)')


def wtsUpdate():
    # print('Strings.wts updating begins. The file will contain', len(WC3Str.strings), 'string(s)')
    with open(path, 'w', encoding=encoding) as f:
        out = ''
        for s in WC3Str.strings:
            out += str(s)
        f.write(out)
