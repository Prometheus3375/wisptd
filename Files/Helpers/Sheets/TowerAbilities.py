from SheetParser import Table, ParseTowerAbilities
from Misc import GetAbilsAndModes

table = Table()
Name = table.addRow()
Mode = table.addRow()
ActiveDesc = table.addRow()
PassiveDesc = table.addRow()
Important = table.addRow()
Rawcode = table.addRow()


info = ParseTowerAbilities(table)
AbilityList, ModeList, Pattern_Abilities, Pattern_Modes, GetModesOfAbility = GetAbilsAndModes(info, Name, Mode)
