from SheetParser import Table, ParseMinionAbilities
from Misc import GetAbilsAndModes

table = Table()
Name = table.addRow()
Mode = table.addRow()
SADesc = table.addRow()
IsActive = table.addRow()
Desc = table.addRow()
Range = table.addRow()
PreCD = table.addRow()
CD = table.addRow()
Important = table.addRow()
Rawcode = table.addRow()


info = ParseMinionAbilities(table)
AbilityList, ModeList, Pattern_Abilities, Pattern_Modes, GetModesOfAbility = GetAbilsAndModes(info, Name, Mode)
