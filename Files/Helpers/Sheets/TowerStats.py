from SheetParser import Table, ParseTowerStats

table = Table()
Towers = table.addRow()
GoldCost = table.addRow()
Limit = table.addRow()
Income = table.addRow()

Ability = table.addRow(1)

Damage = table.addRow(1)
UpDmg = table.addRow()
DmgCD = table.addRow()
DmgType = table.addRow()
Range = table.addRow()
Targets = table.addRow()
MaxTargets = table.addRow()
Explosive = table.addRow()

UnitRaw = table.addRow(1)
InfoRaw = table.addRow()


info = ParseTowerStats(table)
