from SheetParser import Table, ParseMinionStats

FullRegenTimeSec = 180.

table = Table()
Name = table.addRow()
Plural = table.addRow()
GoldCost = table.addRow()
ExpCost = table.addRow()
Supply = table.addRow()
Income = table.addRow()
Limit = table.addRow()
Amount = table.addRow()

Ability = table.addRow(1)

LivesConsume = table.addRow(1)
LivesGain = table.addRow()

KGold = table.addRow(1)
KExp = table.addRow()
KTowerExp = table.addRow()
KIncome = table.addRow()
KLives = table.addRow()
LGold = table.addRow()
LExp = table.addRow()

MaxHealth = table.addRow(2)
Regen = table.addRow()
HealthUp = table.addRow()
MSFactor = table.addRow()
MSType = table.addRow()
PhysArmor = table.addRow()
MagArmor = table.addRow()

Class = table.addRow(1)
Decays = table.addRow()
Special = table.addRow()
Merc = table.addRow()
Challenger = table.addRow()

UnitRaw = table.addRow(1)
SpawnRaw = table.addRow()
InfoRaw = table.addRow()


info = ParseMinionStats(table)
