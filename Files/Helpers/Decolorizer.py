import re
from Misc import output, input_str

pattern = re.compile(r'\|c[a-f0-9]{8}|\|r')
output(pattern.sub('', input_str()))
