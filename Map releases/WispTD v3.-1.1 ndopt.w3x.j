globals
constant integer o=$D00A6
constant integer A=$D00A5
constant integer B=$D00FA
constant integer C=$D0097
constant integer D=$D00FE
constant integer E=$D0057
constant integer F=$D0106
constant integer G=$D0012
constant integer H=$D0004
constant integer I=$D0058
constant integer J=$D0008
constant integer K=$D0005
constant integer L=$D028B
constant real M=GetCameraMargin(CAMERA_MARGIN_LEFT)
constant real N=GetCameraMargin(CAMERA_MARGIN_RIGHT)
constant real O=GetCameraMargin(CAMERA_MARGIN_BOTTOM)
constant real P=GetCameraMargin(CAMERA_MARGIN_TOP)
constant real Q=.023/ 10.
constant real R=.071/ 128.
player S
boolean U=true
constant integer V=$A
player array W
integer X
constant playercolor Y=PLAYER_COLOR_RED
constant unittype Z=UNIT_TYPE_MECHANICAL
constant unittype d4=UNIT_TYPE_GIANT
constant unittype e4=UNIT_TYPE_SAPPER
constant unittype f4=UNIT_TYPE_TAUREN
constant unittype g4=UNIT_TYPE_ANCIENT
boolexpr h4
integer i4
integer array j4
constant real k4=-1472.
constant real m4=-1792.
constant real n4=-1920.
constant real o4=-8448.
constant real p4=m4
constant integer q4=256/ 64
constant integer r4=R2I(1024.)/ 64
constant integer s4=R2I(640.-p4)/ 64
constant integer t4=608-r4*q4
constant integer u4=t4+r4
integer v4=0
constant integer w4=$7D0
constant integer x4=E
constant integer y4=I
integer array z4
boolean array A4
player a4=null
constant integer B4=0+StringLength("Spawn")
constant integer b4=B4
constant integer C4=b4+2
constant integer c4=0+StringLength("Upgrade")
constant integer D4=c4
constant integer E4=D4+2
constant integer F4=E4+1
constant integer G4=F4+1
constant integer H4=G4+1
constant integer I4=-25
constant integer l4=-25
integer J4=0
integer K4=0
constant integer L4=51-1
integer array M4
integer N4
constant real O4=1./ 16.
boolexpr array P4
integer array Q4
constant integer R4=$A
constant integer S4=$B
constant integer T4=$C
constant integer U4=$D
constant integer V4=$E
constant integer W4=$F
constant integer X4=51-1
constant integer Y4=$80
integer array Z4
trigger array d7
trigger array e7
integer array f7
integer array g7
trigger array h7
integer array i7
constant integer j7=$A
integer array k7
constant timer m7=CreateTimer()
constant hashtable n7=InitHashtable()
region o7
region p7
integer q7=-1
integer r7=0
integer s7=1
integer t7=61
constant integer u7=$B
constant trigger v7=CreateTrigger()
constant trigger w7=CreateTrigger()
constant trigger x7=CreateTrigger()
constant trigger y7=CreateTrigger()
constant trigger z7=CreateTrigger()
constant trigger A7=CreateTrigger()
boolexpr a7
constant trigger B7=CreateTrigger()
constant trigger b7=CreateTrigger()
constant integer C7=StringLength("inc")
constant integer c7=C7+1
integer D7
boolexpr E7
constant trigger F7=CreateTrigger()
constant integer G7=StringLength("gold")
constant integer H7=G7+1
constant trigger I7=CreateTrigger()
constant integer l7=StringLength("exp")
constant integer J7=l7+1
constant trigger K7=CreateTrigger()
constant integer L7=StringLength("lives")
constant integer M7=L7+1
constant trigger N7=CreateTrigger()
constant integer O7=StringLength("wave")
constant integer P7=O7+1
constant trigger Q7=CreateTrigger()
integer R7
constant trigger S7=CreateTrigger()
constant trigger T7=CreateTrigger()
constant trigger U7=CreateTrigger()
constant trigger V7=CreateTrigger()
constant trigger W7=CreateTrigger()
constant trigger X7=CreateTrigger()
constant trigger Y7=CreateTrigger()
constant trigger Z7=CreateTrigger()
constant trigger d8=CreateTrigger()
constant trigger e8=CreateTrigger()
constant trigger f8=CreateTrigger()
constant trigger g8=CreateTrigger()
constant trigger h8=CreateTrigger()
constant trigger i8=CreateTrigger()
constant trigger j8=CreateTrigger()
constant trigger k8=CreateTrigger()
constant trigger m8=CreateTrigger()
sound n8
sound o8
sound p8
sound q8
sound r8
sound s8
sound t8
sound u8
constant real v8=-8960.
constant real w8=-1920.
constant real x8=(v8+6912.)/ 2.
constant real y8=(w8+1920.)/ 2.
constant trigger z8=CreateTrigger()
constant trigger A8=CreateTrigger()
multiboard a8
integer B8=0
constant real b8=2.*.0041
constant real C8=14.*.0041
constant real c8=6.*.0041
constant real D8=6.*.0041
constant real E8=6.*.0041
constant real F8=5.*.0041
constant real G8=5.*.0041
constant real H8=4.*.0041
constant real I8=9.*.0041
constant real l8=6.*.0041
multiboarditem J8
constant integer K8=$FF
constant trigger L8=CreateTrigger()
constant trigger M8=CreateTrigger()
integer array N8
integer O8=-1
integer array P8
integer Q8=-1
integer array R8
integer S8
integer T8=0
integer U8=0
integer array V8
integer array W8
integer array X8
integer array Y8
integer Z8=0
integer d9=0
integer array e9
integer array f9
integer array g9
integer array h9
integer i9=0
integer j9=0
integer array k9
integer array m9
integer array n9
integer array o9
integer p9=0
integer q9=0
integer array r9
integer array s9
integer array t9
integer array u9
integer v9=0
integer w9=0
integer array x9
integer array y9
integer array z9
integer array A9
integer a9=0
integer B9=0
integer array b9
integer array C9
integer array c9
integer array D9
integer E9=0
integer F9=0
integer array G9
boolean array H9
integer array I9
integer l9=-1
integer J9
integer K9
integer L9
integer M9
integer N9
integer O9
integer P9
integer Q9
integer R9
integer S9
integer T9
integer U9
integer array V9
integer array W9
integer array X9
string array Y9
integer Z9=0
integer ed=0
integer array fd
real array gd
integer array hd
integer jd=0
integer kd=0
integer array md
integer array nd
integer array od
integer array pd
integer array qd
integer array rd
integer array sd
string array td
integer ud=0
integer vd=0
integer array wd
integer array xd
integer array yd
integer array zd
integer array Ad
integer array ad
boolean array Bd
boolean array bd
integer array Cd
integer array Dd
integer array Ed
integer array Fd
integer array Gd
integer array Hd
integer Id=0
integer ld=0
integer array Jd
integer array Kd
integer array Ld
integer Md=0
integer Nd=0
integer array Od
integer array Pd
integer array Qd
integer array Rd
real array Sd
real array Td
real array Ud
real array Vd
rect array Wd
rect array Xd
rect array Yd
rect array Zd
rect array de
fogmodifier array ee
integer array fe
fogmodifier array ge
integer array he
boolean array ie
boolean array je
integer ke=0
integer me=0
integer array ne
unit array oe
integer array pe
timer array qe
integer array re
integer array se
integer te=0
integer ue=0
integer array ve
integer array we
integer array xe
integer array ye
integer array ze
real array Ae
integer array ae
integer array Be
real array be
integer Ce=0
integer ce=0
integer array De
integer array Ee
integer array Fe
integer array Ge
integer array He
integer array Ie
real array le
integer array Je
integer array Ke
integer array Le
real array Me
integer array Ne
integer array Oe
integer array Pe
boolean array Qe
integer array Re
integer array Se
integer array Te
boolean array Ue
integer array Ve
integer array We
integer array Xe
integer array Ye
integer array Ze
integer array df
integer array ef
integer array ff
integer array gf
integer array hf
integer array jf
integer array kf
boolean array mf
integer array nf
integer array of
integer array pf
boolean array qf
integer array rf
integer array sf
integer array tf
boolean array uf
integer array vf
integer array wf
integer array xf
boolean array yf
integer zf=0
integer Af=0
integer array af
player array Bf
integer array bf
string array Cf
integer array cf
integer array Df
boolean array Ef
boolean array Ff
player array Gf
boolean array Hf
integer array If
integer array lf
integer array Jf
integer array Kf
integer array Lf
integer array Mf
real array Nf
integer array Of
integer array Pf
integer array Qf
integer array Rf
multiboarditem array Sf
multiboarditem array Tf
multiboarditem array Uf
multiboarditem array Vf
multiboarditem array Wf
multiboarditem array Xf
multiboarditem array Yf
image array Zf
integer array dg
boolean array eg
unit array fg
unit array gg
unit array hg
unit array ig
unit array jg
integer array kg
integer array mg
integer array ng
integer array og
integer array pg
integer array qg
integer array rg
integer array sg
integer array tg
integer array ug
integer array vg
integer array wg
integer array xg
integer array yg
integer array zg
integer array Ag
integer array ag
integer array Bg
integer array bg
integer array Cg
integer array cg
integer array Dg
integer array Eg
integer array Fg
integer array Gg
integer array Hg
item array Ig
item array lg
item array Jg
integer array Kg
timer array Lg
integer Mg=0
integer Ng=0
integer array Og
integer Pg
integer Qg
integer array Rg
integer array Sg
integer array Tg
integer array Ug
unit array Vg
integer array Wg
integer array Xg
integer array Yg
boolean array Zg
boolean array dh
real array eh
real array fh
real array gh
integer array hh
real array ih
real array jh
real array kh
item array mh
item array nh
item array oh
item array ph
item array qh
item array rh
real array sh
real array th
real array uh
integer array vh
integer array wh
timer array xh
boolexpr yh
integer zh=0
integer Ah=0
integer array ah
integer array Bh
integer array bh
unit array Ch
integer array ch
integer array Dh
integer array Eh
boolean array Fh
integer array Gh
integer array Hh
timer array Ih
integer lh=0
integer Jh=0
integer array Kh
integer array Lh
unit array Mh
integer array Nh
integer array Oh
integer Ph=0
integer Qh=0
integer array Rh
integer array Sh
integer array Th
real array Uh
integer array Vh
integer array Wh
integer Xh=0
integer Yh=0
integer array Zh
integer array di
integer array ei
boolean array fi
boolean array gi
integer hi=0
integer ii=0
integer array ji
boolean array ki
boolean array mi
integer array ni
integer array oi
real array pi
real array qi
integer array ri
integer si=0
integer ti=0
integer array ui
unit array vi
integer array wi
integer xi=0
integer yi=0
integer array zi
constant real Ai=1./ 64.
constant integer Bi=$CC
constant integer bi=$CC
unit array Ci
image array ci
timer array Di
integer Ei=0
integer Fi=0
integer array Gi
integer Hi
integer array Ii
integer array li
boolean array Ji
timer array Ki
constant integer Li=$A
boolean array Mi
integer array Ni
real array Oi
constant integer Pi=$F
boolexpr Qi
integer array Ri
integer array Si
integer Ti
boolean Ui
constant real Vi=-.15
integer array Wi
integer array Xi
boolexpr Yi
boolean array Zi
real array dj
integer array ej
integer array fj
integer array gj
integer hj
integer ij
constant integer jj=B
real array kj
integer array mj
boolexpr nj
integer oj
integer pj
real qj
real array rj
boolexpr sj
code tj
timer array uj
integer vj
integer wj
real xj
integer yj=0
integer zj=0
integer array Aj
unit aj
integer Bj
real bj
real Cj
integer array cj
unit array Dj
integer array Ej
integer array Fj
boolean array Gj
real array Hj
real array Ij
real array lj
real array Jj
integer array Kj
real array Lj
integer array Mj
integer array Nj
integer array Oj
timer array Pj
image array Qj
integer array Rj
boolean array Sj
integer array Tj
integer array Uj
integer array Vj
integer array Wj
item array Xj
item array Yj
item array Zj
item array dk
integer array ek
integer array fk
code gk
boolexpr hk
integer ik
boolexpr jk
constant integer kk=$A
integer array mk
constant integer nk=$A
constant integer ok=$A
integer pk=0
integer qk=0
integer array rk
integer array sk
boolean array tk
boolean array uk
boolean array vk
real array wk
real array xk
real array yk
integer zk=0
integer Ak=0
integer array ak
integer Bk
integer array bk
integer array Ck
constant integer ck=D
code Dk
item array Ek
real array Fk
real array Gk
real array Hk
real array Ik
integer array lk
timer array Jk
constant real Kk=-.3
constant integer Lk=-25
real array Mk
constant real Nk=-.75
integer array Ok
integer array Pk
unit array Qk
unit array Rk
boolean array Sk
real array Tk
integer array Uk
item array Vk
timer array Wk
integer array Xk
integer array Yk
timer array Zk
constant integer dm=$80
constant integer em=$CC
constant integer fm=$FF
code gm
code hm
integer array im
real array jm
real array km
image array mm
integer array nm
real array om
unit array pm
effect array qm
integer array rm
integer array sm
integer array tm
timer array um
boolexpr vm
integer wm
boolexpr xm
integer ym
integer zm
boolexpr Am
real am
boolean Bm
real array bm
real array Cm
real array cm
code Dm
code Em
real array Fm
real array Gm
real array Hm
timer array Im
timer array lm
constant integer Jm=A
constant integer Km=o
constant integer Lm=$A
constant real Mm=1.-.1
constant real Nm=-.01
integer array Om
real array Pm
real array Qm
boolean array Rm
integer array Sm
item array Tm
integer array Um
timer array Vm
integer array Wm
integer array Xm
integer array Ym
integer array Zm
trigger array dn
integer array en
integer fn=0
integer gn=0
integer array hn
constant integer jn=0+StringLength("AbilityUpgrade")
constant integer kn=jn
constant integer mn=kn+2
constant integer nn=mn+1
constant integer on=nn+1
constant integer pn=on+1
constant integer qn=0+StringLength("HeroAbility")
constant integer rn=qn
constant integer sn=rn+2
integer tn
integer array un
integer array vn
integer array wn
integer array xn
integer array yn
string array zn
integer array An
integer array an
integer array Bn
integer array bn
integer array Cn
integer array cn
integer array Dn
integer array En
integer array Fn
integer array Gn
trigger array Hn
integer array In
integer ln=0
integer Jn=0
integer array Kn
integer array Ln
integer array Mn
integer array Nn
real array On
integer array Pn
boolean array Qn
boolean array Rn
integer array Sn
integer array Tn
real array Un
integer array Vn
integer array Wn
integer array Xn
integer array Yn
integer array Zn
integer array do
integer array eo
integer array fo
real array go
real array ho
real array io
real array jo
real array ko
integer array mo
integer array no
boolean array oo
integer array po
integer qo=0
integer ro=0
integer array so
integer array to
integer array uo
integer array vo
integer array wo
integer array xo
integer array yo
integer array zo
integer array Ao
integer array ao
integer array Bo
trigger array bo
integer array Co
integer co=0
integer Do=0
integer array Eo
integer array Fo
integer array Go
integer array Ho
integer array Io
integer array lo
integer array Jo
string array Ko
real array Lo
integer array Mo
real array No
integer array Oo
boolexpr array Po
real array Qo
integer array Ro
boolean array So
integer array To
integer array Uo
real array Vo
real array Wo
integer array Xo
integer array Yo
boolean array Zo
integer array dp
integer array ep
boolean array fp
integer array gp
boolean array hp
boolean array ip
integer array jp
integer array kp
integer array mp
integer array np
integer array op
integer array pp
trigger array qp
trigger array rp
trigger array sp
trigger array tp
trigger array vp
trigger array wp
trigger array xp
trigger array yp
trigger array zp
trigger array Ap
trigger array ap
trigger array Bp
trigger array bp
trigger array Cp
trigger array cp
trigger array Dp
trigger array Ep
trigger array Fp
trigger array Gp
trigger array Hp
trigger array Ip
trigger array lp
trigger array Jp
trigger array Kp
trigger array Lp
trigger array Mp
trigger array Np
trigger array Op
trigger array Pp
integer array Qp
trigger array Rp
integer array Sp
trigger array Tp
integer array Up
trigger array Vp
integer array Wp
trigger array Xp
integer array Yp
trigger array Zp
integer array dq
trigger array eq
trigger fq
trigger gq
trigger hq
trigger iq
trigger jq
trigger kq
trigger mq
trigger nq
trigger oq
trigger pq
trigger qq
trigger rq
trigger sq
trigger tq
trigger uq
trigger array vq
trigger array wq
trigger array xq
trigger array yq
trigger array zq
integer Aq
integer aq
integer Bq
integer bq
player Cq
real cq
boolean Dq
integer Eq
real Fq
boolean Gq
endglobals
native UnitAlive takes unit id returns boolean
function fr takes nothing returns integer
local integer gr=co
if(gr!=0)then
set co=Eo[gr]
else
set Do=Do+1
set gr=Do
endif
if(gr>$AA9)then
return 0
endif
set Ho[gr]=(gr-1)*3
set Eo[gr]=-1
return gr
endfunction
function hr takes nothing returns integer
local integer gr=qo
if(gr!=0)then
set qo=so[gr]
else
set ro=ro+1
set gr=ro
endif
if(gr>$AA9)then
return 0
endif
set uo[gr]=(gr-1)*3
set wo[gr]=(gr-1)*3
set yo[gr]=(gr-1)*3
set Ao[gr]=(gr-1)*3
set Bo[gr]=(gr-1)*3
set Co[gr]=(gr-1)*3
set so[gr]=-1
return gr
endfunction
function jr takes nothing returns integer
local integer gr=ln
if(gr!=0)then
set ln=Kn[gr]
else
set Jn=Jn+1
set gr=Jn
endif
if(gr>$AA9)then
return 0
endif
set po[gr]=(gr-1)*3
set Kn[gr]=-1
return gr
endfunction
function kr takes nothing returns integer
local integer gr=fn
if(gr!=0)then
set fn=hn[gr]
else
set gn=gn+1
set gr=gn
endif
if(gr>$FFE)then
return 0
endif
set an[gr]=(gr-1)*2
set bn[gr]=(gr-1)*2
set cn[gr]=(gr-1)*2
set En[gr]=(gr-1)*2
set Gn[gr]=(gr-1)*2
set In[gr]=(gr-1)*2
set hn[gr]=-1
return gr
endfunction
function mr takes nothing returns integer
local integer gr=T8
if(gr!=0)then
set T8=V8[gr]
else
set U8=U8+1
set gr=U8
endif
if(gr>67)then
return 0
endif
set X8[gr]=(gr-1)*120
set Y8[gr]=-1
set V8[gr]=-1
return gr
endfunction
function nr takes nothing returns integer
local integer gr=Z8
if(gr!=0)then
set Z8=e9[gr]
else
set d9=d9+1
set gr=d9
endif
if(gr>77)then
return 0
endif
set g9[gr]=(gr-1)*105
set h9[gr]=-1
set e9[gr]=-1
return gr
endfunction
function pr takes nothing returns integer
local integer gr=i9
if(gr!=0)then
set i9=k9[gr]
else
set j9=j9+1
set gr=j9
endif
if(gr>77)then
return 0
endif
set n9[gr]=(gr-1)*105
set o9[gr]=-1
set k9[gr]=-1
return gr
endfunction
function qr takes nothing returns integer
local integer gr=p9
if(gr!=0)then
set p9=r9[gr]
else
set q9=q9+1
set gr=q9
endif
if(gr>77)then
return 0
endif
set t9[gr]=(gr-1)*105
set u9[gr]=-1
set r9[gr]=-1
return gr
endfunction
function rr takes nothing returns integer
local integer gr=v9
if(gr!=0)then
set v9=x9[gr]
else
set w9=w9+1
set gr=w9
endif
if(gr>77)then
return 0
endif
set z9[gr]=(gr-1)*105
set A9[gr]=-1
set x9[gr]=-1
return gr
endfunction
function sr takes nothing returns integer
local integer gr=a9
if(gr!=0)then
set a9=b9[gr]
else
set B9=B9+1
set gr=B9
endif
if(gr>77)then
return 0
endif
set c9[gr]=(gr-1)*105
set D9[gr]=-1
set b9[gr]=-1
return gr
endfunction
function tr takes nothing returns integer
local integer gr=E9
if(gr!=0)then
set E9=G9[gr]
else
set F9=F9+1
set gr=F9
endif
if(gr>$FFE)then
return 0
endif
set I9[gr]=(gr-1)*2
set G9[gr]=-1
return gr
endfunction
function ur takes nothing returns integer
local integer gr=Z9
if(gr!=0)then
set Z9=fd[gr]
else
set ed=ed+1
set gr=ed
endif
if(gr>$9F)then
return 0
endif
set hd[gr]=(gr-1)*51
set fd[gr]=-1
return gr
endfunction
function vr takes integer m,integer T returns integer
set aq=m
set Bq=T
call TriggerEvaluate(uq)
return Aq
endfunction
function wr takes integer gr returns nothing
set Eq=gr
call TriggerEvaluate(Pp[dq[gr]])
endfunction
function xr takes nothing returns integer
local integer gr=zk
if(gr!=0)then
set zk=ak[gr]
else
set Ak=Ak+1
set gr=Ak
endif
if(gr>8190)then
return 0
endif
set dq[gr]=85
set ak[gr]=-1
return gr
endfunction
function yr takes integer gr returns nothing
if gr==null then
return
elseif(ak[gr]!=-1)then
return
endif
set Eq=gr
call TriggerEvaluate(eq[dq[gr]])
set ak[gr]=zk
set zk=gr
endfunction
function zr takes nothing returns integer
local integer gr=pk
if(gr!=0)then
set pk=rk[gr]
else
set qk=qk+1
set gr=qk
endif
if(gr>8190)then
return 0
endif
set rk[gr]=-1
return gr
endfunction
function Ar takes nothing returns integer
local integer gr=jd
if(gr!=0)then
set jd=md[gr]
else
set kd=kd+1
set gr=kd
endif
if(gr>$7FE)then
return 0
endif
set od[gr]=(gr-1)*4
set md[gr]=-1
return gr
endfunction
function ar takes nothing returns integer
local integer gr=ud
if(gr!=0)then
set ud=wd[gr]
else
set vd=vd+1
set gr=vd
endif
if(gr>8190)then
return 0
endif
set Bd[gr]=true
set wd[gr]=-1
return gr
endfunction
function Br takes integer gr returns nothing
if gr==null then
return
elseif(wd[gr]!=-1)then
return
endif
set wd[gr]=ud
set ud=gr
endfunction
function br takes nothing returns integer
local integer gr=Id
if(gr!=0)then
set Id=Jd[gr]
else
set ld=ld+1
set gr=ld
endif
if(gr>8190)then
return 0
endif
set Kd[gr]=0
set Jd[gr]=-1
return gr
endfunction
function Cr takes integer gr returns nothing
if gr==null then
return
elseif(Jd[gr]!=-1)then
return
endif
set Jd[gr]=Id
set Id=gr
endfunction
function cr takes integer gr returns nothing
set Eq=gr
call TriggerExecute(fq)
endfunction
function Dr takes integer gr returns nothing
set Eq=gr
call TriggerExecute(gq)
endfunction
function Er takes integer gr returns nothing
set Eq=gr
call TriggerExecute(hq)
endfunction
function Fr takes integer gr returns nothing
set Eq=gr
call TriggerExecute(iq)
endfunction
function Gr takes integer gr,integer q returns nothing
set Eq=gr
set aq=q
call TriggerExecute(jq)
endfunction
function Hr takes nothing returns integer
local integer gr=Md
if(gr!=0)then
set Md=Od[gr]
else
set Nd=Nd+1
set gr=Nd
endif
if(gr>$C)then
return 0
endif
set Rd[gr]=(gr-1)*608
set fe[gr]=(gr-1)*$A
set he[gr]=(gr-1)*$A
set Od[gr]=-1
return gr
endfunction
function Ir takes nothing returns integer
local integer gr=ke
if(gr!=0)then
set ke=ne[gr]
else
set me=me+1
set gr=me
endif
if(gr>8190)then
return 0
endif
set pe[gr]=$F7
set ne[gr]=-1
return gr
endfunction
function lr takes integer gr returns nothing
if gr==null then
return
elseif(ne[gr]!=-1)then
return
endif
set ne[gr]=ke
set ke=gr
endfunction
function Jr takes nothing returns integer
local integer gr=te
if(gr!=0)then
set te=ve[gr]
else
set ue=ue+1
set gr=ue
endif
if(gr>8190)then
return 0
endif
set ye[gr]=0
set ze[gr]=0
set Qp[gr]=31
set ve[gr]=-1
return gr
endfunction
function Kr takes integer gr returns nothing
if gr==null then
return
elseif(ve[gr]!=-1)then
return
endif
set Eq=gr
call TriggerEvaluate(Rp[Qp[gr]])
set ve[gr]=te
set te=gr
endfunction
function Lr takes nothing returns integer
local integer gr=Jr()
local integer Mr
if(gr==0)then
return 0
endif
set Qp[gr]=32
set Mr=gr
set Ae[gr]=.0
return gr
endfunction
function Nr takes nothing returns integer
local integer gr=Jr()
local integer Mr
if(gr==0)then
return 0
endif
set Qp[gr]=33
set Mr=gr
set ae[gr]=0
return gr
endfunction
function Pr takes integer gr returns real
set Eq=gr
call TriggerEvaluate(Hp[Yp[gr]])
return Fq
endfunction
function Qr takes integer gr returns real
set Eq=gr
call TriggerEvaluate(Ip[Yp[gr]])
return Fq
endfunction
function Rr takes integer gr returns real
set Eq=gr
call TriggerEvaluate(lp[Yp[gr]])
return Fq
endfunction
function Sr takes integer gr returns nothing
set Eq=gr
call TriggerEvaluate(Jp[Yp[gr]])
endfunction
function Tr takes integer gr,integer m returns nothing
set Eq=gr
set aq=m
call TriggerEvaluate(Kp[Yp[gr]])
endfunction
function Ur takes integer gr,integer Vr returns nothing
set Eq=gr
set aq=Vr
call TriggerExecute(Lp[Yp[gr]])
endfunction
function Wr takes integer gr,integer m returns nothing
set Eq=gr
set aq=m
call TriggerExecute(Mp[Yp[gr]])
endfunction
function Xr takes integer gr returns nothing
set Eq=gr
call TriggerEvaluate(Np[Yp[gr]])
endfunction
function Yr takes integer gr returns nothing
set Eq=gr
call TriggerEvaluate(Op[Yp[gr]])
endfunction
function Zr takes nothing returns integer
local integer gr=yj
if(gr!=0)then
set yj=Aj[gr]
else
set zj=zj+1
set gr=zj
endif
if(gr>8190)then
return 0
endif
set Gj[gr]=false
set Lj[gr]=1.
set Rj[gr]=0
set Tj[gr]=0
set Uj[gr]=0
set ek[gr]=0
set Yp[gr]=70
set Aj[gr]=-1
return gr
endfunction
function ds takes integer gr returns nothing
if gr==null then
return
elseif(Aj[gr]!=-1)then
return
endif
set Eq=gr
call TriggerEvaluate(Zp[Yp[gr]])
set Aj[gr]=yj
set yj=gr
endfunction
function es takes nothing returns integer
local integer gr=Jr()
local integer Mr
if(gr==0)then
return 0
endif
set Qp[gr]=34
set Mr=gr
set Be[gr]=0
set be[gr]=.0
return gr
endfunction
function fs takes nothing returns integer
local integer gr=Ce
if(gr!=0)then
set Ce=De[gr]
else
set ce=ce+1
set gr=ce
endif
if(gr>8190)then
return 0
endif
set Ee[gr]=0
set Ge[gr]=0
set Ie[gr]=0
set le[gr]=.0
set Le[gr]=0
set Me[gr]=.0
set Oe[gr]=0
set Pe[gr]=0
set Qe[gr]=false
set Se[gr]=0
set Te[gr]=0
set Ue[gr]=false
set We[gr]=0
set Xe[gr]=0
set Ze[gr]=0
set df[gr]=0
set ff[gr]=0
set gf[gr]=0
set jf[gr]=0
set kf[gr]=0
set mf[gr]=false
set of[gr]=0
set pf[gr]=0
set qf[gr]=false
set sf[gr]=0
set tf[gr]=0
set uf[gr]=false
set wf[gr]=0
set xf[gr]=0
set yf[gr]=false
set De[gr]=-1
return gr
endfunction
function gs takes player p,integer i returns nothing
set Cq=p
set aq=i
call TriggerExecute(mq)
endfunction
function hs takes integer gr returns nothing
set Eq=gr
call TriggerExecute(nq)
endfunction
function is takes integer gr returns nothing
set Eq=gr
call TriggerExecute(oq)
endfunction
function js takes nothing returns integer
local integer gr=zf
if(gr!=0)then
set zf=af[gr]
else
set Af=Af+1
set gr=Af
endif
if(gr>67)then
return 0
endif
set dg[gr]=(gr-1)*120
set tg[gr]=(gr-1)*32
set vg[gr]=(gr-1)*32
set xg[gr]=(gr-1)*32
set zg[gr]=(gr-1)*32
set Bg[gr]=(gr-1)*24
set Ef[gr]=true
set Hf[gr]=false
set Kf[gr]=w4
set Lf[gr]=0
set Mf[gr]=40
set Nf[gr]=.0
set Of[gr]=500
set Pf[gr]=50
set Qf[gr]=0
set eg[gr]=true
set rg[gr]=0
set Ag[gr]=0
set bg[gr]=0
set Cg[gr]=0
set Kg[gr]=0
set af[gr]=-1
return gr
endfunction
function ks takes integer gr returns nothing
set Eq=gr
call TriggerEvaluate(cp[Wp[gr]])
endfunction
function ms takes integer gr,integer ns returns nothing
set Eq=gr
set aq=ns
call TriggerEvaluate(Dp[Wp[gr]])
endfunction
function os takes integer gr,integer ns,integer ps,integer qs returns boolean
set Eq=gr
set aq=ns
set Bq=ps
set bq=qs
call TriggerEvaluate(Ep[Wp[gr]])
return Gq
endfunction
function rs takes integer gr,real r returns nothing
set Eq=gr
set cq=r
call TriggerEvaluate(tq)
endfunction
function ss takes integer gr returns nothing
set Eq=gr
call TriggerEvaluate(Fp[Wp[gr]])
endfunction
function ts takes integer gr returns nothing
set Eq=gr
call TriggerEvaluate(Gp[Wp[gr]])
endfunction
function us takes nothing returns integer
local integer gr=Ei
if(gr!=0)then
set Ei=Gi[gr]
else
set Fi=Fi+1
set gr=Fi
endif
if(gr>8190)then
return 0
endif
set Ji[gr]=false
set Wp[gr]=61
set Gi[gr]=-1
return gr
endfunction
function vs takes integer gr returns nothing
if gr==null then
return
elseif(Gi[gr]!=-1)then
return
endif
set Eq=gr
call TriggerEvaluate(Xp[Wp[gr]])
set Gi[gr]=Ei
set Ei=gr
endfunction
function ws takes nothing returns integer
local integer gr=xi
if(gr!=0)then
set xi=zi[gr]
else
set yi=yi+1
set gr=yi
endif
if(gr>8190)then
return 0
endif
set zi[gr]=-1
return gr
endfunction
function xs takes nothing returns integer
local integer gr=si
if(gr!=0)then
set si=ui[gr]
else
set ti=ti+1
set gr=ti
endif
if(gr>8190)then
return 0
endif
set ui[gr]=-1
return gr
endfunction
function ys takes nothing returns integer
local integer gr=hi
if(gr!=0)then
set hi=ji[gr]
else
set ii=ii+1
set gr=ii
endif
if(gr>8190)then
return 0
endif
set ji[gr]=-1
return gr
endfunction
function zs takes integer gr returns nothing
set Eq=gr
call TriggerEvaluate(rp[Sp[gr]])
endfunction
function As takes integer gr returns real
set Eq=gr
call TriggerEvaluate(sp[Sp[gr]])
return Fq
endfunction
function as takes integer gr returns integer
set Eq=gr
call TriggerEvaluate(tp[Sp[gr]])
return Aq
endfunction
function Bs takes integer gr returns integer
set Eq=gr
call TriggerEvaluate(vp[Sp[gr]])
return Aq
endfunction
function bs takes integer gr returns boolean
set Eq=gr
call TriggerEvaluate(wp[Sp[gr]])
return Gq
endfunction
function Cs takes integer gr returns boolean
set Eq=gr
call TriggerEvaluate(xp[Sp[gr]])
return Gq
endfunction
function cs takes integer gr returns boolean
set Eq=gr
call TriggerEvaluate(yp[Sp[gr]])
return Gq
endfunction
function Ds takes integer gr returns boolean
set Eq=gr
call TriggerEvaluate(zp[Sp[gr]])
return Gq
endfunction
function Es takes integer gr returns boolean
set Eq=gr
call TriggerEvaluate(Ap[Sp[gr]])
return Gq
endfunction
function Fs takes integer gr,real Gs returns real
set Eq=gr
set cq=Gs
call TriggerEvaluate(ap[Sp[gr]])
return Fq
endfunction
function Hs takes integer gr,real Gs returns real
set Eq=gr
set cq=Gs
call TriggerEvaluate(Bp[Sp[gr]])
return Fq
endfunction
function Is takes integer gr,real Gs returns real
set Eq=gr
set cq=Gs
call TriggerEvaluate(bp[Sp[gr]])
return Fq
endfunction
function ls takes integer gr,boolean Js returns nothing
set Eq=gr
set Dq=Js
call TriggerExecute(pq)
endfunction
function Ks takes nothing returns integer
local integer gr=Mg
if(gr!=0)then
set Mg=Og[gr]
else
set Ng=Ng+1
set gr=Ng
endif
if(gr>8190)then
return 0
endif
set Yg[gr]=0
set Zg[gr]=false
set dh[gr]=true
set Sp[gr]=43
set Og[gr]=-1
return gr
endfunction
function Ls takes integer gr returns nothing
if gr==null then
return
elseif(Og[gr]!=-1)then
return
endif
set Eq=gr
call TriggerEvaluate(Tp[Sp[gr]])
set Og[gr]=Mg
set Mg=gr
endfunction
function Ms takes integer gr returns boolean
set Eq=gr
call TriggerEvaluate(Cp[Up[gr]])
return Gq
endfunction
function Ns takes nothing returns integer
local integer gr=zh
if(gr!=0)then
set zh=ah[gr]
else
set Ah=Ah+1
set gr=Ah
endif
if(gr>8190)then
return 0
endif
set Eh[gr]=0
set Fh[gr]=false
set Up[gr]=44
set ah[gr]=-1
return gr
endfunction
function Os takes integer gr returns nothing
if gr==null then
return
elseif(ah[gr]!=-1)then
return
endif
set Eq=gr
call TriggerEvaluate(Vp[Up[gr]])
set ah[gr]=zh
set zh=gr
endfunction
function Ps takes nothing returns integer
local integer gr=Ns()
local integer Mr
if(gr==0)then
return 0
endif
set Up[gr]=54
set Mr=gr
set gi[gr]=false
return gr
endfunction
function Qs takes nothing returns integer
local integer gr=Ns()
local integer Mr
if(gr==0)then
return 0
endif
set Up[gr]=53
set Mr=gr
return gr
endfunction
function Rs takes nothing returns integer
local integer gr=Ns()
local integer Mr
if(gr==0)then
return 0
endif
set Up[gr]=52
set Mr=gr
return gr
endfunction
function Ss takes nothing returns integer
local integer gr=Ns()
local integer Mr
if(gr==0)then
return 0
endif
set Up[gr]=51
set Mr=gr
set fi[gr]=false
return gr
endfunction
function Ts takes nothing returns integer
local integer gr=Ns()
local integer Mr
if(gr==0)then
return 0
endif
set Up[gr]=50
set Mr=gr
return gr
endfunction
function Us takes nothing returns integer
local integer gr=Ns()
local integer Mr
if(gr==0)then
return 0
endif
set Up[gr]=49
set Mr=gr
return gr
endfunction
function Vs takes nothing returns integer
local integer gr=Ns()
local integer Mr
if(gr==0)then
return 0
endif
set Up[gr]=48
set Mr=gr
return gr
endfunction
function Ws takes nothing returns integer
local integer gr=Xh
if(gr!=0)then
set Xh=Zh[gr]
else
set Yh=Yh+1
set gr=Yh
endif
if(gr>8190)then
return 0
endif
set Zh[gr]=-1
return gr
endfunction
function Xs takes integer gr returns nothing
if gr==null then
return
elseif(Zh[gr]!=-1)then
return
endif
set Zh[gr]=Xh
set Xh=gr
endfunction
function Ys takes nothing returns integer
local integer gr=Ph
if(gr!=0)then
set Ph=Rh[gr]
else
set Qh=Qh+1
set gr=Qh
endif
if(gr>8190)then
return 0
endif
set Vh[gr]=0
set Wh[gr]=0
set Rh[gr]=-1
return gr
endfunction
function Zs takes integer gr returns nothing
if gr==null then
return
elseif(Rh[gr]!=-1)then
return
endif
set Rh[gr]=Ph
set Ph=gr
endfunction
function dt takes nothing returns integer
local integer gr=lh
if(gr!=0)then
set lh=Kh[gr]
else
set Jh=Jh+1
set gr=Jh
endif
if(gr>8190)then
return 0
endif
set Kh[gr]=-1
return gr
endfunction
function et takes nothing returns integer
local integer gr=Zr()
local integer Mr
if(gr==0)then
return 0
endif
set Yp[gr]=105
set Mr=gr
return gr
endfunction
function ft takes nothing returns integer
local integer gr=Ns()
local integer Mr
if(gr==0)then
return 0
endif
set Up[gr]=55
set Mr=gr
return gr
endfunction
function gt takes nothing returns integer
local integer gr=Ns()
local integer Mr
if(gr==0)then
return 0
endif
set Up[gr]=56
set Mr=gr
return gr
endfunction
function ht takes nothing returns integer
local integer gr=Ns()
local integer Mr
if(gr==0)then
return 0
endif
set Up[gr]=57
set Mr=gr
return gr
endfunction
function it takes nothing returns integer
local integer gr=us()
local integer Mr
if(gr==0)then
return 0
endif
set Wp[gr]=62
set Mr=gr
return gr
endfunction
function jt takes nothing returns integer
local integer gr=us()
local integer Mr
if(gr==0)then
return 0
endif
set Wp[gr]=63
set Mr=gr
set Mi[gr]=false
set Ni[gr]=0
set Oi[gr]=.0
return gr
endfunction
function kt takes nothing returns integer
local integer gr=us()
local integer Mr
if(gr==0)then
return 0
endif
set Wp[gr]=64
set Mr=gr
return gr
endfunction
function mt takes nothing returns integer
local integer gr=us()
local integer Mr
if(gr==0)then
return 0
endif
set Wp[gr]=65
set Mr=gr
return gr
endfunction
function nt takes nothing returns integer
local integer gr=us()
local integer Mr
if(gr==0)then
return 0
endif
set Wp[gr]=66
set Mr=gr
set Zi[gr]=false
set dj[gr]=.0
set ej[gr]=0
set fj[gr]=0
set gj[gr]=0
return gr
endfunction
function ot takes nothing returns integer
local integer gr=us()
local integer Mr
if(gr==0)then
return 0
endif
set Wp[gr]=67
set Mr=gr
return gr
endfunction
function pt takes nothing returns integer
local integer gr=us()
local integer Mr
if(gr==0)then
return 0
endif
set Wp[gr]=68
set Mr=gr
return gr
endfunction
function qt takes nothing returns integer
local integer gr=us()
local integer Mr
if(gr==0)then
return 0
endif
set Wp[gr]=69
set Mr=gr
return gr
endfunction
function rt takes nothing returns integer
local integer gr=Ns()
local integer Mr
if(gr==0)then
return 0
endif
set Up[gr]=71
set Mr=gr
return gr
endfunction
function tt takes nothing returns integer
local integer gr=Ns()
local integer Mr
if(gr==0)then
return 0
endif
set Up[gr]=72
set Mr=gr
return gr
endfunction
function ut takes nothing returns integer
local integer gr=Ns()
local integer Mr
if(gr==0)then
return 0
endif
set Up[gr]=73
set Mr=gr
return gr
endfunction
function vt takes nothing returns integer
local integer gr=Ns()
local integer Mr
if(gr==0)then
return 0
endif
set Up[gr]=74
set Mr=gr
return gr
endfunction
function wt takes nothing returns integer
local integer gr=Ns()
local integer Mr
if(gr==0)then
return 0
endif
set Up[gr]=75
set Mr=gr
return gr
endfunction
function xt takes nothing returns integer
local integer gr=Ns()
local integer Mr
if(gr==0)then
return 0
endif
set Up[gr]=76
set Mr=gr
return gr
endfunction
function yt takes nothing returns integer
local integer gr=Ns()
local integer Mr
if(gr==0)then
return 0
endif
set Up[gr]=77
set Mr=gr
return gr
endfunction
function zt takes nothing returns integer
local integer gr=Ks()
local integer Mr
if(gr==0)then
return 0
endif
set Sp[gr]=78
set Mr=gr
return gr
endfunction
function At takes nothing returns integer
local integer gr=Ks()
local integer Mr
if(gr==0)then
return 0
endif
set Sp[gr]=79
set Mr=gr
return gr
endfunction
function at takes nothing returns integer
local integer gr=Ks()
local integer Mr
if(gr==0)then
return 0
endif
set Sp[gr]=80
set Mr=gr
return gr
endfunction
function Bt takes nothing returns integer
local integer gr=Ks()
local integer Mr
if(gr==0)then
return 0
endif
set Sp[gr]=81
set Mr=gr
return gr
endfunction
function bt takes nothing returns integer
local integer gr=Ks()
local integer Mr
if(gr==0)then
return 0
endif
set Sp[gr]=82
set Mr=gr
return gr
endfunction
function Ct takes nothing returns integer
local integer gr=Ks()
local integer Mr
if(gr==0)then
return 0
endif
set Sp[gr]=83
set Mr=gr
return gr
endfunction
function ct takes integer m,integer T returns integer
local integer gr=vr(m,T)
local integer Mr
if(gr==0)then
return 0
endif
set dq[gr]=86
set Mr=gr
set Fk[gr]=10.
set Gk[gr]=.0
set Hk[gr]=1.
set Ik[gr]=1.
return gr
endfunction
function Dt takes integer m,integer T returns integer
local integer gr=vr(m,T)
local integer Mr
if(gr==0)then
return 0
endif
set dq[gr]=87
set Mr=gr
set Mk[gr]=3.
return gr
endfunction
function Et takes integer m,integer T returns integer
local integer gr=vr(m,T)
local integer Mr
if(gr==0)then
return 0
endif
set dq[gr]=88
set Mr=gr
set Ok[gr]=0
return gr
endfunction
function Ft takes integer m,integer T returns integer
local integer gr=vr(m,T)
local integer Mr
if(gr==0)then
return 0
endif
set dq[gr]=89
set Mr=gr
set Rk[gr]=null
set Sk[gr]=false
set Tk[gr]=.0
return gr
endfunction
function Gt takes integer m,integer T returns integer
local integer gr=vr(m,T)
local integer Mr
if(gr==0)then
return 0
endif
set dq[gr]=90
set Mr=gr
set Xk[gr]=1
return gr
endfunction
function Ht takes integer m,integer T returns integer
local integer gr=vr(m,T)
local integer Mr
if(gr==0)then
return 0
endif
set dq[gr]=91
set Mr=gr
set im[gr]=2
set jm[gr]=.04
set km[gr]=384.
set nm[gr]=25
set om[gr]=.1
set sm[gr]=5
return gr
endfunction
function It takes integer m,integer T returns integer
local integer gr=vr(m,T)
local integer Mr
if(gr==0)then
return 0
endif
set dq[gr]=92
set Mr=gr
set bm[gr]=1.
set Cm[gr]=.0
set cm[gr]=.0
return gr
endfunction
function lt takes integer m,integer T returns integer
local integer gr=vr(m,T)
local integer Mr
if(gr==0)then
return 0
endif
set dq[gr]=93
set Mr=gr
set Hm[gr]=.0
return gr
endfunction
function Jt takes integer m,integer T returns integer
local integer gr=vr(m,T)
local integer Mr
if(gr==0)then
return 0
endif
set dq[gr]=94
set Mr=gr
set Om[gr]=0
set Pm[gr]=.1
set Qm[gr]=Mm
set Rm[gr]=false
set Um[gr]=0
return gr
endfunction
function Kt takes nothing returns integer
local integer gr=Zr()
local integer Mr
if(gr==0)then
return 0
endif
set Yp[gr]=96
set Mr=gr
return gr
endfunction
function Lt takes nothing returns integer
local integer gr=Zr()
local integer Mr
if(gr==0)then
return 0
endif
set Yp[gr]=97
set Mr=gr
return gr
endfunction
function Mt takes nothing returns integer
local integer gr=Zr()
local integer Mr
if(gr==0)then
return 0
endif
set Yp[gr]=98
set Mr=gr
return gr
endfunction
function Nt takes nothing returns integer
local integer gr=Zr()
local integer Mr
if(gr==0)then
return 0
endif
set Yp[gr]=99
set Mr=gr
return gr
endfunction
function Ot takes nothing returns integer
local integer gr=Zr()
local integer Mr
if(gr==0)then
return 0
endif
set Yp[gr]=100
set Mr=gr
return gr
endfunction
function Pt takes nothing returns integer
local integer gr=Zr()
local integer Mr
if(gr==0)then
return 0
endif
set Yp[gr]=101
set Mr=gr
return gr
endfunction
function Qt takes nothing returns integer
local integer gr=Zr()
local integer Mr
if(gr==0)then
return 0
endif
set Yp[gr]=102
set Mr=gr
return gr
endfunction
function Rt takes nothing returns integer
local integer gr=Zr()
local integer Mr
if(gr==0)then
return 0
endif
set Yp[gr]=103
set Mr=gr
return gr
endfunction
function St takes nothing returns integer
local integer gr=Zr()
local integer Mr
if(gr==0)then
return 0
endif
set Yp[gr]=104
set Mr=gr
return gr
endfunction
function Tt takes integer i,integer a1,integer a2,integer a3 returns nothing
set aq=a1
set Bq=a2
set bq=a3
call TriggerExecute(wq[i])
endfunction
function Ut takes string s returns nothing
endfunction
function Vt takes integer z1,integer z2 returns integer
if z1<z2 then
return z1
endif
return z2
endfunction
function Xt takes integer a returns boolean
return a/ 2*2!=a
endfunction
function Yt takes integer a,integer n returns boolean
return a/ n*n==a
endfunction
function Zt takes integer a,integer n returns boolean
return a/ n*n!=a
endfunction
function eu takes real r returns integer
local integer i=R2I(r)
if r-i==1. then
return i+1
endif
return i
endfunction
function fu takes real r returns integer
local integer i=R2I(r)
if r==i then
return i
endif
return i+1
endfunction
function gu takes boolean b returns integer
if b then
return 1
endif
return 0
endfunction
function hu takes nothing returns nothing
call PauseUnit(GetFilterUnit(),bj_pauseAllUnitsFlag)
endfunction
function iu takes string s,unit u,player p,real ju,real ku,real mu,real nu returns texttag
set bj_lastCreatedTextTag=CreateTextTag()
call SetTextTagText(bj_lastCreatedTextTag,s,ju*Q)
call SetTextTagPosUnit(bj_lastCreatedTextTag,u,.0)
call SetTextTagVelocity(bj_lastCreatedTextTag,.0,ku*R)
call SetTextTagVisibility(bj_lastCreatedTextTag,S==p)
call SetTextTagPermanent(bj_lastCreatedTextTag,false)
call SetTextTagLifespan(bj_lastCreatedTextTag,mu)
call SetTextTagFadepoint(bj_lastCreatedTextTag,nu)
return bj_lastCreatedTextTag
endfunction
function ou takes string pu,real ju,real x,real y,integer qu returns image
set bj_lastCreatedImage=CreateImage(pu,ju,ju,.0,x,y,.0,ju/ 2.,ju/ 2.,.0,qu)
call SetImageRenderAlways(bj_lastCreatedImage,true)
return bj_lastCreatedImage
endfunction
function ru takes player p,string pu,real ju,real x,real y,integer qu returns image
set bj_lastCreatedImage=CreateImage(pu,ju,ju,.0,x,y,.0,ju/ 2.,ju/ 2.,.0,qu)
call SetImageRenderAlways(bj_lastCreatedImage,S==p)
return bj_lastCreatedImage
endfunction
function su takes rect r returns real
return GetRandomReal(GetRectMinX(r),GetRectMaxX(r))
endfunction
function tu takes rect r returns real
return GetRandomReal(GetRectMinY(r),GetRectMaxY(r))
endfunction
function uu takes player p,string s,widget w,string vu returns effect
if S!=p then
set s=""
endif
return AddSpecialEffectTarget(s,w,vu)
endfunction
function EndThread takes nothing returns nothing
call I2R(1/ 0)
endfunction
function wu takes nothing returns nothing
set S=GetLocalPlayer()
endfunction
function GetCustomTimer takes nothing returns integer
return LoadInteger(n7,6,GetHandleId(GetExpiredTimer()))
endfunction
function xu takes timer t returns nothing
call PauseTimer(t)
call RemoveSavedInteger(n7,6,GetHandleId(t))
call DestroyTimer(t)
endfunction
function IsFilterUnitTower takes nothing returns boolean
return(IsUnitType(GetFilterUnit(),Z))!=null
endfunction
function yu takes nothing returns boolean
return(IsUnitType(GetFilterUnit(),e4))!=null
endfunction
function zu takes real Au,real x,real y,integer qu,integer au,integer Bu,integer bu,integer Cu returns image
call ou("Textures\\RangeIndicator.blp",Au+Au,x,y,qu)
call SetImageColor(bj_lastCreatedImage,au,Bu,bu,Cu)
return bj_lastCreatedImage
endfunction
function cu takes player p,real Au,real x,real y,integer qu,integer au,integer Bu,integer bu,integer Cu returns image
call ru(p,"Textures\\RangeIndicator.blp",Au+Au,x,y,qu)
call SetImageColor(bj_lastCreatedImage,au,Bu,bu,Cu)
return bj_lastCreatedImage
endfunction
function Du takes integer p returns nothing
set R7=p
call TriggerExecute(S7)
endfunction
function Eu takes integer Fu returns nothing
set U=false
set t7=Fu+1
call TriggerExecute(M8)
endfunction
function Gu takes string Hu,string Iu returns nothing
if U then
call PreloadGenClear()
call Preload("\" )        "+(Hu)+".                                //")
call PreloadGenEnd("WispTD\\Errors\\"+Iu+".txt")
call Eu(7)
call DisplayTimedTextToPlayer(S,.0,.0,((7.)*1.),("An error occurred. Game will end in "+Y9[P9]+"7|r seconds."))
endif
call I2R(1/ 0)
endfunction
function lu takes nothing returns nothing
set h4=Filter(function yu)
endfunction
function Ju takes integer Ku returns integer
set O8=O8+1
set N8[(O8)]=Ku
return O8
endfunction
function Lu takes integer Ku returns nothing
set Q8=Q8+1
set P8[(Q8)]=Ku
set lf[Ku]=Q8
endfunction
function Mu takes integer Ku returns nothing
local integer i=lf[Ku]
if Q8>i then
set P8[(i)]=P8[(Q8)]
set lf[P8[(i)]]=i
endif
set Q8=Q8-1
endfunction
function Nu takes integer Ku returns nothing
local integer i=Jf[Ku]
if S8>i then
set R8[(i)]=R8[(S8)]
set Jf[R8[(i)]]=i
endif
set S8=S8-1
endfunction
function Ou takes nothing returns nothing
set S8=-1
loop
set S8=S8+1
set R8[(S8)]=(P8[(S8)])
set Jf[R8[(S8)]]=S8
exitwhen S8==Q8
endloop
endfunction
function Pu takes integer p returns nothing
set Df[p]=(R8[(GetRandomInt(0,(S8+1)*100-1)/ 100)])
call Nu(Df[p])
endfunction
function Qu takes integer gr,integer Ku returns nothing
set Y8[gr]=Y8[gr]+1
set W8[X8[gr]+Y8[gr]]=Ku
set fk[Ku]=Y8[gr]
endfunction
function Ru takes integer gr,integer Ku returns nothing
local integer i=fk[Ku]
if Y8[gr]>i then
set W8[X8[gr]+i]=W8[X8[gr]+Y8[gr]]
set fk[W8[X8[gr]+i]]=i
endif
set Y8[gr]=Y8[gr]-1
endfunction
function Su takes integer gr,integer Ku returns nothing
set h9[gr]=h9[gr]+1
if h9[gr]==105 then
call Gu("Error: not enough cells in ForceArray "+I2S(gr)+". Current amount of cells is "+I2S(105),"ForceArrayError")
endif
set f9[g9[gr]+h9[gr]]=Ku
set vh[Ku]=h9[gr]
endfunction
function Tu takes integer gr,integer Ku returns nothing
local integer i=vh[Ku]
if h9[gr]>i then
set f9[g9[gr]+i]=f9[g9[gr]+h9[gr]]
set vh[f9[g9[gr]+i]]=i
endif
set h9[gr]=h9[gr]-1
endfunction
function Uu takes integer gr,integer Ku returns nothing
set o9[gr]=o9[gr]+1
if o9[gr]==105 then
call Gu("Error: not enough cells in SpawnArray "+I2S(gr)+". Current amount of cells is "+I2S(105),"SpawnArrayError")
endif
set m9[n9[gr]+o9[gr]]=Ku
set vh[Ku]=o9[gr]
endfunction
function Vu takes integer gr,integer Ku returns nothing
local integer i=vh[Ku]
if o9[gr]>i then
set m9[n9[gr]+i]=m9[n9[gr]+o9[gr]]
set vh[m9[n9[gr]+i]]=i
endif
set o9[gr]=o9[gr]-1
endfunction
function Wu takes integer gr,integer Ku returns nothing
set u9[gr]=u9[gr]+1
if u9[gr]==105 then
call Gu("Error: not enough cells in MinionArray "+I2S(gr)+". Current amount of cells is "+I2S(105),"MinionArrayError")
endif
set s9[t9[gr]+u9[gr]]=Ku
set vh[Ku]=u9[gr]
endfunction
function Xu takes integer gr,integer Ku returns nothing
local integer i=vh[Ku]
if u9[gr]>i then
set s9[t9[gr]+i]=s9[t9[gr]+u9[gr]]
set vh[s9[t9[gr]+i]]=i
endif
set u9[gr]=u9[gr]-1
endfunction
function Yu takes integer gr,integer Ku returns nothing
set A9[gr]=A9[gr]+1
if A9[gr]==105 then
call Gu("Error: not enough cells in DeadArray "+I2S(gr)+". Current amount of cells is "+I2S(105),"DeadArrayError")
endif
set y9[z9[gr]+A9[gr]]=Ku
set vh[Ku]=A9[gr]
endfunction
function Zu takes integer gr,integer Ku returns nothing
local integer i=vh[Ku]
if A9[gr]>i then
set y9[z9[gr]+i]=y9[z9[gr]+A9[gr]]
set vh[y9[z9[gr]+i]]=i
endif
set A9[gr]=A9[gr]-1
endfunction
function dv takes integer gr,integer Ku returns nothing
set D9[gr]=D9[gr]+1
if D9[gr]==105 then
call Gu("Error: not enough cells in SentMinionArray "+I2S(gr)+". Current amount of cells is "+I2S(105),"SentMinionArrayError")
endif
set C9[c9[gr]+D9[gr]]=Ku
set wh[Ku]=D9[gr]
endfunction
function ev takes integer gr,integer Ku returns nothing
local integer i=wh[Ku]
if D9[gr]>i then
set C9[c9[gr]+i]=C9[c9[gr]+D9[gr]]
set wh[C9[c9[gr]+i]]=i
endif
set D9[gr]=D9[gr]-1
endfunction
function fv takes playercolor pc returns integer
local integer i=0
loop
exitwhen ConvertPlayerColor(i)==pc
set i=i+1
endloop
return i
endfunction
function gv takes nothing returns integer
set l9=l9+1
return l9
endfunction
function hv takes nothing returns nothing
local integer i=gv()
set V9[i]=$FF
set W9[i]=3
set X9[i]=3
set Y9[i]="|cffff0303"
set i=gv()
set V9[i]=0
set W9[i]=66
set X9[i]=$FF
set Y9[i]="|cff0042ff"
set i=gv()
set V9[i]=28
set W9[i]=$E6
set X9[i]=$B9
set Y9[i]="|cff1ce6b9"
set i=gv()
set V9[i]=84
set W9[i]=0
set X9[i]=$81
set Y9[i]="|cff540081"
set i=gv()
set V9[i]=$FF
set W9[i]=$FC
set X9[i]=0
set Y9[i]="|cfffffc00"
set i=gv()
set V9[i]=$FE
set W9[i]=$8A
set X9[i]=$E
set Y9[i]="|cfffe8a0e"
set i=gv()
set V9[i]=32
set W9[i]=$C0
set X9[i]=0
set Y9[i]="|cff20c000"
set i=gv()
set V9[i]=$E5
set W9[i]=91
set X9[i]=$B0
set Y9[i]="|cffe55bb0"
set i=gv()
set V9[i]=$95
set W9[i]=$96
set X9[i]=$97
set Y9[i]="|cff959697"
set i=gv()
set V9[i]=$7E
set W9[i]=$BF
set X9[i]=$F1
set Y9[i]="|cff7ebff1"
set i=gv()
set V9[i]=16
set W9[i]=98
set X9[i]=70
set Y9[i]="|cff106246"
set i=gv()
set V9[i]=78
set W9[i]=42
set X9[i]=4
set Y9[i]="|cff4e2a04"
set i=gv()
set V9[i]=$9B
set W9[i]=0
set X9[i]=0
set Y9[i]="|cff9b0000"
set i=gv()
set V9[i]=0
set W9[i]=0
set X9[i]=$C3
set Y9[i]="|cff0000c3"
set i=gv()
set V9[i]=0
set W9[i]=$EA
set X9[i]=$FF
set Y9[i]="|cff00eaff"
set i=gv()
set V9[i]=$BE
set W9[i]=0
set X9[i]=$FE
set Y9[i]="|cffbe00fe"
set i=gv()
set V9[i]=$EB
set W9[i]=$CD
set X9[i]=$87
set Y9[i]="|cffebcd87"
set i=gv()
set V9[i]=$F8
set W9[i]=$A4
set X9[i]=$8B
set Y9[i]="|cfff8a48b"
set i=gv()
set V9[i]=$BF
set W9[i]=$FF
set X9[i]=$80
set Y9[i]="|cffbfff80"
set i=gv()
set V9[i]=$DC
set W9[i]=$B9
set X9[i]=$EB
set Y9[i]="|cffdcb9eb"
set i=gv()
set V9[i]=40
set W9[i]=40
set X9[i]=40
set Y9[i]="|cff282828"
set i=gv()
set V9[i]=$EB
set W9[i]=$F0
set X9[i]=$FF
set Y9[i]="|cffebf0ff"
set i=gv()
set V9[i]=0
set W9[i]=120
set X9[i]=30
set Y9[i]="|cff00781e"
set i=gv()
set V9[i]=$A4
set W9[i]=111
set X9[i]=51
set Y9[i]="|cffa46f33"
endfunction
function iv takes nothing returns nothing
call hv()
set J9=gv()
set V9[J9]=$E2
set W9[J9]=$B0
set X9[J9]=7
set Y9[J9]="|cffe2b007"
set K9=gv()
set V9[K9]=$80
set W9[K9]=$CC
set X9[K9]=$FF
set Y9[K9]="|cff80ccff"
set L9=gv()
set V9[L9]=$D1
set W9[L9]=91
set X9[L9]=$8F
set Y9[L9]="|cffd15b8f"
set M9=gv()
set V9[M9]=118
set W9[M9]=$A5
set X9[M9]=$AF
set Y9[M9]="|cff76a5af"
set N9=gv()
set V9[N9]=0
set W9[N9]=$BB
set X9[N9]=46
set Y9[N9]="|cff00bb2e"
set O9=gv()
set V9[O9]=$CC
set W9[O9]=33
set X9[O9]=33
set Y9[O9]="|cffcc3333"
set P9=gv()
set V9[P9]=0
set W9[P9]=$CC
set X9[P9]=$FF
set Y9[P9]="|cff00ccff"
set Q9=gv()
set V9[Q9]=$FF
set W9[Q9]=$CC
set X9[Q9]=$80
set Y9[Q9]="|cffffcc80"
set R9=gv()
set V9[R9]=51
set W9[R9]=$CC
set X9[R9]=51
set Y9[R9]="|cff33cc33"
set U9=gv()
set V9[U9]=$80
set W9[U9]=$80
set X9[U9]=$80
set Y9[U9]="|cff808080"
set S9=O9
set T9=O9
endfunction
function kv takes unit u,integer id returns item
call IncUnitAbilityLevel(u,1093677104)
set bj_lastCreatedItem=UnitAddItemById(u,id)
call SetItemUserData(bj_lastCreatedItem,(GetUnitAbilityLevel((u),1093677104)-2))
return bj_lastCreatedItem
endfunction
function ov takes unit u,item i returns nothing
local integer nv=GetItemUserData(i)
local integer pv=(GetUnitAbilityLevel((u),1093677104)-2)
local integer qv
call RemoveItem(i)
loop
exitwhen nv==pv
set qv=nv
set nv=nv+1
set i=UnitItemInSlot(u,nv)
call SetItemDroppable(i,true)
call UnitDropItemSlot(u,i,qv)
call SetItemUserData(i,qv)
call SetItemDroppable(i,false)
endloop
call DecUnitAbilityLevel(u,1093677104)
endfunction
function rv takes integer T,integer sv,integer tv,integer uv,integer vv,integer wv,integer xv,integer yv,integer zv,string Av returns nothing
set j4[T]=Ar()
set nd[od[j4[T]]]=sv
set nd[od[j4[T]]+1]=tv
set nd[od[j4[T]]+2]=uv
set nd[od[j4[T]]+3]=vv
set pd[j4[T]]=k7[wv]
set qd[j4[T]]=xv
set rd[j4[T]]=yv
set sd[j4[T]]=zv
set td[j4[T]]=Av
endfunction
function Bv takes integer p returns nothing
local integer a=0
local integer b
local integer bv
local integer Cv
local integer cv=Gg[p]
local integer Dv=Hg[p]
loop
set bv=nd[od[cv]+a]
set Cv=nd[od[Dv]+a]
set b=0
loop
call UnitRemoveAbility(hg[p],to[uo[i7[bv]]+b])
call UnitRemoveAbility(hg[p],vo[wo[i7[bv]]+b])
call UnitAddAbility(hg[p],to[uo[i7[Cv]]+b])
call UnitAddAbility(hg[p],vo[wo[i7[Cv]]+b])
set b=b+1
exitwhen b==3
endloop
set a=a+1
exitwhen a==4
endloop
set Gg[p]=Dv
set Hg[p]=cv
endfunction
function Ev takes integer p returns nothing
local integer a=0
local integer cv=Eg[p]
local integer Dv=Fg[p]
loop
call UnitRemoveAbility(gg[p],Mn[g7[nd[od[cv]+a]]])
call UnitAddAbility(gg[p],Mn[g7[nd[od[Dv]+a]]])
set a=a+1
exitwhen a==4
endloop
call UnitRemoveAbility(gg[p],qd[cv])
call UnitAddAbility(gg[p],qd[Dv])
set Eg[p]=Dv
set Fg[p]=cv
endfunction
function Fv takes integer f,integer i returns integer
local integer gr=ar()
set Ad[gr]=f
set ad[gr]=i
set zd[gr]=s4-i/ r4
set xd[gr]=zd[gr]
if i<t4 then
set bd[gr]=true
else
set bd[gr]=false
set yd[gr]=zd[gr]
endif
return gr
endfunction
function Gv takes integer gr returns nothing
if ad[gr]<r4 then
set Cd[gr]=0
else
set Cd[gr]=Qd[Rd[Ad[gr]]+ad[gr]-r4]
endif
if ad[gr]+r4>608 then
set Fd[gr]=0
else
set Fd[gr]=Qd[Rd[Ad[gr]]+ad[gr]+r4]
endif
if Yt(ad[gr]+1,r4)then
set Ed[gr]=0
else
set Ed[gr]=Qd[Rd[Ad[gr]]+ad[gr]+1]
endif
if Yt(ad[gr],r4)then
set Dd[gr]=0
else
set Dd[gr]=Qd[Rd[Ad[gr]]+ad[gr]-1]
endif
endfunction
function Hv takes integer gr returns boolean
return gr!=0 and bd[gr]and Bd[gr]
endfunction
function Iv takes integer gr returns integer
local integer lv=Kd[gr]
set Kd[gr]=Gd[lv]
return lv
endfunction
function Jv takes integer gr,integer c returns nothing
if(Kd[(gr)]==0)then
set Kd[gr]=c
set Ld[gr]=c
set Gd[c]=0
else
set Gd[Ld[gr]]=c
set Hd[c]=Ld[gr]
set Gd[c]=0
set Ld[gr]=c
endif
endfunction
function Kv takes integer gr,integer c,integer Lv returns nothing
if Hv(c)then
call Jv(gr,c)
set bd[(c)]=false
set yd[(c)]=(Lv)
endif
endfunction
function Mv takes integer f returns integer
local integer gr=br()
local integer a=u4
loop
set a=a-1
call Jv(gr,Qd[Rd[f]+a])
exitwhen a<t4
endloop
return gr
endfunction
function Nv takes integer p returns integer
local integer gr=Hr()
local real Ov=o4+bf[p]*(1024.+512.)
local real Pv=Ov+1024.
set Pd[gr]=p
set Sd[gr]=Pv
set Td[gr]=Ov
set Ud[gr]=Pv-64.
set Vd[gr]=Ov+64.
set Wd[gr]=Rect(Ov,1408.,Pv,1920.)
set Xd[gr]=Rect(Ov,640.,Pv,1152.)
set Zd[gr]=Rect(Ov,n4,Pv,m4)
set Yd[gr]=Rect(Vd[gr],k4,Ud[gr],320.)
set de[gr]=Rect(Ov,n4,Pv,1152.)
call RegionAddRect(p7,Xd[gr])
call RegionAddRect(o7,Zd[gr])
call Dr(gr)
return gr
endfunction
function Qv takes integer gr returns nothing
local integer a=-1
local integer p
loop
set a=a+1
set p=(N8[(a)])
if Ef[p]then
set ee[fe[gr]+a]=CreateFogModifierRect(Bf[Pd[gr]],FOG_OF_WAR_VISIBLE,de[If[p]],true,true)
set ge[he[gr]+a]=CreateFogModifierRect(Bf[Pd[gr]],FOG_OF_WAR_VISIBLE,Wd[If[p]],true,true)
call FogModifierStart(ee[fe[gr]+a])
call FogModifierStart(ge[he[gr]+a])
endif
exitwhen a==O8
endloop
endfunction
function Rv takes integer gr returns nothing
local integer a=-1
local integer dr=bf[Pd[gr]]
local integer f
local integer p
loop
set a=a+1
set p=(N8[(a)])
if Hf[p]then
set f=If[p]
call DestroyFogModifier(ee[fe[f]+dr])
set ee[fe[f]+dr]=null
call DestroyFogModifier(ge[he[f]+dr])
set ge[he[f]+dr]=null
call SetFogStateRect(Bf[p],FOG_OF_WAR_MASKED,de[gr],true)
call SetFogStateRect(Bf[p],FOG_OF_WAR_MASKED,Wd[gr],true)
endif
exitwhen a==O8
endloop
call RegionClearRect(p7,Xd[gr])
call RegionClearRect(o7,Zd[gr])
call RemoveRect(de[gr])
set de[gr]=null
call RemoveRect(Wd[gr])
set Wd[gr]=null
call RemoveRect(Xd[gr])
set Xd[gr]=null
call RemoveRect(Zd[gr])
set Zd[gr]=null
call RemoveRect(Yd[gr])
set Yd[gr]=null
call Er(gr)
endfunction
function Sv takes integer gr,real x,real y returns integer
local integer ix
local integer iy
set ix=R2I(x-Td[gr])/ 64
if ix==r4 then
set ix=r4-1
endif
set iy=R2I(640.-y)/ 64
if iy==s4 then
set iy=s4-1
endif
return Qd[Rd[gr]+iy*r4+ix]
endfunction
function Tv takes integer gr returns nothing
local integer q=Mv(gr)
call Gr(gr,q)
if je[gr]then
call Gu("Error: updating ground distaces in field "+I2S(gr)+" was not finished","BFSError")
endif
call Cr(q)
call Fr(gr)
endfunction
function Uv takes integer gr returns nothing
set qe[gr]=CreateTimer()
call SaveInteger(n7,6,GetHandleId((qe[gr])),(gr))
endfunction
function Vv takes integer gr returns nothing
call xu(qe[gr])
set qe[gr]=null
endfunction
function Wv takes nothing returns nothing
local integer gr=(LoadInteger(n7,6,GetHandleId(GetExpiredTimer())))
set pe[gr]=pe[gr]-8
if pe[gr]>0 then
call SetUnitVertexColor(oe[gr],$FF,$FF,$FF,pe[gr])
else
call RemoveUnit(oe[gr])
set oe[gr]=null
call Vv(gr)
call lr(gr)
call Ut("MinionDeleter "+I2S(gr)+" is ended.")
endif
endfunction
function Xv takes unit m returns nothing
local integer gr=Ir()
set oe[gr]=m
call Uv(gr)
call SetUnitInvulnerable(oe[gr],true)
call PauseUnit(oe[gr],true)
call SetUnitVertexColor(oe[gr],$FF,$FF,$FF,pe[gr])
call TimerStart(qe[gr],.05,true,function Wv)
call Ut("MinionDeleter "+I2S(gr)+" is started.")
endfunction
function Yv takes integer gr,integer f,integer Zv returns nothing
set re[gr]=f
set se[gr]=Zv
endfunction
function dw takes integer gr,integer ew,integer fw returns nothing
if gr==0 then
call Gu("Error: MBSNode struct has run out of indexes. Current amount of indexes is 8190. Error detected on field of player "+I2S(ch[ew]),"MBSNodeError")
endif
set we[gr]=ew
set xe[gr]=fw
endfunction
function gw takes integer gr returns nothing
if ye[gr]!=0 then
set ze[ye[gr]]=ze[gr]
endif
if ze[gr]!=0 then
set ye[ze[gr]]=ye[gr]
endif
endfunction
function hw takes integer gr,integer Ku returns integer
local integer iw=Jr()
if Ge[gr]==0 then
set Fe[gr]=iw
else
set ze[iw]=Fe[gr]
set ye[Fe[gr]]=iw
set Fe[gr]=iw
endif
set Ee[gr]=Ee[gr]+1
set Ge[gr]=Ge[gr]+1
call dw(iw,Ku,Ee[gr])
return iw
endfunction
function jw takes integer gr,integer iw returns nothing
set Ge[gr]=Ge[gr]-1
if Fe[gr]==iw then
set Fe[gr]=ze[iw]
endif
call gw(iw)
call Kr(iw)
endfunction
function kw takes integer gr,integer Ku returns integer
local integer iw=Lr()
if Ie[gr]==0 then
set He[gr]=iw
else
set ze[iw]=He[gr]
set ye[He[gr]]=iw
set He[gr]=iw
endif
set Ee[gr]=Ee[gr]+1
set Ie[gr]=Ie[gr]+1
call dw(iw,Ku,Ee[gr])
return iw
endfunction
function mw takes integer gr,integer iw returns nothing
set Ie[gr]=Ie[gr]-1
if He[gr]==iw then
set He[gr]=ze[iw]
endif
set le[gr]=le[gr]-Ae[iw]
call gw(iw)
call Kr(iw)
endfunction
function nw takes integer gr,integer iw,real ow returns nothing
set le[gr]=le[gr]-Ae[iw]+ow
set Ae[iw]=ow
endfunction
function pw takes integer gr,real Gs returns real
local integer iw=He[gr]
local integer qw
loop
exitwhen iw==0
set qw=ze[iw]
set Gs=Gs-Ae[iw]
if Gs>.0 then
call Os(we[iw])
else
call nw(gr,iw,-Gs)
return .0
endif
set iw=qw
endloop
return Gs
endfunction
function rw takes integer gr,integer Ku returns integer
local integer iw=Lr()
if Le[gr]==0 then
set Je[gr]=iw
set Ke[gr]=iw
else
set ze[iw]=Je[gr]
set ye[Je[gr]]=iw
set Je[gr]=iw
endif
set Ee[gr]=Ee[gr]+1
set Le[gr]=Le[gr]+1
call dw(iw,Ku,Ee[gr])
return iw
endfunction
function sw takes integer gr returns nothing
if Le[gr]==0 then
set Me[gr]=.0
elseif Ae[Ke[gr]]<=.0 then
set Me[gr]=Ae[Je[gr]]
elseif Ae[Je[gr]]>=.0 then
set Me[gr]=Ae[Ke[gr]]
else
set Me[gr]=Ae[Je[gr]]+Ae[Ke[gr]]
endif
endfunction
function tw takes integer gr,integer iw returns nothing
set Le[gr]=Le[gr]-1
if Je[gr]==iw then
set Je[gr]=ze[iw]
endif
if Ke[gr]==iw then
set Ke[gr]=ye[iw]
endif
call sw(gr)
call gw(iw)
call Kr(iw)
endfunction
function uw takes integer gr,integer iw returns nothing
local integer n=ze[iw]
local integer p=ye[iw]
if n!=0 and Ae[iw]>Ae[n]then
if p!=0 then
set ze[p]=n
endif
set ye[n]=p
set p=n
set n=ze[n]
set ze[p]=iw
set ze[iw]=n
set ye[iw]=p
if p==Ke[gr]then
set Ke[gr]=iw
endif
if iw==Je[gr]then
set Je[gr]=p
endif
loop
exitwhen n==0
set ye[n]=iw
exitwhen Ae[iw]<=Ae[n]
set ze[p]=n
set ye[n]=p
set p=n
set n=ze[n]
set ze[p]=iw
set ze[iw]=n
set ye[iw]=p
if p==Ke[gr]then
set Ke[gr]=iw
endif
endloop
endif
if p!=0 and Ae[iw]<Ae[p]then
if n!=0 then
set ye[n]=p
endif
set ze[p]=n
set n=p
set p=ye[p]
set ye[n]=iw
set ye[iw]=p
set ze[iw]=n
if n==Je[gr]then
set Je[gr]=iw
endif
if iw==Ke[gr]then
set Ke[gr]=n
endif
loop
exitwhen p==0
set ze[p]=iw
exitwhen Ae[iw]>=Ae[p]
set ye[n]=p
set ze[p]=n
set n=p
set p=ye[p]
set ye[n]=iw
set ye[iw]=p
set ze[iw]=n
if n==Je[gr]then
set Je[gr]=iw
endif
endloop
endif
endfunction
function vw takes integer gr,integer iw,real ow returns nothing
set Ae[iw]=ow
call uw(gr,iw)
call sw(gr)
endfunction
function xw takes integer gr,integer iw returns nothing
set Oe[gr]=Oe[gr]-1
if Ne[gr]==iw then
set Ne[gr]=ze[iw]
endif
set Pe[gr]=Pe[gr]-ae[iw]
set Qe[gr]=Pe[gr]>0
call gw(iw)
call Kr(iw)
endfunction
function zw takes integer gr,integer Ku returns integer
local integer iw=Nr()
if Se[gr]==0 then
set Re[gr]=iw
else
set ze[iw]=Re[gr]
set ye[Re[gr]]=iw
set Re[gr]=iw
endif
set Ee[gr]=Ee[gr]+1
set Se[gr]=Se[gr]+1
call dw(iw,Ku,Ee[gr])
return iw
endfunction
function Aw takes integer gr,integer iw returns nothing
set Se[gr]=Se[gr]-1
if Re[gr]==iw then
set Re[gr]=ze[iw]
endif
set Te[gr]=Te[gr]-ae[iw]
set Ue[gr]=Te[gr]>0
call gw(iw)
call Kr(iw)
endfunction
function aw takes integer gr,integer iw,boolean ow returns nothing
local integer v=gu(ow)
set Te[gr]=Te[gr]-ae[iw]+v
set Ue[gr]=Te[gr]>0
set ae[iw]=v
endfunction
function cw takes integer gr,integer Ku returns integer
local integer iw=Nr()
if Ze[gr]==0 then
set Ye[gr]=iw
else
set ze[iw]=Ye[gr]
set ye[Ye[gr]]=iw
set Ye[gr]=iw
endif
set Ee[gr]=Ee[gr]+1
set Ze[gr]=Ze[gr]+1
call dw(iw,Ku,Ee[gr])
return iw
endfunction
function Dw takes integer gr,integer iw returns nothing
set Ze[gr]=Ze[gr]-1
if Ye[gr]==iw then
set Ye[gr]=ze[iw]
endif
set df[gr]=df[gr]-ae[iw]
call gw(iw)
call Kr(iw)
endfunction
function Ew takes integer gr,integer iw,integer ow returns nothing
set df[gr]=df[gr]-ae[iw]+ow
set ae[iw]=ow
endfunction
function Fw takes integer gr,integer Ku returns integer
local integer iw=Nr()
if ff[gr]==0 then
set ef[gr]=iw
else
set ze[iw]=ef[gr]
set ye[ef[gr]]=iw
set ef[gr]=iw
endif
set Ee[gr]=Ee[gr]+1
set ff[gr]=ff[gr]+1
call dw(iw,Ku,Ee[gr])
return iw
endfunction
function Gw takes integer gr,integer iw returns nothing
set ff[gr]=ff[gr]-1
if ef[gr]==iw then
set ef[gr]=ze[iw]
endif
set gf[gr]=gf[gr]-ae[iw]
call gw(iw)
call Kr(iw)
endfunction
function Hw takes integer gr,integer iw,integer ow returns nothing
set gf[gr]=gf[gr]-ae[iw]+ow
set ae[iw]=ow
endfunction
function Iw takes integer gr,integer Ku returns integer
local integer iw=es()
if jf[gr]==0 then
set hf[gr]=iw
else
set ze[iw]=hf[gr]
set ye[hf[gr]]=iw
set hf[gr]=iw
endif
set Ee[gr]=Ee[gr]+1
set jf[gr]=jf[gr]+1
call dw(iw,Ku,Ee[gr])
return iw
endfunction
function lw takes integer gr,integer iw returns nothing
set jf[gr]=jf[gr]-1
if hf[gr]==iw then
set hf[gr]=ze[iw]
endif
set kf[gr]=kf[gr]-Be[iw]
set mf[gr]=kf[gr]>0
call gw(iw)
call Kr(iw)
endfunction
function Jw takes integer gr,integer iw,boolean ow returns nothing
local integer v=gu(ow)
set kf[gr]=kf[gr]-Be[iw]+v
set mf[gr]=kf[gr]>0
set Be[iw]=v
endfunction
function Nw takes integer gr,integer Ku returns integer
local integer iw=es()
if sf[gr]==0 then
set rf[gr]=iw
else
set ze[iw]=rf[gr]
set ye[rf[gr]]=iw
set rf[gr]=iw
endif
set Ee[gr]=Ee[gr]+1
set sf[gr]=sf[gr]+1
call dw(iw,Ku,Ee[gr])
return iw
endfunction
function Ow takes integer gr,integer iw returns nothing
set sf[gr]=sf[gr]-1
if rf[gr]==iw then
set rf[gr]=ze[iw]
endif
set tf[gr]=tf[gr]-Be[iw]
set uf[gr]=tf[gr]>0
call gw(iw)
call Kr(iw)
endfunction
function Pw takes integer gr,integer iw,boolean ow returns nothing
local integer v=gu(ow)
set tf[gr]=tf[gr]-Be[iw]+v
set uf[gr]=tf[gr]>0
set Be[iw]=v
endfunction
function Qw takes integer gr,integer Ku returns integer
local integer iw=Nr()
if wf[gr]==0 then
set vf[gr]=iw
else
set ze[iw]=vf[gr]
set ye[vf[gr]]=iw
set vf[gr]=iw
endif
set Ee[gr]=Ee[gr]+1
set wf[gr]=wf[gr]+1
call dw(iw,Ku,Ee[gr])
return iw
endfunction
function Rw takes integer gr,integer iw returns nothing
set wf[gr]=wf[gr]-1
if vf[gr]==iw then
set vf[gr]=ze[iw]
endif
set xf[gr]=xf[gr]-ae[iw]
set yf[gr]=xf[gr]>0
call gw(iw)
call Kr(iw)
endfunction
function Sw takes integer gr,integer iw,boolean ow returns nothing
local integer v=gu(ow)
set xf[gr]=xf[gr]-ae[iw]+v
set yf[gr]=xf[gr]>0
set ae[iw]=v
endfunction
function Tw takes integer gr,real Gs returns nothing
if jf[gr]>0 and(sf[gr]==0 or xe[hf[gr]]>xe[rf[gr]])then
set be[hf[gr]]=be[hf[gr]]+Gs
else
set be[rf[gr]]=be[rf[gr]]+Gs
endif
endfunction
function Uw takes integer gr,real Gs returns nothing
if of[gr]>0 and(sf[gr]==0 or xe[nf[gr]]>xe[rf[gr]])then
set be[nf[gr]]=be[nf[gr]]+Gs
else
set be[rf[gr]]=be[rf[gr]]+Gs
endif
endfunction
function Vw takes integer gr,real Gs returns nothing
set be[rf[gr]]=be[rf[gr]]+Gs
endfunction
function Ww takes integer gr returns nothing
local integer iw=Je[gr]
local integer qw
loop
exitwhen iw==0
set qw=ze[iw]
if ki[Dh[we[iw]]]then
call Os(we[iw])
endif
set iw=qw
endloop
set iw=Ne[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
endfunction
function Xw takes integer gr returns nothing
local integer iw=Fe[gr]
local integer qw
loop
exitwhen iw==0
set qw=ze[iw]
if ki[Dh[we[iw]]]then
call Os(we[iw])
endif
set iw=qw
endloop
set iw=Je[gr]
loop
exitwhen iw==0
set qw=ze[iw]
if ki[Dh[we[iw]]]then
call Os(we[iw])
endif
set iw=qw
endloop
set iw=Ne[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=Ve[gr]
loop
exitwhen iw==0
set qw=ze[iw]
if ki[Dh[we[iw]]]then
call Os(we[iw])
endif
set iw=qw
endloop
set iw=Ye[gr]
loop
exitwhen iw==0
set qw=ze[iw]
if ki[Dh[we[iw]]]then
call Os(we[iw])
endif
set iw=qw
endloop
set iw=ef[gr]
loop
exitwhen iw==0
set qw=ze[iw]
if ki[Dh[we[iw]]]then
call Os(we[iw])
endif
set iw=qw
endloop
endfunction
function Yw takes integer gr returns nothing
local integer iw=Fe[gr]
local integer qw
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=He[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=Je[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=Ne[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=Re[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=Ve[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=Ye[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=ef[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=hf[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=nf[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=rf[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=vf[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
call Ut("MinionBuffStorage "+I2S(gr)+" is deleted.")
endfunction
function Zw takes integer gr returns nothing
if gr==null then
return
elseif(De[gr]!=-1)then
return
endif
call Yw(gr)
set De[gr]=Ce
set Ce=gr
endfunction
function ex takes integer T returns integer
local integer fx=ys()
set ki[fx]=ki[Q4[T]]
set mi[fx]=mi[Q4[T]]
set ni[fx]=ni[Q4[T]]
set oi[fx]=oi[Q4[T]]
set pi[fx]=pi[Q4[T]]
set qi[fx]=qi[Q4[T]]
set ri[fx]=ri[Q4[T]]
return fx
endfunction
function gx takes integer T returns integer
local integer fx=zr()
set sk[fx]=sk[Z4[T]]
set tk[fx]=tk[Z4[T]]
set uk[fx]=uk[Z4[T]]
set vk[fx]=vk[Z4[T]]
set wk[fx]=wk[Z4[T]]
set xk[fx]=xk[Z4[T]]
set yk[fx]=yk[Z4[T]]
return fx
endfunction
function hx takes integer T returns integer
local integer fx=jr()
set Ln[fx]=Ln[g7[T]]
set Mn[fx]=Mn[g7[T]]
set Nn[fx]=Nn[g7[T]]
set On[fx]=On[g7[T]]
set Pn[fx]=Pn[g7[T]]
set Qn[fx]=Qn[g7[T]]
set Rn[fx]=Rn[g7[T]]
set Sn[fx]=Sn[g7[T]]
set Tn[fx]=Tn[g7[T]]
set Un[fx]=Un[g7[T]]
set Vn[fx]=Vn[g7[T]]
set Wn[fx]=Wn[g7[T]]
set Xn[fx]=Xn[g7[T]]
set Yn[fx]=Yn[g7[T]]
set Zn[fx]=Zn[g7[T]]
set do[fx]=do[g7[T]]
set eo[fx]=eo[g7[T]]
set fo[fx]=fo[g7[T]]
set go[fx]=go[g7[T]]
set ho[fx]=ho[g7[T]]
set io[fx]=go[fx]
set jo[fx]=ho[fx]
set ko[fx]=ko[g7[T]]
set mo[fx]=mo[g7[T]]
set no[fx]=no[g7[T]]
return fx
endfunction
function jx takes integer p returns nothing
local integer a=0
local integer b
local integer kx
local integer f1=cg[p]
local integer f2=Dg[p]
call UnitAddAbility(fg[p],rd[f1])
call UnitAddAbility(fg[p],sd[f2])
call UnitAddAbility(gg[p],qd[f1])
call UnitAddAbility(ig[p],lo[pd[f1]])
call UnitAddAbility(ig[p],Jo[pd[f2]])
loop
set kx=nd[od[f1]+a]
call UnitAddAbility(gg[p],Mn[g7[kx]])
set b=0
loop
call UnitAddAbility(hg[p],to[uo[i7[kx]]+b])
call UnitAddAbility(hg[p],vo[wo[i7[kx]]+b])
set b=b+1
exitwhen b==3
endloop
set a=a+1
exitwhen a==4
endloop
set Eg[p]=f1
set Gg[p]=f1
set Fg[p]=f2
set Hg[p]=f2
endfunction
function mx takes integer gr returns nothing
set Lg[gr]=CreateTimer()
call SaveInteger(n7,6,GetHandleId((Lg[gr])),(gr))
endfunction
function nx takes integer gr returns nothing
call xu(Lg[gr])
set Lg[gr]=null
endfunction
function ox takes integer gr,integer T returns nothing
set ug[vg[gr]+T]=ug[vg[gr]+T]+1
if ug[vg[gr]+T]==Wn[sg[tg[gr]+T]]then
call SetPlayerAbilityAvailable(Bf[gr],Mn[g7[T]],false)
endif
endfunction
function px takes integer gr,integer T returns nothing
set ug[vg[gr]+T]=ug[vg[gr]+T]-1
call SetPlayerAbilityAvailable(Bf[gr],Mn[g7[T]],true)
endfunction
function qx takes integer gr returns nothing
local integer a=0
loop
set ug[vg[gr]+a]=0
call SetPlayerAbilityAvailable(Bf[gr],Mn[g7[a]],true)
set a=a+1
exitwhen a==32
endloop
endfunction
function rx takes integer gr,integer T,real Gs returns real
set go[sg[tg[gr]+T]]=go[sg[tg[gr]+T]]+Gs
set Gs=(gd[hd[re[(Pn[sg[tg[gr]+T]])]]+(rg[gr])])*Gs
set io[sg[tg[gr]+T]]=io[sg[tg[gr]+T]]+Gs
return Gs
endfunction
function sx takes integer gr,integer T,real Gs returns real
set ho[sg[tg[gr]+T]]=ho[sg[tg[gr]+T]]+Gs
set Gs=(gd[hd[re[(Pn[sg[tg[gr]+T]])]]+(rg[gr])])*Gs
set jo[sg[tg[gr]+T]]=jo[sg[tg[gr]+T]]+Gs
return Gs
endfunction
function tx takes integer gr,integer Zv returns nothing
set Sn[sg[tg[gr]+Fo[Ag[gr]]]]=un[Zv]
if Cg[gr]!=0 then
if Yg[Cg[gr]]!=0 then
call vs(Yg[Cg[gr]])
endif
set Hi=gr
call TriggerEvaluate(d7[un[Zv]])
endif
set bg[gr]=Zv
endfunction
function ux takes integer gr returns nothing
set Kg[gr]=0
if Ff[gr]then
call ClearTextMessages()
endif
endfunction
function vx takes nothing returns nothing
set Kg[((LoadInteger(n7,6,GetHandleId(GetExpiredTimer()))))]=0
endfunction
function wx takes integer gr,sound xx,string s returns nothing
if Ff[gr]then
if Kg[gr]==5 then
set Kg[gr]=0
call ClearTextMessages()
endif
call StartSound(xx)
call DisplayTimedTextToPlayer(Bf[gr],.0,.0,5.,s)
set Kg[gr]=Kg[gr]+1
endif
call TimerStart(Lg[gr],5.,false,function vx)
endfunction
function yx takes integer gr returns nothing
local integer a=0
set eg[gr]=not eg[gr]
loop
call ShowImage(Zf[dg[gr]+a],eg[gr])
set a=a+1
exitwhen a==120
endloop
endfunction
function zx takes integer gr returns nothing
set Qf[gr]=Qf[gr]+1
call MultiboardSetItemValue(Yf[gr],I2S(Qf[gr]))
endfunction
function Ax takes integer gr,integer ax returns nothing
set Of[gr]=Of[gr]+ax
call MultiboardSetItemValue(Wf[gr],I2S(Of[gr]))
endfunction
function Bx takes integer gr,integer ow returns nothing
set Kf[gr]=Kf[gr]+ow
if Kf[gr]>99999 then
set Kf[gr]=99999
endif
call MultiboardSetItemValue(Tf[gr],I2S(Kf[gr]))
endfunction
function bx takes integer gr,integer Gs returns boolean
local boolean Cx=(Kf[(gr)]-(Gs)>=0)
if Cx then
call Bx((gr),-(Gs))
endif
return Cx
endfunction
function Dx takes integer gr,integer Gs returns boolean
local boolean Cx=(Kf[(gr)]-(Gs)>=0)
if Cx then
call Bx((gr),-(Gs))
else
call wx((gr),o8,Y9[T9]+"Error|r: not enough Gold.")
endif
return Cx
endfunction
function Ex takes integer gr,string s,unit u returns nothing
call iu(("+"+s),(u),(Bf[gr]),9.,48.,3.,2.)
call SetTextTagColor(bj_lastCreatedTextTag,V9[J9],W9[J9],X9[J9],$FF)
endfunction
function Fx takes integer gr,integer ow returns nothing
set Lf[gr]=Lf[gr]+ow
if Lf[gr]>99999 then
set Lf[gr]=99999
endif
call MultiboardSetItemValue(Uf[gr],I2S(Lf[gr]))
endfunction
function Gx takes integer gr,integer Gs returns boolean
local boolean Cx=(Lf[(gr)]-(Gs)>=0)
if Cx then
call Fx((gr),-(Gs))
endif
return Cx
endfunction
function Hx takes integer gr,integer Gs returns boolean
local boolean Cx=(Lf[(gr)]-(Gs)>=0)
if Cx then
call Fx((gr),-(Gs))
else
call wx((gr),n8,Y9[T9]+"Error|r: not enough Experience.")
endif
return Cx
endfunction
function Ix takes integer gr,string s,unit u returns nothing
call iu(("+"+s),(u),(Bf[gr]),9.,48.,3.,2.)
call SetTextTagColor(bj_lastCreatedTextTag,V9[K9],W9[K9],X9[K9],$FF)
endfunction
function lx takes integer gr,real ow returns nothing
set Nf[gr]=Nf[gr]-ow
call MultiboardSetItemValue(Vf[gr],I2S(fu(Nf[gr]))+"/"+I2S(Mf[gr]))
endfunction
function Jx takes integer gr,real ow returns boolean
return Nf[gr]+ow<=Mf[gr]
endfunction
function Kx takes integer gr,integer l returns nothing
set Pf[gr]=Pf[gr]+l
if Pf[gr]<0 then
set Pf[gr]=0
elseif Pf[gr]>9999 then
set Pf[gr]=9999
endif
call MultiboardSetItemValue(Xf[gr],I2S(Pf[gr]))
endfunction
function Lx takes integer gr,integer l returns nothing
if l==0 then
return
elseif l<-1 then
call wx(gr,s8,Y9[S9]+"Warning|r: you lost "+Y9[P9]+I2S(-l)+"|r lives.")
elseif l==-1 then
call wx(gr,s8,Y9[S9]+"Warning|r: you lost "+Y9[P9]+"1|r life.")
elseif l==1 then
call wx(gr,t8,Y9[R9]+"Note|r: you received "+Y9[P9]+"1|r life.")
elseif l>1 then
call wx(gr,t8,Y9[R9]+"Note|r: you received "+Y9[P9]+I2S(l)+"|r lives.")
endif
call Kx(gr,l)
endfunction
function Mx takes integer gr returns nothing
local integer Nx=fu(Pf[gr]*.5)
if Pf[gr]-Nx<1 then
set Nx=Pf[gr]-1
endif
if Nx==1 then
call wx(gr,u8,Y9[S9]+"Attention|r: the game lasts too long! You lost "+Y9[P9]+"1|r life.")
else
call wx(gr,u8,Y9[S9]+"Attention|r: the game lasts too long! You lost "+Y9[P9]+I2S(Nx)+"|r lives.")
endif
call Kx(gr,-Nx)
endfunction
function Ox takes integer gr returns nothing
local integer a=0
local multiboarditem Px
loop
set Px=MultiboardGetItem(a8,Rf[gr],a)
call MultiboardSetItemValueColor(Px,V9[U9],W9[U9],X9[U9],K8)
call MultiboardReleaseItem(Px)
set a=a+1
exitwhen a==8
endloop
set Px=null
endfunction
function Qx takes integer gr returns nothing
call nx(gr)
call Rv(If[gr])
if Hf[gr]then
call is(gr)
call Ox(gr)
call RemoveUnit(fg[gr])
call RemoveUnit(gg[gr])
call RemoveUnit(ig[gr])
call RemoveUnit(hg[gr])
call RemoveUnit(jg[gr])
set fg[gr]=null
set gg[gr]=null
set hg[gr]=null
set ig[gr]=null
set jg[gr]=null
set Ig[gr]=null
set lg[gr]=null
set Jg[gr]=null
endif
set Ef[gr]=false
endfunction
function Rx takes integer gr returns nothing
set xh[gr]=CreateTimer()
call SaveInteger(n7,6,GetHandleId((xh[gr])),(gr))
endfunction
function Sx takes integer gr returns nothing
call xu(xh[gr])
set xh[gr]=null
endfunction
function Tx takes integer p,integer id returns boolean
local string Ux=GetObjectName(id)
local integer T
local real Vx
if SubString(Ux,0,B4)=="Spawn" then
set T=S2I(SubString(Ux,b4,C4))
set Vx=Vn[sg[tg[p]+T]]
if Jx(p,Vx)then
if Dx(p,Tn[sg[tg[p]+T]])then
call lx((p),-((Vx)*1.))
set Pg=p
call TriggerEvaluate(h7[T])
return true
endif
else
call wx(((p)),n8,Y9[T9]+"Error|r: "+("unable to create a minion due to the maximum supply limit."))
endif
endif
return false
endfunction
function Wx takes integer p returns nothing
if Ag[p]!=0 and Cg[p]==0 then
set Pg=p
call TriggerEvaluate(h7[Fo[Ag[p]]])
set Cg[p]=(f9[g9[(mg[p])]+(h9[mg[p]])])
endif
endfunction
function Xx takes integer p,integer id,boolean Yx returns boolean
local string Ux=GetObjectName(id)
local integer T
local integer Zx
if SubString(Ux,0,c4)=="Upgrade" then
set T=S2I(SubString(Ux,D4,E4))
set Zx=S2I(SubString(Ux,F4,G4))
set Ux=SubString(Ux,H4,StringLength(Ux))
if(Yx and Dx(p,ao[Bo[i7[T]]+Zx]))or((not Yx)and Hx(p,ao[Bo[i7[T]]+Zx]))then
set Qg=p
set oo[po[sg[tg[p]+T]]+Zx]=true
call TriggerExecute(bo[Co[i7[T]]+Zx])
call SetPlayerAbilityAvailable(Bf[p],id,false)
call SetPlayerAbilityAvailable(Bf[p],vo[wo[i7[T]]+Zx],true)
call SetPlayerTechResearched(Bf[p],zo[Ao[i7[T]]+Zx],1)
call wx((p),p8,Y9[R9]+"Research complete|r: "+(Ux)+".")
return true
endif
endif
return false
endfunction
function ey takes nothing returns nothing
if(IsUnitType(GetFilterUnit(),e4))then
call IssueImmediateOrderById(GetFilterUnit(),H)
endif
endfunction
function fy takes integer gr,integer T,integer p returns nothing
set Sg[gr]=p
set Vg[gr]=CreateUnit(Bf[p],Ln[g7[T]],su(Wd[If[p]]),tu(Wd[If[p]]),270.)
call UnitAddAbility(Vg[gr],1098282348)
call UnitMakeAbilityPermanent(Vg[gr],true,1098282348)
call SetUnitUserData(Vg[gr],gr)
set Rg[gr]=T
set Wg[gr]=GetHandleId(Vg[gr])
set hh[gr]=sg[tg[p]+T]
call UnitAddAbility(Vg[gr],1093677099)
call UnitMakeAbilityPermanent(Vg[gr],true,1093677099)
call UnitAddAbility(Vg[gr],Nn[g7[T]])
call UnitMakeAbilityPermanent(Vg[gr],true,Nn[g7[T]])
call UnitAddAbility(Vg[gr],xo[yo[i7[T]]])
call UnitMakeAbilityPermanent(Vg[gr],true,xo[yo[i7[T]]])
call UnitAddAbility(Vg[gr],xo[yo[i7[T]]+1])
call UnitMakeAbilityPermanent(Vg[gr],true,xo[yo[i7[T]]+1])
call UnitAddAbility(Vg[gr],xo[yo[i7[T]]+2])
call UnitMakeAbilityPermanent(Vg[gr],true,xo[yo[i7[T]]+2])
set Hi=gr
call TriggerEvaluate(d7[Sn[hh[gr]]])
call UnitAddAbility(Vg[gr],1093677122)
call UnitMakeAbilityPermanent(Vg[gr],true,1093677122)
call UnitAddAbility(Vg[gr],sk[Z4[Sn[hh[gr]]]])
call UnitMakeAbilityPermanent(Vg[gr],true,sk[Z4[Sn[hh[gr]]]])
call Su(mg[p],gr)
call Ax(p,Xn[hh[gr]])
call ox(p,T)
endfunction
function gy takes integer gr,integer T,integer p returns nothing
local integer a
set Sg[gr]=p
set Vg[gr]=CreateUnit(Bf[p],Ln[g7[T]],su(Wd[If[p]]),tu(Wd[If[p]]),270.)
call UnitAddAbility(Vg[gr],1098282348)
call UnitMakeAbilityPermanent(Vg[gr],true,1098282348)
call SetUnitUserData(Vg[gr],gr)
set Rg[gr]=T
set Wg[gr]=GetHandleId(Vg[gr])
set hh[gr]=sg[tg[p]+T]
call UnitAddAbility(Vg[gr],1093677099)
call UnitMakeAbilityPermanent(Vg[gr],true,1093677099)
call UnitAddAbility(Vg[gr],Nn[g7[T]])
call UnitMakeAbilityPermanent(Vg[gr],true,Nn[g7[T]])
call UnitAddAbility(Vg[gr],xo[yo[i7[T]]])
call UnitMakeAbilityPermanent(Vg[gr],true,xo[yo[i7[T]]])
call UnitAddAbility(Vg[gr],xo[yo[i7[T]]+1])
call UnitMakeAbilityPermanent(Vg[gr],true,xo[yo[i7[T]]+1])
call UnitAddAbility(Vg[gr],xo[yo[i7[T]]+2])
call UnitMakeAbilityPermanent(Vg[gr],true,xo[yo[i7[T]]+2])
set Hi=gr
call TriggerEvaluate(d7[Sn[hh[gr]]])
call Su(mg[p],gr)
set T=0
loop
set a=Go[Ho[Ag[p]]+T]
call UnitAddAbility(Vg[gr],yn[a])
call UnitMakeAbilityPermanent(Vg[gr],true,yn[a])
call UnitAddAbility(Vg[gr],Cn[cn[a]])
call UnitMakeAbilityPermanent(Vg[gr],true,Cn[cn[a]])
call UnitAddAbility(Vg[gr],Cn[cn[a]+1])
call UnitMakeAbilityPermanent(Vg[gr],true,Cn[cn[a]+1])
set T=T+1
exitwhen T==3
endloop
endfunction
function hy takes integer gr returns nothing
call Tu(mg[Sg[gr]],gr)
call px(Sg[gr],Rg[gr])
call lx(Sg[gr],Un[hh[gr]])
call Ax(Sg[gr],-Xn[hh[gr]])
call Bx(Sg[gr],Tn[hh[gr]])
if Tn[hh[gr]]>0 then
call Ex(Sg[gr],I2S(Tn[hh[gr]]),Vg[gr])
endif
call DestroyEffect(AddSpecialEffect(("Abilities\\Spells\\Other\\Transmute\\PileofGold.mdx"),((GetUnitX(Vg[gr]))*1.),((GetUnitY(Vg[gr]))*1.)))
call RemoveUnit(Vg[gr])
call Ls(gr)
call Ut("Minion "+I2S(gr)+" is sold.")
endfunction
function jy takes integer gr returns nothing
call UnitRemoveAbility(Vg[gr],1093677099)
set Tg[gr]=Df[Sg[gr]]
set Ug[gr]=If[Tg[gr]]
call Uu(ng[Tg[gr]],gr)
call SetUnitMoveSpeed(Vg[gr],256.)
call SetUnitOwner(Vg[gr],Gf[Tg[gr]],false)
call SetUnitX(Vg[gr],GetUnitX(Vg[gr])-GetRectCenterX(Wd[If[Sg[gr]]])+GetRectCenterX(Xd[Ug[gr]]))
call SetUnitY(Vg[gr],GetUnitY(Vg[gr])-GetRectCenterY(Wd[If[Sg[gr]]])+GetRectCenterY(Xd[Ug[gr]]))
set ih[gr]=su(Zd[Ug[gr]])
set jh[gr]=GetRectMinY(Zd[Ug[gr]])
call IssuePointOrderById(Vg[gr],G,ih[gr],jh[gr])
call Ut("Minion "+I2S(gr)+" is sent.")
endfunction
function ky takes integer gr returns nothing
call Vu(ng[Tg[gr]],gr)
call lx(Sg[gr],Un[hh[gr]])
call RemoveUnit(Vg[gr])
call Ls(gr)
call Ut("Spawned "+I2S(gr)+" is deleted.")
endfunction
function my takes integer gr,boolean ny returns nothing
local string oy
if ny then
set oy=Y9[O9]+"+1"
else
set oy=""
endif
if Yn[hh[gr]]>0 then
call Bx(Tg[gr],Yn[hh[gr]])
set oy=oy+"|r
"+Y9[J9]+"+"+I2S(Yn[hh[gr]])
endif
if Zn[hh[gr]]>0 then
call Fx(Tg[gr],Zn[hh[gr]])
set oy=oy+"|r
"+Y9[K9]+"+"+I2S(Zn[hh[gr]])
endif
call iu((oy),(Vg[gr]),(Bf[Tg[gr]]),9.,48.,3.,2.)
endfunction
function py takes integer gr returns boolean
return not(Zg[gr]or dh[gr])
endfunction
function qy takes integer gr returns nothing
local real qw=(le[Xg[(gr)]])
if qw>.0 then
call UnitAddAbility(Vg[gr],1093677105)
else
call UnitRemoveAbility(Vg[gr],1093677105)
endif
call SetItemCharges(nh[gr],fu(qw))
endfunction
function ry takes integer gr returns real
return eh[gr]/ fh[gr]
endfunction
function sy takes integer gr,real ow returns nothing
if ow<.0 then
set ow=.0
elseif ow>fh[gr]then
set ow=fh[gr]
endif
set eh[gr]=ow
call SetItemCharges(mh[gr],fu(eh[gr]))
call SetWidgetLife(Vg[gr],3.+eh[gr]/ fh[gr]*1000.)
endfunction
function ty takes integer gr,real ow returns nothing
call sy(gr,eh[gr]+ow)
endfunction
function uy takes integer gr,real vy,real wy returns nothing
set fh[gr]=fh[gr]+vy
set gh[gr]=gh[gr]+wy
if Zg[gr]then
call ty(gr,vy)
endif
endfunction
function xy takes integer gr,real ow,string yy,string vu returns nothing
if eh[gr]<fh[gr]then
call ty(gr,ow)
call DestroyEffect(AddSpecialEffectTarget((yy),(Vg[gr]),(vu)))
endif
endfunction
function zy takes integer gr,real Ay,string yy,string vu returns nothing
call xy(gr,fh[gr]*Ay,yy,vu)
endfunction
function ay takes integer gr returns integer
local integer By
set J4=as(gr)
set By=J4+Xe[Xg[gr]]+gf[Xg[gr]]
if By>75 then
return 75
elseif By<I4 then
return I4
endif
return By
endfunction
function by takes integer gr returns integer
local integer By
set K4=Bs(gr)
set By=K4+df[Xg[gr]]+gf[Xg[gr]]
if By>75 then
return 75
elseif By<l4 then
return l4
endif
return By
endfunction
function Cy takes integer gr returns nothing
local real Dy=As(gr)
local real Ey=Me[Xg[gr]]
local real Fy
call SetUnitMoveSpeed(Vg[gr],256.*(Dy+Ey))
set Fy=GetUnitMoveSpeed(Vg[gr])
set Ey=Fy/ 256.-Dy
if Ey>.0 then
call UnitRemoveAbility(Vg[gr],1093677134)
call UnitAddAbility(Vg[gr],1093677128)
elseif Ey<.0 then
call UnitRemoveAbility(Vg[gr],1093677128)
call UnitAddAbility(Vg[gr],1093677134)
else
call UnitRemoveAbility(Vg[gr],1093677134)
call UnitRemoveAbility(Vg[gr],1093677128)
endif
call SetItemCharges(qh[gr],eu(Fy/ 256.*100.))
endfunction
function Gy takes integer gr returns nothing
if(Qe[Xg[(gr)]])then
call IssueImmediateOrderById(Vg[gr],K)
call UnitAddAbility(Vg[gr],1093677142)
else
call IssuePointOrderById(Vg[gr],G,ih[gr],jh[gr])
call UnitRemoveAbility(Vg[gr],1093677142)
endif
endfunction
function Hy takes integer gr returns nothing
local integer By=ay(gr)
local integer Iy=J4
if By>0 then
call SetItemCharges(oh[gr],By)
call UnitRemoveAbility(Vg[gr],1093677115)
call UnitRemoveAbility(Vg[gr],1110454321)
call UnitAddAbility(Vg[gr],1093677114)
elseif By<0 then
call SetItemCharges(oh[gr],-By)
call UnitRemoveAbility(Vg[gr],1093677114)
call UnitRemoveAbility(Vg[gr],1110454320)
call UnitAddAbility(Vg[gr],1093677115)
else
call SetItemCharges(oh[gr],0)
call UnitRemoveAbility(Vg[gr],1093677114)
call UnitRemoveAbility(Vg[gr],1110454320)
call UnitRemoveAbility(Vg[gr],1093677115)
call UnitRemoveAbility(Vg[gr],1110454321)
endif
if By>Iy then
call UnitRemoveAbility(Vg[gr],1093677089)
call UnitAddAbility(Vg[gr],1093677147)
elseif By<Iy then
call UnitRemoveAbility(Vg[gr],1093677147)
call UnitAddAbility(Vg[gr],1093677089)
else
call UnitRemoveAbility(Vg[gr],1093677089)
call UnitRemoveAbility(Vg[gr],1093677147)
endif
endfunction
function ly takes integer gr returns nothing
local integer By=by(gr)
local integer Iy=K4
if By>0 then
call SetItemCharges(ph[gr],By)
call UnitRemoveAbility(Vg[gr],1093677117)
call UnitRemoveAbility(Vg[gr],1110454323)
call UnitAddAbility(Vg[gr],1093677116)
elseif By<0 then
call SetItemCharges(ph[gr],-By)
call UnitRemoveAbility(Vg[gr],1093677116)
call UnitRemoveAbility(Vg[gr],1110454322)
call UnitAddAbility(Vg[gr],1093677117)
else
call SetItemCharges(ph[gr],0)
call UnitRemoveAbility(Vg[gr],1093677116)
call UnitRemoveAbility(Vg[gr],1110454322)
call UnitRemoveAbility(Vg[gr],1093677117)
call UnitRemoveAbility(Vg[gr],1110454323)
endif
if By>Iy then
call UnitRemoveAbility(Vg[gr],1093677150)
call UnitAddAbility(Vg[gr],1093677149)
elseif By<Iy then
call UnitRemoveAbility(Vg[gr],1093677149)
call UnitAddAbility(Vg[gr],1093677150)
else
call UnitRemoveAbility(Vg[gr],1093677150)
call UnitRemoveAbility(Vg[gr],1093677149)
endif
endfunction
function Jy takes integer gr returns nothing
local integer By=ay(gr)
local integer Iy=J4
if By>0 then
call SetItemCharges(oh[gr],By)
call UnitRemoveAbility(Vg[gr],1093677115)
call UnitRemoveAbility(Vg[gr],1110454321)
call UnitAddAbility(Vg[gr],1093677114)
elseif By<0 then
call SetItemCharges(oh[gr],-By)
call UnitRemoveAbility(Vg[gr],1093677114)
call UnitRemoveAbility(Vg[gr],1110454320)
call UnitAddAbility(Vg[gr],1093677115)
else
call SetItemCharges(oh[gr],0)
call UnitRemoveAbility(Vg[gr],1093677114)
call UnitRemoveAbility(Vg[gr],1110454320)
call UnitRemoveAbility(Vg[gr],1093677115)
call UnitRemoveAbility(Vg[gr],1110454321)
endif
if By>Iy then
call UnitRemoveAbility(Vg[gr],1093677089)
call UnitAddAbility(Vg[gr],1093677147)
elseif By<Iy then
call UnitRemoveAbility(Vg[gr],1093677147)
call UnitAddAbility(Vg[gr],1093677089)
else
call UnitRemoveAbility(Vg[gr],1093677089)
call UnitRemoveAbility(Vg[gr],1093677147)
endif
set By=by(gr)
set Iy=K4
if By>0 then
call SetItemCharges(ph[gr],By)
call UnitRemoveAbility(Vg[gr],1093677117)
call UnitRemoveAbility(Vg[gr],1110454323)
call UnitAddAbility(Vg[gr],1093677116)
elseif By<0 then
call SetItemCharges(ph[gr],-By)
call UnitRemoveAbility(Vg[gr],1093677116)
call UnitRemoveAbility(Vg[gr],1110454322)
call UnitAddAbility(Vg[gr],1093677117)
else
call SetItemCharges(ph[gr],0)
call UnitRemoveAbility(Vg[gr],1093677116)
call UnitRemoveAbility(Vg[gr],1110454322)
call UnitRemoveAbility(Vg[gr],1093677117)
call UnitRemoveAbility(Vg[gr],1110454323)
endif
if By>Iy then
call UnitRemoveAbility(Vg[gr],1093677150)
call UnitAddAbility(Vg[gr],1093677149)
elseif By<Iy then
call UnitRemoveAbility(Vg[gr],1093677149)
call UnitAddAbility(Vg[gr],1093677150)
else
call UnitRemoveAbility(Vg[gr],1093677150)
call UnitRemoveAbility(Vg[gr],1093677149)
endif
endfunction
function Ky takes integer gr,integer iw,real ow returns nothing
call nw(Xg[gr],iw,ow)
call qy(gr)
endfunction
function Ly takes integer gr,integer iw,real ow returns nothing
call vw(Xg[gr],iw,ow)
call Cy(gr)
endfunction
function Ny takes integer gr,integer iw,boolean ow returns nothing
local boolean Oy=not(Ue[Xg[(gr)]])
call aw(Xg[gr],iw,ow)
if ow and Oy then
call Ww(Xg[gr])
call Cy(gr)
call Gy(gr)
call Ut("Minion is Unstoppable!")
endif
endfunction
function Qy takes integer gr,integer iw,integer ow returns nothing
call Ew(Xg[gr],iw,ow)
call ly(gr)
endfunction
function Ry takes integer gr,integer iw,integer ow returns nothing
call Hw(Xg[gr],iw,ow)
call Jy(gr)
endfunction
function Sy takes integer gr,integer iw,boolean ow returns nothing
local boolean Oy=not(yf[Xg[(gr)]])
call Sw(Xg[gr],iw,ow)
if ow then
if Oy then
call Xw(Xg[gr])
call Cy(gr)
call Gy(gr)
call Jy(gr)
call UnitAddType(Vg[gr],g4)
call Ut("Minion is Invulnerable!")
endif
elseif not(yf[Xg[(gr)]])then
call UnitRemoveType(Vg[(gr)],g4)
endif
endfunction
function Ty takes integer gr,integer ew,real ow returns integer
local integer iw=kw(Xg[gr],ew)
call Ky(gr,iw,ow)
return iw
endfunction
function Uy takes integer gr,integer ew,real ow returns integer
local integer iw=rw(Xg[gr],ew)
call Ly(gr,iw,ow)
return iw
endfunction
function Wy takes integer gr,integer ew,boolean ow returns integer
local integer iw=zw(Xg[gr],ew)
call Ny(gr,iw,ow)
return iw
endfunction
function Xy takes integer gr,integer ew,integer ow returns integer
local integer iw=cw(Xg[gr],ew)
call Qy(gr,iw,ow)
return iw
endfunction
function Yy takes integer gr,integer ew,integer ow returns integer
local integer iw=Fw(Xg[gr],ew)
call Ry(gr,iw,ow)
return iw
endfunction
function Zy takes integer gr,integer ew,boolean ow returns integer
local integer iw=Iw(Xg[gr],ew)
call Jw(Xg[(gr)],(iw),(ow))
return iw
endfunction
function dz takes integer gr,integer ew,boolean ow returns integer
local integer iw=Nw(Xg[gr],ew)
call Pw(Xg[(gr)],(iw),(ow))
return iw
endfunction
function ez takes integer gr,integer ew,boolean ow returns integer
local integer iw=Qw(Xg[gr],ew)
call Sy(gr,iw,ow)
return iw
endfunction
function fz takes integer gr,integer iw returns nothing
call mw(Xg[gr],iw)
call qy(gr)
endfunction
function gz takes integer gr,integer iw returns nothing
call tw(Xg[gr],iw)
call Cy(gr)
endfunction
function hz takes integer gr,integer iw returns nothing
call xw(Xg[gr],iw)
call Gy(gr)
endfunction
function iz takes integer gr,integer iw returns nothing
call Dw(Xg[gr],iw)
call ly(gr)
endfunction
function jz takes integer gr,integer iw returns nothing
call Gw(Xg[gr],iw)
call Jy(gr)
endfunction
function kz takes integer gr,integer iw returns nothing
call Rw(Xg[gr],iw)
call UnitRemoveType(Vg[(gr)],g4)
endfunction
function mz takes integer gr returns boolean
return(yf[Xg[(gr)]])or(uf[Xg[(gr)]])or Cs(gr)
endfunction
function nz takes integer gr,integer qs returns boolean
return(yf[Xg[(gr)]])or(uf[Xg[(gr)]])or Cs(gr)or(qs==1227894834 and((mf[Xg[(gr)]])or cs(gr)))or(qs==1227894841 and((qf[Xg[(gr)]])or Ds(gr)))or(qs==1227894832 and Es(gr))
endfunction
function oz takes integer gr,real Gs returns real
return(100-ay(gr))*Gs/ 100.
endfunction
function pz takes integer gr,real Gs returns real
return(100-by(gr))*Gs/ 100.
endfunction
function qz takes integer gr,real Gs,integer qs returns real
if(yf[Xg[(gr)]])then
return .0
endif
if qs==1227894834 then
if(mf[Xg[(gr)]])or(uf[Xg[(gr)]])then
call Tw(Xg[gr],Gs)
return .0
endif
return Fs(gr,Gs)
endif
if qs==1227894841 then
if(qf[Xg[(gr)]])or(uf[Xg[(gr)]])then
call Uw(Xg[gr],Gs)
return .0
endif
return Hs(gr,Gs)
endif
if qs==1227894832 then
if(uf[Xg[(gr)]])then
call Vw(Xg[gr],(fh[(gr)]*((Gs)*1.)/ 1000.))
return .0
endif
return Is(gr,Gs)
endif
call Ut("Unrecognized damage type!")
return .0
endfunction
function rz takes integer gr,real Gs returns real
set Gs=pw(Xg[gr],Gs)
call qy(gr)
return Gs
endfunction
function sz takes integer gr,integer tz,real Gs,integer qs,boolean uz returns boolean
if not nz(gr,qs)then
call Tt(1,gr,tz,qs)
endif
set Gs=qz(gr,Gs,qs)
if Gs<=.0 then
return true
endif
set Gs=rz(gr,Gs)
if Gs>.0 then
call ty(gr,-Gs)
endif
if eh[gr]>.0 then
return true
endif
call my(gr,true)
call Wr(tz,gr)
call ls(gr,uz)
return false
endfunction
function vz takes integer gr returns integer
if IsUnitType(Vg[gr],UNIT_TYPE_FLYING)then
return zd[Sv(Ug[gr],GetUnitX(Vg[gr]),GetUnitY(Vg[gr]))]
endif
return xd[Sv(Ug[gr],GetUnitX(Vg[gr]),GetUnitY(Vg[gr]))]
endfunction
function wz takes integer gr returns nothing
call Xu(og[Tg[gr]],gr)
call lx(Sg[gr],Un[hh[gr]])
call Zw(Xg[gr])
call RemoveItem(mh[gr])
set mh[gr]=null
call RemoveItem(nh[gr])
set nh[gr]=null
call RemoveItem(oh[gr])
set oh[gr]=null
call RemoveItem(ph[gr])
set ph[gr]=null
call RemoveItem(qh[gr])
set qh[gr]=null
call RemoveItem(rh[gr])
set rh[gr]=null
call UnitRemoveAbility(Vg[gr],1093677105)
call UnitRemoveAbility(Vg[gr],1093677128)
call UnitRemoveAbility(Vg[gr],1093677134)
call UnitRemoveAbility(Vg[gr],1093677142)
call UnitRemoveAbility(Vg[gr],1093677149)
call UnitRemoveAbility(Vg[gr],1093677150)
call UnitRemoveAbility(Vg[gr],1093677147)
call UnitRemoveAbility(Vg[gr],1093677089)
endfunction
function xz takes integer gr returns nothing
call ev(pg[Sg[gr]],gr)
call wz(gr)
call Sx(gr)
call Ls(gr)
call Ut("Minion "+I2S(gr)+" is deleted.")
endfunction
function yz takes nothing returns nothing
local integer gr=(LoadInteger(n7,6,GetHandleId(GetExpiredTimer())))
if gh[gr]>.0 then
call ty(gr,gh[gr])
endif
set kh[gr]=kh[gr]-.25
if kh[gr]>.0 then
call SetItemCharges(rh[gr],fu(kh[gr]))
else
call Xv(Vg[gr])
call xz(gr)
endif
endfunction
function zz takes integer gr returns nothing
call Vu(ng[Tg[gr]],gr)
set fh[gr]=io[hh[gr]]
set gh[gr]=jo[hh[gr]]
set kh[gr]=On[hh[gr]]
call Rx(gr)
call UnitAddAbility(Vg[gr],1093677113)
call UnitAddAbility(Vg[gr],1093677118)
set mh[gr]=UnitAddItemById(Vg[gr],1227894836)
set nh[gr]=UnitAddItemById(Vg[gr],1227894847)
set oh[gr]=UnitAddItemById(Vg[gr],1227894840)
set ph[gr]=UnitAddItemById(Vg[gr],1227894837)
set qh[gr]=UnitAddItemById(Vg[gr],1227894848)
set rh[gr]=UnitAddItemById(Vg[gr],1227894849)
call SetItemCharges(rh[gr],fu(kh[gr]))
call TriggerRegisterUnitEvent(m8,Vg[gr],EVENT_UNIT_DAMAGED)
call TimerStart(xh[gr],.25,true,function yz)
set Xg[gr]=fs()
call Wu(og[Tg[gr]],gr)
call dv(pg[Sg[gr]],gr)
set Hi=gr
call TriggerEvaluate(e7[Sn[hh[gr]]])
call sy(gr,fh[gr])
call Cy(gr)
call Jy(gr)
set Zg[gr]=true
call SetUnitInvulnerable(Vg[gr],false)
set dh[gr]=false
endfunction
function Az takes integer gr returns boolean
return Jx(Sg[gr],Un[hh[gr]])
endfunction
function az takes integer gr,integer Bz,real Ay returns nothing
call Zu(qg[Tg[gr]],gr)
call RemoveUnit(Vg[gr])
set Vg[gr]=CreateUnit(Gf[Tg[gr]],Bz,sh[gr],th[gr],uh[gr])
set Wg[gr]=GetHandleId(Vg[gr])
call SetUnitColor(Vg[gr],GetPlayerColor(Bf[Sg[gr]]))
call SetUnitUserData(Vg[gr],gr)
call lx((Sg[gr]),-((Un[hh[gr]])*1.))
set kh[gr]=On[hh[gr]]
call UnitAddAbility(Vg[gr],1093677113)
call UnitAddAbility(Vg[gr],1093677118)
set mh[gr]=UnitAddItemById(Vg[gr],1227894836)
set nh[gr]=UnitAddItemById(Vg[gr],1227894847)
set oh[gr]=UnitAddItemById(Vg[gr],1227894840)
set ph[gr]=UnitAddItemById(Vg[gr],1227894837)
set qh[gr]=UnitAddItemById(Vg[gr],1227894848)
set rh[gr]=UnitAddItemById(Vg[gr],1227894849)
call SetItemCharges(rh[gr],fu(kh[gr]))
call TriggerRegisterUnitEvent(m8,Vg[gr],EVENT_UNIT_DAMAGED)
call TimerStart(xh[gr],.25,true,function yz)
set Xg[gr]=fs()
call Wu(og[Tg[gr]],gr)
call ts(Yg[gr])
call sy(gr,fh[gr]*Ay)
call Cy(gr)
call Jy(gr)
set Zg[gr]=true
call IssuePointOrderById(Vg[gr],G,ih[gr],jh[gr])
endfunction
function bz takes integer gr,real Ay returns nothing
call az(gr,GetUnitTypeId(Vg[gr]),Ay)
endfunction
function Cz takes integer gr returns nothing
call Zu(qg[Tg[gr]],gr)
call ev(pg[Sg[gr]],gr)
call Sx(gr)
call RemoveUnit(Vg[gr])
call Ls(gr)
call Ut("Corpse "+I2S(gr)+" is removed.")
endfunction
function cz takes nothing returns nothing
call Cz(((LoadInteger(n7,6,GetHandleId(GetExpiredTimer())))))
endfunction
function Dz takes integer gr returns nothing
call RemoveUnit(Vg[gr])
call xz(gr)
endfunction
function Ez takes integer gr,boolean Js returns nothing
call SetUnitExploded(Vg[gr],Js)
call KillUnit(Vg[gr])
if Js or Rn[hh[gr]]then
call xz(gr)
else
set Zg[gr]=false
call wz(gr)
call Yu(qg[Tg[gr]],gr)
call ss(Yg[gr])
set sh[gr]=GetUnitX(Vg[gr])
set th[gr]=GetUnitY(Vg[gr])
set uh[gr]=GetUnitFacing(Vg[gr])
call TimerStart(xh[gr],15.,false,function cz)
endif
endfunction
function s__Minion_on_init takes nothing returns nothing
set yh=Filter(function ey)
endfunction
function Fz takes integer Gz returns integer
if Gz<=9 then
return M4[Gz]
endif
return M4[9]
endfunction
function Hz takes integer p returns nothing
local integer m
local integer a=0
local real Dy
local real array Iz
local real array lz
local integer Jz=Fz(rg[p])
if Dx(p,Jz)then
set rg[p]=rg[p]+1
loop
set Dy=(gd[hd[re[(Pn[sg[tg[p]+a]])]]+(rg[p])])
set Iz[a]=go[sg[tg[p]+a]]*Dy-io[sg[tg[p]+a]]
set io[sg[tg[p]+a]]=io[sg[tg[p]+a]]+Iz[a]
set lz[a]=ho[sg[tg[p]+a]]*Dy-jo[sg[tg[p]+a]]
set jo[sg[tg[p]+a]]=jo[sg[tg[p]+a]]+lz[a]
set a=a+1
exitwhen a==32
endloop
set a=0
loop
exitwhen a>D9[pg[p]]
set m=(C9[c9[(pg[p])]+(a)])
call uy(m,Iz[Rg[m]],lz[Rg[m]])
set a=a+1
endloop
call SetItemCharges(lg[p],rg[p])
if rg[p]==L4 then
call ov(hg[p],Jg[p])
else
call SetItemCharges(Jg[p],Fz(rg[p])+1)
endif
call wx((p),p8,Y9[R9]+"Research complete|r: "+(("Minion Health Upgrade (level "+I2S((rg[p]))+")"))+".")
else
call SetItemCharges(Jg[p],Jz+1)
endif
endfunction
function Kz takes nothing returns nothing
set yh=Filter(function ey)
set M4[0]=500
set M4[1]=540
set M4[2]=585
set M4[3]=630
set M4[4]=680
set M4[5]=735
set M4[6]=795
set M4[7]=855
set M4[8]=925
set M4[9]=$3E8
set N4=M4[0]
endfunction
function Lz takes integer p,integer id,real ow returns nothing
local integer a=0
local integer m
local real Mz=rx(p,id,ow)
loop
exitwhen a>D9[pg[p]]
set m=(C9[c9[(pg[p])]+(a)])
if Rg[m]==id then
set fh[m]=fh[m]+Mz
if Zg[m]then
call ty(m,Mz)
endif
endif
set a=a+1
endloop
endfunction
function Nz takes integer p,integer id,real ow returns nothing
local integer a=0
local integer m
local real lz=sx(p,id,ow)
loop
exitwhen a>D9[pg[p]]
set m=(C9[c9[(pg[p])]+(a)])
if Rg[m]==id then
set gh[m]=gh[m]+lz
endif
set a=a+1
endloop
endfunction
function Oz takes integer p,integer id,real ow returns nothing
local integer a=0
local integer m
set ko[sg[tg[p]+id]]=ko[sg[tg[p]+id]]+ow
loop
exitwhen a>D9[pg[p]]
set m=(C9[c9[(pg[p])]+(a)])
if Rg[m]==id and Zg[m]then
call Cy(m)
endif
set a=a+1
endloop
endfunction
function Pz takes integer p,integer id,integer ow returns nothing
local integer a=0
local integer m
set mo[sg[tg[p]+id]]=mo[sg[tg[p]+id]]+ow
set no[sg[tg[p]+id]]=no[sg[tg[p]+id]]+ow
loop
exitwhen a>D9[pg[p]]
set m=(C9[c9[(pg[p])]+(a)])
if Rg[m]==id and Zg[m]then
call Jy(m)
endif
set a=a+1
endloop
endfunction
function Qz takes integer p,integer id,integer ow returns nothing
local integer a=0
local integer m
set mo[sg[tg[p]+id]]=mo[sg[tg[p]+id]]+ow
loop
exitwhen a>D9[pg[p]]
set m=(C9[c9[(pg[p])]+(a)])
if Rg[m]==id and Zg[m]then
call Hy(m)
endif
set a=a+1
endloop
endfunction
function Rz takes integer gr returns nothing
set Ih[gr]=CreateTimer()
call SaveInteger(n7,6,GetHandleId((Ih[gr])),(gr))
endfunction
function Sz takes integer gr returns nothing
call xu(Ih[gr])
set Ih[gr]=null
endfunction
function Tz takes integer gr,integer Uz,unit Vz,integer p,integer T returns nothing
if gr==0 then
call Gu("Error: MinionBuff struct has run out of indexes. Current amount of indexes is 8190. Error detected on field of player "+I2S(p),"MinionBuffError")
endif
set Bh[gr]=T
set bh[gr]=Uz
set Ch[gr]=Vz
set ch[gr]=p
set Dh[gr]=yg[zg[p]+T]
call Rz(gr)
call SaveInteger(n7,Wg[bh[gr]],T,gr)
endfunction
function Wz takes nothing returns nothing
local integer gr=(LoadInteger(n7,6,GetHandleId(GetExpiredTimer())))
set Eh[gr]=Eh[gr]-1
if Ms(gr)then
call Os(gr)
endif
endfunction
function Xz takes integer gr,integer Yz returns nothing
set Eh[gr]=Eh[gr]+Yz
if Eh[gr]>oi[Dh[gr]]then
set Eh[gr]=oi[Dh[gr]]
endif
endfunction
function Zz takes integer gr,real dA returns nothing
set Eh[gr]=ni[Dh[gr]]
call UnitAddAbility(Vg[bh[gr]],ri[Dh[gr]])
call TimerStart(Ih[gr],dA,mi[Dh[gr]],function Wz)
set Fh[gr]=true
endfunction
function eA takes integer gr returns nothing
call Zz(gr,qi[Dh[gr]])
endfunction
function fA takes integer gr,integer Yz returns nothing
if Fh[gr]then
call Xz(gr,Yz)
else
call eA(gr)
endif
endfunction
function gA takes integer m returns integer
local integer gr=dt()
set Lh[gr]=m
set Mh[gr]=CreateUnit(Bf[cj[m]],1685417325,lj[m],Jj[m],.0)
call SetUnitPathing(Mh[gr],false)
call SetUnitFlyHeight(Mh[gr],Hj[m],.0)
call UnitAddType(Mh[gr],d4)
set Oh[gr]=To[Nj[m]]
call UnitAddAbility(Mh[gr],Oh[gr])
call SetUnitUserData(Mh[gr],m)
set Nh[gr]=Uo[Nj[m]]
call Ut("TowerAttacker "+I2S(gr)+" is created.")
return gr
endfunction
function hA takes integer gr,integer iA,integer jA returns nothing
call UnitRemoveAbility(Mh[gr],Oh[gr])
set Oh[gr]=iA
call UnitAddAbility(Mh[gr],iA)
set Nh[gr]=jA
endfunction
function kA takes integer gr returns nothing
call hA(gr,To[Nj[Lh[gr]]],Uo[Nj[Lh[gr]]])
endfunction
function mA takes integer gr returns nothing
call IncUnitAbilityLevel(Mh[gr],Oh[gr])
endfunction
function nA takes integer gr,real x,real y returns boolean
return IssuePointOrderById(Mh[gr],Nh[gr],x,y)
endfunction
function oA takes integer gr,widget ns returns boolean
return IssueTargetOrderById(Mh[gr],Nh[gr],ns)
endfunction
function pA takes integer gr returns nothing
call RemoveUnit(Mh[gr])
set Mh[gr]=null
call Ut("TowerAttacker "+I2S(gr)+" is deleted.")
endfunction
function qA takes integer gr returns nothing
if gr==null then
return
elseif(Kh[gr]!=-1)then
return
endif
call pA(gr)
set Kh[gr]=lh
set lh=gr
endfunction
function rA takes integer m returns integer
local integer gr=Ys()
set Sh[gr]=m
set Th[gr]=vz(m)
set Uh[gr]=eh[m]
return gr
endfunction
function sA takes integer gr,integer tA returns nothing
set Wh[gr]=tA
set Vh[gr]=Vh[tA]
if Vh[gr]!=0 then
set Wh[Vh[gr]]=gr
endif
set Vh[tA]=gr
endfunction
function uA takes integer gr,integer tA returns nothing
set Vh[gr]=tA
set Wh[gr]=Wh[tA]
if Wh[gr]!=0 then
set Vh[Wh[gr]]=gr
endif
set Wh[tA]=gr
endfunction
function vA takes integer gr returns integer
local integer lv=di[gr]
local integer m=Sh[lv]
set di[gr]=Wh[lv]
if(di[(gr)]!=0)then
set Vh[di[gr]]=0
endif
call Zs(lv)
return m
endfunction
function wA takes integer xA,integer yA returns integer
local integer gr=Ws()
set di[gr]=xA
set ei[gr]=yA
return gr
endfunction
function zA takes integer gr returns nothing
local integer n=di[gr]
local integer AA
loop
exitwhen n==0
set AA=Wh[n]
call Zs(n)
set n=AA
endloop
endfunction
function aA takes integer Uz returns integer
local group g=CreateGroup()
local unit u
local integer c
local integer iw
local integer yA=0
local integer xA=0
call GroupEnumUnitsInRange(g,lj[Uz],Jj[Uz],Qo[Nj[Uz]],Po[Nj[Uz]])
loop
set u=FirstOfGroup(g)
exitwhen u==null
call GroupRemoveUnit(g,u)
set c=rA(GetUnitUserData(u))
set iw=yA
loop
if iw==0 then
if xA==0 then
set yA=c
set xA=c
else
call sA(c,xA)
set xA=c
endif
exitwhen true
elseif Th[c]>=Th[iw]then
call uA(c,iw)
if iw==yA then
set yA=c
endif
exitwhen true
endif
set iw=Vh[iw]
endloop
endloop
call DestroyGroup(g)
set g=null
if xA==0 then
return 0
endif
return wA(xA,yA)
endfunction
function BA takes nothing returns boolean
local integer m=GetUnitUserData(GetFilterUnit())
return((IsUnitType(GetFilterUnit(),e4))and Zg[m]and not(yf[Xg[(m)]]))!=null
endfunction
function bA takes nothing returns boolean
local integer m=GetUnitUserData(GetFilterUnit())
return((IsUnitType(GetFilterUnit(),e4))and IsUnitType(Vg[m],UNIT_TYPE_GROUND)and Zg[m]and not(yf[Xg[(m)]]))!=null
endfunction
function CA takes nothing returns boolean
local integer m=GetUnitUserData(GetFilterUnit())
return((IsUnitType(GetFilterUnit(),e4))and IsUnitType(Vg[m],UNIT_TYPE_FLYING)and Zg[m]and not(yf[Xg[(m)]]))!=null
endfunction
function cA takes nothing returns nothing
set P4[0]=Filter(function BA)
set P4[1]=Filter(function bA)
set P4[2]=Filter(function CA)
endfunction
function DA takes integer gr,integer EA returns nothing
set Gh[gr]=Xy(bh[gr],gr,EA)
endfunction
function FA takes integer m,unit GA,integer p,integer EA returns nothing
local integer gr=(LoadInteger(n7,Wg[(m)],(8)))
if gr==0 then
set gr=Vs()
call Tz(gr,m,GA,p,8)
call DA(gr,EA)
elseif Ch[gr]!=GA then
set Ch[gr]=GA
endif
call eA(gr)
endfunction
function HA takes integer gr returns nothing
set Gh[gr]=dz(bh[gr],gr,true)
endfunction
function IA takes integer m,integer GA returns nothing
local integer gr=(LoadInteger(n7,Wg[(m)],(9)))
if gr==0 then
set gr=Us()
call Tz(gr,m,Vg[GA],Sg[GA],9)
call HA(gr)
elseif Ch[gr]!=Vg[GA]then
set Ch[gr]=Vg[GA]
endif
call eA(gr)
endfunction
function lA takes integer gr,real JA returns nothing
set Gh[gr]=Ty(bh[gr],gr,JA)
endfunction
function KA takes integer GA,real JA returns nothing
local integer gr=(LoadInteger(n7,Wg[(GA)],(S4)))
if gr==0 then
set gr=Ts()
call Tz(gr,GA,Vg[GA],Sg[GA],S4)
call lA(gr,JA)
elseif Ch[gr]!=Vg[GA]then
set Ch[gr]=Vg[GA]
endif
call eA(gr)
endfunction
function LA takes integer gr,boolean up returns nothing
set Gh[gr]=Zy(bh[gr],gr,true)
if up then
set fi[gr]=true
set Hh[gr]=Wy(bh[gr],gr,true)
endif
call AddUnitAnimationProperties(Vg[bh[gr]],"Defend",true)
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\Human\\Defend\\DefendCaster.mdx"),(Vg[bh[gr]]),("origin")))
endfunction
function MA takes integer GA returns nothing
local integer gr=(LoadInteger(n7,Wg[(GA)],(7)))
if gr==0 then
set gr=Ss()
call Tz(gr,GA,Vg[GA],Sg[GA],7)
call LA(gr,oo[po[hh[GA]]+1])
elseif Ch[gr]!=Vg[GA]then
set Ch[gr]=Vg[GA]
endif
call eA(gr)
endfunction
function NA takes integer gr,integer By,real OA returns nothing
set Gh[gr]=Yy(bh[gr],gr,By)
set Hh[gr]=Uy(bh[gr],gr,OA)
endfunction
function PA takes integer m,integer GA,integer By,real OA returns nothing
local integer gr=(LoadInteger(n7,Wg[(m)],(R4)))
if gr==0 then
set gr=Rs()
call Tz(gr,m,Vg[GA],Sg[GA],R4)
call NA(gr,By,OA)
elseif Ch[gr]!=Vg[GA]then
set Ch[gr]=Vg[GA]
endif
call eA(gr)
endfunction
function QA takes integer gr returns nothing
set Gh[gr]=(hw(Xg[(bh[gr])],(gr)))
endfunction
function RA takes integer m,integer GA returns nothing
local integer gr=(LoadInteger(n7,Wg[(m)],(T4)))
if gr==0 then
set gr=Qs()
call Tz(gr,m,Vg[GA],Sg[GA],T4)
call QA(gr)
elseif Ch[gr]!=Vg[GA]then
set Ch[gr]=Vg[GA]
endif
call fA(gr,ni[Dh[gr]])
endfunction
function SA takes integer gr,boolean TA,real OA returns nothing
set Gh[gr]=Uy(bh[gr],gr,OA)
if TA then
set gi[gr]=true
set Hh[gr]=Wy(bh[gr],gr,true)
endif
endfunction
function UA takes integer GA,real OA returns nothing
local integer gr=(LoadInteger(n7,Wg[(GA)],(W4)))
if gr==0 then
set gr=Ps()
call Tz(gr,GA,Vg[GA],Sg[GA],W4)
call SA(gr,ry(GA)<=.35,OA)
elseif Ch[gr]!=Vg[GA]then
set Ch[gr]=Vg[GA]
endif
call eA(gr)
endfunction
function VA takes integer gr returns nothing
set Gh[gr]=ez(bh[gr],gr,true)
endfunction
function WA takes integer GA returns nothing
local integer gr=(LoadInteger(n7,Wg[(GA)],(U4)))
if gr==0 then
set gr=ft()
call Tz(gr,GA,Vg[GA],Sg[GA],U4)
call VA(gr)
elseif Ch[gr]!=Vg[GA]then
set Ch[gr]=Vg[GA]
endif
call eA(gr)
endfunction
function XA takes integer gr returns nothing
set Gh[gr]=ez(bh[gr],gr,true)
endfunction
function YA takes integer m,integer GA returns nothing
local integer gr=(LoadInteger(n7,Wg[(m)],(16)))
if gr==0 then
set gr=gt()
call Tz(gr,m,Vg[GA],Sg[GA],16)
call XA(gr)
elseif Ch[gr]!=Vg[GA]then
set Ch[gr]=Vg[GA]
endif
call eA(gr)
endfunction
function ZA takes integer gr returns nothing
set Gh[gr]=(hw(Xg[(bh[gr])],(gr)))
endfunction
function da takes integer m,integer GA returns nothing
local integer gr=(LoadInteger(n7,Wg[(m)],(17)))
if gr==0 then
set gr=ht()
call Tz(gr,m,Vg[GA],Sg[GA],17)
call ZA(gr)
elseif Ch[gr]!=Vg[GA]then
set Ch[gr]=Vg[GA]
endif
call fA(gr,ni[Dh[gr]])
endfunction
function ea takes integer T,boolean fa,integer ga,integer ha,real ia,integer ja returns nothing
set Q4[T]=ys()
set ki[Q4[T]]=fa
set mi[Q4[T]]=ha>1
set ni[Q4[T]]=ga
set oi[Q4[T]]=ha
set pi[Q4[T]]=ia
set qi[Q4[T]]=ia/ ga
set ri[Q4[T]]=ja
endfunction
function ma takes integer a,unit m,player p,integer Zv,integer jA returns integer
local integer gr=xs()
set vi[gr]=CreateUnit(p,1685417325,GetUnitX(m),GetUnitY(m),.0)
call SetUnitPathing(vi[gr],false)
call UnitAddAbility(vi[gr],Zv)
call UnitAddType(vi[gr],f4)
call SetUnitUserData(vi[gr],a)
set wi[gr]=jA
call Ut("MinionDummy "+I2S(gr)+" is created.")
return gr
endfunction
function na takes integer gr,real x,real y returns nothing
call SetUnitX(vi[gr],x)
call SetUnitY(vi[gr],y)
endfunction
function oa takes integer gr,real x,real y returns boolean
return IssuePointOrderById(vi[gr],wi[gr],x,y)
endfunction
function pa takes integer gr returns nothing
call RemoveUnit(vi[gr])
set vi[gr]=null
call Ut("MinionDummy "+I2S(gr)+" is deleted.")
endfunction
function qa takes integer gr returns nothing
if gr==null then
return
elseif(ui[gr]!=-1)then
return
endif
call pa(gr)
set ui[gr]=si
set si=gr
endfunction
function ra takes integer gr returns nothing
set Di[gr]=CreateTimer()
call SaveInteger(n7,6,GetHandleId((Di[gr])),(gr))
endfunction
function sa takes integer gr returns nothing
call xu(Di[gr])
set Di[gr]=null
endfunction
function ta takes nothing returns nothing
local integer gr=(LoadInteger(n7,6,GetHandleId(GetExpiredTimer())))
call SetImagePosition(ci[gr],GetUnitX(Ci[gr]),GetUnitY(Ci[gr]),.0)
endfunction
function ua takes unit u,real Au returns integer
local integer gr=ws()
set Ci[gr]=u
set ci[gr]=zu(Au,GetUnitX(u),GetUnitY(u),3,Bi,51,51,bi)
call ra(gr)
call TimerStart(Di[gr],Ai,true,function ta)
return gr
endfunction
function va takes integer gr returns nothing
call sa(gr)
call DestroyImage(ci[gr])
set ci[gr]=null
set Ci[gr]=null
endfunction
function wa takes integer gr returns nothing
if gr==null then
return
elseif(zi[gr]!=-1)then
return
endif
call va(gr)
set zi[gr]=xi
set xi=gr
endfunction
function xa takes integer gr returns nothing
if Ki[gr]==null then
set Ki[gr]=CreateTimer()
call SaveInteger(n7,6,GetHandleId((Ki[gr])),(gr))
endif
endfunction
function ya takes integer gr returns nothing
if Ki[gr]!=null then
call xu(Ki[gr])
set Ki[gr]=null
endif
endfunction
function za takes nothing returns nothing
local integer gr=(LoadInteger(n7,6,GetHandleId(GetExpiredTimer())))
set Ji[gr]=not vk[li[gr]]
call ks(gr)
endfunction
function Aa takes integer gr returns nothing
call xa(gr)
if wk[li[gr]]>.0 then
call TimerStart(Ki[gr],wk[li[gr]],vk[li[gr]],function za)
else
set Ji[gr]=true
endif
endfunction
function aa takes integer gr,integer m,integer T returns nothing
set Yg[m]=gr
set Ii[gr]=m
set li[gr]=wg[xg[Sg[m]]+T]
endfunction
function Ba takes integer gr returns nothing
set Ji[gr]=false
call TimerStart(Ki[gr],xk[li[gr]],vk[li[gr]],function za)
endfunction
function ba takes integer gr,integer ns,integer ps,integer qs returns boolean
if Ji[gr]and not((Qe[Xg[(Ii[gr])]]))and IsUnitInRange(Vg[ns],Vg[Ii[gr]],yk[li[gr]])then
return os(gr,ns,ps,qs)
endif
return false
endfunction
function Ca takes nothing returns nothing
local integer m=Hi
local integer gr=it()
call aa(gr,m,4)
call Aa(gr)
call Ut("AbilArcaneShield "+I2S(gr)+" is created.")
endfunction
function ca takes nothing returns nothing
local integer m=Hi
local integer gr=jt()
call aa(gr,m,1)
call Ut("AbilFlexibility "+I2S(gr)+" is created.")
endfunction
function Da takes integer gr returns nothing
set Mi[gr]=not Mi[gr]
endfunction
function Ea takes nothing returns nothing
local integer m=Hi
local integer gr=Yg[m]
if Mi[gr]then
set Oi[gr]=.1
set Ni[gr]=0
else
set Oi[gr]=.0
set Ni[gr]=Li
endif
endfunction
function Fa takes nothing returns nothing
local integer m=Hi
local integer gr=kt()
call aa(gr,m,2)
call Aa(gr)
call Ut("AbilShieldsUp "+I2S(gr)+" is created.")
endfunction
function Ga takes nothing returns nothing
local integer m=Hi
local integer gr=mt()
call aa(gr,m,5)
call Aa(gr)
set Ri[gr]=ua(Vg[m],yk[li[gr]])
call Ut("AbilInspiration "+I2S(gr)+" is created.")
endfunction
function Ha takes nothing returns nothing
local integer m=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),e4))and IsUnitType(Vg[m],UNIT_TYPE_GROUND)and Zg[m]and Sg[m]==Sg[Ti]and Qn[hh[m]]then
if Rg[m]!=3 then
call PA(m,Ti,Pi,.05)
elseif not Ui then
set Ui=m!=Ti
endif
endif
endfunction
function Ia takes nothing returns nothing
set Qi=Filter(function Ha)
endfunction
function la takes nothing returns nothing
local integer m=Hi
local integer gr=nt()
call aa(gr,m,3)
call Ut("AbilCommander "+I2S(gr)+" is created.")
endfunction
function Ja takes integer gr,integer id returns nothing
if id==sk[li[gr]]then
set Zi[gr]=true
call UnitRemoveAbility(Vg[Ii[gr]],id)
call UnitAddAbility(Vg[Ii[gr]],1093677143)
call AddUnitAnimationProperties(Vg[Ii[gr]],"Defend",true)
elseif id==1093677143 then
set Zi[gr]=false
call UnitRemoveAbility(Vg[Ii[gr]],id)
call UnitAddAbility(Vg[Ii[gr]],sk[li[gr]])
call AddUnitAnimationProperties(Vg[Ii[gr]],"Defend",false)
endif
endfunction
function Ka takes nothing returns nothing
local integer m=Hi
local integer gr=Yg[m]
if Zi[gr]then
set dj[gr]=Vi
if oo[po[hh[m]]+0]then
set ej[gr]=25
endif
else
call Aa(gr)
set gj[gr]=ua(Vg[m],yk[li[gr]])
endif
endfunction
function La takes nothing returns nothing
local integer m=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),e4))and IsUnitType(Vg[m],UNIT_TYPE_GROUND)and Zg[m]and Sg[m]==hj and Qn[hh[m]]and Rg[m]!=5 then
set ij=ij+1
endif
endfunction
function Ma takes nothing returns nothing
local integer i=0
loop
set Wi[i]=3
set Xi[i]=30
set i=i+1
exitwhen i==V
endloop
set Yi=Filter(function La)
endfunction
function Na takes nothing returns nothing
local integer i=0
loop
set kj[i]=.25
set i=i+1
exitwhen i==V
endloop
endfunction
function Oa takes nothing returns nothing
local integer m=Hi
local integer gr=ot()
call aa(gr,m,6)
call Aa(gr)
set mj[gr]=ma(gr,Vg[m],Bf[Tg[m]],1093677368,jj)
call Ut("AbilHealingWave "+I2S(gr)+" is created.")
endfunction
function Pa takes nothing returns nothing
local integer i=bf[tn]
set kj[i]=kj[i]+.1
endfunction
function Qa takes nothing returns nothing
local integer m=Hi
local integer gr=pt()
call aa(gr,m,7)
call Aa(gr)
call Ut("AbilDivineShield "+I2S(gr)+" is created.")
endfunction
function Ra takes nothing returns nothing
local integer m=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),e4))and Zg[m]and Sg[m]==oj and Qn[hh[m]]and not mz(m)and ry(m)<qj then
set qj=ry(m)
set pj=m
endif
endfunction
function Sa takes integer gr returns integer
set oj=Sg[Ii[gr]]
set pj=0
set qj=2.
call GroupEnumUnitsInRange(bj_lastCreatedGroup,GetUnitX(Vg[Ii[gr]]),GetUnitY(Vg[Ii[gr]]),512.,nj)
return pj
endfunction
function Ta takes nothing returns nothing
set nj=Filter(function Ra)
endfunction
function Ua takes integer gr returns nothing
set uj[gr]=CreateTimer()
call SaveInteger(n7,6,GetHandleId((uj[gr])),(gr))
endfunction
function Va takes integer gr returns nothing
call xu(uj[gr])
set uj[gr]=null
endfunction
function Wa takes nothing returns nothing
local integer m=Hi
local integer gr=qt()
call aa(gr,m,8)
call Aa(gr)
call Ua(gr)
call Ut("AbilResurrection "+I2S(gr)+" is created.")
endfunction
function Xa takes nothing returns nothing
local integer m=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),e4))and py(m)and Sg[m]==vj and Az(m)and Un[hh[m]]>xj then
set xj=Un[hh[m]]
set wj=m
endif
endfunction
function Ya takes integer gr returns nothing
local integer Za=Ii[gr]
set vj=Sg[Za]
set wj=0
set xj=.0
call GroupEnumUnitsInRange(bj_lastCreatedGroup,GetUnitX(Vg[Za]),GetUnitY(Vg[Za]),yk[li[gr]],sj)
set Za=wj
if Za!=0 then
call bz(Za,rj[bf[Sg[Ii[gr]]]])
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\Human\\Resurrect\\ResurrectTarget.mdx"),(Vg[Za]),("origin")))
if(H9[I9[(ag[Bg[Sg[Za]]+3])]+(1)])then
call da(Za,Ii[gr])
endif
call Ba(gr)
else
call TimerStart(uj[gr],.25,false,tj)
endif
endfunction
function dB takes nothing returns nothing
call Ya(((LoadInteger(n7,6,GetHandleId(GetExpiredTimer())))))
endfunction
function eB takes nothing returns nothing
local integer i=0
loop
set rj[i]=.5
set i=i+1
exitwhen i==V
endloop
set sj=Filter(function Xa)
set tj=function dB
endfunction
function fB takes nothing returns nothing
local integer i=bf[tn]
set rj[i]=.75
endfunction
function gB takes integer gr returns nothing
set Pj[gr]=CreateTimer()
call SaveInteger(n7,6,GetHandleId((Pj[gr])),(gr))
endfunction
function hB takes integer gr returns nothing
call xu(Pj[gr])
set Pj[gr]=null
endfunction
function iB takes integer f,real x,real y,boolean jB returns nothing
local integer c=Sv(f,x,y)
set jB=not jB
set Bd[(c)]=(jB)
set Bd[(Cd[c])]=(jB)
set Bd[(Dd[Cd[c]])]=(jB)
set Bd[(Dd[c])]=(jB)
endfunction
function kB takes unit u,integer p,real x,real y,integer T returns nothing
set aj=u
set Bj=p
set bj=x
set Cj=y
call TriggerEvaluate(qp[(T)])
endfunction
function mB takes unit u returns nothing
local real x=GetUnitX(u)
local real y=GetUnitY(u)
local integer p=(z4[GetPlayerId((GetOwningPlayer(u)))])
local integer f=If[p]
local integer T=GetUnitTypeId(u)-1949315120
if x>Ud[f]or x<Vd[f]or y>320. or y<k4 then
call wx((p),n8,Y9[T9]+"Error|r: "+("it is not allowed to build here."))
call RemoveUnit(u)
return
endif
if Zt(R2I(x-Vd[f]),$80)or Zt(R2I(y-k4),$80)then
call wx((p),n8,Y9[T9]+"Error|r: "+("towers must be build precisely on the grid."))
call RemoveUnit(u)
return
endif
if(Kf[(p)]-(ep[(T)])>=0)then
call iB(f,x,y,true)
call Tv(f)
if ie[f]then
call Bx((p),-(ep[(T)]))
call kB(u,p,x,y,T)
else
call iB(f,x,y,false)
call wx((p),n8,Y9[T9]+"Error|r: "+("this tower will hinder minions' movement."))
call RemoveUnit(u)
endif
else
call wx((p),o8,Y9[T9]+"Error|r: not enough Gold.")
call RemoveUnit(u)
endif
endfunction
function nB takes integer gr,integer T returns nothing
set Ej[gr]=T
set Dj[gr]=aj
set cj[gr]=Bj
set lj[gr]=bj
set Jj[gr]=Cj
set Fj[gr]=(T)
set Kj[gr]=ep[Fj[gr]]
set Nj[gr]=(T)
call UnitAddAbility(Dj[gr],Xo[Fj[gr]])
call UnitAddAbility(Dj[gr],Yo[Fj[gr]])
call Qu(kg[cj[gr]],gr)
call SetUnitUserData(Dj[gr],gr)
endfunction
function oB takes integer gr,real Au,integer au,integer Bu,integer bu,boolean pB returns image
call cu(Bf[cj[gr]],Au,lj[gr],Jj[gr],2,au,Bu,bu,Y4)
call ShowImage(bj_lastCreatedImage,pB and IsUnitSelected(Dj[gr],Bf[cj[gr]]))
return bj_lastCreatedImage
endfunction
function qB takes integer gr returns nothing
set Mj[gr]=Ro[Nj[gr]]
set Xj[gr]=kv(Dj[gr],Oo[Nj[gr]])
set Yj[gr]=kv(Dj[gr],1227894833)
call gB(gr)
set Oj[gr]=gA(gr)
set Qj[gr]=oB(gr,Qo[Nj[gr]],51,$CC,51,true)
endfunction
function rB takes integer gr returns nothing
call hB(gr)
call qA(Oj[gr])
call DestroyImage(Qj[gr])
set Qj[gr]=null
set Xj[gr]=null
set Yj[gr]=null
endfunction
function sB takes integer gr returns real
return .0
endfunction
function tB takes integer gr returns real
return .0
endfunction
function uB takes integer gr returns real
local real qw=Pr(gr)+Qr(gr)
return qw+qw*Rr(gr)
endfunction
function vB takes integer gr returns nothing
endfunction
function wB takes integer gr,integer ns,real Gs,integer qs,boolean xB returns boolean
return sz(ns,gr,Gs,qs,xB)
endfunction
function yB takes integer gr returns nothing
call SetItemCharges(Xj[gr],eu(uB(gr)))
endfunction
function zB takes integer gr,integer m returns boolean
local real r=uB(gr)
call SetItemCharges(Xj[gr],eu(r))
return wB(gr,m,r,Oo[Nj[gr]],So[Nj[gr]])
endfunction
function AB takes integer gr,integer m returns boolean
return wB(gr,m,uB(gr),Oo[Nj[gr]],So[Nj[gr]])
endfunction
function aB takes integer gr returns nothing
call TimerStart(Pj[gr],No[Nj[gr]],false,gk)
endfunction
function BB takes integer gr,integer Vr,integer ps returns boolean
local integer bB=0
local integer m
loop
set m=vA(Vr)
call Tt(1,m,gr,Oo[Nj[gr]])
if(yf[Xg[(m)]])then
exitwhen(di[(Vr)]==0)
else
call oA(ps,Vg[m])
set bB=bB+1
if(di[(Vr)]==0)or bB==Mj[gr]then
return true
endif
endif
endloop
return false
endfunction
function CB takes integer gr returns nothing
local integer Vr=aA(gr)
if Vr!=0 then
call Ur(gr,Vr)
call zA(Vr)
call Xs(Vr)
else
call TimerStart(Pj[(gr)],O4,false,gk)
endif
endfunction
function cB takes nothing returns nothing
call CB(((LoadInteger(n7,6,GetHandleId(GetExpiredTimer())))))
endfunction
function DB takes integer gr returns nothing
set Hj[gr]=Vo[Fj[gr]]*Wo[Fj[gr]]
set Ij[gr]=Wo[Fj[gr]]
call SetUnitScale(Dj[gr],Ij[gr],Ij[gr],Ij[gr])
if Zo[Fj[gr]]then
call qB(gr)
endif
set Sj[gr]=hp[Fj[gr]]
if Sj[gr]then
set Wj[gr]=np[Fj[gr]]
set Vj[gr]=np[Fj[gr]]
set Zj[gr]=kv(Dj[gr],1227894839)
if ip[Fj[gr]]then
set dk[gr]=kv(Dj[gr],1227894838)
else
set dk[gr]=kv(Dj[gr],1227894842)
call UnitAddAbility(Dj[gr],1093677129)
endif
call SetItemCharges(dk[gr],Wj[gr])
endif
if fp[Fj[gr]]then
set Lj[gr]=.5
call UnitAddAbility(Dj[gr],1093677119)
else
set Lj[gr]=.0
call UnitAddAbility(Dj[gr],1093677121)
endif
set Bk=gr
call TriggerEvaluate(dn[(dp[Fj[gr]])])
set Gj[gr]=true
if Zo[Fj[gr]]then
call yB(gr)
call CB(gr)
endif
endfunction
function EB takes integer gr returns nothing
call Ru(kg[cj[gr]],gr)
if Zo[Fj[gr]]and Gj[gr]then
call rB(gr)
endif
if ek[gr]!=0 then
call yr(ek[gr])
endif
set Zj[gr]=null
set dk[gr]=null
call SetUnitUserData(Dj[gr],0)
set Dj[gr]=null
call ds(gr)
call Ut("Tower "+I2S(gr)+" is deleted.")
endfunction
function FB takes integer gr returns nothing
call RemoveUnit(Dj[gr])
call EB(gr)
endfunction
function GB takes integer gr returns nothing
call DestroyEffect(AddSpecialEffect(("Objects\\Spawnmodels\\Human\\HCancelDeath\\HCancelDeath.mdx"),((lj[gr])*1.),((Jj[gr])*1.)))
if Lj[gr]>.0 then
set Kj[gr]=eu(Kj[gr]*Lj[gr])
call Bx(cj[gr],Kj[gr])
call Ex(cj[gr],I2S(Kj[gr]),Dj[gr])
endif
call iB(If[cj[gr]],lj[gr],Jj[gr],false)
call Tv(If[cj[gr]])
call FB(gr)
endfunction
function HB takes integer gr returns nothing
local boolean Yx=not ip[Fj[gr]]
set Uj[gr]=Uj[gr]+1
call SetItemCharges(Zj[gr],Uj[gr])
call wr(ek[gr])
if Zo[Fj[gr]]then
call Sr(gr)
call yB(gr)
endif
if Yx then
set Kj[gr]=Kj[gr]+Wj[gr]
endif
if Uj[gr]<pp[Fj[gr]]then
if Uj[gr]<op[Fj[gr]]then
set Wj[gr]=jp[Fj[gr]]*Wj[gr]+(Uj[gr]+1)*kp[Fj[gr]]+mp[Fj[gr]]
endif
if Yx then
call SetItemCharges(dk[gr],Wj[gr])
else
set Vj[gr]=Vj[gr]+Wj[gr]
endif
else
if Yx then
call UnitRemoveAbility(Dj[gr],1093677129)
endif
call ov(Dj[gr],dk[gr])
set Sj[gr]=false
set Wj[gr]=0
set Vj[gr]=0
endif
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\Items\\AIlm\\AIlmTarget.mdx"),(Dj[gr]),("origin")))
endfunction
function IB takes integer gr,integer Gs returns nothing
set Tj[gr]=Tj[gr]+Gs
loop
exitwhen Tj[gr]<Vj[gr]or(not Sj[gr])
call HB(gr)
endloop
if Sj[gr]then
call SetItemCharges(dk[gr],Vj[gr]-Tj[gr])
endif
endfunction
function lB takes nothing returns nothing
local integer t=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),Z))and Gj[t]and ip[Fj[t]]and Sj[t]then
call IB(t,ik)
endif
endfunction
function JB takes integer m,integer tz returns nothing
local real x=GetUnitX(Vg[m])
local real y=GetUnitY(Vg[m])
set ik=do[hh[m]]
if ip[Fj[tz]]and Sj[tz]and not IsUnitInRangeXY(Dj[tz],x,y,384.)then
call IB(tz,ik)
endif
call GroupEnumUnitsInRange(bj_lastCreatedGroup,x,y,384.,hk)
endfunction
function KB takes integer gr,integer m returns nothing
set Rj[gr]=Rj[gr]+1
call SetItemCharges(Yj[gr],Rj[gr])
call zx(cj[gr])
if do[hh[m]]>0 then
call JB(m,gr)
endif
endfunction
function LB takes nothing returns boolean
local integer t=GetUnitUserData(GetFilterUnit())
return((IsUnitType(GetFilterUnit(),Z))and Gj[t]and not ip[Fj[t]]and Sj[t]and(Kf[(cj[t])]-(Wj[t])>=0))!=null
endfunction
function MB takes integer gr returns nothing
local integer array NB
local integer OB
local integer PB
local integer n=-1
local integer i=-1
local group g=CreateGroup()
local unit u
call GroupEnumUnitsSelected(g,Bf[cj[gr]],jk)
loop
set u=FirstOfGroup(g)
exitwhen u==null
call GroupRemoveUnit(g,u)
set OB=GetUnitUserData(u)
set n=n+1
set NB[n]=OB
loop
exitwhen i<0 or Wj[NB[i]]<=Wj[OB]
set PB=NB[i]
set NB[i]=OB
set NB[i+1]=PB
set i=i-1
endloop
set i=n
endloop
call DestroyGroup(g)
set g=null
if n>-1 then
loop
set OB=NB[n]
exitwhen not bx(cj[gr],Wj[OB])
call HB(OB)
set n=n-1
exitwhen n==-1
endloop
else
call wx((cj[gr]),o8,Y9[T9]+"Error|r: not enough Gold.")
endif
endfunction
function QB takes nothing returns nothing
set gk=function cB
set hk=Filter(function lB)
set jk=Filter(function LB)
endfunction
function RB takes integer gr returns nothing
set Gh[gr]=(hw(Xg[(bh[gr])],(gr)))
endfunction
function SB takes integer m,integer GA returns nothing
local integer gr
if(yf[Xg[(m)]])then
return
endif
set gr=(LoadInteger(n7,Wg[(m)],(1)))
if gr==0 then
set gr=rt()
call Tz(gr,m,Dj[GA],cj[GA],1)
call RB(gr)
elseif Ch[gr]!=Dj[GA]then
set Ch[gr]=Dj[GA]
endif
call fA(gr,ni[Dh[gr]])
endfunction
function TB takes integer gr,real UB returns nothing
set Gh[gr]=Uy(bh[gr],gr,UB)
endfunction
function VB takes integer m,integer GA,real UB returns nothing
local integer gr
if bs(m)or(yf[Xg[(m)]])then
return
endif
set gr=(LoadInteger(n7,Wg[(m)],(2)))
if gr==0 then
set gr=tt()
call Tz(gr,m,Dj[GA],cj[GA],2)
call TB(gr,UB)
elseif Ch[gr]!=Dj[GA]then
set Ch[gr]=Dj[GA]
endif
call eA(gr)
endfunction
function WB takes integer gr,integer By returns nothing
set Gh[gr]=Yy(bh[gr],gr,By)
endfunction
function XB takes integer m,integer GA,integer By returns nothing
local integer gr
if(yf[Xg[(m)]])then
return
endif
set gr=(LoadInteger(n7,Wg[(m)],(3)))
if gr==0 then
set gr=ut()
call Tz(gr,m,Dj[GA],cj[GA],3)
call WB(gr,By)
elseif Ch[gr]!=Dj[GA]then
set Ch[gr]=Dj[GA]
endif
call eA(gr)
endfunction
function YB takes integer gr,real UB returns nothing
set Gh[gr]=Uy(bh[gr],gr,UB)
endfunction
function ZB takes integer m,integer GA,real UB returns nothing
local integer gr
if bs(m)or(yf[Xg[(m)]])then
return
endif
set gr=(LoadInteger(n7,Wg[(m)],(4)))
if gr==0 then
set gr=vt()
call Tz(gr,m,Dj[GA],cj[GA],4)
call YB(gr,UB)
elseif Ch[gr]!=Dj[GA]then
set Ch[gr]=Dj[GA]
endif
call eA(gr)
endfunction
function fb takes integer gr,integer By returns nothing
set Gh[gr]=Yy(bh[gr],gr,By)
endfunction
function gb takes integer m,integer GA,integer By returns nothing
local integer gr=(LoadInteger(n7,Wg[(m)],(6)))
if gr==0 then
set gr=xt()
call Tz(gr,m,Dj[GA],cj[GA],6)
call fb(gr,By)
elseif Ch[gr]!=Dj[GA]then
set Ch[gr]=Dj[GA]
endif
call eA(gr)
endfunction
function hb takes integer gr,real JA returns nothing
set Gh[gr]=Ty(bh[gr],gr,JA)
endfunction
function ib takes integer m,integer GA,real JA returns nothing
local integer gr=(LoadInteger(n7,Wg[(m)],(V4)))
if gr==0 then
set gr=yt()
call Tz(gr,m,Dj[GA],cj[GA],V4)
call hb(gr,JA)
elseif Ch[gr]!=Dj[GA]then
set Ch[gr]=Dj[GA]
endif
call eA(gr)
endfunction
function jb takes nothing returns nothing
local integer gr=zt()
call fy(gr,4,Pg)
call Ut("Wizard "+I2S(gr)+" is created.")
endfunction
function kb takes nothing returns nothing
local integer d=yg[zg[Qg]+9]
set pi[d]=pi[d]+.5
set qi[d]=pi[d]
endfunction
function mb takes integer m,integer p returns integer
local integer gr=At()
set Sg[gr]=p
set Vg[gr]=CreateUnit(Bf[p],Ln[g7[1]],su(Wd[If[p]]),tu(Wd[If[p]]),270.)
call UnitAddAbility(Vg[gr],1098282348)
call UnitMakeAbilityPermanent(Vg[gr],true,1098282348)
call SetUnitUserData(Vg[gr],gr)
set Rg[gr]=1
set Wg[gr]=GetHandleId(Vg[gr])
set hh[gr]=sg[tg[p]+1]
call UnitAddAbility(Vg[gr],1093677099)
call UnitMakeAbilityPermanent(Vg[gr],true,1093677099)
call UnitAddAbility(Vg[gr],Nn[g7[1]])
call UnitMakeAbilityPermanent(Vg[gr],true,Nn[g7[1]])
call UnitAddAbility(Vg[gr],xo[yo[i7[1]]])
call UnitMakeAbilityPermanent(Vg[gr],true,xo[yo[i7[1]]])
call UnitAddAbility(Vg[gr],xo[yo[i7[1]]+1])
call UnitMakeAbilityPermanent(Vg[gr],true,xo[yo[i7[1]]+1])
call UnitAddAbility(Vg[gr],xo[yo[i7[1]]+2])
call UnitMakeAbilityPermanent(Vg[gr],true,xo[yo[i7[1]]+2])
set Hi=gr
call TriggerEvaluate(d7[Sn[hh[gr]]])
call UnitAddAbility(Vg[gr],1093677122)
call UnitMakeAbilityPermanent(Vg[gr],true,1093677122)
call UnitAddAbility(Vg[gr],sk[Z4[Sn[hh[gr]]]])
call UnitMakeAbilityPermanent(Vg[gr],true,sk[Z4[Sn[hh[gr]]]])
call Su(mg[p],gr)
set mk[gr]=m
call Ut("ConscriptTwin "+I2S(gr)+" is created.")
return gr
endfunction
function nb takes nothing returns nothing
local integer gr=At()
call fy(gr,1,Pg)
if oo[po[hh[gr]]+2]then
set mk[gr]=mb(gr,Sg[gr])
else
set mk[gr]=0
endif
call Ut("Conscript "+I2S(gr)+" is created.")
endfunction
function ob takes integer gr returns nothing
call Tu(mg[Sg[gr]],gr)
call lx(Sg[gr],Un[hh[gr]])
call DestroyEffect(AddSpecialEffect(("Abilities\\Spells\\Other\\Transmute\\PileofGold.mdx"),((GetUnitX(Vg[gr]))*1.),((GetUnitY(Vg[gr]))*1.)))
call RemoveUnit(Vg[gr])
call Ls(gr)
call Ut("ConscriptTwin "+I2S(gr)+" is sold.")
endfunction
function pb takes nothing returns nothing
call Oz(Qg,1,.1)
endfunction
function qb takes nothing returns nothing
call Qz(Qg,1,kk)
endfunction
function rb takes nothing returns nothing
local integer sb=Qg
local integer tb=h9[mg[sb]]
local real ub=Un[sg[tg[sb]+1]]-.5
local integer a=0
local integer m
set Un[sg[tg[sb]+1]]=.5
loop
exitwhen a>tb
set m=(f9[g9[(mg[sb])]+(a)])
if Rg[m]==1 then
set mk[m]=mb(m,sb)
endif
set a=a+1
endloop
set a=0
set tb=D9[pg[sb]]
loop
exitwhen a>tb
set m=(C9[c9[(pg[sb])]+(a)])
if Rg[m]==1 and Zg[m]then
call lx(sb,ub)
endif
set a=a+1
endloop
set a=0
set tb=o9[ng[Df[sb]]]
loop
exitwhen a>tb
set m=(m9[n9[(ng[Df[sb]])]+(a)])
if Rg[m]==1 then
call lx(sb,ub)
endif
set a=a+1
endloop
endfunction
function vb takes nothing returns nothing
local integer gr=at()
call fy(gr,2,Pg)
call Ut("Footman "+I2S(gr)+" is created.")
endfunction
function wb takes nothing returns nothing
call Qz(Qg,2,30)
endfunction
function xb takes nothing returns nothing
local integer gr=Bt()
call fy(gr,3,Pg)
call Ut("Knight "+I2S(gr)+" is created.")
endfunction
function yb takes nothing returns nothing
call Lz(Qg,3,.5*go[sg[tg[Qg]+3]])
endfunction
function zb takes nothing returns nothing
call Pz(Qg,3,ok)
endfunction
function Ab takes nothing returns nothing
local integer d=yg[zg[Qg]+R4]
set pi[d]=1.
set qi[d]=1.
endfunction
function ab takes nothing returns nothing
local integer gr=bt()
call fy(gr,5,Pg)
call Ut("Captain "+I2S(gr)+" is created.")
endfunction
function Bb takes nothing returns nothing
local integer sb=Qg
local integer a=0
local integer m
local integer b
loop
exitwhen a>D9[pg[sb]]
set m=(C9[c9[(pg[sb])]+(a)])
set b=Yg[m]
if Rg[m]==5 and Zi[b]then
set ej[b]=25
if Zg[m]then
call ly(m)
endif
endif
set a=a+1
endloop
endfunction
function bb takes nothing returns nothing
local integer i=bf[Qg]
set Wi[i]=5
set Xi[i]=50
endfunction
function Cb takes nothing returns nothing
local integer gr=Ct()
call gy(gr,6,Pg)
call Ut("Hero Paladin "+I2S(gr)+" is created.")
endfunction
function cb takes nothing returns nothing
call Lz(Qg,6,1.*go[sg[tg[Qg]+6]])
endfunction
function Db takes nothing returns nothing
call Pz(Qg,6,25)
endfunction
function Eb takes nothing returns nothing
call Nz(Qg,6,1.*ho[sg[tg[Qg]+6]])
endfunction
function Fb takes integer T,integer Gb,boolean Hb,boolean Ib,boolean lb,real Jb,real cd,real Au returns nothing
set Z4[T]=zr()
set sk[Z4[T]]=Gb
set tk[Z4[T]]=Hb
set uk[Z4[T]]=Ib
set vk[Z4[T]]=lb
set wk[Z4[T]]=Jb
if lb then
set xk[Z4[T]]=Jb
else
set xk[Z4[T]]=cd
endif
set yk[Z4[T]]=Au
endfunction
function Kb takes integer T,code Lb,code Mb returns nothing
if Lb!=null then
set d7[T]=CreateTrigger()
call TriggerAddCondition(d7[T],Filter(Lb))
endif
if Mb!=null then
set e7[T]=CreateTrigger()
call TriggerAddCondition(e7[T],Filter(Mb))
endif
endfunction
function Ob takes integer gr returns nothing
set Jk[gr]=CreateTimer()
call SaveInteger(n7,6,GetHandleId((Jk[gr])),(gr))
endfunction
function Pb takes integer gr returns nothing
call xu(Jk[gr])
set Jk[gr]=null
endfunction
function Qb takes nothing returns nothing
local integer m=Bk
local integer gr=ct(m,1)
set Ek[gr]=kv(Dj[m],1227894835)
set lk[gr]=Oj[m]
call Ob(gr)
call hA(lk[(gr)],1093677096,ck)
call Ut("AbilFocusedAttack "+I2S(gr)+" is created.")
endfunction
function Rb takes integer gr returns real
if Gk[gr]<=.0 then
return Hk[gr]
endif
return .0
endfunction
function Sb takes integer gr,real r returns nothing
set Gk[gr]=Gk[gr]-r
if Gk[gr]<=.0 then
set Gk[gr]=.0
call hA(lk[(gr)],1093677096,ck)
call yB(Ck[gr])
call PauseTimer(Jk[gr])
endif
call SetItemCharges(Ek[gr],fu(Gk[gr]))
endfunction
function Tb takes nothing returns nothing
call Sb(((LoadInteger(n7,6,GetHandleId(GetExpiredTimer())))),.2)
endfunction
function Ub takes integer gr returns nothing
if Gk[gr]<=.0 then
set Gk[gr]=Fk[gr]
call yB(Ck[gr])
call SetItemCharges(Ek[gr],fu(Gk[gr]))
call TimerStart(Jk[gr],.2,true,Dk)
else
call Sb(gr,Ik[gr])
endif
endfunction
function Vb takes integer gr,integer id returns nothing
if id==1093677096 then
call kA(Oj[Ck[gr]])
endif
endfunction
function Wb takes nothing returns nothing
set Dk=function Tb
endfunction
function Xb takes nothing returns nothing
local integer m=Bk
local integer gr=Dt(m,2)
call Ut("AbilAcid "+I2S(gr)+" is created.")
endfunction
function Yb takes integer gr,integer m returns nothing
local integer Gz=Uj[Ck[gr]]
call SB(m,Ck[gr])
if Gz>=Xm[bk[gr]]then
call VB(m,Ck[gr],Kk)
endif
if Gz>=Ym[bk[gr]]then
call XB(m,Ck[gr],Lk)
endif
endfunction
function Zb takes nothing returns nothing
local integer m=Bk
local integer gr=Et(m,3)
call Ut("AbilLightningAttack "+I2S(gr)+" is created.")
endfunction
function dC takes integer gr,integer Vr returns boolean
if BB(Ck[gr],Vr,Oj[Ck[gr]])then
if Uj[Ck[gr]]>=Ym[bk[gr]]and(di[(Vr)]!=0)then
call BB(Ck[gr],Vr,Ok[gr])
endif
return true
endif
return false
endfunction
function eC takes integer gr returns nothing
set Wk[gr]=CreateTimer()
call SaveInteger(n7,6,GetHandleId((Wk[gr])),(gr))
endfunction
function fC takes integer gr returns nothing
call xu(Wk[gr])
set Wk[gr]=null
endfunction
function gC takes nothing returns nothing
local integer m=Bk
local integer gr=Ft(m,4)
set Qk[gr]=CreateUnit(Bf[cj[m]],1932537904,lj[m],Jj[m],.0)
call SetUnitPathing(Qk[gr],false)
call SetUnitFlyHeight(Qk[gr],Hj[m],.0)
call SetUnitUserData(Qk[gr],m)
call eC(gr)
set Vk[gr]=kv(Dj[m],1227894845)
call Ut("AbilEnhancedWeapon "+I2S(gr)+" is created.")
endfunction
function hC takes nothing returns nothing
local integer gr=(LoadInteger(n7,6,GetHandleId(GetExpiredTimer())))
set Tk[gr]=.0
set Rk[gr]=null
call yB(Ck[gr])
endfunction
function iC takes integer gr,integer m returns nothing
if Uj[Ck[gr]]>=Xm[bk[gr]]and Rk[gr]==Vg[m]then
set Tk[gr]=Tk[gr]+.25
if Tk[gr]>1. then
set Tk[gr]=1.
endif
call TimerStart(Wk[gr],3.,false,function hC)
else
set Tk[gr]=.0
endif
set Rk[gr]=Vg[m]
call zB(Ck[gr],m)
endfunction
function jC takes nothing returns nothing
local integer gr=(LoadInteger(n7,6,GetHandleId(GetExpiredTimer())))
if UnitAlive(Rk[gr])then
call oA(Pk[gr],Rk[gr])
set Uk[gr]=Uk[gr]+1
if Uk[gr]==4 then
call PauseTimer(Wk[gr])
endif
else
call PauseTimer(Wk[gr])
endif
endfunction
function kC takes integer gr,integer m returns nothing
set Rk[gr]=Vg[m]
call oA(Pk[gr],Rk[gr])
set Uk[gr]=1
call TimerStart(Wk[gr],.125,true,function jC)
endfunction
function mC takes integer gr,integer m returns nothing
if Sk[gr]then
call kC(gr,m)
else
call iC(gr,m)
endif
endfunction
function s__AbilEnhancedWeapon_filterToggleModeSelected takes nothing returns nothing
endfunction
function nC takes integer gr returns nothing
set Zk[gr]=CreateTimer()
call SaveInteger(n7,6,GetHandleId((Zk[gr])),(gr))
endfunction
function oC takes integer gr returns nothing
call xu(Zk[gr])
set Zk[gr]=null
endfunction
function pC takes nothing returns nothing
local integer gr=(LoadInteger(n7,6,GetHandleId(GetExpiredTimer())))
call Bx(Yk[gr],Xk[gr])
call Ex(Yk[gr],I2S(Xk[gr]),Dj[Ck[gr]])
endfunction
function qC takes nothing returns nothing
local integer m=Bk
local integer gr=Gt(m,5)
set Yk[gr]=cj[m]
call nC(gr)
call TimerStart(Zk[gr],1.,true,function pC)
call Ut("AbilGoldMining "+I2S(gr)+" is created.")
endfunction
function rC takes integer gr returns nothing
set um[gr]=CreateTimer()
call SaveInteger(n7,6,GetHandleId((um[gr])),(gr))
endfunction
function sC takes integer gr returns nothing
call xu(um[gr])
set um[gr]=null
endfunction
function tC takes nothing returns nothing
local integer m=Bk
local integer gr=Ht(m,6)
call rC(gr)
set mm[gr]=oB(Ck[gr],km[gr],dm,em,fm,false)
set tm[gr]=cj[m]
call UnitAddAbility(Dj[Ck[gr]],1093677127)
call TimerStart(um[gr],5.,true,gm)
call Ut("AbilExpGenerator "+I2S(gr)+" is created.")
endfunction
function uC takes integer gr returns nothing
if qm[gr]==null then
set qm[gr]=uu(Bf[tm[gr]],"Abilities\\Spells\\Other\\Aneu\\AneuCaster.mdx",pm[gr],"overhead")
endif
endfunction
function vC takes integer gr returns nothing
if qm[gr]!=null then
call DestroyEffect(qm[gr])
set qm[gr]=null
endif
endfunction
function wC takes integer gr,integer Dv returns nothing
if im[gr]==0 then
call ShowImage(mm[gr],false)
call UnitRemoveAbility(Dj[Ck[gr]],1093677106)
elseif im[gr]==2 then
call UnitRemoveAbility(Dj[Ck[gr]],1093677111)
else
set pm[gr]=null
call vC(gr)
call UnitRemoveAbility(Dj[Ck[gr]],1093677138)
endif
if Dv==0 then
call ShowImage(mm[gr],IsUnitSelected(Dj[Ck[gr]],Bf[tm[gr]]))
call UnitAddAbility(Dj[Ck[gr]],1093677106)
elseif Dv==2 then
call UnitAddAbility(Dj[Ck[gr]],1093677111)
else
set rm[gr]=im[gr]
call UnitAddAbility(Dj[Ck[gr]],1093677138)
endif
set im[gr]=Dv
endfunction
function xC takes integer gr,integer t returns nothing
if pm[gr]!=Dj[t]then
if im[gr]==1 then
call DestroyEffect(qm[gr])
else
call wC(gr,1)
endif
set pm[gr]=Dj[t]
set qm[gr]=uu(Bf[tm[gr]],"Abilities\\Spells\\Other\\Aneu\\AneuCaster.mdx",Dj[t],"overhead")
endif
endfunction
function yC takes nothing returns nothing
local integer t=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),Z))and Gj[t]and dq[ek[t]]==91 then
call xC(ek[t],wm)
endif
endfunction
function zC takes integer gr,integer t returns nothing
if not Gj[t]then
call wx((tm[gr]),n8,Y9[T9]+"Error|r: "+("this tower is constructing."))
return
elseif not ip[Fj[t]]then
call wx((tm[gr]),n8,Y9[T9]+"Error|r: "+("this tower does not use experience."))
return
elseif not Sj[t]then
call wx((tm[gr]),n8,Y9[T9]+"Error|r: "+("this tower can no longer be upgraded."))
return
endif
set wm=t
call GroupEnumUnitsSelected(bj_lastCreatedGroup,Bf[tm[gr]],vm)
endfunction
function AC takes nothing returns nothing
local integer t=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),Z))and Gj[t]and dq[ek[t]]==91 and im[(ek[t])]==ym then
call wC((ek[t]),zm)
endif
endfunction
function aC takes integer gr,integer id returns nothing
if id==1093677106 then
set zm=2
elseif id==1093677111 then
set zm=0
elseif id==1093677138 then
set zm=rm[gr]
else
return
endif
set ym=im[gr]
call GroupEnumUnitsSelected(bj_lastCreatedGroup,Bf[tm[gr]],xm)
endfunction
function BC takes integer gr returns nothing
call Fx(tm[gr],sm[gr])
call Ix(tm[gr],I2S(sm[gr]),Dj[Ck[gr]])
endfunction
function bC takes nothing returns nothing
local integer t=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),Z))and Gj[t]and ip[Fj[t]]and Sj[t]then
call IB(t,R2I(am*Wj[t]))
set Bm=false
endif
endfunction
function CC takes integer gr returns nothing
set am=jm[gr]
set Bm=true
call GroupEnumUnitsInRange(bj_lastCreatedGroup,lj[Ck[gr]],Jj[Ck[gr]],km[gr],Am)
if Bm then
call wC(gr,2)
call BC(gr)
endif
endfunction
function cC takes integer gr returns nothing
local integer t=GetUnitUserData(pm[gr])
call IB(t,Vt(nm[gr],R2I(om[gr]*Wj[t])))
endfunction
function DC takes integer gr returns nothing
if im[gr]==0 then
call ShowImage(mm[gr],true)
elseif im[gr]==1 and UnitAlive(pm[gr])then
call uC(gr)
endif
endfunction
function EC takes integer gr returns nothing
if im[gr]==0 then
call ShowImage(mm[gr],false)
elseif im[gr]==1 and UnitAlive(pm[gr])then
call vC(gr)
endif
endfunction
function FC takes integer gr returns boolean
if UnitAlive(pm[gr])and Sj[(GetUnitUserData(pm[gr]))]then
return true
endif
call wC(gr,rm[gr])
return false
endfunction
function GC takes integer gr returns nothing
if im[gr]==1 and FC(gr)then
call cC(gr)
elseif im[gr]==0 then
call CC(gr)
else
call BC(gr)
endif
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\NightElf\\MoonWell\\MoonWellCasterArt.mdx"),(Dj[Ck[gr]]),("origin")))
endfunction
function HC takes nothing returns nothing
call GC(((LoadInteger(n7,6,GetHandleId(GetExpiredTimer())))))
endfunction
function IC takes nothing returns nothing
local integer gr=(LoadInteger(n7,6,GetHandleId(GetExpiredTimer())))
call GC(gr)
if Uj[Ck[gr]]>=Zm[bk[gr]]then
call TimerStart(um[gr],3.,true,hm)
endif
endfunction
function lC takes nothing returns nothing
set gm=function IC
set hm=function HC
set Am=Filter(function bC)
set vm=Filter(function yC)
set xm=Filter(function AC)
endfunction
function JC takes nothing returns nothing
local integer m=Bk
local integer gr=It(m,7)
call Ut("AbilBloodlust "+I2S(gr)+" is created.")
endfunction
function KC takes integer gr returns nothing
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\Undead\\DeathPact\\DeathPactTarget.mdx"),(Dj[Ck[gr]]),("overhead")))
set cm[gr]=cm[gr]+.5
if Rj[Ck[gr]]==Xm[bk[gr]]then
set cm[gr]=cm[gr]+5.
elseif Rj[Ck[gr]]==Ym[bk[gr]]then
set bm[gr]=2.
endif
endfunction
function LC takes integer gr,integer m returns nothing
set Cm[gr]=(1.-ry(m))*bm[gr]
if AB(Ck[gr],m)then
set Cm[gr]=.0
else
set Cm[gr]=.0
call yB(Ck[gr])
endif
endfunction
function MC takes integer gr returns nothing
set Im[gr]=CreateTimer()
call SaveInteger(n7,6,GetHandleId((Im[gr])),(gr))
endfunction
function NC takes integer gr returns nothing
call xu(Im[gr])
set Im[gr]=null
endfunction
function OC takes integer gr returns nothing
set lm[gr]=CreateTimer()
call SaveInteger(n7,6,GetHandleId((lm[gr])),(gr))
endfunction
function PC takes integer gr returns nothing
call xu(lm[gr])
set lm[gr]=null
endfunction
function QC takes nothing returns nothing
local integer m=Bk
local integer gr=lt(m,8)
call MC(gr)
call OC(gr)
call Ut("AbilShockwave "+I2S(gr)+" is created.")
endfunction
function RC takes integer gr returns nothing
if Uj[Ck[gr]]>=Xm[bk[gr]]then
set Hm[gr]=Hm[gr]+.1
if Hm[gr]>1. then
set Hm[gr]=1.
endif
call TimerStart(Im[gr],1.5,false,Dm)
call yB(Ck[gr])
endif
endfunction
function SC takes integer gr,real cx,real cy returns nothing
if Hm[gr]!=.0 then
set Hm[gr]=.0
call yB(Ck[gr])
endif
call nA(Oj[Ck[gr]],cx,cy)
if Uj[Ck[gr]]>=Ym[bk[gr]]then
set Fm[gr]=cx
set Gm[gr]=cy
call TimerStart(lm[gr],.4,false,Em)
endif
endfunction
function TC takes nothing returns nothing
local integer gr=(LoadInteger(n7,6,GetHandleId(GetExpiredTimer())))
call nA(Oj[Ck[gr]],Fm[gr],Gm[gr])
endfunction
function UC takes nothing returns nothing
local integer gr=(LoadInteger(n7,6,GetHandleId(GetExpiredTimer())))
set Hm[gr]=.0
call yB(Ck[gr])
endfunction
function VC takes nothing returns nothing
set Dm=function UC
set Em=function TC
endfunction
function WC takes integer gr returns nothing
set Vm[gr]=CreateTimer()
call SaveInteger(n7,6,GetHandleId((Vm[gr])),(gr))
endfunction
function XC takes integer gr returns nothing
call xu(Vm[gr])
set Vm[gr]=null
endfunction
function YC takes integer gr,integer m returns nothing
local integer Gz=Uj[Ck[gr]]
set Om[gr]=Om[gr]-30
call SetItemCharges(Tm[gr],Om[gr])
call zy(m,Pm[gr],"Effects\\RestoreHealthGreen.mdx","origin")
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\NightElf\\MoonWell\\MoonWellCasterArt.mdx"),(Dj[Ck[gr]]),("origin")))
if Gz>=Xm[bk[gr]]then
call gb(m,Ck[gr],25)
if Gz>=Ym[bk[gr]]then
call ib(m,Ck[gr],.1*fh[m])
endif
endif
endfunction
function ZC takes integer gr returns nothing
local integer a=0
local real dc=.0
local real qw=.0
local integer ns=0
local boolean ec=true
local integer fc=pg[Sm[gr]]
loop
exitwhen a>u9[fc]
if Zg[(s9[t9[(fc)]+(a)])]then
set qw=ry((s9[t9[(fc)]+(a)]))
if ec or qw<dc then
set dc=qw
set ns=(s9[t9[(fc)]+(a)])
set ec=false
endif
endif
set a=a+1
endloop
if ns!=0 and dc<=Qm[gr]then
call YC(gr,ns)
endif
endfunction
function gc takes nothing returns nothing
local integer gr=(LoadInteger(n7,6,GetHandleId(GetExpiredTimer())))
local integer qw=Om[gr]
set Um[gr]=Um[gr]+1
if Um[gr]==3 then
set Um[gr]=0
if Om[gr]<50 and Gx(Sm[gr],Lm)then
set Om[gr]=Om[gr]+Lm
if Om[gr]>50 then
set Om[gr]=50
endif
set qw=Om[gr]-qw
call SetItemCharges(Tm[gr],Om[gr])
call Ix(Sm[gr],I2S(qw),Dj[Ck[gr]])
endif
endif
if Om[gr]>=30 and Rm[gr]then
call ZC(gr)
endif
endfunction
function hc takes nothing returns nothing
local integer m=Bk
local integer gr=Jt(m,9)
set Sm[gr]=cj[m]
set Tm[gr]=kv(Dj[m],1227894843)
call WC(gr)
call TimerStart(Vm[gr],1.,true,function gc)
call Ut("AbilReplenishLife "+I2S(gr)+" is created.")
endfunction
function ic takes integer gr,integer jc returns nothing
if jc==Jm or jc==Km then
set Rm[gr]=not Rm[gr]
endif
endfunction
function kc takes integer T,integer mc,integer b1,integer b2,integer b3,code Lb returns nothing
set Wm[(T)]=mc
set Xm[(T)]=b1
set Ym[(T)]=b2
set Zm[(T)]=b3
set dn[(T)]=CreateTrigger()
call TriggerAddCondition(dn[(T)],Filter(Lb))
endfunction
function oc takes nothing returns nothing
local integer gr=Kt()
call nB(gr,0)
call UnitAddAbility(Dj[gr],1093677101)
call Ut("Wall "+I2S(gr)+" is created.")
endfunction
function qc takes unit u returns nothing
local integer gr=GetUnitUserData(u)
if Dx(cj[gr],ep[(en[gr])])then
set Gj[gr]=false
else
call DisableTrigger(Y7)
call IssueImmediateOrderById(Dj[gr],J)
call EnableTrigger(Y7)
endif
endfunction
function rc takes unit u returns nothing
local integer gr=GetUnitUserData(u)
local integer qw=ep[(en[gr])]
set Gj[gr]=true
call Bx(cj[gr],qw)
call Ex(cj[gr],I2S(qw),Dj[gr])
endfunction
function sc takes unit u returns nothing
local integer gr=GetUnitUserData(u)
local integer p=cj[gr]
local real x=lj[gr]
local real y=Jj[gr]
local integer T=en[gr]
call Bx(cj[gr],Kj[gr])
call Ex(cj[gr],I2S(Kj[gr]),Dj[gr])
call EB(gr)
call kB(u,p,x,y,T)
call DB((GetUnitUserData(u)))
endfunction
function tc takes nothing returns nothing
local integer gr=Lt()
call nB(gr,1)
call Ut("Arrow "+I2S(gr)+" is created.")
endfunction
function uc takes nothing returns nothing
local integer gr=Mt()
call nB(gr,2)
call Ut("Multishot "+I2S(gr)+" is created.")
endfunction
function vc takes nothing returns nothing
local integer gr=Nt()
call nB(gr,3)
call Ut("Lightning "+I2S(gr)+" is created.")
endfunction
function wc takes nothing returns nothing
local integer gr=Ot()
call nB(gr,4)
call Ut("Burst "+I2S(gr)+" is created.")
endfunction
function xc takes nothing returns nothing
local integer gr=Pt()
call nB(gr,5)
call Ut("GoldMine "+I2S(gr)+" is created.")
endfunction
function yc takes nothing returns nothing
local integer gr=Qt()
call nB(gr,7)
call Ut("FoE "+I2S(gr)+" is created.")
endfunction
function zc takes nothing returns nothing
local integer gr=Rt()
call nB(gr,8)
call Ut("Blood "+I2S(gr)+" is created.")
endfunction
function Ac takes nothing returns nothing
local integer gr=St()
call nB(gr,9)
call Ut("Wave "+I2S(gr)+" is created.")
endfunction
function ac takes nothing returns nothing
local integer gr=et()
call nB(gr,6)
call Ut("FoL "+I2S(gr)+" is created.")
endfunction
function Bc takes integer T,integer bc,integer Cc,integer cc,integer Dc,string Av returns nothing
set f7[T]=kr()
set un[f7[T]]=bc
set vn[f7[T]]=Cc
set wn[f7[T]]=cc
set xn[f7[T]]=Dc
set yn[f7[T]]=sk[Z4[bc]]
set zn[f7[T]]=Av
endfunction
function Ec takes integer T,integer zv,integer Fc,integer Gc,integer Hc,integer Ic,code lc,integer Jc,integer Kc,integer Lc,integer Mc,integer Nc,code Oc returns nothing
set An[an[f7[T]]]=zv
set Bn[bn[f7[T]]]=Gc
set Cn[cn[f7[T]]]=Fc
set Dn[En[f7[T]]]=Hc
set Fn[Gn[f7[T]]]=Ic
if lc!=null then
set Hn[In[f7[T]]]=CreateTrigger()
call TriggerAddAction(Hn[In[f7[T]]],lc)
endif
set An[an[f7[T]]+1]=Jc
set Bn[bn[f7[T]]+1]=Lc
set Cn[cn[f7[T]]+1]=Kc
set Dn[En[f7[T]]+1]=Mc
set Fn[Gn[f7[T]]+1]=Nc
if Oc!=null then
set Hn[In[f7[T]]+1]=CreateTrigger()
call TriggerAddAction(Hn[In[f7[T]]+1],Oc)
endif
endfunction
function Qc takes integer p,integer id returns boolean
local string Ux=GetObjectName(id)
local integer T
local integer Zx
if SubString(Ux,0,jn)=="AbilityUpgrade" then
set T=S2I(SubString(Ux,kn,mn))
set Zx=S2I(SubString(Ux,nn,on))
set Ux=SubString(Ux,pn,StringLength(Ux))
if Hx(p,Fn[Gn[f7[T]]+Zx])then
set tn=p
set H9[I9[(ag[Bg[p]+T])]+(Zx)]=(true)
call TriggerExecute(Hn[In[f7[T]]+Zx])
call SetPlayerAbilityAvailable(Bf[p],id,false)
call SetPlayerAbilityAvailable(Bf[p],Bn[bn[f7[T]]+Zx],true)
call SetPlayerTechResearched(Bf[p],Dn[En[f7[T]]+Zx],1)
call wx((p),p8,Y9[R9]+"Research complete|r: "+(Ux)+".")
return true
endif
endif
return false
endfunction
function Rc takes integer gr,integer p returns nothing
call UnitAddAbility(ig[p],vn[gr])
call UnitAddAbility(ig[p],wn[gr])
call UnitAddAbility(ig[p],An[an[gr]])
call UnitAddAbility(ig[p],Bn[bn[gr]])
call UnitAddAbility(ig[p],An[an[gr]+1])
call UnitAddAbility(ig[p],Bn[bn[gr]+1])
call UnitAddAbility(fg[p],xn[gr])
endfunction
function Sc takes integer gr,player p,boolean Tc returns nothing
call SetPlayerAbilityAvailable(p,yn[gr],Tc)
call SetPlayerAbilityAvailable(p,xn[gr],Tc)
call SetPlayerAbilityAvailable(p,Cn[cn[gr]],Tc)
call SetPlayerAbilityAvailable(p,Cn[cn[gr]+1],Tc)
call SetPlayerAbilityAvailable(p,wn[gr],Tc)
call SetPlayerAbilityAvailable(p,vn[gr],not Tc)
endfunction
function Uc takes integer gr,integer p returns nothing
call Sc(bg[p],Bf[p],false)
call Sc(gr,Bf[p],true)
call tx(p,gr)
endfunction
function Vc takes integer p,integer id returns boolean
local string Ux=GetObjectName(id)
local integer T
if SubString(Ux,0,qn)=="HeroAbility" then
set T=S2I(SubString(Ux,rn,sn))
call Uc(f7[T],p)
return true
endif
return false
endfunction
function Wc takes integer T,integer Xc,real Yc,integer Vx,integer Zc,integer dD,integer eD,integer fD,integer gD,integer hD,integer iD,real jD,real kD,integer mD,real nD,integer oD,integer pD returns nothing
set g7[T]=jr()
set Wn[g7[T]]=Zc
set go[g7[T]]=jD
if kD==7 then
set ho[g7[T]]=jD/ 180.*.25
else
set ho[g7[T]]=kD*.25
endif
set Pn[g7[T]]=mD
set Tn[g7[T]]=Xc
set Xn[g7[T]]=dD
set Yn[g7[T]]=eD
set Zn[g7[T]]=fD
set do[g7[T]]=gD
set Un[g7[T]]=Yc
set Vn[g7[T]]=Vx
set eo[g7[T]]=hD
set fo[g7[T]]=iD
set ko[g7[T]]=nD
set mo[g7[T]]=oD
set no[g7[T]]=pD
endfunction
function qD takes integer T,integer rD,integer sD,integer tD,integer uD,boolean vD,boolean wD,real xD,code Lb returns nothing
set Ln[g7[T]]=rD
set Mn[g7[T]]=sD
set Nn[g7[T]]=tD
set Sn[g7[T]]=uD
set Qn[g7[T]]=vD
set Rn[g7[T]]=wD
set On[g7[T]]=xD
set h7[T]=CreateTrigger()
call TriggerAddCondition(h7[T],Filter(Lb))
endfunction
function zD takes integer T,integer zv,integer Fc,integer Gc,integer Hc,integer Ic,code lc,integer Jc,integer Kc,integer Lc,integer Mc,integer Nc,code Oc,integer AD,integer aD,integer BD,integer bD,integer CD,code cD returns nothing
set i7[T]=hr()
set to[uo[i7[T]]]=zv
set to[uo[i7[T]]+1]=Jc
set to[uo[i7[T]]+2]=AD
set vo[wo[i7[T]]]=Gc
set vo[wo[i7[T]]+1]=Lc
set vo[wo[i7[T]]+2]=BD
set xo[yo[i7[T]]]=Fc
set xo[yo[i7[T]]+1]=Kc
set xo[yo[i7[T]]+2]=aD
set zo[Ao[i7[T]]]=Hc
set zo[Ao[i7[T]]+1]=Mc
set zo[Ao[i7[T]]+2]=bD
set ao[Bo[i7[T]]]=Ic
set ao[Bo[i7[T]]+1]=Nc
set ao[Bo[i7[T]]+2]=CD
if lc!=null then
set bo[Co[i7[T]]]=CreateTrigger()
call TriggerAddAction(bo[Co[i7[T]]],lc)
endif
if Oc!=null then
set bo[Co[i7[T]]+1]=CreateTrigger()
call TriggerAddAction(bo[Co[i7[T]]+1],Oc)
endif
if cD!=null then
set bo[Co[i7[T]]+2]=CreateTrigger()
call TriggerAddAction(bo[Co[i7[T]]+2],cD)
endif
endfunction
function FD takes integer T,integer Xc,boolean GD,integer Zc,boolean HD,boolean ID,integer lD,integer JD,integer KD,integer LD,integer MD,integer ND returns nothing
set ep[(T)]=Xc
set fp[(T)]=GD
set gp[(T)]=Zc
set hp[(T)]=HD
set ip[(T)]=ID
set jp[(T)]=lD
set kp[(T)]=JD
set mp[(T)]=KD
set np[(T)]=LD
set op[(T)]=MD
set pp[(T)]=ND
endfunction
function OD takes integer T,real PD,real QD,integer mc,integer RD,boolean SD,integer TD,code UD returns nothing
set Vo[(T)]=PD
set Wo[(T)]=QD
set Xo[(T)]=mc
set Yo[(T)]=RD
set Zo[(T)]=SD
set dp[(T)]=TD
set qp[(T)]=CreateTrigger()
call TriggerAddCondition(qp[(T)],Filter(UD))
endfunction
function VD takes integer T,real WD,integer Dy,real cd,integer kx,integer XD,real Au,integer n,boolean xB,integer Zv,integer jc returns nothing
set Lo[(T)]=WD
set Mo[(T)]=Dy
set No[(T)]=cd
set Oo[(T)]=kx
set Po[(T)]=P4[XD]
set Qo[(T)]=Au
set Ro[(T)]=n
set So[(T)]=xB
set To[(T)]=Zv
set Uo[(T)]=jc
endfunction
function ZD takes integer T,integer dE,integer eE,integer fE,integer gE,integer Dc,integer hE,integer iE,string Av returns nothing
set k7[T]=fr()
set Fo[k7[T]]=dE
set Go[Ho[k7[T]]]=f7[eE]
set Go[Ho[k7[T]]+1]=f7[fE]
set Go[Ho[k7[T]]+2]=f7[gE]
set Io[k7[T]]=Dc
set lo[k7[T]]=hE
set Jo[k7[T]]=iE
set Ko[k7[T]]=Av
endfunction
function InitHeroData takes nothing returns nothing
call ZD(1,6,1,2,3,1093677365,1093677366,1093677367,null)
endfunction
function jE takes integer gr,integer p returns nothing
local integer a=0
set Ag[p]=gr
call UnitRemoveAbility(ig[p],lo[gr])
call UnitRemoveAbility(ig[p],Jo[gr])
call UnitRemoveAbility(fg[p],1093677364)
call UnitAddAbility(fg[p],Io[gr])
loop
call Rc(Go[Ho[gr]+a],p)
set a=a+1
exitwhen a==3
endloop
set a=0
loop
call UnitAddAbility(ig[p],to[uo[i7[Fo[gr]]]+a])
call UnitAddAbility(ig[p],vo[wo[i7[Fo[gr]]]+a])
set a=a+1
exitwhen a==3
endloop
call Uc(Go[Ho[gr]],p)
endfunction
function InitGlobals takes nothing returns nothing
endfunction
function kE takes nothing returns nothing
local string s
set q7=q7+1
if q7==60 then
set q7=0
set r7=r7+1
endif
set t7=t7-1
if t7==0 then
set s7=s7+1
call TriggerExecute(Q7)
set t7=30
endif
if r7<$A then
set s="0"
else
set s=""
endif
set s=s+I2S(r7)+":"
if q7<$A then
set s=s+"0"
endif
set s=s+I2S(q7)
call MultiboardSetItemValue(J8,s)
set s="Wave "+Y9[P9]+I2S(s7)+"|r in "
if Ef[X]and t7<4 then
set s=s+Y9[S9]
call StartSound(r8)
else
set s=s+Y9[P9]
endif
set s=s+I2S(t7)+"|r second"
if t7!=1 then
set s=s+"s"
endif
call MultiboardSetTitleText(a8,s)
endfunction
function mE takes nothing returns nothing
local integer p=(z4[GetPlayerId((GetTriggerPlayer()))])
local integer nE=If[p]
local real x=Vd[nE]
local real y=320.
local boolean oE=true
local integer t
local unit u
loop
call iB(nE,x,y,true)
set u=CreateUnit(Bf[p],1949315120,x,y,270.)
call kB(u,p,x,y,0)
set t=GetUnitUserData(u)
call DB(t)
if oE then
set x=x+128.
if x>=Ud[nE]then
set y=y-256.
set oE=false
endif
else
set x=x-128.
if x<=Vd[nE]then
set y=y-256.
set oE=true
endif
endif
exitwhen y<k4
endloop
call Tv(nE)
set u=null
endfunction
function pE takes nothing returns nothing
call PauseTimer(m7)
call DisableTrigger(w7)
call EnableTrigger(x7)
endfunction
function qE takes nothing returns nothing
call kE()
call TimerStart(m7,1.,true,function kE)
call EnableTrigger(w7)
endfunction
function rE takes nothing returns nothing
call TimerStart(m7,TimerGetRemaining(m7),false,function qE)
call DisableTrigger(x7)
endfunction
function sE takes nothing returns nothing
set t7=1
call kE()
endfunction
function tE takes nothing returns nothing
call pE()
call sE()
endfunction
function uE takes nothing returns nothing
local integer m=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),e4))and Zg[m]then
call Ez(m,false)
endif
endfunction
function vE takes nothing returns nothing
call GroupEnumUnitsSelected(bj_lastCreatedGroup,GetTriggerPlayer(),a7)
endfunction
function wE takes nothing returns nothing
call Wx((z4[GetPlayerId((GetTriggerPlayer()))]))
endfunction
function xE takes nothing returns nothing
local integer t=GetUnitUserData(GetFilterUnit())
local integer a=0
if(IsUnitType(GetFilterUnit(),Z))and Gj[t]and Sj[t]then
loop
set a=a+1
call HB(t)
exitwhen(not Sj[t])or a==D7
endloop
if Sj[t]and ip[Fj[t]]then
call SetItemCharges(dk[t],Vj[t]-Tj[t])
endif
endif
endfunction
function yE takes nothing returns nothing
local string in=GetEventPlayerChatString()
if SubString(in,0,C7)=="inc" then
set D7=S2I(SubString(in,c7,StringLength(in)))
if D7>0 then
call GroupEnumUnitsSelected(bj_lastCreatedGroup,GetTriggerPlayer(),E7)
endif
endif
endfunction
function zE takes nothing returns nothing
local string in=GetEventPlayerChatString()
local integer p=(z4[GetPlayerId((GetTriggerPlayer()))])
local integer Gs
if SubString(in,0,G7)=="gold" then
set Gs=S2I(SubString(in,H7,StringLength(in)))
if 0<Gs and Gs<=99999 then
call Bx(p,Gs-Kf[p])
endif
endif
endfunction
function AE takes nothing returns nothing
local string in=GetEventPlayerChatString()
local integer p=(z4[GetPlayerId((GetTriggerPlayer()))])
local integer Gs
if SubString(in,0,l7)=="exp" then
set Gs=S2I(SubString(in,J7,StringLength(in)))
if 0<Gs and Gs<=99999 then
call Fx(p,Gs-Lf[p])
endif
endif
endfunction
function aE takes nothing returns nothing
local string in=GetEventPlayerChatString()
local integer p=(z4[GetPlayerId((GetTriggerPlayer()))])
local integer Gs
if SubString(in,0,L7)=="lives" then
set Gs=S2I(SubString(in,M7,StringLength(in)))
if 0<Gs and Gs<=9999 then
call Kx(p,Gs-Pf[p])
endif
endif
endfunction
function BE takes nothing returns nothing
local string in=GetEventPlayerChatString()
local integer bE
if SubString(in,0,O7)=="wave" then
set bE=S2I(SubString(in,P7,StringLength(in)))
if bE>0 then
set s7=bE
endif
endif
endfunction
function CE takes player p returns nothing
call TriggerAddAction(v7,function mE)
call TriggerAddAction(w7,function pE)
call DisableTrigger(x7)
call TriggerAddAction(x7,function rE)
call TriggerAddAction(y7,function sE)
call TriggerAddAction(z7,function tE)
call TriggerAddAction(A7,function vE)
set a7=Filter(function uE)
call TriggerAddAction(B7,function wE)
call TriggerAddAction(b7,function yE)
set E7=Filter(function xE)
call TriggerAddAction(F7,function zE)
call TriggerAddAction(I7,function AE)
call TriggerAddAction(K7,function aE)
call TriggerAddAction(N7,function BE)
call TriggerRegisterPlayerChatEvent(v7,p,"maze",true)
call TriggerRegisterPlayerChatEvent(w7,p,"p",true)
call TriggerRegisterPlayerChatEvent(x7,p,"r",true)
call TriggerRegisterPlayerChatEvent(y7,p,"s",true)
call TriggerRegisterPlayerChatEvent(z7,p,"ps",true)
call TriggerRegisterPlayerChatEvent(A7,p,"kill",true)
call TriggerRegisterPlayerChatEvent(B7,p,"hero",true)
call TriggerRegisterPlayerChatEvent(b7,p,"inc",false)
call TriggerRegisterPlayerChatEvent(F7,p,"gold",false)
call TriggerRegisterPlayerChatEvent(I7,p,"exp",false)
call TriggerRegisterPlayerChatEvent(K7,p,"lives",false)
call TriggerRegisterPlayerChatEvent(N7,p,"wave",false)
endfunction
function cE takes nothing returns nothing
local integer i=0
local integer a
local integer p
call Ou()
loop
exitwhen i>Q8
set p=(P8[(i)])
call Pu(p)
set a=0
loop
exitwhen a>h9[mg[p]]
call jy((f9[g9[(mg[p])]+(a)]))
set a=a+1
endloop
set Cg[p]=0
set h9[mg[p]]=-1
call qx(p)
if u7<=s7 and(25<s7 or Xt(s7))then
call Wx(p)
endif
call ux(p)
if 41<=s7 and Pf[p]>1 then
call Mx(p)
endif
call Bx(p,Of[p])
call wx(p,q8,"You receive "+Y9[J9]+I2S(Of[p])+"|r Gold.")
set i=i+1
endloop
endfunction
function InitTrig_SendMinions takes nothing returns nothing
call TriggerAddAction(Q7,function cE)
endfunction
function DE takes nothing returns nothing
local integer fc=mg[R7]
local integer a=h9[fc]
loop
exitwhen a==-1
call zs((f9[g9[(fc)]+(a)]))
set a=a-1
endloop
call Ut("forces of player "+I2S(R7)+" is flushed.")
endfunction
function EE takes nothing returns nothing
local integer fc=ng[R7]
local integer a=o9[fc]
loop
exitwhen a==-1
call ky((m9[n9[(fc)]+(a)]))
set a=a-1
endloop
call Ut("spawned of player "+I2S(R7)+" is flushed.")
endfunction
function FE takes nothing returns nothing
local integer fc=og[R7]
local integer a=u9[fc]
loop
exitwhen a==-1
call Dz((s9[t9[(fc)]+(a)]))
set a=a-1
endloop
call Ut("minions of player "+I2S(R7)+" is flushed.")
endfunction
function GE takes nothing returns nothing
local integer fc=qg[R7]
local integer a=A9[fc]
loop
exitwhen a==-1
call Cz((y9[z9[(fc)]+(a)]))
set a=a-1
endloop
call Ut("dead of player "+I2S(R7)+" is flushed.")
endfunction
function HE takes nothing returns nothing
local integer fc=kg[R7]
local integer a=Y8[fc]
loop
exitwhen a==-1
call FB((W8[X8[(fc)]+(a)]))
set a=a-1
endloop
call Ut("towers of player "+I2S(R7)+" is flushed.")
endfunction
function IE takes nothing returns nothing
local integer p=R7
call Mu(p)
call ExecuteFunc("DE")
call ExecuteFunc("EE")
call ExecuteFunc("FE")
call ExecuteFunc("GE")
call ExecuteFunc("HE")
call Qx(p)
call Ut("Player "+I2S(p)+" is flushed.")
endfunction
function InitTrig_FlushPlayer takes nothing returns nothing
call TriggerAddAction(S7,function IE)
endfunction
function lE takes nothing returns nothing
local integer p=(z4[GetPlayerId((GetTriggerPlayer()))])
if Ef[p]then
call Du(p)
if Q8==0 then
call Eu($A)
endif
endif
endfunction
function InitTrig_LeavesGame takes nothing returns nothing
call TriggerAddAction(T7,function lE)
endfunction
function JE takes nothing returns nothing
call mB(GetConstructingStructure())
endfunction
function InitTrig_ConstructStart takes nothing returns nothing
call TriggerAddAction(U7,function JE)
endfunction
function KE takes nothing returns nothing
call GB((GetUnitUserData(GetCancelledStructure())))
endfunction
function InitTrig_ConstructCancel takes nothing returns nothing
call TriggerAddAction(V7,function KE)
endfunction
function LE takes nothing returns nothing
call DB((GetUnitUserData(GetConstructedStructure())))
endfunction
function InitTrig_ConstructEnd takes nothing returns nothing
call TriggerAddAction(W7,function LE)
endfunction
function ME takes nothing returns nothing
call qc(GetTriggerUnit())
endfunction
function InitTrig_UpgradeStart takes nothing returns nothing
call TriggerAddAction(X7,function ME)
endfunction
function NE takes nothing returns nothing
call rc(GetTriggerUnit())
endfunction
function InitTrig_UpgradeCancel takes nothing returns nothing
call TriggerAddAction(Y7,function NE)
endfunction
function OE takes nothing returns nothing
call sc(GetTriggerUnit())
endfunction
function InitTrig_UpgradeEnd takes nothing returns nothing
call TriggerAddAction(Z7,function OE)
endfunction
function PE takes nothing returns nothing
local integer m=GetUnitUserData(GetLeavingUnit())
if dh[m]then
call zz(m)
endif
endfunction
function InitTrig_LeaveSpawn takes nothing returns nothing
call TriggerAddAction(d8,function PE)
endfunction
function QE takes nothing returns nothing
local integer m=GetUnitUserData(GetEnteringUnit())
local integer p=Tg[m]
call Lx(p,eo[hh[m]])
call my(m,false)
if p!=Sg[m]and Ef[Sg[m]]then
call Lx(Sg[m],fo[hh[m]])
endif
call DestroyEffect(AddSpecialEffect(("Abilities\\Spells\\Undead\\DarkRitual\\DarkRitualTarget.mdx"),((GetUnitX(Vg[m]))*1.),((GetUnitY(Vg[m]))*1.)))
call Dz(m)
if Pf[p]==0 then
call Du(p)
if bj_isSinglePlayer!=(Q8==0)then
call Eu($A)
endif
endif
endfunction
function InitTrig_EnterFinish takes nothing returns nothing
call TriggerAddAction(e8,function QE)
endfunction
function RE takes unit b,integer id returns nothing
local integer p=(z4[GetPlayerId((GetOwningPlayer(b)))])
if id==rd[cg[p]]then
if Ff[p]then
call ClearSelection()
call SelectUnit(gg[p],true)
endif
if Eg[p]!=cg[p]then
call Ev(p)
endif
return
endif
if id==sd[Dg[p]]then
if Ff[p]then
call ClearSelection()
call SelectUnit(gg[p],true)
endif
if Eg[p]!=Dg[p]then
call Ev(p)
endif
return
endif
if(id==Io[Ag[p]]or id==1093677364)and Ff[p]then
call ClearSelection()
call SelectUnit(ig[p],true)
endif
endfunction
function SE takes unit b,integer id returns nothing
local integer p=(z4[GetPlayerId((GetOwningPlayer(b)))])
if Tx(p,id)then
return
endif
if id==1093677123 then
if Ff[p]then
call ClearSelection()
call SelectUnit(hg[p],true)
endif
if Eg[p]!=Gg[p]then
call Bv(p)
endif
return
endif
if id==1093677135 and Ff[p]then
call ClearSelection()
call SelectUnit(ig[p],true)
return
endif
if id==1093677144 and Ff[p]then
call ClearSelection()
call SelectUnit(jg[p],true)
return
endif
if id==1093677124 then
call Ev(p)
endif
endfunction
function TE takes unit b,integer id returns nothing
local integer p=(z4[GetPlayerId((GetOwningPlayer(b)))])
if Xx(p,id,true)then
return
endif
if id==1093677362 then
call Bv(p)
return
endif
if id==1093677361 then
call Hz(p)
endif
endfunction
function UE takes unit b,integer id returns nothing
local integer p=(z4[GetPlayerId((GetOwningPlayer(b)))])
if id==lo[pd[cg[p]]]then
call UnitRemoveAbility(b,Jo[pd[Dg[p]]])
call jE(pd[cg[p]],p)
return
endif
if id==Jo[pd[Dg[p]]]then
call UnitRemoveAbility(b,lo[pd[cg[p]]])
call jE(pd[Dg[p]],p)
return
endif
if Xx(p,id,false)then
return
endif
if Vc(p,id)then
return
endif
call Qc(p,id)
endfunction
function VE takes integer GA,integer id,integer ns returns nothing
if id==1093677119 or id==1093677121 then
call GB(GA)
return
endif
if id==1093677129 then
call MB(GA)
return
endif
if id==1093677127 then
call zC(ek[GA],ns)
endif
endfunction
function WE takes integer m,integer id returns nothing
if id==1093677122 then
call zs(m)
return
endif
if id==sk[Z4[1]]then
call Da(Yg[m])
endif
endfunction
function XE takes nothing returns nothing
local integer id=GetSpellAbilityId()
local unit u=GetSpellAbilityUnit()
local integer GA=GetUnitUserData(u)
local integer ns=GetUnitUserData(GetSpellTargetUnit())
local integer T=GetUnitTypeId(u)
if T==1112294482 then
call RE(u,id)
elseif T==1112689491 then
call SE(u,id)
elseif T==1112232788 then
call TE(u,id)
elseif T==1095521362 then
call UE(u,id)
elseif(IsUnitType((u),Z))then
call VE(GA,id,ns)
elseif(IsUnitType((u),e4))then
call WE(GA,id)
endif
set u=null
endfunction
function InitTrig_PlayerAbilityEffect takes nothing returns nothing
call TriggerAddAction(f8,function XE)
endfunction
function YE takes integer GA,integer id returns nothing
local integer Zv=ek[GA]
if dq[Zv]==86 then
call Vb(Zv,id)
return
endif
if dq[Zv]==91 then
call aC(Zv,id)
endif
endfunction
function ZE takes integer m,integer id returns nothing
if id==1093677099 then
call GroupEnumUnitsSelected(bj_lastCreatedGroup,Bf[(Sg[m])],yh)
return
endif
if Wp[Yg[m]]==66 then
call Ja(Yg[m],id)
endif
endfunction
function d3 takes nothing returns nothing
local integer id=GetSpellAbilityId()
local unit u=GetSpellAbilityUnit()
local integer GA=GetUnitUserData(u)
if(IsUnitType((u),Z))or(IsUnitType((u),d4))then
call YE(GA,id)
elseif(IsUnitType((u),e4))then
call ZE(GA,id)
endif
set u=null
endfunction
function InitTrig_PlayerAbilityEndcast takes nothing returns nothing
call TriggerAddAction(g8,function d3)
endfunction
function e3 takes unit b,integer id returns nothing
local integer p=(z4[GetPlayerId((GetOwningPlayer(b)))])
if id==x4 or id==y4 then
call yx(p)
endif
endfunction
function f3 takes integer GA,integer id returns nothing
local integer Zv=ek[GA]
if 1949315120<=id and id<1949315120+j7 then
set en[(GA)]=id-1949315120
return
endif
if dq[Zv]==94 then
call ic(Zv,id)
endif
endfunction
function g3 takes nothing returns nothing
local integer id=GetIssuedOrderId()
local unit u=GetOrderedUnit()
local integer GA=GetUnitUserData(u)
local integer T=GetUnitTypeId(u)
if T==1112294482 then
call e3(u,id)
elseif(IsUnitType((u),Z))then
call f3(GA,id)
endif
set u=null
endfunction
function InitTrig_PlayerImdtOrders takes nothing returns nothing
call TriggerAddAction(h8,function g3)
endfunction
function h3 takes unit b,integer id,real x,real y returns nothing
if id==L or(1949315120<=id and id<1949315120+j7)then
call SetUnitX(b,x)
call SetUnitY(b,y)
endif
endfunction
function i3 takes nothing returns nothing
local integer id=GetIssuedOrderId()
local unit u=GetOrderedUnit()
local integer T=GetUnitTypeId(u)
local real x=GetOrderPointX()
local real y=GetOrderPointY()
local integer nE=If[(z4[GetPlayerId((GetOwningPlayer(u)))])]
if T==1112294482 and Td[nE]<=x and x<=Sd[nE]then
call h3(u,id,x,y)
endif
set u=null
endfunction
function InitTrig_PlayerPointOrders takes nothing returns nothing
call TriggerAddAction(i8,function i3)
endfunction
function j3 takes unit u returns nothing
local integer NB=GetUnitUserData(u)
if(IsUnitType((u),Z))then
call Xr((NB))
endif
endfunction
function k3 takes nothing returns nothing
if GetTriggerPlayer()==GetOwningPlayer(GetTriggerUnit())then
call j3(GetTriggerUnit())
endif
endfunction
function InitTrig_PlayerSelects takes nothing returns nothing
call TriggerAddAction(j8,function k3)
endfunction
function m3 takes unit u returns nothing
local integer NB=GetUnitUserData(u)
if(IsUnitType((u),Z))then
call Yr((NB))
endif
endfunction
function n3 takes nothing returns nothing
if GetTriggerPlayer()==GetOwningPlayer(GetTriggerUnit())then
call m3(GetTriggerUnit())
endif
endfunction
function InitTrig_PlayerDeselects takes nothing returns nothing
call TriggerAddAction(k8,function n3)
endfunction
function o3 takes integer t,integer m returns nothing
if not(yf[Xg[(m)]])then
call Tr(t,m)
endif
endfunction
function p3 takes unit DD,unit q3,real WD returns nothing
local integer dd=GetUnitUserData(DD)
local integer ns=GetUnitUserData(q3)
call SetWidgetLife(q3,GetWidgetLife(q3)+WD)
if(not UnitAlive((q3)))then
call DisplayTimedTextToPlayer(S,.0,.0,((10.)*1.),("No target took damage!"))
return
endif
if(IsUnitType((DD),d4))then
call o3(dd,ns)
elseif(IsUnitType((DD),f4))then
call ms((dd),(ns))
endif
endfunction
function r3 takes nothing returns nothing
if GetEventDamage()>.0 and(GetUnitTypeId((GetEventDamageSource()))!=0)then
call p3(GetEventDamageSource(),GetTriggerUnit(),GetEventDamage())
endif
endfunction
function InitTrig_TakeDamage takes nothing returns nothing
call TriggerAddAction(m8,function r3)
endfunction
function s3 takes nothing returns nothing
local integer i=0
local integer ai=0
local player p
loop
set p=Player(i)
if GetPlayerSlotState(p)==PLAYER_SLOT_STATE_PLAYING then
if GetPlayerController(p)==MAP_CONTROL_USER then
call gs(p,i)
endif
elseif ai<2 then
set W[ai]=p
call SetPlayerColor(p,Y)
call FogModifierStart(CreateFogModifierRect(p,FOG_OF_WAR_VISIBLE,bj_mapInitialPlayableArea,false,true))
set ai=ai+1
endif
set i=i+1
exitwhen i==$C
endloop
if a4!=null then
set i=0
loop
exitwhen(not A4[i])and ConvertPlayerColor(i)!=Y
set i=i+1
endloop
call SetPlayerColor(a4,ConvertPlayerColor(i))
set cf[(z4[GetPlayerId((a4))])]=i
endif
set X=(z4[GetPlayerId((S))])
call TriggerRegisterLeaveRegion(d8,p7,h4)
call TriggerRegisterEnterRegion(e8,o7,h4)
call Ut("Initialization complete!")
set p=null
endfunction
function InitTrig_Main takes nothing returns nothing
call TriggerAddAction(z8,function s3)
endfunction
function t3 takes nothing returns nothing
local integer i=0
local integer p
loop
exitwhen i>Q8
set p=(P8[(i)])
set cg[p]=j4[1]
set Dg[p]=0
call hs(p)
call jE(pd[j4[1]],p)
set i=i+1
endloop
set bj_isSinglePlayer=Q8==0
call TriggerExecute(L8)
call kE()
call TimerStart(m7,1.,true,function kE)
if bj_isSinglePlayer then
call CE(Bf[(P8[(0)])])
endif
endfunction
function v3 takes nothing returns integer
set B8=B8+1
call MultiboardSetRowCount(a8,B8)
return B8-1
endfunction
function w3 takes nothing returns nothing
local integer a
local multiboarditem Px
local integer p
set a8=CreateMultiboard()
call MultiboardSetTitleTextColor(a8,$FF,$FF,$FF,$FF)
call MultiboardSetColumnCount(a8,8)
call MultiboardSetItemsStyle(a8,true,false)
call MultiboardSetItemsWidth(a8,b8)
set a=v3()
set Px=MultiboardGetItem(a8,a,0)
call MultiboardSetItemWidth(Px,C8)
call MultiboardSetItemValue(Px,"Player")
call MultiboardReleaseItem(Px)
set Px=MultiboardGetItem(a8,a,2)
call MultiboardSetItemWidth(Px,c8)
call MultiboardSetItemValue(Px,"Gold")
call MultiboardReleaseItem(Px)
set Px=MultiboardGetItem(a8,a,3)
call MultiboardSetItemWidth(Px,D8)
call MultiboardSetItemValue(Px,"Exp.")
call MultiboardReleaseItem(Px)
set Px=MultiboardGetItem(a8,a,4)
call MultiboardSetItemWidth(Px,E8)
call MultiboardSetItemValue(Px,"Supply")
call MultiboardReleaseItem(Px)
set Px=MultiboardGetItem(a8,a,5)
call MultiboardSetItemWidth(Px,F8)
call MultiboardSetItemValue(Px,"Inc.")
call MultiboardReleaseItem(Px)
set Px=MultiboardGetItem(a8,a,6)
call MultiboardSetItemWidth(Px,G8)
call MultiboardSetItemValue(Px,"Lives")
call MultiboardReleaseItem(Px)
set Px=MultiboardGetItem(a8,a,7)
call MultiboardSetItemWidth(Px,H8)
call MultiboardSetItemValue(Px,"Kills")
call MultiboardReleaseItem(Px)
set a=0
loop
exitwhen a>Q8
set p=(P8[(a)])
set Rf[p]=v3()
set Sf[p]=MultiboardGetItem(a8,Rf[p],0)
call MultiboardSetItemWidth(Sf[p],C8)
call MultiboardSetItemValue(Sf[p],Cf[p])
call MultiboardSetItemValueColor(Sf[p],V9[cf[p]],W9[cf[p]],X9[cf[p]],K8)
set Tf[p]=MultiboardGetItem(a8,Rf[p],2)
call MultiboardSetItemWidth(Tf[p],c8)
call MultiboardSetItemValue(Tf[p],I2S(Kf[p]))
call MultiboardSetItemValueColor(Tf[p],V9[J9],W9[J9],X9[J9],K8)
set Uf[p]=MultiboardGetItem(a8,Rf[p],3)
call MultiboardSetItemWidth(Uf[p],D8)
call MultiboardSetItemValue(Uf[p],I2S(Lf[p]))
call MultiboardSetItemValueColor(Uf[p],V9[K9],W9[K9],X9[K9],K8)
set Vf[p]=MultiboardGetItem(a8,Rf[p],4)
call MultiboardSetItemWidth(Vf[p],E8)
call MultiboardSetItemValue(Vf[p],"0/"+I2S(Mf[p]))
call MultiboardSetItemValueColor(Vf[p],V9[L9],W9[L9],X9[L9],K8)
set Wf[p]=MultiboardGetItem(a8,Rf[p],5)
call MultiboardSetItemWidth(Wf[p],F8)
call MultiboardSetItemValue(Wf[p],I2S(Of[p]))
call MultiboardSetItemValueColor(Wf[p],V9[M9],W9[M9],X9[M9],K8)
set Xf[p]=MultiboardGetItem(a8,Rf[p],6)
call MultiboardSetItemWidth(Xf[p],G8)
call MultiboardSetItemValue(Xf[p],I2S(Pf[p]))
call MultiboardSetItemValueColor(Xf[p],V9[N9],W9[N9],X9[N9],K8)
set Yf[p]=MultiboardGetItem(a8,Rf[p],7)
call MultiboardSetItemWidth(Yf[p],H8)
call MultiboardSetItemValue(Yf[p],"0")
call MultiboardSetItemValueColor(Yf[p],V9[O9],W9[O9],X9[O9],K8)
set a=a+1
endloop
call v3()
set a=v3()
set Px=MultiboardGetItem(a8,a,0)
call MultiboardSetItemWidth(Px,I8)
call MultiboardSetItemValue(Px,"Game time: ")
call MultiboardReleaseItem(Px)
set Px=null
set J8=MultiboardGetItem(a8,a,1)
call MultiboardSetItemWidth(J8,l8)
call MultiboardSetItemValueColor(J8,V9[Q9],W9[Q9],X9[Q9],K8)
call MultiboardDisplay(a8,true)
call MultiboardMinimize(a8,false)
endfunction
function InitTrig_Multiboard takes nothing returns nothing
call TriggerAddAction(L8,function w3)
endfunction
function x3 takes nothing returns nothing
local string s
set t7=t7-1
set s="Game will end in "+Y9[P9]+I2S(t7)+"|r second"
if t7>1 then
set s=s+"s"
endif
if t7==0 then
call MultiboardSetTitleText(a8,"Game is ending...")
call EndGame(true)
else
call MultiboardSetTitleText(a8,s)
endif
endfunction
function y3 takes nothing returns nothing
local integer i=0
loop
exitwhen i>Q8
call Du((P8[(i)]))
set i=i+1
endloop
call ClearTextMessages()
call x3()
call TimerStart(m7,1.,true,function x3)
endfunction
function InitTrig_FinishGame takes nothing returns nothing
call TriggerAddAction(M8,function y3)
endfunction
function InitCustomPlayerSlots takes nothing returns nothing
call SetPlayerStartLocation(Player(0),0)
call SetPlayerColor(Player(0),ConvertPlayerColor(0))
call SetPlayerRacePreference(Player(0),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(0),true)
call SetPlayerController(Player(0),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(1),1)
call SetPlayerColor(Player(1),ConvertPlayerColor(1))
call SetPlayerRacePreference(Player(1),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(1),true)
call SetPlayerController(Player(1),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(2),2)
call SetPlayerColor(Player(2),ConvertPlayerColor(2))
call SetPlayerRacePreference(Player(2),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(2),true)
call SetPlayerController(Player(2),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(3),3)
call SetPlayerColor(Player(3),ConvertPlayerColor(3))
call SetPlayerRacePreference(Player(3),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(3),true)
call SetPlayerController(Player(3),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(4),4)
call SetPlayerColor(Player(4),ConvertPlayerColor(4))
call SetPlayerRacePreference(Player(4),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(4),true)
call SetPlayerController(Player(4),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(5),5)
call SetPlayerColor(Player(5),ConvertPlayerColor(5))
call SetPlayerRacePreference(Player(5),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(5),true)
call SetPlayerController(Player(5),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(6),6)
call SetPlayerColor(Player(6),ConvertPlayerColor(6))
call SetPlayerRacePreference(Player(6),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(6),true)
call SetPlayerController(Player(6),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(7),7)
call SetPlayerColor(Player(7),ConvertPlayerColor(7))
call SetPlayerRacePreference(Player(7),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(7),true)
call SetPlayerController(Player(7),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(8),8)
call SetPlayerColor(Player(8),ConvertPlayerColor(8))
call SetPlayerRacePreference(Player(8),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(8),true)
call SetPlayerController(Player(8),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(9),9)
call SetPlayerColor(Player(9),ConvertPlayerColor(9))
call SetPlayerRacePreference(Player(9),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(9),true)
call SetPlayerController(Player(9),MAP_CONTROL_USER)
endfunction
function InitCustomTeams takes nothing returns nothing
call SetPlayerTeam(Player(0),0)
call SetPlayerTeam(Player(1),0)
call SetPlayerTeam(Player(2),0)
call SetPlayerTeam(Player(3),0)
call SetPlayerTeam(Player(4),0)
call SetPlayerTeam(Player(5),0)
call SetPlayerTeam(Player(6),0)
call SetPlayerTeam(Player(7),0)
call SetPlayerTeam(Player(8),0)
call SetPlayerTeam(Player(9),0)
endfunction
function InitAllyPriorities takes nothing returns nothing
call SetStartLocPrioCount(0,2)
call SetStartLocPrio(0,0,8,MAP_LOC_PRIO_LOW)
call SetStartLocPrio(0,1,9,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(1,1)
call SetStartLocPrio(1,0,2,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(2,2)
call SetStartLocPrio(2,0,1,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(2,1,3,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(3,2)
call SetStartLocPrio(3,0,2,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(3,1,4,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(4,2)
call SetStartLocPrio(4,0,3,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(4,1,5,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(5,2)
call SetStartLocPrio(5,0,4,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(5,1,6,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(6,2)
call SetStartLocPrio(6,0,5,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(6,1,7,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(7,2)
call SetStartLocPrio(7,0,6,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(7,1,8,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(8,2)
call SetStartLocPrio(8,0,7,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(8,1,9,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(9,1)
call SetStartLocPrio(9,0,8,MAP_LOC_PRIO_HIGH)
endfunction
function main takes nothing returns nothing
call SetCameraBounds(v8+M,w8+O,6912.-N,1920.-P,v8+M,1920.-P,6912.-N,w8+O)
set bj_mapInitialPlayableArea=GetWorldBounds()
call ExecuteFunc("z3")
call ExecuteFunc("wu")
call ExecuteFunc("lu")
call ExecuteFunc("Kz")
call ExecuteFunc("cA")
call TriggerAddAction(Q7,function cE)
call TriggerAddAction(S7,function IE)
call TriggerAddAction(T7,function lE)
call TriggerAddAction(U7,function JE)
call TriggerAddAction(V7,function KE)
call TriggerAddAction(W7,function LE)
call TriggerAddAction(X7,function ME)
call TriggerAddAction(Y7,function NE)
call TriggerAddAction(Z7,function OE)
call TriggerAddAction(d8,function PE)
call TriggerAddAction(e8,function QE)
call TriggerAddAction(f8,function XE)
call TriggerAddAction(g8,function d3)
call TriggerAddAction(h8,function g3)
call TriggerAddAction(i8,function i3)
call TriggerAddAction(j8,function k3)
call TriggerAddAction(k8,function n3)
call TriggerAddAction(m8,function r3)
call TriggerAddAction(z8,function s3)
call TriggerRegisterTimerEvent(A8,1.,false)
call TriggerAddAction(A8,function t3)
call TriggerAddAction(L8,function w3)
call TriggerAddAction(M8,function y3)
set n8=CreateSound("Sound\\Interface\\Error.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(n8,"InterfaceError")
call SetSoundDuration(n8,614)
call SetSoundVolume(n8,105)
set q8=CreateSound("Abilities\\Spells\\Items\\ResourceItems\\ReceiveGold.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(q8,"ReceiveGold")
call SetSoundDuration(q8,589)
call SetSoundChannel(q8,8)
call SetSoundVolume(q8,105)
set r8=CreateSound("Sound\\Interface\\BattleNetTick.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(r8,"ChatroomTimerTick")
call SetSoundDuration(r8,476)
call SetSoundVolume(r8,105)
set s8=CreateSound("Sound\\Interface\\Warning.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(s8,"Warning")
call SetSoundDuration(s8,$770)
call SetSoundVolume(s8,105)
set t8=CreateSound("Sound\\Interface\\ItemReceived.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(t8,"ItemReward")
call SetSoundDuration(t8,$5CB)
call SetSoundVolume(t8,105)
set u8=CreateSound("Sound\\Interface\\UpkeepRing.wav",false,false,false,$A,$A,"")
call SetSoundParamsFromLabel(u8,"UpkeepLevel")
call SetSoundDuration(u8,$62B)
call SetSoundVolume(u8,105)
set o8=CreateSound("Sound\\Interface\\Warning\\Human\\KnightNoGold1.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(o8,"NoGoldHuman")
call SetSoundDuration(o8,$5CE)
call SetSoundVolume(o8,105)
set p8=CreateSound("Sound\\Buildings\\Human\\KnightResearchComplete1.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(p8,"ResearchCompleteHuman")
call SetSoundDuration(p8,$52E)
call SetSoundVolume(p8,105)
set o7=CreateRegion()
set p7=CreateRegion()
set i4=ur()
set gd[hd[i4]]=1.
set gd[hd[i4]+1]=1.075
set gd[hd[i4]+2]=1.155625
set gd[hd[i4]+3]=1.242297
set gd[hd[i4]+4]=1.335469
set gd[hd[i4]+5]=1.435629
set gd[hd[i4]+6]=1.543302
set gd[hd[i4]+7]=1.659049
set gd[hd[i4]+8]=1.783478
set gd[hd[i4]+9]=1.917239
set gd[hd[i4]+$A]=2.061032
set gd[hd[i4]+$B]=2.215609
set gd[hd[i4]+$C]=2.38178
set gd[hd[i4]+$D]=2.560413
set gd[hd[i4]+$E]=2.752444
set gd[hd[i4]+$F]=2.958877
set gd[hd[i4]+16]=3.180793
set gd[hd[i4]+17]=3.419353
set gd[hd[i4]+18]=3.675804
set gd[hd[i4]+19]=3.951489
set gd[hd[i4]+20]=4.247851
set gd[hd[i4]+21]=4.56644
set gd[hd[i4]+22]=4.908923
set gd[hd[i4]+23]=5.277092
set gd[hd[i4]+24]=5.672874
set gd[hd[i4]+25]=6.09834
set gd[hd[i4]+26]=6.555715
set gd[hd[i4]+27]=7.047394
set gd[hd[i4]+28]=7.575948
set gd[hd[i4]+29]=8.144144
set gd[hd[i4]+30]=8.754955
set gd[hd[i4]+31]=9.411577
set gd[hd[i4]+32]=10.117445
set gd[hd[i4]+33]=10.876253
set gd[hd[i4]+34]=11.691972
set gd[hd[i4]+35]=12.56887
set gd[hd[i4]+36]=13.511536
set gd[hd[i4]+37]=14.524901
set gd[hd[i4]+38]=15.614268
set gd[hd[i4]+39]=16.785339
set gd[hd[i4]+40]=18.044239
set gd[hd[i4]+41]=19.397557
set gd[hd[i4]+42]=20.852374
set gd[hd[i4]+43]=22.416302
set gd[hd[i4]+44]=24.097524
set gd[hd[i4]+45]=25.904839
set gd[hd[i4]+46]=27.847702
set gd[hd[i4]+47]=29.936279
set gd[hd[i4]+48]=32.1815
set gd[hd[i4]+49]=34.595113
set gd[hd[i4]+50]=37.189746
call kc(1,1093677109,5,$A,0,function Qb)
call kc(2,1093677133,5,$A,0,function Xb)
call kc(3,1093677108,5,$A,0,function Zb)
call kc(4,1093677130,5,$A,0,function gC)
call kc(5,1093677131,5,$A,0,function qC)
call kc(6,1093677111,1,2,3,function tC)
call kc(7,1093677136,$A,20,0,function JC)
call kc(8,1093677107,5,$A,0,function QC)
call kc(9,1093677141,5,$A,0,function hc)
call FD(0,50,true,-1,false,false,0,0,0,0,0,X4)
call FD(1,100,true,-1,true,true,1,5,20,50,$A,X4)
call FD(2,100,true,-1,true,true,1,5,20,50,$A,X4)
call FD(3,100,true,-1,true,true,1,5,20,50,$A,X4)
call FD(7,500,false,4,true,false,1,0,0,500,3,3)
call FD(8,$C8,true,-1,false,false,0,0,0,0,0,0)
call FD(9,$C8,true,-1,true,true,1,5,20,50,$A,X4)
call FD(4,$C8,true,0,true,false,0,0,0,0,$A,X4)
call FD(5,$4B0,false,0,true,false,1,0,60,$B4,$A,$A)
call FD(6,$3E8,false,0,true,false,1,0,50,$96,$A,$A)
call OD(0,60.,.4,1097609264,0,false,0,function oc)
call OD(1,145.,.6,1097609265,1098133553,true,1,function tc)
call OD(2,60.,.6,1097609266,1098133554,true,2,function uc)
call OD(3,255.,.5,1097609267,1098133555,true,3,function vc)
call OD(4,135.,.6,1097609268,1098133556,true,4,function wc)
call OD(5,60.,.4,1097609269,1098133557,false,5,function xc)
call OD(6,60.,.4,1097609270,1098133558,false,9,function ac)
call OD(7,60.,.4,1097609271,1098133559,false,6,function yc)
call OD(8,255.,.5,1097609272,0,true,7,function zc)
call OD(9,135.,.6,1097609273,1098133561,true,8,function Ac)
call VD(1,10.,i4,1.,1227894834,0,384.,1,false,1093677094,D)
call VD(2,6.,i4,2.,1227894834,0,384.,3,false,1093677097,D)
call VD(3,15.,i4,3.,1227894841,0,384.,1,false,1093677110,C)
call VD(8,30.,0,1.,1227894832,0,384.,1,true,1093677098,F)
call VD(9,10.,i4,3.,1227894834,1,512.,1,true,1093677137,B)
call VD(4,5.,0,.5,1227894832,0,384.,1,false,1093677126,D)
call Yv(v4,i4,0)
call ea(1,true,3,3,3.,0)
call ea(2,true,1,1,3.,0)
call ea(3,true,1,1,3.,0)
call ea(4,true,1,1,.5,0)
call ea(5,true,1,1,.5,0)
call ea(6,false,1,1,3.,0)
call ea(V4,false,1,1,3.,0)
call ea(7,false,1,1,1.5,0)
call ea(S4,false,1,1,2.,0)
call ea(8,false,1,1,2.,0)
call ea(9,false,1,1,1.5,1093677120)
call ea(R4,false,1,1,1./ 16.,0)
call ea(T4,false,1,99,5.,0)
call ea(U4,false,1,1,3.,1093677088)
call ea(W4,false,1,1,2.,0)
call ea(16,false,1,1,3.,1093677088)
call ea(17,false,5,5,5.,1093677092)
call Fb(1,1096888368,false,false,false,.0,.0,.0)
call Fb(2,1096888369,true,false,false,.0,5.,.0)
call Fb(3,1096888370,false,false,true,.05,.0,256.)
call Fb(4,1096888371,true,true,false,.0,5.,512.)
call Fb(5,1096888372,false,false,true,.05,.0,256.)
call Fb(6,1096888373,true,true,false,10.,10.,512.)
call Fb(7,1096888374,true,false,false,10.,10.,.0)
call Fb(8,1096888375,true,false,false,10.,10.,512.)
call Kb(1,function ca,function Ea)
call Kb(2,null,function Fa)
call Kb(3,function la,function Ka)
call Kb(4,null,function Ca)
call Kb(5,null,function Ga)
call Kb(6,null,function Oa)
call Kb(7,null,function Qa)
call Kb(8,null,function Wa)
call Wc(1,100,1.,1,$A,5,25,$A,25,-1,0,100.,7,v4,1.,0,0)
call Wc(2,$DC,2.,2,$A,$B,55,20,50,-1,1,210.,7,v4,1.,$F,0)
call Wc(3,440,3.,3,5,22,110,40,75,-2,1,390.,7,v4,1.25,25,$A)
call Wc(5,440,3.,3,5,22,110,40,75,-3,2,400.,7,v4,1.,25,0)
call Wc(6,0,.0,0,1,0,0,0,0,-5,3,1000.,7,v4,1.,0,0)
call Wc(4,400,2.,2,5,40,40,40,0,-1,1,100.,7,v4,1.,0,0)
call qD(1,1831874608,1097674800,1097412656,1,true,false,60.,function nb)
call qD(2,1831874609,1097674801,1097412657,2,true,false,60.,function vb)
call qD(3,1831874610,1097674802,1097412658,5,true,false,60.,function xb)
call qD(4,1831874611,1097674803,1097412659,4,true,false,60.,function jb)
call qD(5,1831874612,1097674804,1097412660,3,true,false,60.,function ab)
call qD(6,1831874613,0,1093677139,0,false,true,60.,function Cb)
set i7[0]=hr()
call zD(1,1096953904,1097019440,1097084976,1378889776,500,function pb,1096953905,1097019441,1097084977,1378889777,500,function qb,1096953906,1097019442,1097084978,1378889778,$5DC,function rb)
call zD(2,1096953907,1097019443,1097084979,1378889779,$3E8,function wb,1096953908,1097019444,1097084980,1378889780,$3E8,null,1096953909,1097019445,1097084981,1378889781,$5DC,null)
call zD(3,1096953910,1097019446,1097084982,1378889782,$3E8,function yb,1096953911,1097019447,1097084983,1378889783,$5DC,function zb,1096953912,1097019448,1097084984,1378889784,$3E8,function Ab)
call zD(5,1096953913,1097019449,1097084985,1378889785,$3E8,function Bb,1096953889,1097019425,1097084961,1378889761,$3E8,function bb,1096953915,1097019451,1097084987,1378889787,$5DC,null)
call zD(6,1096953914,1097019450,1097084986,1378889786,$FA,function cb,1096953919,1097019455,1097084991,1378889791,$FA,function Db,1096953920,1097019456,1097084992,1378889792,$FA,function Eb)
call zD(4,1096953916,1097019452,1097084988,1378889788,0,null,1096953917,1097019453,1097084989,1378889789,0,function kb,1096953918,1097019454,1097084990,1378889790,0,null)
call Bc(1,6,1097150512,1097216048,1097281584,null)
call Bc(2,7,1097150513,1097216049,1097281585,null)
call Bc(3,8,1097150514,1097216050,1097281586,null)
call Ec(1,1097347120,1097478192,1097543728,1382101040,$FA,function Pa,1097347121,1097478193,1097543729,1382101041,500,null)
call Ec(2,1097347122,1097478194,1097543730,1382101042,$FA,null,1097347123,1097478195,1097543731,1382101043,500,null)
call Ec(3,1097347124,1097478196,1097543732,1382101044,$FA,function fB,1097347125,1097478197,1097543733,1382101045,500,null)
call ZD(1,6,1,2,3,1093677365,1093677366,1093677367,null)
call rv(1,1,2,3,5,1,0,1093677132,1093677360,null)
call rv(2,0,0,0,4,0,0,0,0,null)
call SetGameSpeed(MAP_SPEED_FASTEST)
call SetMapFlag(MAP_LOCK_SPEED,true)
call SetMapFlag(MAP_USE_HANDICAPS,false)
call SetAllyColorFilterState(0)
call SetCreepCampFilterState(false)
call EnableMinimapFilterButtons(false,false)
call EnableWorldFogBoundary(false)
call SetFloatGameState(GAME_STATE_TIME_OF_DAY,12.)
call SuspendTimeOfDay(true)
call SetMapMusic("Music",true,0)
call TriggerExecute(z8)
endfunction
function config takes nothing returns nothing
local player p
call SetMapName("TRIGSTR_001")
call SetMapDescription("TRIGSTR_904")
call SetPlayers(V)
call SetTeams(V)
set p=Player(0)
call DefineStartLocation(0,x8,y8)
call SetPlayerStartLocation(p,0)
call SetPlayerColor(p,ConvertPlayerColor(0))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,0)
set p=Player(1)
call DefineStartLocation(1,x8,y8)
call SetPlayerStartLocation(p,1)
call SetPlayerColor(p,ConvertPlayerColor(1))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(2)
call DefineStartLocation(2,x8,y8)
call SetPlayerStartLocation(p,2)
call SetPlayerColor(p,ConvertPlayerColor(2))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(3)
call DefineStartLocation(3,x8,y8)
call SetPlayerStartLocation(p,3)
call SetPlayerColor(p,ConvertPlayerColor(3))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(4)
call DefineStartLocation(4,x8,y8)
call SetPlayerStartLocation(p,4)
call SetPlayerColor(p,ConvertPlayerColor(4))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(5)
call DefineStartLocation(5,x8,y8)
call SetPlayerStartLocation(p,5)
call SetPlayerColor(p,ConvertPlayerColor(5))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(6)
call DefineStartLocation(6,x8,y8)
call SetPlayerStartLocation(p,6)
call SetPlayerColor(p,ConvertPlayerColor(6))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(7)
call DefineStartLocation(7,x8,y8)
call SetPlayerStartLocation(p,7)
call SetPlayerColor(p,ConvertPlayerColor(7))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(8)
call DefineStartLocation(8,x8,y8)
call SetPlayerStartLocation(p,8)
call SetPlayerColor(p,ConvertPlayerColor(8))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(9)
call DefineStartLocation(9,x8,y8)
call SetPlayerStartLocation(p,9)
call SetPlayerColor(p,ConvertPlayerColor(9))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=null
endfunction
function A3 takes nothing returns boolean
local integer m=aq
local integer T=Bq
local integer gr=xr()
set bk[gr]=(T)
set Ck[gr]=m
set ek[m]=gr
call UnitAddAbility(Dj[m],Wm[bk[gr]])
set Aq=gr
return true
endfunction
function B3 takes nothing returns boolean
local integer gr=Eq
return true
endfunction
function C3 takes nothing returns boolean
local integer gr=Eq
local integer a=0
set a=0
loop
call Gv(Qd[Rd[gr]+a])
set a=a+1
exitwhen a==608
endloop
call Ut("Neighbors are ready!")
return true
endfunction
function c3 takes nothing returns boolean
local integer gr=Eq
local integer a=0
loop
set Qd[Rd[gr]+a]=Fv(gr,a)
set a=a+1
exitwhen a==608
endloop
call cr(gr)
call Ut("Cells are ready!")
return true
endfunction
function D3 takes nothing returns boolean
local integer gr=Eq
local integer a=0
loop
call Br(Qd[Rd[gr]+a])
set a=a+1
exitwhen a==608
endloop
call Ut("Cells destroyed!")
return true
endfunction
function E3 takes nothing returns boolean
local integer gr=Eq
local integer a=0
local boolean F3=ie[gr]
local integer c
loop
set c=Qd[Rd[gr]+a]
set bd[(c)]=true
if F3 then
set xd[(c)]=(yd[c])
set yd[(c)]=(0)
endif
set a=a+1
exitwhen a==t4
endloop
return true
endfunction
function G3 takes nothing returns boolean
local integer gr=Eq
local integer q=aq
local integer c
local integer d
set je[gr]=true
loop
set c=Iv(q)
set d=yd[c]+1
call Kv(q,Cd[c],d)
call Kv(q,Dd[c],d)
call Kv(q,Ed[c],d)
call Kv(q,Fd[c],d)
exitwhen(Kd[(q)]==0)
endloop
set ie[gr]=not bd[Qd[Rd[gr]]]
set je[gr]=false
return true
endfunction
function H3 takes nothing returns boolean
local integer gr=Eq
set Fq=Lo[Nj[gr]]*(gd[hd[(Mo[Nj[gr]])]+(Uj[gr])])
return true
endfunction
function I3 takes nothing returns boolean
local integer gr=Eq
set Fq=.0
return true
endfunction
function l3 takes nothing returns boolean
local integer gr=Eq
set Fq=.0
return true
endfunction
function J3 takes nothing returns boolean
local integer gr=Eq
return true
endfunction
function K3 takes nothing returns boolean
local integer gr=Eq
local integer m=aq
call AB(gr,m)
return true
endfunction
function L3 takes nothing returns boolean
local integer gr=Eq
local integer Vr=aq
if BB(gr,Vr,Oj[gr])then
call aB(gr)
else
call TimerStart(Pj[(gr)],O4,false,gk)
endif
return true
endfunction
function M3 takes nothing returns boolean
local integer gr=Eq
local integer m=aq
set Rj[gr]=Rj[gr]+1
call SetItemCharges(Yj[gr],Rj[gr])
call zx(cj[gr])
if do[hh[m]]>0 then
call JB(m,gr)
endif
return true
endfunction
function N3 takes nothing returns boolean
local integer gr=Eq
if Zo[Fj[gr]]then
call ShowImage(Qj[gr],true)
endif
return true
endfunction
function O3 takes nothing returns boolean
local integer gr=Eq
if Zo[Fj[gr]]then
call ShowImage(Qj[gr],false)
endif
return true
endfunction
function P3 takes nothing returns boolean
local integer gr=Eq
local integer iw=Fe[gr]
local integer qw
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=He[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=Je[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=Ne[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=Re[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=Ve[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=Ye[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=ef[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=hf[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=nf[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=rf[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
set iw=vf[gr]
loop
exitwhen iw==0
set qw=ze[iw]
call Os(we[iw])
set iw=qw
endloop
call Ut("MinionBuffStorage "+I2S(gr)+" is deleted.")
return true
endfunction
function Q3 takes nothing returns boolean
local player p=Cq
local integer i=aq
local integer gr=js()
local integer a
set Bf[gr]=p
set Ff[gr]=p==S
set Cf[gr]=GetPlayerName(p)
call mx(gr)
set bf[gr]=Ju(gr)
call Lu(gr)
set z4[i]=gr
set cf[gr]=fv(GetPlayerColor(p))
set A4[cf[gr]]=true
if GetPlayerColor(p)==Y then
set a4=p
endif
set If[gr]=Nv(gr)
set kg[gr]=mr()
set mg[gr]=nr()
set ng[gr]=pr()
set og[gr]=qr()
set pg[gr]=sr()
set qg[gr]=rr()
set a=0
loop
call SetPlayerTechMaxAllowed(p,1949315120+a,gp[(a)])
set a=a+1
exitwhen a==j7
endloop
set a=1
loop
set yg[zg[gr]+a]=ex(a)
set a=a+1
exitwhen a==32
endloop
set a=1
loop
set wg[xg[gr]+a]=gx(a)
set a=a+1
exitwhen a==32
endloop
set a=1
loop
set sg[tg[gr]+a]=hx(a)
call SetPlayerAbilityAvailable(p,vo[wo[i7[a]]],false)
call SetPlayerAbilityAvailable(p,vo[wo[i7[a]]+1],false)
call SetPlayerAbilityAvailable(p,vo[wo[i7[a]]+2],false)
set a=a+1
exitwhen a==32
endloop
set a=1
loop
set ag[Bg[gr]+a]=tr()
call SetPlayerAbilityAvailable(p,yn[f7[a]],false)
call SetPlayerAbilityAvailable(p,xn[f7[a]],false)
call SetPlayerAbilityAvailable(p,wn[f7[a]],false)
call SetPlayerAbilityAvailable(p,Cn[cn[f7[a]]],false)
call SetPlayerAbilityAvailable(p,Cn[cn[f7[a]]+1],false)
call SetPlayerAbilityAvailable(p,Bn[bn[f7[a]]],false)
call SetPlayerAbilityAvailable(p,Bn[bn[f7[a]]+1],false)
set a=a+1
exitwhen a==24
endloop
call TriggerRegisterPlayerUnitEvent(U7,p,EVENT_PLAYER_UNIT_CONSTRUCT_START,null)
call TriggerRegisterPlayerUnitEvent(V7,p,EVENT_PLAYER_UNIT_CONSTRUCT_CANCEL,null)
call TriggerRegisterPlayerUnitEvent(W7,p,EVENT_PLAYER_UNIT_CONSTRUCT_FINISH,null)
call TriggerRegisterPlayerUnitEvent(f8,p,EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerRegisterPlayerUnitEvent(g8,p,EVENT_PLAYER_UNIT_SPELL_ENDCAST,null)
call TriggerRegisterPlayerUnitEvent(h8,p,EVENT_PLAYER_UNIT_ISSUED_ORDER,null)
call TriggerRegisterPlayerUnitEvent(i8,p,EVENT_PLAYER_UNIT_ISSUED_POINT_ORDER,null)
call TriggerRegisterPlayerUnitEvent(X7,p,EVENT_PLAYER_UNIT_UPGRADE_START,null)
call TriggerRegisterPlayerUnitEvent(Y7,p,EVENT_PLAYER_UNIT_UPGRADE_CANCEL,null)
call TriggerRegisterPlayerUnitEvent(Z7,p,EVENT_PLAYER_UNIT_UPGRADE_FINISH,null)
call TriggerRegisterPlayerUnitEvent(j8,p,EVENT_PLAYER_UNIT_SELECTED,null)
call TriggerRegisterPlayerUnitEvent(k8,p,EVENT_PLAYER_UNIT_DESELECTED,null)
call TriggerRegisterPlayerEvent(T7,p,EVENT_PLAYER_LEAVE)
set Aq=gr
return true
endfunction
function R3 takes nothing returns boolean
local integer gr=Eq
local integer k=0
local real x=Vd[If[gr]]
local real y=320.
set Gf[gr]=W[bf[gr]*2/(Q8+1)]
call Qv(If[gr])
loop
set Zf[dg[gr]+k]=ru(Bf[gr],"Textures\\Pavement.blp",256.,x,y,4)
set x=x+128.
if x>Sd[If[gr]]then
set y=y-128.
set x=Vd[If[gr]]
endif
set k=k+1
exitwhen k==120
endloop
set x=(Td[If[gr]]+Sd[If[gr]])/ 2.
set y=(n4+1152.)/ 2.
set fg[gr]=CreateUnit(Bf[gr],1112294482,x,y,.0)
set k=bf[gr]
call SetHeroXP(fg[gr],k,false)
if Ff[gr]then
call PanCameraToTimed(x,y,.0)
call SelectUnit(fg[gr],true)
endif
set y=1920.-64.
set gg[gr]=CreateUnit(Bf[gr],1112689491,x-128.,y,270.)
call SetUnitVertexColor(gg[gr],$FF,$FF,$FF,64)
call SetHeroXP(gg[gr],k,false)
set hg[gr]=CreateUnit(Bf[gr],1112232788,x-384.,y,270.)
call SetUnitVertexColor(hg[gr],$FF,$FF,$FF,64)
call SetHeroXP(hg[gr],k,false)
set ig[gr]=CreateUnit(Bf[gr],1095521362,x+128.,y,270.)
call SetUnitVertexColor(ig[gr],$FF,$FF,$FF,64)
call SetHeroXP(ig[gr],k,false)
set jg[gr]=CreateUnit(Bf[gr],1296256336,x+384.,y,270.)
call SetUnitVertexColor(jg[gr],$FF,$FF,$FF,64)
call SetHeroXP(jg[gr],k,false)
set lg[gr]=kv(hg[gr],1227894850)
set Jg[gr]=kv(hg[gr],1227894851)
call SetItemCharges(Jg[gr],N4)
call jx(gr)
set Hf[gr]=true
call Ut("Player "+I2S(gr)+" is ready.")
return true
endfunction
function S3 takes nothing returns boolean
local integer gr=Eq
local integer a=0
loop
call DestroyImage(Zf[dg[gr]+a])
set Zf[dg[gr]+a]=null
set a=a+1
exitwhen a==120
endloop
return true
endfunction
function T3 takes nothing returns boolean
local integer gr=Eq
call ya(gr)
call Ut("MinionAbility "+I2S(gr)+" is deleted.")
return true
endfunction
function U3 takes nothing returns boolean
local integer gr=Eq
return true
endfunction
function V3 takes nothing returns boolean
local integer gr=Eq
local integer ns=aq
return true
endfunction
function W3 takes nothing returns boolean
local integer gr=Eq
local integer ns=aq
local integer ps=Bq
local integer qs=bq
set Gq=false
return true
endfunction
function X3 takes nothing returns boolean
local integer gr=Eq
local real r=cq
if r>.0 and not(Ji[gr])then
set r=TimerGetRemaining(Ki[gr])-r
if r<=.0 then
call TimerStart((Ki[gr]),.0,false,null)
call ks(gr)
else
call TimerStart(Ki[gr],r,false,function za)
endif
endif
return true
endfunction
function Y3 takes nothing returns boolean
local integer gr=Eq
return true
endfunction
function Z3 takes nothing returns boolean
local integer gr=Eq
return true
endfunction
function dF takes nothing returns boolean
local integer gr=Eq
call sa(gr)
call DestroyImage(ci[gr])
set ci[gr]=null
set Ci[gr]=null
return true
endfunction
function eF takes nothing returns boolean
local integer gr=Eq
call RemoveUnit(vi[gr])
set vi[gr]=null
call Ut("MinionDummy "+I2S(gr)+" is deleted.")
return true
endfunction
function fF takes nothing returns boolean
local integer gr=Eq
if Yg[gr]!=0 then
call vs(Yg[gr])
endif
call SetUnitUserData(Vg[gr],0)
set Vg[gr]=null
return true
endfunction
function gF takes nothing returns boolean
local integer gr=Eq
call Tu(mg[Sg[gr]],gr)
call px(Sg[gr],Rg[gr])
call lx(Sg[gr],Un[hh[gr]])
call Ax(Sg[gr],-Xn[hh[gr]])
call Bx(Sg[gr],Tn[hh[gr]])
if Tn[hh[gr]]>0 then
call Ex(Sg[gr],I2S(Tn[hh[gr]]),Vg[gr])
endif
call DestroyEffect(AddSpecialEffect(("Abilities\\Spells\\Other\\Transmute\\PileofGold.mdx"),((GetUnitX(Vg[gr]))*1.),((GetUnitY(Vg[gr]))*1.)))
call RemoveUnit(Vg[gr])
call Ls(gr)
call Ut("Minion "+I2S(gr)+" is sold.")
return true
endfunction
function hF takes nothing returns boolean
local integer gr=Eq
set Fq=ko[hh[gr]]
return true
endfunction
function iF takes nothing returns boolean
local integer gr=Eq
set Aq=mo[hh[gr]]
return true
endfunction
function jF takes nothing returns boolean
local integer gr=Eq
set Aq=no[hh[gr]]
return true
endfunction
function kF takes nothing returns boolean
local integer gr=Eq
set Gq=(Ue[Xg[(gr)]])
return true
endfunction
function mF takes nothing returns boolean
local integer gr=Eq
set Gq=false
return true
endfunction
function nF takes nothing returns boolean
local integer gr=Eq
set Gq=false
return true
endfunction
function oF takes nothing returns boolean
local integer gr=Eq
set Gq=false
return true
endfunction
function pF takes nothing returns boolean
local integer gr=Eq
set Gq=false
return true
endfunction
function qF takes nothing returns boolean
local integer gr=Eq
local real Gs=cq
set Fq=oz(gr,Gs)
return true
endfunction
function rF takes nothing returns boolean
local integer gr=Eq
local real Gs=cq
set Fq=pz(gr,Gs)
return true
endfunction
function sF takes nothing returns boolean
local integer gr=Eq
local real Gs=cq
set Fq=(fh[(gr)]*((Gs)*1.)/ 1000.)
return true
endfunction
function tF takes nothing returns boolean
local integer gr=Eq
local boolean Js=Dq
call SetUnitExploded(Vg[gr],Js)
call KillUnit(Vg[gr])
if Js or Rn[hh[gr]]then
call xz(gr)
else
set Zg[gr]=false
call wz(gr)
call Yu(qg[Tg[gr]],gr)
call ss(Yg[gr])
set sh[gr]=GetUnitX(Vg[gr])
set th[gr]=GetUnitY(Vg[gr])
set uh[gr]=GetUnitFacing(Vg[gr])
call TimerStart(xh[gr],15.,false,function cz)
endif
return true
endfunction
function uF takes nothing returns boolean
local integer gr=Eq
call Sz(gr)
call RemoveSavedInteger(n7,Wg[bh[gr]],Bh[gr])
call UnitRemoveAbility(Vg[bh[gr]],ri[Dh[gr]])
set Ch[gr]=null
return true
endfunction
function vF takes nothing returns boolean
local integer gr=Eq
set Gq=true
return true
endfunction
function wF takes nothing returns boolean
local integer gr=Eq
call gz(bh[gr],Gh[gr])
if gi[gr]then
call Aw(Xg[(bh[gr])],(Hh[gr]))
endif
set Eq=gr
return true
endfunction
function xF takes nothing returns boolean
local integer gr=Eq
call jw(Xg[(bh[gr])],(Gh[gr]))
set Eq=gr
return true
endfunction
function yF takes nothing returns boolean
local integer gr=Eq
call zy(bh[gr],kj[bf[ch[gr]]],"Effects\\RestoreHealthGreen.mdx","origin")
set Gq=Eh[gr]==0
return true
endfunction
function zF takes nothing returns boolean
local integer gr=Eq
call jz(bh[gr],Gh[gr])
call gz(bh[gr],Hh[gr])
set Eq=gr
return true
endfunction
function AF takes nothing returns boolean
local integer gr=Eq
call lw(Xg[(bh[gr])],(Gh[gr]))
if fi[gr]then
call Aw(Xg[(bh[gr])],(Hh[gr]))
endif
call AddUnitAnimationProperties(Vg[bh[gr]],"Defend",false)
set Eq=gr
return true
endfunction
function aF takes nothing returns boolean
local integer gr=Eq
if oo[po[sg[tg[ch[gr]]+2]]+2]then
call KA(bh[gr],be[(Gh[gr])]*.5)
endif
set Gq=true
return true
endfunction
function BF takes nothing returns boolean
local integer gr=Eq
call fz(bh[gr],Gh[gr])
set Eq=gr
return true
endfunction
function bF takes nothing returns boolean
local integer gr=Eq
call Ow(Xg[(bh[gr])],(Gh[gr]))
set Eq=gr
return true
endfunction
function CF takes nothing returns boolean
local integer gr=Eq
local integer fx=sg[tg[ch[gr]]+4]
if oo[po[fx]+2]then
call xy(bh[gr],be[(Gh[gr])]*.5,"Effects\\RestoreHealthGreen.mdx","origin")
endif
if oo[po[fx]+0]then
call FA(bh[gr],Ch[gr],ch[gr],40)
endif
set Gq=true
return true
endfunction
function cF takes nothing returns boolean
local integer gr=Eq
call iz(bh[gr],Gh[gr])
set Eq=gr
return true
endfunction
function DF takes nothing returns boolean
local integer gr=Eq
call RemoveUnit(Mh[gr])
set Mh[gr]=null
call Ut("TowerAttacker "+I2S(gr)+" is deleted.")
return true
endfunction
function EF takes nothing returns boolean
local integer gr=Eq
call kz(bh[gr],Gh[gr])
set Eq=gr
return true
endfunction
function FF takes nothing returns boolean
local integer gr=Eq
if(H9[I9[(ag[Bg[ch[gr]]+2])]+(0)])then
call UA(bh[gr],.15)
endif
set Gq=true
return true
endfunction
function GF takes nothing returns boolean
local integer gr=Eq
call kz(bh[gr],Gh[gr])
set Eq=gr
return true
endfunction
function HF takes nothing returns boolean
local integer gr=Eq
call jw(Xg[(bh[gr])],(Gh[gr]))
set Eq=gr
return true
endfunction
function IF takes nothing returns boolean
local integer gr=Eq
if UnitAlive(Ch[gr])then
call rs(Yg[(GetUnitUserData(Ch[gr]))],1.)
set Gq=Eh[gr]==0
return true
endif
set Gq=true
return true
endfunction
function lF takes nothing returns boolean
local integer gr=Eq
local integer ns=aq
local integer ps=Bq
local integer qs=bq
if Sg[ns]==Sg[Ii[gr]]then
call IA(ns,Ii[gr])
call Ba(gr)
set Gq=true
return true
endif
set Gq=false
return true
endfunction
function JF takes nothing returns boolean
local integer gr=Eq
local integer ns=aq
local integer ps=Bq
local integer qs=bq
if qs==1227894834 then
call MA(ns)
call Ba(gr)
set Gq=true
return true
endif
set Gq=false
return true
endfunction
function KF takes nothing returns boolean
local integer gr=Eq
if Ri[gr]!=0 then
call wa(Ri[gr])
endif
set Eq=gr
return true
endfunction
function LF takes nothing returns boolean
local integer gr=Eq
local integer m=Ii[gr]
set Ti=m
set Ui=false
call GroupEnumUnitsInRange(bj_lastCreatedGroup,GetUnitX(Vg[m]),GetUnitY(Vg[m]),yk[li[gr]],Qi)
if oo[po[hh[m]]+1]then
set Si[gr]=gu(Ui)*nk
call Jy(m)
endif
return true
endfunction
function MF takes nothing returns boolean
local integer gr=Eq
call wa(Ri[gr])
set Ri[gr]=0
call PauseTimer(Ki[gr])
return true
endfunction
function NF takes nothing returns boolean
local integer gr=Eq
set Ri[gr]=ua(Vg[Ii[gr]],yk[li[gr]])
call Ba(gr)
return true
endfunction
function OF takes nothing returns boolean
local integer gr=Eq
if gj[gr]!=0 then
call wa(gj[gr])
endif
set Eq=gr
return true
endfunction
function PF takes nothing returns boolean
local integer gr=Eq
local integer m=Ii[gr]
local integer QF=bf[Sg[m]]
local integer Ey
set hj=Sg[m]
set ij=0
call GroupEnumUnitsInRange(bj_lastCreatedGroup,GetUnitX(Vg[m]),GetUnitY(Vg[m]),yk[li[gr]],Yi)
set Ey=ij*Wi[QF]
if Ey>Xi[QF]then
set Ey=Xi[QF]
endif
set fj[gr]=Ey
set ej[gr]=Ey
call Jy(m)
return true
endfunction
function RF takes nothing returns boolean
local integer gr=Eq
if Zi[gr]then
call AddUnitAnimationProperties(Vg[Ii[gr]],"Defend",false)
else
call wa(gj[gr])
set gj[gr]=0
call PauseTimer(Ki[gr])
endif
return true
endfunction
function SF takes nothing returns boolean
local integer gr=Eq
if Zi[gr]then
call AddUnitAnimationProperties(Vg[Ii[gr]],"Defend",true)
else
set gj[gr]=ua(Vg[Ii[gr]],yk[li[gr]])
call Ba(gr)
endif
return true
endfunction
function TF takes nothing returns boolean
local integer gr=Eq
call qa(mj[gr])
set Eq=gr
return true
endfunction
function UF takes nothing returns boolean
local integer gr=Eq
local integer ns=aq
local integer ps=Bq
local integer qs=bq
if ns!=Ii[gr]and Sg[ns]==Sg[Ii[gr]]and ry(ns)<=(1.-kj[bf[Sg[Ii[gr]]]])then
call na(mj[gr],GetUnitX(Vg[Ii[gr]]),GetUnitY(Vg[Ii[gr]]))
call oa(mj[gr],GetUnitX(Vg[ns]),GetUnitY(Vg[ns]))
call Ba(gr)
set Gq=true
return true
endif
set Gq=false
return true
endfunction
function VF takes nothing returns boolean
local integer gr=Eq
local integer ns=aq
local integer p=Sg[Ii[gr]]
if ns!=Ii[gr]and Sg[ns]==p then
call zy(ns,kj[bf[Sg[Ii[gr]]]],"Effects\\RestoreHealthGreen.mdx","origin")
if(H9[I9[(ag[Bg[p]+1])]+(1)])then
call RA(ns,Ii[gr])
endif
endif
return true
endfunction
function WF takes nothing returns boolean
local integer gr=Eq
local integer ns=aq
local integer ps=Bq
local integer qs=bq
call WA(ns)
if(H9[I9[(ag[Bg[Sg[ns]]+2])]+(1)])then
set ns=Sa(gr)
if ns!=0 then
call YA(ns,Ii[gr])
endif
endif
call Ba(gr)
set Gq=true
return true
endfunction
function XF takes nothing returns boolean
local integer gr=Eq
call Va(gr)
set Eq=gr
return true
endfunction
function YF takes nothing returns boolean
local integer gr=Eq
call Ya(gr)
return true
endfunction
function ZF takes nothing returns boolean
local integer gr=Eq
call jw(Xg[(bh[gr])],(Gh[gr]))
set Eq=gr
return true
endfunction
function dG takes nothing returns boolean
local integer gr=Eq
local integer dd
if(GetUnitTypeId((Ch[gr]))!=0)then
set dd=GetUnitUserData(Ch[gr])
set Gq=wB(dd,bh[gr],Mk[(ek[dd])],1227894841,false)and Eh[gr]==0
return true
endif
set Gq=true
return true
endfunction
function eG takes nothing returns boolean
local integer gr=Eq
call gz(bh[gr],Gh[gr])
set Eq=gr
return true
endfunction
function fG takes nothing returns boolean
local integer gr=Eq
call jz(bh[gr],Gh[gr])
set Eq=gr
return true
endfunction
function gG takes nothing returns boolean
local integer gr=Eq
call gz(bh[gr],Gh[gr])
set Eq=gr
return true
endfunction
function hG takes nothing returns boolean
local integer gr=Eq
call hz(bh[gr],Gh[gr])
set Eq=gr
return true
endfunction
function iG takes nothing returns boolean
local integer gr=Eq
call jz(bh[gr],Gh[gr])
set Eq=gr
return true
endfunction
function jG takes nothing returns boolean
local integer gr=Eq
call fz(bh[gr],Gh[gr])
set Eq=gr
return true
endfunction
function kG takes nothing returns boolean
local integer gr=Eq
set Fq=(ko[hh[(gr)]])+Oi[(Yg[gr])]
return true
endfunction
function mG takes nothing returns boolean
local integer gr=Eq
set Aq=(mo[hh[(gr)]])+Ni[(Yg[gr])]
return true
endfunction
function nG takes nothing returns boolean
local integer gr=Eq
if oo[po[hh[gr]]+2]then
call ob(mk[gr])
endif
call hy(gr)
return true
endfunction
function oG takes nothing returns boolean
local integer gr=Eq
set Aq=(mo[hh[(gr)]])+Si[(Yg[gr])]
return true
endfunction
function pG takes nothing returns boolean
local integer gr=Eq
set Aq=(no[hh[(gr)]])+Si[(Yg[gr])]
return true
endfunction
function qG takes nothing returns boolean
local integer gr=Eq
set Fq=(ko[hh[(gr)]])+dj[(Yg[gr])]
return true
endfunction
function rG takes nothing returns boolean
local integer gr=Eq
set Aq=(mo[hh[(gr)]])+fj[(Yg[gr])]
return true
endfunction
function sG takes nothing returns boolean
local integer gr=Eq
set Aq=(no[hh[(gr)]])+ej[(Yg[gr])]
return true
endfunction
function tG takes nothing returns boolean
local integer gr=Eq
set Gq=(Ue[Xg[((gr))]])or(oo[po[hh[gr]]+2]and Zi[(Yg[gr])])
return true
endfunction
function uG takes nothing returns boolean
local integer gr=Eq
set Gq=Zi[(Yg[gr])]
return true
endfunction
function vG takes nothing returns boolean
local integer gr=Eq
local real Gs=cq
if Zi[(Yg[gr])]then
set Fq=.0
return true
endif
set Fq=oz(gr,Gs)
return true
endfunction
function wG takes nothing returns boolean
local integer gr=Eq
call Pb(gr)
set Ek[gr]=null
set Eq=gr
return true
endfunction
function xG takes nothing returns boolean
local integer gr=Eq
local integer Gz=Uj[Ck[gr]]
if Gz<=Ym[bk[gr]]then
set Hk[gr]=Hk[gr]+.1
set Ik[gr]=Ik[gr]+.2
if Gz==Xm[bk[gr]]then
set Hk[gr]=Hk[gr]+1.
elseif Gz==Ym[bk[gr]]then
set Ik[gr]=Ik[gr]+2.
endif
call yB(Ck[gr])
endif
return true
endfunction
function yG takes nothing returns boolean
local integer gr=Eq
set Mk[gr]=Mk[gr]+Mk[gr]*.075
return true
endfunction
function zG takes nothing returns boolean
local integer gr=Eq
if Ok[gr]!=0 then
call qA(Ok[gr])
endif
set Eq=gr
return true
endfunction
function AG takes nothing returns boolean
local integer gr=Eq
if Uj[Ck[gr]]==Xm[bk[gr]]then
call mA(Oj[Ck[gr]])
elseif Uj[Ck[gr]]==Ym[bk[gr]]then
set Ok[gr]=gA(Ck[gr])
endif
return true
endfunction
function aG takes nothing returns boolean
local integer gr=Eq
call qA(Pk[gr])
call RemoveUnit(Qk[gr])
call fC(gr)
set Qk[gr]=null
set Rk[gr]=null
set Vk[gr]=null
set Eq=gr
return true
endfunction
function BG takes nothing returns boolean
local integer gr=Eq
call oC(gr)
set Eq=gr
return true
endfunction
function bG takes nothing returns boolean
local integer gr=Eq
set Xk[gr]=Xk[gr]+1
if Uj[Ck[gr]]==Xm[bk[gr]]then
set Xk[gr]=Xk[gr]+1
elseif Uj[Ck[gr]]==Ym[bk[gr]]then
set Xk[gr]=Xk[gr]+3
endif
return true
endfunction
function CG takes nothing returns boolean
local integer gr=Eq
call sC(gr)
call DestroyImage(mm[gr])
set mm[gr]=null
set pm[gr]=null
call vC(gr)
set Eq=gr
return true
endfunction
function cG takes nothing returns boolean
local integer gr=Eq
if Uj[Ck[gr]]==Xm[bk[gr]]then
set jm[gr]=jm[gr]*2.
set om[gr]=om[gr]*2.
set sm[gr]=sm[gr]+5
elseif Uj[Ck[gr]]==Ym[bk[gr]]then
set km[gr]=km[gr]+128.
set nm[gr]=nm[gr]*2
set sm[gr]=sm[gr]+5
call DestroyImage(mm[gr])
set mm[gr]=oB(Ck[gr],km[gr],dm,em,fm,im[gr]==0)
elseif Uj[Ck[gr]]==Zm[bk[gr]]and TimerGetRemaining(um[gr])>=3. then
call TimerStart(um[gr],3.,true,hm)
endif
return true
endfunction
function DG takes nothing returns boolean
local integer gr=Eq
call NC(gr)
call PC(gr)
set Eq=gr
return true
endfunction
function EG takes nothing returns boolean
local integer gr=Eq
call XC(gr)
set Tm[gr]=null
set Eq=gr
return true
endfunction
function FG takes nothing returns boolean
local integer gr=Eq
set Pm[gr]=Pm[gr]+.01
set Qm[gr]=Qm[gr]+Nm
return true
endfunction
function GG takes nothing returns boolean
local integer gr=Eq
set Fq=Rb(ek[gr])+tB(gr)
return true
endfunction
function HG takes nothing returns boolean
local integer gr=Eq
local integer m=aq
call AB(gr,m)
call Ub(ek[gr])
return true
endfunction
function IG takes nothing returns boolean
local integer gr=Eq
call vB(gr)
if Yt(Uj[gr],5)then
set Mj[gr]=Mj[gr]+1
endif
return true
endfunction
function lG takes nothing returns boolean
local integer gr=Eq
local integer m=aq
if AB(gr,m)then
call Yb(ek[gr],m)
endif
return true
endfunction
function JG takes nothing returns boolean
local integer gr=Eq
local integer m=aq
if AB(gr,m)then
call ZB((m),Ck[(ek[gr])],Nk)
endif
return true
endfunction
function KG takes nothing returns boolean
local integer gr=Eq
local integer Vr=aq
if dC(ek[gr],Vr)then
call aB(gr)
else
call TimerStart(Pj[(gr)],O4,false,gk)
endif
return true
endfunction
function LG takes nothing returns boolean
local integer gr=Eq
set Fq=Lo[Nj[gr]]+.5*Uj[gr]
return true
endfunction
function MG takes nothing returns boolean
local integer gr=Eq
set Fq=Tk[(ek[gr])]+tB(gr)
return true
endfunction
function NG takes nothing returns boolean
local integer gr=Eq
local integer m=aq
call mC(ek[gr],m)
return true
endfunction
function OG takes nothing returns boolean
local integer gr=Eq
call DC(ek[gr])
return true
endfunction
function PG takes nothing returns boolean
local integer gr=Eq
call EC(ek[gr])
return true
endfunction
function QG takes nothing returns boolean
local integer gr=Eq
set Fq=cm[(ek[gr])]+sB(gr)
return true
endfunction
function RG takes nothing returns boolean
local integer gr=Eq
set Fq=Cm[(ek[gr])]+tB(gr)
return true
endfunction
function SG takes nothing returns boolean
local integer gr=Eq
local integer m=aq
call KB(gr,m)
call KC(ek[gr])
return true
endfunction
function TG takes nothing returns boolean
local integer gr=Eq
local integer m=aq
call LC(ek[gr],m)
return true
endfunction
function UG takes nothing returns boolean
local integer gr=Eq
set Fq=Hm[(ek[gr])]+tB(gr)
return true
endfunction
function VG takes nothing returns boolean
local integer gr=Eq
local integer m=aq
call AB(gr,m)
call RC(ek[gr])
return true
endfunction
function WG takes nothing returns boolean
local integer gr=Eq
local integer Vr=aq
local integer m=vA(Vr)
call SC(ek[gr],GetUnitX(Vg[m]),GetUnitY(Vg[m]))
call aB(gr)
return true
endfunction
function XG takes nothing returns boolean
local integer YG=aq
local integer ps=Bq
local integer qs=bq
local boolean ZG=not nz(YG,qs)
local integer a
local integer fc
local integer d6
if ZG then
set ZG=ba(Yg[YG],YG,ps,qs)
set fc=og[Tg[YG]]
set a=0
loop
exitwhen ZG or a>u9[fc]
set d6=Yg[(s9[t9[(fc)]+(a)])]
if uk[li[d6]]then
set ZG=ba(d6,YG,ps,qs)
endif
set a=a+1
endloop
endif
return true
endfunction
function e6 takes nothing returns boolean
local integer gr=aq
local integer a=0
set a=0
loop
call Gv(Qd[Rd[gr]+a])
set a=a+1
exitwhen a==608
endloop
call Ut("Neighbors are ready!")
return true
endfunction
function f6 takes nothing returns boolean
local integer gr=aq
local integer a=0
loop
set Qd[Rd[gr]+a]=Fv(gr,a)
set a=a+1
exitwhen a==608
endloop
call cr(gr)
call Ut("Cells are ready!")
return true
endfunction
function g6 takes nothing returns boolean
local integer gr=aq
local integer a=0
loop
call Br(Qd[Rd[gr]+a])
set a=a+1
exitwhen a==608
endloop
call Ut("Cells destroyed!")
return true
endfunction
function h6 takes nothing returns boolean
local integer gr=aq
local integer q=Bq
local integer c
local integer d
set je[gr]=true
loop
set c=Iv(q)
set d=yd[c]+1
call Kv(q,Cd[c],d)
call Kv(q,Dd[c],d)
call Kv(q,Ed[c],d)
call Kv(q,Fd[c],d)
exitwhen(Kd[(q)]==0)
endloop
set ie[gr]=not bd[Qd[Rd[gr]]]
set je[gr]=false
return true
endfunction
function i6 takes nothing returns boolean
local integer gr=aq
local integer a=0
local boolean F3=ie[gr]
local integer c
loop
set c=Qd[Rd[gr]+a]
set bd[(c)]=true
if F3 then
set xd[(c)]=(yd[c])
set yd[(c)]=(0)
endif
set a=a+1
exitwhen a==t4
endloop
return true
endfunction
function j6 takes nothing returns boolean
local integer gr=aq
local integer a=0
loop
call DestroyImage(Zf[dg[gr]+a])
set Zf[dg[gr]+a]=null
set a=a+1
exitwhen a==120
endloop
return true
endfunction
function k6 takes nothing returns boolean
local integer gr=aq
local boolean Js=Dq
call SetUnitExploded(Vg[gr],Js)
call KillUnit(Vg[gr])
if Js or Rn[hh[gr]]then
call xz(gr)
else
set Zg[gr]=false
call wz(gr)
call Yu(qg[Tg[gr]],gr)
call ss(Yg[gr])
set sh[gr]=GetUnitX(Vg[gr])
set th[gr]=GetUnitY(Vg[gr])
set uh[gr]=GetUnitFacing(Vg[gr])
call TimerStart(xh[gr],15.,false,function cz)
endif
return true
endfunction
function m6 takes nothing returns boolean
local integer gr=aq
local integer Vr=Bq
if BB(gr,Vr,Oj[gr])then
call aB(gr)
else
call TimerStart(Pj[(gr)],O4,false,gk)
endif
return true
endfunction
function n6 takes nothing returns boolean
local player p=Cq
local integer i=aq
local integer gr=js()
local integer a
set Bf[gr]=p
set Ff[gr]=p==S
set Cf[gr]=GetPlayerName(p)
call mx(gr)
set bf[gr]=Ju(gr)
call Lu(gr)
set z4[i]=gr
set cf[gr]=fv(GetPlayerColor(p))
set A4[cf[gr]]=true
if GetPlayerColor(p)==Y then
set a4=p
endif
set If[gr]=Nv(gr)
set kg[gr]=mr()
set mg[gr]=nr()
set ng[gr]=pr()
set og[gr]=qr()
set pg[gr]=sr()
set qg[gr]=rr()
set a=0
loop
call SetPlayerTechMaxAllowed(p,1949315120+a,gp[(a)])
set a=a+1
exitwhen a==j7
endloop
set a=1
loop
set yg[zg[gr]+a]=ex(a)
set a=a+1
exitwhen a==32
endloop
set a=1
loop
set wg[xg[gr]+a]=gx(a)
set a=a+1
exitwhen a==32
endloop
set a=1
loop
set sg[tg[gr]+a]=hx(a)
call SetPlayerAbilityAvailable(p,vo[wo[i7[a]]],false)
call SetPlayerAbilityAvailable(p,vo[wo[i7[a]]+1],false)
call SetPlayerAbilityAvailable(p,vo[wo[i7[a]]+2],false)
set a=a+1
exitwhen a==32
endloop
set a=1
loop
set ag[Bg[gr]+a]=tr()
call SetPlayerAbilityAvailable(p,yn[f7[a]],false)
call SetPlayerAbilityAvailable(p,xn[f7[a]],false)
call SetPlayerAbilityAvailable(p,wn[f7[a]],false)
call SetPlayerAbilityAvailable(p,Cn[cn[f7[a]]],false)
call SetPlayerAbilityAvailable(p,Cn[cn[f7[a]]+1],false)
call SetPlayerAbilityAvailable(p,Bn[bn[f7[a]]],false)
call SetPlayerAbilityAvailable(p,Bn[bn[f7[a]]+1],false)
set a=a+1
exitwhen a==24
endloop
call TriggerRegisterPlayerUnitEvent(U7,p,EVENT_PLAYER_UNIT_CONSTRUCT_START,null)
call TriggerRegisterPlayerUnitEvent(V7,p,EVENT_PLAYER_UNIT_CONSTRUCT_CANCEL,null)
call TriggerRegisterPlayerUnitEvent(W7,p,EVENT_PLAYER_UNIT_CONSTRUCT_FINISH,null)
call TriggerRegisterPlayerUnitEvent(f8,p,EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerRegisterPlayerUnitEvent(g8,p,EVENT_PLAYER_UNIT_SPELL_ENDCAST,null)
call TriggerRegisterPlayerUnitEvent(h8,p,EVENT_PLAYER_UNIT_ISSUED_ORDER,null)
call TriggerRegisterPlayerUnitEvent(i8,p,EVENT_PLAYER_UNIT_ISSUED_POINT_ORDER,null)
call TriggerRegisterPlayerUnitEvent(X7,p,EVENT_PLAYER_UNIT_UPGRADE_START,null)
call TriggerRegisterPlayerUnitEvent(Y7,p,EVENT_PLAYER_UNIT_UPGRADE_CANCEL,null)
call TriggerRegisterPlayerUnitEvent(Z7,p,EVENT_PLAYER_UNIT_UPGRADE_FINISH,null)
call TriggerRegisterPlayerUnitEvent(j8,p,EVENT_PLAYER_UNIT_SELECTED,null)
call TriggerRegisterPlayerUnitEvent(k8,p,EVENT_PLAYER_UNIT_DESELECTED,null)
call TriggerRegisterPlayerEvent(T7,p,EVENT_PLAYER_LEAVE)
set Aq=gr
return true
endfunction
function z3 takes nothing returns nothing
set uq=CreateTrigger()
call TriggerAddCondition(uq,Condition(function A3))
set Pp[85]=CreateTrigger()
set Pp[89]=Pp[85]
set Pp[92]=Pp[85]
set Pp[93]=Pp[85]
call TriggerAddCondition(Pp[85],Condition(function B3))
call TriggerAddAction(Pp[85],function B3)
set eq[85]=null
set eq[87]=null
set eq[92]=null
set fq=CreateTrigger()
call TriggerAddCondition(fq,Condition(function C3))
call TriggerAddAction(fq,function C3)
set gq=CreateTrigger()
call TriggerAddCondition(gq,Condition(function c3))
call TriggerAddAction(gq,function c3)
set hq=CreateTrigger()
call TriggerAddCondition(hq,Condition(function D3))
call TriggerAddAction(hq,function D3)
set iq=CreateTrigger()
call TriggerAddCondition(iq,Condition(function E3))
call TriggerAddAction(iq,function E3)
set jq=CreateTrigger()
call TriggerAddCondition(jq,Condition(function G3))
call TriggerAddAction(jq,function G3)
set Rp[31]=null
set Rp[32]=null
set Rp[33]=null
set Rp[34]=null
set Hp[70]=CreateTrigger()
set Hp[96]=Hp[70]
set Hp[97]=Hp[70]
set Hp[98]=Hp[70]
set Hp[99]=Hp[70]
set Hp[101]=Hp[70]
set Hp[102]=Hp[70]
set Hp[103]=Hp[70]
set Hp[104]=Hp[70]
set Hp[105]=Hp[70]
call TriggerAddCondition(Hp[70],Condition(function H3))
call TriggerAddAction(Hp[70],function H3)
set Ip[70]=CreateTrigger()
set Ip[96]=Ip[70]
set Ip[97]=Ip[70]
set Ip[98]=Ip[70]
set Ip[99]=Ip[70]
set Ip[100]=Ip[70]
set Ip[101]=Ip[70]
set Ip[102]=Ip[70]
set Ip[104]=Ip[70]
set Ip[105]=Ip[70]
call TriggerAddCondition(Ip[70],Condition(function I3))
call TriggerAddAction(Ip[70],function I3)
set lp[70]=CreateTrigger()
set lp[96]=lp[70]
set lp[98]=lp[70]
set lp[99]=lp[70]
set lp[101]=lp[70]
set lp[102]=lp[70]
set lp[105]=lp[70]
call TriggerAddCondition(lp[70],Condition(function l3))
call TriggerAddAction(lp[70],function l3)
set Jp[70]=CreateTrigger()
set Jp[96]=Jp[70]
set Jp[97]=Jp[70]
set Jp[99]=Jp[70]
set Jp[100]=Jp[70]
set Jp[101]=Jp[70]
set Jp[102]=Jp[70]
set Jp[103]=Jp[70]
set Jp[104]=Jp[70]
set Jp[105]=Jp[70]
call TriggerAddCondition(Jp[70],Condition(function J3))
call TriggerAddAction(Jp[70],function J3)
set Kp[70]=CreateTrigger()
set Kp[96]=Kp[70]
set Kp[101]=Kp[70]
set Kp[102]=Kp[70]
set Kp[105]=Kp[70]
call TriggerAddCondition(Kp[70],Condition(function K3))
call TriggerAddAction(Kp[70],function K3)
set Lp[70]=CreateTrigger()
set Lp[96]=Lp[70]
set Lp[97]=Lp[70]
set Lp[98]=Lp[70]
set Lp[100]=Lp[70]
set Lp[101]=Lp[70]
set Lp[102]=Lp[70]
set Lp[103]=Lp[70]
set Lp[105]=Lp[70]
call TriggerAddCondition(Lp[70],Condition(function L3))
call TriggerAddAction(Lp[70],function L3)
set Mp[70]=CreateTrigger()
set Mp[96]=Mp[70]
set Mp[97]=Mp[70]
set Mp[98]=Mp[70]
set Mp[99]=Mp[70]
set Mp[100]=Mp[70]
set Mp[101]=Mp[70]
set Mp[102]=Mp[70]
set Mp[104]=Mp[70]
set Mp[105]=Mp[70]
call TriggerAddCondition(Mp[70],Condition(function M3))
call TriggerAddAction(Mp[70],function M3)
set Np[70]=CreateTrigger()
set Np[96]=Np[70]
set Np[97]=Np[70]
set Np[98]=Np[70]
set Np[99]=Np[70]
set Np[100]=Np[70]
set Np[101]=Np[70]
set Np[103]=Np[70]
set Np[104]=Np[70]
set Np[105]=Np[70]
call TriggerAddCondition(Np[70],Condition(function N3))
call TriggerAddAction(Np[70],function N3)
set Op[70]=CreateTrigger()
set Op[96]=Op[70]
set Op[97]=Op[70]
set Op[98]=Op[70]
set Op[99]=Op[70]
set Op[100]=Op[70]
set Op[101]=Op[70]
set Op[103]=Op[70]
set Op[104]=Op[70]
set Op[105]=Op[70]
call TriggerAddCondition(Op[70],Condition(function O3))
call TriggerAddAction(Op[70],function O3)
set Zp[70]=null
set Zp[96]=null
set Zp[97]=null
set Zp[98]=null
set Zp[99]=null
set Zp[100]=null
set Zp[101]=null
set Zp[102]=null
set Zp[103]=null
set Zp[104]=null
set Zp[105]=null
set kq=CreateTrigger()
call TriggerAddCondition(kq,Condition(function P3))
set mq=CreateTrigger()
call TriggerAddCondition(mq,Condition(function Q3))
call TriggerAddAction(mq,function Q3)
set nq=CreateTrigger()
call TriggerAddCondition(nq,Condition(function R3))
call TriggerAddAction(nq,function R3)
set oq=CreateTrigger()
call TriggerAddCondition(oq,Condition(function S3))
call TriggerAddAction(oq,function S3)
set Xp[61]=CreateTrigger()
set Xp[62]=Xp[61]
set Xp[63]=Xp[61]
set Xp[64]=Xp[61]
set Xp[68]=Xp[61]
call TriggerAddCondition(Xp[61],Condition(function T3))
set cp[61]=CreateTrigger()
set cp[62]=cp[61]
set cp[63]=cp[61]
set cp[64]=cp[61]
set cp[67]=cp[61]
set cp[68]=cp[61]
call TriggerAddCondition(cp[61],Condition(function U3))
call TriggerAddAction(cp[61],function U3)
set Dp[61]=CreateTrigger()
set Dp[62]=Dp[61]
set Dp[63]=Dp[61]
set Dp[64]=Dp[61]
set Dp[65]=Dp[61]
set Dp[66]=Dp[61]
set Dp[68]=Dp[61]
set Dp[69]=Dp[61]
call TriggerAddCondition(Dp[61],Condition(function V3))
call TriggerAddAction(Dp[61],function V3)
set Ep[61]=CreateTrigger()
set Ep[63]=Ep[61]
set Ep[65]=Ep[61]
set Ep[66]=Ep[61]
set Ep[69]=Ep[61]
call TriggerAddCondition(Ep[61],Condition(function W3))
call TriggerAddAction(Ep[61],function W3)
set tq=CreateTrigger()
call TriggerAddCondition(tq,Condition(function X3))
set Fp[61]=CreateTrigger()
set Fp[62]=Fp[61]
set Fp[63]=Fp[61]
set Fp[64]=Fp[61]
set Fp[67]=Fp[61]
set Fp[68]=Fp[61]
set Fp[69]=Fp[61]
call TriggerAddCondition(Fp[61],Condition(function Y3))
call TriggerAddAction(Fp[61],function Y3)
set Gp[61]=CreateTrigger()
set Gp[62]=Gp[61]
set Gp[63]=Gp[61]
set Gp[64]=Gp[61]
set Gp[67]=Gp[61]
set Gp[68]=Gp[61]
set Gp[69]=Gp[61]
call TriggerAddCondition(Gp[61],Condition(function Z3))
call TriggerAddAction(Gp[61],function Z3)
set sq=CreateTrigger()
call TriggerAddCondition(sq,Condition(function dF))
set rq=CreateTrigger()
call TriggerAddCondition(rq,Condition(function eF))
set Tp[43]=CreateTrigger()
set Tp[78]=Tp[43]
set Tp[79]=Tp[43]
set Tp[80]=Tp[43]
set Tp[81]=Tp[43]
set Tp[82]=Tp[43]
set Tp[83]=Tp[43]
call TriggerAddCondition(Tp[43],Condition(function fF))
set rp[43]=CreateTrigger()
set rp[78]=rp[43]
set rp[80]=rp[43]
set rp[81]=rp[43]
set rp[82]=rp[43]
set rp[83]=rp[43]
call TriggerAddCondition(rp[43],Condition(function gF))
call TriggerAddAction(rp[43],function gF)
set sp[43]=CreateTrigger()
set sp[78]=sp[43]
set sp[80]=sp[43]
set sp[81]=sp[43]
set sp[83]=sp[43]
call TriggerAddCondition(sp[43],Condition(function hF))
call TriggerAddAction(sp[43],function hF)
set tp[43]=CreateTrigger()
set tp[78]=tp[43]
set tp[80]=tp[43]
set tp[83]=tp[43]
call TriggerAddCondition(tp[43],Condition(function iF))
call TriggerAddAction(tp[43],function iF)
set vp[43]=CreateTrigger()
set vp[78]=vp[43]
set vp[79]=vp[43]
set vp[80]=vp[43]
set vp[83]=vp[43]
call TriggerAddCondition(vp[43],Condition(function jF))
call TriggerAddAction(vp[43],function jF)
set wp[43]=CreateTrigger()
set wp[78]=wp[43]
set wp[79]=wp[43]
set wp[80]=wp[43]
set wp[81]=wp[43]
set wp[83]=wp[43]
call TriggerAddCondition(wp[43],Condition(function kF))
call TriggerAddAction(wp[43],function kF)
set xp[43]=CreateTrigger()
set xp[78]=xp[43]
set xp[79]=xp[43]
set xp[80]=xp[43]
set xp[81]=xp[43]
set xp[82]=xp[43]
set xp[83]=xp[43]
call TriggerAddCondition(xp[43],Condition(function mF))
call TriggerAddAction(xp[43],function mF)
set yp[43]=CreateTrigger()
set yp[78]=yp[43]
set yp[79]=yp[43]
set yp[80]=yp[43]
set yp[81]=yp[43]
set yp[83]=yp[43]
call TriggerAddCondition(yp[43],Condition(function nF))
call TriggerAddAction(yp[43],function nF)
set zp[43]=CreateTrigger()
set zp[78]=zp[43]
set zp[79]=zp[43]
set zp[80]=zp[43]
set zp[81]=zp[43]
set zp[82]=zp[43]
set zp[83]=zp[43]
call TriggerAddCondition(zp[43],Condition(function oF))
call TriggerAddAction(zp[43],function oF)
set Ap[43]=CreateTrigger()
set Ap[78]=Ap[43]
set Ap[79]=Ap[43]
set Ap[80]=Ap[43]
set Ap[81]=Ap[43]
set Ap[82]=Ap[43]
set Ap[83]=Ap[43]
call TriggerAddCondition(Ap[43],Condition(function pF))
call TriggerAddAction(Ap[43],function pF)
set ap[43]=CreateTrigger()
set ap[78]=ap[43]
set ap[79]=ap[43]
set ap[80]=ap[43]
set ap[81]=ap[43]
set ap[83]=ap[43]
call TriggerAddCondition(ap[43],Condition(function qF))
call TriggerAddAction(ap[43],function qF)
set Bp[43]=CreateTrigger()
set Bp[78]=Bp[43]
set Bp[79]=Bp[43]
set Bp[80]=Bp[43]
set Bp[81]=Bp[43]
set Bp[82]=Bp[43]
set Bp[83]=Bp[43]
call TriggerAddCondition(Bp[43],Condition(function rF))
call TriggerAddAction(Bp[43],function rF)
set bp[43]=CreateTrigger()
set bp[78]=bp[43]
set bp[79]=bp[43]
set bp[80]=bp[43]
set bp[81]=bp[43]
set bp[82]=bp[43]
set bp[83]=bp[43]
call TriggerAddCondition(bp[43],Condition(function sF))
call TriggerAddAction(bp[43],function sF)
set pq=CreateTrigger()
call TriggerAddCondition(pq,Condition(function tF))
call TriggerAddAction(pq,function tF)
set Vp[44]=CreateTrigger()
call TriggerAddCondition(Vp[44],Condition(function uF))
set Cp[44]=CreateTrigger()
set Cp[48]=Cp[44]
set Cp[50]=Cp[44]
set Cp[52]=Cp[44]
set Cp[54]=Cp[44]
set Cp[56]=Cp[44]
set Cp[72]=Cp[44]
set Cp[73]=Cp[44]
set Cp[74]=Cp[44]
set Cp[75]=Cp[44]
set Cp[76]=Cp[44]
set Cp[77]=Cp[44]
call TriggerAddCondition(Cp[44],Condition(function vF))
call TriggerAddAction(Cp[44],function vF)
set Vp[54]=CreateTrigger()
call TriggerAddCondition(Vp[54],Condition(function wF))
call TriggerAddCondition(Vp[54],Condition(function uF))
set Cp[53]=CreateTrigger()
call TriggerAddCondition(Cp[53],Condition(function yF))
call TriggerAddAction(Cp[53],function yF)
set Vp[53]=CreateTrigger()
call TriggerAddCondition(Vp[53],Condition(function xF))
call TriggerAddCondition(Vp[53],Condition(function uF))
set Vp[52]=CreateTrigger()
call TriggerAddCondition(Vp[52],Condition(function zF))
call TriggerAddCondition(Vp[52],Condition(function uF))
set Cp[51]=CreateTrigger()
call TriggerAddCondition(Cp[51],Condition(function aF))
call TriggerAddAction(Cp[51],function aF)
set Vp[51]=CreateTrigger()
call TriggerAddCondition(Vp[51],Condition(function AF))
call TriggerAddCondition(Vp[51],Condition(function uF))
set Vp[50]=CreateTrigger()
call TriggerAddCondition(Vp[50],Condition(function BF))
call TriggerAddCondition(Vp[50],Condition(function uF))
set Cp[49]=CreateTrigger()
call TriggerAddCondition(Cp[49],Condition(function CF))
call TriggerAddAction(Cp[49],function CF)
set Vp[49]=CreateTrigger()
call TriggerAddCondition(Vp[49],Condition(function bF))
call TriggerAddCondition(Vp[49],Condition(function uF))
set Vp[48]=CreateTrigger()
call TriggerAddCondition(Vp[48],Condition(function cF))
call TriggerAddCondition(Vp[48],Condition(function uF))
set qq=CreateTrigger()
call TriggerAddCondition(qq,Condition(function DF))
set Cp[55]=CreateTrigger()
call TriggerAddCondition(Cp[55],Condition(function FF))
call TriggerAddAction(Cp[55],function FF)
set Vp[55]=CreateTrigger()
call TriggerAddCondition(Vp[55],Condition(function EF))
call TriggerAddCondition(Vp[55],Condition(function uF))
set Vp[56]=CreateTrigger()
call TriggerAddCondition(Vp[56],Condition(function GF))
call TriggerAddCondition(Vp[56],Condition(function uF))
set Cp[57]=CreateTrigger()
call TriggerAddCondition(Cp[57],Condition(function IF))
call TriggerAddAction(Cp[57],function IF)
set Vp[57]=CreateTrigger()
call TriggerAddCondition(Vp[57],Condition(function HF))
call TriggerAddCondition(Vp[57],Condition(function uF))
set Ep[62]=CreateTrigger()
call TriggerAddCondition(Ep[62],Condition(function lF))
call TriggerAddAction(Ep[62],function lF)
set Ep[64]=CreateTrigger()
call TriggerAddCondition(Ep[64],Condition(function JF))
call TriggerAddAction(Ep[64],function JF)
set cp[65]=CreateTrigger()
call TriggerAddCondition(cp[65],Condition(function LF))
call TriggerAddAction(cp[65],function LF)
set Fp[65]=CreateTrigger()
call TriggerAddCondition(Fp[65],Condition(function MF))
call TriggerAddAction(Fp[65],function MF)
set Gp[65]=CreateTrigger()
call TriggerAddCondition(Gp[65],Condition(function NF))
call TriggerAddAction(Gp[65],function NF)
set Xp[65]=CreateTrigger()
call TriggerAddCondition(Xp[65],Condition(function KF))
call TriggerAddCondition(Xp[65],Condition(function T3))
set cp[66]=CreateTrigger()
call TriggerAddCondition(cp[66],Condition(function PF))
call TriggerAddAction(cp[66],function PF)
set Fp[66]=CreateTrigger()
call TriggerAddCondition(Fp[66],Condition(function RF))
call TriggerAddAction(Fp[66],function RF)
set Gp[66]=CreateTrigger()
call TriggerAddCondition(Gp[66],Condition(function SF))
call TriggerAddAction(Gp[66],function SF)
set Xp[66]=CreateTrigger()
call TriggerAddCondition(Xp[66],Condition(function OF))
call TriggerAddCondition(Xp[66],Condition(function T3))
set Ep[67]=CreateTrigger()
call TriggerAddCondition(Ep[67],Condition(function UF))
call TriggerAddAction(Ep[67],function UF)
set Dp[67]=CreateTrigger()
call TriggerAddCondition(Dp[67],Condition(function VF))
call TriggerAddAction(Dp[67],function VF)
set Xp[67]=CreateTrigger()
call TriggerAddCondition(Xp[67],Condition(function TF))
call TriggerAddCondition(Xp[67],Condition(function T3))
set Ep[68]=CreateTrigger()
call TriggerAddCondition(Ep[68],Condition(function WF))
call TriggerAddAction(Ep[68],function WF)
set cp[69]=CreateTrigger()
call TriggerAddCondition(cp[69],Condition(function YF))
call TriggerAddAction(cp[69],function YF)
set Xp[69]=CreateTrigger()
call TriggerAddCondition(Xp[69],Condition(function XF))
call TriggerAddCondition(Xp[69],Condition(function T3))
set Cp[71]=CreateTrigger()
call TriggerAddCondition(Cp[71],Condition(function dG))
call TriggerAddAction(Cp[71],function dG)
set Vp[71]=CreateTrigger()
call TriggerAddCondition(Vp[71],Condition(function ZF))
call TriggerAddCondition(Vp[71],Condition(function uF))
set Vp[72]=CreateTrigger()
call TriggerAddCondition(Vp[72],Condition(function eG))
call TriggerAddCondition(Vp[72],Condition(function uF))
set Vp[73]=CreateTrigger()
call TriggerAddCondition(Vp[73],Condition(function fG))
call TriggerAddCondition(Vp[73],Condition(function uF))
set Vp[74]=CreateTrigger()
call TriggerAddCondition(Vp[74],Condition(function gG))
call TriggerAddCondition(Vp[74],Condition(function uF))
set Vp[75]=CreateTrigger()
call TriggerAddCondition(Vp[75],Condition(function hG))
call TriggerAddCondition(Vp[75],Condition(function uF))
set Vp[76]=CreateTrigger()
call TriggerAddCondition(Vp[76],Condition(function iG))
call TriggerAddCondition(Vp[76],Condition(function uF))
set Vp[77]=CreateTrigger()
call TriggerAddCondition(Vp[77],Condition(function jG))
call TriggerAddCondition(Vp[77],Condition(function uF))
set sp[79]=CreateTrigger()
call TriggerAddCondition(sp[79],Condition(function kG))
call TriggerAddAction(sp[79],function kG)
set tp[79]=CreateTrigger()
call TriggerAddCondition(tp[79],Condition(function mG))
call TriggerAddAction(tp[79],function mG)
set rp[79]=CreateTrigger()
call TriggerAddCondition(rp[79],Condition(function nG))
call TriggerAddAction(rp[79],function nG)
set tp[81]=CreateTrigger()
call TriggerAddCondition(tp[81],Condition(function oG))
call TriggerAddAction(tp[81],function oG)
set vp[81]=CreateTrigger()
call TriggerAddCondition(vp[81],Condition(function pG))
call TriggerAddAction(vp[81],function pG)
set sp[82]=CreateTrigger()
call TriggerAddCondition(sp[82],Condition(function qG))
call TriggerAddAction(sp[82],function qG)
set tp[82]=CreateTrigger()
call TriggerAddCondition(tp[82],Condition(function rG))
call TriggerAddAction(tp[82],function rG)
set vp[82]=CreateTrigger()
call TriggerAddCondition(vp[82],Condition(function sG))
call TriggerAddAction(vp[82],function sG)
set wp[82]=CreateTrigger()
call TriggerAddCondition(wp[82],Condition(function tG))
call TriggerAddAction(wp[82],function tG)
set yp[82]=CreateTrigger()
call TriggerAddCondition(yp[82],Condition(function uG))
call TriggerAddAction(yp[82],function uG)
set ap[82]=CreateTrigger()
call TriggerAddCondition(ap[82],Condition(function vG))
call TriggerAddAction(ap[82],function vG)
set Pp[86]=CreateTrigger()
call TriggerAddCondition(Pp[86],Condition(function xG))
call TriggerAddAction(Pp[86],function xG)
set eq[86]=CreateTrigger()
call TriggerAddCondition(eq[86],Condition(function wG))
set Pp[87]=CreateTrigger()
call TriggerAddCondition(Pp[87],Condition(function yG))
call TriggerAddAction(Pp[87],function yG)
set Pp[88]=CreateTrigger()
call TriggerAddCondition(Pp[88],Condition(function AG))
call TriggerAddAction(Pp[88],function AG)
set eq[88]=CreateTrigger()
call TriggerAddCondition(eq[88],Condition(function zG))
set eq[89]=CreateTrigger()
call TriggerAddCondition(eq[89],Condition(function aG))
set Pp[90]=CreateTrigger()
call TriggerAddCondition(Pp[90],Condition(function bG))
call TriggerAddAction(Pp[90],function bG)
set eq[90]=CreateTrigger()
call TriggerAddCondition(eq[90],Condition(function BG))
set Pp[91]=CreateTrigger()
call TriggerAddCondition(Pp[91],Condition(function cG))
call TriggerAddAction(Pp[91],function cG)
set eq[91]=CreateTrigger()
call TriggerAddCondition(eq[91],Condition(function CG))
set eq[93]=CreateTrigger()
call TriggerAddCondition(eq[93],Condition(function DG))
set Pp[94]=CreateTrigger()
call TriggerAddCondition(Pp[94],Condition(function FG))
call TriggerAddAction(Pp[94],function FG)
set eq[94]=CreateTrigger()
call TriggerAddCondition(eq[94],Condition(function EG))
set lp[97]=CreateTrigger()
call TriggerAddCondition(lp[97],Condition(function GG))
call TriggerAddAction(lp[97],function GG)
set Kp[97]=CreateTrigger()
call TriggerAddCondition(Kp[97],Condition(function HG))
call TriggerAddAction(Kp[97],function HG)
set Jp[98]=CreateTrigger()
call TriggerAddCondition(Jp[98],Condition(function IG))
call TriggerAddAction(Jp[98],function IG)
set Kp[98]=CreateTrigger()
call TriggerAddCondition(Kp[98],Condition(function lG))
call TriggerAddAction(Kp[98],function lG)
set Kp[99]=CreateTrigger()
call TriggerAddCondition(Kp[99],Condition(function JG))
call TriggerAddAction(Kp[99],function JG)
set Lp[99]=CreateTrigger()
call TriggerAddCondition(Lp[99],Condition(function KG))
call TriggerAddAction(Lp[99],function KG)
set Hp[100]=CreateTrigger()
call TriggerAddCondition(Hp[100],Condition(function LG))
call TriggerAddAction(Hp[100],function LG)
set lp[100]=CreateTrigger()
call TriggerAddCondition(lp[100],Condition(function MG))
call TriggerAddAction(lp[100],function MG)
set Kp[100]=CreateTrigger()
call TriggerAddCondition(Kp[100],Condition(function NG))
call TriggerAddAction(Kp[100],function NG)
set Np[102]=CreateTrigger()
call TriggerAddCondition(Np[102],Condition(function OG))
call TriggerAddAction(Np[102],function OG)
set Op[102]=CreateTrigger()
call TriggerAddCondition(Op[102],Condition(function PG))
call TriggerAddAction(Op[102],function PG)
set Ip[103]=CreateTrigger()
call TriggerAddCondition(Ip[103],Condition(function QG))
call TriggerAddAction(Ip[103],function QG)
set lp[103]=CreateTrigger()
call TriggerAddCondition(lp[103],Condition(function RG))
call TriggerAddAction(lp[103],function RG)
set Mp[103]=CreateTrigger()
call TriggerAddCondition(Mp[103],Condition(function SG))
call TriggerAddAction(Mp[103],function SG)
set Kp[103]=CreateTrigger()
call TriggerAddCondition(Kp[103],Condition(function TG))
call TriggerAddAction(Kp[103],function TG)
set lp[104]=CreateTrigger()
call TriggerAddCondition(lp[104],Condition(function UG))
call TriggerAddAction(lp[104],function UG)
set Kp[104]=CreateTrigger()
call TriggerAddCondition(Kp[104],Condition(function VG))
call TriggerAddAction(Kp[104],function VG)
set Lp[104]=CreateTrigger()
call TriggerAddCondition(Lp[104],Condition(function WG))
call TriggerAddAction(Lp[104],function WG)
set wq[1]=CreateTrigger()
call TriggerAddAction(wq[1],function XG)
call TriggerAddCondition(wq[1],Condition(function XG))
set vq[1]=CreateTrigger()
call TriggerAddAction(vq[1],function e6)
call TriggerAddCondition(vq[1],Condition(function e6))
set vq[2]=CreateTrigger()
call TriggerAddAction(vq[2],function f6)
call TriggerAddCondition(vq[2],Condition(function f6))
set vq[3]=CreateTrigger()
call TriggerAddAction(vq[3],function g6)
call TriggerAddCondition(vq[3],Condition(function g6))
set xq[1]=CreateTrigger()
call TriggerAddAction(xq[1],function h6)
call TriggerAddCondition(xq[1],Condition(function h6))
set vq[4]=CreateTrigger()
call TriggerAddAction(vq[4],function i6)
call TriggerAddCondition(vq[4],Condition(function i6))
set vq[5]=CreateTrigger()
call TriggerAddAction(vq[5],function j6)
call TriggerAddCondition(vq[5],Condition(function j6))
set yq[1]=CreateTrigger()
call TriggerAddAction(yq[1],function k6)
call TriggerAddCondition(yq[1],Condition(function k6))
set xq[2]=CreateTrigger()
call TriggerAddAction(xq[2],function m6)
call TriggerAddCondition(xq[2],Condition(function m6))
set zq[1]=CreateTrigger()
call TriggerAddAction(zq[1],function n6)
call TriggerAddCondition(zq[1],Condition(function n6))
call ExecuteFunc("iv")
call ExecuteFunc("Ia")
call ExecuteFunc("Ma")
call ExecuteFunc("Na")
call ExecuteFunc("Ta")
call ExecuteFunc("eB")
call ExecuteFunc("QB")
call ExecuteFunc("Wb")
call ExecuteFunc("lC")
call ExecuteFunc("VC")
endfunction
