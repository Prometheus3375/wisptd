globals
constant integer o=$D00A6
constant integer A=$D00A5
constant integer B=$D00FA
constant integer C=$D0097
constant integer D=$D00FE
constant integer E=$D0057
constant integer F=$D0106
constant integer G=$D0012
constant integer H=$D0058
constant integer I=$D0008
constant integer J=$D0005
constant integer K=$D028B
constant real L=GetCameraMargin(CAMERA_MARGIN_LEFT)
constant real M=GetCameraMargin(CAMERA_MARGIN_RIGHT)
constant real N=GetCameraMargin(CAMERA_MARGIN_BOTTOM)
constant real O=GetCameraMargin(CAMERA_MARGIN_TOP)
player P
boolean Q=true
constant integer R=$A
player array S
constant playercolor U=PLAYER_COLOR_RED
constant unittype V=UNIT_TYPE_MECHANICAL
constant unittype W=UNIT_TYPE_GIANT
constant unittype X=UNIT_TYPE_SAPPER
constant unittype Y=UNIT_TYPE_TAUREN
constant unittype Z=UNIT_TYPE_ANCIENT
boolexpr d4
integer array e4
constant real f4=-1472.
constant real g4=-1792.
constant real h4=-1920.
constant real i4=-8448.
constant real j4=g4
constant integer k4=256/ 64
constant integer m4=R2I(1024.)/ 64
constant integer n4=R2I(640.-j4)/ 64
constant integer o4=608-m4*k4
constant integer p4=o4+m4
integer q4
constant integer r4=$CC
constant integer s4=$CC
constant integer t4=$80
constant integer u4=$CC
constant integer v4=$FF
constant integer w4=E
constant integer x4=H
constant integer y4=0+StringLength("Spawn")
constant integer z4=y4
constant integer A4=z4+2
constant integer a4=0+StringLength("Upgrade")
constant integer B4=a4
constant integer b4=B4+2
constant integer C4=b4+1
constant integer c4=C4+1
constant integer D4=c4
constant integer E4=-25
constant integer F4=-25
integer G4=0
integer H4=0
constant real I4=1./ 8.
boolexpr array l4
integer array J4
constant integer K4=$A
constant integer L4=$B
constant integer M4=$C
constant integer N4=$D
constant integer O4=$E
constant integer P4=$F
constant integer Q4=$80
code R4
integer array S4
trigger array T4
trigger array U4
integer array V4
integer array W4
trigger array X4
integer array Y4
constant integer Z4=$A
integer array d7
constant timer e7=CreateTimer()
constant hashtable f7=InitHashtable()
region g7
region h7
integer array i7
integer j7
integer k7
integer m7
integer n7=-1
integer o7=0
integer p7=1
integer q7=30+1
constant trigger r7=CreateTrigger()
constant trigger s7=CreateTrigger()
constant trigger t7=CreateTrigger()
integer u7
constant trigger v7=CreateTrigger()
constant trigger w7=CreateTrigger()
unit x7
integer y7
real z7
real A7
constant trigger a7=CreateTrigger()
constant trigger B7=CreateTrigger()
constant trigger b7=CreateTrigger()
constant trigger C7=CreateTrigger()
constant trigger c7=CreateTrigger()
constant trigger D7=CreateTrigger()
constant trigger E7=CreateTrigger()
constant trigger F7=CreateTrigger()
constant trigger G7=CreateTrigger()
constant trigger H7=CreateTrigger()
constant trigger I7=CreateTrigger()
constant trigger l7=CreateTrigger()
constant trigger J7=CreateTrigger()
constant trigger K7=CreateTrigger()
constant trigger L7=CreateTrigger()
sound M7
sound N7
sound O7
sound P7
sound Q7
sound R7
sound S7
constant real T7=-8960.
constant real U7=-1920.
constant real V7=(T7+6912.)/ 2.
constant real W7=(U7+1920.)/ 2.
constant trigger X7=CreateTrigger()
constant trigger Y7=CreateTrigger()
multiboard Z7
integer d8=0
constant real e8=2.*.005
constant real f8=14.*.005
constant real g8=6.*.005
constant real h8=6.*.005
constant real i8=6.*.005
constant real j8=4.*.005
constant real k8=4.*.005
constant real m8=4.*.005
constant real n8=17.*.005
multiboarditem o8
constant trigger p8=CreateTrigger()
constant trigger q8=CreateTrigger()
integer r8=0
integer s8=0
integer array t8
integer array u8
integer array v8
integer array w8
integer x8=0
integer y8=0
integer array z8
integer array A8
integer array a8
integer array B8
integer b8=0
integer C8=0
integer array c8
integer array D8
integer array E8
integer array F8
integer G8=0
integer H8=0
integer array I8
integer array l8
integer array J8
integer array K8
integer L8=0
integer M8=0
integer array N8
integer array O8
integer array P8
integer array Q8
integer R8=0
integer S8=0
integer array T8
integer array U8
integer array V8
integer array W8
integer X8=0
integer Y8=0
integer array Z8
integer array d9
integer array e9
integer array f9
integer g9=0
integer h9=0
integer array i9
integer array j9
integer array k9
integer array m9
integer n9=0
integer o9=0
integer array p9
boolean array q9
integer array r9
integer s9=0
integer t9=0
integer array u9
integer array v9
integer array w9
integer array x9
integer array y9
integer array z9
integer array A9
string array a9
integer B9=0
integer b9=0
integer array C9
integer array c9
integer array D9
integer array E9
integer array F9
integer array G9
boolean array H9
boolean array I9
integer array l9
integer array J9
integer array K9
integer array L9
integer array M9
integer array N9
integer O9=0
integer P9=0
integer array Q9
integer array R9
integer array S9
integer T9=0
integer U9=0
integer array V9
integer array W9
integer array X9
integer array Y9
real array Z9
real array ed
real array fd
real array gd
rect array hd
rect array jd
rect array kd
rect array md
rect array nd
fogmodifier array od
integer array pd
fogmodifier array qd
integer array rd
boolean array sd
boolean array td
integer ud=0
integer vd=0
integer array wd
unit array xd
integer array yd
timer array zd
integer Ad=0
integer ad=0
integer array Bd
real array bd
integer array Cd
integer array Dd
integer Ed=0
integer Fd=0
integer array Gd
integer array Hd
integer array Id
integer array ld
integer array Jd
real array Kd
integer array Ld
integer array Md
real array Nd
integer Od=0
integer Pd=0
integer array Qd
integer array Rd
integer array Sd
integer array Td
integer array Ud
integer array Vd
real array Wd
integer array Xd
integer array Yd
integer array Zd
real array de
integer array ee
integer array fe
integer array ge
boolean array he
integer array ie
integer array je
integer array ke
boolean array me
integer array ne
integer array oe
integer array pe
integer array qe
integer array re
integer array se
integer array te
integer array ue
integer array ve
integer array we
integer array xe
integer array ye
boolean array ze
integer array Ae
integer array ae
integer array Be
boolean array be
integer array Ce
integer array ce
integer array De
boolean array Ee
integer array Fe
integer array Ge
integer array He
boolean array Ie
integer le=0
integer Je=0
integer array Ke
player array Le
integer array Me
integer array Ne
string array Oe
integer array Pe
boolean array Qe
boolean array Re
player array Se
boolean array Te
integer array Ue
integer array Ve
integer array We
integer array Xe
real array Ye
integer array Ze
integer array df
integer array ef
integer array ff
multiboarditem array gf
multiboarditem array hf
multiboarditem array jf
multiboarditem array kf
multiboarditem array mf
multiboarditem array nf
multiboarditem array of
image array pf
integer array qf
boolean array rf
unit array sf
unit array tf
unit array uf
unit array vf
unit array wf
integer array xf
integer array yf
integer array zf
integer array Af
integer array af
integer array Bf
integer array bf
integer array Cf
integer array cf
integer array Df
integer array Ef
integer array Ff
integer array Gf
integer array Hf
integer array If
integer array lf
integer array Jf
integer array Kf
integer array Lf
integer array Mf
integer array Nf
integer array Of
integer array Pf
integer array Qf
integer array Rf
integer array Sf
integer array Tf
item array Uf
item array Vf
item array Wf
item array Xf
item array Yf
integer array Zf
timer array dg
integer eg=0
integer fg=0
integer array gg
integer hg
integer ig
integer array jg
integer array kg
integer array mg
integer array ng
unit array og
integer array pg
integer array qg
integer array rg
boolean array sg
boolean array tg
real array ug
real array vg
real array wg
integer array xg
real array yg
real array zg
real array Ag
item array ag
item array Bg
item array bg
item array Cg
item array cg
item array Dg
real array Eg
real array Fg
real array Gg
integer array Hg
integer array Ig
timer array lg
integer Jg=0
integer Kg=0
integer array Lg
integer array Mg
integer array Ng
unit array Og
integer array Pg
integer array Qg
integer array Rg
boolean array Sg
integer array Tg
integer array Ug
timer array Vg
integer Wg=0
integer Xg=0
integer array Yg
integer array Zg
unit array dh
integer array eh
integer array fh
integer gh=0
integer hh=0
integer array ih
integer array jh
integer array kh
real array mh
integer array nh
integer array oh
integer ph=0
integer qh=0
integer array rh
integer array sh
integer array th
boolean array uh
boolean array vh
integer wh=0
integer xh=0
integer array yh
boolean array zh
boolean array Ah
integer array ah
integer array Bh
real array bh
real array Ch
integer array ch
integer Dh=0
integer Eh=0
integer array Fh
unit array Gh
integer array Hh
integer Ih=0
integer lh=0
integer array Jh
constant real Kh=1./ 64.
constant integer Lh=$CC
constant integer Mh=$CC
unit array Nh
image array Oh
timer array Ph
integer Qh=0
integer Rh=0
integer array Sh
integer Th
integer array Uh
integer array Vh
boolean array Wh
timer array Xh
constant integer Yh=-$F
boolean array Zh
integer array di
real array ei
constant integer fi=$F
boolexpr gi
integer array hi
integer array ii
integer ji
boolean ki
constant real mi=-.15
integer array ni
integer array oi
boolexpr pi
boolean array qi
real array ri
integer array si
integer array ti
integer array ui
integer vi
integer wi
constant integer xi=B
real array yi
integer array zi
boolexpr Ai
integer ai
integer Bi
real bi
real array Ci
boolexpr ci
code Di
timer array Ei
integer Fi
integer Gi
real Hi
integer Ii=0
integer li=0
integer array Ji
integer array Ki
timer array Li
integer Mi=0
integer Ni=0
integer array Oi
integer array Pi
unit array Qi
integer array Ri
integer array Si
boolean array Ti
real array Ui
real array Vi
real array Wi
real array Xi
integer array Yi
real array Zi
boolean array dj
real array ej
integer array fj
integer array gj
integer array hj
timer array ij
image array jj
boolean array kj
integer array mj
boolean array nj
integer array oj
integer array pj
integer array qj
integer array rj
item array sj
item array tj
item array uj
item array vj
integer array wj
integer array xj
integer array yj
integer array zj
constant integer Aj=$A
constant integer aj=$A
integer Bj=0
integer bj=0
integer array Cj
integer array cj
boolean array Dj
boolean array Ej
boolean array Fj
real array Gj
real array Hj
real array Ij
integer lj=0
integer Jj=0
integer array Kj
integer Lj
integer array Mj
integer array Nj
constant integer Oj=D
constant trigger Pj=CreateTrigger()
code Qj
item array Rj
real array Sj
real array Tj
real array Uj
real array Vj
integer array Wj
timer array Xj
constant real Yj=-.3
constant integer Zj=-25
real array dk
constant real ek=-.75
integer array fk
integer array gk
unit array hk
unit array ik
boolean array jk
real array kk
integer array mk
item array nk
timer array ok
integer array pk
integer array qk
timer array rk
constant integer sk=t4
constant integer tk=u4
constant integer uk=v4
code vk
code wk
integer array xk
integer array yk
real array zk
image array Ak
integer array ak
unit array Bk
effect array bk
integer array Ck
integer array ck
integer array Dk
timer array Ek
boolexpr Fk
integer Gk
boolexpr Hk
integer Ik
integer lk
boolexpr Jk
integer Kk
real array Lk
real array Mk
real array Nk
code Ok
code Pk
real array Qk
real array Rk
real array Sk
timer array Tk
timer array Uk
constant integer Vk=A
constant integer Wk=o
constant integer Xk=$A
constant real Yk=1.-.1
constant real Zk=-.01
integer array dm
real array em
real array fm
boolean array gm
integer array hm
item array im
integer array jm
timer array km
integer array mm
integer array nm
integer array om
integer array pm
trigger array qm
integer array rm
integer sm=0
integer tm=0
integer array um
constant integer vm=0+StringLength("AbilityUpgrade")
constant integer wm=vm
constant integer xm=wm+2
constant integer ym=xm+1
constant integer zm=ym+1
constant integer Am=zm
constant integer am=0+StringLength("HeroAbility")
constant integer Bm=am
constant integer bm=Bm+2
integer Cm
integer array cm
integer array Dm
integer array Em
integer array Fm
integer array Gm
string array Hm
integer array Im
integer array lm
integer array Jm
integer array Km
integer array Lm
integer array Mm
integer array Nm
integer array Om
integer array Pm
integer array Qm
trigger array Rm
integer array Sm
integer Tm=0
integer Um=0
integer array Vm
integer array Wm
integer array Xm
integer array Ym
real array Zm
integer array dn
boolean array en
boolean array fn
integer array gn
integer array hn
real array in
real array jn
real array kn
real array mn
integer array nn
integer array on
integer array pn
integer array qn
real array rn
integer array sn
integer array tn
integer array un
real array vn
integer array wn
integer array xn
boolean array yn
integer array zn
integer An=0
integer an=0
integer array Bn
integer array bn
integer array Cn
integer array cn
integer array Dn
integer array En
integer array Fn
integer array Gn
integer array Hn
integer array In
integer array ln
trigger array Jn
integer array Kn
integer Ln=0
integer Mn=0
integer array Nn
integer array On
integer array Pn
integer array Qn
integer array Rn
integer array Sn
integer array Tn
string array Un
real array Vn
real array Wn
real array Xn
integer array Yn
boolexpr array Zn
real array do
integer array eo
boolean array fo
integer array go
integer array ho
real array io
real array jo
integer array ko
boolean array mo
integer array no
boolean array oo
integer array po
boolean array qo
boolean array ro
integer array so
integer array to
integer array uo
integer array vo
integer array wo
integer array xo
trigger array yo
trigger array zo
trigger array Ao
trigger array ao
trigger array Bo
trigger array bo
trigger array Co
trigger array co
trigger array Do
trigger array Eo
trigger array Fo
trigger array Go
trigger array Ho
trigger array Io
trigger array lo
trigger array Jo
trigger array Ko
trigger array Lo
trigger array Mo
trigger array No
trigger array Oo
trigger array Po
trigger array Qo
trigger array Ro
trigger array So
trigger array To
trigger array Uo
trigger array Vo
trigger array Wo
integer array Xo
trigger array Yo
integer array Zo
trigger array dp
integer array ep
trigger array fp
integer array gp
trigger array hp
integer array ip
trigger array jp
integer array kp
trigger array mp
trigger np
trigger op
trigger pp
trigger qp
trigger rp
trigger sp
trigger tp
trigger vp
trigger wp
trigger xp
trigger yp
trigger zp
trigger Ap
trigger ap
trigger array Bp
trigger array bp
trigger array Cp
trigger array Dp
integer Ep
integer Fp
integer Gp
real Hp
boolean Ip
integer lp
real Jp
integer Kp
boolean Lp
endglobals
native UnitAlive takes unit id returns boolean
function mq takes nothing returns integer
local integer nq=r8
if(nq!=0)then
set r8=t8[nq]
else
set s8=s8+1
set nq=s8
endif
if(nq>818)then
return 0
endif
set v8[nq]=(nq-1)*$A
set w8[nq]=-1
set t8[nq]=-1
return nq
endfunction
function oq takes nothing returns integer
local integer nq=Ln
if(nq!=0)then
set Ln=Nn[nq]
else
set Mn=Mn+1
set nq=Mn
endif
if(nq>$AA9)then
return 0
endif
set Qn[nq]=(nq-1)*3
set Nn[nq]=-1
return nq
endfunction
function pq takes nothing returns integer
local integer nq=An
if(nq!=0)then
set An=Bn[nq]
else
set an=an+1
set nq=an
endif
if(nq>$AA9)then
return 0
endif
set Cn[nq]=(nq-1)*3
set Dn[nq]=(nq-1)*3
set Fn[nq]=(nq-1)*3
set Hn[nq]=(nq-1)*3
set ln[nq]=(nq-1)*3
set Kn[nq]=(nq-1)*3
set Bn[nq]=-1
return nq
endfunction
function qq takes nothing returns integer
local integer nq=Tm
if(nq!=0)then
set Tm=Vm[nq]
else
set Um=Um+1
set nq=Um
endif
if(nq>$AA9)then
return 0
endif
set zn[nq]=(nq-1)*3
set Vm[nq]=-1
return nq
endfunction
function rq takes nothing returns integer
local integer nq=sm
if(nq!=0)then
set sm=um[nq]
else
set tm=tm+1
set nq=tm
endif
if(nq>$FFE)then
return 0
endif
set lm[nq]=(nq-1)*2
set Km[nq]=(nq-1)*2
set Mm[nq]=(nq-1)*2
set Om[nq]=(nq-1)*2
set Qm[nq]=(nq-1)*2
set Sm[nq]=(nq-1)*2
set um[nq]=-1
return nq
endfunction
function sq takes nothing returns integer
local integer nq=x8
if(nq!=0)then
set x8=z8[nq]
else
set y8=y8+1
set nq=y8
endif
if(nq>818)then
return 0
endif
set a8[nq]=(nq-1)*$A
set B8[nq]=-1
set z8[nq]=-1
return nq
endfunction
function tq takes nothing returns integer
local integer nq=b8
if(nq!=0)then
set b8=c8[nq]
else
set C8=C8+1
set nq=C8
endif
if(nq>67)then
return 0
endif
set E8[nq]=(nq-1)*120
set F8[nq]=-1
set c8[nq]=-1
return nq
endfunction
function uq takes nothing returns integer
local integer nq=G8
if(nq!=0)then
set G8=I8[nq]
else
set H8=H8+1
set nq=H8
endif
if(nq>77)then
return 0
endif
set J8[nq]=(nq-1)*105
set K8[nq]=-1
set I8[nq]=-1
return nq
endfunction
function vq takes nothing returns integer
local integer nq=L8
if(nq!=0)then
set L8=N8[nq]
else
set M8=M8+1
set nq=M8
endif
if(nq>77)then
return 0
endif
set P8[nq]=(nq-1)*105
set Q8[nq]=-1
set N8[nq]=-1
return nq
endfunction
function wq takes nothing returns integer
local integer nq=R8
if(nq!=0)then
set R8=T8[nq]
else
set S8=S8+1
set nq=S8
endif
if(nq>77)then
return 0
endif
set V8[nq]=(nq-1)*105
set W8[nq]=-1
set T8[nq]=-1
return nq
endfunction
function xq takes nothing returns integer
local integer nq=X8
if(nq!=0)then
set X8=Z8[nq]
else
set Y8=Y8+1
set nq=Y8
endif
if(nq>77)then
return 0
endif
set e9[nq]=(nq-1)*105
set f9[nq]=-1
set Z8[nq]=-1
return nq
endfunction
function yq takes nothing returns integer
local integer nq=g9
if(nq!=0)then
set g9=i9[nq]
else
set h9=h9+1
set nq=h9
endif
if(nq>77)then
return 0
endif
set k9[nq]=(nq-1)*105
set m9[nq]=-1
set i9[nq]=-1
return nq
endfunction
function zq takes nothing returns integer
local integer nq=n9
if(nq!=0)then
set n9=p9[nq]
else
set o9=o9+1
set nq=o9
endif
if(nq>$FFE)then
return 0
endif
set r9[nq]=(nq-1)*2
set p9[nq]=-1
return nq
endfunction
function Aq takes nothing returns integer
local integer nq=s9
if(nq!=0)then
set s9=u9[nq]
else
set t9=t9+1
set nq=t9
endif
if(nq>$7FE)then
return 0
endif
set w9[nq]=(nq-1)*4
set u9[nq]=-1
return nq
endfunction
function aq takes integer m,integer T returns integer
set Ep=m
set Fp=T
call TriggerEvaluate(ap)
return Kp
endfunction
function Bq takes integer nq returns nothing
set lp=nq
call TriggerEvaluate(Wo[kp[nq]])
endfunction
function bq takes nothing returns integer
local integer nq=lj
if(nq!=0)then
set lj=Kj[nq]
else
set Jj=Jj+1
set nq=Jj
endif
if(nq>8190)then
return 0
endif
set kp[nq]=85
set Kj[nq]=-1
return nq
endfunction
function Cq takes integer nq returns nothing
if nq==null then
return
elseif(Kj[nq]!=-1)then
return
endif
set lp=nq
call TriggerEvaluate(mp[kp[nq]])
set Kj[nq]=lj
set lj=nq
endfunction
function cq takes nothing returns integer
local integer nq=Bj
if(nq!=0)then
set Bj=Cj[nq]
else
set bj=bj+1
set nq=bj
endif
if(nq>8190)then
return 0
endif
set Cj[nq]=-1
return nq
endfunction
function Dq takes nothing returns integer
local integer nq=B9
if(nq!=0)then
set B9=C9[nq]
else
set b9=b9+1
set nq=b9
endif
if(nq>8190)then
return 0
endif
set H9[nq]=true
set C9[nq]=-1
return nq
endfunction
function Eq takes integer nq returns nothing
if nq==null then
return
elseif(C9[nq]!=-1)then
return
endif
set C9[nq]=B9
set B9=nq
endfunction
function Fq takes nothing returns integer
local integer nq=O9
if(nq!=0)then
set O9=Q9[nq]
else
set P9=P9+1
set nq=P9
endif
if(nq>8190)then
return 0
endif
set R9[nq]=0
set Q9[nq]=-1
return nq
endfunction
function Gq takes integer nq returns nothing
if nq==null then
return
elseif(Q9[nq]!=-1)then
return
endif
set Q9[nq]=O9
set O9=nq
endfunction
function Hq takes integer nq returns nothing
set lp=nq
call TriggerExecute(np)
endfunction
function Iq takes integer nq returns nothing
set lp=nq
call TriggerExecute(op)
endfunction
function lq takes integer nq returns nothing
set lp=nq
call TriggerExecute(pp)
endfunction
function Jq takes integer nq returns nothing
set lp=nq
call TriggerExecute(qp)
endfunction
function Kq takes integer nq,integer q returns nothing
set lp=nq
set Ep=q
call TriggerExecute(rp)
endfunction
function Lq takes nothing returns integer
local integer nq=T9
if(nq!=0)then
set T9=V9[nq]
else
set U9=U9+1
set nq=U9
endif
if(nq>$C)then
return 0
endif
set Y9[nq]=(nq-1)*608
set pd[nq]=(nq-1)*$A
set rd[nq]=(nq-1)*$A
set V9[nq]=-1
return nq
endfunction
function Mq takes nothing returns integer
local integer nq=ud
if(nq!=0)then
set ud=wd[nq]
else
set vd=vd+1
set nq=vd
endif
if(nq>8190)then
return 0
endif
set yd[nq]=$F7
set wd[nq]=-1
return nq
endfunction
function Nq takes integer nq returns nothing
if nq==null then
return
elseif(wd[nq]!=-1)then
return
endif
set wd[nq]=ud
set ud=nq
endfunction
function Oq takes nothing returns integer
local integer nq=Ad
if(nq!=0)then
set Ad=Bd[nq]
else
set ad=ad+1
set nq=ad
endif
if(nq>80)then
return 0
endif
set Cd[nq]=(nq-1)*101
set Bd[nq]=-1
return nq
endfunction
function Pq takes nothing returns integer
local integer nq=Ed
if(nq!=0)then
set Ed=Gd[nq]
else
set Fd=Fd+1
set nq=Fd
endif
if(nq>8190)then
return 0
endif
set ld[nq]=0
set Jd[nq]=0
set Xo[nq]=30
set Gd[nq]=-1
return nq
endfunction
function Qq takes integer nq returns nothing
if nq==null then
return
elseif(Gd[nq]!=-1)then
return
endif
set lp=nq
call TriggerEvaluate(Yo[Xo[nq]])
set Gd[nq]=Ed
set Ed=nq
endfunction
function Rq takes nothing returns integer
local integer nq=Pq()
local integer Sq
if(nq==0)then
return 0
endif
set Xo[nq]=31
set Sq=nq
set Kd[nq]=.0
return nq
endfunction
function Tq takes nothing returns integer
local integer nq=Pq()
local integer Sq
if(nq==0)then
return 0
endif
set Xo[nq]=32
set Sq=nq
set Ld[nq]=0
return nq
endfunction
function Uq takes nothing returns integer
local integer nq=Pq()
local integer Sq
if(nq==0)then
return 0
endif
set Xo[nq]=33
set Sq=nq
set Md[nq]=0
set Nd[nq]=.0
return nq
endfunction
function Vq takes integer nq returns real
set lp=nq
call TriggerEvaluate(Oo[ip[nq]])
return Jp
endfunction
function Wq takes integer nq returns real
set lp=nq
call TriggerEvaluate(Po[ip[nq]])
return Jp
endfunction
function Xq takes integer nq returns nothing
set lp=nq
call TriggerEvaluate(Qo[ip[nq]])
endfunction
function Yq takes integer nq,integer m returns nothing
set lp=nq
set Ep=m
call TriggerEvaluate(Ro[ip[nq]])
endfunction
function Zq takes integer nq,integer dr returns nothing
set lp=nq
set Ep=dr
call TriggerExecute(So[ip[nq]])
endfunction
function er takes integer nq,integer m returns nothing
set lp=nq
set Ep=m
call TriggerExecute(To[ip[nq]])
endfunction
function fr takes integer nq returns nothing
set lp=nq
call TriggerEvaluate(Uo[ip[nq]])
endfunction
function gr takes integer nq returns nothing
set lp=nq
call TriggerEvaluate(Vo[ip[nq]])
endfunction
function hr takes nothing returns integer
local integer nq=Mi
if(nq!=0)then
set Mi=Oi[nq]
else
set Ni=Ni+1
set nq=Ni
endif
if(nq>8190)then
return 0
endif
set Ti[nq]=false
set Zi[nq]=1.
set dj[nq]=false
set kj[nq]=true
set mj[nq]=0
set oj[nq]=0
set pj[nq]=0
set wj[nq]=0
set xj[nq]=0
set ip[nq]=70
set Oi[nq]=-1
return nq
endfunction
function jr takes integer nq returns nothing
if nq==null then
return
elseif(Oi[nq]!=-1)then
return
endif
set lp=nq
call TriggerEvaluate(jp[ip[nq]])
set Oi[nq]=Mi
set Mi=nq
endfunction
function kr takes nothing returns integer
local integer nq=Ii
if(nq!=0)then
set Ii=Ji[nq]
else
set li=li+1
set nq=li
endif
if(nq>8190)then
return 0
endif
set Ji[nq]=-1
return nq
endfunction
function mr takes nothing returns integer
local integer nq=Od
if(nq!=0)then
set Od=Qd[nq]
else
set Pd=Pd+1
set nq=Pd
endif
if(nq>8190)then
return 0
endif
set Rd[nq]=0
set Td[nq]=0
set Vd[nq]=0
set Wd[nq]=.0
set Zd[nq]=0
set de[nq]=.0
set fe[nq]=0
set ge[nq]=0
set he[nq]=false
set je[nq]=0
set ke[nq]=0
set me[nq]=false
set oe[nq]=0
set pe[nq]=0
set re[nq]=0
set se[nq]=0
set ue[nq]=0
set ve[nq]=0
set xe[nq]=0
set ye[nq]=0
set ze[nq]=false
set ae[nq]=0
set Be[nq]=0
set be[nq]=false
set ce[nq]=0
set De[nq]=0
set Ee[nq]=false
set Ge[nq]=0
set He[nq]=0
set Ie[nq]=false
set Qd[nq]=-1
return nq
endfunction
function nr takes integer nq returns nothing
set lp=nq
call TriggerExecute(tp)
endfunction
function pr takes nothing returns integer
local integer nq=le
if(nq!=0)then
set le=Ke[nq]
else
set Je=Je+1
set nq=Je
endif
if(nq>67)then
return 0
endif
set qf[nq]=(nq-1)*120
set Df[nq]=(nq-1)*32
set Ff[nq]=(nq-1)*32
set Hf[nq]=(nq-1)*32
set lf[nq]=(nq-1)*32
set Lf[nq]=(nq-1)*24
set Qe[nq]=true
set Te[nq]=false
set Ve[nq]=500
set We[nq]=100
set Xe[nq]=40
set Ye[nq]=.0
set Ze[nq]=100
set df[nq]=50
set ef[nq]=0
set rf[nq]=true
set bf[nq]=0
set Cf[nq]=500
set Jf[nq]=0
set Mf[nq]=0
set Nf[nq]=0
set Zf[nq]=0
set Ke[nq]=-1
return nq
endfunction
function qr takes integer nq returns nothing
set lp=nq
call TriggerEvaluate(Jo[gp[nq]])
endfunction
function rr takes integer nq,integer sr returns nothing
set lp=nq
set Ep=sr
call TriggerEvaluate(Ko[gp[nq]])
endfunction
function tr takes integer nq,integer sr,integer ur,integer vr returns boolean
set lp=nq
set Ep=sr
set Fp=ur
set Gp=vr
call TriggerEvaluate(Lo[gp[nq]])
return Lp
endfunction
function wr takes integer nq,real r returns nothing
set lp=nq
set Hp=r
call TriggerEvaluate(zp)
endfunction
function xr takes integer nq returns nothing
set lp=nq
call TriggerEvaluate(Mo[gp[nq]])
endfunction
function yr takes integer nq returns nothing
set lp=nq
call TriggerEvaluate(No[gp[nq]])
endfunction
function zr takes nothing returns integer
local integer nq=Qh
if(nq!=0)then
set Qh=Sh[nq]
else
set Rh=Rh+1
set nq=Rh
endif
if(nq>8190)then
return 0
endif
set Wh[nq]=false
set gp[nq]=60
set Sh[nq]=-1
return nq
endfunction
function Ar takes integer nq returns nothing
if nq==null then
return
elseif(Sh[nq]!=-1)then
return
endif
set lp=nq
call TriggerEvaluate(hp[gp[nq]])
set Sh[nq]=Qh
set Qh=nq
endfunction
function ar takes nothing returns integer
local integer nq=Ih
if(nq!=0)then
set Ih=Jh[nq]
else
set lh=lh+1
set nq=lh
endif
if(nq>8190)then
return 0
endif
set Jh[nq]=-1
return nq
endfunction
function Br takes nothing returns integer
local integer nq=Dh
if(nq!=0)then
set Dh=Fh[nq]
else
set Eh=Eh+1
set nq=Eh
endif
if(nq>8190)then
return 0
endif
set Fh[nq]=-1
return nq
endfunction
function br takes nothing returns integer
local integer nq=wh
if(nq!=0)then
set wh=yh[nq]
else
set xh=xh+1
set nq=xh
endif
if(nq>8190)then
return 0
endif
set yh[nq]=-1
return nq
endfunction
function Cr takes integer nq returns nothing
set lp=nq
call TriggerEvaluate(Ao[Zo[nq]])
endfunction
function cr takes integer nq returns real
set lp=nq
call TriggerEvaluate(ao[Zo[nq]])
return Jp
endfunction
function Dr takes integer nq returns integer
set lp=nq
call TriggerEvaluate(Bo[Zo[nq]])
return Kp
endfunction
function Er takes integer nq returns integer
set lp=nq
call TriggerEvaluate(bo[Zo[nq]])
return Kp
endfunction
function Fr takes integer nq returns boolean
set lp=nq
call TriggerEvaluate(Co[Zo[nq]])
return Lp
endfunction
function Gr takes integer nq returns boolean
set lp=nq
call TriggerEvaluate(co[Zo[nq]])
return Lp
endfunction
function Hr takes integer nq returns boolean
set lp=nq
call TriggerEvaluate(Do[Zo[nq]])
return Lp
endfunction
function Ir takes integer nq returns boolean
set lp=nq
call TriggerEvaluate(Eo[Zo[nq]])
return Lp
endfunction
function lr takes integer nq returns boolean
set lp=nq
call TriggerEvaluate(Fo[Zo[nq]])
return Lp
endfunction
function Jr takes integer nq,real Kr returns real
set lp=nq
set Hp=Kr
call TriggerEvaluate(Go[Zo[nq]])
return Jp
endfunction
function Lr takes integer nq,real Kr returns real
set lp=nq
set Hp=Kr
call TriggerEvaluate(Ho[Zo[nq]])
return Jp
endfunction
function Mr takes integer nq,real Kr returns real
set lp=nq
set Hp=Kr
call TriggerEvaluate(Io[Zo[nq]])
return Jp
endfunction
function Nr takes integer nq,boolean Pr returns nothing
set lp=nq
set Ip=Pr
call TriggerExecute(vp)
endfunction
function Qr takes nothing returns integer
local integer nq=eg
if(nq!=0)then
set eg=gg[nq]
else
set fg=fg+1
set nq=fg
endif
if(nq>8190)then
return 0
endif
set rg[nq]=0
set sg[nq]=false
set tg[nq]=true
set Zo[nq]=42
set gg[nq]=-1
return nq
endfunction
function Rr takes integer nq returns nothing
if nq==null then
return
elseif(gg[nq]!=-1)then
return
endif
set lp=nq
call TriggerEvaluate(dp[Zo[nq]])
set gg[nq]=eg
set eg=nq
endfunction
function Sr takes integer nq returns boolean
set lp=nq
call TriggerEvaluate(lo[ep[nq]])
return Lp
endfunction
function Tr takes nothing returns integer
local integer nq=Jg
if(nq!=0)then
set Jg=Lg[nq]
else
set Kg=Kg+1
set nq=Kg
endif
if(nq>8190)then
return 0
endif
set Rg[nq]=0
set Sg[nq]=false
set ep[nq]=43
set Lg[nq]=-1
return nq
endfunction
function Ur takes integer nq returns nothing
if nq==null then
return
elseif(Lg[nq]!=-1)then
return
endif
set lp=nq
call TriggerEvaluate(fp[ep[nq]])
set Lg[nq]=Jg
set Jg=nq
endfunction
function Vr takes nothing returns integer
local integer nq=Tr()
local integer Sq
if(nq==0)then
return 0
endif
set ep[nq]=54
set Sq=nq
return nq
endfunction
function Wr takes nothing returns integer
local integer nq=Tr()
local integer Sq
if(nq==0)then
return 0
endif
set ep[nq]=53
set Sq=nq
set vh[nq]=false
return nq
endfunction
function Xr takes nothing returns integer
local integer nq=Tr()
local integer Sq
if(nq==0)then
return 0
endif
set ep[nq]=52
set Sq=nq
return nq
endfunction
function Yr takes nothing returns integer
local integer nq=Tr()
local integer Sq
if(nq==0)then
return 0
endif
set ep[nq]=51
set Sq=nq
return nq
endfunction
function Zr takes nothing returns integer
local integer nq=Tr()
local integer Sq
if(nq==0)then
return 0
endif
set ep[nq]=50
set Sq=nq
set uh[nq]=false
return nq
endfunction
function ds takes nothing returns integer
local integer nq=Tr()
local integer Sq
if(nq==0)then
return 0
endif
set ep[nq]=49
set Sq=nq
return nq
endfunction
function es takes nothing returns integer
local integer nq=Tr()
local integer Sq
if(nq==0)then
return 0
endif
set ep[nq]=48
set Sq=nq
return nq
endfunction
function fs takes nothing returns integer
local integer nq=Tr()
local integer Sq
if(nq==0)then
return 0
endif
set ep[nq]=47
set Sq=nq
return nq
endfunction
function gs takes nothing returns integer
local integer nq=ph
if(nq!=0)then
set ph=rh[nq]
else
set qh=qh+1
set nq=qh
endif
if(nq>8190)then
return 0
endif
set rh[nq]=-1
return nq
endfunction
function hs takes integer nq returns nothing
if nq==null then
return
elseif(rh[nq]!=-1)then
return
endif
set rh[nq]=ph
set ph=nq
endfunction
function is takes nothing returns integer
local integer nq=gh
if(nq!=0)then
set gh=ih[nq]
else
set hh=hh+1
set nq=hh
endif
if(nq>8190)then
return 0
endif
set nh[nq]=0
set oh[nq]=0
set ih[nq]=-1
return nq
endfunction
function js takes integer nq returns nothing
if nq==null then
return
elseif(ih[nq]!=-1)then
return
endif
set ih[nq]=gh
set gh=nq
endfunction
function ks takes nothing returns integer
local integer nq=Wg
if(nq!=0)then
set Wg=Yg[nq]
else
set Xg=Xg+1
set nq=Xg
endif
if(nq>8190)then
return 0
endif
set Yg[nq]=-1
return nq
endfunction
function ms takes nothing returns integer
local integer nq=hr()
local integer Sq
if(nq==0)then
return 0
endif
set ip[nq]=105
set Sq=nq
return nq
endfunction
function ns takes nothing returns integer
local integer nq=Tr()
local integer Sq
if(nq==0)then
return 0
endif
set ep[nq]=55
set Sq=nq
return nq
endfunction
function os takes nothing returns integer
local integer nq=Tr()
local integer Sq
if(nq==0)then
return 0
endif
set ep[nq]=56
set Sq=nq
return nq
endfunction
function ps takes nothing returns integer
local integer nq=zr()
local integer Sq
if(nq==0)then
return 0
endif
set gp[nq]=61
set Sq=nq
return nq
endfunction
function qs takes nothing returns integer
local integer nq=zr()
local integer Sq
if(nq==0)then
return 0
endif
set gp[nq]=62
set Sq=nq
set Zh[nq]=false
set di[nq]=0
set ei[nq]=.0
return nq
endfunction
function rs takes nothing returns integer
local integer nq=zr()
local integer Sq
if(nq==0)then
return 0
endif
set gp[nq]=63
set Sq=nq
return nq
endfunction
function ss takes nothing returns integer
local integer nq=zr()
local integer Sq
if(nq==0)then
return 0
endif
set gp[nq]=64
set Sq=nq
return nq
endfunction
function ts takes nothing returns integer
local integer nq=zr()
local integer Sq
if(nq==0)then
return 0
endif
set gp[nq]=65
set Sq=nq
set qi[nq]=false
set ri[nq]=.0
set si[nq]=0
set ti[nq]=0
set ui[nq]=0
return nq
endfunction
function us takes nothing returns integer
local integer nq=zr()
local integer Sq
if(nq==0)then
return 0
endif
set gp[nq]=66
set Sq=nq
return nq
endfunction
function vs takes nothing returns integer
local integer nq=zr()
local integer Sq
if(nq==0)then
return 0
endif
set gp[nq]=67
set Sq=nq
return nq
endfunction
function ws takes nothing returns integer
local integer nq=zr()
local integer Sq
if(nq==0)then
return 0
endif
set gp[nq]=68
set Sq=nq
return nq
endfunction
function xs takes nothing returns integer
local integer nq=Tr()
local integer Sq
if(nq==0)then
return 0
endif
set ep[nq]=71
set Sq=nq
return nq
endfunction
function ys takes nothing returns integer
local integer nq=Tr()
local integer Sq
if(nq==0)then
return 0
endif
set ep[nq]=72
set Sq=nq
return nq
endfunction
function zs takes nothing returns integer
local integer nq=Tr()
local integer Sq
if(nq==0)then
return 0
endif
set ep[nq]=73
set Sq=nq
return nq
endfunction
function As takes nothing returns integer
local integer nq=Tr()
local integer Sq
if(nq==0)then
return 0
endif
set ep[nq]=74
set Sq=nq
return nq
endfunction
function as takes nothing returns integer
local integer nq=Tr()
local integer Sq
if(nq==0)then
return 0
endif
set ep[nq]=75
set Sq=nq
return nq
endfunction
function Bs takes nothing returns integer
local integer nq=Tr()
local integer Sq
if(nq==0)then
return 0
endif
set ep[nq]=76
set Sq=nq
return nq
endfunction
function bs takes nothing returns integer
local integer nq=Tr()
local integer Sq
if(nq==0)then
return 0
endif
set ep[nq]=77
set Sq=nq
return nq
endfunction
function Cs takes nothing returns integer
local integer nq=Qr()
local integer Sq
if(nq==0)then
return 0
endif
set Zo[nq]=78
set Sq=nq
return nq
endfunction
function cs takes nothing returns integer
local integer nq=Qr()
local integer Sq
if(nq==0)then
return 0
endif
set Zo[nq]=79
set Sq=nq
return nq
endfunction
function Ds takes nothing returns integer
local integer nq=Qr()
local integer Sq
if(nq==0)then
return 0
endif
set Zo[nq]=80
set Sq=nq
return nq
endfunction
function Es takes nothing returns integer
local integer nq=Qr()
local integer Sq
if(nq==0)then
return 0
endif
set Zo[nq]=81
set Sq=nq
return nq
endfunction
function Fs takes nothing returns integer
local integer nq=Qr()
local integer Sq
if(nq==0)then
return 0
endif
set Zo[nq]=82
set Sq=nq
return nq
endfunction
function Gs takes nothing returns integer
local integer nq=Qr()
local integer Sq
if(nq==0)then
return 0
endif
set Zo[nq]=83
set Sq=nq
return nq
endfunction
function Hs takes integer m,integer T returns integer
local integer nq=aq(m,T)
local integer Sq
if(nq==0)then
return 0
endif
set kp[nq]=86
set Sq=nq
set Sj[nq]=10.
set Tj[nq]=.0
set Uj[nq]=1.
set Vj[nq]=1.
return nq
endfunction
function Is takes integer m,integer T returns integer
local integer nq=aq(m,T)
local integer Sq
if(nq==0)then
return 0
endif
set kp[nq]=87
set Sq=nq
set dk[nq]=3.
return nq
endfunction
function ls takes integer m,integer T returns integer
local integer nq=aq(m,T)
local integer Sq
if(nq==0)then
return 0
endif
set kp[nq]=88
set Sq=nq
return nq
endfunction
function Js takes integer m,integer T returns integer
local integer nq=aq(m,T)
local integer Sq
if(nq==0)then
return 0
endif
set kp[nq]=89
set Sq=nq
set ik[nq]=null
set jk[nq]=false
set kk[nq]=.0
return nq
endfunction
function Ks takes integer m,integer T returns integer
local integer nq=aq(m,T)
local integer Sq
if(nq==0)then
return 0
endif
set kp[nq]=90
set Sq=nq
set pk[nq]=1
return nq
endfunction
function Ls takes integer m,integer T returns integer
local integer nq=aq(m,T)
local integer Sq
if(nq==0)then
return 0
endif
set kp[nq]=91
set Sq=nq
set xk[nq]=0
set yk[nq]=1
set zk[nq]=384.
set ak[nq]=5
set ck[nq]=5
return nq
endfunction
function Ms takes integer m,integer T returns integer
local integer nq=aq(m,T)
local integer Sq
if(nq==0)then
return 0
endif
set kp[nq]=92
set Sq=nq
set Lk[nq]=1.
set Mk[nq]=.0
set Nk[nq]=.0
return nq
endfunction
function Ns takes integer m,integer T returns integer
local integer nq=aq(m,T)
local integer Sq
if(nq==0)then
return 0
endif
set kp[nq]=93
set Sq=nq
return nq
endfunction
function Os takes integer m,integer T returns integer
local integer nq=aq(m,T)
local integer Sq
if(nq==0)then
return 0
endif
set kp[nq]=94
set Sq=nq
set dm[nq]=0
set em[nq]=.1
set fm[nq]=Yk
set gm[nq]=false
set jm[nq]=0
return nq
endfunction
function Ps takes nothing returns integer
local integer nq=hr()
local integer Sq
if(nq==0)then
return 0
endif
set ip[nq]=96
set Sq=nq
return nq
endfunction
function Qs takes nothing returns integer
local integer nq=hr()
local integer Sq
if(nq==0)then
return 0
endif
set ip[nq]=97
set Sq=nq
return nq
endfunction
function Rs takes nothing returns integer
local integer nq=hr()
local integer Sq
if(nq==0)then
return 0
endif
set ip[nq]=98
set Sq=nq
return nq
endfunction
function Ss takes nothing returns integer
local integer nq=hr()
local integer Sq
if(nq==0)then
return 0
endif
set ip[nq]=99
set Sq=nq
return nq
endfunction
function Ts takes nothing returns integer
local integer nq=hr()
local integer Sq
if(nq==0)then
return 0
endif
set ip[nq]=100
set Sq=nq
return nq
endfunction
function Us takes nothing returns integer
local integer nq=hr()
local integer Sq
if(nq==0)then
return 0
endif
set ip[nq]=101
set Sq=nq
return nq
endfunction
function Vs takes nothing returns integer
local integer nq=hr()
local integer Sq
if(nq==0)then
return 0
endif
set ip[nq]=102
set Sq=nq
return nq
endfunction
function Ws takes nothing returns integer
local integer nq=hr()
local integer Sq
if(nq==0)then
return 0
endif
set ip[nq]=103
set Sq=nq
return nq
endfunction
function Xs takes nothing returns integer
local integer nq=hr()
local integer Sq
if(nq==0)then
return 0
endif
set ip[nq]=104
set Sq=nq
return nq
endfunction
function Ys takes integer i,integer a1,integer a2,integer a3 returns nothing
set Ep=a1
set Fp=a2
set Gp=a3
call TriggerExecute(bp[i])
endfunction
function Zs takes string s returns nothing
endfunction
function dt takes integer a returns boolean
return a/ 2*2==a
endfunction
function et takes integer a,integer n returns boolean
return a/ n*n==a
endfunction
function gt takes real r returns integer
local integer i=R2I(r)
if r-i==1. then
return i+1
endif
return i
endfunction
function ht takes real r returns integer
local integer i=R2I(r)
if r==i then
return i
endif
return i+1
endfunction
function it takes real r returns integer
local integer i=R2I(r)
local real ir=i+.5
if(r==ir and dt(i))or r<ir then
return i
endif
return i+1
endfunction
function jt takes boolean b returns integer
if b then
return 1
endif
return 0
endfunction
function kt takes player p returns boolean
return GetPlayerController(p)==MAP_CONTROL_USER and GetPlayerSlotState(p)==PLAYER_SLOT_STATE_PLAYING
endfunction
function mt takes nothing returns nothing
call PauseUnit(GetFilterUnit(),bj_pauseAllUnitsFlag)
endfunction
function nt takes string s,unit u,player p,real ot,real pt,real qt,real rt returns texttag
set bj_lastCreatedTextTag=CreateTextTag()
call SetTextTagText(bj_lastCreatedTextTag,s,ot*.0025)
call SetTextTagPosUnit(bj_lastCreatedTextTag,u,.0)
call SetTextTagVelocity(bj_lastCreatedTextTag,.0,pt/ 1600.)
call SetTextTagVisibility(bj_lastCreatedTextTag,P==p)
call SetTextTagPermanent(bj_lastCreatedTextTag,false)
call SetTextTagLifespan(bj_lastCreatedTextTag,qt)
call SetTextTagFadepoint(bj_lastCreatedTextTag,rt)
return bj_lastCreatedTextTag
endfunction
function tt takes string ut,real ot,real x,real y,integer vt returns image
set bj_lastCreatedImage=CreateImage(ut,ot,ot,.0,x,y,.0,ot/ 2.,ot/ 2.,.0,vt)
call SetImageRenderAlways(bj_lastCreatedImage,true)
return bj_lastCreatedImage
endfunction
function wt takes player p,string ut,real ot,real x,real y,integer vt returns image
set bj_lastCreatedImage=CreateImage(ut,ot,ot,.0,x,y,.0,ot/ 2.,ot/ 2.,.0,vt)
call SetImageRenderAlways(bj_lastCreatedImage,P==p)
return bj_lastCreatedImage
endfunction
function xt takes rect r returns real
return GetRandomReal(GetRectMinX(r),GetRectMaxX(r))
endfunction
function yt takes rect r returns real
return GetRandomReal(GetRectMinY(r),GetRectMaxY(r))
endfunction
function zt takes player p,string s,widget w,string At returns effect
if P!=p then
set s=""
endif
return AddSpecialEffectTarget(s,w,At)
endfunction
function EndThread takes nothing returns nothing
call I2R(1/ 0)
endfunction
function at takes nothing returns nothing
set P=GetLocalPlayer()
endfunction
function GetCustomTimer takes nothing returns integer
return LoadInteger(f7,3,GetHandleId(GetExpiredTimer()))
endfunction
function Bt takes timer t returns nothing
call PauseTimer(t)
call RemoveSavedInteger(f7,3,GetHandleId(t))
call DestroyTimer(t)
endfunction
function IsFilterUnitTower takes nothing returns boolean
return(IsUnitType(GetFilterUnit(),V))!=null
endfunction
function bt takes nothing returns boolean
return(IsUnitType(GetFilterUnit(),X))!=null
endfunction
function Ct takes real ct,real x,real y,integer vt,integer Dt,integer Et,integer Ft,integer Gt returns image
call tt("Textures\\RangeIndicator.blp",ct+ct,x,y,vt)
call SetImageColor(bj_lastCreatedImage,Dt,Et,Ft,Gt)
return bj_lastCreatedImage
endfunction
function Ht takes player p,real ct,real x,real y,integer vt,integer Dt,integer Et,integer Ft,integer Gt returns image
call wt(p,"Textures\\RangeIndicator.blp",ct+ct,x,y,vt)
call SetImageColor(bj_lastCreatedImage,Dt,Et,Ft,Gt)
return bj_lastCreatedImage
endfunction
function It takes integer p returns nothing
set u7=p
call TriggerExecute(v7)
endfunction
function lt takes integer Jt returns nothing
set Q=false
set q7=Jt+1
call TriggerExecute(q8)
endfunction
function Kt takes string Lt,string Mt returns nothing
if Q then
call PreloadGenClear()
call Preload("\" )        "+(Lt)+".                                //")
call PreloadGenEnd("WispTD\\Errors\\"+Mt+".txt")
call lt(7)
call DisplayTimedTextToPlayer(P,.0,.0,((7.)*1.),("An error occurred. Game will end in 7 seconds."))
endif
call I2R(1/ 0)
endfunction
function Nt takes nothing returns nothing
set d4=Filter(function bt)
set S[0]=Player($A)
set S[1]=Player($B)
endfunction
function Ot takes integer nq,integer Pt returns integer
set w8[nq]=w8[nq]+1
set u8[v8[nq]+w8[nq]]=Pt
return w8[nq]
endfunction
function Qt takes integer nq,integer Pt returns nothing
set B8[nq]=B8[nq]+1
set A8[a8[nq]+B8[nq]]=Pt
set Ne[Pt]=B8[nq]
endfunction
function Rt takes integer nq,integer Pt returns nothing
local integer i=Ne[Pt]
if B8[nq]>i then
set A8[a8[nq]+i]=A8[a8[nq]+B8[nq]]
set Ne[A8[a8[nq]+i]]=i
endif
set B8[nq]=B8[nq]-1
endfunction
function St takes integer nq,integer Pt returns nothing
set F8[nq]=F8[nq]+1
set D8[E8[nq]+F8[nq]]=Pt
set yj[Pt]=F8[nq]
endfunction
function Tt takes integer nq,integer Pt returns nothing
local integer i=yj[Pt]
if F8[nq]>i then
set D8[E8[nq]+i]=D8[E8[nq]+F8[nq]]
set yj[D8[E8[nq]+i]]=i
endif
set F8[nq]=F8[nq]-1
endfunction
function Ut takes integer nq,integer Pt returns nothing
set K8[nq]=K8[nq]+1
if K8[nq]==105 then
call Kt("Error: not enough cells in ForceArray "+I2S(nq)+". Current amount of cells is "+I2S(105),"ForceArrayError")
endif
set l8[J8[nq]+K8[nq]]=Pt
set Hg[Pt]=K8[nq]
endfunction
function Vt takes integer nq,integer Pt returns nothing
local integer i=Hg[Pt]
if K8[nq]>i then
set l8[J8[nq]+i]=l8[J8[nq]+K8[nq]]
set Hg[l8[J8[nq]+i]]=i
endif
set K8[nq]=K8[nq]-1
endfunction
function Wt takes integer nq,integer Pt returns nothing
set Q8[nq]=Q8[nq]+1
if Q8[nq]==105 then
call Kt("Error: not enough cells in SpawnArray "+I2S(nq)+". Current amount of cells is "+I2S(105),"SpawnArrayError")
endif
set O8[P8[nq]+Q8[nq]]=Pt
set Hg[Pt]=Q8[nq]
endfunction
function Xt takes integer nq,integer Pt returns nothing
local integer i=Hg[Pt]
if Q8[nq]>i then
set O8[P8[nq]+i]=O8[P8[nq]+Q8[nq]]
set Hg[O8[P8[nq]+i]]=i
endif
set Q8[nq]=Q8[nq]-1
endfunction
function Yt takes integer nq,integer Pt returns nothing
set W8[nq]=W8[nq]+1
if W8[nq]==105 then
call Kt("Error: not enough cells in MinionArray "+I2S(nq)+". Current amount of cells is "+I2S(105),"MinionArrayError")
endif
set U8[V8[nq]+W8[nq]]=Pt
set Hg[Pt]=W8[nq]
endfunction
function Zt takes integer nq,integer Pt returns nothing
local integer i=Hg[Pt]
if W8[nq]>i then
set U8[V8[nq]+i]=U8[V8[nq]+W8[nq]]
set Hg[U8[V8[nq]+i]]=i
endif
set W8[nq]=W8[nq]-1
endfunction
function du takes integer nq,integer Pt returns nothing
set f9[nq]=f9[nq]+1
if f9[nq]==105 then
call Kt("Error: not enough cells in DeadArray "+I2S(nq)+". Current amount of cells is "+I2S(105),"DeadArrayError")
endif
set d9[e9[nq]+f9[nq]]=Pt
set Hg[Pt]=f9[nq]
endfunction
function eu takes integer nq,integer Pt returns nothing
local integer i=Hg[Pt]
if f9[nq]>i then
set d9[e9[nq]+i]=d9[e9[nq]+f9[nq]]
set Hg[d9[e9[nq]+i]]=i
endif
set f9[nq]=f9[nq]-1
endfunction
function fu takes integer nq,integer Pt returns nothing
set m9[nq]=m9[nq]+1
if m9[nq]==105 then
call Kt("Error: not enough cells in SentMinionArray "+I2S(nq)+". Current amount of cells is "+I2S(105),"SentMinionArrayError")
endif
set j9[k9[nq]+m9[nq]]=Pt
set Ig[Pt]=m9[nq]
endfunction
function gu takes integer nq,integer Pt returns nothing
local integer i=Ig[Pt]
if m9[nq]>i then
set j9[k9[nq]+i]=j9[k9[nq]+m9[nq]]
set Ig[j9[k9[nq]+i]]=i
endif
set m9[nq]=m9[nq]-1
endfunction
function hu takes integer T,integer iu,integer ju,integer ku,integer mu,integer nu,integer ou,integer pu,integer qu,string ru returns nothing
set e4[T]=Aq()
set v9[w9[e4[T]]]=iu
set v9[w9[e4[T]]+1]=ju
set v9[w9[e4[T]]+2]=ku
set v9[w9[e4[T]]+3]=mu
set x9[e4[T]]=d7[nu]
set y9[e4[T]]=ou
set z9[e4[T]]=pu
set A9[e4[T]]=qu
set a9[e4[T]]=ru
endfunction
function tu takes integer p returns nothing
local integer a=0
local integer b
local integer uu
local integer vu
local integer wu=Sf[p]
local integer xu=Tf[p]
loop
set uu=v9[w9[wu]+a]
set vu=v9[w9[xu]+a]
set b=0
loop
call UnitRemoveAbility(uf[p],bn[Cn[Y4[uu]]+b])
call UnitRemoveAbility(uf[p],cn[Dn[Y4[uu]]+b])
call UnitAddAbility(uf[p],bn[Cn[Y4[vu]]+b])
call UnitAddAbility(uf[p],cn[Dn[Y4[vu]]+b])
set b=b+1
exitwhen b==3
endloop
set a=a+1
exitwhen a==4
endloop
set Sf[p]=xu
set Tf[p]=wu
endfunction
function yu takes integer p returns nothing
local integer a=0
local integer wu=Qf[p]
local integer xu=Rf[p]
loop
call UnitRemoveAbility(tf[p],Xm[W4[v9[w9[wu]+a]]])
call UnitAddAbility(tf[p],Xm[W4[v9[w9[xu]+a]]])
set a=a+1
exitwhen a==4
endloop
call UnitRemoveAbility(tf[p],y9[wu])
call UnitAddAbility(tf[p],y9[xu])
set Qf[p]=xu
set Rf[p]=wu
endfunction
function zu takes integer f,integer i returns integer
local integer nq=Dq()
set F9[nq]=f
set G9[nq]=i
set E9[nq]=n4-i/ m4
set c9[nq]=E9[nq]
if i<o4 then
set I9[nq]=true
else
set I9[nq]=false
set D9[nq]=E9[nq]
endif
return nq
endfunction
function Au takes integer nq returns nothing
if G9[nq]<m4 then
set l9[nq]=0
else
set l9[nq]=X9[Y9[F9[nq]]+G9[nq]-m4]
endif
if G9[nq]+m4>608 then
set L9[nq]=0
else
set L9[nq]=X9[Y9[F9[nq]]+G9[nq]+m4]
endif
if et(G9[nq]+1,m4)then
set K9[nq]=0
else
set K9[nq]=X9[Y9[F9[nq]]+G9[nq]+1]
endif
if et(G9[nq],m4)then
set J9[nq]=0
else
set J9[nq]=X9[Y9[F9[nq]]+G9[nq]-1]
endif
endfunction
function au takes integer nq returns boolean
return nq!=0 and I9[nq]and H9[nq]
endfunction
function Bu takes integer nq returns integer
local integer bu=R9[nq]
set R9[nq]=M9[bu]
return bu
endfunction
function Cu takes integer nq,integer c returns nothing
if(R9[(nq)]==0)then
set R9[nq]=c
set S9[nq]=c
set M9[c]=0
else
set M9[S9[nq]]=c
set N9[c]=S9[nq]
set M9[c]=0
set S9[nq]=c
endif
endfunction
function cu takes integer nq,integer c,integer Du returns nothing
if au(c)then
call Cu(nq,c)
set I9[(c)]=false
set D9[(c)]=(Du)
endif
endfunction
function Eu takes integer f returns integer
local integer nq=Fq()
local integer a=p4
loop
set a=a-1
call Cu(nq,X9[Y9[f]+a])
exitwhen a<o4
endloop
return nq
endfunction
function Fu takes integer p returns integer
local integer nq=Lq()
local real Gu=i4+Me[p]*(1024.+512.)
local real Hu=Gu+1024.
set W9[nq]=p
set Z9[nq]=Hu
set ed[nq]=Gu
set fd[nq]=Hu-64.
set gd[nq]=Gu+64.
set hd[nq]=Rect(Gu,1408.,Hu,1920.)
set jd[nq]=Rect(Gu,640.,Hu,1152.)
set md[nq]=Rect(Gu,h4,Hu,g4)
set kd[nq]=Rect(gd[nq],f4,fd[nq],320.)
set nd[nq]=Rect(Gu,h4,Hu,1152.)
call RegionAddRect(h7,jd[nq])
call RegionAddRect(g7,md[nq])
call Iq(nq)
return nq
endfunction
function Iu takes integer nq returns nothing
local integer a=-1
local integer p
loop
set a=a+1
set p=(u8[v8[(j7)]+(a)])
if Qe[p]then
set od[pd[nq]+a]=CreateFogModifierRect(Le[W9[nq]],FOG_OF_WAR_VISIBLE,nd[Ue[p]],true,true)
set qd[rd[nq]+a]=CreateFogModifierRect(Le[W9[nq]],FOG_OF_WAR_VISIBLE,hd[Ue[p]],true,true)
call FogModifierStart(od[pd[nq]+a])
call FogModifierStart(qd[rd[nq]+a])
endif
exitwhen a==w8[j7]
endloop
endfunction
function lu takes integer nq returns nothing
local integer a=-1
local integer jq=Me[W9[nq]]
local integer f
local integer p
loop
set a=a+1
set p=(u8[v8[(j7)]+(a)])
if Te[p]then
set f=Ue[p]
call DestroyFogModifier(od[pd[f]+jq])
set od[pd[f]+jq]=null
call DestroyFogModifier(qd[rd[f]+jq])
set qd[rd[f]+jq]=null
call SetFogStateRect(Le[p],FOG_OF_WAR_MASKED,nd[nq],true)
call SetFogStateRect(Le[p],FOG_OF_WAR_MASKED,hd[nq],true)
endif
exitwhen a==w8[j7]
endloop
call RegionClearRect(h7,jd[nq])
call RegionClearRect(g7,md[nq])
call RemoveRect(nd[nq])
set nd[nq]=null
call RemoveRect(hd[nq])
set hd[nq]=null
call RemoveRect(jd[nq])
set jd[nq]=null
call RemoveRect(md[nq])
set md[nq]=null
call RemoveRect(kd[nq])
set kd[nq]=null
call lq(nq)
endfunction
function Ju takes integer nq,real x,real y returns integer
local integer ix
local integer iy
set ix=R2I(x-ed[nq])/ 64
if ix==m4 then
set ix=m4-1
endif
set iy=R2I(640.-y)/ 64
if iy==n4 then
set iy=n4-1
endif
return X9[Y9[nq]+iy*m4+ix]
endfunction
function Ku takes integer nq returns nothing
local integer q=Eu(nq)
call Kq(nq,q)
if td[nq]then
call Kt("Error: updating ground distaces in field "+I2S(nq)+" was not finished","BFSError")
endif
call Gq(q)
call Jq(nq)
endfunction
function Lu takes integer nq returns nothing
set zd[nq]=CreateTimer()
call SaveInteger(f7,3,GetHandleId((zd[nq])),(nq))
endfunction
function Mu takes integer nq returns nothing
call Bt(zd[nq])
set zd[nq]=null
endfunction
function Nu takes nothing returns nothing
local integer nq=(LoadInteger(f7,3,GetHandleId(GetExpiredTimer())))
set yd[nq]=yd[nq]-8
if yd[nq]>0 then
call SetUnitVertexColor(xd[nq],$FF,$FF,$FF,yd[nq])
else
call RemoveUnit(xd[nq])
set xd[nq]=null
call Mu(nq)
call Nq(nq)
call Zs("MinionDeleter "+I2S(nq)+" is ended.")
endif
endfunction
function Ou takes unit m returns nothing
local integer nq=Mq()
set xd[nq]=m
call Lu(nq)
call SetUnitInvulnerable(xd[nq],true)
call PauseUnit(xd[nq],true)
call SetUnitVertexColor(xd[nq],$FF,$FF,$FF,yd[nq])
call TimerStart(zd[nq],.05,true,function Nu)
call Zs("MinionDeleter "+I2S(nq)+" is started.")
endfunction
function Pu takes integer Qu returns nothing
set q4=Oq()
set Dd[q4]=Qu
set bd[Cd[q4]]=1.
set bd[Cd[q4]+1]=1.075
set bd[Cd[q4]+2]=1.155625
set bd[Cd[q4]+3]=1.242297
set bd[Cd[q4]+4]=1.335469
set bd[Cd[q4]+5]=1.435629
set bd[Cd[q4]+6]=1.543302
set bd[Cd[q4]+7]=1.659049
set bd[Cd[q4]+8]=1.783478
set bd[Cd[q4]+9]=1.917239
set bd[Cd[q4]+$A]=2.061032
set bd[Cd[q4]+$B]=2.215609
set bd[Cd[q4]+$C]=2.38178
set bd[Cd[q4]+$D]=2.560413
set bd[Cd[q4]+$E]=2.752444
set bd[Cd[q4]+$F]=2.958877
set bd[Cd[q4]+16]=3.180793
set bd[Cd[q4]+17]=3.419353
set bd[Cd[q4]+18]=3.675804
set bd[Cd[q4]+19]=3.951489
set bd[Cd[q4]+20]=4.247851
set bd[Cd[q4]+21]=4.56644
set bd[Cd[q4]+22]=4.908923
set bd[Cd[q4]+23]=5.277092
set bd[Cd[q4]+24]=5.672874
set bd[Cd[q4]+25]=6.09834
set bd[Cd[q4]+26]=6.555715
set bd[Cd[q4]+27]=7.047394
set bd[Cd[q4]+28]=7.575948
set bd[Cd[q4]+29]=8.144144
set bd[Cd[q4]+30]=8.754955
set bd[Cd[q4]+31]=9.411577
set bd[Cd[q4]+32]=10.117445
set bd[Cd[q4]+33]=10.876253
set bd[Cd[q4]+34]=11.691972
set bd[Cd[q4]+35]=12.56887
set bd[Cd[q4]+36]=13.511536
set bd[Cd[q4]+37]=14.524901
set bd[Cd[q4]+38]=15.614268
set bd[Cd[q4]+39]=16.785339
set bd[Cd[q4]+40]=18.044239
set bd[Cd[q4]+41]=19.397557
set bd[Cd[q4]+42]=20.852374
set bd[Cd[q4]+43]=22.416302
set bd[Cd[q4]+44]=24.097524
set bd[Cd[q4]+45]=25.904839
set bd[Cd[q4]+46]=27.847702
set bd[Cd[q4]+47]=29.936279
set bd[Cd[q4]+48]=32.1815
set bd[Cd[q4]+49]=34.595113
set bd[Cd[q4]+50]=37.189746
set bd[Cd[q4]+51]=39.978977
set bd[Cd[q4]+52]=42.9774
set bd[Cd[q4]+53]=46.200705
set bd[Cd[q4]+54]=49.665758
set bd[Cd[q4]+55]=53.39069
set bd[Cd[q4]+56]=57.394992
set bd[Cd[q4]+57]=61.699616
set bd[Cd[q4]+58]=66.327087
set bd[Cd[q4]+59]=71.301619
set bd[Cd[q4]+60]=76.64924
set bd[Cd[q4]+61]=82.397933
set bd[Cd[q4]+62]=88.577778
set bd[Cd[q4]+63]=95.221112
set bd[Cd[q4]+64]=102.362695
set bd[Cd[q4]+65]=110.039897
set bd[Cd[q4]+66]=118.29289
set bd[Cd[q4]+67]=127.164856
set bd[Cd[q4]+68]=136.702221
set bd[Cd[q4]+69]=146.954887
set bd[Cd[q4]+70]=157.976504
set bd[Cd[q4]+71]=169.824741
set bd[Cd[q4]+72]=182.561597
set bd[Cd[q4]+73]=196.253717
set bd[Cd[q4]+74]=210.972746
set bd[Cd[q4]+75]=226.795701
set bd[Cd[q4]+76]=243.805379
set bd[Cd[q4]+77]=262.090782
set bd[Cd[q4]+78]=281.747591
set bd[Cd[q4]+79]=302.87866
set bd[Cd[q4]+80]=325.59456
set bd[Cd[q4]+81]=350.014152
set bd[Cd[q4]+82]=376.265213
set bd[Cd[q4]+83]=404.485104
set bd[Cd[q4]+84]=434.821487
set bd[Cd[q4]+85]=467.433099
set bd[Cd[q4]+86]=502.490581
set bd[Cd[q4]+87]=540.177375
set bd[Cd[q4]+88]=580.690678
set bd[Cd[q4]+89]=624.242479
set bd[Cd[q4]+90]=671.060665
set bd[Cd[q4]+91]=721.390214
set bd[Cd[q4]+92]=775.494481
set bd[Cd[q4]+93]=833.656567
set bd[Cd[q4]+94]=896.180809
set bd[Cd[q4]+95]=963.39437
set bd[Cd[q4]+96]=1035.648948
set bd[Cd[q4]+97]=1113.322619
set bd[Cd[q4]+98]=1196.821815
set bd[Cd[q4]+99]=1286.583451
set bd[Cd[q4]+100]=1383.07721
endfunction
function Ru takes integer nq,integer Su,integer Tu returns nothing
if nq==0 then
call Kt("Error: MBSNode struct has run out of indexes. Current amount of indexes is 8190. Error detected on field of player "+I2S(Pg[Su]),"MBSNodeError")
endif
set Hd[nq]=Su
set Id[nq]=Tu
endfunction
function Uu takes integer nq returns nothing
if ld[nq]!=0 then
set Jd[ld[nq]]=Jd[nq]
endif
if Jd[nq]!=0 then
set ld[Jd[nq]]=ld[nq]
endif
endfunction
function Vu takes integer nq,integer Pt returns integer
local integer Wu=Pq()
if Td[nq]==0 then
set Sd[nq]=Wu
else
set Jd[Wu]=Sd[nq]
set ld[Sd[nq]]=Wu
set Sd[nq]=Wu
endif
set Rd[nq]=Rd[nq]+1
set Td[nq]=Td[nq]+1
call Ru(Wu,Pt,Rd[nq])
return Wu
endfunction
function Xu takes integer nq,integer Wu returns nothing
set Td[nq]=Td[nq]-1
if Sd[nq]==Wu then
set Sd[nq]=Jd[Wu]
endif
call Uu(Wu)
call Qq(Wu)
endfunction
function Yu takes integer nq,integer Pt returns integer
local integer Wu=Rq()
if Vd[nq]==0 then
set Ud[nq]=Wu
else
set Jd[Wu]=Ud[nq]
set ld[Ud[nq]]=Wu
set Ud[nq]=Wu
endif
set Rd[nq]=Rd[nq]+1
set Vd[nq]=Vd[nq]+1
call Ru(Wu,Pt,Rd[nq])
return Wu
endfunction
function Zu takes integer nq,integer Wu returns nothing
set Vd[nq]=Vd[nq]-1
if Ud[nq]==Wu then
set Ud[nq]=Jd[Wu]
endif
set Wd[nq]=Wd[nq]-Kd[Wu]
call Uu(Wu)
call Qq(Wu)
endfunction
function dv takes integer nq,integer Wu,real ev returns nothing
set Wd[nq]=Wd[nq]-Kd[Wu]+ev
set Kd[Wu]=ev
endfunction
function fv takes integer nq,real Kr returns real
local integer Wu=Ud[nq]
local integer gv
loop
exitwhen Wu==0
set gv=Jd[Wu]
set Kr=Kr-Kd[Wu]
if Kr>.0 then
call Ur(Hd[Wu])
else
call dv(nq,Wu,-Kr)
return .0
endif
set Wu=gv
endloop
return Kr
endfunction
function hv takes integer nq,integer Pt returns integer
local integer Wu=Rq()
if Zd[nq]==0 then
set Xd[nq]=Wu
set Yd[nq]=Wu
else
set Jd[Wu]=Xd[nq]
set ld[Xd[nq]]=Wu
set Xd[nq]=Wu
endif
set Rd[nq]=Rd[nq]+1
set Zd[nq]=Zd[nq]+1
call Ru(Wu,Pt,Rd[nq])
return Wu
endfunction
function iv takes integer nq returns nothing
if Zd[nq]==0 then
set de[nq]=.0
elseif Kd[Yd[nq]]<=.0 then
set de[nq]=Kd[Xd[nq]]
elseif Kd[Xd[nq]]>=.0 then
set de[nq]=Kd[Yd[nq]]
else
set de[nq]=Kd[Xd[nq]]+Kd[Yd[nq]]
endif
endfunction
function jv takes integer nq,integer Wu returns nothing
set Zd[nq]=Zd[nq]-1
if Xd[nq]==Wu then
set Xd[nq]=Jd[Wu]
endif
if Yd[nq]==Wu then
set Yd[nq]=ld[Wu]
endif
call iv(nq)
call Uu(Wu)
call Qq(Wu)
endfunction
function kv takes integer nq,integer Wu returns nothing
local integer n=Jd[Wu]
local integer p=ld[Wu]
if n!=0 and Kd[Wu]>Kd[n]then
if p!=0 then
set Jd[p]=n
endif
set ld[n]=p
set p=n
set n=Jd[n]
set Jd[p]=Wu
set Jd[Wu]=n
set ld[Wu]=p
if p==Yd[nq]then
set Yd[nq]=Wu
endif
if Wu==Xd[nq]then
set Xd[nq]=p
endif
loop
exitwhen n==0
set ld[n]=Wu
exitwhen Kd[Wu]<=Kd[n]
set Jd[p]=n
set ld[n]=p
set p=n
set n=Jd[n]
set Jd[p]=Wu
set Jd[Wu]=n
set ld[Wu]=p
if p==Yd[nq]then
set Yd[nq]=Wu
endif
endloop
endif
if p!=0 and Kd[Wu]<Kd[p]then
if n!=0 then
set ld[n]=p
endif
set Jd[p]=n
set n=p
set p=ld[p]
set ld[n]=Wu
set ld[Wu]=p
set Jd[Wu]=n
if n==Xd[nq]then
set Xd[nq]=Wu
endif
if Wu==Yd[nq]then
set Yd[nq]=n
endif
loop
exitwhen p==0
set Jd[p]=Wu
exitwhen Kd[Wu]>=Kd[p]
set ld[n]=p
set Jd[p]=n
set n=p
set p=ld[p]
set ld[n]=Wu
set ld[Wu]=p
set Jd[Wu]=n
if n==Xd[nq]then
set Xd[nq]=Wu
endif
endloop
endif
endfunction
function mv takes integer nq,integer Wu,real ev returns nothing
set Kd[Wu]=ev
call kv(nq,Wu)
call iv(nq)
endfunction
function ov takes integer nq,integer Wu returns nothing
set fe[nq]=fe[nq]-1
if ee[nq]==Wu then
set ee[nq]=Jd[Wu]
endif
set ge[nq]=ge[nq]-Ld[Wu]
set he[nq]=ge[nq]>0
call Uu(Wu)
call Qq(Wu)
endfunction
function qv takes integer nq,integer Pt returns integer
local integer Wu=Tq()
if je[nq]==0 then
set ie[nq]=Wu
else
set Jd[Wu]=ie[nq]
set ld[ie[nq]]=Wu
set ie[nq]=Wu
endif
set Rd[nq]=Rd[nq]+1
set je[nq]=je[nq]+1
call Ru(Wu,Pt,Rd[nq])
return Wu
endfunction
function rv takes integer nq,integer Wu returns nothing
set je[nq]=je[nq]-1
if ie[nq]==Wu then
set ie[nq]=Jd[Wu]
endif
set ke[nq]=ke[nq]-Ld[Wu]
set me[nq]=ke[nq]>0
call Uu(Wu)
call Qq(Wu)
endfunction
function sv takes integer nq,integer Wu,boolean ev returns nothing
local integer v=jt(ev)
set ke[nq]=ke[nq]-Ld[Wu]+v
set me[nq]=ke[nq]>0
set Ld[Wu]=v
endfunction
function wv takes integer nq,integer Pt returns integer
local integer Wu=Tq()
if re[nq]==0 then
set qe[nq]=Wu
else
set Jd[Wu]=qe[nq]
set ld[qe[nq]]=Wu
set qe[nq]=Wu
endif
set Rd[nq]=Rd[nq]+1
set re[nq]=re[nq]+1
call Ru(Wu,Pt,Rd[nq])
return Wu
endfunction
function xv takes integer nq,integer Wu returns nothing
set re[nq]=re[nq]-1
if qe[nq]==Wu then
set qe[nq]=Jd[Wu]
endif
set se[nq]=se[nq]-Ld[Wu]
call Uu(Wu)
call Qq(Wu)
endfunction
function yv takes integer nq,integer Wu,integer ev returns nothing
set se[nq]=se[nq]-Ld[Wu]+ev
set Ld[Wu]=ev
endfunction
function zv takes integer nq,integer Pt returns integer
local integer Wu=Tq()
if ue[nq]==0 then
set te[nq]=Wu
else
set Jd[Wu]=te[nq]
set ld[te[nq]]=Wu
set te[nq]=Wu
endif
set Rd[nq]=Rd[nq]+1
set ue[nq]=ue[nq]+1
call Ru(Wu,Pt,Rd[nq])
return Wu
endfunction
function Av takes integer nq,integer Wu returns nothing
set ue[nq]=ue[nq]-1
if te[nq]==Wu then
set te[nq]=Jd[Wu]
endif
set ve[nq]=ve[nq]-Ld[Wu]
call Uu(Wu)
call Qq(Wu)
endfunction
function av takes integer nq,integer Wu,integer ev returns nothing
set ve[nq]=ve[nq]-Ld[Wu]+ev
set Ld[Wu]=ev
endfunction
function Bv takes integer nq,integer Pt returns integer
local integer Wu=Uq()
if xe[nq]==0 then
set we[nq]=Wu
else
set Jd[Wu]=we[nq]
set ld[we[nq]]=Wu
set we[nq]=Wu
endif
set Rd[nq]=Rd[nq]+1
set xe[nq]=xe[nq]+1
call Ru(Wu,Pt,Rd[nq])
return Wu
endfunction
function bv takes integer nq,integer Wu returns nothing
set xe[nq]=xe[nq]-1
if we[nq]==Wu then
set we[nq]=Jd[Wu]
endif
set ye[nq]=ye[nq]-Md[Wu]
set ze[nq]=ye[nq]>0
call Uu(Wu)
call Qq(Wu)
endfunction
function Cv takes integer nq,integer Wu,boolean ev returns nothing
local integer v=jt(ev)
set ye[nq]=ye[nq]-Md[Wu]+v
set ze[nq]=ye[nq]>0
set Md[Wu]=v
endfunction
function Fv takes integer nq,integer Pt returns integer
local integer Wu=Uq()
if ce[nq]==0 then
set Ce[nq]=Wu
else
set Jd[Wu]=Ce[nq]
set ld[Ce[nq]]=Wu
set Ce[nq]=Wu
endif
set Rd[nq]=Rd[nq]+1
set ce[nq]=ce[nq]+1
call Ru(Wu,Pt,Rd[nq])
return Wu
endfunction
function Gv takes integer nq,integer Wu returns nothing
set ce[nq]=ce[nq]-1
if Ce[nq]==Wu then
set Ce[nq]=Jd[Wu]
endif
set De[nq]=De[nq]-Md[Wu]
set Ee[nq]=De[nq]>0
call Uu(Wu)
call Qq(Wu)
endfunction
function Hv takes integer nq,integer Wu,boolean ev returns nothing
local integer v=jt(ev)
set De[nq]=De[nq]-Md[Wu]+v
set Ee[nq]=De[nq]>0
set Md[Wu]=v
endfunction
function Iv takes integer nq,integer Pt returns integer
local integer Wu=Tq()
if Ge[nq]==0 then
set Fe[nq]=Wu
else
set Jd[Wu]=Fe[nq]
set ld[Fe[nq]]=Wu
set Fe[nq]=Wu
endif
set Rd[nq]=Rd[nq]+1
set Ge[nq]=Ge[nq]+1
call Ru(Wu,Pt,Rd[nq])
return Wu
endfunction
function lv takes integer nq,integer Wu returns nothing
set Ge[nq]=Ge[nq]-1
if Fe[nq]==Wu then
set Fe[nq]=Jd[Wu]
endif
set He[nq]=He[nq]-Ld[Wu]
set Ie[nq]=He[nq]>0
call Uu(Wu)
call Qq(Wu)
endfunction
function Jv takes integer nq,integer Wu,boolean ev returns nothing
local integer v=jt(ev)
set He[nq]=He[nq]-Ld[Wu]+v
set Ie[nq]=He[nq]>0
set Ld[Wu]=v
endfunction
function Kv takes integer nq,real Kr returns nothing
if xe[nq]>0 and(ce[nq]==0 or Id[we[nq]]>Id[Ce[nq]])then
set Nd[we[nq]]=Nd[we[nq]]+Kr
else
set Nd[Ce[nq]]=Nd[Ce[nq]]+Kr
endif
endfunction
function Lv takes integer nq,real Kr returns nothing
if ae[nq]>0 and(ce[nq]==0 or Id[Ae[nq]]>Id[Ce[nq]])then
set Nd[Ae[nq]]=Nd[Ae[nq]]+Kr
else
set Nd[Ce[nq]]=Nd[Ce[nq]]+Kr
endif
endfunction
function Mv takes integer nq,real Kr returns nothing
set Nd[Ce[nq]]=Nd[Ce[nq]]+Kr
endfunction
function Nv takes integer nq returns nothing
local integer Wu=Xd[nq]
local integer gv
loop
exitwhen Wu==0
set gv=Jd[Wu]
if zh[Qg[Hd[Wu]]]then
call Ur(Hd[Wu])
endif
set Wu=gv
endloop
set Wu=ee[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
endfunction
function Ov takes integer nq returns nothing
local integer Wu=Sd[nq]
local integer gv
loop
exitwhen Wu==0
set gv=Jd[Wu]
if zh[Qg[Hd[Wu]]]then
call Ur(Hd[Wu])
endif
set Wu=gv
endloop
set Wu=Xd[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
if zh[Qg[Hd[Wu]]]then
call Ur(Hd[Wu])
endif
set Wu=gv
endloop
set Wu=ee[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=ne[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
if zh[Qg[Hd[Wu]]]then
call Ur(Hd[Wu])
endif
set Wu=gv
endloop
set Wu=qe[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
if zh[Qg[Hd[Wu]]]then
call Ur(Hd[Wu])
endif
set Wu=gv
endloop
set Wu=te[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
if zh[Qg[Hd[Wu]]]then
call Ur(Hd[Wu])
endif
set Wu=gv
endloop
endfunction
function Pv takes integer nq returns nothing
local integer Wu=Sd[nq]
local integer gv
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=Ud[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=Xd[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=ee[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=ie[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=ne[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=qe[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=te[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=we[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=Ae[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=Ce[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=Fe[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
call Zs("MinionBuffStorage "+I2S(nq)+" is deleted.")
endfunction
function Qv takes integer nq returns nothing
if nq==null then
return
elseif(Qd[nq]!=-1)then
return
endif
call Pv(nq)
set Qd[nq]=Od
set Od=nq
endfunction
function Rv takes integer T returns integer
local integer Sv=br()
set zh[Sv]=zh[J4[T]]
set Ah[Sv]=Ah[J4[T]]
set ah[Sv]=ah[J4[T]]
set Bh[Sv]=Bh[J4[T]]
set bh[Sv]=bh[J4[T]]
set Ch[Sv]=Ch[J4[T]]
set ch[Sv]=ch[J4[T]]
return Sv
endfunction
function Tv takes integer T returns integer
local integer Sv=cq()
set cj[Sv]=cj[S4[T]]
set Dj[Sv]=Dj[S4[T]]
set Ej[Sv]=Ej[S4[T]]
set Fj[Sv]=Fj[S4[T]]
set Gj[Sv]=Gj[S4[T]]
set Hj[Sv]=Hj[S4[T]]
set Ij[Sv]=Ij[S4[T]]
return Sv
endfunction
function Uv takes integer T returns integer
local integer Sv=qq()
set Wm[Sv]=Wm[W4[T]]
set Xm[Sv]=Xm[W4[T]]
set Ym[Sv]=Ym[W4[T]]
set Zm[Sv]=Zm[W4[T]]
set en[Sv]=en[W4[T]]
set fn[Sv]=fn[W4[T]]
set gn[Sv]=gn[W4[T]]
set hn[Sv]=hn[W4[T]]
set in[Sv]=in[W4[T]]
set jn[Sv]=jn[W4[T]]
set dn[Sv]=dn[W4[T]]
set kn[Sv]=in[Sv]
set mn[Sv]=jn[Sv]
set nn[Sv]=nn[W4[T]]
set on[Sv]=on[W4[T]]
set pn[Sv]=pn[W4[T]]
set qn[Sv]=qn[W4[T]]
set rn[Sv]=rn[W4[T]]
set sn[Sv]=sn[W4[T]]
set tn[Sv]=tn[W4[T]]
set un[Sv]=un[W4[T]]
set vn[Sv]=vn[W4[T]]
set wn[Sv]=wn[W4[T]]
set xn[Sv]=xn[W4[T]]
return Sv
endfunction
function Vv takes integer p returns nothing
local integer a=0
local integer b
local integer Wv
local integer f1=Of[p]
local integer f2=Pf[p]
call UnitAddAbility(sf[p],z9[f1])
call UnitAddAbility(sf[p],A9[f2])
call UnitAddAbility(tf[p],y9[f1])
call UnitAddAbility(vf[p],Sn[x9[f1]])
call UnitAddAbility(vf[p],Tn[x9[f2]])
loop
set Wv=v9[w9[f1]+a]
call UnitAddAbility(tf[p],Xm[W4[Wv]])
set b=0
loop
call UnitAddAbility(uf[p],bn[Cn[Y4[Wv]]+b])
call UnitAddAbility(uf[p],cn[Dn[Y4[Wv]]+b])
set b=b+1
exitwhen b==3
endloop
set a=a+1
exitwhen a==4
endloop
set Qf[p]=f1
set Sf[p]=f1
set Rf[p]=f2
set Tf[p]=f2
endfunction
function Xv takes integer nq returns nothing
set dg[nq]=CreateTimer()
call SaveInteger(f7,3,GetHandleId((dg[nq])),(nq))
endfunction
function Yv takes integer nq returns nothing
call Bt(dg[nq])
set dg[nq]=null
endfunction
function Zv takes player p,integer i returns integer
local integer nq=pr()
local integer a
set Le[nq]=p
set Re[nq]=p==P
set Oe[nq]=GetPlayerName(p)
call Xv(nq)
set Me[nq]=Ot(j7,nq)
call Qt(k7,nq)
set i7[i]=nq
set Ue[nq]=Fu(nq)
set xf[nq]=tq()
set yf[nq]=uq()
set zf[nq]=vq()
set Af[nq]=wq()
set af[nq]=yq()
set Bf[nq]=xq()
set a=0
loop
call SetPlayerTechMaxAllowed(p,1949315120+a,po[(a)])
set a=a+1
exitwhen a==Z4
endloop
set a=1
loop
set If[lf[nq]+a]=Rv(a)
set a=a+1
exitwhen a==32
endloop
set a=1
loop
set Gf[Hf[nq]+a]=Tv(a)
set a=a+1
exitwhen a==32
endloop
set a=1
loop
set cf[Df[nq]+a]=Uv(a)
call SetPlayerAbilityAvailable(p,cn[Dn[Y4[a]]],false)
call SetPlayerAbilityAvailable(p,cn[Dn[Y4[a]]+1],false)
call SetPlayerAbilityAvailable(p,cn[Dn[Y4[a]]+2],false)
set a=a+1
exitwhen a==32
endloop
set a=1
loop
set Kf[Lf[nq]+a]=zq()
call SetPlayerAbilityAvailable(p,Gm[V4[a]],false)
call SetPlayerAbilityAvailable(p,Fm[V4[a]],false)
call SetPlayerAbilityAvailable(p,Em[V4[a]],false)
call SetPlayerAbilityAvailable(p,Lm[Mm[V4[a]]],false)
call SetPlayerAbilityAvailable(p,Lm[Mm[V4[a]]+1],false)
call SetPlayerAbilityAvailable(p,Jm[Km[V4[a]]],false)
call SetPlayerAbilityAvailable(p,Jm[Km[V4[a]]+1],false)
set a=a+1
exitwhen a==24
endloop
call TriggerRegisterPlayerUnitEvent(a7,p,EVENT_PLAYER_UNIT_CONSTRUCT_START,null)
call TriggerRegisterPlayerUnitEvent(B7,p,EVENT_PLAYER_UNIT_CONSTRUCT_CANCEL,null)
call TriggerRegisterPlayerUnitEvent(b7,p,EVENT_PLAYER_UNIT_CONSTRUCT_FINISH,null)
call TriggerRegisterPlayerUnitEvent(G7,p,EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerRegisterPlayerUnitEvent(H7,p,EVENT_PLAYER_UNIT_SPELL_ENDCAST,null)
call TriggerRegisterPlayerUnitEvent(I7,p,EVENT_PLAYER_UNIT_ISSUED_ORDER,null)
call TriggerRegisterPlayerUnitEvent(l7,p,EVENT_PLAYER_UNIT_ISSUED_POINT_ORDER,null)
call TriggerRegisterPlayerUnitEvent(C7,p,EVENT_PLAYER_UNIT_UPGRADE_START,null)
call TriggerRegisterPlayerUnitEvent(c7,p,EVENT_PLAYER_UNIT_UPGRADE_CANCEL,null)
call TriggerRegisterPlayerUnitEvent(D7,p,EVENT_PLAYER_UNIT_UPGRADE_FINISH,null)
call TriggerRegisterPlayerUnitEvent(J7,p,EVENT_PLAYER_UNIT_SELECTED,null)
call TriggerRegisterPlayerUnitEvent(K7,p,EVENT_PLAYER_UNIT_DESELECTED,null)
call TriggerRegisterPlayerEvent(w7,p,EVENT_PLAYER_LEAVE)
return nq
endfunction
function dw takes integer nq returns nothing
local integer k=0
local real x=gd[Ue[nq]]
local real y=320.
set Se[nq]=S[Me[nq]*2/(B8[k7]+1)]
call Iu(Ue[nq])
loop
set pf[qf[nq]+k]=wt(Le[nq],"Textures\\Pavement.blp",256.,x,y,4)
set x=x+128.
if x>Z9[Ue[nq]]then
set y=y-128.
set x=gd[Ue[nq]]
endif
set k=k+1
exitwhen k==120
endloop
set x=(ed[Ue[nq]]+Z9[Ue[nq]])/ 2.
set y=(h4+1152.)/ 2.
set sf[nq]=CreateUnit(Le[nq],1112294482,x,y,.0)
set k=Me[nq]
call SetHeroXP(sf[nq],k,false)
if Re[nq]then
call PanCameraToTimed(x,y,.0)
call SelectUnit(sf[nq],true)
endif
set y=1920.-64.
set tf[nq]=CreateUnit(Le[nq],1112689491,x-128.,y,270.)
call SetUnitVertexColor(tf[nq],$FF,$FF,$FF,64)
call SetHeroXP(tf[nq],k,false)
set uf[nq]=CreateUnit(Le[nq],1112232788,x-384.,y,270.)
call SetUnitVertexColor(uf[nq],$FF,$FF,$FF,64)
call SetHeroXP(uf[nq],k,false)
set vf[nq]=CreateUnit(Le[nq],1095521362,x+128.,y,270.)
call SetUnitVertexColor(vf[nq],$FF,$FF,$FF,64)
call SetHeroXP(vf[nq],k,false)
set wf[nq]=CreateUnit(Le[nq],1296256336,x+384.,y,270.)
call SetUnitVertexColor(wf[nq],$FF,$FF,$FF,64)
call SetHeroXP(wf[nq],k,false)
set Vf[nq]=UnitAddItemById(uf[nq],1227894850)
set Xf[nq]=UnitAddItemById(uf[nq],1227894854)
set Wf[nq]=UnitAddItemById(uf[nq],1227894851)
set Yf[nq]=UnitAddItemById(uf[nq],1227894853)
set Uf[nq]=UnitAddItemById(uf[nq],1227894852)
call SetItemCharges(Wf[nq],500)
call Vv(nq)
set Te[nq]=true
call Zs("Player "+I2S(nq)+" is ready.")
endfunction
function ew takes integer nq,integer T returns nothing
set Ef[Ff[nq]+T]=Ef[Ff[nq]+T]+1
if Ef[Ff[nq]+T]==hn[cf[Df[nq]+T]]then
call SetPlayerAbilityAvailable(Le[nq],Xm[W4[T]],false)
endif
endfunction
function fw takes integer nq,integer T returns nothing
set Ef[Ff[nq]+T]=Ef[Ff[nq]+T]-1
call SetPlayerAbilityAvailable(Le[nq],Xm[W4[T]],true)
endfunction
function gw takes integer nq returns nothing
local integer a=0
loop
set Ef[Ff[nq]+a]=0
call SetPlayerAbilityAvailable(Le[nq],Xm[W4[a]],true)
set a=a+1
exitwhen a==32
endloop
endfunction
function hw takes integer nq,integer T,real Kr returns real
set in[cf[Df[nq]+T]]=in[cf[Df[nq]+T]]+Kr
set Kr=(bd[Cd[(dn[cf[Df[nq]+T]])]+(bf[nq])])*Kr
set kn[cf[Df[nq]+T]]=kn[cf[Df[nq]+T]]+Kr
return Kr
endfunction
function iw takes integer nq,integer T,real Kr returns real
set jn[cf[Df[nq]+T]]=jn[cf[Df[nq]+T]]+Kr
set Kr=(bd[Cd[(dn[cf[Df[nq]+T]])]+(bf[nq])])*Kr
set mn[cf[Df[nq]+T]]=mn[cf[Df[nq]+T]]+Kr
return Kr
endfunction
function jw takes integer nq,integer Qu returns nothing
set gn[cf[Df[nq]+On[Jf[nq]]]]=cm[Qu]
if Nf[nq]!=0 then
if rg[Nf[nq]]!=0 then
call Ar(rg[Nf[nq]])
endif
set Th=nq
call TriggerEvaluate(T4[cm[Qu]])
endif
set Mf[nq]=Qu
endfunction
function kw takes nothing returns nothing
set Zf[((LoadInteger(f7,3,GetHandleId(GetExpiredTimer()))))]=0
endfunction
function mw takes integer nq,sound nw,string s returns nothing
if Re[nq]then
if Zf[nq]==5 then
set Zf[nq]=0
call ClearTextMessages()
endif
call StartSound(nw)
call DisplayTimedTextToPlayer(Le[nq],.0,.0,5.,s)
set Zf[nq]=Zf[nq]+1
endif
call TimerStart(dg[nq],5.,false,function kw)
endfunction
function ow takes integer nq returns nothing
local integer a=0
set rf[nq]=not rf[nq]
loop
call ShowImage(pf[qf[nq]+a],rf[nq])
set a=a+1
exitwhen a==120
endloop
endfunction
function pw takes integer nq returns nothing
set ef[nq]=ef[nq]+1
call MultiboardSetItemValue(of[nq],I2S(ef[nq]))
endfunction
function qw takes integer nq,integer rw returns nothing
set Ze[nq]=Ze[nq]+rw
call MultiboardSetItemValue(mf[nq],I2S(Ze[nq]))
endfunction
function sw takes integer nq,integer ev returns nothing
set Ve[nq]=Ve[nq]+ev
if Ve[nq]>99999 then
set Ve[nq]=99999
endif
call MultiboardSetItemValue(hf[nq],I2S(Ve[nq]))
endfunction
function tw takes integer nq,integer Kr returns boolean
local boolean uw=(Ve[(nq)]-(Kr)>=0)
if uw then
call sw(nq,-Kr)
endif
return uw
endfunction
function vw takes integer nq,integer Kr returns boolean
local boolean uw=(Ve[(nq)]-(Kr)>=0)
if uw then
call sw(nq,-Kr)
else
call mw(nq,N7,"|cffcc3333Error|r: not enough gold.")
endif
return uw
endfunction
function ww takes integer nq,string s,unit u returns nothing
call nt(("+"+s),(u),(Le[nq]),9.,48.,3.,2.)
call SetTextTagColor(bj_lastCreatedTextTag,r4,s4,51,$FF)
endfunction
function xw takes integer nq,integer ev returns nothing
set We[nq]=We[nq]+ev
if We[nq]>99999 then
set We[nq]=99999
endif
call MultiboardSetItemValue(jf[nq],I2S(We[nq]))
endfunction
function yw takes integer nq,integer Kr returns boolean
local boolean uw=(We[(nq)]-(Kr)>=0)
if uw then
call xw(nq,-Kr)
endif
return uw
endfunction
function zw takes integer nq,integer Kr returns boolean
local boolean uw=(We[(nq)]-(Kr)>=0)
if uw then
call xw(nq,-Kr)
else
call mw(nq,M7,"|cffcc3333Error|r: not enough experience.")
endif
return uw
endfunction
function Aw takes integer nq,string s,unit u returns nothing
call nt(("+"+s),(u),(Le[nq]),9.,48.,3.,2.)
call SetTextTagColor(bj_lastCreatedTextTag,t4,u4,v4,$FF)
endfunction
function aw takes integer nq,real ev returns nothing
set Ye[nq]=Ye[nq]-ev
call MultiboardSetItemValue(kf[nq],I2S(ht(Ye[nq]))+"/"+I2S(Xe[nq]))
endfunction
function Bw takes integer nq,real ev returns boolean
return Ye[nq]+ev<=Xe[nq]
endfunction
function bw takes integer nq,real ev returns boolean
local boolean uw=Bw(nq,ev)
if uw then
call aw((nq),-((ev)*1.))
else
call mw(nq,M7,"|cffcc3333Error|r: unable to create a minion due to the maximum supply limit.")
endif
return uw
endfunction
function Cw takes integer nq,integer l returns nothing
if l==0 then
return
elseif l<-1 then
call mw(nq,R7,"|cffcc3333Warning|r: you lost |cff00ccff"+I2S(-l)+"|r lives.")
elseif l==-1 then
call mw(nq,R7,"|cffcc3333Warning|r: you lost |cff00ccff1|r life.")
elseif l==1 then
call mw(nq,S7,"|cff33cc33Note|r: you received |cff00ccff1|r life.")
elseif l>1 then
call mw(nq,S7,"|cff33cc33Note|r: you received |cff00ccff"+I2S(l)+"|r lives.")
endif
set df[nq]=df[nq]+l
if df[nq]<=0 then
set df[nq]=0
endif
call MultiboardSetItemValue(nf[nq],I2S(df[nq]))
endfunction
function cw takes integer nq returns nothing
call Yv(nq)
call lu(Ue[nq])
if Te[nq]then
call nr(nq)
call RemoveUnit(sf[nq])
call RemoveUnit(tf[nq])
call RemoveUnit(vf[nq])
call RemoveUnit(uf[nq])
call RemoveUnit(wf[nq])
set sf[nq]=null
set tf[nq]=null
set uf[nq]=null
set vf[nq]=null
set wf[nq]=null
set Uf[nq]=null
set Vf[nq]=null
set Wf[nq]=null
set Xf[nq]=null
set Yf[nq]=null
endif
set Qe[nq]=false
endfunction
function Dw takes integer nq returns nothing
set lg[nq]=CreateTimer()
call SaveInteger(f7,3,GetHandleId((lg[nq])),(nq))
endfunction
function Ew takes integer nq returns nothing
call Bt(lg[nq])
set lg[nq]=null
endfunction
function Fw takes integer p,integer id returns boolean
local string Gw=GetObjectName(id)
local integer T
if SubString(Gw,0,y4)=="Spawn" then
set T=S2I(SubString(Gw,z4,A4))
if bw(p,sn[cf[Df[p]+T]])and vw(p,nn[cf[Df[p]+T]])then
set hg=p
call TriggerEvaluate(X4[T])
return true
endif
endif
return false
endfunction
function Hw takes integer p,integer id,boolean Iw returns boolean
local string Gw=GetObjectName(id)
local integer T
local integer lw
if SubString(Gw,0,a4)=="Upgrade" then
set T=S2I(SubString(Gw,B4,b4))
set lw=S2I(SubString(Gw,C4,c4))
set Gw=SubString(Gw,D4,StringLength(Gw))
if(Iw and vw(p,In[ln[Y4[T]]+lw]))or((not Iw)and zw(p,In[ln[Y4[T]]+lw]))then
set ig=p
set yn[zn[cf[Df[p]+T]]+lw]=true
call TriggerExecute(Jn[Kn[Y4[T]]+lw])
call SetPlayerAbilityAvailable(Le[p],id,false)
call SetPlayerAbilityAvailable(Le[p],cn[Dn[Y4[T]]+lw],true)
call SetPlayerTechResearched(Le[p],Gn[Hn[Y4[T]]+lw],1)
call mw(p,O7,"|cff33cc33Research complete|r:"+Gw+".")
return true
endif
endif
return false
endfunction
function Jw takes integer p returns nothing
set ig=p
call TriggerExecute(t7)
endfunction
function Kw takes integer nq,integer T,integer p returns nothing
set kg[nq]=p
set og[nq]=CreateUnit(Le[p],Wm[W4[T]],xt(hd[Ue[p]]),yt(hd[Ue[p]]),270.)
call UnitAddAbility(og[nq],1098282348)
call UnitMakeAbilityPermanent(og[nq],true,1098282348)
call SetUnitUserData(og[nq],nq)
set jg[nq]=T
set pg[nq]=GetHandleId(og[nq])
set xg[nq]=cf[Df[p]+T]
call UnitAddAbility(og[nq],1093677099)
call UnitMakeAbilityPermanent(og[nq],true,1093677099)
call UnitAddAbility(og[nq],Ym[W4[T]])
call UnitMakeAbilityPermanent(og[nq],true,Ym[W4[T]])
call UnitAddAbility(og[nq],En[Fn[Y4[T]]])
call UnitMakeAbilityPermanent(og[nq],true,En[Fn[Y4[T]]])
call UnitAddAbility(og[nq],En[Fn[Y4[T]]+1])
call UnitMakeAbilityPermanent(og[nq],true,En[Fn[Y4[T]]+1])
call UnitAddAbility(og[nq],En[Fn[Y4[T]]+2])
call UnitMakeAbilityPermanent(og[nq],true,En[Fn[Y4[T]]+2])
set Th=nq
call TriggerEvaluate(T4[gn[xg[nq]]])
call UnitAddAbility(og[nq],1093677122)
call UnitMakeAbilityPermanent(og[nq],true,1093677122)
call UnitAddAbility(og[nq],cj[S4[gn[xg[nq]]]])
call UnitMakeAbilityPermanent(og[nq],true,cj[S4[gn[xg[nq]]]])
call Ut(yf[p],nq)
call qw(p,on[xg[nq]])
call ew(p,T)
endfunction
function Lw takes integer nq,integer T,integer p returns nothing
local integer a
set kg[nq]=p
set og[nq]=CreateUnit(Le[p],Wm[W4[T]],xt(hd[Ue[p]]),yt(hd[Ue[p]]),270.)
call UnitAddAbility(og[nq],1098282348)
call UnitMakeAbilityPermanent(og[nq],true,1098282348)
call SetUnitUserData(og[nq],nq)
set jg[nq]=T
set pg[nq]=GetHandleId(og[nq])
set xg[nq]=cf[Df[p]+T]
call UnitAddAbility(og[nq],1093677099)
call UnitMakeAbilityPermanent(og[nq],true,1093677099)
call UnitAddAbility(og[nq],Ym[W4[T]])
call UnitMakeAbilityPermanent(og[nq],true,Ym[W4[T]])
call UnitAddAbility(og[nq],En[Fn[Y4[T]]])
call UnitMakeAbilityPermanent(og[nq],true,En[Fn[Y4[T]]])
call UnitAddAbility(og[nq],En[Fn[Y4[T]]+1])
call UnitMakeAbilityPermanent(og[nq],true,En[Fn[Y4[T]]+1])
call UnitAddAbility(og[nq],En[Fn[Y4[T]]+2])
call UnitMakeAbilityPermanent(og[nq],true,En[Fn[Y4[T]]+2])
set Th=nq
call TriggerEvaluate(T4[gn[xg[nq]]])
call Ut(yf[p],nq)
set T=0
loop
set a=Pn[Qn[Jf[p]]+T]
call UnitAddAbility(og[nq],Gm[a])
call UnitMakeAbilityPermanent(og[nq],true,Gm[a])
call UnitAddAbility(og[nq],Lm[Mm[a]])
call UnitMakeAbilityPermanent(og[nq],true,Lm[Mm[a]])
call UnitAddAbility(og[nq],Lm[Mm[a]+1])
call UnitMakeAbilityPermanent(og[nq],true,Lm[Mm[a]+1])
set T=T+1
exitwhen T==3
endloop
endfunction
function Mw takes integer nq returns nothing
call Vt(yf[kg[nq]],nq)
call fw(kg[nq],jg[nq])
call aw(kg[nq],rn[xg[nq]])
call qw(kg[nq],-on[xg[nq]])
call sw(kg[nq],nn[xg[nq]])
call ww(kg[nq],I2S(nn[xg[nq]]),og[nq])
call DestroyEffect(AddSpecialEffect(("Abilities\\Spells\\Other\\Transmute\\PileofGold.mdx"),((GetUnitX(og[nq]))*1.),((GetUnitY(og[nq]))*1.)))
call RemoveUnit(og[nq])
call Rr(nq)
call Zs("Minion "+I2S(nq)+" is sold.")
endfunction
function Nw takes integer nq returns nothing
call UnitRemoveAbility(og[nq],1093677099)
set mg[nq]=Pe[kg[nq]]
set ng[nq]=Ue[mg[nq]]
call Wt(zf[mg[nq]],nq)
call SetUnitMoveSpeed(og[nq],256.)
call SetUnitOwner(og[nq],Se[mg[nq]],false)
call SetUnitX(og[nq],GetUnitX(og[nq])-GetRectCenterX(hd[Ue[kg[nq]]])+GetRectCenterX(jd[ng[nq]]))
call SetUnitY(og[nq],GetUnitY(og[nq])-GetRectCenterY(hd[Ue[kg[nq]]])+GetRectCenterY(jd[ng[nq]]))
set yg[nq]=xt(md[ng[nq]])
set zg[nq]=GetRectMinY(md[ng[nq]])
call IssuePointOrderById(og[nq],G,yg[nq],zg[nq])
call Zs("Minion "+I2S(nq)+" is sent.")
endfunction
function Ow takes integer nq returns nothing
call Xt(zf[mg[nq]],nq)
call aw(kg[nq],rn[xg[nq]])
call RemoveUnit(og[nq])
call Rr(nq)
call Zs("Spawner "+I2S(nq)+" is deleted.")
endfunction
function Pw takes integer nq,boolean Qw returns nothing
local string Rw
if Qw then
set Rw="|cffcc3333+1"
else
set Rw=""
endif
if pn[xg[nq]]>0 then
call sw(mg[nq],pn[xg[nq]])
set Rw=Rw+"|r
|cffcccc33+"+I2S(pn[xg[nq]])
endif
if qn[xg[nq]]>0 then
call xw(mg[nq],qn[xg[nq]])
set Rw=Rw+"|r
|cff80ccff+"+I2S(qn[xg[nq]])
endif
call nt((Rw),(og[nq]),(Le[mg[nq]]),9.,48.,3.,2.)
endfunction
function Sw takes integer nq returns boolean
return not(sg[nq]or tg[nq])
endfunction
function Tw takes integer nq returns nothing
local real gv=(Wd[qg[(nq)]])
if gv>.0 then
call UnitAddAbility(og[nq],1093677105)
else
call UnitRemoveAbility(og[nq],1093677105)
endif
call SetItemCharges(Bg[nq],ht(gv))
endfunction
function Uw takes integer nq returns real
return ug[nq]/ vg[nq]
endfunction
function Vw takes integer nq,real ev returns nothing
if ev<.0 then
set ev=.0
elseif ev>vg[nq]then
set ev=vg[nq]
endif
set ug[nq]=ev
call SetItemCharges(ag[nq],ht(ug[nq]))
call SetWidgetLife(og[nq],3.+ug[nq]/ vg[nq]*1000.)
endfunction
function Ww takes integer nq,real ev returns nothing
call Vw(nq,ug[nq]+ev)
endfunction
function Xw takes integer nq,real Yw,real Zw returns nothing
set vg[nq]=vg[nq]+Yw
set wg[nq]=wg[nq]+Zw
if sg[nq]then
call Ww(nq,Yw)
endif
endfunction
function ex takes integer nq,real ev,string fx,string At returns nothing
if ug[nq]<vg[nq]then
call Ww(nq,ev)
call DestroyEffect(AddSpecialEffectTarget((fx),(og[nq]),(At)))
endif
endfunction
function gx takes integer nq,real hx,string fx,string At returns nothing
call ex(nq,vg[nq]*hx,fx,At)
endfunction
function jx takes integer nq returns integer
local integer kx
set G4=Dr(nq)
set kx=G4+pe[qg[nq]]+ve[qg[nq]]
if kx>75 then
return 75
elseif kx<E4 then
return E4
endif
return kx
endfunction
function mx takes integer nq returns integer
local integer kx
set H4=Er(nq)
set kx=H4+se[qg[nq]]+ve[qg[nq]]
if kx>75 then
return 75
elseif kx<F4 then
return F4
endif
return kx
endfunction
function nx takes integer nq returns nothing
local real ox=cr(nq)
local real px=de[qg[nq]]
local real qx
call SetUnitMoveSpeed(og[nq],256.*(ox+px))
set qx=GetUnitMoveSpeed(og[nq])
set px=qx/ 256.-ox
if px>.0 then
call UnitRemoveAbility(og[nq],1093677134)
call UnitAddAbility(og[nq],1093677128)
elseif px<.0 then
call UnitRemoveAbility(og[nq],1093677128)
call UnitAddAbility(og[nq],1093677134)
else
call UnitRemoveAbility(og[nq],1093677134)
call UnitRemoveAbility(og[nq],1093677128)
endif
call SetItemCharges(cg[nq],gt(qx/ 256.*100.))
endfunction
function rx takes integer nq returns nothing
if(he[qg[(nq)]])then
call IssueImmediateOrderById(og[nq],J)
call UnitAddAbility(og[nq],1093677142)
else
call IssuePointOrderById(og[nq],G,yg[nq],zg[nq])
call UnitRemoveAbility(og[nq],1093677142)
endif
endfunction
function sx takes integer nq returns nothing
local integer kx=jx(nq)
local integer tx=G4
if kx>0 then
call SetItemCharges(bg[nq],kx)
call UnitRemoveAbility(og[nq],1093677115)
call UnitRemoveAbility(og[nq],1110454321)
call UnitAddAbility(og[nq],1093677114)
elseif kx<0 then
call SetItemCharges(bg[nq],-kx)
call UnitRemoveAbility(og[nq],1093677114)
call UnitRemoveAbility(og[nq],1110454320)
call UnitAddAbility(og[nq],1093677115)
else
call SetItemCharges(bg[nq],0)
call UnitRemoveAbility(og[nq],1093677114)
call UnitRemoveAbility(og[nq],1110454320)
call UnitRemoveAbility(og[nq],1093677115)
call UnitRemoveAbility(og[nq],1110454321)
endif
if kx>tx then
call UnitRemoveAbility(og[nq],1093677089)
call UnitAddAbility(og[nq],1093677147)
elseif kx<tx then
call UnitRemoveAbility(og[nq],1093677147)
call UnitAddAbility(og[nq],1093677089)
else
call UnitRemoveAbility(og[nq],1093677089)
call UnitRemoveAbility(og[nq],1093677147)
endif
endfunction
function ux takes integer nq returns nothing
local integer kx=mx(nq)
local integer tx=H4
if kx>0 then
call SetItemCharges(Cg[nq],kx)
call UnitRemoveAbility(og[nq],1093677117)
call UnitRemoveAbility(og[nq],1110454323)
call UnitAddAbility(og[nq],1093677116)
elseif kx<0 then
call SetItemCharges(Cg[nq],-kx)
call UnitRemoveAbility(og[nq],1093677116)
call UnitRemoveAbility(og[nq],1110454322)
call UnitAddAbility(og[nq],1093677117)
else
call SetItemCharges(Cg[nq],0)
call UnitRemoveAbility(og[nq],1093677116)
call UnitRemoveAbility(og[nq],1110454322)
call UnitRemoveAbility(og[nq],1093677117)
call UnitRemoveAbility(og[nq],1110454323)
endif
if kx>tx then
call UnitRemoveAbility(og[nq],1093677150)
call UnitAddAbility(og[nq],1093677149)
elseif kx<tx then
call UnitRemoveAbility(og[nq],1093677149)
call UnitAddAbility(og[nq],1093677150)
else
call UnitRemoveAbility(og[nq],1093677150)
call UnitRemoveAbility(og[nq],1093677149)
endif
endfunction
function vx takes integer nq returns nothing
local integer kx=jx(nq)
local integer tx=G4
if kx>0 then
call SetItemCharges(bg[nq],kx)
call UnitRemoveAbility(og[nq],1093677115)
call UnitRemoveAbility(og[nq],1110454321)
call UnitAddAbility(og[nq],1093677114)
elseif kx<0 then
call SetItemCharges(bg[nq],-kx)
call UnitRemoveAbility(og[nq],1093677114)
call UnitRemoveAbility(og[nq],1110454320)
call UnitAddAbility(og[nq],1093677115)
else
call SetItemCharges(bg[nq],0)
call UnitRemoveAbility(og[nq],1093677114)
call UnitRemoveAbility(og[nq],1110454320)
call UnitRemoveAbility(og[nq],1093677115)
call UnitRemoveAbility(og[nq],1110454321)
endif
if kx>tx then
call UnitRemoveAbility(og[nq],1093677089)
call UnitAddAbility(og[nq],1093677147)
elseif kx<tx then
call UnitRemoveAbility(og[nq],1093677147)
call UnitAddAbility(og[nq],1093677089)
else
call UnitRemoveAbility(og[nq],1093677089)
call UnitRemoveAbility(og[nq],1093677147)
endif
set kx=mx(nq)
set tx=H4
if kx>0 then
call SetItemCharges(Cg[nq],kx)
call UnitRemoveAbility(og[nq],1093677117)
call UnitRemoveAbility(og[nq],1110454323)
call UnitAddAbility(og[nq],1093677116)
elseif kx<0 then
call SetItemCharges(Cg[nq],-kx)
call UnitRemoveAbility(og[nq],1093677116)
call UnitRemoveAbility(og[nq],1110454322)
call UnitAddAbility(og[nq],1093677117)
else
call SetItemCharges(Cg[nq],0)
call UnitRemoveAbility(og[nq],1093677116)
call UnitRemoveAbility(og[nq],1110454322)
call UnitRemoveAbility(og[nq],1093677117)
call UnitRemoveAbility(og[nq],1110454323)
endif
if kx>tx then
call UnitRemoveAbility(og[nq],1093677150)
call UnitAddAbility(og[nq],1093677149)
elseif kx<tx then
call UnitRemoveAbility(og[nq],1093677149)
call UnitAddAbility(og[nq],1093677150)
else
call UnitRemoveAbility(og[nq],1093677150)
call UnitRemoveAbility(og[nq],1093677149)
endif
endfunction
function wx takes integer nq,integer Wu,real ev returns nothing
call dv(qg[nq],Wu,ev)
call Tw(nq)
endfunction
function xx takes integer nq,integer Wu,real ev returns nothing
call mv(qg[nq],Wu,ev)
call nx(nq)
endfunction
function zx takes integer nq,integer Wu,boolean ev returns nothing
local boolean Ax=not(me[qg[(nq)]])
call sv(qg[nq],Wu,ev)
if ev and Ax then
call Nv(qg[nq])
call nx(nq)
call rx(nq)
call Zs("Minion is Unstoppable!")
endif
endfunction
function Bx takes integer nq,integer Wu,integer ev returns nothing
call yv(qg[nq],Wu,ev)
call ux(nq)
endfunction
function bx takes integer nq,integer Wu,integer ev returns nothing
call av(qg[nq],Wu,ev)
call vx(nq)
endfunction
function Cx takes integer nq,integer Wu,boolean ev returns nothing
local boolean Ax=not(Ie[qg[(nq)]])
call Jv(qg[nq],Wu,ev)
if ev then
if Ax then
call Ov(qg[nq])
call nx(nq)
call rx(nq)
call vx(nq)
call UnitAddType(og[nq],Z)
call Zs("Minion is Invulnerable!")
endif
elseif not(Ie[qg[(nq)]])then
call UnitRemoveType(og[(nq)],Z)
endif
endfunction
function Dx takes integer nq,integer Su,real ev returns integer
local integer Wu=Yu(qg[nq],Su)
call wx(nq,Wu,ev)
return Wu
endfunction
function Ex takes integer nq,integer Su,real ev returns integer
local integer Wu=hv(qg[nq],Su)
call xx(nq,Wu,ev)
return Wu
endfunction
function Gx takes integer nq,integer Su,boolean ev returns integer
local integer Wu=qv(qg[nq],Su)
call zx(nq,Wu,ev)
return Wu
endfunction
function Hx takes integer nq,integer Su,integer ev returns integer
local integer Wu=wv(qg[nq],Su)
call Bx(nq,Wu,ev)
return Wu
endfunction
function Ix takes integer nq,integer Su,integer ev returns integer
local integer Wu=zv(qg[nq],Su)
call bx(nq,Wu,ev)
return Wu
endfunction
function lx takes integer nq,integer Su,boolean ev returns integer
local integer Wu=Bv(qg[nq],Su)
call Cv(qg[(nq)],(Wu),(ev))
return Wu
endfunction
function Jx takes integer nq,integer Su,boolean ev returns integer
local integer Wu=Fv(qg[nq],Su)
call Hv(qg[(nq)],(Wu),(ev))
return Wu
endfunction
function Kx takes integer nq,integer Su,boolean ev returns integer
local integer Wu=Iv(qg[nq],Su)
call Cx(nq,Wu,ev)
return Wu
endfunction
function Lx takes integer nq,integer Wu returns nothing
call Zu(qg[nq],Wu)
call Tw(nq)
endfunction
function Mx takes integer nq,integer Wu returns nothing
call jv(qg[nq],Wu)
call nx(nq)
endfunction
function Nx takes integer nq,integer Wu returns nothing
call ov(qg[nq],Wu)
call rx(nq)
endfunction
function Ox takes integer nq,integer Wu returns nothing
call xv(qg[nq],Wu)
call ux(nq)
endfunction
function Px takes integer nq,integer Wu returns nothing
call Av(qg[nq],Wu)
call vx(nq)
endfunction
function Qx takes integer nq,integer Wu returns nothing
call lv(qg[nq],Wu)
call UnitRemoveType(og[(nq)],Z)
endfunction
function Rx takes integer nq returns boolean
return(Ie[qg[(nq)]])or(Ee[qg[(nq)]])or Gr(nq)
endfunction
function Sx takes integer nq,integer vr returns boolean
return(Ie[qg[(nq)]])or(Ee[qg[(nq)]])or Gr(nq)or(vr==1227894834 and((ze[qg[(nq)]])or Hr(nq)))or(vr==1227894841 and((be[qg[(nq)]])or Ir(nq)))or(vr==1227894832 and lr(nq))
endfunction
function Tx takes integer nq,real Kr returns real
return(100-jx(nq))*Kr/ 100.
endfunction
function Ux takes integer nq,real Kr returns real
return(100-mx(nq))*Kr/ 100.
endfunction
function Vx takes integer nq,real Kr,integer vr returns real
if(Ie[qg[(nq)]])then
return .0
endif
if vr==1227894834 then
if(ze[qg[(nq)]])or(Ee[qg[(nq)]])then
call Kv(qg[nq],Kr)
return .0
endif
return Jr(nq,Kr)
endif
if vr==1227894841 then
if(be[qg[(nq)]])or(Ee[qg[(nq)]])then
call Lv(qg[nq],Kr)
return .0
endif
return Lr(nq,Kr)
endif
if vr==1227894832 then
if(Ee[qg[(nq)]])then
call Mv(qg[nq],(vg[(nq)]*((Kr)*1.)/ 1000.))
return .0
endif
return Mr(nq,Kr)
endif
call Zs("Unrecognized damage type!")
return .0
endfunction
function Wx takes integer nq,real Kr returns real
set Kr=fv(qg[nq],Kr)
call Tw(nq)
return Kr
endfunction
function Xx takes integer nq,integer Yx,real Kr,integer vr,boolean Zx returns boolean
if not Sx(nq,vr)then
call Ys(1,nq,Yx,vr)
endif
set Kr=Vx(nq,Kr,vr)
if Kr<=.0 then
return true
endif
set Kr=Wx(nq,Kr)
if Kr>.0 then
call Ww(nq,-Kr)
endif
if ug[nq]>.0 then
return true
endif
call Pw(nq,true)
call er(Yx,nq)
call Nr(nq,Zx)
return false
endfunction
function ey takes integer nq returns integer
if IsUnitType(og[nq],UNIT_TYPE_FLYING)then
return E9[Ju(ng[nq],GetUnitX(og[nq]),GetUnitY(og[nq]))]
endif
return c9[Ju(ng[nq],GetUnitX(og[nq]),GetUnitY(og[nq]))]
endfunction
function fy takes integer nq returns nothing
call aw(kg[nq],rn[xg[nq]])
call Zt(Af[mg[nq]],nq)
call Qv(qg[nq])
set ag[nq]=null
set Bg[nq]=null
set bg[nq]=null
set Cg[nq]=null
set cg[nq]=null
set Dg[nq]=null
call UnitRemoveAbility(og[nq],1093677105)
call UnitRemoveAbility(og[nq],1093677128)
call UnitRemoveAbility(og[nq],1093677134)
call UnitRemoveAbility(og[nq],1093677142)
call UnitRemoveAbility(og[nq],1093677149)
call UnitRemoveAbility(og[nq],1093677150)
call UnitRemoveAbility(og[nq],1093677147)
call UnitRemoveAbility(og[nq],1093677089)
endfunction
function gy takes integer nq returns nothing
call Ew(nq)
call gu(af[kg[nq]],nq)
call fy(nq)
call Rr(nq)
call Zs("Minion "+I2S(nq)+" is deleted.")
endfunction
function hy takes nothing returns nothing
local integer nq=(LoadInteger(f7,3,GetHandleId(GetExpiredTimer())))
if wg[nq]>.0 then
call Ww(nq,wg[nq])
endif
set Ag[nq]=Ag[nq]-.25
if Ag[nq]>.0 then
call SetItemCharges(Dg[nq],ht(Ag[nq]))
else
call Ou(og[nq])
call gy(nq)
endif
endfunction
function jy takes integer nq returns nothing
call Xt(zf[mg[nq]],nq)
set vg[nq]=kn[xg[nq]]
set wg[nq]=mn[xg[nq]]
set Ag[nq]=Zm[xg[nq]]
call Dw(nq)
call UnitAddAbility(og[nq],1093677113)
call UnitAddAbility(og[nq],1093677118)
set ag[nq]=UnitAddItemById(og[nq],1227894836)
set Bg[nq]=UnitAddItemById(og[nq],1227894847)
set bg[nq]=UnitAddItemById(og[nq],1227894840)
set Cg[nq]=UnitAddItemById(og[nq],1227894837)
set cg[nq]=UnitAddItemById(og[nq],1227894848)
set Dg[nq]=UnitAddItemById(og[nq],1227894849)
call SetItemCharges(Dg[nq],ht(Ag[nq]))
call TriggerRegisterUnitEvent(L7,og[nq],EVENT_UNIT_DAMAGED)
call TimerStart(lg[nq],.25,true,function hy)
set qg[nq]=mr()
call Yt(Af[mg[nq]],nq)
call fu(af[kg[nq]],nq)
set Th=nq
call TriggerEvaluate(U4[gn[xg[nq]]])
call Vw(nq,vg[nq])
call nx(nq)
call vx(nq)
set sg[nq]=true
call SetUnitInvulnerable(og[nq],false)
set tg[nq]=false
endfunction
function ky takes integer nq returns boolean
return Bw(kg[nq],rn[xg[nq]])
endfunction
function my takes integer nq,integer ny,real hx returns nothing
call eu(Bf[mg[nq]],nq)
call RemoveUnit(og[nq])
set og[nq]=CreateUnit(Se[mg[nq]],ny,Eg[nq],Fg[nq],Gg[nq])
set pg[nq]=GetHandleId(og[nq])
call SetUnitColor(og[nq],GetPlayerColor(Le[kg[nq]]))
call SetUnitUserData(og[nq],nq)
call aw((kg[nq]),-((rn[xg[nq]])*1.))
set Ag[nq]=Zm[xg[nq]]
call UnitAddAbility(og[nq],1093677113)
call UnitAddAbility(og[nq],1093677118)
set ag[nq]=UnitAddItemById(og[nq],1227894836)
set Bg[nq]=UnitAddItemById(og[nq],1227894847)
set bg[nq]=UnitAddItemById(og[nq],1227894840)
set Cg[nq]=UnitAddItemById(og[nq],1227894837)
set cg[nq]=UnitAddItemById(og[nq],1227894848)
set Dg[nq]=UnitAddItemById(og[nq],1227894849)
call SetItemCharges(Dg[nq],ht(Ag[nq]))
call TriggerRegisterUnitEvent(L7,og[nq],EVENT_UNIT_DAMAGED)
call TimerStart(lg[nq],.25,true,function hy)
set qg[nq]=mr()
call Yt(Af[mg[nq]],nq)
call yr(rg[nq])
call Vw(nq,vg[nq]*hx)
call nx(nq)
call vx(nq)
set sg[nq]=true
call IssuePointOrderById(og[nq],G,yg[nq],zg[nq])
endfunction
function oy takes integer nq,real hx returns nothing
call my(nq,GetUnitTypeId(og[nq]),hx)
endfunction
function py takes integer nq returns nothing
call eu(Bf[mg[nq]],nq)
call gu(af[kg[nq]],nq)
call Ew(nq)
call RemoveUnit(og[nq])
call Rr(nq)
call Zs("Corpse "+I2S(nq)+" is removed.")
endfunction
function qy takes nothing returns nothing
call py(((LoadInteger(f7,3,GetHandleId(GetExpiredTimer())))))
endfunction
function ry takes integer nq returns nothing
call RemoveUnit(og[nq])
call gy(nq)
endfunction
function sy takes integer p,integer id,real ev returns nothing
local integer a=0
local integer m
local real ty=hw(p,id,ev)
loop
exitwhen a>m9[af[p]]
set m=(j9[k9[(af[p])]+(a)])
if jg[m]==id then
set vg[m]=vg[m]+ty
if sg[m]then
call Ww(m,ty)
endif
endif
set a=a+1
endloop
endfunction
function uy takes integer p,integer id,real ev returns nothing
local integer a=0
local integer m
local real vy=iw(p,id,ev)
loop
exitwhen a>m9[af[p]]
set m=(j9[k9[(af[p])]+(a)])
if jg[m]==id then
set wg[m]=wg[m]+vy
endif
set a=a+1
endloop
endfunction
function wy takes integer p,integer id,real ev returns nothing
local integer a=0
local integer m
set vn[cf[Df[p]+id]]=vn[cf[Df[p]+id]]+ev
loop
exitwhen a>m9[af[p]]
set m=(j9[k9[(af[p])]+(a)])
if jg[m]==id and sg[m]then
call nx(m)
endif
set a=a+1
endloop
endfunction
function xy takes integer p,integer id,integer ev returns nothing
local integer a=0
local integer m
set wn[cf[Df[p]+id]]=wn[cf[Df[p]+id]]+ev
set xn[cf[Df[p]+id]]=xn[cf[Df[p]+id]]+ev
loop
exitwhen a>m9[af[p]]
set m=(j9[k9[(af[p])]+(a)])
if jg[m]==id and sg[m]then
call vx(m)
endif
set a=a+1
endloop
endfunction
function yy takes integer p,integer id,integer ev returns nothing
local integer a=0
local integer m
set wn[cf[Df[p]+id]]=wn[cf[Df[p]+id]]+ev
loop
exitwhen a>m9[af[p]]
set m=(j9[k9[(af[p])]+(a)])
if jg[m]==id and sg[m]then
call sx(m)
endif
set a=a+1
endloop
endfunction
function zy takes integer nq returns nothing
set Vg[nq]=CreateTimer()
call SaveInteger(f7,3,GetHandleId((Vg[nq])),(nq))
endfunction
function Ay takes integer nq returns nothing
call Bt(Vg[nq])
set Vg[nq]=null
endfunction
function ay takes integer nq,integer By,unit by,integer p,integer T returns nothing
if nq==0 then
call Kt("Error: MinionBuff struct has run out of indexes. Current amount of indexes is 8190. Error detected on field of player "+I2S(p),"MinionBuffError")
endif
set Mg[nq]=T
set Ng[nq]=By
set Og[nq]=by
set Pg[nq]=p
set Qg[nq]=If[lf[p]+T]
call zy(nq)
call SaveInteger(f7,pg[Ng[nq]],T,nq)
endfunction
function Cy takes nothing returns nothing
local integer nq=(LoadInteger(f7,3,GetHandleId(GetExpiredTimer())))
set Rg[nq]=Rg[nq]-1
if Sr(nq)then
call Ur(nq)
endif
endfunction
function Dy takes integer nq,integer Ey returns nothing
set Rg[nq]=Rg[nq]+Ey
if Rg[nq]>Bh[Qg[nq]]then
set Rg[nq]=Bh[Qg[nq]]
endif
endfunction
function Fy takes integer nq,real Gy returns nothing
set Rg[nq]=ah[Qg[nq]]
call UnitAddAbility(og[Ng[nq]],ch[Qg[nq]])
call TimerStart(Vg[nq],Gy,Ah[Qg[nq]],function Cy)
set Sg[nq]=true
endfunction
function Hy takes integer nq returns nothing
call Fy(nq,Ch[Qg[nq]])
endfunction
function Iy takes integer nq,integer Ey returns nothing
if Sg[nq]then
call Dy(nq,Ey)
else
call Hy(nq)
endif
endfunction
function ly takes integer m returns integer
local integer nq=ks()
set Zg[nq]=m
set dh[nq]=CreateUnit(Le[Pi[m]],1685417325,Wi[m],Xi[m],.0)
call SetUnitPathing(dh[nq],false)
call SetUnitFlyHeight(dh[nq],Ui[m],.0)
call UnitAddType(dh[nq],W)
set fh[nq]=go[gj[m]]
call UnitAddAbility(dh[nq],fh[nq])
call SetUnitUserData(dh[nq],m)
set eh[nq]=ho[gj[m]]
call Zs("TowerAttacker "+I2S(nq)+" is created.")
return nq
endfunction
function Jy takes integer nq,integer Ky,integer Ly returns nothing
call UnitRemoveAbility(dh[nq],fh[nq])
set fh[nq]=Ky
call UnitAddAbility(dh[nq],Ky)
set eh[nq]=Ly
endfunction
function My takes integer nq returns nothing
call Jy(nq,go[gj[Zg[nq]]],ho[gj[Zg[nq]]])
endfunction
function Ny takes integer nq returns nothing
call IncUnitAbilityLevel(dh[nq],fh[nq])
endfunction
function Oy takes integer nq,real x,real y returns boolean
return IssuePointOrderById(dh[nq],eh[nq],x,y)
endfunction
function Py takes integer nq,widget sr returns boolean
return IssueTargetOrderById(dh[nq],eh[nq],sr)
endfunction
function Qy takes integer nq returns nothing
call RemoveUnit(dh[nq])
set dh[nq]=null
call Zs("TowerAttacker "+I2S(nq)+" is deleted.")
endfunction
function Ry takes integer nq returns nothing
if nq==null then
return
elseif(Yg[nq]!=-1)then
return
endif
call Qy(nq)
set Yg[nq]=Wg
set Wg=nq
endfunction
function Sy takes integer m returns integer
local integer nq=is()
set jh[nq]=m
set kh[nq]=ey(m)
set mh[nq]=ug[m]
return nq
endfunction
function Ty takes integer nq,integer Uy returns nothing
set oh[nq]=Uy
set nh[nq]=nh[Uy]
if nh[nq]!=0 then
set oh[nh[nq]]=nq
endif
set nh[Uy]=nq
endfunction
function Vy takes integer nq,integer Uy returns nothing
set nh[nq]=Uy
set oh[nq]=oh[Uy]
if oh[nq]!=0 then
set nh[oh[nq]]=nq
endif
set oh[Uy]=nq
endfunction
function Wy takes integer nq returns integer
local integer bu=sh[nq]
local integer m=jh[bu]
set sh[nq]=oh[bu]
if(sh[(nq)]!=0)then
set nh[sh[nq]]=0
endif
call js(bu)
return m
endfunction
function Xy takes integer Yy,integer Zy returns integer
local integer nq=gs()
set sh[nq]=Yy
set th[nq]=Zy
return nq
endfunction
function dz takes integer nq returns nothing
local integer n=sh[nq]
local integer ez
loop
exitwhen n==0
set ez=oh[n]
call js(n)
set n=ez
endloop
endfunction
function fz takes integer By returns integer
local group g=CreateGroup()
local unit u
local integer m
local integer c
local integer Wu
local integer Zy=0
local integer Yy=0
call GroupEnumUnitsInRange(g,Wi[By],Xi[By],do[gj[By]],Zn[gj[By]])
loop
set u=FirstOfGroup(g)
exitwhen u==null
call GroupRemoveUnit(g,u)
set m=GetUnitUserData(u)
set c=Sy(m)
set Wu=Zy
loop
if Wu==0 then
if Yy==0 then
set Zy=c
set Yy=c
else
call Ty(c,Yy)
set Yy=c
endif
exitwhen true
elseif kh[c]>=kh[Wu]then
call Vy(c,Wu)
if Wu==Zy then
set Zy=c
endif
exitwhen true
endif
set Wu=nh[Wu]
endloop
endloop
call DestroyGroup(g)
set g=null
if Yy==0 then
return 0
endif
return Xy(Yy,Zy)
endfunction
function gz takes nothing returns boolean
local integer m=GetUnitUserData(GetFilterUnit())
return((IsUnitType(GetFilterUnit(),X))and sg[m]and not(Ie[qg[(m)]]))!=null
endfunction
function hz takes nothing returns boolean
local integer m=GetUnitUserData(GetFilterUnit())
return((IsUnitType(GetFilterUnit(),X))and IsUnitType(og[m],UNIT_TYPE_GROUND)and sg[m]and not(Ie[qg[(m)]]))!=null
endfunction
function iz takes nothing returns boolean
local integer m=GetUnitUserData(GetFilterUnit())
return((IsUnitType(GetFilterUnit(),X))and IsUnitType(og[m],UNIT_TYPE_FLYING)and sg[m]and not(Ie[qg[(m)]]))!=null
endfunction
function jz takes nothing returns nothing
set l4[0]=Filter(function gz)
set l4[1]=Filter(function hz)
set l4[2]=Filter(function iz)
endfunction
function kz takes integer nq,integer mz returns nothing
set Tg[nq]=Hx(Ng[nq],nq,mz)
endfunction
function nz takes integer m,unit oz,integer p,integer mz returns nothing
local integer nq=(LoadInteger(f7,pg[(m)],(8)))
if nq==0 then
set nq=fs()
call ay(nq,m,oz,p,8)
call kz(nq,mz)
elseif Og[nq]!=oz then
set Og[nq]=oz
endif
call Hy(nq)
endfunction
function pz takes integer nq returns nothing
set Tg[nq]=Jx(Ng[nq],nq,true)
endfunction
function qz takes integer m,integer oz returns nothing
local integer nq=(LoadInteger(f7,pg[(m)],(9)))
if nq==0 then
set nq=es()
call ay(nq,m,og[oz],kg[oz],9)
call pz(nq)
elseif Og[nq]!=og[oz]then
set Og[nq]=og[oz]
endif
call Hy(nq)
endfunction
function rz takes integer nq,real sz returns nothing
set Tg[nq]=Dx(Ng[nq],nq,sz)
endfunction
function tz takes integer oz,real sz returns nothing
local integer nq=(LoadInteger(f7,pg[(oz)],(L4)))
if nq==0 then
set nq=ds()
call ay(nq,oz,og[oz],kg[oz],L4)
call rz(nq,sz)
elseif Og[nq]!=og[oz]then
set Og[nq]=og[oz]
endif
call Hy(nq)
endfunction
function uz takes integer nq,boolean up returns nothing
set Tg[nq]=lx(Ng[nq],nq,true)
if up then
set uh[nq]=true
set Ug[nq]=Gx(Ng[nq],nq,true)
endif
call AddUnitAnimationProperties(og[Ng[nq]],"Defend",true)
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\Human\\Defend\\DefendCaster.mdx"),(og[Ng[nq]]),("origin")))
endfunction
function vz takes integer oz returns nothing
local integer nq=(LoadInteger(f7,pg[(oz)],(7)))
if nq==0 then
set nq=Zr()
call ay(nq,oz,og[oz],kg[oz],7)
call uz(nq,yn[zn[xg[oz]]+1])
elseif Og[nq]!=og[oz]then
set Og[nq]=og[oz]
endif
call Hy(nq)
endfunction
function wz takes integer nq,integer kx,real xz returns nothing
set Tg[nq]=Ix(Ng[nq],nq,kx)
set Ug[nq]=Ex(Ng[nq],nq,xz)
endfunction
function yz takes integer m,integer oz,integer kx,real xz returns nothing
local integer nq=(LoadInteger(f7,pg[(m)],(K4)))
if nq==0 then
set nq=Yr()
call ay(nq,m,og[oz],kg[oz],K4)
call wz(nq,kx,xz)
elseif Og[nq]!=og[oz]then
set Og[nq]=og[oz]
endif
call Hy(nq)
endfunction
function zz takes integer nq returns nothing
set Tg[nq]=(Vu(qg[(Ng[nq])],(nq)))
endfunction
function Az takes integer m,integer oz returns nothing
local integer nq=(LoadInteger(f7,pg[(m)],(M4)))
if nq==0 then
set nq=Xr()
call ay(nq,m,og[oz],kg[oz],M4)
call zz(nq)
elseif Og[nq]!=og[oz]then
set Og[nq]=og[oz]
endif
call Iy(nq,ah[Qg[nq]])
endfunction
function az takes integer nq,boolean Bz,real xz returns nothing
set Tg[nq]=Ex(Ng[nq],nq,xz)
if Bz then
set vh[nq]=true
set Ug[nq]=Gx(Ng[nq],nq,true)
endif
endfunction
function bz takes integer oz,real xz returns nothing
local integer nq=(LoadInteger(f7,pg[(oz)],(P4)))
if nq==0 then
set nq=Wr()
call ay(nq,oz,og[oz],kg[oz],P4)
call az(nq,Uw(oz)<=.35,xz)
elseif Og[nq]!=og[oz]then
set Og[nq]=og[oz]
endif
call Hy(nq)
endfunction
function Cz takes integer nq returns nothing
set Tg[nq]=Kx(Ng[nq],nq,true)
endfunction
function cz takes integer oz returns nothing
local integer nq=(LoadInteger(f7,pg[(oz)],(N4)))
if nq==0 then
set nq=Vr()
call ay(nq,oz,og[oz],kg[oz],N4)
call Cz(nq)
elseif Og[nq]!=og[oz]then
set Og[nq]=og[oz]
endif
call Hy(nq)
endfunction
function Dz takes integer nq returns nothing
set Tg[nq]=Kx(Ng[nq],nq,true)
endfunction
function Ez takes integer m,integer oz returns nothing
local integer nq=(LoadInteger(f7,pg[(m)],(16)))
if nq==0 then
set nq=ns()
call ay(nq,m,og[oz],kg[oz],16)
call Dz(nq)
elseif Og[nq]!=og[oz]then
set Og[nq]=og[oz]
endif
call Hy(nq)
endfunction
function Fz takes integer nq returns nothing
set Tg[nq]=(Vu(qg[(Ng[nq])],(nq)))
endfunction
function Gz takes integer m,integer oz returns nothing
local integer nq=(LoadInteger(f7,pg[(m)],(17)))
if nq==0 then
set nq=os()
call ay(nq,m,og[oz],kg[oz],17)
call Fz(nq)
elseif Og[nq]!=og[oz]then
set Og[nq]=og[oz]
endif
call Iy(nq,ah[Qg[nq]])
endfunction
function Hz takes integer T,boolean Iz,integer lz,integer Jz,real Kz,integer Lz returns nothing
set J4[T]=br()
set zh[J4[T]]=Iz
set Ah[J4[T]]=Jz>1
set ah[J4[T]]=lz
set Bh[J4[T]]=Jz
set bh[J4[T]]=Kz
set Ch[J4[T]]=Kz/ lz
set ch[J4[T]]=Lz
endfunction
function Nz takes integer a,unit m,player p,integer Qu,integer Ly returns integer
local integer nq=Br()
set Gh[nq]=CreateUnit(p,1685417325,GetUnitX(m),GetUnitY(m),.0)
call SetUnitPathing(Gh[nq],false)
call UnitAddAbility(Gh[nq],Qu)
call UnitAddType(Gh[nq],Y)
call SetUnitUserData(Gh[nq],a)
set Hh[nq]=Ly
call Zs("MinionDummy "+I2S(nq)+" is created.")
return nq
endfunction
function Oz takes integer nq,real x,real y returns nothing
call SetUnitX(Gh[nq],x)
call SetUnitY(Gh[nq],y)
endfunction
function Pz takes integer nq,real x,real y returns boolean
call Zs(OrderId2String(Hh[nq])+" "+I2S(Hh[nq]))
return IssuePointOrderById(Gh[nq],Hh[nq],x,y)
endfunction
function Qz takes integer nq returns nothing
call RemoveUnit(Gh[nq])
set Gh[nq]=null
call Zs("MinionDummy "+I2S(nq)+" is deleted.")
endfunction
function Rz takes integer nq returns nothing
if nq==null then
return
elseif(Fh[nq]!=-1)then
return
endif
call Qz(nq)
set Fh[nq]=Dh
set Dh=nq
endfunction
function Sz takes integer nq returns nothing
set Ph[nq]=CreateTimer()
call SaveInteger(f7,3,GetHandleId((Ph[nq])),(nq))
endfunction
function Tz takes integer nq returns nothing
call Bt(Ph[nq])
set Ph[nq]=null
endfunction
function Uz takes nothing returns nothing
local integer nq=(LoadInteger(f7,3,GetHandleId(GetExpiredTimer())))
call SetImagePosition(Oh[nq],GetUnitX(Nh[nq]),GetUnitY(Nh[nq]),.0)
endfunction
function Vz takes unit u,real ct returns integer
local integer nq=ar()
set Nh[nq]=u
set Oh[nq]=Ct(ct,GetUnitX(u),GetUnitY(u),3,Lh,51,51,Mh)
call Sz(nq)
call TimerStart(Ph[nq],Kh,true,function Uz)
return nq
endfunction
function Wz takes integer nq returns nothing
call Tz(nq)
call DestroyImage(Oh[nq])
set Oh[nq]=null
set Nh[nq]=null
endfunction
function Xz takes integer nq returns nothing
if nq==null then
return
elseif(Jh[nq]!=-1)then
return
endif
call Wz(nq)
set Jh[nq]=Ih
set Ih=nq
endfunction
function Yz takes integer nq returns nothing
if Xh[nq]==null then
set Xh[nq]=CreateTimer()
call SaveInteger(f7,3,GetHandleId((Xh[nq])),(nq))
endif
endfunction
function Zz takes integer nq returns nothing
if Xh[nq]!=null then
call Bt(Xh[nq])
set Xh[nq]=null
endif
endfunction
function dA takes nothing returns nothing
local integer nq=(LoadInteger(f7,3,GetHandleId(GetExpiredTimer())))
set Wh[nq]=not Fj[Vh[nq]]
call qr(nq)
endfunction
function eA takes integer nq returns nothing
call Yz(nq)
if Gj[Vh[nq]]>.0 then
call TimerStart(Xh[nq],Gj[Vh[nq]],Fj[Vh[nq]],function dA)
else
set Wh[nq]=true
endif
endfunction
function fA takes integer nq,integer m,integer T returns nothing
set rg[m]=nq
set Uh[nq]=m
set Vh[nq]=Gf[Hf[kg[m]]+T]
endfunction
function gA takes integer nq returns nothing
set Wh[nq]=false
call TimerStart(Xh[nq],Hj[Vh[nq]],Fj[Vh[nq]],function dA)
endfunction
function hA takes integer nq,integer sr,integer ur,integer vr returns boolean
if Wh[nq]and not((he[qg[(Uh[nq])]]))and IsUnitInRange(og[sr],og[Uh[nq]],Ij[Vh[nq]])then
return tr(nq,sr,ur,vr)
endif
return false
endfunction
function iA takes nothing returns nothing
local integer m=Th
local integer nq=ps()
call fA(nq,m,4)
call eA(nq)
call Zs("AbilArcaneShield "+I2S(nq)+" is created.")
endfunction
function jA takes nothing returns nothing
local integer m=Th
local integer nq=qs()
call fA(nq,m,1)
call Zs("AbilFlexibility "+I2S(nq)+" is created.")
endfunction
function kA takes integer nq returns nothing
set Zh[nq]=not Zh[nq]
endfunction
function mA takes nothing returns nothing
local integer m=Th
local integer nq=rg[m]
if Zh[nq]then
set ei[nq]=.15
set di[nq]=Yh
endif
endfunction
function nA takes nothing returns nothing
local integer m=Th
local integer nq=rs()
call fA(nq,m,2)
call eA(nq)
call Zs("AbilShieldsUp "+I2S(nq)+" is created.")
endfunction
function oA takes nothing returns nothing
local integer m=Th
local integer nq=ss()
call fA(nq,m,5)
call eA(nq)
set hi[nq]=Vz(og[m],Ij[Vh[nq]])
call Zs("AbilInspiration "+I2S(nq)+" is created.")
endfunction
function pA takes nothing returns nothing
local integer m=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),X))and IsUnitType(og[m],UNIT_TYPE_GROUND)and sg[m]and kg[m]==kg[ji]and en[xg[m]]then
if jg[m]!=3 then
call yz(m,ji,fi,.05)
elseif not ki then
set ki=m!=ji
endif
endif
endfunction
function qA takes nothing returns nothing
set gi=Filter(function pA)
endfunction
function rA takes nothing returns nothing
local integer m=Th
local integer nq=ts()
call fA(nq,m,3)
call Zs("AbilCommander "+I2S(nq)+" is created.")
endfunction
function sA takes integer nq,integer id returns nothing
if id==cj[Vh[nq]]then
set qi[nq]=true
call UnitRemoveAbility(og[Uh[nq]],id)
call UnitAddAbility(og[Uh[nq]],1093677143)
call AddUnitAnimationProperties(og[Uh[nq]],"Defend",true)
elseif id==1093677143 then
set qi[nq]=false
call UnitRemoveAbility(og[Uh[nq]],id)
call UnitAddAbility(og[Uh[nq]],cj[Vh[nq]])
call AddUnitAnimationProperties(og[Uh[nq]],"Defend",false)
endif
endfunction
function tA takes nothing returns nothing
local integer m=Th
local integer nq=rg[m]
if qi[nq]then
set ri[nq]=mi
if yn[zn[xg[m]]+0]then
set si[nq]=25
endif
else
call eA(nq)
set ui[nq]=Vz(og[m],Ij[Vh[nq]])
endif
endfunction
function uA takes nothing returns nothing
local integer m=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),X))and IsUnitType(og[m],UNIT_TYPE_GROUND)and sg[m]and kg[m]==vi and en[xg[m]]and jg[m]!=5 then
set wi=wi+1
endif
endfunction
function vA takes nothing returns nothing
local integer i=0
loop
set ni[i]=3
set oi[i]=30
set i=i+1
exitwhen i==R
endloop
set pi=Filter(function uA)
endfunction
function wA takes nothing returns nothing
local integer i=0
loop
set yi[i]=.25
set i=i+1
exitwhen i==R
endloop
endfunction
function xA takes nothing returns nothing
local integer m=Th
local integer nq=us()
call fA(nq,m,6)
call eA(nq)
set zi[nq]=Nz(nq,og[m],Le[mg[m]],1093677368,xi)
call Zs("AbilHealingWave "+I2S(nq)+" is created.")
endfunction
function yA takes nothing returns nothing
local integer i=Me[(Cm)]
set yi[i]=yi[i]+.1
endfunction
function zA takes nothing returns nothing
local integer m=Th
local integer nq=vs()
call fA(nq,m,7)
call eA(nq)
call Zs("AbilDivineShield "+I2S(nq)+" is created.")
endfunction
function AA takes nothing returns nothing
local integer m=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),X))and sg[m]and kg[m]==ai and en[xg[m]]and not Rx(m)and Uw(m)<bi then
set bi=Uw(m)
set Bi=m
endif
endfunction
function aA takes integer nq returns integer
set ai=kg[Uh[nq]]
set Bi=0
set bi=2.
call GroupEnumUnitsInRange(bj_lastCreatedGroup,GetUnitX(og[Uh[nq]]),GetUnitY(og[Uh[nq]]),512.,Ai)
return Bi
endfunction
function BA takes nothing returns nothing
set Ai=Filter(function AA)
endfunction
function bA takes integer nq returns nothing
set Ei[nq]=CreateTimer()
call SaveInteger(f7,3,GetHandleId((Ei[nq])),(nq))
endfunction
function CA takes integer nq returns nothing
call Bt(Ei[nq])
set Ei[nq]=null
endfunction
function cA takes nothing returns nothing
local integer m=Th
local integer nq=ws()
call fA(nq,m,8)
call eA(nq)
call bA(nq)
call Zs("AbilResurrection "+I2S(nq)+" is created.")
endfunction
function DA takes nothing returns nothing
local integer m=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),X))and Sw(m)and kg[m]==Fi and ky(m)and rn[xg[m]]>Hi then
set Hi=rn[xg[m]]
set Gi=m
endif
endfunction
function EA takes integer nq returns nothing
local integer FA=Uh[nq]
set Fi=kg[FA]
set Gi=0
set Hi=.0
call GroupEnumUnitsInRange(bj_lastCreatedGroup,GetUnitX(og[FA]),GetUnitY(og[FA]),Ij[Vh[nq]],ci)
set FA=Gi
if FA!=0 then
call oy(FA,Ci[Me[kg[Uh[nq]]]])
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\Human\\Resurrect\\ResurrectTarget.mdx"),(og[FA]),("origin")))
if(q9[r9[(Kf[Lf[kg[FA]]+3])]+(1)])then
call Gz(FA,Uh[nq])
endif
call gA(nq)
else
call TimerStart(Ei[nq],.25,false,Di)
endif
endfunction
function GA takes nothing returns nothing
call EA(((LoadInteger(f7,3,GetHandleId(GetExpiredTimer())))))
endfunction
function HA takes nothing returns nothing
local integer i=0
loop
set Ci[i]=.5
set i=i+1
exitwhen i==R
endloop
set ci=Filter(function DA)
set Di=function GA
endfunction
function IA takes nothing returns nothing
local integer i=Me[(Cm)]
set Ci[i]=.75
endfunction
function lA takes integer nq returns nothing
set Li[nq]=CreateTimer()
call SaveInteger(f7,3,GetHandleId((Li[nq])),(nq))
endfunction
function JA takes integer nq returns nothing
call Bt(Li[nq])
set Li[nq]=null
endfunction
function KA takes integer nq returns nothing
call JA(nq)
call Zs("TowerSeller "+I2S(nq)+" is deleted.")
endfunction
function LA takes integer nq returns nothing
if nq==null then
return
elseif(Ji[nq]!=-1)then
return
endif
call KA(nq)
set Ji[nq]=Ii
set Ii=nq
endfunction
function MA takes nothing returns nothing
local integer nq=(LoadInteger(f7,3,GetHandleId(GetExpiredTimer())))
set Zi[Ki[nq]]=.5
call IncUnitAbilityLevel(Qi[Ki[nq]],1093677119)
set xj[Ki[nq]]=0
call LA(nq)
endfunction
function NA takes integer u returns integer
local integer nq=kr()
set Ki[nq]=u
call lA(nq)
call TimerStart(Li[nq],5.,false,function MA)
return nq
endfunction
function OA takes integer nq returns nothing
set ij[nq]=CreateTimer()
call SaveInteger(f7,3,GetHandleId((ij[nq])),(nq))
endfunction
function PA takes integer nq returns nothing
call Bt(ij[nq])
set ij[nq]=null
endfunction
function QA takes integer f,real x,real y,boolean RA returns nothing
local integer c=Ju(f,x,y)
set RA=not RA
set H9[(c)]=(RA)
set H9[(l9[c])]=(RA)
set H9[(J9[l9[c]])]=(RA)
set H9[(J9[c])]=(RA)
endfunction
function SA takes integer nq,integer T returns nothing
set Ri[nq]=T
call UnitAddAbility(Qi[nq],1098133552+T)
set Si[nq]=(T)
set Yi[nq]=no[Si[nq]]
set gj[nq]=(T)
endfunction
function TA takes integer nq,integer id returns item
call IncUnitAbilityLevel(Qi[nq],1093677104)
set bj_lastCreatedItem=UnitAddItemById(Qi[nq],id)
call SetItemUserData(bj_lastCreatedItem,(GetUnitAbilityLevel(Qi[(nq)],1093677104)-2))
return bj_lastCreatedItem
endfunction
function WA takes integer nq,item i returns nothing
local integer VA=GetItemUserData(i)
local integer XA=(GetUnitAbilityLevel(Qi[(nq)],1093677104)-2)
local integer YA
call RemoveItem(i)
loop
exitwhen VA==XA
set YA=VA
set VA=VA+1
set i=UnitItemInSlot(Qi[nq],VA)
call SetItemDroppable(i,true)
call UnitDropItemSlot(Qi[nq],i,YA)
call SetItemUserData(i,YA)
call SetItemDroppable(i,false)
endloop
call DecUnitAbilityLevel(Qi[nq],1093677104)
endfunction
function ZA takes integer nq,real ct,integer Dt,integer Et,integer Ft,boolean da returns image
call Ht(Le[Pi[nq]],ct,Wi[nq],Xi[nq],2,Dt,Et,Ft,Q4)
call ShowImage(bj_lastCreatedImage,da and IsUnitSelected(Qi[nq],Le[Pi[nq]]))
return bj_lastCreatedImage
endfunction
function ea takes integer nq returns nothing
set ej[nq]=Vn[gj[nq]]
set fj[nq]=eo[gj[nq]]
set sj[nq]=TA(nq,Yn[gj[nq]])
set tj[nq]=TA(nq,1227894833)
call OA(nq)
set hj[nq]=ly(nq)
set jj[nq]=ZA(nq,do[gj[nq]],51,$CC,51,true)
set dj[nq]=true
endfunction
function fa takes integer nq returns nothing
call PA(nq)
call Ry(hj[nq])
call DestroyImage(jj[nq])
set jj[nq]=null
set sj[nq]=null
set tj[nq]=null
endfunction
function ga takes integer nq returns real
return .0
endfunction
function ha takes integer nq returns real
return .0
endfunction
function ia takes integer nq returns real
local real gv=ej[nq]+Vq(nq)
return gv+gv*Wq(nq)
endfunction
function ja takes integer nq,integer sr,real Kr,integer vr,boolean ka returns boolean
return Xx(sr,nq,Kr,vr,ka)
endfunction
function ma takes integer nq returns nothing
call SetItemCharges(sj[nq],gt(ia(nq)))
endfunction
function na takes integer nq,integer m returns boolean
local real r=ia(nq)
call SetItemCharges(sj[nq],gt(r))
return ja(nq,m,r,Yn[gj[nq]],fo[gj[nq]])
endfunction
function oa takes integer nq,integer m returns boolean
return ja(nq,m,ia(nq),Yn[gj[nq]],fo[gj[nq]])
endfunction
function pa takes integer nq returns nothing
call TimerStart(ij[nq],Xn[gj[nq]],false,R4)
endfunction
function qa takes integer nq,integer dr,integer ur returns boolean
local integer ra=0
local integer m
loop
set m=Wy(dr)
call Ys(1,m,nq,Yn[gj[nq]])
if(Ie[qg[(m)]])then
exitwhen(sh[(dr)]==0)
else
call Py(ur,og[m])
set ra=ra+1
if(sh[(dr)]==0)or ra==fj[nq]then
return true
endif
endif
endloop
return false
endfunction
function sa takes integer nq returns nothing
local integer dr=fz(nq)
if dr!=0 then
call Zq(nq,dr)
call dz(dr)
call hs(dr)
else
call TimerStart(ij[(nq)],I4,false,R4)
endif
endfunction
function ta takes nothing returns nothing
call sa(((LoadInteger(f7,3,GetHandleId(GetExpiredTimer())))))
endfunction
function s__Tower_filterAttack takes nothing returns nothing
endfunction
function s__Tower_filterAutoattackToggle takes nothing returns nothing
endfunction
function ua takes integer nq returns nothing
set Ui[nq]=io[Si[nq]]*jo[Si[nq]]
set Vi[nq]=jo[Si[nq]]
call SetUnitScale(Qi[nq],Vi[nq],Vi[nq],Vi[nq])
if mo[Si[nq]]then
call ea(nq)
endif
set nj[nq]=qo[Si[nq]]
if nj[nq]then
set qj[nq]=vo[Si[nq]]
set rj[nq]=vo[Si[nq]]
set uj[nq]=TA(nq,1227894839)
if ro[Si[nq]]then
set vj[nq]=TA(nq,1227894838)
else
set vj[nq]=TA(nq,1227894842)
call UnitAddAbility(Qi[nq],1093677129)
endif
call SetItemCharges(vj[nq],qj[nq])
endif
if oo[Si[nq]]then
set xj[nq]=NA(nq)
elseif Si[nq]!=(0)then
set Zi[nq]=.0
endif
set Lj=nq
call TriggerEvaluate(qm[(ko[Si[nq]])])
set Ti[nq]=true
if mo[Si[nq]]then
call ma(nq)
call sa(nq)
endif
endfunction
function va takes integer nq returns nothing
call Tt(xf[Pi[nq]],nq)
call RemoveUnit(Qi[nq])
if mo[Si[nq]]and dj[nq]then
call fa(nq)
endif
if wj[nq]!=0 then
call Cq(wj[nq])
endif
set uj[nq]=null
set vj[nq]=null
if xj[nq]!=0 then
call LA(xj[nq])
endif
set Qi[nq]=null
call jr(nq)
call Zs("Tower "+I2S(nq)+" is deleted.")
endfunction
function wa takes integer nq returns nothing
call DestroyEffect(AddSpecialEffect(("Objects\\Spawnmodels\\Human\\HCancelDeath\\HCancelDeath.mdx"),((Wi[nq])*1.),((Xi[nq])*1.)))
if Zi[nq]>.0 then
set Yi[nq]=gt(Yi[nq]*Zi[nq])
call sw(Pi[nq],Yi[nq])
call ww(Pi[nq],I2S(Yi[nq]),Qi[nq])
endif
call QA(Ue[Pi[nq]],Wi[nq],Xi[nq],false)
call Ku(Ue[Pi[nq]])
call va(nq)
endfunction
function xa takes integer nq returns nothing
call UnitRemoveAbility(Qi[nq],1093677129)
call WA(nq,vj[nq])
set nj[nq]=false
endfunction
function ya takes integer nq returns nothing
local boolean Iw=not ro[Si[nq]]
set pj[nq]=pj[nq]+1
call Bq(wj[nq])
call SetItemCharges(uj[nq],pj[nq])
if mo[Si[nq]]then
call Xq(nq)
call ma(nq)
endif
if Iw then
set Yi[nq]=Yi[nq]+rj[nq]
endif
if pj[nq]<wo[Si[nq]]then
set rj[nq]=rj[nq]*so[Si[nq]]+(pj[nq]+1)*to[Si[nq]]+uo[Si[nq]]
elseif pj[nq]==wo[Si[nq]]then
set rj[nq]=qj[nq]/ xo[Si[nq]]
endif
set qj[nq]=qj[nq]+rj[nq]
if Iw then
call SetItemCharges(vj[nq],rj[nq])
endif
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\Items\\AIlm\\AIlmTarget.mdx"),(Qi[nq]),("origin")))
endfunction
function za takes integer nq,integer Kr returns nothing
set oj[nq]=oj[nq]+Kr
loop
exitwhen oj[nq]<qj[nq]
call ya(nq)
endloop
call SetItemCharges(vj[nq],qj[nq]-oj[nq])
endfunction
function Aa takes integer m,integer t returns nothing
local real Kr=qn[xg[m]]
local integer p=Pi[t]
local integer aa
local integer Ba
local boolean ba=true
local integer array Ca
local integer n=-1
local integer a=0
local real x=GetUnitX(og[m])
local real y=GetUnitY(og[m])
if ro[Si[t]]and not IsUnitInRangeXY(Qi[t],x,y,384.)then
set n=0
set Ca[0]=t
set aa=pj[t]
set Ba=pj[t]
set ba=false
endif
loop
exitwhen a>F8[xf[p]]
set t=(D8[E8[(xf[p])]+(a)])
if Ti[t]and ro[Si[t]]and IsUnitInRangeXY(Qi[t],x,y,384.)then
set n=n+1
set Ca[n]=t
if ba then
set Ba=pj[t]
set aa=pj[t]
set ba=false
endif
if pj[t]>Ba then
set Ba=pj[t]
endif
if pj[t]<aa then
set aa=pj[t]
endif
endif
set a=a+1
endloop
if ba then
return
endif
set Kr=Kr*(aa+Ba+2)/ 2.
set a=0
loop
exitwhen a>n
set t=Ca[a]
set Ba=ht(Kr/(pj[t]+1))
if Ba==0 then
set Ba=1
endif
call za(t,Ba)
set a=a+1
endloop
endfunction
function ca takes integer nq,integer m returns nothing
set mj[nq]=mj[nq]+1
call SetItemCharges(tj[nq],mj[nq])
call pw(Pi[nq])
if qn[xg[m]]>0 then
call Aa(m,nq)
endif
endfunction
function Da takes integer nq returns nothing
local integer array Ea
local integer Fa
local integer Ga
local integer n=-1
local integer a=0
local integer u=-1
loop
set Fa=(D8[E8[(xf[Pi[nq]])]+(a)])
if IsUnitSelected(Qi[Fa],Le[Pi[nq]])and not ro[Si[Fa]]and Ti[Fa]and(Ve[(Pi[nq])]-(rj[Fa])>=0)then
set n=n+1
set Ea[n]=Fa
loop
exitwhen u<0 or rj[Ea[u]]>=rj[Fa]
set Ga=Ea[u]
set Ea[u]=Fa
set Ea[u+1]=Ga
set u=u-1
endloop
set u=n
endif
set a=a+1
exitwhen a>F8[xf[Pi[nq]]]
endloop
if n==-1 then
call vw(Pi[nq],rj[nq])
return
endif
set a=0
loop
set Fa=Ea[a]
exitwhen not tw(Pi[nq],rj[Fa])
call ya(Fa)
set a=a+1
exitwhen a>n
endloop
endfunction
function Ha takes nothing returns nothing
set R4=function ta
endfunction
function Ia takes integer nq returns nothing
set Tg[nq]=(Vu(qg[(Ng[nq])],(nq)))
endfunction
function la takes integer m,integer oz returns nothing
local integer nq
if(Ie[qg[(m)]])then
return
endif
set nq=(LoadInteger(f7,pg[(m)],(1)))
if nq==0 then
set nq=xs()
call ay(nq,m,Qi[oz],Pi[oz],1)
call Ia(nq)
elseif Og[nq]!=Qi[oz]then
set Og[nq]=Qi[oz]
endif
call Iy(nq,ah[Qg[nq]])
endfunction
function Ja takes integer nq,real Ka returns nothing
set Tg[nq]=Ex(Ng[nq],nq,Ka)
endfunction
function La takes integer m,integer oz,real Ka returns nothing
local integer nq
if Fr(m)or(Ie[qg[(m)]])then
return
endif
set nq=(LoadInteger(f7,pg[(m)],(2)))
if nq==0 then
set nq=ys()
call ay(nq,m,Qi[oz],Pi[oz],2)
call Ja(nq,Ka)
elseif Og[nq]!=Qi[oz]then
set Og[nq]=Qi[oz]
endif
call Hy(nq)
endfunction
function Ma takes integer nq,integer kx returns nothing
set Tg[nq]=Ix(Ng[nq],nq,kx)
endfunction
function Na takes integer m,integer oz,integer kx returns nothing
local integer nq
if(Ie[qg[(m)]])then
return
endif
set nq=(LoadInteger(f7,pg[(m)],(3)))
if nq==0 then
set nq=zs()
call ay(nq,m,Qi[oz],Pi[oz],3)
call Ma(nq,kx)
elseif Og[nq]!=Qi[oz]then
set Og[nq]=Qi[oz]
endif
call Hy(nq)
endfunction
function Oa takes integer nq,real Ka returns nothing
set Tg[nq]=Ex(Ng[nq],nq,Ka)
endfunction
function Pa takes integer m,integer oz,real Ka returns nothing
local integer nq
if Fr(m)or(Ie[qg[(m)]])then
return
endif
set nq=(LoadInteger(f7,pg[(m)],(4)))
if nq==0 then
set nq=As()
call ay(nq,m,Qi[oz],Pi[oz],4)
call Oa(nq,Ka)
elseif Og[nq]!=Qi[oz]then
set Og[nq]=Qi[oz]
endif
call Hy(nq)
endfunction
function Sa takes integer nq,integer kx returns nothing
set Tg[nq]=Ix(Ng[nq],nq,kx)
endfunction
function Ta takes integer m,integer oz,integer kx returns nothing
local integer nq=(LoadInteger(f7,pg[(m)],(6)))
if nq==0 then
set nq=Bs()
call ay(nq,m,Qi[oz],Pi[oz],6)
call Sa(nq,kx)
elseif Og[nq]!=Qi[oz]then
set Og[nq]=Qi[oz]
endif
call Hy(nq)
endfunction
function Ua takes integer nq,real sz returns nothing
set Tg[nq]=Dx(Ng[nq],nq,sz)
endfunction
function Va takes integer m,integer oz,real sz returns nothing
local integer nq=(LoadInteger(f7,pg[(m)],(O4)))
if nq==0 then
set nq=bs()
call ay(nq,m,Qi[oz],Pi[oz],O4)
call Ua(nq,sz)
elseif Og[nq]!=Qi[oz]then
set Og[nq]=Qi[oz]
endif
call Hy(nq)
endfunction
function Wa takes nothing returns nothing
local integer nq=Cs()
call Kw(nq,4,hg)
call Zs("Wizard "+I2S(nq)+" is created.")
endfunction
function Xa takes nothing returns nothing
local integer d=If[lf[(ig)]+9]
set bh[d]=bh[d]+.5
set Ch[d]=bh[d]
endfunction
function Ya takes integer m,integer p returns integer
local integer nq=cs()
set kg[nq]=p
set og[nq]=CreateUnit(Le[p],Wm[W4[1]],xt(hd[Ue[p]]),yt(hd[Ue[p]]),270.)
call UnitAddAbility(og[nq],1098282348)
call UnitMakeAbilityPermanent(og[nq],true,1098282348)
call SetUnitUserData(og[nq],nq)
set jg[nq]=1
set pg[nq]=GetHandleId(og[nq])
set xg[nq]=cf[Df[p]+1]
call UnitAddAbility(og[nq],1093677099)
call UnitMakeAbilityPermanent(og[nq],true,1093677099)
call UnitAddAbility(og[nq],Ym[W4[1]])
call UnitMakeAbilityPermanent(og[nq],true,Ym[W4[1]])
call UnitAddAbility(og[nq],En[Fn[Y4[1]]])
call UnitMakeAbilityPermanent(og[nq],true,En[Fn[Y4[1]]])
call UnitAddAbility(og[nq],En[Fn[Y4[1]]+1])
call UnitMakeAbilityPermanent(og[nq],true,En[Fn[Y4[1]]+1])
call UnitAddAbility(og[nq],En[Fn[Y4[1]]+2])
call UnitMakeAbilityPermanent(og[nq],true,En[Fn[Y4[1]]+2])
set Th=nq
call TriggerEvaluate(T4[gn[xg[nq]]])
call UnitAddAbility(og[nq],1093677122)
call UnitMakeAbilityPermanent(og[nq],true,1093677122)
call UnitAddAbility(og[nq],cj[S4[gn[xg[nq]]]])
call UnitMakeAbilityPermanent(og[nq],true,cj[S4[gn[xg[nq]]]])
call Ut(yf[p],nq)
set zj[nq]=m
call Zs("ConscriptTwin "+I2S(nq)+" is created.")
return nq
endfunction
function Za takes nothing returns nothing
local integer nq=cs()
call Kw(nq,1,hg)
if yn[zn[xg[nq]]+2]then
set zj[nq]=Ya(nq,kg[nq])
else
set zj[nq]=0
endif
call Zs("Conscript "+I2S(nq)+" is created.")
endfunction
function dB takes integer nq returns nothing
call Vt(yf[kg[nq]],nq)
call aw(kg[nq],rn[xg[nq]])
call DestroyEffect(AddSpecialEffect(("Abilities\\Spells\\Other\\Transmute\\PileofGold.mdx"),((GetUnitX(og[nq]))*1.),((GetUnitY(og[nq]))*1.)))
call RemoveUnit(og[nq])
call Rr(nq)
call Zs("ConscriptTwin "+I2S(nq)+" is sold.")
endfunction
function eB takes nothing returns nothing
call wy(ig,1,.1)
endfunction
function fB takes nothing returns nothing
call xy(ig,1,20)
endfunction
function gB takes nothing returns nothing
local integer hB=ig
local integer iB=K8[yf[hB]]
local real jB=rn[cf[Df[hB]+1]]-.5
local integer a=0
local integer m
set rn[cf[Df[hB]+1]]=.5
loop
exitwhen a>iB
set m=(l8[J8[(yf[hB])]+(a)])
if jg[m]==1 then
set zj[m]=Ya(m,hB)
endif
set a=a+1
endloop
set a=0
set iB=m9[af[hB]]
loop
exitwhen a>iB
set m=(j9[k9[(af[hB])]+(a)])
if jg[m]==1 and sg[m]then
call aw(hB,jB)
endif
set a=a+1
endloop
set a=0
set iB=Q8[zf[Pe[hB]]]
loop
exitwhen a>iB
set m=(O8[P8[(zf[Pe[hB]])]+(a)])
if jg[m]==1 then
call aw(hB,jB)
endif
set a=a+1
endloop
endfunction
function kB takes nothing returns nothing
local integer nq=Ds()
call Kw(nq,2,hg)
call Zs("Footman "+I2S(nq)+" is created.")
endfunction
function mB takes nothing returns nothing
call yy(ig,2,30)
endfunction
function nB takes nothing returns nothing
local integer nq=Es()
call Kw(nq,3,hg)
call Zs("Knight "+I2S(nq)+" is created.")
endfunction
function oB takes nothing returns nothing
call sy(ig,3,100.)
endfunction
function pB takes nothing returns nothing
call xy(ig,3,aj)
endfunction
function qB takes nothing returns nothing
local integer d=If[lf[(ig)]+K4]
set bh[d]=1.
set Ch[d]=1.
endfunction
function rB takes nothing returns nothing
local integer nq=Fs()
call Kw(nq,5,hg)
call Zs("Captain "+I2S(nq)+" is created.")
endfunction
function sB takes nothing returns nothing
local integer hB=ig
local integer a=0
local integer m
local integer b
loop
exitwhen a>m9[af[hB]]
set m=(j9[k9[(af[hB])]+(a)])
set b=rg[m]
if jg[m]==5 and qi[b]then
set si[b]=25
if sg[m]then
call ux(m)
endif
endif
set a=a+1
endloop
endfunction
function tB takes nothing returns nothing
local integer i=Me[(ig)]
set ni[i]=5
set oi[i]=50
endfunction
function uB takes nothing returns nothing
local integer nq=Gs()
call Lw(nq,6,hg)
call Zs("Hero Paladin "+I2S(nq)+" is created.")
endfunction
function vB takes nothing returns nothing
call sy(ig,6,1000.)
endfunction
function wB takes nothing returns nothing
call xy(ig,6,25)
endfunction
function xB takes nothing returns nothing
call uy(ig,6,15.)
endfunction
function yB takes integer T,integer zB,boolean AB,boolean aB,boolean BB,real bB,real cd,real ct returns nothing
set S4[T]=cq()
set cj[S4[T]]=zB
set Dj[S4[T]]=AB
set Ej[S4[T]]=aB
set Fj[S4[T]]=BB
set Gj[S4[T]]=bB
if BB then
set Hj[S4[T]]=bB
else
set Hj[S4[T]]=cd
endif
set Ij[S4[T]]=ct
endfunction
function CB takes integer T,code cB,code DB returns nothing
if cB!=null then
set T4[T]=CreateTrigger()
call TriggerAddCondition(T4[T],Filter(cB))
endif
if DB!=null then
set U4[T]=CreateTrigger()
call TriggerAddCondition(U4[T],Filter(DB))
endif
endfunction
function FB takes integer nq returns nothing
set Xj[nq]=CreateTimer()
call SaveInteger(f7,3,GetHandleId((Xj[nq])),(nq))
endfunction
function GB takes integer nq returns nothing
call Bt(Xj[nq])
set Xj[nq]=null
endfunction
function HB takes nothing returns nothing
local integer m=Lj
local integer nq=Hs(m,1)
set Rj[nq]=TA(m,1227894835)
set Wj[nq]=hj[m]
call FB(nq)
call Jy(Wj[(nq)],1093677096,Oj)
call TriggerRegisterUnitEvent(Pj,dh[Wj[nq]],EVENT_UNIT_SPELL_ENDCAST)
call Zs("AbilFocusedAttack "+I2S(nq)+" is created.")
endfunction
function IB takes integer nq returns real
if Tj[nq]<=.0 then
return Uj[nq]
endif
return .0
endfunction
function lB takes integer nq,real r returns nothing
set Tj[nq]=Tj[nq]-r
if Tj[nq]<=.0 then
set Tj[nq]=.0
call Jy(Wj[(nq)],1093677096,Oj)
call ma(Nj[nq])
call PauseTimer(Xj[nq])
endif
call SetItemCharges(Rj[nq],ht(Tj[nq]))
endfunction
function JB takes nothing returns nothing
call lB(((LoadInteger(f7,3,GetHandleId(GetExpiredTimer())))),.2)
endfunction
function KB takes integer nq returns nothing
if Tj[nq]<=.0 then
set Tj[nq]=Sj[nq]
call ma(Nj[nq])
call SetItemCharges(Rj[nq],ht(Tj[nq]))
call TimerStart(Xj[nq],.2,true,Qj)
else
call lB(nq,Vj[nq])
endif
endfunction
function LB takes nothing returns nothing
local integer t=GetUnitUserData(GetSpellAbilityUnit())
if GetSpellAbilityId()==1093677096 then
call My(hj[t])
endif
endfunction
function MB takes nothing returns nothing
set Qj=function JB
call TriggerAddCondition(Pj,Filter(function LB))
endfunction
function NB takes nothing returns nothing
local integer m=Lj
local integer nq=Is(m,2)
call Zs("AbilMultishot "+I2S(nq)+" is created.")
endfunction
function OB takes integer nq,integer m returns nothing
local integer PB=pj[Nj[nq]]
call la(m,Nj[nq])
if PB>=nm[Mj[nq]]then
call La(m,Nj[nq],Yj)
endif
if PB>=om[Mj[nq]]then
call Na(m,Nj[nq],Zj)
endif
endfunction
function QB takes nothing returns nothing
local integer m=Lj
local integer nq=ls(m,3)
set fk[nq]=ly(m)
call Zs("AbilLightningAttack "+I2S(nq)+" is created.")
endfunction
function RB takes integer nq,integer dr returns boolean
if qa(Nj[nq],dr,hj[Nj[nq]])then
if pj[Nj[nq]]==om[Mj[nq]]and(sh[(dr)]!=0)then
call qa(Nj[nq],dr,fk[nq])
endif
return true
endif
return false
endfunction
function SB takes integer nq returns nothing
set ok[nq]=CreateTimer()
call SaveInteger(f7,3,GetHandleId((ok[nq])),(nq))
endfunction
function TB takes integer nq returns nothing
call Bt(ok[nq])
set ok[nq]=null
endfunction
function UB takes nothing returns nothing
local integer m=Lj
local integer nq=Js(m,4)
set hk[nq]=CreateUnit(Le[Pi[m]],1932537904,Wi[m],Xi[m],.0)
call SetUnitPathing(hk[nq],false)
call SetUnitFlyHeight(hk[nq],Ui[m],.0)
call SetUnitUserData(hk[nq],m)
call SB(nq)
set nk[nq]=TA(m,1227894845)
call Zs("AbilEnhancedWeapon "+I2S(nq)+" is created.")
endfunction
function VB takes nothing returns nothing
local integer nq=(LoadInteger(f7,3,GetHandleId(GetExpiredTimer())))
set kk[nq]=.0
set ik[nq]=null
call ma(Nj[nq])
endfunction
function WB takes integer nq,integer m returns nothing
if pj[Nj[nq]]>=nm[Mj[nq]]and ik[nq]==og[m]then
set kk[nq]=kk[nq]+.25
if kk[nq]>1. then
set kk[nq]=1.
endif
call TimerStart(ok[nq],3.,false,function VB)
else
set kk[nq]=.0
endif
set ik[nq]=og[m]
call na(Nj[nq],m)
endfunction
function XB takes nothing returns nothing
local integer nq=(LoadInteger(f7,3,GetHandleId(GetExpiredTimer())))
if UnitAlive(ik[nq])then
call Py(gk[nq],ik[nq])
set mk[nq]=mk[nq]+1
if mk[nq]==4 then
call PauseTimer(ok[nq])
endif
else
call PauseTimer(ok[nq])
endif
endfunction
function YB takes integer nq,integer m returns nothing
set ik[nq]=og[m]
call Py(gk[nq],ik[nq])
set mk[nq]=1
call TimerStart(ok[nq],.125,true,function XB)
endfunction
function ZB takes integer nq,integer m returns nothing
if jk[nq]then
call YB(nq,m)
else
call WB(nq,m)
endif
endfunction
function s__AbilEnhancedWeapon_filterToggleModeSelected takes nothing returns nothing
endfunction
function db takes integer nq returns nothing
set rk[nq]=CreateTimer()
call SaveInteger(f7,3,GetHandleId((rk[nq])),(nq))
endfunction
function eb takes integer nq returns nothing
call Bt(rk[nq])
set rk[nq]=null
endfunction
function fb takes nothing returns nothing
local integer nq=(LoadInteger(f7,3,GetHandleId(GetExpiredTimer())))
call sw(qk[nq],pk[nq])
call ww(qk[nq],I2S(pk[nq]),Qi[Nj[nq]])
endfunction
function gb takes nothing returns nothing
local integer m=Lj
local integer nq=Ks(m,5)
set qk[nq]=Pi[m]
call db(nq)
call TimerStart(rk[nq],1.,true,function fb)
call Zs("AbilGoldMining "+I2S(nq)+" is created.")
endfunction
function hb takes integer nq returns nothing
set Ek[nq]=CreateTimer()
call SaveInteger(f7,3,GetHandleId((Ek[nq])),(nq))
endfunction
function ib takes integer nq returns nothing
call Bt(Ek[nq])
set Ek[nq]=null
endfunction
function jb takes nothing returns nothing
local integer m=Lj
local integer nq=Ls(m,6)
call hb(nq)
set Ak[nq]=ZA(Nj[nq],zk[nq],sk,tk,uk,true)
set Dk[nq]=Pi[m]
call UnitAddAbility(Qi[Nj[nq]],1093677127)
call TimerStart(Ek[nq],5.,true,vk)
call Zs("AbilExpGenerator "+I2S(nq)+" is created.")
endfunction
function kb takes integer nq returns nothing
if bk[nq]==null then
set bk[nq]=zt(Le[Dk[nq]],"Abilities\\Spells\\Other\\Aneu\\AneuCaster.mdx",Bk[nq],"overhead")
endif
endfunction
function mb takes integer nq returns nothing
if bk[nq]!=null then
call DestroyEffect(bk[nq])
set bk[nq]=null
endif
endfunction
function nb takes integer nq,integer xu returns nothing
if xk[nq]==0 then
call ShowImage(Ak[nq],false)
call UnitRemoveAbility(Qi[Nj[nq]],mm[Mj[nq]])
elseif xk[nq]==2 then
call UnitRemoveAbility(Qi[Nj[nq]],1093677111)
else
set Bk[nq]=null
call mb(nq)
call UnitRemoveAbility(Qi[Nj[nq]],1093677138)
endif
if xu==0 then
call ShowImage(Ak[nq],IsUnitSelected(Qi[Nj[nq]],Le[Dk[nq]]))
call UnitAddAbility(Qi[Nj[nq]],mm[Mj[nq]])
elseif xu==2 then
call UnitAddAbility(Qi[Nj[nq]],1093677111)
else
set Ck[nq]=xk[nq]
call UnitAddAbility(Qi[Nj[nq]],1093677138)
endif
set xk[nq]=xu
endfunction
function ob takes integer nq,integer t returns nothing
if Bk[nq]!=Qi[t]then
if xk[nq]==1 then
call DestroyEffect(bk[nq])
else
call nb(nq,1)
endif
set Bk[nq]=Qi[t]
set bk[nq]=zt(Le[Dk[nq]],"Abilities\\Spells\\Other\\Aneu\\AneuCaster.mdx",Qi[t],"overhead")
endif
endfunction
function pb takes nothing returns nothing
local integer t=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),V))and Ti[t]and kp[wj[t]]==91 then
call ob(wj[t],Gk)
endif
endfunction
function qb takes integer nq,integer t returns nothing
if not Ti[t]then
call mw(Dk[nq],M7,"|cffcc3333Error|r: this tower is constructing.")
return
elseif not ro[Si[t]]then
call mw(Dk[nq],M7,"|cffcc3333Error|r: this tower does not use experience.")
return
endif
set Gk=t
call GroupEnumUnitsSelected(bj_lastCreatedGroup,Le[Dk[nq]],Fk)
endfunction
function rb takes nothing returns nothing
local integer t=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),V))and Ti[t]and kp[wj[t]]==91 and xk[(wj[t])]==Ik then
call nb((wj[t]),lk)
endif
endfunction
function sb takes integer nq,integer id returns nothing
if id==mm[Mj[nq]]then
set lk=2
elseif id==1093677111 then
set lk=0
elseif id==1093677138 then
set lk=Ck[nq]
else
return
endif
set Ik=xk[nq]
call GroupEnumUnitsSelected(bj_lastCreatedGroup,Le[Dk[nq]],Hk)
endfunction
function tb takes integer nq returns boolean
if UnitAlive(Bk[nq])then
return true
endif
call nb(nq,Ck[nq])
return false
endfunction
function ub takes nothing returns nothing
local integer t=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),V))and Ti[t]and ro[Si[t]]then
call za(t,Kk)
endif
endfunction
function vb takes integer nq returns nothing
set Kk=yk[nq]
call GroupEnumUnitsInRange(bj_lastCreatedGroup,Wi[Nj[nq]],Xi[Nj[nq]],zk[nq],Jk)
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\NightElf\\MoonWell\\MoonWellCasterArt.mdx"),(Qi[Nj[nq]]),("origin")))
endfunction
function wb takes integer nq returns nothing
call za((GetUnitUserData(Bk[nq])),ak[nq])
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\NightElf\\MoonWell\\MoonWellCasterArt.mdx"),(Qi[Nj[nq]]),("origin")))
endfunction
function xb takes integer nq returns nothing
call xw(Dk[nq],ck[nq])
call Aw(Dk[nq],I2S(ck[nq]),Qi[Nj[nq]])
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\NightElf\\MoonWell\\MoonWellCasterArt.mdx"),(Qi[Nj[nq]]),("origin")))
endfunction
function yb takes integer nq returns nothing
if xk[nq]==0 then
call ShowImage(Ak[nq],true)
elseif xk[nq]==1 and UnitAlive(Bk[nq])then
call kb(nq)
endif
endfunction
function zb takes integer nq returns nothing
if xk[nq]==0 then
call ShowImage(Ak[nq],false)
elseif xk[nq]==1 and UnitAlive(Bk[nq])then
call mb(nq)
endif
endfunction
function Ab takes nothing returns nothing
local integer nq=(LoadInteger(f7,3,GetHandleId(GetExpiredTimer())))
if xk[nq]==1 and tb(nq)then
call wb(nq)
elseif xk[nq]==0 then
call vb(nq)
else
call xb(nq)
endif
endfunction
function ab takes nothing returns nothing
local integer nq=(LoadInteger(f7,3,GetHandleId(GetExpiredTimer())))
if xk[nq]==1 and tb(nq)then
call wb(nq)
elseif xk[nq]==0 then
call vb(nq)
else
call xb(nq)
endif
if pj[Nj[nq]]==pm[Mj[nq]]then
call TimerStart(Ek[nq],3.,true,wk)
endif
endfunction
function Bb takes nothing returns nothing
set vk=function ab
set wk=function Ab
set Jk=Filter(function ub)
set Fk=Filter(function pb)
set Hk=Filter(function rb)
endfunction
function bb takes nothing returns nothing
local integer m=Lj
local integer nq=Ms(m,7)
call Zs("AbilBloodlust "+I2S(nq)+" is created.")
endfunction
function Cb takes integer nq returns nothing
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\Undead\\DeathPact\\DeathPactTarget.mdx"),(Qi[Nj[nq]]),("overhead")))
set Nk[nq]=Nk[nq]+.5
if mj[Nj[nq]]==nm[Mj[nq]]then
set Nk[nq]=Nk[nq]+5.
elseif mj[Nj[nq]]==om[Mj[nq]]then
set Lk[nq]=2.
endif
endfunction
function cb takes integer nq,integer m returns nothing
set Mk[nq]=(1.-Uw(m))*Lk[nq]
if oa(Nj[nq],m)then
set Mk[nq]=.0
else
set Mk[nq]=.0
call ma(Nj[nq])
endif
endfunction
function Db takes integer nq returns nothing
set Tk[nq]=CreateTimer()
call SaveInteger(f7,3,GetHandleId((Tk[nq])),(nq))
endfunction
function Eb takes integer nq returns nothing
call Bt(Tk[nq])
set Tk[nq]=null
endfunction
function Fb takes integer nq returns nothing
set Uk[nq]=CreateTimer()
call SaveInteger(f7,3,GetHandleId((Uk[nq])),(nq))
endfunction
function Gb takes integer nq returns nothing
call Bt(Uk[nq])
set Uk[nq]=null
endfunction
function Hb takes nothing returns nothing
local integer m=Lj
local integer nq=Ns(m,8)
call Db(nq)
call Fb(nq)
call Zs("AbilShockwave "+I2S(nq)+" is created.")
endfunction
function Ib takes integer nq returns nothing
if pj[Nj[nq]]>=nm[Mj[nq]]then
set Sk[nq]=Sk[nq]+.1
if Sk[nq]>1. then
set Sk[nq]=1.
endif
call TimerStart(Tk[nq],1.5,false,Ok)
call ma(Nj[nq])
endif
endfunction
function lb takes integer nq,real cx,real cy returns nothing
call Oy(hj[Nj[nq]],cx,cy)
if pj[Nj[nq]]==om[Mj[nq]]then
set Qk[nq]=cx
set Rk[nq]=cy
call TimerStart(Uk[nq],.4,false,Pk)
endif
endfunction
function Jb takes nothing returns nothing
local integer nq=(LoadInteger(f7,3,GetHandleId(GetExpiredTimer())))
call Oy(hj[Nj[nq]],Qk[nq],Rk[nq])
endfunction
function Kb takes nothing returns nothing
local integer nq=(LoadInteger(f7,3,GetHandleId(GetExpiredTimer())))
set Sk[nq]=.0
call ma(Nj[nq])
endfunction
function Lb takes nothing returns nothing
set Ok=function Kb
set Pk=function Jb
endfunction
function Mb takes integer nq returns nothing
set km[nq]=CreateTimer()
call SaveInteger(f7,3,GetHandleId((km[nq])),(nq))
endfunction
function Nb takes integer nq returns nothing
call Bt(km[nq])
set km[nq]=null
endfunction
function Ob takes integer nq,integer m returns nothing
local integer PB=pj[Nj[nq]]
set dm[nq]=dm[nq]-30
call SetItemCharges(im[nq],dm[nq])
call gx(m,em[nq],"Effects\\RestoreHealthGreen.mdx","origin")
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\NightElf\\MoonWell\\MoonWellCasterArt.mdx"),(Qi[Nj[nq]]),("origin")))
if PB>=nm[Mj[nq]]then
call Ta(m,Nj[nq],25)
if PB==om[Mj[nq]]then
call Va(m,Nj[nq],.1*vg[m])
endif
endif
endfunction
function Pb takes integer nq returns nothing
local integer a=0
local real Qb=.0
local real gv=.0
local integer sr=0
local boolean Rb=true
local integer Sb=af[hm[nq]]
loop
exitwhen a>W8[Sb]
if sg[(U8[V8[(Sb)]+(a)])]then
set gv=Uw((U8[V8[(Sb)]+(a)]))
if Rb or gv<Qb then
set Qb=gv
set sr=(U8[V8[(Sb)]+(a)])
set Rb=false
endif
endif
set a=a+1
endloop
if sr!=0 and Qb<=fm[nq]then
call Ob(nq,sr)
endif
endfunction
function Tb takes nothing returns nothing
local integer nq=(LoadInteger(f7,3,GetHandleId(GetExpiredTimer())))
local integer gv=dm[nq]
set jm[nq]=jm[nq]+1
if jm[nq]==3 then
set jm[nq]=0
if dm[nq]<50 and yw(hm[nq],Xk)then
set dm[nq]=dm[nq]+Xk
if dm[nq]>50 then
set dm[nq]=50
endif
set gv=dm[nq]-gv
call SetItemCharges(im[nq],dm[nq])
call Aw(hm[nq],I2S(gv),Qi[Nj[nq]])
endif
endif
if dm[nq]>=30 and gm[nq]then
call Pb(nq)
endif
endfunction
function Ub takes nothing returns nothing
local integer m=Lj
local integer nq=Os(m,9)
set hm[nq]=Pi[m]
set im[nq]=TA(m,1227894843)
call Mb(nq)
call TimerStart(km[nq],1.,true,function Tb)
call Zs("AbilReplenishLife "+I2S(nq)+" is created.")
endfunction
function Vb takes integer nq,integer Wb returns nothing
if Wb==Vk or Wb==Wk then
set gm[nq]=not gm[nq]
endif
endfunction
function Xb takes integer T,integer Yb,integer b1,integer b2,integer b3,code cB returns nothing
set mm[(T)]=Yb
set nm[(T)]=b1
set om[(T)]=b2
set pm[(T)]=b3
set qm[(T)]=CreateTrigger()
call TriggerAddCondition(qm[(T)],Filter(cB))
endfunction
function dC takes nothing returns nothing
local integer nq=Ps()
set Qi[nq]=x7
set Pi[nq]=y7
set Wi[nq]=z7
set Xi[nq]=A7
call SA(nq,0)
call St(xf[Pi[nq]],nq)
call SetUnitUserData(Qi[nq],nq)
call Zs("Wall "+I2S(nq)+" is created.")
endfunction
function eC takes integer nq returns nothing
call sw(Pi[nq],Yi[nq])
call ww(Pi[nq],I2S(Yi[nq]),Qi[nq])
call jr(nq)
call Zs("Wall "+I2S(nq)+" is deleted.")
endfunction
function fC takes nothing returns nothing
local integer nq=Qs()
set Qi[nq]=x7
set Pi[nq]=y7
set Wi[nq]=z7
set Xi[nq]=A7
call SA(nq,1)
call St(xf[Pi[nq]],nq)
call SetUnitUserData(Qi[nq],nq)
call Zs("Arrow "+I2S(nq)+" is created.")
endfunction
function gC takes nothing returns nothing
local integer nq=Qs()
call SA(nq,1)
call Zs("Arrow "+I2S(nq)+" is created.")
endfunction
function hC takes nothing returns nothing
local integer nq=Rs()
set Qi[nq]=x7
set Pi[nq]=y7
set Wi[nq]=z7
set Xi[nq]=A7
call SA(nq,2)
call St(xf[Pi[nq]],nq)
call SetUnitUserData(Qi[nq],nq)
call Zs("Burrow "+I2S(nq)+" is created.")
endfunction
function iC takes nothing returns nothing
local integer nq=Rs()
call SA(nq,2)
call Zs("Burrow "+I2S(nq)+" is created.")
endfunction
function jC takes nothing returns nothing
local integer nq=Ss()
set Qi[nq]=x7
set Pi[nq]=y7
set Wi[nq]=z7
set Xi[nq]=A7
call SA(nq,3)
call St(xf[Pi[nq]],nq)
call SetUnitUserData(Qi[nq],nq)
call Zs("Lightning "+I2S(nq)+" is created.")
endfunction
function kC takes nothing returns nothing
local integer nq=Ss()
call SA(nq,3)
call Zs("Lightning "+I2S(nq)+" is created.")
endfunction
function mC takes nothing returns nothing
local integer nq=Ts()
set Qi[nq]=x7
set Pi[nq]=y7
set Wi[nq]=z7
set Xi[nq]=A7
call SA(nq,4)
call St(xf[Pi[nq]],nq)
call SetUnitUserData(Qi[nq],nq)
call Zs("Burst "+I2S(nq)+" is created.")
endfunction
function nC takes nothing returns nothing
local integer nq=Ts()
call SA(nq,4)
call Zs("Burst "+I2S(nq)+" is created.")
endfunction
function oC takes nothing returns nothing
local integer nq=Us()
set Qi[nq]=x7
set Pi[nq]=y7
set Wi[nq]=z7
set Xi[nq]=A7
call SA(nq,5)
call St(xf[Pi[nq]],nq)
call SetUnitUserData(Qi[nq],nq)
call Zs("GoldMine "+I2S(nq)+" is created.")
endfunction
function pC takes nothing returns nothing
local integer nq=Us()
call SA(nq,5)
call Zs("GoldMine "+I2S(nq)+" is created.")
endfunction
function qC takes nothing returns nothing
local integer nq=Vs()
set Qi[nq]=x7
set Pi[nq]=y7
set Wi[nq]=z7
set Xi[nq]=A7
call SA(nq,7)
call St(xf[Pi[nq]],nq)
call SetUnitUserData(Qi[nq],nq)
call Zs("FoE "+I2S(nq)+" is created.")
endfunction
function rC takes nothing returns nothing
local integer nq=Vs()
call SA(nq,7)
call Zs("FoE "+I2S(nq)+" is created.")
endfunction
function sC takes nothing returns nothing
local integer nq=Ws()
set Qi[nq]=x7
set Pi[nq]=y7
set Wi[nq]=z7
set Xi[nq]=A7
call SA(nq,8)
call St(xf[Pi[nq]],nq)
call SetUnitUserData(Qi[nq],nq)
call Zs("Blood "+I2S(nq)+" is created.")
endfunction
function tC takes nothing returns nothing
local integer nq=Ws()
call SA(nq,8)
call Zs("Blood "+I2S(nq)+" is created.")
endfunction
function uC takes nothing returns nothing
local integer nq=Xs()
set Qi[nq]=x7
set Pi[nq]=y7
set Wi[nq]=z7
set Xi[nq]=A7
call SA(nq,9)
call St(xf[Pi[nq]],nq)
call SetUnitUserData(Qi[nq],nq)
call Zs("Wave "+I2S(nq)+" is created.")
endfunction
function vC takes nothing returns nothing
local integer nq=Xs()
call SA(nq,9)
call Zs("Wave "+I2S(nq)+" is created.")
endfunction
function wC takes nothing returns nothing
local integer nq=ms()
set Qi[nq]=x7
set Pi[nq]=y7
set Wi[nq]=z7
set Xi[nq]=A7
call SA(nq,6)
call St(xf[Pi[nq]],nq)
call SetUnitUserData(Qi[nq],nq)
call Zs("FoL "+I2S(nq)+" is created.")
endfunction
function xC takes nothing returns nothing
local integer nq=ms()
call SA(nq,6)
call Zs("FoL "+I2S(nq)+" is created.")
endfunction
function yC takes integer T,integer zC,integer AC,integer aC,integer BC,string ru returns nothing
set V4[T]=rq()
set cm[V4[T]]=zC
set Dm[V4[T]]=AC
set Em[V4[T]]=aC
set Fm[V4[T]]=BC
set Gm[V4[T]]=cj[S4[zC]]
set Hm[V4[T]]=ru
endfunction
function bC takes integer T,integer qu,integer CC,integer cC,integer DC,integer EC,code FC,integer GC,integer HC,integer IC,integer lC,integer JC,code KC returns nothing
set Im[lm[V4[T]]]=qu
set Jm[Km[V4[T]]]=cC
set Lm[Mm[V4[T]]]=CC
set Nm[Om[V4[T]]]=DC
set Pm[Qm[V4[T]]]=EC
if FC!=null then
set Rm[Sm[V4[T]]]=CreateTrigger()
call TriggerAddAction(Rm[Sm[V4[T]]],FC)
endif
set Im[lm[V4[T]]+1]=GC
set Jm[Km[V4[T]]+1]=IC
set Lm[Mm[V4[T]]+1]=HC
set Nm[Om[V4[T]]+1]=lC
set Pm[Qm[V4[T]]+1]=JC
if KC!=null then
set Rm[Sm[V4[T]]+1]=CreateTrigger()
call TriggerAddAction(Rm[Sm[V4[T]]+1],KC)
endif
endfunction
function MC takes integer p,integer id returns boolean
local string Gw=GetObjectName(id)
local integer T
local integer lw
if SubString(Gw,0,vm)=="AbilityUpgrade" then
set T=S2I(SubString(Gw,wm,xm))
set lw=S2I(SubString(Gw,ym,zm))
set Gw=SubString(Gw,Am,StringLength(Gw))
if zw(p,Pm[Qm[V4[T]]+lw])then
set Cm=p
set q9[r9[(Kf[Lf[p]+T])]+(lw)]=(true)
call TriggerExecute(Rm[Sm[V4[T]]+lw])
call SetPlayerAbilityAvailable(Le[p],id,false)
call SetPlayerAbilityAvailable(Le[p],Jm[Km[V4[T]]+lw],true)
call SetPlayerTechResearched(Le[p],Nm[Om[V4[T]]+lw],1)
call mw(p,O7,"|cff33cc33Research complete|r:"+Gw+".")
return true
endif
endif
return false
endfunction
function NC takes integer nq,integer p returns nothing
call UnitAddAbility(vf[p],Dm[nq])
call UnitAddAbility(vf[p],Em[nq])
call UnitAddAbility(vf[p],Im[lm[nq]])
call UnitAddAbility(vf[p],Jm[Km[nq]])
call UnitAddAbility(vf[p],Im[lm[nq]+1])
call UnitAddAbility(vf[p],Jm[Km[nq]+1])
call UnitAddAbility(sf[p],Fm[nq])
endfunction
function OC takes integer nq,player p,boolean PC returns nothing
call SetPlayerAbilityAvailable(p,Gm[nq],PC)
call SetPlayerAbilityAvailable(p,Fm[nq],PC)
call SetPlayerAbilityAvailable(p,Lm[Mm[nq]],PC)
call SetPlayerAbilityAvailable(p,Lm[Mm[nq]+1],PC)
call SetPlayerAbilityAvailable(p,Em[nq],PC)
call SetPlayerAbilityAvailable(p,Dm[nq],not PC)
endfunction
function QC takes integer nq,integer p returns nothing
call OC(Mf[p],Le[p],false)
call OC(nq,Le[p],true)
call jw(p,nq)
endfunction
function RC takes integer p,integer id returns boolean
local string Gw=GetObjectName(id)
local integer T
if SubString(Gw,0,am)=="HeroAbility" then
set T=S2I(SubString(Gw,Bm,bm))
call QC(V4[T],p)
return true
endif
return false
endfunction
function SC takes integer T,integer TC,real UC,real VC,integer WC,integer XC,integer YC,integer ZC,integer dc,real ec,integer fc,integer gc,integer hc,real ic,integer jc,integer kc returns nothing
set W4[T]=qq()
set hn[W4[T]]=TC
set in[W4[T]]=UC
if VC==5 then
set jn[W4[T]]=UC/ 180.*.25
else
set jn[W4[T]]=VC*.25
endif
set dn[W4[T]]=WC
set nn[W4[T]]=XC
set on[W4[T]]=YC
set pn[W4[T]]=ZC
set qn[W4[T]]=dc
set rn[W4[T]]=ec
set sn[W4[T]]=fc
set tn[W4[T]]=gc
set un[W4[T]]=hc
set vn[W4[T]]=ic
set wn[W4[T]]=jc
set xn[W4[T]]=kc
endfunction
function mc takes integer T,integer nc,integer oc,integer pc,integer qc,boolean rc,boolean sc,real tc,code cB returns nothing
set Wm[W4[T]]=nc
set Xm[W4[T]]=oc
set Ym[W4[T]]=pc
set gn[W4[T]]=qc
set en[W4[T]]=rc
set fn[W4[T]]=sc
set Zm[W4[T]]=tc
set X4[T]=CreateTrigger()
call TriggerAddCondition(X4[T],Filter(cB))
endfunction
function vc takes integer T,integer qu,integer CC,integer cC,integer DC,integer EC,code FC,integer GC,integer HC,integer IC,integer lC,integer JC,code KC,integer wc,integer xc,integer yc,integer zc,integer Ac,code ac returns nothing
set Y4[T]=pq()
set bn[Cn[Y4[T]]]=qu
set bn[Cn[Y4[T]]+1]=GC
set bn[Cn[Y4[T]]+2]=wc
set cn[Dn[Y4[T]]]=cC
set cn[Dn[Y4[T]]+1]=IC
set cn[Dn[Y4[T]]+2]=yc
set En[Fn[Y4[T]]]=CC
set En[Fn[Y4[T]]+1]=HC
set En[Fn[Y4[T]]+2]=xc
set Gn[Hn[Y4[T]]]=DC
set Gn[Hn[Y4[T]]+1]=lC
set Gn[Hn[Y4[T]]+2]=zc
set In[ln[Y4[T]]]=EC
set In[ln[Y4[T]]+1]=JC
set In[ln[Y4[T]]+2]=Ac
if FC!=null then
set Jn[Kn[Y4[T]]]=CreateTrigger()
call TriggerAddAction(Jn[Kn[Y4[T]]],FC)
endif
if KC!=null then
set Jn[Kn[Y4[T]]+1]=CreateTrigger()
call TriggerAddAction(Jn[Kn[Y4[T]]+1],KC)
endif
if ac!=null then
set Jn[Kn[Y4[T]]+2]=CreateTrigger()
call TriggerAddAction(Jn[Kn[Y4[T]]+2],ac)
endif
endfunction
function bc takes integer T,integer XC,boolean Cc,integer TC,boolean cc,boolean Dc,integer Ec,integer Fc,integer Gc,integer Hc,integer Ic,integer lc returns nothing
set no[(T)]=XC
set oo[(T)]=Cc
set po[(T)]=TC
set qo[(T)]=cc
set ro[(T)]=Dc
set so[(T)]=Ec
set to[(T)]=Fc
set uo[(T)]=Gc
set vo[(T)]=Hc
set wo[(T)]=Ic
set xo[(T)]=lc
endfunction
function Jc takes integer T,real Kc,real Lc,integer Mc,boolean Nc returns nothing
set io[(T)]=Kc
set jo[(T)]=Lc
set ko[(T)]=Mc
set mo[(T)]=Nc
endfunction
function Oc takes integer T,code Pc,code Qc returns nothing
set yo[(T)]=CreateTrigger()
call TriggerAddCondition(yo[(T)],Filter(Pc))
if Qc!=null then
set zo[(T)]=CreateTrigger()
call TriggerAddCondition(zo[(T)],Filter(Qc))
endif
endfunction
function Rc takes integer T,real Sc,real up,real cd,integer Wv,integer Tc,real ct,integer n,boolean ka,integer Qu,integer Wb returns nothing
set Vn[(T)]=Sc
set Wn[(T)]=up
set Xn[(T)]=cd
set Yn[(T)]=Wv
set Zn[(T)]=l4[Tc]
set do[(T)]=ct
set eo[(T)]=n
set fo[(T)]=ka
set go[(T)]=Qu
set ho[(T)]=Wb
endfunction
function Vc takes integer T,integer Wc,integer Xc,integer Yc,integer Zc,integer BC,integer dD,integer eD,string ru returns nothing
set d7[T]=oq()
set On[d7[T]]=Wc
set Pn[Qn[d7[T]]]=V4[Xc]
set Pn[Qn[d7[T]]+1]=V4[Yc]
set Pn[Qn[d7[T]]+2]=V4[Zc]
set Rn[d7[T]]=BC
set Sn[d7[T]]=dD
set Tn[d7[T]]=eD
set Un[d7[T]]=ru
endfunction
function InitHeroData takes nothing returns nothing
call Vc(1,6,1,2,3,1093677365,1093677366,1093677367,null)
endfunction
function fD takes integer nq,integer p returns nothing
local integer a=0
set Jf[p]=nq
call UnitRemoveAbility(vf[p],Sn[nq])
call UnitRemoveAbility(vf[p],Tn[nq])
call UnitRemoveAbility(sf[p],1093677364)
call UnitAddAbility(sf[p],Rn[nq])
loop
call NC(Pn[Qn[nq]+a],p)
set a=a+1
exitwhen a==3
endloop
set a=0
loop
call UnitAddAbility(vf[p],bn[Cn[Y4[On[nq]]]+a])
call UnitAddAbility(vf[p],cn[Dn[Y4[On[nq]]]+a])
set a=a+1
exitwhen a==3
endloop
call QC(Pn[Qn[nq]],p)
endfunction
function InitGlobals takes nothing returns nothing
endfunction
function gD takes nothing returns nothing
local string s
set n7=n7+1
if n7==60 then
set n7=0
set o7=o7+1
endif
set q7=q7-1
if q7==0 then
set p7=p7+1
call TriggerExecute(s7)
set q7=30
endif
set s="Game time: |cffffffff"
if o7<$A then
set s=s+"0"
endif
set s=s+I2S(o7)+":"
if n7<$A then
set s=s+"0"
endif
set s=s+I2S(n7)
call MultiboardSetItemValue(o8,s)
set s="Wave "+I2S(p7)+" in "
if Qe[m7]and q7<4 then
set s=s+"|cffcc3333"+I2S(q7)+"|r"
call StartSound(Q7)
else
set s=s+I2S(q7)
endif
set s=s+" second"
if q7!=1 then
set s=s+"s"
endif
call MultiboardSetTitleText(Z7,s)
endfunction
function hD takes trigger t,string s,boolean iD returns nothing
local integer i=0
loop
call TriggerRegisterPlayerChatEvent(t,Player(i),s,iD)
set i=i+1
exitwhen i==$C
endloop
endfunction
function jD takes nothing returns nothing
local integer p=(i7[GetPlayerId((GetTriggerPlayer()))])
if Jf[p]!=0 then
set hg=p
call TriggerEvaluate(X4[On[Jf[p]]])
set Nf[p]=(l8[J8[(yf[p])]+(K8[yf[p]])])
endif
endfunction
function kD takes nothing returns nothing
local integer i=0
local integer a
local integer p
loop
exitwhen i>B8[k7]
set p=(A8[a8[(k7)]+(i)])
set a=0
loop
exitwhen a>K8[yf[p]]
call Nw((l8[J8[(yf[p])]+(a)]))
set a=a+1
endloop
set Nf[p]=0
set K8[yf[p]]=-1
call gw(p)
call sw(p,Ze[p])
call mw(p,P7,"You receive |cffcccc33"+I2S(Ze[p])+"|r gold.")
set i=i+1
endloop
endfunction
function InitTrig_SendMinions takes nothing returns nothing
call TriggerAddAction(s7,function kD)
endfunction
function mD takes nothing returns nothing
local integer p=ig
local integer m
local integer a=0
local real ox
local real array nD
local real array vy
if vw(p,Cf[p])then
set bf[p]=bf[p]+1
set Cf[p]=it(Cf[p]*1.075)
loop
set ox=(bd[Cd[(dn[cf[Df[p]+a]])]+(bf[p])])
set nD[a]=in[cf[Df[p]+a]]*ox-kn[cf[Df[p]+a]]
set kn[cf[Df[p]+a]]=kn[cf[Df[p]+a]]+nD[a]
set vy[a]=jn[cf[Df[p]+a]]*ox-mn[cf[Df[p]+a]]
set mn[cf[Df[p]+a]]=mn[cf[Df[p]+a]]+vy[a]
set a=a+1
exitwhen a==32
endloop
set a=0
loop
exitwhen a>m9[af[p]]
set m=(j9[k9[(af[p])]+(a)])
call Xw(m,nD[jg[m]],vy[jg[m]])
set a=a+1
endloop
call SetItemCharges(Vf[p],bf[p])
endif
call SetItemCharges(Wf[p],Cf[p]+1)
endfunction
function InitTrig_UpgradeMinionHP takes nothing returns nothing
call TriggerAddAction(t7,function mD)
endfunction
function oD takes nothing returns nothing
local integer Sb=yf[u7]
local integer a=K8[Sb]
loop
exitwhen a==-1
call Cr((l8[J8[(Sb)]+(a)]))
set a=a-1
endloop
call Zs("forces of player "+I2S(u7)+" is flushed.")
endfunction
function pD takes nothing returns nothing
local integer Sb=zf[u7]
local integer a=Q8[Sb]
loop
exitwhen a==-1
call Ow((O8[P8[(Sb)]+(a)]))
set a=a-1
endloop
call Zs("spawned of player "+I2S(u7)+" is flushed.")
endfunction
function qD takes nothing returns nothing
local integer Sb=Af[u7]
local integer a=W8[Sb]
loop
exitwhen a==-1
call ry((U8[V8[(Sb)]+(a)]))
set a=a-1
endloop
call Zs("minions of player "+I2S(u7)+" is flushed.")
endfunction
function rD takes nothing returns nothing
local integer Sb=Bf[u7]
local integer a=f9[Sb]
loop
exitwhen a==-1
call py((d9[e9[(Sb)]+(a)]))
set a=a-1
endloop
call Zs("dead of player "+I2S(u7)+" is flushed.")
endfunction
function sD takes nothing returns nothing
local integer Sb=xf[u7]
local integer a=F8[Sb]
loop
exitwhen a==-1
call va((D8[E8[(Sb)]+(a)]))
set a=a-1
endloop
call Zs("towers of player "+I2S(u7)+" is flushed.")
endfunction
function tD takes nothing returns nothing
local integer p=u7
local integer a=120-1
call Rt(k7,p)
call ExecuteFunc("oD")
call ExecuteFunc("pD")
call ExecuteFunc("qD")
call ExecuteFunc("rD")
call ExecuteFunc("sD")
call cw(p)
call Zs("Player "+I2S(p)+" is flushed.")
endfunction
function InitTrig_FlushPlayer takes nothing returns nothing
call TriggerAddAction(v7,function tD)
endfunction
function uD takes nothing returns nothing
local integer p=(i7[GetPlayerId((GetTriggerPlayer()))])
if Qe[p]then
call It(p)
endif
if B8[k7]==0 then
call lt($A)
endif
endfunction
function InitTrig_LeavesGame takes nothing returns nothing
call TriggerAddAction(w7,function uD)
endfunction
function vD takes nothing returns nothing
local unit u=GetConstructingStructure()
local real x=GetUnitX(u)
local real y=GetUnitY(u)
local integer p=(i7[GetPlayerId((GetOwningPlayer(u)))])
local integer f=Ue[p]
local integer T=GetUnitTypeId(u)-1949315120
if x>fd[f]or x<gd[f]or y>320. or y<f4 then
call mw(p,M7,"|cffcc3333Error|r: you are not allowed to build here.")
call RemoveUnit(u)
set u=null
return
endif
call QA(f,x,y,true)
call Ku(f)
if sd[f]then
call QA(f,x,y,false)
call mw(p,M7,"|cffcc3333Error|r: this tower will hinder minions' movement.")
call RemoveUnit(u)
set u=null
return
endif
if not vw(p,no[(T)])then
call RemoveUnit(u)
set u=null
return
endif
set x7=u
set y7=p
set z7=x
set A7=y
set u=null
call TriggerEvaluate(yo[(T)])
endfunction
function InitTrig_ConstructStart takes nothing returns nothing
call TriggerAddAction(a7,function vD)
endfunction
function wD takes nothing returns nothing
call wa((GetUnitUserData(GetCancelledStructure())))
endfunction
function InitTrig_ConstructCancel takes nothing returns nothing
call TriggerAddAction(B7,function wD)
endfunction
function xD takes nothing returns nothing
call ua((GetUnitUserData(GetConstructedStructure())))
endfunction
function InitTrig_ConstructEnd takes nothing returns nothing
call TriggerAddAction(b7,function xD)
endfunction
function yD takes nothing returns nothing
local integer t=GetUnitUserData(GetTriggerUnit())
if vw(Pi[t],no[(rm[t])])then
set Ti[t]=false
else
call DisableTrigger(c7)
call IssueImmediateOrderById(Qi[t],I)
call EnableTrigger(c7)
endif
endfunction
function InitTrig_UpgradeStart takes nothing returns nothing
call TriggerAddAction(C7,function yD)
endfunction
function zD takes nothing returns nothing
local integer t=GetUnitUserData(GetTriggerUnit())
local integer gv=no[(rm[t])]
set Ti[t]=true
call sw(Pi[t],gv)
call ww(Pi[t],I2S(gv),Qi[t])
endfunction
function InitTrig_UpgradeCancel takes nothing returns nothing
call TriggerAddAction(c7,function zD)
endfunction
function AD takes nothing returns nothing
local integer t=GetUnitUserData(GetTriggerUnit())
call eC(t)
call TriggerEvaluate(zo[(rm[t])])
call ua(t)
endfunction
function InitTrig_UpgradeEnd takes nothing returns nothing
call TriggerAddAction(D7,function AD)
endfunction
function aD takes nothing returns nothing
local integer m=GetUnitUserData(GetLeavingUnit())
if tg[m]then
call jy(m)
endif
endfunction
function InitTrig_LeaveSpawn takes nothing returns nothing
call TriggerAddAction(E7,function aD)
endfunction
function BD takes nothing returns nothing
local integer m=GetUnitUserData(GetEnteringUnit())
local integer p=mg[m]
call Cw(p,tn[xg[m]])
call Pw(m,false)
if p!=kg[m]and Qe[kg[m]]then
call Cw(kg[m],un[xg[m]])
endif
call DestroyEffect(AddSpecialEffect(("Abilities\\Spells\\Undead\\DarkRitual\\DarkRitualTarget.mdx"),((GetUnitX(og[m]))*1.),((GetUnitY(og[m]))*1.)))
call ry(m)
if df[p]==0 then
call It(p)
if bj_isSinglePlayer!=(B8[k7]==0)then
call lt($A)
endif
endif
endfunction
function InitTrig_EnterFinish takes nothing returns nothing
call TriggerAddAction(F7,function BD)
endfunction
function bD takes unit b,integer id returns nothing
local integer p=(i7[GetPlayerId((GetOwningPlayer(b)))])
if id==z9[Of[p]]then
if Re[p]then
call ClearSelection()
call SelectUnit(tf[p],true)
endif
if Qf[p]!=Of[p]then
call yu(p)
endif
return
endif
if id==A9[Pf[p]]then
if Re[p]then
call ClearSelection()
call SelectUnit(tf[p],true)
endif
if Qf[p]!=Pf[p]then
call yu(p)
endif
return
endif
if(id==Rn[Jf[p]]or id==1093677364)and Re[p]then
call ClearSelection()
call SelectUnit(vf[p],true)
endif
endfunction
function CD takes unit b,integer id returns nothing
local integer p=(i7[GetPlayerId((GetOwningPlayer(b)))])
if Fw(p,id)then
return
endif
if id==1093677123 then
if Re[p]then
call ClearSelection()
call SelectUnit(uf[p],true)
endif
if Qf[p]!=Sf[p]then
call tu(p)
endif
return
endif
if id==1093677135 and Re[p]then
call ClearSelection()
call SelectUnit(vf[p],true)
return
endif
if id==1093677144 and Re[p]then
call ClearSelection()
call SelectUnit(wf[p],true)
return
endif
if id==1093677124 then
call yu(p)
endif
endfunction
function cD takes unit b,integer id returns nothing
local integer p=(i7[GetPlayerId((GetOwningPlayer(b)))])
if Hw(p,id,true)then
return
endif
if id==1093677362 then
call tu(p)
return
endif
if id==1093677361 then
call Jw(p)
endif
endfunction
function ED takes unit b,integer id returns nothing
local integer p=(i7[GetPlayerId((GetOwningPlayer(b)))])
if id==Sn[x9[Of[p]]]then
call UnitRemoveAbility(b,Tn[x9[Pf[p]]])
call fD(x9[Of[p]],p)
return
endif
if id==Tn[x9[Pf[p]]]then
call UnitRemoveAbility(b,Sn[x9[Of[p]]])
call fD(x9[Pf[p]],p)
return
endif
if Hw(p,id,false)then
return
endif
if RC(p,id)then
return
endif
call MC(p,id)
endfunction
function FD takes integer oz,integer id,integer sr returns nothing
if id==1093677119 or id==1093677121 then
call wa(oz)
return
endif
if id==1093677129 then
call Da(oz)
return
endif
if id==1093677127 then
call qb(wj[oz],sr)
endif
endfunction
function GD takes integer m,integer id returns nothing
if id==1093677122 then
call Cr(m)
return
endif
if id==cj[S4[1]]then
call kA(rg[m])
endif
endfunction
function HD takes nothing returns nothing
local integer id=GetSpellAbilityId()
local unit u=GetSpellAbilityUnit()
local integer oz=GetUnitUserData(u)
local integer sr=GetUnitUserData(GetSpellTargetUnit())
local integer T=GetUnitTypeId(u)
if T==1112294482 then
call bD(u,id)
elseif T==1112689491 then
call CD(u,id)
elseif T==1112232788 then
call cD(u,id)
elseif T==1095521362 then
call ED(u,id)
elseif(IsUnitType((u),V))then
call FD(oz,id,sr)
elseif(IsUnitType((u),X))then
call GD(oz,id)
endif
set u=null
endfunction
function InitTrig_PlayerAbilityEffect takes nothing returns nothing
call TriggerAddAction(G7,function HD)
endfunction
function ID takes integer oz,integer id returns nothing
if kp[wj[oz]]==91 then
call sb(wj[oz],id)
endif
endfunction
function lD takes integer m,integer id returns nothing
if gp[rg[m]]==65 then
call sA(rg[m],id)
endif
endfunction
function JD takes nothing returns nothing
local integer id=GetSpellAbilityId()
local unit u=GetSpellAbilityUnit()
local integer oz=GetUnitUserData(u)
if(IsUnitType((u),V))then
call ID(oz,id)
elseif(IsUnitType((u),X))then
call lD(oz,id)
endif
set u=null
endfunction
function InitTrig_PlayerAbilityEndcast takes nothing returns nothing
call TriggerAddAction(H7,function JD)
endfunction
function KD takes unit b,integer id returns nothing
local integer p=(i7[GetPlayerId((GetOwningPlayer(b)))])
if id==w4 or id==x4 then
call ow(p)
endif
endfunction
function LD takes integer oz,integer id returns nothing
local integer Qu=wj[oz]
if 1949315120<=id and id<1949315120+Z4 then
set rm[(oz)]=id-1949315120
return
endif
if kp[Qu]==94 then
call Vb(Qu,id)
endif
endfunction
function MD takes nothing returns nothing
local integer id=GetIssuedOrderId()
local unit u=GetOrderedUnit()
local integer oz=GetUnitUserData(u)
local integer T=GetUnitTypeId(u)
if T==1112294482 then
call KD(u,id)
elseif(IsUnitType((u),V))then
call LD(oz,id)
endif
set u=null
endfunction
function InitTrig_PlayerImdtOrders takes nothing returns nothing
call TriggerAddAction(I7,function MD)
endfunction
function ND takes unit b,integer id,real x,real y returns nothing
if id==K or 1949315120<=id and id<1949315120+Z4 then
call SetUnitX(b,x)
call SetUnitY(b,y)
endif
endfunction
function OD takes nothing returns nothing
local integer id=GetIssuedOrderId()
local unit u=GetOrderedUnit()
local integer T=GetUnitTypeId(u)
local real x=GetOrderPointX()
local real y=GetOrderPointY()
local integer PD=Ue[(i7[GetPlayerId((GetOwningPlayer(u)))])]
if T==1112294482 and ed[PD]<=x and x<=Z9[PD]then
call ND(u,id,x,y)
endif
set u=null
endfunction
function InitTrig_PlayerPointOrders takes nothing returns nothing
call TriggerAddAction(l7,function OD)
endfunction
function QD takes nothing returns nothing
local unit u=GetTriggerUnit()
local integer Ea=GetUnitUserData(u)
if(IsUnitType((u),V))then
call fr((Ea))
endif
set u=null
endfunction
function InitTrig_PlayerSelects takes nothing returns nothing
call TriggerAddAction(J7,function QD)
endfunction
function RD takes nothing returns nothing
local unit u=GetTriggerUnit()
local integer SD=GetUnitUserData(u)
if(IsUnitType((u),V))then
call gr((SD))
endif
set u=null
endfunction
function InitTrig_PlayerDeselects takes nothing returns nothing
call TriggerAddAction(K7,function RD)
endfunction
function TD takes integer t,integer m returns nothing
if not(Ie[qg[(m)]])then
call Yq(t,m)
endif
endfunction
function UD takes unit DD,unit VD,real Sc returns nothing
local integer dd=GetUnitUserData(DD)
local integer sr=GetUnitUserData(VD)
call SetWidgetLife(VD,GetWidgetLife(VD)+Sc)
if(IsUnitType((DD),W))then
call TD(dd,sr)
elseif(IsUnitType((DD),Y))then
call rr((dd),(sr))
endif
endfunction
function WD takes nothing returns nothing
if GetEventDamage()>.0 and(GetUnitTypeId((GetEventDamageSource()))!=0)then
call UD(GetEventDamageSource(),GetTriggerUnit(),GetEventDamage())
endif
endfunction
function InitTrig_TakeDamage takes nothing returns nothing
call TriggerAddAction(L7,function WD)
endfunction
function XD takes nothing returns nothing
local integer i=0
local player p
local integer cp
loop
set p=Player(i)
if kt(p)then
set cp=Zv(p,i)
set Pe[cp]=cp
set Of[cp]=e4[1]
set Pf[cp]=e4[2]
endif
set i=i+1
exitwhen i==R
endloop
set bj_isSinglePlayer=w8[j7]==0
set m7=(i7[GetPlayerId((P))])
set i=0
loop
set p=S[i]
call SetPlayerColor(p,U)
call FogModifierStart(CreateFogModifierRect(p,FOG_OF_WAR_VISIBLE,bj_mapInitialPlayableArea,false,true))
set i=i+1
exitwhen i==2
endloop
call TriggerRegisterLeaveRegion(E7,h7,d4)
call TriggerRegisterEnterRegion(F7,g7,d4)
call Zs("Initialization complete!")
set p=null
endfunction
function InitTrig_Main takes nothing returns nothing
call TriggerAddAction(X7,function XD)
endfunction
function YD takes nothing returns nothing
local integer i=0
loop
exitwhen i>B8[k7]
call dw((A8[a8[(k7)]+(i)]))
set i=i+1
endloop
call TriggerExecute(p8)
call gD()
call TimerStart(e7,1.,true,function gD)
endfunction
function dE takes nothing returns integer
set d8=d8+1
call MultiboardSetRowCount(Z7,d8)
return d8-1
endfunction
function eE takes nothing returns nothing
local integer a=0
local integer p
set Z7=CreateMultiboard()
call MultiboardSetTitleTextColor(Z7,$FF,$FF,$FF,$FF)
call MultiboardSetColumnCount(Z7,8)
call MultiboardSetItemsStyle(Z7,true,false)
call MultiboardSetItemsWidth(Z7,e8)
loop
exitwhen a>B8[k7]
set p=(A8[a8[(k7)]+(a)])
set ff[p]=dE()
set gf[p]=MultiboardGetItem(Z7,ff[p],0)
call MultiboardSetItemWidth(gf[p],f8)
call MultiboardSetItemValue(gf[p],Oe[p])
set hf[p]=MultiboardGetItem(Z7,ff[p],2)
call MultiboardSetItemWidth(hf[p],g8)
call MultiboardSetItemValue(hf[p],I2S(Ve[p]))
call MultiboardSetItemValueColor(hf[p],r4,s4,51,$FF)
set jf[p]=MultiboardGetItem(Z7,ff[p],3)
call MultiboardSetItemWidth(jf[p],h8)
call MultiboardSetItemValue(jf[p],I2S(We[p]))
call MultiboardSetItemValueColor(jf[p],t4,u4,v4,$FF)
set kf[p]=MultiboardGetItem(Z7,ff[p],4)
call MultiboardSetItemWidth(kf[p],i8)
call MultiboardSetItemValue(kf[p],"0/"+I2S(Xe[p]))
call MultiboardSetItemValueColor(kf[p],$80,51,51,$FF)
set mf[p]=MultiboardGetItem(Z7,ff[p],5)
call MultiboardSetItemWidth(mf[p],j8)
call MultiboardSetItemValue(mf[p],I2S(Ze[p]))
call MultiboardSetItemValueColor(mf[p],$CC,$80,51,$FF)
set nf[p]=MultiboardGetItem(Z7,ff[p],6)
call MultiboardSetItemWidth(nf[p],k8)
call MultiboardSetItemValue(nf[p],I2S(df[p]))
call MultiboardSetItemValueColor(nf[p],51,$CC,51,$FF)
set of[p]=MultiboardGetItem(Z7,ff[p],7)
call MultiboardSetItemWidth(of[p],m8)
call MultiboardSetItemValue(of[p],"0")
call MultiboardSetItemValueColor(of[p],$CC,51,51,$FF)
set a=a+1
endloop
call dE()
set o8=MultiboardGetItem(Z7,dE(),0)
call MultiboardSetItemWidth(o8,n8)
call MultiboardDisplay(Z7,true)
call MultiboardMinimize(Z7,false)
endfunction
function InitTrig_Multiboard takes nothing returns nothing
call TriggerAddAction(p8,function eE)
endfunction
function fE takes nothing returns nothing
local string s
set q7=q7-1
set s="Game will end in "+I2S(q7)+" second"
if q7>1 then
set s=s+"s"
endif
if q7==0 then
call MultiboardSetTitleText(Z7,"Game is ending...")
call EndGame(true)
else
call MultiboardSetTitleText(Z7,s)
endif
endfunction
function gE takes nothing returns nothing
local integer i=0
loop
exitwhen i>B8[k7]
call It((A8[a8[(k7)]+(i)]))
set i=i+1
endloop
call ClearTextMessages()
call fE()
call TimerStart(e7,1.,true,function fE)
endfunction
function InitTrig_FinishGame takes nothing returns nothing
call TriggerAddAction(q8,function gE)
endfunction
function InitCustomPlayerSlots takes nothing returns nothing
call SetPlayerStartLocation(Player(0),0)
call SetPlayerColor(Player(0),ConvertPlayerColor(0))
call SetPlayerRacePreference(Player(0),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(0),false)
call SetPlayerController(Player(0),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(1),1)
call SetPlayerColor(Player(1),ConvertPlayerColor(1))
call SetPlayerRacePreference(Player(1),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(1),false)
call SetPlayerController(Player(1),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(2),2)
call SetPlayerColor(Player(2),ConvertPlayerColor(2))
call SetPlayerRacePreference(Player(2),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(2),false)
call SetPlayerController(Player(2),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(3),3)
call SetPlayerColor(Player(3),ConvertPlayerColor(3))
call SetPlayerRacePreference(Player(3),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(3),false)
call SetPlayerController(Player(3),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(4),4)
call SetPlayerColor(Player(4),ConvertPlayerColor(4))
call SetPlayerRacePreference(Player(4),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(4),false)
call SetPlayerController(Player(4),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(5),5)
call SetPlayerColor(Player(5),ConvertPlayerColor(5))
call SetPlayerRacePreference(Player(5),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(5),false)
call SetPlayerController(Player(5),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(6),6)
call SetPlayerColor(Player(6),ConvertPlayerColor(6))
call SetPlayerRacePreference(Player(6),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(6),false)
call SetPlayerController(Player(6),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(7),7)
call SetPlayerColor(Player(7),ConvertPlayerColor(7))
call SetPlayerRacePreference(Player(7),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(7),false)
call SetPlayerController(Player(7),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(8),8)
call SetPlayerColor(Player(8),ConvertPlayerColor(8))
call SetPlayerRacePreference(Player(8),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(8),false)
call SetPlayerController(Player(8),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(9),9)
call SetPlayerColor(Player(9),ConvertPlayerColor(9))
call SetPlayerRacePreference(Player(9),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(9),false)
call SetPlayerController(Player(9),MAP_CONTROL_USER)
endfunction
function InitCustomTeams takes nothing returns nothing
call SetPlayerTeam(Player(0),0)
call SetPlayerTeam(Player(1),1)
call SetPlayerTeam(Player(2),1)
call SetPlayerTeam(Player(3),1)
call SetPlayerTeam(Player(4),1)
call SetPlayerTeam(Player(5),1)
call SetPlayerTeam(Player(6),1)
call SetPlayerTeam(Player(7),1)
call SetPlayerTeam(Player(8),1)
call SetPlayerTeam(Player(9),1)
endfunction
function InitAllyPriorities takes nothing returns nothing
call SetStartLocPrioCount(0,2)
call SetStartLocPrio(0,0,8,MAP_LOC_PRIO_LOW)
call SetStartLocPrio(0,1,9,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(1,1)
call SetStartLocPrio(1,0,2,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(2,2)
call SetStartLocPrio(2,0,1,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(2,1,3,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(3,2)
call SetStartLocPrio(3,0,2,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(3,1,4,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(4,2)
call SetStartLocPrio(4,0,3,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(4,1,5,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(5,2)
call SetStartLocPrio(5,0,4,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(5,1,6,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(6,2)
call SetStartLocPrio(6,0,5,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(6,1,7,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(7,2)
call SetStartLocPrio(7,0,6,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(7,1,8,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(8,2)
call SetStartLocPrio(8,0,7,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(8,1,9,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(9,1)
call SetStartLocPrio(9,0,8,MAP_LOC_PRIO_HIGH)
endfunction
function main takes nothing returns nothing
call SetCameraBounds(T7+L,U7+N,6912.-M,1920.-O,T7+L,1920.-O,6912.-M,U7+N)
set bj_mapInitialPlayableArea=GetWorldBounds()
call ExecuteFunc("hE")
call ExecuteFunc("at")
call ExecuteFunc("Nt")
call ExecuteFunc("jz")
call ExecuteFunc("Ha")
call TriggerAddAction(r7,function jD)
call hD(r7,"hero",true)
call TriggerAddAction(s7,function kD)
call TriggerAddAction(t7,function mD)
call TriggerAddAction(v7,function tD)
call TriggerAddAction(w7,function uD)
call TriggerAddAction(a7,function vD)
call TriggerAddAction(B7,function wD)
call TriggerAddAction(b7,function xD)
call TriggerAddAction(C7,function yD)
call TriggerAddAction(c7,function zD)
call TriggerAddAction(D7,function AD)
call TriggerAddAction(E7,function aD)
call TriggerAddAction(F7,function BD)
call TriggerAddAction(G7,function HD)
call TriggerAddAction(H7,function JD)
call TriggerAddAction(I7,function MD)
call TriggerAddAction(l7,function OD)
call TriggerAddAction(J7,function QD)
call TriggerAddAction(K7,function RD)
call TriggerAddAction(L7,function WD)
call TriggerAddAction(X7,function XD)
call TriggerRegisterTimerEvent(Y7,1.,false)
call TriggerAddAction(Y7,function YD)
call TriggerAddAction(p8,function eE)
call TriggerAddAction(q8,function gE)
set M7=CreateSound("Sound\\Interface\\Error.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(M7,"InterfaceError")
call SetSoundDuration(M7,614)
set P7=CreateSound("Abilities\\Spells\\Items\\ResourceItems\\ReceiveGold.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(P7,"ReceiveGold")
call SetSoundDuration(P7,589)
call SetSoundChannel(P7,8)
call SetSoundVolume(P7,105)
set Q7=CreateSound("Sound\\Interface\\BattleNetTick.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(Q7,"ChatroomTimerTick")
call SetSoundDuration(Q7,476)
call SetSoundVolume(Q7,105)
set R7=CreateSound("Sound\\Interface\\Warning.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(R7,"Warning")
call SetSoundDuration(R7,$770)
call SetSoundVolume(R7,105)
set S7=CreateSound("Sound\\Interface\\ItemReceived.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(S7,"ItemReward")
call SetSoundDuration(S7,$5CB)
set N7=CreateSound("Sound\\Interface\\Warning\\Human\\KnightNoGold1.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(N7,"NoGoldHuman")
call SetSoundDuration(N7,$5CE)
set O7=CreateSound("Sound\\Buildings\\Human\\KnightResearchComplete1.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(O7,"ResearchCompleteHuman")
call SetSoundDuration(O7,$52E)
set g7=CreateRegion()
set h7=CreateRegion()
set j7=mq()
set k7=sq()
call Pu(0)
call Xb(1,1093677109,5,$A,0,function HB)
call Xb(2,1093677133,5,$A,0,function NB)
call Xb(3,1093677108,5,$A,0,function QB)
call Xb(4,1093677130,5,$A,0,function UB)
call Xb(5,1093677131,5,$A,0,function gb)
call Xb(6,1093677106,1,2,3,function jb)
call Xb(7,1093677136,$A,20,0,function bb)
call Xb(8,1093677107,5,$A,0,function Hb)
call Xb(9,1093677141,5,$A,0,function Ub)
call bc(0,5,false,-1,false,false,0,0,0,0,0,$A)
call bc(1,$A,true,-1,true,true,1,5,0,20,$A,$A)
call bc(2,$A,true,-1,true,true,1,5,0,20,$A,$A)
call bc(3,$A,true,-1,true,true,1,5,0,20,$A,$A)
call bc(4,20,true,0,true,false,1,0,20,40,$A,$A)
call bc(5,120,false,0,true,false,1,0,60,$B4,$A,$A)
call bc(6,100,false,0,true,false,1,0,50,$96,$A,$A)
call bc(7,100,false,4,true,false,1,0,50,$96,$A,$A)
call bc(8,20,true,-1,false,false,0,0,0,0,0,$A)
call bc(9,20,true,-1,true,true,1,5,0,20,$A,$A)
call Jc(0,60.,.4,0,false)
call Jc(1,145.,.6,1,true)
call Jc(2,60.,.6,2,true)
call Jc(3,255.,.5,3,true)
call Jc(4,135.,.6,4,true)
call Jc(5,60.,.4,5,false)
call Jc(6,60.,.4,9,false)
call Jc(7,60.,.4,6,false)
call Jc(8,255.,.5,7,true)
call Jc(9,135.,.6,8,true)
call Oc(0,function dC,null)
call Oc(1,function fC,function gC)
call Oc(2,function hC,function iC)
call Oc(3,function jC,function kC)
call Oc(4,function mC,function nC)
call Oc(5,function oC,function pC)
call Oc(6,function wC,function xC)
call Oc(7,function qC,function rC)
call Oc(8,function sC,function tC)
call Oc(9,function uC,function vC)
call Rc(1,10.,.075,1.,1227894834,0,384.,1,false,1093677094,D)
call Rc(2,3.,.075,2.,1227894834,0,384.,3,false,1093677097,D)
call Rc(3,15.,.075,3.,1227894841,0,384.,1,true,1093677110,C)
call Rc(4,5.,.5,.5,1227894832,0,384.,1,false,1093677126,D)
call Rc(8,30.,.0,1.,1227894832,0,384.,1,true,1093677098,F)
call Rc(9,10.,.075,3.,1227894834,1,512.,1,true,1093677137,B)
call Hz(1,true,3,3,3.,0)
call Hz(2,true,1,1,3.,0)
call Hz(3,true,1,1,3.,0)
call Hz(4,true,1,1,.5,0)
call Hz(5,true,1,1,.5,0)
call Hz(6,false,1,1,3.,0)
call Hz(O4,false,1,1,3.,0)
call Hz(7,false,1,1,1.5,0)
call Hz(L4,false,1,1,2.,0)
call Hz(8,false,1,1,2.,0)
call Hz(9,false,1,1,1.5,1093677120)
call Hz(K4,false,1,1,1./ 16.,0)
call Hz(M4,false,1,99,5.,0)
call Hz(N4,false,1,1,3.,1093677088)
call Hz(P4,false,1,1,2.,0)
call Hz(16,false,1,1,3.,1093677088)
call Hz(17,false,5,5,5.,1093677092)
call yB(1,1096888368,false,false,false,.0,.0,.0)
call yB(2,1096888369,true,false,false,.0,5.,.0)
call yB(3,1096888370,false,false,true,.05,.0,256.)
call yB(4,1096888371,true,true,false,.0,5.,512.)
call yB(5,1096888372,false,false,true,.05,.0,256.)
call yB(6,1096888373,true,true,false,10.,10.,512.)
call yB(7,1096888374,true,false,false,10.,10.,.0)
call yB(8,1096888375,true,false,false,10.,10.,512.)
call CB(1,function jA,function mA)
call CB(2,null,function nA)
call CB(3,function rA,function tA)
call CB(4,null,function iA)
call CB(5,null,function oA)
call CB(6,null,function xA)
call CB(7,null,function zA)
call CB(8,null,function cA)
call SC(1,$A,100.,5,q4,100,$A,$A,$A,1.,1,-1,0,1.,$F,$F)
call SC(2,$A,200.,5,q4,$C8,20,20,20,2.,2,-1,1,1.,0,0)
call SC(3,5,300.,5,q4,400,40,40,40,3.,3,-2,1,1.25,25,25)
call SC(4,5,100.,5,q4,400,40,40,40,2.,2,-1,1,1.,0,0)
call SC(5,5,300.,5,q4,400,40,40,40,3.,3,-3,2,1.,0,0)
call SC(6,1,1000.,5,q4,0,0,0,0,.0,0,-5,3,1.,0,0)
call mc(1,1831874608,1097674800,1097412656,1,true,false,60.,function Za)
call mc(2,1831874609,1097674801,1097412657,2,true,false,60.,function kB)
call mc(3,1831874610,1097674802,1097412658,5,true,false,60.,function nB)
call mc(4,1831874611,1097674803,1097412659,4,true,false,60.,function Wa)
call mc(5,1831874612,1097674804,1097412660,3,true,false,60.,function rB)
call mc(6,1831874613,0,1097412661,0,false,true,60.,function uB)
set Y4[0]=pq()
call vc(1,1096953904,1097019440,1097084976,1378889776,0,function eB,1096953905,1097019441,1097084977,1378889777,0,function fB,1096953906,1097019442,1097084978,1378889778,0,function gB)
call vc(2,1096953907,1097019443,1097084979,1378889779,0,function mB,1096953908,1097019444,1097084980,1378889780,0,null,1096953909,1097019445,1097084981,1378889781,0,null)
call vc(3,1096953910,1097019446,1097084982,1378889782,0,function oB,1096953911,1097019447,1097084983,1378889783,0,function pB,1096953912,1097019448,1097084984,1378889784,0,function qB)
call vc(4,1096953916,1097019452,1097084988,1378889788,0,null,1096953917,1097019453,1097084989,1378889789,0,function Xa,1096953918,1097019454,1097084990,1378889790,0,null)
call vc(5,1096953913,1097019449,1097084985,1378889785,0,function sB,1096953889,1097019425,1097084961,1378889761,0,function tB,1096953915,1097019451,1097084987,1378889787,0,null)
call vc(6,1096953914,1097019450,1097084986,1378889786,0,function vB,1096953919,1097019455,1097084991,1378889791,0,function wB,1096953920,1097019456,1097084992,1378889792,0,function xB)
call yC(1,6,1097150512,1097216048,1097281584,null)
call yC(2,7,1097150513,1097216049,1097281585,null)
call yC(3,8,1097150514,1097216050,1097281586,null)
call bC(1,1097347120,1097478192,1097543728,1382101040,0,function yA,1097347121,1097478193,1097543729,1382101041,0,null)
call bC(2,1097347122,1097478194,1097543730,1382101042,0,null,1097347123,1097478195,1097543731,1382101043,0,null)
call bC(3,1097347124,1097478196,1097543732,1382101044,0,function IA,1097347125,1097478197,1097543733,1382101045,0,null)
call Vc(1,6,1,2,3,1093677365,1093677366,1093677367,null)
call hu(1,1,2,3,5,1,0,1093677132,1093677360,null)
call hu(2,0,0,0,4,0,0,0,0,null)
call SetGameSpeed(MAP_SPEED_FASTEST)
call SetMapFlag(MAP_LOCK_SPEED,true)
call SetMapFlag(MAP_USE_HANDICAPS,false)
call SetAllyColorFilterState(0)
call SetCreepCampFilterState(false)
call EnableMinimapFilterButtons(true,false)
call EnableWorldFogBoundary(false)
call SetFloatGameState(GAME_STATE_TIME_OF_DAY,12.)
call SuspendTimeOfDay(true)
call SetMapMusic("Music",true,0)
call TriggerExecute(X7)
endfunction
function config takes nothing returns nothing
local player p
call SetMapName("TRIGSTR_001")
call SetMapDescription("TRIGSTR_904")
call SetPlayers(R)
call SetTeams(2)
set p=Player(0)
call DefineStartLocation(0,V7,W7)
call SetPlayerStartLocation(p,0)
call SetPlayerColor(p,ConvertPlayerColor(1))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,0)
set p=Player(1)
call DefineStartLocation(1,V7,W7)
call SetPlayerStartLocation(p,1)
call SetPlayerColor(p,ConvertPlayerColor(2))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(2)
call DefineStartLocation(2,V7,W7)
call SetPlayerStartLocation(p,2)
call SetPlayerColor(p,ConvertPlayerColor(3))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(3)
call DefineStartLocation(3,V7,W7)
call SetPlayerStartLocation(p,3)
call SetPlayerColor(p,ConvertPlayerColor(4))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(4)
call DefineStartLocation(4,V7,W7)
call SetPlayerStartLocation(p,4)
call SetPlayerColor(p,ConvertPlayerColor(5))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(5)
call DefineStartLocation(5,V7,W7)
call SetPlayerStartLocation(p,5)
call SetPlayerColor(p,ConvertPlayerColor(6))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(6)
call DefineStartLocation(6,V7,W7)
call SetPlayerStartLocation(p,6)
call SetPlayerColor(p,ConvertPlayerColor(7))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(7)
call DefineStartLocation(7,V7,W7)
call SetPlayerStartLocation(p,7)
call SetPlayerColor(p,ConvertPlayerColor(8))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(8)
call DefineStartLocation(8,V7,W7)
call SetPlayerStartLocation(p,8)
call SetPlayerColor(p,ConvertPlayerColor(9))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(9)
call DefineStartLocation(9,V7,W7)
call SetPlayerStartLocation(p,9)
call SetPlayerColor(p,ConvertPlayerColor($B))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=null
endfunction
function iE takes nothing returns boolean
local integer m=Ep
local integer T=Fp
local integer nq=bq()
set Mj[nq]=(T)
set Nj[nq]=m
set wj[m]=nq
call UnitAddAbility(Qi[m],mm[Mj[nq]])
set Kp=nq
return true
endfunction
function jE takes nothing returns boolean
local integer nq=lp
return true
endfunction
function kE takes nothing returns boolean
local integer nq=lp
local integer a=0
set a=0
loop
call Au(X9[Y9[nq]+a])
set a=a+1
exitwhen a==608
endloop
call Zs("Neighbors are ready!")
return true
endfunction
function mE takes nothing returns boolean
local integer nq=lp
local integer a=0
loop
set X9[Y9[nq]+a]=zu(nq,a)
set a=a+1
exitwhen a==608
endloop
call Hq(nq)
call Zs("Cells are ready!")
return true
endfunction
function nE takes nothing returns boolean
local integer nq=lp
local integer a=0
loop
call Eq(X9[Y9[nq]+a])
set a=a+1
exitwhen a==608
endloop
call Zs("Cells destroyed!")
return true
endfunction
function oE takes nothing returns boolean
local integer nq=lp
local integer a=0
local boolean pE=not sd[nq]
local integer c
loop
set c=X9[Y9[nq]+a]
set I9[(c)]=true
if pE then
set c9[(c)]=(D9[c])
set D9[(c)]=(0)
endif
set a=a+1
exitwhen a==o4
endloop
return true
endfunction
function qE takes nothing returns boolean
local integer nq=lp
local integer q=Ep
local integer c
local integer d
set td[nq]=true
loop
set c=Bu(q)
set d=D9[c]+1
call cu(q,l9[c],d)
call cu(q,J9[c],d)
call cu(q,K9[c],d)
call cu(q,L9[c],d)
exitwhen(R9[(q)]==0)
endloop
set sd[nq]=I9[X9[Y9[nq]]]
set td[nq]=false
return true
endfunction
function rE takes nothing returns boolean
local integer nq=lp
set Jp=.0
return true
endfunction
function sE takes nothing returns boolean
local integer nq=lp
set Jp=.0
return true
endfunction
function tE takes nothing returns boolean
local integer nq=lp
set ej[nq]=ej[nq]+ej[nq]*Wn[gj[nq]]
return true
endfunction
function uE takes nothing returns boolean
local integer nq=lp
local integer m=Ep
call oa(nq,m)
return true
endfunction
function vE takes nothing returns boolean
local integer nq=lp
local integer dr=Ep
if qa(nq,dr,hj[nq])then
call pa(nq)
else
call TimerStart(ij[(nq)],I4,false,R4)
endif
return true
endfunction
function wE takes nothing returns boolean
local integer nq=lp
local integer m=Ep
set mj[nq]=mj[nq]+1
call SetItemCharges(tj[nq],mj[nq])
call pw(Pi[nq])
if qn[xg[m]]>0 then
call Aa(m,nq)
endif
return true
endfunction
function xE takes nothing returns boolean
local integer nq=lp
if mo[Si[nq]]then
call ShowImage(jj[nq],true)
endif
return true
endfunction
function yE takes nothing returns boolean
local integer nq=lp
if mo[Si[nq]]then
call ShowImage(jj[nq],false)
endif
return true
endfunction
function zE takes nothing returns boolean
local integer nq=lp
call JA(nq)
call Zs("TowerSeller "+I2S(nq)+" is deleted.")
return true
endfunction
function AE takes nothing returns boolean
local integer nq=lp
local integer Wu=Sd[nq]
local integer gv
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=Ud[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=Xd[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=ee[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=ie[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=ne[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=qe[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=te[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=we[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=Ae[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=Ce[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
set Wu=Fe[nq]
loop
exitwhen Wu==0
set gv=Jd[Wu]
call Ur(Hd[Wu])
set Wu=gv
endloop
call Zs("MinionBuffStorage "+I2S(nq)+" is deleted.")
return true
endfunction
function aE takes nothing returns boolean
local integer nq=lp
local integer a=0
loop
call DestroyImage(pf[qf[nq]+a])
set pf[qf[nq]+a]=null
set a=a+1
exitwhen a==120
endloop
return true
endfunction
function BE takes nothing returns boolean
local integer nq=lp
call Zz(nq)
call Zs("MinionAbility "+I2S(nq)+" is deleted.")
return true
endfunction
function bE takes nothing returns boolean
local integer nq=lp
return true
endfunction
function CE takes nothing returns boolean
local integer nq=lp
local integer sr=Ep
return true
endfunction
function cE takes nothing returns boolean
local integer nq=lp
local integer sr=Ep
local integer ur=Fp
local integer vr=Gp
set Lp=false
return true
endfunction
function DE takes nothing returns boolean
local integer nq=lp
local real r=Hp
if r>.0 and not(Wh[nq])then
set r=TimerGetRemaining(Xh[nq])-r
if r<=.0 then
call TimerStart((Xh[nq]),.0,false,null)
call qr(nq)
else
call TimerStart(Xh[nq],r,false,function dA)
endif
endif
return true
endfunction
function EE takes nothing returns boolean
local integer nq=lp
return true
endfunction
function FE takes nothing returns boolean
local integer nq=lp
return true
endfunction
function GE takes nothing returns boolean
local integer nq=lp
call Tz(nq)
call DestroyImage(Oh[nq])
set Oh[nq]=null
set Nh[nq]=null
return true
endfunction
function HE takes nothing returns boolean
local integer nq=lp
call RemoveUnit(Gh[nq])
set Gh[nq]=null
call Zs("MinionDummy "+I2S(nq)+" is deleted.")
return true
endfunction
function IE takes nothing returns boolean
local integer nq=lp
if rg[nq]!=0 then
call Ar(rg[nq])
endif
call SetUnitUserData(og[nq],0)
set og[nq]=null
return true
endfunction
function lE takes nothing returns boolean
local integer nq=lp
call Vt(yf[kg[nq]],nq)
call fw(kg[nq],jg[nq])
call aw(kg[nq],rn[xg[nq]])
call qw(kg[nq],-on[xg[nq]])
call sw(kg[nq],nn[xg[nq]])
call ww(kg[nq],I2S(nn[xg[nq]]),og[nq])
call DestroyEffect(AddSpecialEffect(("Abilities\\Spells\\Other\\Transmute\\PileofGold.mdx"),((GetUnitX(og[nq]))*1.),((GetUnitY(og[nq]))*1.)))
call RemoveUnit(og[nq])
call Rr(nq)
call Zs("Minion "+I2S(nq)+" is sold.")
return true
endfunction
function JE takes nothing returns boolean
local integer nq=lp
set Jp=vn[xg[nq]]
return true
endfunction
function KE takes nothing returns boolean
local integer nq=lp
set Kp=wn[xg[nq]]
return true
endfunction
function LE takes nothing returns boolean
local integer nq=lp
set Kp=xn[xg[nq]]
return true
endfunction
function ME takes nothing returns boolean
local integer nq=lp
set Lp=(me[qg[(nq)]])
return true
endfunction
function NE takes nothing returns boolean
local integer nq=lp
set Lp=false
return true
endfunction
function OE takes nothing returns boolean
local integer nq=lp
set Lp=false
return true
endfunction
function PE takes nothing returns boolean
local integer nq=lp
set Lp=false
return true
endfunction
function QE takes nothing returns boolean
local integer nq=lp
set Lp=false
return true
endfunction
function RE takes nothing returns boolean
local integer nq=lp
local real Kr=Hp
set Jp=Tx(nq,Kr)
return true
endfunction
function SE takes nothing returns boolean
local integer nq=lp
local real Kr=Hp
set Jp=Ux(nq,Kr)
return true
endfunction
function TE takes nothing returns boolean
local integer nq=lp
local real Kr=Hp
set Jp=(vg[(nq)]*((Kr)*1.)/ 1000.)
return true
endfunction
function UE takes nothing returns boolean
local integer nq=lp
local boolean Pr=Ip
call SetUnitExploded(og[nq],Pr)
call KillUnit(og[nq])
if Pr or fn[xg[nq]]then
call gy(nq)
else
set sg[nq]=false
call fy(nq)
call du(Bf[mg[nq]],nq)
call xr(rg[nq])
set Eg[nq]=GetUnitX(og[nq])
set Fg[nq]=GetUnitY(og[nq])
set Gg[nq]=GetUnitFacing(og[nq])
call TimerStart(lg[nq],15.,false,function qy)
endif
return true
endfunction
function VE takes nothing returns boolean
local integer nq=lp
call Ay(nq)
call RemoveSavedInteger(f7,pg[Ng[nq]],Mg[nq])
call UnitRemoveAbility(og[Ng[nq]],ch[Qg[nq]])
set Og[nq]=null
return true
endfunction
function WE takes nothing returns boolean
local integer nq=lp
set Lp=true
return true
endfunction
function XE takes nothing returns boolean
local integer nq=lp
call Qx(Ng[nq],Tg[nq])
set lp=nq
return true
endfunction
function YE takes nothing returns boolean
local integer nq=lp
if(q9[r9[(Kf[Lf[Pg[nq]]+2])]+(0)])then
call bz(Ng[nq],.15)
endif
set Lp=true
return true
endfunction
function ZE takes nothing returns boolean
local integer nq=lp
call Mx(Ng[nq],Tg[nq])
if vh[nq]then
call rv(qg[(Ng[nq])],(Ug[nq]))
endif
set lp=nq
return true
endfunction
function d3 takes nothing returns boolean
local integer nq=lp
call Xu(qg[(Ng[nq])],(Tg[nq]))
set lp=nq
return true
endfunction
function e3 takes nothing returns boolean
local integer nq=lp
call gx(Ng[nq],yi[Me[Pg[nq]]],"Effects\\RestoreHealthGreen.mdx","origin")
set Lp=Rg[nq]==0
return true
endfunction
function f3 takes nothing returns boolean
local integer nq=lp
call Px(Ng[nq],Tg[nq])
call Mx(Ng[nq],Ug[nq])
set lp=nq
return true
endfunction
function g3 takes nothing returns boolean
local integer nq=lp
call bv(qg[(Ng[nq])],(Tg[nq]))
if uh[nq]then
call rv(qg[(Ng[nq])],(Ug[nq]))
endif
call AddUnitAnimationProperties(og[Ng[nq]],"Defend",false)
set lp=nq
return true
endfunction
function h3 takes nothing returns boolean
local integer nq=lp
if yn[zn[cf[Df[Pg[nq]]+2]]+2]then
call tz(Ng[nq],Nd[(Tg[nq])]*.5)
endif
set Lp=true
return true
endfunction
function i3 takes nothing returns boolean
local integer nq=lp
call Lx(Ng[nq],Tg[nq])
set lp=nq
return true
endfunction
function j3 takes nothing returns boolean
local integer nq=lp
call Gv(qg[(Ng[nq])],(Tg[nq]))
set lp=nq
return true
endfunction
function k3 takes nothing returns boolean
local integer nq=lp
local integer Sv=cf[Df[Pg[nq]]+4]
if yn[zn[Sv]+2]then
call ex(Ng[nq],Nd[(Tg[nq])]*.5,"Effects\\RestoreHealthGreen.mdx","origin")
endif
if yn[zn[Sv]+0]then
call nz(Ng[nq],Og[nq],Pg[nq],40)
endif
set Lp=true
return true
endfunction
function m3 takes nothing returns boolean
local integer nq=lp
call Ox(Ng[nq],Tg[nq])
set lp=nq
return true
endfunction
function n3 takes nothing returns boolean
local integer nq=lp
call RemoveUnit(dh[nq])
set dh[nq]=null
call Zs("TowerAttacker "+I2S(nq)+" is deleted.")
return true
endfunction
function o3 takes nothing returns boolean
local integer nq=lp
call Qx(Ng[nq],Tg[nq])
set lp=nq
return true
endfunction
function p3 takes nothing returns boolean
local integer nq=lp
call Xu(qg[(Ng[nq])],(Tg[nq]))
set lp=nq
return true
endfunction
function q3 takes nothing returns boolean
local integer nq=lp
if UnitAlive(Og[nq])then
call wr(rg[(GetUnitUserData(Og[nq]))],1.)
set Lp=Rg[nq]==0
return true
endif
set Lp=true
return true
endfunction
function r3 takes nothing returns boolean
local integer nq=lp
local integer sr=Ep
local integer ur=Fp
local integer vr=Gp
if kg[sr]==kg[Uh[nq]]then
call qz(sr,Uh[nq])
call gA(nq)
set Lp=true
return true
endif
set Lp=false
return true
endfunction
function s3 takes nothing returns boolean
local integer nq=lp
local integer sr=Ep
local integer ur=Fp
local integer vr=Gp
if vr==1227894834 then
call vz(sr)
call gA(nq)
set Lp=true
return true
endif
set Lp=false
return true
endfunction
function t3 takes nothing returns boolean
local integer nq=lp
if hi[nq]!=0 then
call Xz(hi[nq])
endif
set lp=nq
return true
endfunction
function u3 takes nothing returns boolean
local integer nq=lp
local integer m=Uh[nq]
set ji=m
set ki=false
call GroupEnumUnitsInRange(bj_lastCreatedGroup,GetUnitX(og[m]),GetUnitY(og[m]),Ij[Vh[nq]],gi)
if yn[zn[xg[m]]+1]then
set ii[nq]=jt(ki)*Aj
call vx(m)
endif
return true
endfunction
function v3 takes nothing returns boolean
local integer nq=lp
call Xz(hi[nq])
set hi[nq]=0
call PauseTimer(Xh[nq])
return true
endfunction
function w3 takes nothing returns boolean
local integer nq=lp
set hi[nq]=Vz(og[Uh[nq]],Ij[Vh[nq]])
call gA(nq)
return true
endfunction
function x3 takes nothing returns boolean
local integer nq=lp
if ui[nq]!=0 then
call Xz(ui[nq])
endif
set lp=nq
return true
endfunction
function y3 takes nothing returns boolean
local integer nq=lp
local integer m=Uh[nq]
local integer z3=Me[kg[m]]
local integer px
set vi=kg[m]
set wi=0
call GroupEnumUnitsInRange(bj_lastCreatedGroup,GetUnitX(og[m]),GetUnitY(og[m]),Ij[Vh[nq]],pi)
set px=wi*ni[z3]
if px>oi[z3]then
set px=oi[z3]
endif
set ti[nq]=px
set si[nq]=px
call vx(m)
return true
endfunction
function A3 takes nothing returns boolean
local integer nq=lp
if qi[nq]then
call AddUnitAnimationProperties(og[Uh[nq]],"Defend",false)
else
call Xz(ui[nq])
set ui[nq]=0
call PauseTimer(Xh[nq])
endif
return true
endfunction
function B3 takes nothing returns boolean
local integer nq=lp
if qi[nq]then
call AddUnitAnimationProperties(og[Uh[nq]],"Defend",true)
else
set ui[nq]=Vz(og[Uh[nq]],Ij[Vh[nq]])
call gA(nq)
endif
return true
endfunction
function C3 takes nothing returns boolean
local integer nq=lp
call Rz(zi[nq])
set lp=nq
return true
endfunction
function c3 takes nothing returns boolean
local integer nq=lp
local integer sr=Ep
local integer ur=Fp
local integer vr=Gp
if sr!=Uh[nq]and kg[sr]==kg[Uh[nq]]and Uw(sr)<=(1.-yi[Me[kg[Uh[nq]]]])then
call Oz(zi[nq],GetUnitX(og[Uh[nq]]),GetUnitY(og[Uh[nq]]))
call Pz(zi[nq],GetUnitX(og[sr]),GetUnitY(og[sr]))
call gA(nq)
set Lp=true
return true
endif
set Lp=false
return true
endfunction
function D3 takes nothing returns boolean
local integer nq=lp
local integer sr=Ep
local integer p=kg[Uh[nq]]
if sr!=Uh[nq]and kg[sr]==p then
call gx(sr,yi[Me[kg[Uh[nq]]]],"Effects\\RestoreHealthGreen.mdx","origin")
if(q9[r9[(Kf[Lf[p]+1])]+(1)])then
call Az(sr,Uh[nq])
endif
endif
return true
endfunction
function E3 takes nothing returns boolean
local integer nq=lp
local integer sr=Ep
local integer ur=Fp
local integer vr=Gp
call cz(sr)
if(q9[r9[(Kf[Lf[kg[sr]]+2])]+(1)])then
set sr=aA(nq)
if sr!=0 then
call Ez(sr,Uh[nq])
endif
endif
call gA(nq)
set Lp=true
return true
endfunction
function F3 takes nothing returns boolean
local integer nq=lp
call CA(nq)
set lp=nq
return true
endfunction
function G3 takes nothing returns boolean
local integer nq=lp
call EA(nq)
return true
endfunction
function H3 takes nothing returns boolean
local integer nq=lp
call Xu(qg[(Ng[nq])],(Tg[nq]))
set lp=nq
return true
endfunction
function I3 takes nothing returns boolean
local integer nq=lp
local integer dd
if(GetUnitTypeId((Og[nq]))!=0)then
set dd=GetUnitUserData(Og[nq])
set Lp=ja(dd,Ng[nq],dk[(wj[dd])],1227894841,false)and Rg[nq]==0
return true
endif
set Lp=true
return true
endfunction
function l3 takes nothing returns boolean
local integer nq=lp
call Mx(Ng[nq],Tg[nq])
set lp=nq
return true
endfunction
function J3 takes nothing returns boolean
local integer nq=lp
call Px(Ng[nq],Tg[nq])
set lp=nq
return true
endfunction
function K3 takes nothing returns boolean
local integer nq=lp
call Mx(Ng[nq],Tg[nq])
set lp=nq
return true
endfunction
function L3 takes nothing returns boolean
local integer nq=lp
call Nx(Ng[nq],Tg[nq])
set lp=nq
return true
endfunction
function M3 takes nothing returns boolean
local integer nq=lp
call Px(Ng[nq],Tg[nq])
set lp=nq
return true
endfunction
function N3 takes nothing returns boolean
local integer nq=lp
call Lx(Ng[nq],Tg[nq])
set lp=nq
return true
endfunction
function O3 takes nothing returns boolean
local integer nq=lp
set Jp=(vn[xg[(nq)]])+ei[(rg[nq])]
return true
endfunction
function P3 takes nothing returns boolean
local integer nq=lp
set Kp=(wn[xg[(nq)]])+di[(rg[nq])]
return true
endfunction
function Q3 takes nothing returns boolean
local integer nq=lp
set Kp=(xn[xg[(nq)]])+di[(rg[nq])]
return true
endfunction
function R3 takes nothing returns boolean
local integer nq=lp
if yn[zn[xg[nq]]+2]then
call dB(zj[nq])
endif
call Mw(nq)
return true
endfunction
function S3 takes nothing returns boolean
local integer nq=lp
set Kp=(wn[xg[(nq)]])+ii[(rg[nq])]
return true
endfunction
function T3 takes nothing returns boolean
local integer nq=lp
set Kp=(xn[xg[(nq)]])+ii[(rg[nq])]
return true
endfunction
function U3 takes nothing returns boolean
local integer nq=lp
set Jp=(vn[xg[(nq)]])+ri[(rg[nq])]
return true
endfunction
function V3 takes nothing returns boolean
local integer nq=lp
set Kp=(wn[xg[(nq)]])+ti[(rg[nq])]
return true
endfunction
function W3 takes nothing returns boolean
local integer nq=lp
set Kp=(xn[xg[(nq)]])+si[(rg[nq])]
return true
endfunction
function X3 takes nothing returns boolean
local integer nq=lp
set Lp=(me[qg[((nq))]])or(yn[zn[xg[nq]]+2]and qi[(rg[nq])])
return true
endfunction
function Y3 takes nothing returns boolean
local integer nq=lp
set Lp=qi[(rg[nq])]
return true
endfunction
function Z3 takes nothing returns boolean
local integer nq=lp
local real Kr=Hp
if qi[(rg[nq])]then
set Jp=.0
return true
endif
set Jp=Tx(nq,Kr)
return true
endfunction
function dF takes nothing returns boolean
local integer nq=lp
call GB(nq)
set Rj[nq]=null
set lp=nq
return true
endfunction
function eF takes nothing returns boolean
local integer nq=lp
local integer PB=pj[Nj[nq]]
if PB<=om[Mj[nq]]then
set Uj[nq]=Uj[nq]+.1
set Vj[nq]=Vj[nq]+.2
if PB==nm[Mj[nq]]then
set Uj[nq]=Uj[nq]+1.
elseif PB==om[Mj[nq]]then
set Vj[nq]=Vj[nq]+2.
endif
call ma(Nj[nq])
endif
return true
endfunction
function fF takes nothing returns boolean
local integer nq=lp
set dk[nq]=dk[nq]+dk[nq]*.075
if et(pj[Nj[nq]],5)then
set fj[Nj[nq]]=fj[Nj[nq]]+1
endif
return true
endfunction
function gF takes nothing returns boolean
local integer nq=lp
call Ry(fk[nq])
set lp=nq
return true
endfunction
function hF takes nothing returns boolean
local integer nq=lp
if pj[Nj[nq]]==nm[Mj[nq]]then
call Ny(hj[Nj[nq]])
endif
return true
endfunction
function iF takes nothing returns boolean
local integer nq=lp
call Ry(gk[nq])
call RemoveUnit(hk[nq])
call TB(nq)
set hk[nq]=null
set ik[nq]=null
set nk[nq]=null
set lp=nq
return true
endfunction
function jF takes nothing returns boolean
local integer nq=lp
call eb(nq)
set lp=nq
return true
endfunction
function kF takes nothing returns boolean
local integer nq=lp
set pk[nq]=pk[nq]+1
if pj[Nj[nq]]==nm[Mj[nq]]then
set pk[nq]=pk[nq]+1
elseif pj[Nj[nq]]==om[Mj[nq]]then
set pk[nq]=pk[nq]+3
call xa(Nj[nq])
endif
return true
endfunction
function mF takes nothing returns boolean
local integer nq=lp
call ib(nq)
call DestroyImage(Ak[nq])
set Ak[nq]=null
set Bk[nq]=null
call mb(nq)
set lp=nq
return true
endfunction
function nF takes nothing returns boolean
local integer nq=lp
if pj[Nj[nq]]==nm[Mj[nq]]then
set yk[nq]=yk[nq]+2
set ak[nq]=ak[nq]+2
set ck[nq]=ck[nq]+2
elseif pj[Nj[nq]]==om[Mj[nq]]then
set zk[nq]=zk[nq]+128.
set ak[nq]=ak[nq]+3
set ck[nq]=ck[nq]+3
call DestroyImage(Ak[nq])
set Ak[nq]=ZA(Nj[nq],zk[nq],sk,tk,uk,xk[nq]==0)
else
call xa(Nj[nq])
if TimerGetRemaining(Ek[nq])>=3. then
call TimerStart(Ek[nq],3.,true,wk)
endif
endif
return true
endfunction
function oF takes nothing returns boolean
local integer nq=lp
call Eb(nq)
call Gb(nq)
set lp=nq
return true
endfunction
function pF takes nothing returns boolean
local integer nq=lp
call Nb(nq)
set im[nq]=null
set lp=nq
return true
endfunction
function qF takes nothing returns boolean
local integer nq=lp
set em[nq]=em[nq]+.01
set fm[nq]=fm[nq]+Zk
if pj[Nj[nq]]==om[Mj[nq]]then
call xa(Nj[nq])
endif
return true
endfunction
function rF takes nothing returns boolean
local integer nq=lp
set Jp=IB(wj[nq])+ha(nq)
return true
endfunction
function sF takes nothing returns boolean
local integer nq=lp
local integer m=Ep
call oa(nq,m)
call KB(wj[nq])
return true
endfunction
function tF takes nothing returns boolean
local integer nq=lp
local integer m=Ep
if oa(nq,m)then
call OB(wj[nq],m)
endif
return true
endfunction
function uF takes nothing returns boolean
local integer nq=lp
local integer m=Ep
if oa(nq,m)then
call Pa((m),Nj[(wj[nq])],ek)
endif
return true
endfunction
function vF takes nothing returns boolean
local integer nq=lp
local integer dr=Ep
if RB(wj[nq],dr)then
call pa(nq)
else
call TimerStart(ij[(nq)],I4,false,R4)
endif
return true
endfunction
function wF takes nothing returns boolean
local integer nq=lp
set Jp=kk[(wj[nq])]+ha(nq)
return true
endfunction
function xF takes nothing returns boolean
local integer nq=lp
set ej[nq]=ej[nq]+Wn[gj[nq]]
return true
endfunction
function yF takes nothing returns boolean
local integer nq=lp
local integer m=Ep
call ZB(wj[nq],m)
return true
endfunction
function zF takes nothing returns boolean
local integer nq=lp
call yb(wj[nq])
return true
endfunction
function AF takes nothing returns boolean
local integer nq=lp
call zb(wj[nq])
return true
endfunction
function aF takes nothing returns boolean
local integer nq=lp
set Jp=Nk[(wj[nq])]+ga(nq)
return true
endfunction
function BF takes nothing returns boolean
local integer nq=lp
set Jp=Mk[(wj[nq])]+ha(nq)
return true
endfunction
function bF takes nothing returns boolean
local integer nq=lp
local integer m=Ep
call ca(nq,m)
call Cb(wj[nq])
return true
endfunction
function CF takes nothing returns boolean
local integer nq=lp
local integer m=Ep
call cb(wj[nq],m)
return true
endfunction
function cF takes nothing returns boolean
local integer nq=lp
set Jp=Sk[(wj[nq])]+ha(nq)
return true
endfunction
function DF takes nothing returns boolean
local integer nq=lp
local integer m=Ep
call oa(nq,m)
call Ib(wj[nq])
return true
endfunction
function EF takes nothing returns boolean
local integer nq=lp
local integer dr=Ep
local integer m=Wy(dr)
call lb(wj[nq],GetUnitX(og[m]),GetUnitY(og[m]))
call pa(nq)
return true
endfunction
function FF takes nothing returns boolean
local integer GF=Ep
local integer ur=Fp
local integer vr=Gp
local boolean HF=not Sx(GF,vr)
local integer a
local integer Sb
local integer IF
if HF then
set HF=hA(rg[GF],GF,ur,vr)
set Sb=Af[mg[GF]]
set a=0
loop
exitwhen HF or a>W8[Sb]
set IF=rg[(U8[V8[(Sb)]+(a)])]
if Ej[Vh[IF]]then
set HF=hA(IF,GF,ur,vr)
endif
set a=a+1
endloop
endif
return true
endfunction
function lF takes nothing returns boolean
local integer nq=Ep
local integer a=0
set a=0
loop
call Au(X9[Y9[nq]+a])
set a=a+1
exitwhen a==608
endloop
call Zs("Neighbors are ready!")
return true
endfunction
function JF takes nothing returns boolean
local integer nq=Ep
local integer a=0
loop
set X9[Y9[nq]+a]=zu(nq,a)
set a=a+1
exitwhen a==608
endloop
call Hq(nq)
call Zs("Cells are ready!")
return true
endfunction
function KF takes nothing returns boolean
local integer nq=Ep
local integer a=0
loop
call Eq(X9[Y9[nq]+a])
set a=a+1
exitwhen a==608
endloop
call Zs("Cells destroyed!")
return true
endfunction
function LF takes nothing returns boolean
local integer nq=Ep
local integer q=Fp
local integer c
local integer d
set td[nq]=true
loop
set c=Bu(q)
set d=D9[c]+1
call cu(q,l9[c],d)
call cu(q,J9[c],d)
call cu(q,K9[c],d)
call cu(q,L9[c],d)
exitwhen(R9[(q)]==0)
endloop
set sd[nq]=I9[X9[Y9[nq]]]
set td[nq]=false
return true
endfunction
function MF takes nothing returns boolean
local integer nq=Ep
local integer a=0
local boolean pE=not sd[nq]
local integer c
loop
set c=X9[Y9[nq]+a]
set I9[(c)]=true
if pE then
set c9[(c)]=(D9[c])
set D9[(c)]=(0)
endif
set a=a+1
exitwhen a==o4
endloop
return true
endfunction
function NF takes nothing returns boolean
local integer nq=Ep
local integer a=0
loop
call DestroyImage(pf[qf[nq]+a])
set pf[qf[nq]+a]=null
set a=a+1
exitwhen a==120
endloop
return true
endfunction
function OF takes nothing returns boolean
local integer nq=Ep
local boolean Pr=Ip
call SetUnitExploded(og[nq],Pr)
call KillUnit(og[nq])
if Pr or fn[xg[nq]]then
call gy(nq)
else
set sg[nq]=false
call fy(nq)
call du(Bf[mg[nq]],nq)
call xr(rg[nq])
set Eg[nq]=GetUnitX(og[nq])
set Fg[nq]=GetUnitY(og[nq])
set Gg[nq]=GetUnitFacing(og[nq])
call TimerStart(lg[nq],15.,false,function qy)
endif
return true
endfunction
function PF takes nothing returns boolean
local integer nq=Ep
local integer dr=Fp
if qa(nq,dr,hj[nq])then
call pa(nq)
else
call TimerStart(ij[(nq)],I4,false,R4)
endif
return true
endfunction
function hE takes nothing returns nothing
set ap=CreateTrigger()
call TriggerAddCondition(ap,Condition(function iE))
set Wo[85]=CreateTrigger()
set Wo[89]=Wo[85]
set Wo[92]=Wo[85]
set Wo[93]=Wo[85]
call TriggerAddCondition(Wo[85],Condition(function jE))
call TriggerAddAction(Wo[85],function jE)
set mp[85]=null
set mp[87]=null
set mp[92]=null
set np=CreateTrigger()
call TriggerAddCondition(np,Condition(function kE))
call TriggerAddAction(np,function kE)
set op=CreateTrigger()
call TriggerAddCondition(op,Condition(function mE))
call TriggerAddAction(op,function mE)
set pp=CreateTrigger()
call TriggerAddCondition(pp,Condition(function nE))
call TriggerAddAction(pp,function nE)
set qp=CreateTrigger()
call TriggerAddCondition(qp,Condition(function oE))
call TriggerAddAction(qp,function oE)
set rp=CreateTrigger()
call TriggerAddCondition(rp,Condition(function qE))
call TriggerAddAction(rp,function qE)
set Yo[30]=null
set Yo[31]=null
set Yo[32]=null
set Yo[33]=null
set Oo[70]=CreateTrigger()
set Oo[96]=Oo[70]
set Oo[97]=Oo[70]
set Oo[98]=Oo[70]
set Oo[99]=Oo[70]
set Oo[100]=Oo[70]
set Oo[101]=Oo[70]
set Oo[102]=Oo[70]
set Oo[104]=Oo[70]
set Oo[105]=Oo[70]
call TriggerAddCondition(Oo[70],Condition(function rE))
call TriggerAddAction(Oo[70],function rE)
set Po[70]=CreateTrigger()
set Po[96]=Po[70]
set Po[98]=Po[70]
set Po[99]=Po[70]
set Po[101]=Po[70]
set Po[102]=Po[70]
set Po[105]=Po[70]
call TriggerAddCondition(Po[70],Condition(function sE))
call TriggerAddAction(Po[70],function sE)
set Qo[70]=CreateTrigger()
set Qo[96]=Qo[70]
set Qo[97]=Qo[70]
set Qo[98]=Qo[70]
set Qo[99]=Qo[70]
set Qo[101]=Qo[70]
set Qo[102]=Qo[70]
set Qo[103]=Qo[70]
set Qo[104]=Qo[70]
set Qo[105]=Qo[70]
call TriggerAddCondition(Qo[70],Condition(function tE))
call TriggerAddAction(Qo[70],function tE)
set Ro[70]=CreateTrigger()
set Ro[96]=Ro[70]
set Ro[101]=Ro[70]
set Ro[102]=Ro[70]
set Ro[105]=Ro[70]
call TriggerAddCondition(Ro[70],Condition(function uE))
call TriggerAddAction(Ro[70],function uE)
set So[70]=CreateTrigger()
set So[96]=So[70]
set So[97]=So[70]
set So[98]=So[70]
set So[100]=So[70]
set So[101]=So[70]
set So[102]=So[70]
set So[103]=So[70]
set So[105]=So[70]
call TriggerAddCondition(So[70],Condition(function vE))
call TriggerAddAction(So[70],function vE)
set To[70]=CreateTrigger()
set To[96]=To[70]
set To[97]=To[70]
set To[98]=To[70]
set To[99]=To[70]
set To[100]=To[70]
set To[101]=To[70]
set To[102]=To[70]
set To[104]=To[70]
set To[105]=To[70]
call TriggerAddCondition(To[70],Condition(function wE))
call TriggerAddAction(To[70],function wE)
set Uo[70]=CreateTrigger()
set Uo[96]=Uo[70]
set Uo[97]=Uo[70]
set Uo[98]=Uo[70]
set Uo[99]=Uo[70]
set Uo[100]=Uo[70]
set Uo[101]=Uo[70]
set Uo[103]=Uo[70]
set Uo[104]=Uo[70]
set Uo[105]=Uo[70]
call TriggerAddCondition(Uo[70],Condition(function xE))
call TriggerAddAction(Uo[70],function xE)
set Vo[70]=CreateTrigger()
set Vo[96]=Vo[70]
set Vo[97]=Vo[70]
set Vo[98]=Vo[70]
set Vo[99]=Vo[70]
set Vo[100]=Vo[70]
set Vo[101]=Vo[70]
set Vo[103]=Vo[70]
set Vo[104]=Vo[70]
set Vo[105]=Vo[70]
call TriggerAddCondition(Vo[70],Condition(function yE))
call TriggerAddAction(Vo[70],function yE)
set jp[70]=null
set jp[96]=null
set jp[97]=null
set jp[98]=null
set jp[99]=null
set jp[100]=null
set jp[101]=null
set jp[102]=null
set jp[103]=null
set jp[104]=null
set jp[105]=null
set Ap=CreateTrigger()
call TriggerAddCondition(Ap,Condition(function zE))
set sp=CreateTrigger()
call TriggerAddCondition(sp,Condition(function AE))
set tp=CreateTrigger()
call TriggerAddCondition(tp,Condition(function aE))
call TriggerAddAction(tp,function aE)
set hp[60]=CreateTrigger()
set hp[61]=hp[60]
set hp[62]=hp[60]
set hp[63]=hp[60]
set hp[67]=hp[60]
call TriggerAddCondition(hp[60],Condition(function BE))
set Jo[60]=CreateTrigger()
set Jo[61]=Jo[60]
set Jo[62]=Jo[60]
set Jo[63]=Jo[60]
set Jo[66]=Jo[60]
set Jo[67]=Jo[60]
call TriggerAddCondition(Jo[60],Condition(function bE))
call TriggerAddAction(Jo[60],function bE)
set Ko[60]=CreateTrigger()
set Ko[61]=Ko[60]
set Ko[62]=Ko[60]
set Ko[63]=Ko[60]
set Ko[64]=Ko[60]
set Ko[65]=Ko[60]
set Ko[67]=Ko[60]
set Ko[68]=Ko[60]
call TriggerAddCondition(Ko[60],Condition(function CE))
call TriggerAddAction(Ko[60],function CE)
set Lo[60]=CreateTrigger()
set Lo[62]=Lo[60]
set Lo[64]=Lo[60]
set Lo[65]=Lo[60]
set Lo[68]=Lo[60]
call TriggerAddCondition(Lo[60],Condition(function cE))
call TriggerAddAction(Lo[60],function cE)
set zp=CreateTrigger()
call TriggerAddCondition(zp,Condition(function DE))
set Mo[60]=CreateTrigger()
set Mo[61]=Mo[60]
set Mo[62]=Mo[60]
set Mo[63]=Mo[60]
set Mo[66]=Mo[60]
set Mo[67]=Mo[60]
set Mo[68]=Mo[60]
call TriggerAddCondition(Mo[60],Condition(function EE))
call TriggerAddAction(Mo[60],function EE)
set No[60]=CreateTrigger()
set No[61]=No[60]
set No[62]=No[60]
set No[63]=No[60]
set No[66]=No[60]
set No[67]=No[60]
set No[68]=No[60]
call TriggerAddCondition(No[60],Condition(function FE))
call TriggerAddAction(No[60],function FE)
set yp=CreateTrigger()
call TriggerAddCondition(yp,Condition(function GE))
set xp=CreateTrigger()
call TriggerAddCondition(xp,Condition(function HE))
set dp[42]=CreateTrigger()
set dp[78]=dp[42]
set dp[79]=dp[42]
set dp[80]=dp[42]
set dp[81]=dp[42]
set dp[82]=dp[42]
set dp[83]=dp[42]
call TriggerAddCondition(dp[42],Condition(function IE))
set Ao[42]=CreateTrigger()
set Ao[78]=Ao[42]
set Ao[80]=Ao[42]
set Ao[81]=Ao[42]
set Ao[82]=Ao[42]
set Ao[83]=Ao[42]
call TriggerAddCondition(Ao[42],Condition(function lE))
call TriggerAddAction(Ao[42],function lE)
set ao[42]=CreateTrigger()
set ao[78]=ao[42]
set ao[80]=ao[42]
set ao[81]=ao[42]
set ao[83]=ao[42]
call TriggerAddCondition(ao[42],Condition(function JE))
call TriggerAddAction(ao[42],function JE)
set Bo[42]=CreateTrigger()
set Bo[78]=Bo[42]
set Bo[80]=Bo[42]
set Bo[83]=Bo[42]
call TriggerAddCondition(Bo[42],Condition(function KE))
call TriggerAddAction(Bo[42],function KE)
set bo[42]=CreateTrigger()
set bo[78]=bo[42]
set bo[80]=bo[42]
set bo[83]=bo[42]
call TriggerAddCondition(bo[42],Condition(function LE))
call TriggerAddAction(bo[42],function LE)
set Co[42]=CreateTrigger()
set Co[78]=Co[42]
set Co[79]=Co[42]
set Co[80]=Co[42]
set Co[81]=Co[42]
set Co[83]=Co[42]
call TriggerAddCondition(Co[42],Condition(function ME))
call TriggerAddAction(Co[42],function ME)
set co[42]=CreateTrigger()
set co[78]=co[42]
set co[79]=co[42]
set co[80]=co[42]
set co[81]=co[42]
set co[82]=co[42]
set co[83]=co[42]
call TriggerAddCondition(co[42],Condition(function NE))
call TriggerAddAction(co[42],function NE)
set Do[42]=CreateTrigger()
set Do[78]=Do[42]
set Do[79]=Do[42]
set Do[80]=Do[42]
set Do[81]=Do[42]
set Do[83]=Do[42]
call TriggerAddCondition(Do[42],Condition(function OE))
call TriggerAddAction(Do[42],function OE)
set Eo[42]=CreateTrigger()
set Eo[78]=Eo[42]
set Eo[79]=Eo[42]
set Eo[80]=Eo[42]
set Eo[81]=Eo[42]
set Eo[82]=Eo[42]
set Eo[83]=Eo[42]
call TriggerAddCondition(Eo[42],Condition(function PE))
call TriggerAddAction(Eo[42],function PE)
set Fo[42]=CreateTrigger()
set Fo[78]=Fo[42]
set Fo[79]=Fo[42]
set Fo[80]=Fo[42]
set Fo[81]=Fo[42]
set Fo[82]=Fo[42]
set Fo[83]=Fo[42]
call TriggerAddCondition(Fo[42],Condition(function QE))
call TriggerAddAction(Fo[42],function QE)
set Go[42]=CreateTrigger()
set Go[78]=Go[42]
set Go[79]=Go[42]
set Go[80]=Go[42]
set Go[81]=Go[42]
set Go[83]=Go[42]
call TriggerAddCondition(Go[42],Condition(function RE))
call TriggerAddAction(Go[42],function RE)
set Ho[42]=CreateTrigger()
set Ho[78]=Ho[42]
set Ho[79]=Ho[42]
set Ho[80]=Ho[42]
set Ho[81]=Ho[42]
set Ho[82]=Ho[42]
set Ho[83]=Ho[42]
call TriggerAddCondition(Ho[42],Condition(function SE))
call TriggerAddAction(Ho[42],function SE)
set Io[42]=CreateTrigger()
set Io[78]=Io[42]
set Io[79]=Io[42]
set Io[80]=Io[42]
set Io[81]=Io[42]
set Io[82]=Io[42]
set Io[83]=Io[42]
call TriggerAddCondition(Io[42],Condition(function TE))
call TriggerAddAction(Io[42],function TE)
set vp=CreateTrigger()
call TriggerAddCondition(vp,Condition(function UE))
call TriggerAddAction(vp,function UE)
set fp[43]=CreateTrigger()
call TriggerAddCondition(fp[43],Condition(function VE))
set lo[43]=CreateTrigger()
set lo[47]=lo[43]
set lo[49]=lo[43]
set lo[51]=lo[43]
set lo[53]=lo[43]
set lo[55]=lo[43]
set lo[72]=lo[43]
set lo[73]=lo[43]
set lo[74]=lo[43]
set lo[75]=lo[43]
set lo[76]=lo[43]
set lo[77]=lo[43]
call TriggerAddCondition(lo[43],Condition(function WE))
call TriggerAddAction(lo[43],function WE)
set lo[54]=CreateTrigger()
call TriggerAddCondition(lo[54],Condition(function YE))
call TriggerAddAction(lo[54],function YE)
set fp[54]=CreateTrigger()
call TriggerAddCondition(fp[54],Condition(function XE))
call TriggerAddCondition(fp[54],Condition(function VE))
set fp[53]=CreateTrigger()
call TriggerAddCondition(fp[53],Condition(function ZE))
call TriggerAddCondition(fp[53],Condition(function VE))
set lo[52]=CreateTrigger()
call TriggerAddCondition(lo[52],Condition(function e3))
call TriggerAddAction(lo[52],function e3)
set fp[52]=CreateTrigger()
call TriggerAddCondition(fp[52],Condition(function d3))
call TriggerAddCondition(fp[52],Condition(function VE))
set fp[51]=CreateTrigger()
call TriggerAddCondition(fp[51],Condition(function f3))
call TriggerAddCondition(fp[51],Condition(function VE))
set lo[50]=CreateTrigger()
call TriggerAddCondition(lo[50],Condition(function h3))
call TriggerAddAction(lo[50],function h3)
set fp[50]=CreateTrigger()
call TriggerAddCondition(fp[50],Condition(function g3))
call TriggerAddCondition(fp[50],Condition(function VE))
set fp[49]=CreateTrigger()
call TriggerAddCondition(fp[49],Condition(function i3))
call TriggerAddCondition(fp[49],Condition(function VE))
set lo[48]=CreateTrigger()
call TriggerAddCondition(lo[48],Condition(function k3))
call TriggerAddAction(lo[48],function k3)
set fp[48]=CreateTrigger()
call TriggerAddCondition(fp[48],Condition(function j3))
call TriggerAddCondition(fp[48],Condition(function VE))
set fp[47]=CreateTrigger()
call TriggerAddCondition(fp[47],Condition(function m3))
call TriggerAddCondition(fp[47],Condition(function VE))
set wp=CreateTrigger()
call TriggerAddCondition(wp,Condition(function n3))
set fp[55]=CreateTrigger()
call TriggerAddCondition(fp[55],Condition(function o3))
call TriggerAddCondition(fp[55],Condition(function VE))
set lo[56]=CreateTrigger()
call TriggerAddCondition(lo[56],Condition(function q3))
call TriggerAddAction(lo[56],function q3)
set fp[56]=CreateTrigger()
call TriggerAddCondition(fp[56],Condition(function p3))
call TriggerAddCondition(fp[56],Condition(function VE))
set Lo[61]=CreateTrigger()
call TriggerAddCondition(Lo[61],Condition(function r3))
call TriggerAddAction(Lo[61],function r3)
set Lo[63]=CreateTrigger()
call TriggerAddCondition(Lo[63],Condition(function s3))
call TriggerAddAction(Lo[63],function s3)
set Jo[64]=CreateTrigger()
call TriggerAddCondition(Jo[64],Condition(function u3))
call TriggerAddAction(Jo[64],function u3)
set Mo[64]=CreateTrigger()
call TriggerAddCondition(Mo[64],Condition(function v3))
call TriggerAddAction(Mo[64],function v3)
set No[64]=CreateTrigger()
call TriggerAddCondition(No[64],Condition(function w3))
call TriggerAddAction(No[64],function w3)
set hp[64]=CreateTrigger()
call TriggerAddCondition(hp[64],Condition(function t3))
call TriggerAddCondition(hp[64],Condition(function BE))
set Jo[65]=CreateTrigger()
call TriggerAddCondition(Jo[65],Condition(function y3))
call TriggerAddAction(Jo[65],function y3)
set Mo[65]=CreateTrigger()
call TriggerAddCondition(Mo[65],Condition(function A3))
call TriggerAddAction(Mo[65],function A3)
set No[65]=CreateTrigger()
call TriggerAddCondition(No[65],Condition(function B3))
call TriggerAddAction(No[65],function B3)
set hp[65]=CreateTrigger()
call TriggerAddCondition(hp[65],Condition(function x3))
call TriggerAddCondition(hp[65],Condition(function BE))
set Lo[66]=CreateTrigger()
call TriggerAddCondition(Lo[66],Condition(function c3))
call TriggerAddAction(Lo[66],function c3)
set Ko[66]=CreateTrigger()
call TriggerAddCondition(Ko[66],Condition(function D3))
call TriggerAddAction(Ko[66],function D3)
set hp[66]=CreateTrigger()
call TriggerAddCondition(hp[66],Condition(function C3))
call TriggerAddCondition(hp[66],Condition(function BE))
set Lo[67]=CreateTrigger()
call TriggerAddCondition(Lo[67],Condition(function E3))
call TriggerAddAction(Lo[67],function E3)
set Jo[68]=CreateTrigger()
call TriggerAddCondition(Jo[68],Condition(function G3))
call TriggerAddAction(Jo[68],function G3)
set hp[68]=CreateTrigger()
call TriggerAddCondition(hp[68],Condition(function F3))
call TriggerAddCondition(hp[68],Condition(function BE))
set lo[71]=CreateTrigger()
call TriggerAddCondition(lo[71],Condition(function I3))
call TriggerAddAction(lo[71],function I3)
set fp[71]=CreateTrigger()
call TriggerAddCondition(fp[71],Condition(function H3))
call TriggerAddCondition(fp[71],Condition(function VE))
set fp[72]=CreateTrigger()
call TriggerAddCondition(fp[72],Condition(function l3))
call TriggerAddCondition(fp[72],Condition(function VE))
set fp[73]=CreateTrigger()
call TriggerAddCondition(fp[73],Condition(function J3))
call TriggerAddCondition(fp[73],Condition(function VE))
set fp[74]=CreateTrigger()
call TriggerAddCondition(fp[74],Condition(function K3))
call TriggerAddCondition(fp[74],Condition(function VE))
set fp[75]=CreateTrigger()
call TriggerAddCondition(fp[75],Condition(function L3))
call TriggerAddCondition(fp[75],Condition(function VE))
set fp[76]=CreateTrigger()
call TriggerAddCondition(fp[76],Condition(function M3))
call TriggerAddCondition(fp[76],Condition(function VE))
set fp[77]=CreateTrigger()
call TriggerAddCondition(fp[77],Condition(function N3))
call TriggerAddCondition(fp[77],Condition(function VE))
set ao[79]=CreateTrigger()
call TriggerAddCondition(ao[79],Condition(function O3))
call TriggerAddAction(ao[79],function O3)
set Bo[79]=CreateTrigger()
call TriggerAddCondition(Bo[79],Condition(function P3))
call TriggerAddAction(Bo[79],function P3)
set bo[79]=CreateTrigger()
call TriggerAddCondition(bo[79],Condition(function Q3))
call TriggerAddAction(bo[79],function Q3)
set Ao[79]=CreateTrigger()
call TriggerAddCondition(Ao[79],Condition(function R3))
call TriggerAddAction(Ao[79],function R3)
set Bo[81]=CreateTrigger()
call TriggerAddCondition(Bo[81],Condition(function S3))
call TriggerAddAction(Bo[81],function S3)
set bo[81]=CreateTrigger()
call TriggerAddCondition(bo[81],Condition(function T3))
call TriggerAddAction(bo[81],function T3)
set ao[82]=CreateTrigger()
call TriggerAddCondition(ao[82],Condition(function U3))
call TriggerAddAction(ao[82],function U3)
set Bo[82]=CreateTrigger()
call TriggerAddCondition(Bo[82],Condition(function V3))
call TriggerAddAction(Bo[82],function V3)
set bo[82]=CreateTrigger()
call TriggerAddCondition(bo[82],Condition(function W3))
call TriggerAddAction(bo[82],function W3)
set Co[82]=CreateTrigger()
call TriggerAddCondition(Co[82],Condition(function X3))
call TriggerAddAction(Co[82],function X3)
set Do[82]=CreateTrigger()
call TriggerAddCondition(Do[82],Condition(function Y3))
call TriggerAddAction(Do[82],function Y3)
set Go[82]=CreateTrigger()
call TriggerAddCondition(Go[82],Condition(function Z3))
call TriggerAddAction(Go[82],function Z3)
set Wo[86]=CreateTrigger()
call TriggerAddCondition(Wo[86],Condition(function eF))
call TriggerAddAction(Wo[86],function eF)
set mp[86]=CreateTrigger()
call TriggerAddCondition(mp[86],Condition(function dF))
set Wo[87]=CreateTrigger()
call TriggerAddCondition(Wo[87],Condition(function fF))
call TriggerAddAction(Wo[87],function fF)
set Wo[88]=CreateTrigger()
call TriggerAddCondition(Wo[88],Condition(function hF))
call TriggerAddAction(Wo[88],function hF)
set mp[88]=CreateTrigger()
call TriggerAddCondition(mp[88],Condition(function gF))
set mp[89]=CreateTrigger()
call TriggerAddCondition(mp[89],Condition(function iF))
set Wo[90]=CreateTrigger()
call TriggerAddCondition(Wo[90],Condition(function kF))
call TriggerAddAction(Wo[90],function kF)
set mp[90]=CreateTrigger()
call TriggerAddCondition(mp[90],Condition(function jF))
set Wo[91]=CreateTrigger()
call TriggerAddCondition(Wo[91],Condition(function nF))
call TriggerAddAction(Wo[91],function nF)
set mp[91]=CreateTrigger()
call TriggerAddCondition(mp[91],Condition(function mF))
set mp[93]=CreateTrigger()
call TriggerAddCondition(mp[93],Condition(function oF))
set Wo[94]=CreateTrigger()
call TriggerAddCondition(Wo[94],Condition(function qF))
call TriggerAddAction(Wo[94],function qF)
set mp[94]=CreateTrigger()
call TriggerAddCondition(mp[94],Condition(function pF))
set Po[97]=CreateTrigger()
call TriggerAddCondition(Po[97],Condition(function rF))
call TriggerAddAction(Po[97],function rF)
set Ro[97]=CreateTrigger()
call TriggerAddCondition(Ro[97],Condition(function sF))
call TriggerAddAction(Ro[97],function sF)
set Ro[98]=CreateTrigger()
call TriggerAddCondition(Ro[98],Condition(function tF))
call TriggerAddAction(Ro[98],function tF)
set Ro[99]=CreateTrigger()
call TriggerAddCondition(Ro[99],Condition(function uF))
call TriggerAddAction(Ro[99],function uF)
set So[99]=CreateTrigger()
call TriggerAddCondition(So[99],Condition(function vF))
call TriggerAddAction(So[99],function vF)
set Po[100]=CreateTrigger()
call TriggerAddCondition(Po[100],Condition(function wF))
call TriggerAddAction(Po[100],function wF)
set Qo[100]=CreateTrigger()
call TriggerAddCondition(Qo[100],Condition(function xF))
call TriggerAddAction(Qo[100],function xF)
set Ro[100]=CreateTrigger()
call TriggerAddCondition(Ro[100],Condition(function yF))
call TriggerAddAction(Ro[100],function yF)
set Uo[102]=CreateTrigger()
call TriggerAddCondition(Uo[102],Condition(function zF))
call TriggerAddAction(Uo[102],function zF)
set Vo[102]=CreateTrigger()
call TriggerAddCondition(Vo[102],Condition(function AF))
call TriggerAddAction(Vo[102],function AF)
set Oo[103]=CreateTrigger()
call TriggerAddCondition(Oo[103],Condition(function aF))
call TriggerAddAction(Oo[103],function aF)
set Po[103]=CreateTrigger()
call TriggerAddCondition(Po[103],Condition(function BF))
call TriggerAddAction(Po[103],function BF)
set To[103]=CreateTrigger()
call TriggerAddCondition(To[103],Condition(function bF))
call TriggerAddAction(To[103],function bF)
set Ro[103]=CreateTrigger()
call TriggerAddCondition(Ro[103],Condition(function CF))
call TriggerAddAction(Ro[103],function CF)
set Po[104]=CreateTrigger()
call TriggerAddCondition(Po[104],Condition(function cF))
call TriggerAddAction(Po[104],function cF)
set Ro[104]=CreateTrigger()
call TriggerAddCondition(Ro[104],Condition(function DF))
call TriggerAddAction(Ro[104],function DF)
set So[104]=CreateTrigger()
call TriggerAddCondition(So[104],Condition(function EF))
call TriggerAddAction(So[104],function EF)
set bp[1]=CreateTrigger()
call TriggerAddAction(bp[1],function FF)
call TriggerAddCondition(bp[1],Condition(function FF))
set Bp[1]=CreateTrigger()
call TriggerAddAction(Bp[1],function lF)
call TriggerAddCondition(Bp[1],Condition(function lF))
set Bp[2]=CreateTrigger()
call TriggerAddAction(Bp[2],function JF)
call TriggerAddCondition(Bp[2],Condition(function JF))
set Bp[3]=CreateTrigger()
call TriggerAddAction(Bp[3],function KF)
call TriggerAddCondition(Bp[3],Condition(function KF))
set Cp[1]=CreateTrigger()
call TriggerAddAction(Cp[1],function LF)
call TriggerAddCondition(Cp[1],Condition(function LF))
set Bp[4]=CreateTrigger()
call TriggerAddAction(Bp[4],function MF)
call TriggerAddCondition(Bp[4],Condition(function MF))
set Bp[5]=CreateTrigger()
call TriggerAddAction(Bp[5],function NF)
call TriggerAddCondition(Bp[5],Condition(function NF))
set Dp[1]=CreateTrigger()
call TriggerAddAction(Dp[1],function OF)
call TriggerAddCondition(Dp[1],Condition(function OF))
set Cp[2]=CreateTrigger()
call TriggerAddAction(Cp[2],function PF)
call TriggerAddCondition(Cp[2],Condition(function PF))
call ExecuteFunc("qA")
call ExecuteFunc("vA")
call ExecuteFunc("wA")
call ExecuteFunc("BA")
call ExecuteFunc("HA")
call ExecuteFunc("MB")
call ExecuteFunc("Bb")
call ExecuteFunc("Lb")
endfunction
