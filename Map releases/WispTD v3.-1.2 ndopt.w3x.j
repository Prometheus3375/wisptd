globals
constant integer e=$400
integer j=e
constant real o=GetCameraMargin(CAMERA_MARGIN_LEFT)
constant real A=GetCameraMargin(CAMERA_MARGIN_RIGHT)
constant real B=GetCameraMargin(CAMERA_MARGIN_BOTTOM)
constant real C=GetCameraMargin(CAMERA_MARGIN_TOP)
constant real D=.023/ 10.
constant real E=.071/ 128.
player F
constant integer I=$D00A6
constant integer J=$D00A5
constant integer K=$D00FA
constant integer L=$D0097
constant integer M=$D00FE
constant integer N=$D0057
constant integer O=$D0106
constant integer P=$D0012
constant integer Q=$D0004
constant integer R=$D0058
constant integer S=$D0008
constant integer U=$D0005
constant integer V=$D028B
constant integer W=$B
boolean X=true
constant integer Y=$A
player array Z
integer d4
constant playercolor e4=PLAYER_COLOR_RED
constant unittype f4=UNIT_TYPE_MECHANICAL
constant unittype g4=UNIT_TYPE_GIANT
constant unittype h4=UNIT_TYPE_SAPPER
constant unittype i4=UNIT_TYPE_TAUREN
constant unittype j4=UNIT_TYPE_ANCIENT
boolexpr k4
integer m4
integer array n4
constant real o4=-1472.
constant real p4=-1792.
constant real q4=-1920.
constant real r4=-8448.
constant real s4=p4
constant integer t4=256/ 64
constant integer u4=R2I(1024.)/ 64
constant integer v4=R2I(640.-s4)/ 64
constant integer w4=608-u4*t4
constant integer x4=w4+u4
integer y4=0
constant integer z4=$7D0
constant integer A4=N
constant integer a4=R
integer array B4
boolean array b4
player C4=null
constant integer c4=$D
constant integer D4=0+StringLength("Spawn")
constant integer E4=D4
constant integer F4=E4+2
constant integer G4=0+StringLength("Upgrade")
constant integer H4=G4
constant integer I4=H4+2
constant integer l4=I4+1
constant integer J4=l4+1
constant integer K4=J4+1
constant integer L4=-25
constant integer M4=-25
integer N4=0
integer O4=0
constant integer P4=51-1
integer array Q4
integer R4
constant real S4=1./ 16.
boolexpr array T4
integer array U4
constant integer V4=$A
constant integer W4=$B
constant integer X4=$C
constant integer Y4=$D
constant integer Z4=$E
constant integer d7=$F
constant integer e7=51-1
constant integer f7=$80
integer array g7
trigger array h7
trigger array i7
integer array j7
integer array k7
trigger array m7
integer array n7
constant integer o7=$A
integer array p7
constant timer q7=CreateTimer()
constant hashtable r7=InitHashtable()
region s7
region t7
integer u7=-1
integer v7=0
integer w7=1
integer x7=61
constant integer y7=$B
constant trigger z7=CreateTrigger()
constant trigger A7=CreateTrigger()
constant trigger a7=CreateTrigger()
constant trigger B7=CreateTrigger()
constant trigger b7=CreateTrigger()
constant trigger C7=CreateTrigger()
boolexpr c7
constant trigger D7=CreateTrigger()
constant trigger E7=CreateTrigger()
constant integer F7=StringLength("inc")
constant integer G7=F7+1
integer H7
boolexpr I7
constant trigger l7=CreateTrigger()
constant integer J7=StringLength("gold")
constant integer K7=J7+1
constant trigger L7=CreateTrigger()
constant integer M7=StringLength("exp")
constant integer N7=M7+1
constant trigger O7=CreateTrigger()
constant integer P7=StringLength("lives")
constant integer Q7=P7+1
constant trigger R7=CreateTrigger()
constant integer S7=StringLength("wave")
constant integer T7=S7+1
constant trigger U7=CreateTrigger()
constant integer V7=StringLength("min")
constant integer W7=V7+1
constant trigger X7=CreateTrigger()
integer Y7
constant trigger Z7=CreateTrigger()
constant trigger d8=CreateTrigger()
constant trigger e8=CreateTrigger()
constant trigger f8=CreateTrigger()
constant trigger g8=CreateTrigger()
constant trigger h8=CreateTrigger()
constant trigger i8=CreateTrigger()
constant trigger j8=CreateTrigger()
constant trigger k8=CreateTrigger()
constant trigger m8=CreateTrigger()
constant trigger n8=CreateTrigger()
constant trigger o8=CreateTrigger()
constant trigger p8=CreateTrigger()
constant trigger q8=CreateTrigger()
constant trigger r8=CreateTrigger()
constant trigger s8=CreateTrigger()
constant trigger t8=CreateTrigger()
sound u8
sound v8
sound w8
sound x8
sound y8
sound z8
sound A8
sound a8
constant real B8=-8960.
constant real b8=-1920.
constant real C8=(B8+6912.)/ 2.
constant real c8=(b8+1920.)/ 2.
constant trigger D8=CreateTrigger()
constant trigger E8=CreateTrigger()
multiboard F8
integer G8=0
constant integer H8=$A
constant real I8=2.*.0041
constant real l8=14.*.0041
constant real J8=3.*.0041
constant real K8=4.*.0041
constant real L8=6.*.0041
constant real M8=6.*.0041
constant real N8=6.*.0041
constant real O8=5.*.0041
constant real P8=5.*.0041
constant real Q8=4.*.0041
constant real R8=9.*.0041
constant real S8=6.*.0041
multiboarditem T8
constant integer U8=$FF
constant trigger V8=CreateTrigger()
constant trigger W8=CreateTrigger()
integer array X8
integer Y8=-1
integer array Z8
integer d9=-1
integer array e9
integer f9
integer g9=0
integer h9=0
integer array i9
integer array j9
integer array k9
integer array m9
integer n9=0
integer o9=0
integer array p9
integer array q9
integer array r9
integer array s9
integer t9=0
integer u9=0
integer array v9
integer array w9
integer array x9
integer array y9
integer z9=0
integer A9=0
integer array a9
integer array B9
integer array b9
integer array C9
integer c9=0
integer D9=0
integer array E9
integer array F9
integer array G9
integer array H9
integer I9=0
integer l9=0
integer array J9
integer array K9
integer array L9
integer array M9
integer N9=0
integer O9=0
integer array P9
boolean array Q9
integer array R9
integer S9=-1
integer T9
integer U9
integer V9
integer W9
integer X9
integer Y9
integer Z9
integer ed
integer fd
integer gd
integer hd
integer jd
integer array kd
integer array md
integer array nd
string array od
string array pd
integer qd=0
integer rd=0
integer array sd
real array td
integer array ud
integer vd=0
integer wd=0
integer array xd
integer array yd
integer array zd
integer array Ad
integer array ad
integer array Bd
integer array bd
string array Cd
integer Dd=0
integer Ed=0
integer array Fd
integer array Gd
integer array Hd
integer array Id
integer array ld
integer array Jd
boolean array Kd
boolean array Ld
integer array Md
integer array Nd
integer array Od
integer array Pd
integer array Qd
integer array Rd
integer Sd=0
integer Td=0
integer array Ud
integer array Vd
integer array Wd
integer Xd=0
integer Yd=0
integer array Zd
integer array de
integer array ee
integer array fe
real array ge
real array he
real array ie
real array je
rect array ke
rect array me
rect array ne
rect array oe
rect array pe
fogmodifier array qe
integer array re
fogmodifier array se
integer array te
boolean array ue
boolean array ve
integer we=0
integer xe=0
integer array ye
unit array ze
integer array Ae
timer array ae
integer array Be
integer array be
integer Ce=0
integer ce=0
integer array De
integer array Ee
integer array Fe
integer array Ge
integer array He
real array Ie
integer array le
integer array Je
real array Ke
integer Le=0
integer Me=0
integer array Ne
integer array Oe
integer array Pe
integer array Qe
integer array Re
integer array Se
real array Te
integer array Ue
integer array Ve
integer array We
real array Xe
integer array Ye
integer array Ze
integer array df
boolean array ef
integer array ff
integer array gf
integer array hf
boolean array jf
integer array kf
integer array mf
integer array nf
integer array of
integer array pf
integer array qf
integer array rf
integer array sf
integer array tf
integer array uf
integer array vf
integer array wf
boolean array xf
integer array yf
integer array zf
integer array Af
boolean array af
integer array Bf
integer array bf
integer array Cf
boolean array cf
integer array Df
integer array Ef
integer array Ff
boolean array Gf
integer Hf=0
integer If=0
integer array lf
player array Jf
integer array Kf
string array Lf
integer array Mf
integer array Nf
boolean array Of
boolean array Pf
player array Qf
boolean array Rf
integer array Sf
integer array Tf
integer array Uf
integer array Vf
integer array Wf
integer array Xf
real array Yf
integer array Zf
integer array dg
integer array eg
integer array fg
multiboarditem array gg
multiboarditem array hg
multiboarditem array ig
multiboarditem array jg
multiboarditem array kg
multiboarditem array mg
multiboarditem array ng
multiboarditem array og
multiboarditem array pg
image array qg
integer array rg
boolean array sg
unit array tg
unit array ug
unit array vg
unit array wg
unit array xg
integer array yg
integer array zg
integer array Ag
integer array ag
integer array Bg
integer array bg
integer array Cg
integer array cg
integer array Dg
integer array Eg
integer array Fg
integer array Gg
integer array Hg
integer array Ig
integer array lg
integer array Jg
integer array Kg
integer array Lg
integer array Mg
integer array Ng
integer array Og
integer array Pg
integer array Qg
integer array Rg
integer array Sg
integer array Tg
item array Ug
item array Vg
item array Wg
integer array Xg
timer array Yg
integer Zg=0
integer dh=0
integer array eh
integer fh
integer gh
integer array hh
integer array ih
integer array jh
integer array kh
unit array mh
integer array nh
integer array oh
integer array ph
boolean array qh
boolean array rh
real array sh
real array th
real array uh
integer array vh
real array wh
real array xh
real array yh
item array zh
item array Ah
item array ah
item array Bh
item array bh
item array Ch
real array ch
real array Dh
real array Eh
integer array Fh
integer array Gh
timer array Hh
boolexpr Ih
integer lh=0
integer Jh=0
integer array Kh
integer array Lh
integer array Mh
unit array Nh
integer array Oh
integer array Ph
integer array Qh
boolean array Rh
integer array Sh
integer array Th
timer array Uh
integer Vh=0
integer Wh=0
integer array Xh
integer array Yh
unit array Zh
integer array di
integer array ei
integer fi=0
integer gi=0
integer array hi
integer array ii
integer array ji
real array ki
integer array mi
integer array ni
integer oi=0
integer pi=0
integer array qi
integer array ri
integer array si
boolean array ti
boolean array ui
integer vi=0
integer wi=0
integer array xi
boolean array yi
boolean array zi
integer array Ai
integer array Bi
real array bi
real array Ci
integer array ci
integer Di=0
integer Ei=0
integer array Fi
unit array Gi
integer array Hi
integer Ii=0
integer li=0
integer array Ji
constant real Ki=1./ 64.
constant integer Li=$CC
constant integer Mi=$CC
unit array Ni
image array Oi
timer array Pi
integer Qi=0
integer Ri=0
integer array Si
integer Ti
integer array Ui
integer array Vi
boolean array Wi
timer array Xi
constant integer Yi=$A
boolean array Zi
integer array dj
real array ej
constant integer fj=$F
boolexpr gj
integer array hj
integer array ij
integer jj
boolean kj
constant real mj=-.15
integer array nj
integer array oj
boolexpr pj
boolean array qj
real array rj
integer array sj
integer array tj
integer array uj
integer vj
integer wj
constant integer xj=K
real array yj
integer array zj
boolexpr Aj
integer aj
integer Bj
real bj
real array Cj
boolexpr cj
code Dj
timer array Ej
integer Fj
integer Gj
real Hj
integer Ij=0
integer lj=0
integer array Jj
unit Kj
integer Lj
real Mj
real Nj
integer array Oj
unit array Pj
integer array Qj
integer array Rj
boolean array Sj
real array Tj
real array Uj
real array Vj
real array Wj
integer array Xj
real array Yj
integer array Zj
integer array dk
integer array ek
timer array fk
image array gk
integer array hk
boolean array ik
integer array jk
integer array kk
integer array mk
integer array nk
item array ok
item array pk
item array qk
item array rk
integer array sk
integer array tk
code uk
boolexpr vk
integer wk
boolexpr xk
constant integer yk=$A
integer array zk
constant integer Ak=$A
constant integer ak=$A
integer Bk=0
integer bk=0
integer array Ck
integer array ck
boolean array Dk
boolean array Ek
boolean array Fk
real array Gk
real array Hk
real array Ik
integer lk=0
integer Jk=0
integer array Kk
integer Lk
integer array Mk
integer array Nk
constant integer Ok=M
code Pk
item array Qk
real array Rk
real array Sk
real array Tk
real array Uk
integer array Vk
timer array Wk
constant real Xk=-.3
constant integer Yk=-25
real array Zk
constant real dm=-.8
integer array em
integer array fm
unit array gm
unit array hm
boolean array im
real array jm
integer array km
item array mm
timer array nm
integer array om
integer array pm
timer array qm
constant integer rm=$80
constant integer sm=$CC
constant integer tm=$FF
constant integer um=$A
code vm
code wm
integer array xm
real array ym
image array zm
integer array Am
real array am
unit array Bm
effect array bm
integer array Cm
integer array cm
integer array Dm
timer array Em
boolexpr Fm
integer Gm
boolexpr Hm
integer Im
integer lm
boolexpr Jm
real Km
boolean Lm
real array Mm
real array Nm
code Om
code Pm
real array Qm
real array Rm
real array Sm
timer array Tm
timer array Um
constant integer Vm=J
constant integer Wm=I
constant integer Xm=$A
constant real Ym=1.-.1
constant real Zm=-.01
integer array dn
real array en
real array fn
boolean array gn
integer array hn
item array jn
integer array kn
timer array mn
integer array nn
integer array on
integer array pn
integer array qn
trigger array rn
integer array sn
integer tn=0
integer un=0
integer array vn
constant integer wn=0+StringLength("AbilityUpgrade")
constant integer xn=wn
constant integer yn=xn+2
constant integer zn=yn+1
constant integer An=zn+1
constant integer an=An+1
constant integer Bn=0+StringLength("HeroAbility")
constant integer bn=Bn
constant integer Cn=bn+2
integer cn
integer array Dn
integer array En
integer array Fn
integer array Gn
integer array Hn
string array In
integer array ln
integer array Jn
integer array Kn
integer array Ln
integer array Mn
integer array Nn
integer array On
integer array Pn
integer array Qn
integer array Rn
trigger array Sn
integer array Tn
integer Un=0
integer Vn=0
integer array Wn
integer array Xn
integer array Yn
integer array Zn
real array do
integer array eo
boolean array fo
boolean array go
integer array ho
integer array io
real array jo
integer array ko
integer array mo
integer array no
integer array oo
integer array po
integer array qo
integer array ro
integer array so
real array to
real array uo
real array vo
real array wo
real array xo
integer array yo
integer array zo
boolean array Ao
integer array ao
integer Bo=0
integer bo=0
integer array Co
integer array co
integer array Do
integer array Eo
integer array Fo
integer array Go
integer array Ho
integer array Io
integer array lo
integer array Jo
integer array Ko
trigger array Lo
integer array Mo
integer No=0
integer Oo=0
integer array Po
integer array Qo
integer array Ro
integer array So
integer array To
integer array Uo
integer array Vo
string array Wo
real array Xo
integer array Yo
real array Zo
integer array dp
boolexpr array ep
real array fp
integer array gp
boolean array hp
integer array ip
integer array jp
real array kp
real array mp
integer array np
integer array op
boolean array pp
integer array qp
integer array rp
boolean array sp
integer array tp
boolean array vp
boolean array wp
integer array xp
integer array yp
integer array zp
integer array Ap
integer array ap
integer array Bp
trigger array bp
trigger array Cp
trigger array cp
trigger array Dp
trigger array Ep
trigger array Fp
trigger array Gp
trigger array Hp
trigger array Ip
trigger array lp
trigger array Jp
trigger array Kp
trigger array Lp
trigger array Mp
trigger array Np
trigger array Op
trigger array Pp
trigger array Qp
trigger array Rp
trigger array Sp
trigger array Tp
trigger array Up
trigger array Vp
trigger array Wp
trigger array Xp
trigger array Yp
trigger array Zp
trigger array dq
trigger array eq
integer array fq
trigger array gq
integer array hq
trigger array iq
integer array jq
trigger array kq
integer array mq
trigger array nq
integer array oq
trigger array pq
integer array qq
trigger array rq
trigger sq
trigger tq
trigger uq
trigger vq
trigger wq
trigger xq
trigger yq
trigger zq
trigger Aq
trigger aq
trigger Bq
trigger bq
trigger Cq
trigger cq
trigger Dq
trigger array Eq
trigger array Fq
trigger array Gq
trigger array Hq
trigger array Iq
integer lq
integer Jq
integer Kq
integer Lq
player Mq
real Nq
boolean Oq
integer Pq
real Qq
boolean Rq
endglobals
native UnitAlive takes unit id returns boolean
function ur takes nothing returns integer
local integer vr=No
if(vr!=0)then
set No=Po[vr]
else
set Oo=Oo+1
set vr=Oo
endif
if(vr>$AA9)then
return 0
endif
set So[vr]=(vr-1)*3
set Po[vr]=-1
return vr
endfunction
function wr takes nothing returns integer
local integer vr=Bo
if(vr!=0)then
set Bo=Co[vr]
else
set bo=bo+1
set vr=bo
endif
if(vr>$AA9)then
return 0
endif
set Do[vr]=(vr-1)*3
set Fo[vr]=(vr-1)*3
set Ho[vr]=(vr-1)*3
set lo[vr]=(vr-1)*3
set Ko[vr]=(vr-1)*3
set Mo[vr]=(vr-1)*3
set Co[vr]=-1
return vr
endfunction
function xr takes nothing returns integer
local integer vr=Un
if(vr!=0)then
set Un=Wn[vr]
else
set Vn=Vn+1
set vr=Vn
endif
if(vr>$AA9)then
return 0
endif
set ao[vr]=(vr-1)*3
set Wn[vr]=-1
return vr
endfunction
function yr takes nothing returns integer
local integer vr=tn
if(vr!=0)then
set tn=vn[vr]
else
set un=un+1
set vr=un
endif
if(vr>$FFE)then
return 0
endif
set Jn[vr]=(vr-1)*2
set Ln[vr]=(vr-1)*2
set Nn[vr]=(vr-1)*2
set Pn[vr]=(vr-1)*2
set Rn[vr]=(vr-1)*2
set Tn[vr]=(vr-1)*2
set vn[vr]=-1
return vr
endfunction
function zr takes nothing returns integer
local integer vr=g9
if(vr!=0)then
set g9=i9[vr]
else
set h9=h9+1
set vr=h9
endif
if(vr>67)then
return 0
endif
set k9[vr]=(vr-1)*120
set m9[vr]=-1
set i9[vr]=-1
return vr
endfunction
function Ar takes nothing returns integer
local integer vr=n9
if(vr!=0)then
set n9=p9[vr]
else
set o9=o9+1
set vr=o9
endif
if(vr>77)then
return 0
endif
set r9[vr]=(vr-1)*105
set s9[vr]=-1
set p9[vr]=-1
return vr
endfunction
function ar takes nothing returns integer
local integer vr=t9
if(vr!=0)then
set t9=v9[vr]
else
set u9=u9+1
set vr=u9
endif
if(vr>77)then
return 0
endif
set x9[vr]=(vr-1)*105
set y9[vr]=-1
set v9[vr]=-1
return vr
endfunction
function Br takes nothing returns integer
local integer vr=z9
if(vr!=0)then
set z9=a9[vr]
else
set A9=A9+1
set vr=A9
endif
if(vr>77)then
return 0
endif
set b9[vr]=(vr-1)*105
set C9[vr]=-1
set a9[vr]=-1
return vr
endfunction
function br takes nothing returns integer
local integer vr=c9
if(vr!=0)then
set c9=E9[vr]
else
set D9=D9+1
set vr=D9
endif
if(vr>77)then
return 0
endif
set G9[vr]=(vr-1)*105
set H9[vr]=-1
set E9[vr]=-1
return vr
endfunction
function Cr takes nothing returns integer
local integer vr=I9
if(vr!=0)then
set I9=J9[vr]
else
set l9=l9+1
set vr=l9
endif
if(vr>77)then
return 0
endif
set L9[vr]=(vr-1)*105
set M9[vr]=-1
set J9[vr]=-1
return vr
endfunction
function cr takes nothing returns integer
local integer vr=N9
if(vr!=0)then
set N9=P9[vr]
else
set O9=O9+1
set vr=O9
endif
if(vr>$FFE)then
return 0
endif
set R9[vr]=(vr-1)*2
set P9[vr]=-1
return vr
endfunction
function Dr takes nothing returns integer
local integer vr=qd
if(vr!=0)then
set qd=sd[vr]
else
set rd=rd+1
set vr=rd
endif
if(vr>$9F)then
return 0
endif
set ud[vr]=(vr-1)*51
set sd[vr]=-1
return vr
endfunction
function Er takes integer m,integer T returns integer
set Jq=m
set Kq=T
call TriggerEvaluate(Dq)
return lq
endfunction
function Fr takes integer vr returns nothing
set Pq=vr
call TriggerEvaluate(eq[qq[vr]])
endfunction
function Gr takes nothing returns integer
local integer vr=lk
if(vr!=0)then
set lk=Kk[vr]
else
set Jk=Jk+1
set vr=Jk
endif
if(vr>8190)then
return 0
endif
set qq[vr]=85
set Kk[vr]=-1
return vr
endfunction
function Hr takes integer vr returns nothing
if vr==null then
return
elseif(Kk[vr]!=-1)then
return
endif
set Pq=vr
call TriggerEvaluate(rq[qq[vr]])
set Kk[vr]=lk
set lk=vr
endfunction
function Ir takes nothing returns integer
local integer vr=Bk
if(vr!=0)then
set Bk=Ck[vr]
else
set bk=bk+1
set vr=bk
endif
if(vr>8190)then
return 0
endif
set Ck[vr]=-1
return vr
endfunction
function lr takes nothing returns integer
local integer vr=vd
if(vr!=0)then
set vd=xd[vr]
else
set wd=wd+1
set vr=wd
endif
if(vr>$7FE)then
return 0
endif
set zd[vr]=(vr-1)*4
set xd[vr]=-1
return vr
endfunction
function Jr takes nothing returns integer
local integer vr=Dd
if(vr!=0)then
set Dd=Fd[vr]
else
set Ed=Ed+1
set vr=Ed
endif
if(vr>8190)then
return 0
endif
set Kd[vr]=true
set Fd[vr]=-1
return vr
endfunction
function Kr takes integer vr returns nothing
if vr==null then
return
elseif(Fd[vr]!=-1)then
return
endif
set Fd[vr]=Dd
set Dd=vr
endfunction
function Lr takes nothing returns integer
local integer vr=Sd
if(vr!=0)then
set Sd=Ud[vr]
else
set Td=Td+1
set vr=Td
endif
if(vr>8190)then
return 0
endif
set Vd[vr]=0
set Ud[vr]=-1
return vr
endfunction
function Mr takes integer vr returns nothing
if vr==null then
return
elseif(Ud[vr]!=-1)then
return
endif
set Ud[vr]=Sd
set Sd=vr
endfunction
function Nr takes integer vr returns nothing
set Pq=vr
call TriggerExecute(sq)
endfunction
function Pr takes integer vr returns nothing
set Pq=vr
call TriggerExecute(tq)
endfunction
function Qr takes integer vr returns nothing
set Pq=vr
call TriggerExecute(uq)
endfunction
function Rr takes integer vr returns nothing
set Pq=vr
call TriggerExecute(vq)
endfunction
function Sr takes integer vr,integer q returns nothing
set Pq=vr
set Jq=q
call TriggerExecute(wq)
endfunction
function Tr takes nothing returns integer
local integer vr=Xd
if(vr!=0)then
set Xd=Zd[vr]
else
set Yd=Yd+1
set vr=Yd
endif
if(vr>$C)then
return 0
endif
set fe[vr]=(vr-1)*608
set re[vr]=(vr-1)*$A
set te[vr]=(vr-1)*$A
set Zd[vr]=-1
return vr
endfunction
function Ur takes nothing returns integer
local integer vr=we
if(vr!=0)then
set we=ye[vr]
else
set xe=xe+1
set vr=xe
endif
if(vr>8190)then
return 0
endif
set Ae[vr]=$F7
set ye[vr]=-1
return vr
endfunction
function Vr takes integer vr returns nothing
if vr==null then
return
elseif(ye[vr]!=-1)then
return
endif
set ye[vr]=we
set we=vr
endfunction
function Wr takes nothing returns integer
local integer vr=Ce
if(vr!=0)then
set Ce=De[vr]
else
set ce=ce+1
set vr=ce
endif
if(vr>8190)then
return 0
endif
set Ge[vr]=0
set He[vr]=0
set fq[vr]=31
set De[vr]=-1
return vr
endfunction
function Xr takes integer vr returns nothing
if vr==null then
return
elseif(De[vr]!=-1)then
return
endif
set Pq=vr
call TriggerEvaluate(gq[fq[vr]])
set De[vr]=Ce
set Ce=vr
endfunction
function Yr takes nothing returns integer
local integer vr=Wr()
local integer Zr
if(vr==0)then
return 0
endif
set fq[vr]=32
set Zr=vr
set Ie[vr]=.0
return vr
endfunction
function ds takes nothing returns integer
local integer vr=Wr()
local integer Zr
if(vr==0)then
return 0
endif
set fq[vr]=33
set Zr=vr
set le[vr]=0
return vr
endfunction
function es takes integer vr returns real
set Pq=vr
call TriggerEvaluate(Sp[oq[vr]])
return Qq
endfunction
function fs takes integer vr returns real
set Pq=vr
call TriggerEvaluate(Tp[oq[vr]])
return Qq
endfunction
function gs takes integer vr returns real
set Pq=vr
call TriggerEvaluate(Up[oq[vr]])
return Qq
endfunction
function hs takes integer vr returns nothing
set Pq=vr
call TriggerEvaluate(Vp[oq[vr]])
endfunction
function is takes integer vr,integer m returns nothing
set Pq=vr
set Jq=m
call TriggerEvaluate(Wp[oq[vr]])
endfunction
function js takes integer vr,integer ks returns nothing
set Pq=vr
set Jq=ks
call TriggerExecute(Xp[oq[vr]])
endfunction
function ms takes integer vr,integer m returns nothing
set Pq=vr
set Jq=m
call TriggerExecute(Yp[oq[vr]])
endfunction
function ns takes integer vr returns nothing
set Pq=vr
call TriggerEvaluate(Zp[oq[vr]])
endfunction
function os takes integer vr returns nothing
set Pq=vr
call TriggerEvaluate(dq[oq[vr]])
endfunction
function ps takes nothing returns integer
local integer vr=Ij
if(vr!=0)then
set Ij=Jj[vr]
else
set lj=lj+1
set vr=lj
endif
if(vr>8190)then
return 0
endif
set Sj[vr]=false
set Yj[vr]=1.
set hk[vr]=0
set jk[vr]=0
set kk[vr]=0
set sk[vr]=0
set oq[vr]=70
set Jj[vr]=-1
return vr
endfunction
function qs takes integer vr returns nothing
if vr==null then
return
elseif(Jj[vr]!=-1)then
return
endif
set Pq=vr
call TriggerEvaluate(pq[oq[vr]])
set Jj[vr]=Ij
set Ij=vr
endfunction
function rs takes nothing returns integer
local integer vr=Wr()
local integer Zr
if(vr==0)then
return 0
endif
set fq[vr]=34
set Zr=vr
set Je[vr]=0
set Ke[vr]=.0
return vr
endfunction
function ss takes nothing returns integer
local integer vr=Le
if(vr!=0)then
set Le=Ne[vr]
else
set Me=Me+1
set vr=Me
endif
if(vr>8190)then
return 0
endif
set Oe[vr]=0
set Qe[vr]=0
set Se[vr]=0
set Te[vr]=.0
set We[vr]=0
set Xe[vr]=.0
set Ze[vr]=0
set df[vr]=0
set ef[vr]=false
set gf[vr]=0
set hf[vr]=0
set jf[vr]=false
set mf[vr]=0
set nf[vr]=0
set pf[vr]=0
set qf[vr]=0
set sf[vr]=0
set tf[vr]=0
set vf[vr]=0
set wf[vr]=0
set xf[vr]=false
set zf[vr]=0
set Af[vr]=0
set af[vr]=false
set bf[vr]=0
set Cf[vr]=0
set cf[vr]=false
set Ef[vr]=0
set Ff[vr]=0
set Gf[vr]=false
set Ne[vr]=-1
return vr
endfunction
function ts takes player p,integer i returns nothing
set Mq=p
set Jq=i
call TriggerExecute(yq)
endfunction
function us takes integer vr returns nothing
set Pq=vr
call TriggerExecute(zq)
endfunction
function vs takes integer vr returns nothing
set Pq=vr
call TriggerExecute(Aq)
endfunction
function ws takes nothing returns integer
local integer vr=Hf
if(vr!=0)then
set Hf=lf[vr]
else
set If=If+1
set vr=If
endif
if(vr>67)then
return 0
endif
set rg[vr]=(vr-1)*120
set Dg[vr]=(vr-1)*32
set Fg[vr]=(vr-1)*32
set Hg[vr]=(vr-1)*32
set lg[vr]=(vr-1)*32
set Lg[vr]=(vr-1)*24
set Of[vr]=true
set Rf[vr]=false
set Vf[vr]=z4
set Wf[vr]=0
set Xf[vr]=40
set Yf[vr]=.0
set Zf[vr]=500
set dg[vr]=50
set eg[vr]=0
set sg[vr]=true
set Cg[vr]=0
set Jg[vr]=0
set Mg[vr]=0
set Ng[vr]=0
set Xg[vr]=0
set lf[vr]=-1
return vr
endfunction
function xs takes integer vr returns nothing
set Pq=vr
call TriggerEvaluate(Np[mq[vr]])
endfunction
function ys takes integer vr,integer zs returns nothing
set Pq=vr
set Jq=zs
call TriggerEvaluate(Op[mq[vr]])
endfunction
function As takes integer vr,integer zs,integer as,integer Bs returns boolean
set Pq=vr
set Jq=zs
set Kq=as
set Lq=Bs
call TriggerEvaluate(Pp[mq[vr]])
return Rq
endfunction
function bs takes integer vr,real r returns nothing
set Pq=vr
set Nq=r
call TriggerEvaluate(cq)
endfunction
function Cs takes integer vr returns nothing
set Pq=vr
call TriggerEvaluate(Qp[mq[vr]])
endfunction
function cs takes integer vr returns nothing
set Pq=vr
call TriggerEvaluate(Rp[mq[vr]])
endfunction
function Ds takes nothing returns integer
local integer vr=Qi
if(vr!=0)then
set Qi=Si[vr]
else
set Ri=Ri+1
set vr=Ri
endif
if(vr>8190)then
return 0
endif
set Wi[vr]=false
set mq[vr]=61
set Si[vr]=-1
return vr
endfunction
function Es takes integer vr returns nothing
if vr==null then
return
elseif(Si[vr]!=-1)then
return
endif
set Pq=vr
call TriggerEvaluate(nq[mq[vr]])
set Si[vr]=Qi
set Qi=vr
endfunction
function Fs takes nothing returns integer
local integer vr=Ii
if(vr!=0)then
set Ii=Ji[vr]
else
set li=li+1
set vr=li
endif
if(vr>8190)then
return 0
endif
set Ji[vr]=-1
return vr
endfunction
function Gs takes nothing returns integer
local integer vr=Di
if(vr!=0)then
set Di=Fi[vr]
else
set Ei=Ei+1
set vr=Ei
endif
if(vr>8190)then
return 0
endif
set Fi[vr]=-1
return vr
endfunction
function Hs takes nothing returns integer
local integer vr=vi
if(vr!=0)then
set vi=xi[vr]
else
set wi=wi+1
set vr=wi
endif
if(vr>8190)then
return 0
endif
set xi[vr]=-1
return vr
endfunction
function Is takes integer vr returns nothing
set Pq=vr
call TriggerEvaluate(Cp[hq[vr]])
endfunction
function ls takes integer vr returns real
set Pq=vr
call TriggerEvaluate(cp[hq[vr]])
return Qq
endfunction
function Js takes integer vr returns integer
set Pq=vr
call TriggerEvaluate(Dp[hq[vr]])
return lq
endfunction
function Ks takes integer vr returns integer
set Pq=vr
call TriggerEvaluate(Ep[hq[vr]])
return lq
endfunction
function Ls takes integer vr returns boolean
set Pq=vr
call TriggerEvaluate(Fp[hq[vr]])
return Rq
endfunction
function Ms takes integer vr returns boolean
set Pq=vr
call TriggerEvaluate(Gp[hq[vr]])
return Rq
endfunction
function Ns takes integer vr returns boolean
set Pq=vr
call TriggerEvaluate(Hp[hq[vr]])
return Rq
endfunction
function Os takes integer vr returns boolean
set Pq=vr
call TriggerEvaluate(Ip[hq[vr]])
return Rq
endfunction
function Ps takes integer vr returns boolean
set Pq=vr
call TriggerEvaluate(lp[hq[vr]])
return Rq
endfunction
function Qs takes integer vr,real Rs returns real
set Pq=vr
set Nq=Rs
call TriggerEvaluate(Jp[hq[vr]])
return Qq
endfunction
function Ss takes integer vr,real Rs returns real
set Pq=vr
set Nq=Rs
call TriggerEvaluate(Kp[hq[vr]])
return Qq
endfunction
function Ts takes integer vr,real Rs returns real
set Pq=vr
set Nq=Rs
call TriggerEvaluate(Lp[hq[vr]])
return Qq
endfunction
function Us takes integer vr,boolean Vs returns nothing
set Pq=vr
set Oq=Vs
call TriggerExecute(aq)
endfunction
function Ws takes nothing returns integer
local integer vr=Zg
if(vr!=0)then
set Zg=eh[vr]
else
set dh=dh+1
set vr=dh
endif
if(vr>8190)then
return 0
endif
set ph[vr]=0
set qh[vr]=false
set rh[vr]=true
set hq[vr]=43
set eh[vr]=-1
return vr
endfunction
function Xs takes integer vr returns nothing
if vr==null then
return
elseif(eh[vr]!=-1)then
return
endif
set Pq=vr
call TriggerEvaluate(iq[hq[vr]])
set eh[vr]=Zg
set Zg=vr
endfunction
function Ys takes integer vr returns boolean
set Pq=vr
call TriggerEvaluate(Mp[jq[vr]])
return Rq
endfunction
function Zs takes nothing returns integer
local integer vr=lh
if(vr!=0)then
set lh=Kh[vr]
else
set Jh=Jh+1
set vr=Jh
endif
if(vr>8190)then
return 0
endif
set Qh[vr]=0
set Rh[vr]=false
set jq[vr]=44
set Kh[vr]=-1
return vr
endfunction
function dt takes integer vr returns nothing
if vr==null then
return
elseif(Kh[vr]!=-1)then
return
endif
set Pq=vr
call TriggerEvaluate(kq[jq[vr]])
set Kh[vr]=lh
set lh=vr
endfunction
function et takes nothing returns integer
local integer vr=Zs()
local integer Zr
if(vr==0)then
return 0
endif
set jq[vr]=54
set Zr=vr
set ui[vr]=false
return vr
endfunction
function ft takes nothing returns integer
local integer vr=Zs()
local integer Zr
if(vr==0)then
return 0
endif
set jq[vr]=53
set Zr=vr
return vr
endfunction
function gt takes nothing returns integer
local integer vr=Zs()
local integer Zr
if(vr==0)then
return 0
endif
set jq[vr]=52
set Zr=vr
return vr
endfunction
function ht takes nothing returns integer
local integer vr=Zs()
local integer Zr
if(vr==0)then
return 0
endif
set jq[vr]=51
set Zr=vr
set ti[vr]=false
return vr
endfunction
function it takes nothing returns integer
local integer vr=Zs()
local integer Zr
if(vr==0)then
return 0
endif
set jq[vr]=50
set Zr=vr
return vr
endfunction
function jt takes nothing returns integer
local integer vr=Zs()
local integer Zr
if(vr==0)then
return 0
endif
set jq[vr]=49
set Zr=vr
return vr
endfunction
function kt takes nothing returns integer
local integer vr=Zs()
local integer Zr
if(vr==0)then
return 0
endif
set jq[vr]=48
set Zr=vr
return vr
endfunction
function mt takes nothing returns integer
local integer vr=oi
if(vr!=0)then
set oi=qi[vr]
else
set pi=pi+1
set vr=pi
endif
if(vr>8190)then
return 0
endif
set qi[vr]=-1
return vr
endfunction
function nt takes integer vr returns nothing
if vr==null then
return
elseif(qi[vr]!=-1)then
return
endif
set qi[vr]=oi
set oi=vr
endfunction
function ot takes nothing returns integer
local integer vr=fi
if(vr!=0)then
set fi=hi[vr]
else
set gi=gi+1
set vr=gi
endif
if(vr>8190)then
return 0
endif
set mi[vr]=0
set ni[vr]=0
set hi[vr]=-1
return vr
endfunction
function pt takes integer vr returns nothing
if vr==null then
return
elseif(hi[vr]!=-1)then
return
endif
set hi[vr]=fi
set fi=vr
endfunction
function qt takes nothing returns integer
local integer vr=Vh
if(vr!=0)then
set Vh=Xh[vr]
else
set Wh=Wh+1
set vr=Wh
endif
if(vr>8190)then
return 0
endif
set Xh[vr]=-1
return vr
endfunction
function rt takes nothing returns integer
local integer vr=ps()
local integer Zr
if(vr==0)then
return 0
endif
set oq[vr]=105
set Zr=vr
return vr
endfunction
function tt takes nothing returns integer
local integer vr=Zs()
local integer Zr
if(vr==0)then
return 0
endif
set jq[vr]=55
set Zr=vr
return vr
endfunction
function ut takes nothing returns integer
local integer vr=Zs()
local integer Zr
if(vr==0)then
return 0
endif
set jq[vr]=56
set Zr=vr
return vr
endfunction
function vt takes nothing returns integer
local integer vr=Zs()
local integer Zr
if(vr==0)then
return 0
endif
set jq[vr]=57
set Zr=vr
return vr
endfunction
function wt takes nothing returns integer
local integer vr=Ds()
local integer Zr
if(vr==0)then
return 0
endif
set mq[vr]=62
set Zr=vr
return vr
endfunction
function xt takes nothing returns integer
local integer vr=Ds()
local integer Zr
if(vr==0)then
return 0
endif
set mq[vr]=63
set Zr=vr
set Zi[vr]=false
set dj[vr]=0
set ej[vr]=.0
return vr
endfunction
function yt takes nothing returns integer
local integer vr=Ds()
local integer Zr
if(vr==0)then
return 0
endif
set mq[vr]=64
set Zr=vr
return vr
endfunction
function zt takes nothing returns integer
local integer vr=Ds()
local integer Zr
if(vr==0)then
return 0
endif
set mq[vr]=65
set Zr=vr
return vr
endfunction
function At takes nothing returns integer
local integer vr=Ds()
local integer Zr
if(vr==0)then
return 0
endif
set mq[vr]=66
set Zr=vr
set qj[vr]=false
set rj[vr]=.0
set sj[vr]=0
set tj[vr]=0
set uj[vr]=0
return vr
endfunction
function at takes nothing returns integer
local integer vr=Ds()
local integer Zr
if(vr==0)then
return 0
endif
set mq[vr]=67
set Zr=vr
return vr
endfunction
function Bt takes nothing returns integer
local integer vr=Ds()
local integer Zr
if(vr==0)then
return 0
endif
set mq[vr]=68
set Zr=vr
return vr
endfunction
function bt takes nothing returns integer
local integer vr=Ds()
local integer Zr
if(vr==0)then
return 0
endif
set mq[vr]=69
set Zr=vr
return vr
endfunction
function Ct takes nothing returns integer
local integer vr=Zs()
local integer Zr
if(vr==0)then
return 0
endif
set jq[vr]=71
set Zr=vr
return vr
endfunction
function ct takes nothing returns integer
local integer vr=Zs()
local integer Zr
if(vr==0)then
return 0
endif
set jq[vr]=72
set Zr=vr
return vr
endfunction
function Dt takes nothing returns integer
local integer vr=Zs()
local integer Zr
if(vr==0)then
return 0
endif
set jq[vr]=73
set Zr=vr
return vr
endfunction
function Et takes nothing returns integer
local integer vr=Zs()
local integer Zr
if(vr==0)then
return 0
endif
set jq[vr]=74
set Zr=vr
return vr
endfunction
function Ft takes nothing returns integer
local integer vr=Zs()
local integer Zr
if(vr==0)then
return 0
endif
set jq[vr]=75
set Zr=vr
return vr
endfunction
function Gt takes nothing returns integer
local integer vr=Zs()
local integer Zr
if(vr==0)then
return 0
endif
set jq[vr]=76
set Zr=vr
return vr
endfunction
function Ht takes nothing returns integer
local integer vr=Zs()
local integer Zr
if(vr==0)then
return 0
endif
set jq[vr]=77
set Zr=vr
return vr
endfunction
function It takes nothing returns integer
local integer vr=Ws()
local integer Zr
if(vr==0)then
return 0
endif
set hq[vr]=78
set Zr=vr
return vr
endfunction
function lt takes nothing returns integer
local integer vr=Ws()
local integer Zr
if(vr==0)then
return 0
endif
set hq[vr]=79
set Zr=vr
return vr
endfunction
function Jt takes nothing returns integer
local integer vr=Ws()
local integer Zr
if(vr==0)then
return 0
endif
set hq[vr]=80
set Zr=vr
return vr
endfunction
function Kt takes nothing returns integer
local integer vr=Ws()
local integer Zr
if(vr==0)then
return 0
endif
set hq[vr]=81
set Zr=vr
return vr
endfunction
function Lt takes nothing returns integer
local integer vr=Ws()
local integer Zr
if(vr==0)then
return 0
endif
set hq[vr]=82
set Zr=vr
return vr
endfunction
function Mt takes nothing returns integer
local integer vr=Ws()
local integer Zr
if(vr==0)then
return 0
endif
set hq[vr]=83
set Zr=vr
return vr
endfunction
function Nt takes integer m,integer T returns integer
local integer vr=Er(m,T)
local integer Zr
if(vr==0)then
return 0
endif
set qq[vr]=86
set Zr=vr
set Rk[vr]=10.
set Sk[vr]=.0
set Tk[vr]=1.
set Uk[vr]=2.
return vr
endfunction
function Ot takes integer m,integer T returns integer
local integer vr=Er(m,T)
local integer Zr
if(vr==0)then
return 0
endif
set qq[vr]=87
set Zr=vr
set Zk[vr]=3.
return vr
endfunction
function Pt takes integer m,integer T returns integer
local integer vr=Er(m,T)
local integer Zr
if(vr==0)then
return 0
endif
set qq[vr]=88
set Zr=vr
set em[vr]=0
return vr
endfunction
function Qt takes integer m,integer T returns integer
local integer vr=Er(m,T)
local integer Zr
if(vr==0)then
return 0
endif
set qq[vr]=89
set Zr=vr
set hm[vr]=null
set im[vr]=false
set jm[vr]=.0
return vr
endfunction
function Rt takes integer m,integer T returns integer
local integer vr=Er(m,T)
local integer Zr
if(vr==0)then
return 0
endif
set qq[vr]=90
set Zr=vr
set om[vr]=1
return vr
endfunction
function St takes integer m,integer T returns integer
local integer vr=Er(m,T)
local integer Zr
if(vr==0)then
return 0
endif
set qq[vr]=91
set Zr=vr
set xm[vr]=2
set ym[vr]=.04
set Am[vr]=25
set am[vr]=.1
set cm[vr]=5
return vr
endfunction
function Tt takes integer m,integer T returns integer
local integer vr=Er(m,T)
local integer Zr
if(vr==0)then
return 0
endif
set qq[vr]=92
set Zr=vr
set Mm[vr]=.0
set Nm[vr]=.0
return vr
endfunction
function Ut takes integer m,integer T returns integer
local integer vr=Er(m,T)
local integer Zr
if(vr==0)then
return 0
endif
set qq[vr]=93
set Zr=vr
set Sm[vr]=.0
return vr
endfunction
function Vt takes integer m,integer T returns integer
local integer vr=Er(m,T)
local integer Zr
if(vr==0)then
return 0
endif
set qq[vr]=94
set Zr=vr
set dn[vr]=0
set en[vr]=.1
set fn[vr]=Ym
set gn[vr]=false
set kn[vr]=0
return vr
endfunction
function Wt takes nothing returns integer
local integer vr=ps()
local integer Zr
if(vr==0)then
return 0
endif
set oq[vr]=96
set Zr=vr
return vr
endfunction
function Xt takes nothing returns integer
local integer vr=ps()
local integer Zr
if(vr==0)then
return 0
endif
set oq[vr]=97
set Zr=vr
return vr
endfunction
function Yt takes nothing returns integer
local integer vr=ps()
local integer Zr
if(vr==0)then
return 0
endif
set oq[vr]=98
set Zr=vr
return vr
endfunction
function Zt takes nothing returns integer
local integer vr=ps()
local integer Zr
if(vr==0)then
return 0
endif
set oq[vr]=99
set Zr=vr
return vr
endfunction
function du takes nothing returns integer
local integer vr=ps()
local integer Zr
if(vr==0)then
return 0
endif
set oq[vr]=100
set Zr=vr
return vr
endfunction
function eu takes nothing returns integer
local integer vr=ps()
local integer Zr
if(vr==0)then
return 0
endif
set oq[vr]=101
set Zr=vr
return vr
endfunction
function fu takes nothing returns integer
local integer vr=ps()
local integer Zr
if(vr==0)then
return 0
endif
set oq[vr]=102
set Zr=vr
return vr
endfunction
function gu takes nothing returns integer
local integer vr=ps()
local integer Zr
if(vr==0)then
return 0
endif
set oq[vr]=103
set Zr=vr
return vr
endfunction
function hu takes nothing returns integer
local integer vr=ps()
local integer Zr
if(vr==0)then
return 0
endif
set oq[vr]=104
set Zr=vr
return vr
endfunction
function iu takes integer i,integer a1,integer a2,integer a3 returns nothing
set Jq=a1
set Kq=a2
set Lq=a3
call TriggerExecute(Fq[i])
endfunction
function GetRandomFactor takes nothing returns integer
return j
endfunction
function ResetRandomFactor takes nothing returns nothing
set j=e
endfunction
function ju takes string s returns nothing
endfunction
function ku takes integer z1,integer z2 returns integer
if z1<z2 then
return z1
endif
return z2
endfunction
function nu takes integer a returns boolean
return a/ 2*2!=a
endfunction
function ou takes integer a,integer n returns boolean
return a/ n*n==a
endfunction
function pu takes integer a,integer n returns boolean
return a/ n*n!=a
endfunction
function ru takes real r returns integer
local integer i=R2I(r)
if r-i==1. then
return i+1
endif
return i
endfunction
function su takes real r returns integer
local integer i=R2I(r)
if r==i then
return i
endif
return i+1
endfunction
function tu takes boolean b returns integer
if b then
return 1
endif
return 0
endfunction
function uu takes nothing returns nothing
call PauseUnit(GetFilterUnit(),bj_pauseAllUnitsFlag)
endfunction
function vu takes string s,unit u,player p,real wu,real xu,real yu,real zu returns texttag
set bj_lastCreatedTextTag=CreateTextTag()
call SetTextTagText(bj_lastCreatedTextTag,s,wu*D)
call SetTextTagPosUnit(bj_lastCreatedTextTag,u,.0)
call SetTextTagVelocity(bj_lastCreatedTextTag,.0,xu*E)
call SetTextTagVisibility(bj_lastCreatedTextTag,F==p)
call SetTextTagPermanent(bj_lastCreatedTextTag,false)
call SetTextTagLifespan(bj_lastCreatedTextTag,yu)
call SetTextTagFadepoint(bj_lastCreatedTextTag,zu)
return bj_lastCreatedTextTag
endfunction
function Au takes string au,real wu,real x,real y,integer Bu returns image
set bj_lastCreatedImage=CreateImage(au,wu,wu,.0,x,y,.0,wu/ 2.,wu/ 2.,.0,Bu)
call SetImageRenderAlways(bj_lastCreatedImage,true)
return bj_lastCreatedImage
endfunction
function bu takes player p,string au,real wu,real x,real y,integer Bu returns image
set bj_lastCreatedImage=CreateImage(au,wu,wu,.0,x,y,.0,wu/ 2.,wu/ 2.,.0,Bu)
call SetImageRenderAlways(bj_lastCreatedImage,F==p)
return bj_lastCreatedImage
endfunction
function Cu takes rect r returns real
return GetRandomReal(GetRectMinX(r),GetRectMaxX(r))
endfunction
function cu takes rect r returns real
return GetRandomReal(GetRectMinY(r),GetRectMaxY(r))
endfunction
function Du takes player p,string s,widget w,string Eu returns effect
if F!=p then
set s=""
endif
return AddSpecialEffectTarget(s,w,Eu)
endfunction
function EndThread takes nothing returns nothing
call I2R(1/ 0)
endfunction
function Fu takes nothing returns nothing
set F=GetLocalPlayer()
endfunction
function GetCustomTimer takes nothing returns integer
return LoadInteger(r7,W,GetHandleId(GetExpiredTimer()))
endfunction
function Gu takes timer t returns nothing
call PauseTimer(t)
call RemoveSavedInteger(r7,W,GetHandleId(t))
call DestroyTimer(t)
endfunction
function IsFilterUnitTower takes nothing returns boolean
return(IsUnitType(GetFilterUnit(),f4))!=null
endfunction
function Hu takes nothing returns boolean
return(IsUnitType(GetFilterUnit(),h4))!=null
endfunction
function Iu takes real lu,real x,real y,integer Bu,integer Ju,integer Ku,integer Lu,integer Mu returns image
call Au("Textures\\RangeIndicator.blp",lu+lu,x,y,Bu)
call SetImageColor(bj_lastCreatedImage,Ju,Ku,Lu,Mu)
return bj_lastCreatedImage
endfunction
function Nu takes player p,real lu,real x,real y,integer Bu,integer Ju,integer Ku,integer Lu,integer Mu returns image
call bu(p,"Textures\\RangeIndicator.blp",lu+lu,x,y,Bu)
call SetImageColor(bj_lastCreatedImage,Ju,Ku,Lu,Mu)
return bj_lastCreatedImage
endfunction
function Ou takes integer p returns nothing
set Y7=p
call TriggerExecute(Z7)
endfunction
function Pu takes integer Qu returns nothing
set X=false
set x7=Qu+1
call TriggerExecute(W8)
endfunction
function Ru takes string Su,string Tu returns nothing
if X then
call PreloadGenClear()
call Preload("\" )        "+(Su)+".                                //")
call PreloadGenEnd("WispTD\\Errors\\"+Tu+".txt")
call Pu(7)
call DisplayTimedTextToPlayer(F,.0,.0,((7.)*1.),("An error occurred. Game will end in "+od[Z9]+"7|r seconds."))
endif
call I2R(1/ 0)
endfunction
function Uu takes nothing returns nothing
set k4=Filter(function Hu)
endfunction
function Vu takes integer Wu returns integer
set Y8=Y8+1
set X8[(Y8)]=Wu
return Y8
endfunction
function Xu takes integer Wu returns nothing
set d9=d9+1
set Z8[(d9)]=Wu
set Tf[Wu]=d9
endfunction
function Yu takes integer Wu returns nothing
local integer i=Tf[Wu]
if d9>i then
set Z8[(i)]=Z8[(d9)]
set Tf[Z8[(i)]]=i
endif
set d9=d9-1
endfunction
function Zu takes integer Wu returns nothing
local integer i=Uf[Wu]
if f9>i then
set e9[(i)]=e9[(f9)]
set Uf[e9[(i)]]=i
endif
set f9=f9-1
endfunction
function dv takes nothing returns nothing
set f9=-1
loop
set f9=f9+1
set e9[(f9)]=(Z8[(f9)])
set Uf[e9[(f9)]]=f9
exitwhen f9==d9
endloop
endfunction
function ev takes nothing returns integer
local integer p=e9[((GetRandomInt((0)*j,((f9)+1)*j-1)/ j))]
call Zu(p)
return p
endfunction
function fv takes integer vr,integer Wu returns nothing
set m9[vr]=m9[vr]+1
set j9[k9[vr]+m9[vr]]=Wu
set tk[Wu]=m9[vr]
endfunction
function gv takes integer vr,integer Wu returns nothing
local integer i=tk[Wu]
if m9[vr]>i then
set j9[k9[vr]+i]=j9[k9[vr]+m9[vr]]
set tk[j9[k9[vr]+i]]=i
endif
set m9[vr]=m9[vr]-1
endfunction
function hv takes integer vr,integer Wu returns nothing
set s9[vr]=s9[vr]+1
if s9[vr]==105 then
call Ru("Error: not enough cells in ForceArray "+I2S(vr)+". Current amount of cells is "+I2S(105),"ForceArrayError")
endif
set q9[r9[vr]+s9[vr]]=Wu
set Fh[Wu]=s9[vr]
endfunction
function iv takes integer vr,integer Wu returns nothing
local integer i=Fh[Wu]
if s9[vr]>i then
set q9[r9[vr]+i]=q9[r9[vr]+s9[vr]]
set Fh[q9[r9[vr]+i]]=i
endif
set s9[vr]=s9[vr]-1
endfunction
function jv takes integer vr,integer Wu returns nothing
set y9[vr]=y9[vr]+1
if y9[vr]==105 then
call Ru("Error: not enough cells in SpawnArray "+I2S(vr)+". Current amount of cells is "+I2S(105),"SpawnArrayError")
endif
set w9[x9[vr]+y9[vr]]=Wu
set Fh[Wu]=y9[vr]
endfunction
function kv takes integer vr,integer Wu returns nothing
local integer i=Fh[Wu]
if y9[vr]>i then
set w9[x9[vr]+i]=w9[x9[vr]+y9[vr]]
set Fh[w9[x9[vr]+i]]=i
endif
set y9[vr]=y9[vr]-1
endfunction
function mv takes integer vr,integer Wu returns nothing
set C9[vr]=C9[vr]+1
if C9[vr]==105 then
call Ru("Error: not enough cells in MinionArray "+I2S(vr)+". Current amount of cells is "+I2S(105),"MinionArrayError")
endif
set B9[b9[vr]+C9[vr]]=Wu
set Fh[Wu]=C9[vr]
endfunction
function nv takes integer vr,integer Wu returns nothing
local integer i=Fh[Wu]
if C9[vr]>i then
set B9[b9[vr]+i]=B9[b9[vr]+C9[vr]]
set Fh[B9[b9[vr]+i]]=i
endif
set C9[vr]=C9[vr]-1
endfunction
function ov takes integer vr,integer Wu returns nothing
set H9[vr]=H9[vr]+1
if H9[vr]==105 then
call Ru("Error: not enough cells in DeadArray "+I2S(vr)+". Current amount of cells is "+I2S(105),"DeadArrayError")
endif
set F9[G9[vr]+H9[vr]]=Wu
set Fh[Wu]=H9[vr]
endfunction
function pv takes integer vr,integer Wu returns nothing
local integer i=Fh[Wu]
if H9[vr]>i then
set F9[G9[vr]+i]=F9[G9[vr]+H9[vr]]
set Fh[F9[G9[vr]+i]]=i
endif
set H9[vr]=H9[vr]-1
endfunction
function qv takes integer vr,integer Wu returns nothing
set M9[vr]=M9[vr]+1
if M9[vr]==105 then
call Ru("Error: not enough cells in SentMinionArray "+I2S(vr)+". Current amount of cells is "+I2S(105),"SentMinionArrayError")
endif
set K9[L9[vr]+M9[vr]]=Wu
set Gh[Wu]=M9[vr]
endfunction
function rv takes integer vr,integer Wu returns nothing
local integer i=Gh[Wu]
if M9[vr]>i then
set K9[L9[vr]+i]=K9[L9[vr]+M9[vr]]
set Gh[K9[L9[vr]+i]]=i
endif
set M9[vr]=M9[vr]-1
endfunction
function sv takes playercolor pc returns integer
local integer i=0
loop
exitwhen ConvertPlayerColor(i)==pc
set i=i+1
endloop
return i
endfunction
function tv takes nothing returns integer
set S9=S9+1
return S9
endfunction
function uv takes nothing returns nothing
local integer i=tv()
set kd[i]=$FF
set md[i]=3
set nd[i]=3
set od[i]="|cffff0303"
set pd[i]="Icons\\Player colors\\00. Red.blp"
set i=tv()
set kd[i]=0
set md[i]=66
set nd[i]=$FF
set od[i]="|cff0042ff"
set pd[i]="Icons\\Player colors\\01. Blue.blp"
set i=tv()
set kd[i]=28
set md[i]=$E6
set nd[i]=$B9
set od[i]="|cff1ce6b9"
set pd[i]="Icons\\Player colors\\02. Teal.blp"
set i=tv()
set kd[i]=84
set md[i]=0
set nd[i]=$81
set od[i]="|cff540081"
set pd[i]="Icons\\Player colors\\03. Purple.blp"
set i=tv()
set kd[i]=$FF
set md[i]=$FC
set nd[i]=0
set od[i]="|cfffffc00"
set pd[i]="Icons\\Player colors\\04. Yellow.blp"
set i=tv()
set kd[i]=$FE
set md[i]=$8A
set nd[i]=$E
set od[i]="|cfffe8a0e"
set pd[i]="Icons\\Player colors\\05. Orange.blp"
set i=tv()
set kd[i]=32
set md[i]=$C0
set nd[i]=0
set od[i]="|cff20c000"
set pd[i]="Icons\\Player colors\\06. Green.blp"
set i=tv()
set kd[i]=$E5
set md[i]=91
set nd[i]=$B0
set od[i]="|cffe55bb0"
set pd[i]="Icons\\Player colors\\07. Pink.blp"
set i=tv()
set kd[i]=$95
set md[i]=$96
set nd[i]=$97
set od[i]="|cff959697"
set pd[i]="Icons\\Player colors\\08. Gray.blp"
set i=tv()
set kd[i]=$7E
set md[i]=$BF
set nd[i]=$F1
set od[i]="|cff7ebff1"
set pd[i]="Icons\\Player colors\\09. Light Blue.blp"
set i=tv()
set kd[i]=16
set md[i]=98
set nd[i]=70
set od[i]="|cff106246"
set pd[i]="Icons\\Player colors\\10. Dark Green.blp"
set i=tv()
set kd[i]=78
set md[i]=42
set nd[i]=4
set od[i]="|cff4e2a04"
set pd[i]="Icons\\Player colors\\11. Brown.blp"
set i=tv()
set kd[i]=$9B
set md[i]=0
set nd[i]=0
set od[i]="|cff9b0000"
set pd[i]="Icons\\Player colors\\12. Maroon.blp"
set i=tv()
set kd[i]=0
set md[i]=0
set nd[i]=$C3
set od[i]="|cff0000c3"
set pd[i]="Icons\\Player colors\\13. Navy.blp"
set i=tv()
set kd[i]=0
set md[i]=$EA
set nd[i]=$FF
set od[i]="|cff00eaff"
set pd[i]="Icons\\Player colors\\14. Turquoise.blp"
set i=tv()
set kd[i]=$BE
set md[i]=0
set nd[i]=$FE
set od[i]="|cffbe00fe"
set pd[i]="Icons\\Player colors\\15. Violet.blp"
set i=tv()
set kd[i]=$EB
set md[i]=$CD
set nd[i]=$87
set od[i]="|cffebcd87"
set pd[i]="Icons\\Player colors\\16. Wheat.blp"
set i=tv()
set kd[i]=$F8
set md[i]=$A4
set nd[i]=$8B
set od[i]="|cfff8a48b"
set pd[i]="Icons\\Player colors\\17. Peach.blp"
set i=tv()
set kd[i]=$BF
set md[i]=$FF
set nd[i]=$80
set od[i]="|cffbfff80"
set pd[i]="Icons\\Player colors\\18. Mint.blp"
set i=tv()
set kd[i]=$DC
set md[i]=$B9
set nd[i]=$EB
set od[i]="|cffdcb9eb"
set pd[i]="Icons\\Player colors\\19. Lavender.blp"
set i=tv()
set kd[i]=40
set md[i]=40
set nd[i]=40
set od[i]="|cff282828"
set pd[i]="Icons\\Player colors\\20. Coal.blp"
set i=tv()
set kd[i]=$EB
set md[i]=$F0
set nd[i]=$FF
set od[i]="|cffebf0ff"
set pd[i]="Icons\\Player colors\\21. Snow.blp"
set i=tv()
set kd[i]=0
set md[i]=120
set nd[i]=30
set od[i]="|cff00781e"
set pd[i]="Icons\\Player colors\\22. Emerald.blp"
set i=tv()
set kd[i]=$A4
set md[i]=111
set nd[i]=51
set od[i]="|cffa46f33"
set pd[i]="Icons\\Player colors\\23. Peanut.blp"
endfunction
function vv takes nothing returns nothing
call uv()
set T9=tv()
set kd[T9]=$E2
set md[T9]=$B0
set nd[T9]=7
set od[T9]="|cffe2b007"
set U9=tv()
set kd[U9]=$80
set md[U9]=$CC
set nd[U9]=$FF
set od[U9]="|cff80ccff"
set V9=tv()
set kd[V9]=$D1
set md[V9]=91
set nd[V9]=$8F
set od[V9]="|cffd15b8f"
set W9=tv()
set kd[W9]=118
set md[W9]=$A5
set nd[W9]=$AF
set od[W9]="|cff76a5af"
set X9=tv()
set kd[X9]=0
set md[X9]=$BB
set nd[X9]=46
set od[X9]="|cff00bb2e"
set Y9=tv()
set kd[Y9]=$CC
set md[Y9]=33
set nd[Y9]=33
set od[Y9]="|cffcc3333"
set Z9=tv()
set kd[Z9]=0
set md[Z9]=$CC
set nd[Z9]=$FF
set od[Z9]="|cff00ccff"
set ed=tv()
set kd[ed]=$FF
set md[ed]=$CC
set nd[ed]=$80
set od[ed]="|cffffcc80"
set fd=tv()
set kd[fd]=51
set md[fd]=$CC
set nd[fd]=51
set od[fd]="|cff33cc33"
set jd=tv()
set kd[jd]=$F
set md[jd]=$F
set nd[jd]=$F
set od[jd]="|cff0f0f0f"
set gd=Y9
set hd=Y9
endfunction
function xv takes unit u,integer id returns item
call IncUnitAbilityLevel(u,1093677104)
set bj_lastCreatedItem=UnitAddItemById(u,id)
call SetItemUserData(bj_lastCreatedItem,(GetUnitAbilityLevel((u),1093677104)-2))
return bj_lastCreatedItem
endfunction
function Av takes unit u,item i returns nothing
local integer zv=GetItemUserData(i)
local integer av=(GetUnitAbilityLevel((u),1093677104)-2)
local integer Bv
call RemoveItem(i)
loop
exitwhen zv==av
set Bv=zv
set zv=zv+1
set i=UnitItemInSlot(u,zv)
call SetItemDroppable(i,true)
call UnitDropItemSlot(u,i,Bv)
call SetItemUserData(i,Bv)
call SetItemDroppable(i,false)
endloop
call DecUnitAbilityLevel(u,1093677104)
endfunction
function bv takes integer T,integer Cv,integer cv,integer Dv,integer Ev,integer Fv,integer Gv,integer Hv,integer Iv,string lv returns nothing
set n4[T]=lr()
set yd[zd[n4[T]]]=Cv
set yd[zd[n4[T]]+1]=cv
set yd[zd[n4[T]]+2]=Dv
set yd[zd[n4[T]]+3]=Ev
set Ad[n4[T]]=p7[Fv]
set ad[n4[T]]=Gv
set Bd[n4[T]]=Hv
set bd[n4[T]]=Iv
set Cd[n4[T]]=lv
endfunction
function Kv takes integer p returns nothing
local integer a=0
local integer b
local integer Lv
local integer Mv
local integer Nv=Sg[p]
local integer Ov=Tg[p]
loop
set Lv=yd[zd[Nv]+a]
set Mv=yd[zd[Ov]+a]
set b=0
loop
call UnitRemoveAbility(vg[p],co[Do[n7[Lv]]+b])
call UnitRemoveAbility(vg[p],Eo[Fo[n7[Lv]]+b])
call UnitAddAbility(vg[p],co[Do[n7[Mv]]+b])
call UnitAddAbility(vg[p],Eo[Fo[n7[Mv]]+b])
set b=b+1
exitwhen b==3
endloop
set a=a+1
exitwhen a==4
endloop
set Sg[p]=Ov
set Tg[p]=Nv
endfunction
function Pv takes integer p returns nothing
local integer a=0
local integer Nv=Qg[p]
local integer Ov=Rg[p]
loop
call UnitRemoveAbility(ug[p],Yn[k7[yd[zd[Nv]+a]]])
call UnitAddAbility(ug[p],Yn[k7[yd[zd[Ov]+a]]])
set a=a+1
exitwhen a==4
endloop
call UnitRemoveAbility(ug[p],ad[Nv])
call UnitAddAbility(ug[p],ad[Ov])
set Qg[p]=Ov
set Rg[p]=Nv
endfunction
function Qv takes integer f,integer i returns integer
local integer vr=Jr()
set ld[vr]=f
set Jd[vr]=i
set Id[vr]=v4-i/ u4
set Gd[vr]=Id[vr]
if i<w4 then
set Ld[vr]=true
else
set Ld[vr]=false
set Hd[vr]=Id[vr]
endif
return vr
endfunction
function Rv takes integer vr returns nothing
if Jd[vr]<u4 then
set Md[vr]=0
else
set Md[vr]=ee[fe[ld[vr]]+Jd[vr]-u4]
endif
if Jd[vr]+u4>608 then
set Pd[vr]=0
else
set Pd[vr]=ee[fe[ld[vr]]+Jd[vr]+u4]
endif
if ou(Jd[vr]+1,u4)then
set Od[vr]=0
else
set Od[vr]=ee[fe[ld[vr]]+Jd[vr]+1]
endif
if ou(Jd[vr],u4)then
set Nd[vr]=0
else
set Nd[vr]=ee[fe[ld[vr]]+Jd[vr]-1]
endif
endfunction
function Sv takes integer vr returns boolean
return vr!=0 and Ld[vr]and Kd[vr]
endfunction
function Tv takes integer vr returns integer
local integer Uv=Vd[vr]
set Vd[vr]=Qd[Uv]
return Uv
endfunction
function Vv takes integer vr,integer c returns nothing
if(Vd[(vr)]==0)then
set Vd[vr]=c
set Wd[vr]=c
set Qd[c]=0
else
set Qd[Wd[vr]]=c
set Rd[c]=Wd[vr]
set Qd[c]=0
set Wd[vr]=c
endif
endfunction
function Wv takes integer vr,integer c,integer Xv returns nothing
if Sv(c)then
call Vv(vr,c)
set Ld[(c)]=false
set Hd[(c)]=(Xv)
endif
endfunction
function Yv takes integer f returns integer
local integer vr=Lr()
local integer a=x4
loop
set a=a-1
call Vv(vr,ee[fe[f]+a])
exitwhen a<w4
endloop
return vr
endfunction
function Zv takes integer p returns integer
local integer vr=Tr()
local real dw=r4+Kf[p]*(1024.+512.)
local real ew=dw+1024.
set de[vr]=p
set ge[vr]=ew
set he[vr]=dw
set ie[vr]=ew-64.
set je[vr]=dw+64.
set ke[vr]=Rect(dw,1408.,ew,1920.)
set me[vr]=Rect(dw,640.,ew,1152.)
set oe[vr]=Rect(dw,q4,ew,p4)
set ne[vr]=Rect(je[vr],o4,ie[vr],320.)
set pe[vr]=Rect(dw,q4,ew,1152.)
call RegionAddRect(t7,me[vr])
call RegionAddRect(s7,oe[vr])
call Pr(vr)
return vr
endfunction
function fw takes integer vr returns nothing
local integer a=-1
local integer p
loop
set a=a+1
set p=(X8[(a)])
if Of[p]then
set qe[re[vr]+a]=CreateFogModifierRect(Jf[de[vr]],FOG_OF_WAR_VISIBLE,pe[Sf[p]],true,true)
set se[te[vr]+a]=CreateFogModifierRect(Jf[de[vr]],FOG_OF_WAR_VISIBLE,ke[Sf[p]],true,true)
call FogModifierStart(qe[re[vr]+a])
call FogModifierStart(se[te[vr]+a])
endif
exitwhen a==Y8
endloop
endfunction
function gw takes integer vr returns nothing
local integer a=-1
local integer sr=Kf[de[vr]]
local integer f
local integer p
loop
set a=a+1
set p=(X8[(a)])
if Rf[p]then
set f=Sf[p]
call DestroyFogModifier(qe[re[f]+sr])
set qe[re[f]+sr]=null
call DestroyFogModifier(se[te[f]+sr])
set se[te[f]+sr]=null
call SetFogStateRect(Jf[p],FOG_OF_WAR_MASKED,pe[vr],true)
call SetFogStateRect(Jf[p],FOG_OF_WAR_MASKED,ke[vr],true)
endif
exitwhen a==Y8
endloop
call RegionClearRect(t7,me[vr])
call RegionClearRect(s7,oe[vr])
call RemoveRect(pe[vr])
set pe[vr]=null
call RemoveRect(ke[vr])
set ke[vr]=null
call RemoveRect(me[vr])
set me[vr]=null
call RemoveRect(oe[vr])
set oe[vr]=null
call RemoveRect(ne[vr])
set ne[vr]=null
call Qr(vr)
endfunction
function hw takes integer vr,real x,real y returns integer
local integer ix
local integer iy
set ix=R2I(x-he[vr])/ 64
if ix==u4 then
set ix=u4-1
endif
set iy=R2I(640.-y)/ 64
if iy==v4 then
set iy=v4-1
endif
return ee[fe[vr]+iy*u4+ix]
endfunction
function iw takes integer vr returns nothing
local integer q=Yv(vr)
call Sr(vr,q)
if ve[vr]then
call Ru("Error: updating ground distaces in field "+I2S(vr)+" was not finished","BFSError")
endif
call Mr(q)
call Rr(vr)
endfunction
function jw takes integer vr returns nothing
set ae[vr]=CreateTimer()
call SaveInteger(r7,W,GetHandleId((ae[vr])),(vr))
endfunction
function kw takes integer vr returns nothing
call Gu(ae[vr])
set ae[vr]=null
endfunction
function mw takes nothing returns nothing
local integer vr=(LoadInteger(r7,W,GetHandleId(GetExpiredTimer())))
set Ae[vr]=Ae[vr]-8
if Ae[vr]>0 then
call SetUnitVertexColor(ze[vr],$FF,$FF,$FF,Ae[vr])
else
call RemoveUnit(ze[vr])
set ze[vr]=null
call kw(vr)
call Vr(vr)
call ju("MinionDeleter "+I2S(vr)+" is ended.")
endif
endfunction
function nw takes unit m returns nothing
local integer vr=Ur()
set ze[vr]=m
call jw(vr)
call SetUnitInvulnerable(ze[vr],true)
call PauseUnit(ze[vr],true)
call SetUnitVertexColor(ze[vr],$FF,$FF,$FF,Ae[vr])
call TimerStart(ae[vr],.05,true,function mw)
call ju("MinionDeleter "+I2S(vr)+" is started.")
endfunction
function ow takes integer vr,integer f,integer pw returns nothing
set Be[vr]=f
set be[vr]=pw
endfunction
function qw takes integer vr,integer rw,integer sw returns nothing
if vr==0 then
call Ru("Error: MBSNode struct has run out of indexes. Current amount of indexes is 8190. Error detected on field of player "+I2S(Oh[rw]),"MBSNodeError")
endif
set Ee[vr]=rw
set Fe[vr]=sw
endfunction
function tw takes integer vr returns nothing
if Ge[vr]!=0 then
set He[Ge[vr]]=He[vr]
endif
if He[vr]!=0 then
set Ge[He[vr]]=Ge[vr]
endif
endfunction
function uw takes integer vr,integer Wu returns integer
local integer vw=Wr()
if Qe[vr]==0 then
set Pe[vr]=vw
else
set He[vw]=Pe[vr]
set Ge[Pe[vr]]=vw
set Pe[vr]=vw
endif
set Oe[vr]=Oe[vr]+1
set Qe[vr]=Qe[vr]+1
call qw(vw,Wu,Oe[vr])
return vw
endfunction
function ww takes integer vr,integer vw returns nothing
set Qe[vr]=Qe[vr]-1
if Pe[vr]==vw then
set Pe[vr]=He[vw]
endif
call tw(vw)
call Xr(vw)
endfunction
function xw takes integer vr,integer Wu returns integer
local integer vw=Yr()
if Se[vr]==0 then
set Re[vr]=vw
else
set He[vw]=Re[vr]
set Ge[Re[vr]]=vw
set Re[vr]=vw
endif
set Oe[vr]=Oe[vr]+1
set Se[vr]=Se[vr]+1
call qw(vw,Wu,Oe[vr])
return vw
endfunction
function yw takes integer vr,integer vw returns nothing
set Se[vr]=Se[vr]-1
if Re[vr]==vw then
set Re[vr]=He[vw]
endif
set Te[vr]=Te[vr]-Ie[vw]
call tw(vw)
call Xr(vw)
endfunction
function zw takes integer vr,integer vw,real Aw returns nothing
set Te[vr]=Te[vr]-Ie[vw]+Aw
set Ie[vw]=Aw
endfunction
function aw takes integer vr,real Rs returns real
local integer vw=Re[vr]
local integer Bw
loop
exitwhen vw==0
set Bw=He[vw]
set Rs=Rs-Ie[vw]
if Rs>.0 then
call dt(Ee[vw])
else
call zw(vr,vw,-Rs)
return .0
endif
set vw=Bw
endloop
return Rs
endfunction
function bw takes integer vr,integer Wu returns integer
local integer vw=Yr()
if We[vr]==0 then
set Ue[vr]=vw
set Ve[vr]=vw
else
set He[vw]=Ue[vr]
set Ge[Ue[vr]]=vw
set Ue[vr]=vw
endif
set Oe[vr]=Oe[vr]+1
set We[vr]=We[vr]+1
call qw(vw,Wu,Oe[vr])
return vw
endfunction
function Cw takes integer vr returns nothing
if We[vr]==0 then
set Xe[vr]=.0
elseif Ie[Ve[vr]]<=.0 then
set Xe[vr]=Ie[Ue[vr]]
elseif Ie[Ue[vr]]>=.0 then
set Xe[vr]=Ie[Ve[vr]]
else
set Xe[vr]=Ie[Ue[vr]]+Ie[Ve[vr]]
endif
endfunction
function cw takes integer vr,integer vw returns nothing
set We[vr]=We[vr]-1
if Ue[vr]==vw then
set Ue[vr]=He[vw]
endif
if Ve[vr]==vw then
set Ve[vr]=Ge[vw]
endif
call Cw(vr)
call tw(vw)
call Xr(vw)
endfunction
function Dw takes integer vr,integer vw returns nothing
local integer n=He[vw]
local integer p=Ge[vw]
if n!=0 and Ie[vw]>Ie[n]then
if p!=0 then
set He[p]=n
endif
set Ge[n]=p
set p=n
set n=He[n]
set He[p]=vw
set He[vw]=n
set Ge[vw]=p
if p==Ve[vr]then
set Ve[vr]=vw
endif
if vw==Ue[vr]then
set Ue[vr]=p
endif
loop
exitwhen n==0
set Ge[n]=vw
exitwhen Ie[vw]<=Ie[n]
set He[p]=n
set Ge[n]=p
set p=n
set n=He[n]
set He[p]=vw
set He[vw]=n
set Ge[vw]=p
if p==Ve[vr]then
set Ve[vr]=vw
endif
endloop
endif
if p!=0 and Ie[vw]<Ie[p]then
if n!=0 then
set Ge[n]=p
endif
set He[p]=n
set n=p
set p=Ge[p]
set Ge[n]=vw
set Ge[vw]=p
set He[vw]=n
if n==Ue[vr]then
set Ue[vr]=vw
endif
if vw==Ve[vr]then
set Ve[vr]=n
endif
loop
exitwhen p==0
set He[p]=vw
exitwhen Ie[vw]>=Ie[p]
set Ge[n]=p
set He[p]=n
set n=p
set p=Ge[p]
set Ge[n]=vw
set Ge[vw]=p
set He[vw]=n
if n==Ue[vr]then
set Ue[vr]=vw
endif
endloop
endif
endfunction
function Ew takes integer vr,integer vw,real Aw returns nothing
set Ie[vw]=Aw
call Dw(vr,vw)
call Cw(vr)
endfunction
function Gw takes integer vr,integer vw returns nothing
set Ze[vr]=Ze[vr]-1
if Ye[vr]==vw then
set Ye[vr]=He[vw]
endif
set df[vr]=df[vr]-le[vw]
set ef[vr]=df[vr]>0
call tw(vw)
call Xr(vw)
endfunction
function Iw takes integer vr,integer Wu returns integer
local integer vw=ds()
if gf[vr]==0 then
set ff[vr]=vw
else
set He[vw]=ff[vr]
set Ge[ff[vr]]=vw
set ff[vr]=vw
endif
set Oe[vr]=Oe[vr]+1
set gf[vr]=gf[vr]+1
call qw(vw,Wu,Oe[vr])
return vw
endfunction
function lw takes integer vr,integer vw returns nothing
set gf[vr]=gf[vr]-1
if ff[vr]==vw then
set ff[vr]=He[vw]
endif
set hf[vr]=hf[vr]-le[vw]
set jf[vr]=hf[vr]>0
call tw(vw)
call Xr(vw)
endfunction
function Jw takes integer vr,integer vw,boolean Aw returns nothing
local integer v=tu(Aw)
set hf[vr]=hf[vr]-le[vw]+v
set jf[vr]=hf[vr]>0
set le[vw]=v
endfunction
function Nw takes integer vr,integer Wu returns integer
local integer vw=ds()
if pf[vr]==0 then
set of[vr]=vw
else
set He[vw]=of[vr]
set Ge[of[vr]]=vw
set of[vr]=vw
endif
set Oe[vr]=Oe[vr]+1
set pf[vr]=pf[vr]+1
call qw(vw,Wu,Oe[vr])
return vw
endfunction
function Ow takes integer vr,integer vw returns nothing
set pf[vr]=pf[vr]-1
if of[vr]==vw then
set of[vr]=He[vw]
endif
set qf[vr]=qf[vr]-le[vw]
call tw(vw)
call Xr(vw)
endfunction
function Pw takes integer vr,integer vw,integer Aw returns nothing
set qf[vr]=qf[vr]-le[vw]+Aw
set le[vw]=Aw
endfunction
function Qw takes integer vr,integer Wu returns integer
local integer vw=ds()
if sf[vr]==0 then
set rf[vr]=vw
else
set He[vw]=rf[vr]
set Ge[rf[vr]]=vw
set rf[vr]=vw
endif
set Oe[vr]=Oe[vr]+1
set sf[vr]=sf[vr]+1
call qw(vw,Wu,Oe[vr])
return vw
endfunction
function Rw takes integer vr,integer vw returns nothing
set sf[vr]=sf[vr]-1
if rf[vr]==vw then
set rf[vr]=He[vw]
endif
set tf[vr]=tf[vr]-le[vw]
call tw(vw)
call Xr(vw)
endfunction
function Sw takes integer vr,integer vw,integer Aw returns nothing
set tf[vr]=tf[vr]-le[vw]+Aw
set le[vw]=Aw
endfunction
function Tw takes integer vr,integer Wu returns integer
local integer vw=rs()
if vf[vr]==0 then
set uf[vr]=vw
else
set He[vw]=uf[vr]
set Ge[uf[vr]]=vw
set uf[vr]=vw
endif
set Oe[vr]=Oe[vr]+1
set vf[vr]=vf[vr]+1
call qw(vw,Wu,Oe[vr])
return vw
endfunction
function Uw takes integer vr,integer vw returns nothing
set vf[vr]=vf[vr]-1
if uf[vr]==vw then
set uf[vr]=He[vw]
endif
set wf[vr]=wf[vr]-Je[vw]
set xf[vr]=wf[vr]>0
call tw(vw)
call Xr(vw)
endfunction
function Vw takes integer vr,integer vw,boolean Aw returns nothing
local integer v=tu(Aw)
set wf[vr]=wf[vr]-Je[vw]+v
set xf[vr]=wf[vr]>0
set Je[vw]=v
endfunction
function Zw takes integer vr,integer Wu returns integer
local integer vw=rs()
if bf[vr]==0 then
set Bf[vr]=vw
else
set He[vw]=Bf[vr]
set Ge[Bf[vr]]=vw
set Bf[vr]=vw
endif
set Oe[vr]=Oe[vr]+1
set bf[vr]=bf[vr]+1
call qw(vw,Wu,Oe[vr])
return vw
endfunction
function ex takes integer vr,integer vw returns nothing
set bf[vr]=bf[vr]-1
if Bf[vr]==vw then
set Bf[vr]=He[vw]
endif
set Cf[vr]=Cf[vr]-Je[vw]
set cf[vr]=Cf[vr]>0
call tw(vw)
call Xr(vw)
endfunction
function fx takes integer vr,integer vw,boolean Aw returns nothing
local integer v=tu(Aw)
set Cf[vr]=Cf[vr]-Je[vw]+v
set cf[vr]=Cf[vr]>0
set Je[vw]=v
endfunction
function gx takes integer vr,integer Wu returns integer
local integer vw=ds()
if Ef[vr]==0 then
set Df[vr]=vw
else
set He[vw]=Df[vr]
set Ge[Df[vr]]=vw
set Df[vr]=vw
endif
set Oe[vr]=Oe[vr]+1
set Ef[vr]=Ef[vr]+1
call qw(vw,Wu,Oe[vr])
return vw
endfunction
function hx takes integer vr,integer vw returns nothing
set Ef[vr]=Ef[vr]-1
if Df[vr]==vw then
set Df[vr]=He[vw]
endif
set Ff[vr]=Ff[vr]-le[vw]
set Gf[vr]=Ff[vr]>0
call tw(vw)
call Xr(vw)
endfunction
function jx takes integer vr,integer vw,boolean Aw returns nothing
local integer v=tu(Aw)
set Ff[vr]=Ff[vr]-le[vw]+v
set Gf[vr]=Ff[vr]>0
set le[vw]=v
endfunction
function kx takes integer vr,real Rs returns nothing
if vf[vr]>0 and(bf[vr]==0 or Fe[uf[vr]]>Fe[Bf[vr]])then
set Ke[uf[vr]]=Ke[uf[vr]]+Rs
else
set Ke[Bf[vr]]=Ke[Bf[vr]]+Rs
endif
endfunction
function mx takes integer vr,real Rs returns nothing
if zf[vr]>0 and(bf[vr]==0 or Fe[yf[vr]]>Fe[Bf[vr]])then
set Ke[yf[vr]]=Ke[yf[vr]]+Rs
else
set Ke[Bf[vr]]=Ke[Bf[vr]]+Rs
endif
endfunction
function nx takes integer vr,real Rs returns nothing
set Ke[Bf[vr]]=Ke[Bf[vr]]+Rs
endfunction
function ox takes integer vr returns nothing
local integer vw=Ue[vr]
local integer Bw
loop
exitwhen vw==0
set Bw=He[vw]
if yi[Ph[Ee[vw]]]then
call dt(Ee[vw])
endif
set vw=Bw
endloop
set vw=Ye[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
endfunction
function px takes integer vr returns nothing
local integer vw=Pe[vr]
local integer Bw
loop
exitwhen vw==0
set Bw=He[vw]
if yi[Ph[Ee[vw]]]then
call dt(Ee[vw])
endif
set vw=Bw
endloop
set vw=Ue[vr]
loop
exitwhen vw==0
set Bw=He[vw]
if yi[Ph[Ee[vw]]]then
call dt(Ee[vw])
endif
set vw=Bw
endloop
set vw=Ye[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=kf[vr]
loop
exitwhen vw==0
set Bw=He[vw]
if yi[Ph[Ee[vw]]]then
call dt(Ee[vw])
endif
set vw=Bw
endloop
set vw=of[vr]
loop
exitwhen vw==0
set Bw=He[vw]
if yi[Ph[Ee[vw]]]then
call dt(Ee[vw])
endif
set vw=Bw
endloop
set vw=rf[vr]
loop
exitwhen vw==0
set Bw=He[vw]
if yi[Ph[Ee[vw]]]then
call dt(Ee[vw])
endif
set vw=Bw
endloop
endfunction
function qx takes integer vr returns nothing
local integer vw=Pe[vr]
local integer Bw
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=Re[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=Ue[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=Ye[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=ff[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=kf[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=of[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=rf[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=uf[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=yf[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=Bf[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=Df[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
call ju("MinionBuffStorage "+I2S(vr)+" is deleted.")
endfunction
function rx takes integer vr returns nothing
if vr==null then
return
elseif(Ne[vr]!=-1)then
return
endif
call qx(vr)
set Ne[vr]=Le
set Le=vr
endfunction
function sx takes integer T returns integer
local integer tx=Hs()
set yi[tx]=yi[U4[T]]
set zi[tx]=zi[U4[T]]
set Ai[tx]=Ai[U4[T]]
set Bi[tx]=Bi[U4[T]]
set bi[tx]=bi[U4[T]]
set Ci[tx]=Ci[U4[T]]
set ci[tx]=ci[U4[T]]
return tx
endfunction
function ux takes integer T returns integer
local integer tx=Ir()
set ck[tx]=ck[g7[T]]
set Dk[tx]=Dk[g7[T]]
set Ek[tx]=Ek[g7[T]]
set Fk[tx]=Fk[g7[T]]
set Gk[tx]=Gk[g7[T]]
set Hk[tx]=Hk[g7[T]]
set Ik[tx]=Ik[g7[T]]
return tx
endfunction
function vx takes integer T returns integer
local integer tx=xr()
set Xn[tx]=Xn[k7[T]]
set Yn[tx]=Yn[k7[T]]
set Zn[tx]=Zn[k7[T]]
set do[tx]=do[k7[T]]
set eo[tx]=eo[k7[T]]
set fo[tx]=fo[k7[T]]
set go[tx]=go[k7[T]]
set ho[tx]=ho[k7[T]]
set io[tx]=io[k7[T]]
set jo[tx]=jo[k7[T]]
set ko[tx]=ko[k7[T]]
set mo[tx]=mo[k7[T]]
set no[tx]=no[k7[T]]
set oo[tx]=oo[k7[T]]
set po[tx]=po[k7[T]]
set qo[tx]=qo[k7[T]]
set ro[tx]=ro[k7[T]]
set so[tx]=so[k7[T]]
set to[tx]=to[k7[T]]
set uo[tx]=uo[k7[T]]
set vo[tx]=to[tx]
set wo[tx]=uo[tx]
set xo[tx]=xo[k7[T]]
set yo[tx]=yo[k7[T]]
set zo[tx]=zo[k7[T]]
return tx
endfunction
function wx takes integer p returns nothing
local integer a=0
local integer b
local integer xx
local integer f1=Og[p]
local integer f2=Pg[p]
call UnitAddAbility(tg[p],Bd[f1])
call UnitAddAbility(tg[p],bd[f2])
call UnitAddAbility(ug[p],ad[f1])
call UnitAddAbility(wg[p],Uo[Ad[f1]])
call UnitAddAbility(wg[p],Vo[Ad[f2]])
loop
set xx=yd[zd[f1]+a]
call UnitAddAbility(ug[p],Yn[k7[xx]])
set b=0
loop
call UnitAddAbility(vg[p],co[Do[n7[xx]]+b])
call UnitAddAbility(vg[p],Eo[Fo[n7[xx]]+b])
set b=b+1
exitwhen b==3
endloop
set a=a+1
exitwhen a==4
endloop
set Qg[p]=f1
set Sg[p]=f1
set Rg[p]=f2
set Tg[p]=f2
endfunction
function yx takes integer vr returns nothing
set Yg[vr]=CreateTimer()
call SaveInteger(r7,W,GetHandleId((Yg[vr])),(vr))
endfunction
function zx takes integer vr returns nothing
call Gu(Yg[vr])
set Yg[vr]=null
endfunction
function Ax takes integer vr,integer T returns nothing
set Eg[Fg[vr]+T]=Eg[Fg[vr]+T]+1
if Eg[Fg[vr]+T]==mo[cg[Dg[vr]+T]]then
call SetPlayerAbilityAvailable(Jf[vr],Yn[k7[T]],false)
endif
endfunction
function ax takes integer vr,integer T returns nothing
set Eg[Fg[vr]+T]=Eg[Fg[vr]+T]-1
call SetPlayerAbilityAvailable(Jf[vr],Yn[k7[T]],true)
endfunction
function Bx takes integer vr returns nothing
local integer a=0
loop
set Eg[Fg[vr]+a]=0
call SetPlayerAbilityAvailable(Jf[vr],Yn[k7[a]],true)
set a=a+1
exitwhen a==32
endloop
endfunction
function bx takes integer vr,integer T,real Rs returns real
set to[cg[Dg[vr]+T]]=to[cg[Dg[vr]+T]]+Rs
set Rs=(td[ud[Be[(eo[cg[Dg[vr]+T]])]]+(Cg[vr])])*Rs
set vo[cg[Dg[vr]+T]]=vo[cg[Dg[vr]+T]]+Rs
return Rs
endfunction
function Cx takes integer vr,integer T,real Rs returns real
set uo[cg[Dg[vr]+T]]=uo[cg[Dg[vr]+T]]+Rs
set Rs=(td[ud[Be[(eo[cg[Dg[vr]+T]])]]+(Cg[vr])])*Rs
set wo[cg[Dg[vr]+T]]=wo[cg[Dg[vr]+T]]+Rs
return Rs
endfunction
function Dx takes integer vr,integer pw returns nothing
set ho[cg[Dg[vr]+Qo[Jg[vr]]]]=Dn[pw]
if Ng[vr]!=0 then
if ph[Ng[vr]]!=0 then
call Es(ph[Ng[vr]])
endif
set Ti=vr
call TriggerEvaluate(h7[Dn[pw]])
endif
set Mg[vr]=pw
endfunction
function Ex takes integer vr returns nothing
set Nf[vr]=ev()
call MultiboardSetItemIcon(ig[vr],pd[Mf[Nf[vr]]])
endfunction
function Fx takes integer vr returns nothing
set Xg[vr]=0
if Pf[vr]then
call ClearTextMessages()
endif
endfunction
function Gx takes nothing returns nothing
set Xg[((LoadInteger(r7,W,GetHandleId(GetExpiredTimer()))))]=0
endfunction
function Hx takes integer vr,sound Ix,string s returns nothing
if Pf[vr]then
if Xg[vr]==5 then
set Xg[vr]=0
call ClearTextMessages()
endif
call StartSound(Ix)
call DisplayTimedTextToPlayer(Jf[vr],.0,.0,5.,s)
set Xg[vr]=Xg[vr]+1
endif
call TimerStart(Yg[vr],5.,false,function Gx)
endfunction
function lx takes integer vr returns nothing
local integer a=0
set sg[vr]=not sg[vr]
loop
call ShowImage(qg[rg[vr]+a],sg[vr])
set a=a+1
exitwhen a==120
endloop
endfunction
function Jx takes integer vr returns nothing
set eg[vr]=eg[vr]+1
call MultiboardSetItemValue(pg[vr],I2S(eg[vr]))
endfunction
function Kx takes integer vr,integer Lx returns nothing
set Zf[vr]=Zf[vr]+Lx
call MultiboardSetItemValue(ng[vr],I2S(Zf[vr]))
endfunction
function Mx takes integer vr,integer Aw returns nothing
set Vf[vr]=Vf[vr]+Aw
if Vf[vr]>99999 then
set Vf[vr]=99999
endif
call MultiboardSetItemValue(jg[vr],I2S(Vf[vr]))
endfunction
function Nx takes integer vr,integer Rs returns boolean
local boolean Ox=(Vf[(vr)]-(Rs)>=0)
if Ox then
call Mx((vr),-(Rs))
endif
return Ox
endfunction
function Px takes integer vr,integer Rs returns boolean
local boolean Ox=(Vf[(vr)]-(Rs)>=0)
if Ox then
call Mx((vr),-(Rs))
else
call Hx((vr),v8,od[hd]+"Error|r: not enough Gold.")
endif
return Ox
endfunction
function Qx takes integer vr,string s,unit u returns nothing
call vu(("+"+s),(u),(Jf[vr]),9.,48.,3.,2.)
call SetTextTagColor(bj_lastCreatedTextTag,kd[T9],md[T9],nd[T9],$FF)
endfunction
function Rx takes integer vr,integer Aw returns nothing
set Wf[vr]=Wf[vr]+Aw
if Wf[vr]>99999 then
set Wf[vr]=99999
endif
call MultiboardSetItemValue(kg[vr],I2S(Wf[vr]))
endfunction
function Sx takes integer vr,integer Rs returns boolean
local boolean Ox=(Wf[(vr)]-(Rs)>=0)
if Ox then
call Rx((vr),-(Rs))
endif
return Ox
endfunction
function Tx takes integer vr,integer Rs returns boolean
local boolean Ox=(Wf[(vr)]-(Rs)>=0)
if Ox then
call Rx((vr),-(Rs))
else
call Hx((vr),u8,od[hd]+"Error|r: not enough Experience.")
endif
return Ox
endfunction
function Ux takes integer vr,string s,unit u returns nothing
call vu(("+"+s),(u),(Jf[vr]),9.,48.,3.,2.)
call SetTextTagColor(bj_lastCreatedTextTag,kd[U9],md[U9],nd[U9],$FF)
endfunction
function Vx takes integer vr,real Aw returns nothing
set Yf[vr]=Yf[vr]-Aw
call MultiboardSetItemValue(mg[vr],I2S(su(Yf[vr]))+"/"+I2S(Xf[vr]))
endfunction
function Wx takes integer vr,real Aw returns boolean
return Yf[vr]+Aw<=Xf[vr]
endfunction
function Xx takes integer vr,integer l returns nothing
set dg[vr]=dg[vr]+l
if dg[vr]<0 then
set dg[vr]=0
elseif dg[vr]>9999 then
set dg[vr]=9999
endif
call MultiboardSetItemValue(og[vr],I2S(dg[vr]))
endfunction
function Yx takes integer vr,integer l returns nothing
if l==0 then
return
elseif l<-1 then
call Hx(vr,z8,od[gd]+"Warning|r: you lost "+od[Z9]+I2S(-l)+"|r lives.")
elseif l==-1 then
call Hx(vr,z8,od[gd]+"Warning|r: you lost "+od[Z9]+"1|r life.")
elseif l==1 then
call Hx(vr,A8,od[fd]+"Note|r: you received "+od[Z9]+"1|r life.")
elseif l>1 then
call Hx(vr,A8,od[fd]+"Note|r: you received "+od[Z9]+I2S(l)+"|r lives.")
endif
call Xx(vr,l)
endfunction
function Zx takes integer vr returns nothing
local integer ey=su(dg[vr]*.5)
if dg[vr]-ey<1 then
set ey=dg[vr]-1
endif
if ey==1 then
call Hx(vr,a8,od[gd]+"Attention|r: the game lasts too long! You lost "+od[Z9]+"1|r life.")
else
call Hx(vr,a8,od[gd]+"Attention|r: the game lasts too long! You lost "+od[Z9]+I2S(ey)+"|r lives.")
endif
call Xx(vr,-ey)
endfunction
function fy takes integer vr returns nothing
call MultiboardSetItemValueColor(gg[vr],kd[jd],md[jd],nd[jd],U8)
call MultiboardReleaseItem(gg[vr])
set gg[vr]=null
call MultiboardSetItemValueColor(hg[vr],kd[jd],md[jd],nd[jd],U8)
call MultiboardReleaseItem(hg[vr])
set hg[vr]=null
call MultiboardSetItemStyle(ig[vr],false,false)
call MultiboardReleaseItem(ig[vr])
set ig[vr]=null
call MultiboardSetItemValueColor(jg[vr],kd[jd],md[jd],nd[jd],U8)
call MultiboardReleaseItem(jg[vr])
set jg[vr]=null
call MultiboardSetItemValueColor(kg[vr],kd[jd],md[jd],nd[jd],U8)
call MultiboardReleaseItem(kg[vr])
set kg[vr]=null
call MultiboardSetItemValueColor(mg[vr],kd[jd],md[jd],nd[jd],U8)
call MultiboardReleaseItem(mg[vr])
set mg[vr]=null
call MultiboardSetItemValueColor(ng[vr],kd[jd],md[jd],nd[jd],U8)
call MultiboardReleaseItem(ng[vr])
set ng[vr]=null
call MultiboardSetItemValueColor(og[vr],kd[jd],md[jd],nd[jd],U8)
call MultiboardReleaseItem(og[vr])
set og[vr]=null
call MultiboardSetItemValueColor(pg[vr],kd[jd],md[jd],nd[jd],U8)
call MultiboardReleaseItem(pg[vr])
set pg[vr]=null
endfunction
function gy takes integer vr returns nothing
call zx(vr)
call gw(Sf[vr])
if Rf[vr]then
call vs(vr)
call fy(vr)
call RemoveUnit(tg[vr])
call RemoveUnit(ug[vr])
call RemoveUnit(wg[vr])
call RemoveUnit(vg[vr])
call RemoveUnit(xg[vr])
set tg[vr]=null
set ug[vr]=null
set vg[vr]=null
set wg[vr]=null
set xg[vr]=null
set Ug[vr]=null
set Vg[vr]=null
set Wg[vr]=null
endif
set Of[vr]=false
endfunction
function hy takes integer vr returns nothing
set Hh[vr]=CreateTimer()
call SaveInteger(r7,W,GetHandleId((Hh[vr])),(vr))
endfunction
function jy takes integer vr returns nothing
call Gu(Hh[vr])
set Hh[vr]=null
endfunction
function ky takes integer p,integer id returns boolean
local string my=GetObjectName(id)
local integer T
local real ny
if SubString(my,0,D4)=="Spawn" then
set T=S2I(SubString(my,E4,F4))
set ny=ko[cg[Dg[p]+T]]
if Wx(p,ny)then
if Px(p,io[cg[Dg[p]+T]])then
call Vx((p),-((ny)*1.))
set fh=p
call TriggerEvaluate(m7[T])
return true
endif
else
call Hx(((p)),u8,od[hd]+"Error|r: "+("unable to create a minion due to the maximum supply limit."))
endif
endif
return false
endfunction
function oy takes integer p returns nothing
if Jg[p]!=0 and Ng[p]==0 then
set fh=p
call TriggerEvaluate(m7[Qo[Jg[p]]])
set Ng[p]=(q9[r9[(zg[p])]+(s9[zg[p]])])
endif
endfunction
function py takes integer p,integer id,boolean qy returns boolean
local string my=GetObjectName(id)
local integer T
local integer ry
if SubString(my,0,G4)=="Upgrade" then
set T=S2I(SubString(my,H4,I4))
set ry=S2I(SubString(my,l4,J4))
set my=SubString(my,K4,StringLength(my))
if(qy and Px(p,Jo[Ko[n7[T]]+ry]))or((not qy)and Tx(p,Jo[Ko[n7[T]]+ry]))then
set gh=p
set Ao[ao[cg[Dg[p]+T]]+ry]=true
call TriggerExecute(Lo[Mo[n7[T]]+ry])
call SetPlayerAbilityAvailable(Jf[p],id,false)
call SetPlayerAbilityAvailable(Jf[p],Eo[Fo[n7[T]]+ry],true)
call SetPlayerTechResearched(Jf[p],Io[lo[n7[T]]+ry],1)
call Hx((p),w8,od[fd]+"Research complete|r: "+(my)+".")
return true
endif
endif
return false
endfunction
function sy takes nothing returns nothing
if(IsUnitType(GetFilterUnit(),h4))then
call IssueImmediateOrderById(GetFilterUnit(),Q)
endif
endfunction
function ty takes integer vr,integer T,integer p returns nothing
set ih[vr]=p
set mh[vr]=CreateUnit(Jf[p],Xn[k7[T]],Cu(ke[Sf[p]]),cu(ke[Sf[p]]),270.)
call UnitAddAbility(mh[vr],1098282348)
call UnitMakeAbilityPermanent(mh[vr],true,1098282348)
call SetUnitUserData(mh[vr],vr)
set hh[vr]=T
set nh[vr]=GetHandleId(mh[vr])
set vh[vr]=cg[Dg[p]+T]
call UnitAddAbility(mh[vr],1093677099)
call UnitMakeAbilityPermanent(mh[vr],true,1093677099)
call UnitAddAbility(mh[vr],Zn[k7[T]])
call UnitMakeAbilityPermanent(mh[vr],true,Zn[k7[T]])
call UnitAddAbility(mh[vr],Go[Ho[n7[T]]])
call UnitMakeAbilityPermanent(mh[vr],true,Go[Ho[n7[T]]])
call UnitAddAbility(mh[vr],Go[Ho[n7[T]]+1])
call UnitMakeAbilityPermanent(mh[vr],true,Go[Ho[n7[T]]+1])
call UnitAddAbility(mh[vr],Go[Ho[n7[T]]+2])
call UnitMakeAbilityPermanent(mh[vr],true,Go[Ho[n7[T]]+2])
set Ti=vr
call TriggerEvaluate(h7[ho[vh[vr]]])
call UnitAddAbility(mh[vr],1093677122)
call UnitMakeAbilityPermanent(mh[vr],true,1093677122)
call UnitAddAbility(mh[vr],ck[g7[ho[vh[vr]]]])
call UnitMakeAbilityPermanent(mh[vr],true,ck[g7[ho[vh[vr]]]])
call hv(zg[p],vr)
call Kx(p,no[vh[vr]])
call Ax(p,T)
endfunction
function uy takes integer vr,integer T,integer p returns nothing
local integer a
set ih[vr]=p
set mh[vr]=CreateUnit(Jf[p],Xn[k7[T]],Cu(ke[Sf[p]]),cu(ke[Sf[p]]),270.)
call UnitAddAbility(mh[vr],1098282348)
call UnitMakeAbilityPermanent(mh[vr],true,1098282348)
call SetUnitUserData(mh[vr],vr)
set hh[vr]=T
set nh[vr]=GetHandleId(mh[vr])
set vh[vr]=cg[Dg[p]+T]
call UnitAddAbility(mh[vr],1093677099)
call UnitMakeAbilityPermanent(mh[vr],true,1093677099)
call UnitAddAbility(mh[vr],Zn[k7[T]])
call UnitMakeAbilityPermanent(mh[vr],true,Zn[k7[T]])
call UnitAddAbility(mh[vr],Go[Ho[n7[T]]])
call UnitMakeAbilityPermanent(mh[vr],true,Go[Ho[n7[T]]])
call UnitAddAbility(mh[vr],Go[Ho[n7[T]]+1])
call UnitMakeAbilityPermanent(mh[vr],true,Go[Ho[n7[T]]+1])
call UnitAddAbility(mh[vr],Go[Ho[n7[T]]+2])
call UnitMakeAbilityPermanent(mh[vr],true,Go[Ho[n7[T]]+2])
set Ti=vr
call TriggerEvaluate(h7[ho[vh[vr]]])
call hv(zg[p],vr)
set T=0
loop
set a=Ro[So[Jg[p]]+T]
call UnitAddAbility(mh[vr],Hn[a])
call UnitMakeAbilityPermanent(mh[vr],true,Hn[a])
call UnitAddAbility(mh[vr],Mn[Nn[a]])
call UnitMakeAbilityPermanent(mh[vr],true,Mn[Nn[a]])
call UnitAddAbility(mh[vr],Mn[Nn[a]+1])
call UnitMakeAbilityPermanent(mh[vr],true,Mn[Nn[a]+1])
set T=T+1
exitwhen T==3
endloop
endfunction
function vy takes integer vr returns nothing
call iv(zg[ih[vr]],vr)
call ax(ih[vr],hh[vr])
call Vx(ih[vr],jo[vh[vr]])
call Kx(ih[vr],-no[vh[vr]])
call Mx(ih[vr],io[vh[vr]])
if io[vh[vr]]>0 then
call Qx(ih[vr],I2S(io[vh[vr]]),mh[vr])
endif
call DestroyEffect(AddSpecialEffect(("Abilities\\Spells\\Other\\Transmute\\PileofGold.mdx"),((GetUnitX(mh[vr]))*1.),((GetUnitY(mh[vr]))*1.)))
call RemoveUnit(mh[vr])
call Xs(vr)
call ju("Minion "+I2S(vr)+" is sold.")
endfunction
function wy takes integer vr returns nothing
call UnitRemoveAbility(mh[vr],1093677099)
set jh[vr]=Nf[ih[vr]]
set kh[vr]=Sf[jh[vr]]
call jv(Ag[jh[vr]],vr)
call SetUnitMoveSpeed(mh[vr],256.)
call SetUnitOwner(mh[vr],Qf[jh[vr]],false)
call SetUnitX(mh[vr],GetUnitX(mh[vr])-GetRectCenterX(ke[Sf[ih[vr]]])+GetRectCenterX(me[kh[vr]]))
call SetUnitY(mh[vr],GetUnitY(mh[vr])-GetRectCenterY(ke[Sf[ih[vr]]])+GetRectCenterY(me[kh[vr]]))
set wh[vr]=Cu(oe[kh[vr]])
set xh[vr]=GetRectMinY(oe[kh[vr]])
call IssuePointOrderById(mh[vr],P,wh[vr],xh[vr])
call ju("Minion "+I2S(vr)+" is sent.")
endfunction
function xy takes integer vr returns nothing
call kv(Ag[jh[vr]],vr)
call Vx(ih[vr],jo[vh[vr]])
call RemoveUnit(mh[vr])
call Xs(vr)
call ju("Spawned "+I2S(vr)+" is deleted.")
endfunction
function yy takes integer vr,boolean zy returns nothing
local string Ay
if zy then
set Ay=od[Y9]+"+1"
else
set Ay=""
endif
if oo[vh[vr]]>0 then
call Mx(jh[vr],oo[vh[vr]])
set Ay=Ay+"|r
"+od[T9]+"+"+I2S(oo[vh[vr]])
endif
if po[vh[vr]]>0 then
call Rx(jh[vr],po[vh[vr]])
set Ay=Ay+"|r
"+od[U9]+"+"+I2S(po[vh[vr]])
endif
call vu((Ay),(mh[vr]),(Jf[jh[vr]]),9.,48.,3.,2.)
endfunction
function ay takes integer vr returns boolean
return not(qh[vr]or rh[vr])
endfunction
function By takes integer vr returns nothing
local real Bw=(Te[oh[(vr)]])
if Bw>.0 then
call UnitAddAbility(mh[vr],1093677105)
else
call UnitRemoveAbility(mh[vr],1093677105)
endif
call SetItemCharges(Ah[vr],su(Bw))
endfunction
function by takes integer vr returns real
return sh[vr]/ th[vr]
endfunction
function Cy takes integer vr,real Aw returns nothing
if Aw<.0 then
set Aw=.0
elseif Aw>th[vr]then
set Aw=th[vr]
endif
set sh[vr]=Aw
call SetItemCharges(zh[vr],su(sh[vr]))
call SetWidgetLife(mh[vr],3.+sh[vr]/ th[vr]*1000.)
endfunction
function Dy takes integer vr,real Aw returns nothing
call Cy(vr,sh[vr]+Aw)
endfunction
function Ey takes integer vr,real Fy,real Gy returns nothing
set th[vr]=th[vr]+Fy
set uh[vr]=uh[vr]+Gy
if qh[vr]then
call Dy(vr,Fy)
endif
endfunction
function Hy takes integer vr,real Aw,string Iy,string Eu returns nothing
if sh[vr]<th[vr]then
call Dy(vr,Aw)
call DestroyEffect(AddSpecialEffectTarget((Iy),(mh[vr]),(Eu)))
endif
endfunction
function ly takes integer vr,real Jy,string Iy,string Eu returns nothing
call Hy(vr,th[vr]*Jy,Iy,Eu)
endfunction
function Ky takes integer vr returns integer
local integer Ly
set N4=Js(vr)
set Ly=N4+nf[oh[vr]]+tf[oh[vr]]
if Ly>75 then
return 75
elseif Ly<L4 then
return L4
endif
return Ly
endfunction
function My takes integer vr returns integer
local integer Ly
set O4=Ks(vr)
set Ly=O4+qf[oh[vr]]+tf[oh[vr]]
if Ly>75 then
return 75
elseif Ly<M4 then
return M4
endif
return Ly
endfunction
function Ny takes integer vr returns nothing
local real Oy=ls(vr)
local real Py=Xe[oh[vr]]
local real Qy
call SetUnitMoveSpeed(mh[vr],256.*(Oy+Py))
set Qy=GetUnitMoveSpeed(mh[vr])
set Py=Qy/ 256.-Oy
if Py>.0 then
call UnitRemoveAbility(mh[vr],1093677134)
call UnitAddAbility(mh[vr],1093677128)
elseif Py<.0 then
call UnitRemoveAbility(mh[vr],1093677128)
call UnitAddAbility(mh[vr],1093677134)
else
call UnitRemoveAbility(mh[vr],1093677134)
call UnitRemoveAbility(mh[vr],1093677128)
endif
call SetItemCharges(bh[vr],ru(Qy/ 256.*100.))
endfunction
function Ry takes integer vr returns nothing
if(ef[oh[(vr)]])then
call IssueImmediateOrderById(mh[vr],U)
call UnitAddAbility(mh[vr],1093677142)
else
call IssuePointOrderById(mh[vr],P,wh[vr],xh[vr])
call UnitRemoveAbility(mh[vr],1093677142)
endif
endfunction
function Sy takes integer vr returns nothing
local integer Ly=Ky(vr)
local integer Ty=N4
if Ly>0 then
call SetItemCharges(ah[vr],Ly)
call UnitRemoveAbility(mh[vr],1093677115)
call UnitRemoveAbility(mh[vr],1110454321)
call UnitAddAbility(mh[vr],1093677114)
elseif Ly<0 then
call SetItemCharges(ah[vr],-Ly)
call UnitRemoveAbility(mh[vr],1093677114)
call UnitRemoveAbility(mh[vr],1110454320)
call UnitAddAbility(mh[vr],1093677115)
else
call SetItemCharges(ah[vr],0)
call UnitRemoveAbility(mh[vr],1093677114)
call UnitRemoveAbility(mh[vr],1110454320)
call UnitRemoveAbility(mh[vr],1093677115)
call UnitRemoveAbility(mh[vr],1110454321)
endif
if Ly>Ty then
call UnitRemoveAbility(mh[vr],1093677089)
call UnitAddAbility(mh[vr],1093677147)
elseif Ly<Ty then
call UnitRemoveAbility(mh[vr],1093677147)
call UnitAddAbility(mh[vr],1093677089)
else
call UnitRemoveAbility(mh[vr],1093677089)
call UnitRemoveAbility(mh[vr],1093677147)
endif
endfunction
function Uy takes integer vr returns nothing
local integer Ly=My(vr)
local integer Ty=O4
if Ly>0 then
call SetItemCharges(Bh[vr],Ly)
call UnitRemoveAbility(mh[vr],1093677117)
call UnitRemoveAbility(mh[vr],1110454323)
call UnitAddAbility(mh[vr],1093677116)
elseif Ly<0 then
call SetItemCharges(Bh[vr],-Ly)
call UnitRemoveAbility(mh[vr],1093677116)
call UnitRemoveAbility(mh[vr],1110454322)
call UnitAddAbility(mh[vr],1093677117)
else
call SetItemCharges(Bh[vr],0)
call UnitRemoveAbility(mh[vr],1093677116)
call UnitRemoveAbility(mh[vr],1110454322)
call UnitRemoveAbility(mh[vr],1093677117)
call UnitRemoveAbility(mh[vr],1110454323)
endif
if Ly>Ty then
call UnitRemoveAbility(mh[vr],1093677150)
call UnitAddAbility(mh[vr],1093677149)
elseif Ly<Ty then
call UnitRemoveAbility(mh[vr],1093677149)
call UnitAddAbility(mh[vr],1093677150)
else
call UnitRemoveAbility(mh[vr],1093677150)
call UnitRemoveAbility(mh[vr],1093677149)
endif
endfunction
function Vy takes integer vr returns nothing
local integer Ly=Ky(vr)
local integer Ty=N4
if Ly>0 then
call SetItemCharges(ah[vr],Ly)
call UnitRemoveAbility(mh[vr],1093677115)
call UnitRemoveAbility(mh[vr],1110454321)
call UnitAddAbility(mh[vr],1093677114)
elseif Ly<0 then
call SetItemCharges(ah[vr],-Ly)
call UnitRemoveAbility(mh[vr],1093677114)
call UnitRemoveAbility(mh[vr],1110454320)
call UnitAddAbility(mh[vr],1093677115)
else
call SetItemCharges(ah[vr],0)
call UnitRemoveAbility(mh[vr],1093677114)
call UnitRemoveAbility(mh[vr],1110454320)
call UnitRemoveAbility(mh[vr],1093677115)
call UnitRemoveAbility(mh[vr],1110454321)
endif
if Ly>Ty then
call UnitRemoveAbility(mh[vr],1093677089)
call UnitAddAbility(mh[vr],1093677147)
elseif Ly<Ty then
call UnitRemoveAbility(mh[vr],1093677147)
call UnitAddAbility(mh[vr],1093677089)
else
call UnitRemoveAbility(mh[vr],1093677089)
call UnitRemoveAbility(mh[vr],1093677147)
endif
set Ly=My(vr)
set Ty=O4
if Ly>0 then
call SetItemCharges(Bh[vr],Ly)
call UnitRemoveAbility(mh[vr],1093677117)
call UnitRemoveAbility(mh[vr],1110454323)
call UnitAddAbility(mh[vr],1093677116)
elseif Ly<0 then
call SetItemCharges(Bh[vr],-Ly)
call UnitRemoveAbility(mh[vr],1093677116)
call UnitRemoveAbility(mh[vr],1110454322)
call UnitAddAbility(mh[vr],1093677117)
else
call SetItemCharges(Bh[vr],0)
call UnitRemoveAbility(mh[vr],1093677116)
call UnitRemoveAbility(mh[vr],1110454322)
call UnitRemoveAbility(mh[vr],1093677117)
call UnitRemoveAbility(mh[vr],1110454323)
endif
if Ly>Ty then
call UnitRemoveAbility(mh[vr],1093677150)
call UnitAddAbility(mh[vr],1093677149)
elseif Ly<Ty then
call UnitRemoveAbility(mh[vr],1093677149)
call UnitAddAbility(mh[vr],1093677150)
else
call UnitRemoveAbility(mh[vr],1093677150)
call UnitRemoveAbility(mh[vr],1093677149)
endif
endfunction
function Wy takes integer vr,integer vw,real Aw returns nothing
call zw(oh[vr],vw,Aw)
call By(vr)
endfunction
function Xy takes integer vr,integer vw,real Aw returns nothing
call Ew(oh[vr],vw,Aw)
call Ny(vr)
endfunction
function Zy takes integer vr,integer vw,boolean Aw returns nothing
local boolean dz=not(jf[oh[(vr)]])
call Jw(oh[vr],vw,Aw)
if Aw and dz then
call ox(oh[vr])
call Ny(vr)
call Ry(vr)
call ju("Minion is Unstoppable!")
endif
endfunction
function fz takes integer vr,integer vw,integer Aw returns nothing
call Pw(oh[vr],vw,Aw)
call Uy(vr)
endfunction
function gz takes integer vr,integer vw,integer Aw returns nothing
call Sw(oh[vr],vw,Aw)
call Vy(vr)
endfunction
function hz takes integer vr,integer vw,boolean Aw returns nothing
local boolean dz=not(Gf[oh[(vr)]])
call jx(oh[vr],vw,Aw)
if Aw then
if dz then
call px(oh[vr])
call Ny(vr)
call Ry(vr)
call Vy(vr)
call UnitAddType(mh[vr],j4)
call ju("Minion is Invulnerable!")
endif
elseif not(Gf[oh[(vr)]])then
call UnitRemoveType(mh[(vr)],j4)
endif
endfunction
function iz takes integer vr,integer rw,real Aw returns integer
local integer vw=xw(oh[vr],rw)
call Wy(vr,vw,Aw)
return vw
endfunction
function jz takes integer vr,integer rw,real Aw returns integer
local integer vw=bw(oh[vr],rw)
call Xy(vr,vw,Aw)
return vw
endfunction
function mz takes integer vr,integer rw,boolean Aw returns integer
local integer vw=Iw(oh[vr],rw)
call Zy(vr,vw,Aw)
return vw
endfunction
function nz takes integer vr,integer rw,integer Aw returns integer
local integer vw=Nw(oh[vr],rw)
call fz(vr,vw,Aw)
return vw
endfunction
function oz takes integer vr,integer rw,integer Aw returns integer
local integer vw=Qw(oh[vr],rw)
call gz(vr,vw,Aw)
return vw
endfunction
function pz takes integer vr,integer rw,boolean Aw returns integer
local integer vw=Tw(oh[vr],rw)
call Vw(oh[(vr)],(vw),(Aw))
return vw
endfunction
function qz takes integer vr,integer rw,boolean Aw returns integer
local integer vw=Zw(oh[vr],rw)
call fx(oh[(vr)],(vw),(Aw))
return vw
endfunction
function rz takes integer vr,integer rw,boolean Aw returns integer
local integer vw=gx(oh[vr],rw)
call hz(vr,vw,Aw)
return vw
endfunction
function sz takes integer vr,integer vw returns nothing
call yw(oh[vr],vw)
call By(vr)
endfunction
function tz takes integer vr,integer vw returns nothing
call cw(oh[vr],vw)
call Ny(vr)
endfunction
function uz takes integer vr,integer vw returns nothing
call Gw(oh[vr],vw)
call Ry(vr)
endfunction
function vz takes integer vr,integer vw returns nothing
call Ow(oh[vr],vw)
call Uy(vr)
endfunction
function wz takes integer vr,integer vw returns nothing
call Rw(oh[vr],vw)
call Vy(vr)
endfunction
function xz takes integer vr,integer vw returns nothing
call hx(oh[vr],vw)
call UnitRemoveType(mh[(vr)],j4)
endfunction
function yz takes integer vr returns boolean
return(Gf[oh[(vr)]])or(cf[oh[(vr)]])or Ms(vr)
endfunction
function zz takes integer vr,integer Bs returns boolean
return(Gf[oh[(vr)]])or(cf[oh[(vr)]])or Ms(vr)or(Bs==1227894834 and((xf[oh[(vr)]])or Ns(vr)))or(Bs==1227894841 and((af[oh[(vr)]])or Os(vr)))or(Bs==1227894832 and Ps(vr))
endfunction
function Az takes integer vr,real Rs returns real
return(100-Ky(vr))*Rs/ 100.
endfunction
function az takes integer vr,real Rs returns real
return(100-My(vr))*Rs/ 100.
endfunction
function Bz takes integer vr,real Rs,integer Bs returns real
if(Gf[oh[(vr)]])then
return .0
endif
if Bs==1227894834 then
if(xf[oh[(vr)]])or(cf[oh[(vr)]])then
call kx(oh[vr],Rs)
return .0
endif
return Qs(vr,Rs)
endif
if Bs==1227894841 then
if(af[oh[(vr)]])or(cf[oh[(vr)]])then
call mx(oh[vr],Rs)
return .0
endif
return Ss(vr,Rs)
endif
if Bs==1227894832 then
if(cf[oh[(vr)]])then
call nx(oh[vr],(th[(vr)]*((Rs)*1.)/ 1000.))
return .0
endif
return Ts(vr,Rs)
endif
call ju("Unrecognized damage type!")
return .0
endfunction
function bz takes integer vr,real Rs returns real
set Rs=aw(oh[vr],Rs)
call By(vr)
return Rs
endfunction
function Cz takes integer vr,integer cz,real Rs,integer Bs,boolean Dz returns boolean
if not zz(vr,Bs)then
call iu(1,vr,cz,Bs)
endif
set Rs=Bz(vr,Rs,Bs)
if Rs<=.0 then
return true
endif
set Rs=bz(vr,Rs)
if Rs>.0 then
call Dy(vr,-Rs)
endif
if sh[vr]>.0 then
return true
endif
call yy(vr,true)
call ms(cz,vr)
call Us(vr,Dz)
return false
endfunction
function Ez takes integer vr returns integer
if IsUnitType(mh[vr],UNIT_TYPE_FLYING)then
return Id[hw(kh[vr],GetUnitX(mh[vr]),GetUnitY(mh[vr]))]
endif
return Gd[hw(kh[vr],GetUnitX(mh[vr]),GetUnitY(mh[vr]))]
endfunction
function Fz takes integer vr returns nothing
call nv(ag[jh[vr]],vr)
call Vx(ih[vr],jo[vh[vr]])
call rx(oh[vr])
call RemoveItem(zh[vr])
set zh[vr]=null
call RemoveItem(Ah[vr])
set Ah[vr]=null
call RemoveItem(ah[vr])
set ah[vr]=null
call RemoveItem(Bh[vr])
set Bh[vr]=null
call RemoveItem(bh[vr])
set bh[vr]=null
call RemoveItem(Ch[vr])
set Ch[vr]=null
call UnitRemoveAbility(mh[vr],1093677105)
call UnitRemoveAbility(mh[vr],1093677128)
call UnitRemoveAbility(mh[vr],1093677134)
call UnitRemoveAbility(mh[vr],1093677142)
call UnitRemoveAbility(mh[vr],1093677149)
call UnitRemoveAbility(mh[vr],1093677150)
call UnitRemoveAbility(mh[vr],1093677147)
call UnitRemoveAbility(mh[vr],1093677089)
endfunction
function Gz takes integer vr returns nothing
call rv(Bg[ih[vr]],vr)
call Fz(vr)
call jy(vr)
call Xs(vr)
call ju("Minion "+I2S(vr)+" is deleted.")
endfunction
function Hz takes nothing returns nothing
local integer vr=(LoadInteger(r7,W,GetHandleId(GetExpiredTimer())))
if uh[vr]>.0 then
call Dy(vr,uh[vr])
endif
set yh[vr]=yh[vr]-.25
if yh[vr]>.0 then
call SetItemCharges(Ch[vr],su(yh[vr]))
else
call nw(mh[vr])
call Gz(vr)
endif
endfunction
function Iz takes integer vr returns nothing
call kv(Ag[jh[vr]],vr)
set th[vr]=vo[vh[vr]]
set uh[vr]=wo[vh[vr]]
set yh[vr]=do[vh[vr]]
call hy(vr)
call UnitAddAbility(mh[vr],1093677113)
call UnitAddAbility(mh[vr],1093677118)
set zh[vr]=UnitAddItemById(mh[vr],1227894836)
set Ah[vr]=UnitAddItemById(mh[vr],1227894847)
set ah[vr]=UnitAddItemById(mh[vr],1227894840)
set Bh[vr]=UnitAddItemById(mh[vr],1227894837)
set bh[vr]=UnitAddItemById(mh[vr],1227894848)
set Ch[vr]=UnitAddItemById(mh[vr],1227894849)
call SetItemCharges(Ch[vr],su(yh[vr]))
call TriggerRegisterUnitEvent(t8,mh[vr],EVENT_UNIT_DAMAGED)
call TimerStart(Hh[vr],.25,true,function Hz)
set oh[vr]=ss()
call mv(ag[jh[vr]],vr)
call qv(Bg[ih[vr]],vr)
set Ti=vr
call TriggerEvaluate(i7[ho[vh[vr]]])
call Cy(vr,th[vr])
call Ny(vr)
call Vy(vr)
set qh[vr]=true
call SetUnitInvulnerable(mh[vr],false)
set rh[vr]=false
endfunction
function lz takes integer vr returns boolean
return Wx(ih[vr],jo[vh[vr]])
endfunction
function Jz takes integer vr,integer Kz,real Jy returns nothing
call pv(bg[jh[vr]],vr)
call RemoveUnit(mh[vr])
set mh[vr]=CreateUnit(Qf[jh[vr]],Kz,ch[vr],Dh[vr],Eh[vr])
set nh[vr]=GetHandleId(mh[vr])
call SetUnitColor(mh[vr],GetPlayerColor(Jf[ih[vr]]))
call SetUnitUserData(mh[vr],vr)
call Vx((ih[vr]),-((jo[vh[vr]])*1.))
set yh[vr]=do[vh[vr]]
call UnitAddAbility(mh[vr],1093677113)
call UnitAddAbility(mh[vr],1093677118)
set zh[vr]=UnitAddItemById(mh[vr],1227894836)
set Ah[vr]=UnitAddItemById(mh[vr],1227894847)
set ah[vr]=UnitAddItemById(mh[vr],1227894840)
set Bh[vr]=UnitAddItemById(mh[vr],1227894837)
set bh[vr]=UnitAddItemById(mh[vr],1227894848)
set Ch[vr]=UnitAddItemById(mh[vr],1227894849)
call SetItemCharges(Ch[vr],su(yh[vr]))
call TriggerRegisterUnitEvent(t8,mh[vr],EVENT_UNIT_DAMAGED)
call TimerStart(Hh[vr],.25,true,function Hz)
set oh[vr]=ss()
call mv(ag[jh[vr]],vr)
call cs(ph[vr])
call Cy(vr,th[vr]*Jy)
call Ny(vr)
call Vy(vr)
set qh[vr]=true
call IssuePointOrderById(mh[vr],P,wh[vr],xh[vr])
endfunction
function Lz takes integer vr,real Jy returns nothing
call Jz(vr,GetUnitTypeId(mh[vr]),Jy)
endfunction
function Mz takes integer vr returns nothing
call pv(bg[jh[vr]],vr)
call rv(Bg[ih[vr]],vr)
call jy(vr)
call RemoveUnit(mh[vr])
call Xs(vr)
call ju("Corpse "+I2S(vr)+" is removed.")
endfunction
function Nz takes nothing returns nothing
call Mz(((LoadInteger(r7,W,GetHandleId(GetExpiredTimer())))))
endfunction
function Oz takes integer vr returns nothing
call RemoveUnit(mh[vr])
call Gz(vr)
endfunction
function Pz takes integer vr,boolean Vs returns nothing
call SetUnitExploded(mh[vr],Vs)
call KillUnit(mh[vr])
if Vs or go[vh[vr]]then
call Gz(vr)
else
set qh[vr]=false
call Fz(vr)
call ov(bg[jh[vr]],vr)
call Cs(ph[vr])
set ch[vr]=GetUnitX(mh[vr])
set Dh[vr]=GetUnitY(mh[vr])
set Eh[vr]=GetUnitFacing(mh[vr])
call TimerStart(Hh[vr],15.,false,function Nz)
endif
endfunction
function s__Minion_on_init takes nothing returns nothing
set Ih=Filter(function sy)
endfunction
function Qz takes integer Rz returns integer
if Rz<=9 then
return Q4[Rz]
endif
return Q4[9]
endfunction
function Sz takes integer p returns nothing
local integer m
local integer a=0
local real Oy
local real array Tz
local real array Uz
local integer Vz=Qz(Cg[p])
if Px(p,Vz)then
set Cg[p]=Cg[p]+1
loop
set Oy=(td[ud[Be[(eo[cg[Dg[p]+a]])]]+(Cg[p])])
set Tz[a]=to[cg[Dg[p]+a]]*Oy-vo[cg[Dg[p]+a]]
set vo[cg[Dg[p]+a]]=vo[cg[Dg[p]+a]]+Tz[a]
set Uz[a]=uo[cg[Dg[p]+a]]*Oy-wo[cg[Dg[p]+a]]
set wo[cg[Dg[p]+a]]=wo[cg[Dg[p]+a]]+Uz[a]
set a=a+1
exitwhen a==32
endloop
set a=0
loop
exitwhen a>M9[Bg[p]]
set m=(K9[L9[(Bg[p])]+(a)])
call Ey(m,Tz[hh[m]],Uz[hh[m]])
set a=a+1
endloop
call SetItemCharges(Vg[p],Cg[p])
if Cg[p]==P4 then
call Av(vg[p],Wg[p])
else
call SetItemCharges(Wg[p],Qz(Cg[p])+1)
endif
call Hx((p),w8,od[fd]+"Research complete|r: "+(("Minion Health Upgrade (level "+I2S((Cg[p]))+")"))+".")
else
call SetItemCharges(Wg[p],Vz+1)
endif
endfunction
function Wz takes nothing returns nothing
set Ih=Filter(function sy)
set Q4[0]=500
set Q4[1]=540
set Q4[2]=585
set Q4[3]=630
set Q4[4]=680
set Q4[5]=735
set Q4[6]=795
set Q4[7]=855
set Q4[8]=925
set Q4[9]=$3E8
set R4=Q4[0]
endfunction
function Xz takes integer p,integer id,real Aw returns nothing
local integer a=0
local integer m
local real Yz=bx(p,id,Aw)
loop
exitwhen a>M9[Bg[p]]
set m=(K9[L9[(Bg[p])]+(a)])
if hh[m]==id then
set th[m]=th[m]+Yz
if qh[m]then
call Dy(m,Yz)
endif
endif
set a=a+1
endloop
endfunction
function Zz takes integer p,integer id,real Aw returns nothing
local integer a=0
local integer m
local real Uz=Cx(p,id,Aw)
loop
exitwhen a>M9[Bg[p]]
set m=(K9[L9[(Bg[p])]+(a)])
if hh[m]==id then
set uh[m]=uh[m]+Uz
endif
set a=a+1
endloop
endfunction
function dA takes integer p,integer id,real Aw returns nothing
local integer a=0
local integer m
set xo[cg[Dg[p]+id]]=xo[cg[Dg[p]+id]]+Aw
loop
exitwhen a>M9[Bg[p]]
set m=(K9[L9[(Bg[p])]+(a)])
if hh[m]==id and qh[m]then
call Ny(m)
endif
set a=a+1
endloop
endfunction
function eA takes integer p,integer id,integer Aw returns nothing
local integer a=0
local integer m
set yo[cg[Dg[p]+id]]=yo[cg[Dg[p]+id]]+Aw
set zo[cg[Dg[p]+id]]=zo[cg[Dg[p]+id]]+Aw
loop
exitwhen a>M9[Bg[p]]
set m=(K9[L9[(Bg[p])]+(a)])
if hh[m]==id and qh[m]then
call Vy(m)
endif
set a=a+1
endloop
endfunction
function fA takes integer p,integer id,integer Aw returns nothing
local integer a=0
local integer m
set yo[cg[Dg[p]+id]]=yo[cg[Dg[p]+id]]+Aw
loop
exitwhen a>M9[Bg[p]]
set m=(K9[L9[(Bg[p])]+(a)])
if hh[m]==id and qh[m]then
call Sy(m)
endif
set a=a+1
endloop
endfunction
function gA takes integer vr returns nothing
set Uh[vr]=CreateTimer()
call SaveInteger(r7,W,GetHandleId((Uh[vr])),(vr))
endfunction
function hA takes integer vr returns nothing
call Gu(Uh[vr])
set Uh[vr]=null
endfunction
function iA takes integer vr,integer jA,unit kA,integer p,integer T returns nothing
if vr==0 then
call Ru("Error: MinionBuff struct has run out of indexes. Current amount of indexes is 8190. Error detected on field of player "+I2S(p),"MinionBuffError")
endif
set Lh[vr]=T
set Mh[vr]=jA
set Nh[vr]=kA
set Oh[vr]=p
set Ph[vr]=Ig[lg[p]+T]
call gA(vr)
call SaveInteger(r7,nh[Mh[vr]],T,vr)
endfunction
function mA takes nothing returns nothing
local integer vr=(LoadInteger(r7,W,GetHandleId(GetExpiredTimer())))
set Qh[vr]=Qh[vr]-1
if Ys(vr)then
call dt(vr)
endif
endfunction
function nA takes integer vr,integer oA returns nothing
set Qh[vr]=Qh[vr]+oA
if Qh[vr]>Bi[Ph[vr]]then
set Qh[vr]=Bi[Ph[vr]]
endif
endfunction
function pA takes integer vr,real qA returns nothing
set Qh[vr]=Ai[Ph[vr]]
call UnitAddAbility(mh[Mh[vr]],ci[Ph[vr]])
call TimerStart(Uh[vr],qA,zi[Ph[vr]],function mA)
set Rh[vr]=true
endfunction
function rA takes integer vr returns nothing
call pA(vr,Ci[Ph[vr]])
endfunction
function sA takes integer vr,integer oA returns nothing
if Rh[vr]then
call nA(vr,oA)
else
call rA(vr)
endif
endfunction
function tA takes integer m returns integer
local integer vr=qt()
set Yh[vr]=m
set Zh[vr]=CreateUnit(Jf[Oj[m]],1685417325,Vj[m],Wj[m],.0)
call SetUnitPathing(Zh[vr],false)
call SetUnitFlyHeight(Zh[vr],Tj[m],.0)
call UnitAddType(Zh[vr],g4)
set ei[vr]=ip[dk[m]]
call UnitAddAbility(Zh[vr],ei[vr])
call SetUnitUserData(Zh[vr],m)
set di[vr]=jp[dk[m]]
call ju("TowerAttacker "+I2S(vr)+" is created.")
return vr
endfunction
function uA takes integer vr,integer vA,integer wA returns nothing
call UnitRemoveAbility(Zh[vr],ei[vr])
set ei[vr]=vA
call UnitAddAbility(Zh[vr],vA)
set di[vr]=wA
endfunction
function xA takes integer vr returns nothing
call uA(vr,ip[dk[Yh[vr]]],jp[dk[Yh[vr]]])
endfunction
function yA takes integer vr returns nothing
call IncUnitAbilityLevel(Zh[vr],ei[vr])
endfunction
function zA takes integer vr,real x,real y returns boolean
return IssuePointOrderById(Zh[vr],di[vr],x,y)
endfunction
function AA takes integer vr,widget zs returns boolean
return IssueTargetOrderById(Zh[vr],di[vr],zs)
endfunction
function aA takes integer vr returns nothing
call RemoveUnit(Zh[vr])
set Zh[vr]=null
call ju("TowerAttacker "+I2S(vr)+" is deleted.")
endfunction
function BA takes integer vr returns nothing
if vr==null then
return
elseif(Xh[vr]!=-1)then
return
endif
call aA(vr)
set Xh[vr]=Vh
set Vh=vr
endfunction
function bA takes integer m returns integer
local integer vr=ot()
set ii[vr]=m
set ji[vr]=Ez(m)
set ki[vr]=sh[m]
return vr
endfunction
function CA takes integer vr,integer cA returns nothing
set ni[vr]=cA
set mi[vr]=mi[cA]
if mi[vr]!=0 then
set ni[mi[vr]]=vr
endif
set mi[cA]=vr
endfunction
function DA takes integer vr,integer cA returns nothing
set mi[vr]=cA
set ni[vr]=ni[cA]
if ni[vr]!=0 then
set mi[ni[vr]]=vr
endif
set ni[cA]=vr
endfunction
function EA takes integer vr returns integer
local integer Uv=ri[vr]
local integer m=ii[Uv]
set ri[vr]=ni[Uv]
if(ri[(vr)]!=0)then
set mi[ri[vr]]=0
endif
call pt(Uv)
return m
endfunction
function FA takes integer GA,integer HA returns integer
local integer vr=mt()
set ri[vr]=GA
set si[vr]=HA
return vr
endfunction
function IA takes integer vr returns nothing
local integer n=ri[vr]
local integer lA
loop
exitwhen n==0
set lA=ni[n]
call pt(n)
set n=lA
endloop
endfunction
function JA takes integer jA returns integer
local group g=CreateGroup()
local unit u
local integer c
local integer vw
local integer HA=0
local integer GA=0
call GroupEnumUnitsInRange(g,Vj[jA],Wj[jA],fp[dk[jA]],ep[dk[jA]])
loop
set u=FirstOfGroup(g)
exitwhen u==null
call GroupRemoveUnit(g,u)
set c=bA(GetUnitUserData(u))
set vw=HA
loop
if vw==0 then
if GA==0 then
set HA=c
set GA=c
else
call CA(c,GA)
set GA=c
endif
exitwhen true
elseif ji[c]>=ji[vw]then
call DA(c,vw)
if vw==HA then
set HA=c
endif
exitwhen true
endif
set vw=mi[vw]
endloop
endloop
call DestroyGroup(g)
set g=null
if GA==0 then
return 0
endif
return FA(GA,HA)
endfunction
function KA takes nothing returns boolean
local integer m=GetUnitUserData(GetFilterUnit())
return((IsUnitType(GetFilterUnit(),h4))and qh[m]and not(Gf[oh[(m)]]))!=null
endfunction
function LA takes nothing returns boolean
local integer m=GetUnitUserData(GetFilterUnit())
return((IsUnitType(GetFilterUnit(),h4))and IsUnitType(mh[m],UNIT_TYPE_GROUND)and qh[m]and not(Gf[oh[(m)]]))!=null
endfunction
function MA takes nothing returns boolean
local integer m=GetUnitUserData(GetFilterUnit())
return((IsUnitType(GetFilterUnit(),h4))and IsUnitType(mh[m],UNIT_TYPE_FLYING)and qh[m]and not(Gf[oh[(m)]]))!=null
endfunction
function NA takes nothing returns nothing
set T4[0]=Filter(function KA)
set T4[1]=Filter(function LA)
set T4[2]=Filter(function MA)
endfunction
function OA takes integer vr,integer PA returns nothing
set Sh[vr]=nz(Mh[vr],vr,PA)
endfunction
function QA takes integer m,unit RA,integer p,integer PA returns nothing
local integer vr=(LoadInteger(r7,nh[(m)],(8)))
if vr==0 then
set vr=kt()
call iA(vr,m,RA,p,8)
call OA(vr,PA)
elseif Nh[vr]!=RA then
set Nh[vr]=RA
endif
call rA(vr)
endfunction
function SA takes integer vr returns nothing
set Sh[vr]=qz(Mh[vr],vr,true)
endfunction
function TA takes integer m,integer RA returns nothing
local integer vr=(LoadInteger(r7,nh[(m)],(9)))
if vr==0 then
set vr=jt()
call iA(vr,m,mh[RA],ih[RA],9)
call SA(vr)
elseif Nh[vr]!=mh[RA]then
set Nh[vr]=mh[RA]
endif
call rA(vr)
endfunction
function UA takes integer vr,real VA returns nothing
set Sh[vr]=iz(Mh[vr],vr,VA)
endfunction
function WA takes integer RA,real VA returns nothing
local integer vr=(LoadInteger(r7,nh[(RA)],(W4)))
if vr==0 then
set vr=it()
call iA(vr,RA,mh[RA],ih[RA],W4)
call UA(vr,VA)
elseif Nh[vr]!=mh[RA]then
set Nh[vr]=mh[RA]
endif
call rA(vr)
endfunction
function XA takes integer vr,boolean up returns nothing
set Sh[vr]=pz(Mh[vr],vr,true)
if up then
set ti[vr]=true
set Th[vr]=mz(Mh[vr],vr,true)
endif
call AddUnitAnimationProperties(mh[Mh[vr]],"Defend",true)
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\Human\\Defend\\DefendCaster.mdx"),(mh[Mh[vr]]),("origin")))
endfunction
function YA takes integer RA returns nothing
local integer vr=(LoadInteger(r7,nh[(RA)],(7)))
if vr==0 then
set vr=ht()
call iA(vr,RA,mh[RA],ih[RA],7)
call XA(vr,Ao[ao[vh[RA]]+1])
elseif Nh[vr]!=mh[RA]then
set Nh[vr]=mh[RA]
endif
call rA(vr)
endfunction
function ZA takes integer vr,integer Ly,real da returns nothing
set Sh[vr]=oz(Mh[vr],vr,Ly)
set Th[vr]=jz(Mh[vr],vr,da)
endfunction
function ea takes integer m,integer RA,integer Ly,real da returns nothing
local integer vr=(LoadInteger(r7,nh[(m)],(V4)))
if vr==0 then
set vr=gt()
call iA(vr,m,mh[RA],ih[RA],V4)
call ZA(vr,Ly,da)
elseif Nh[vr]!=mh[RA]then
set Nh[vr]=mh[RA]
endif
call rA(vr)
endfunction
function fa takes integer vr returns nothing
set Sh[vr]=(uw(oh[(Mh[vr])],(vr)))
endfunction
function ga takes integer m,integer RA returns nothing
local integer vr=(LoadInteger(r7,nh[(m)],(X4)))
if vr==0 then
set vr=ft()
call iA(vr,m,mh[RA],ih[RA],X4)
call fa(vr)
elseif Nh[vr]!=mh[RA]then
set Nh[vr]=mh[RA]
endif
call sA(vr,Ai[Ph[vr]])
endfunction
function ha takes integer vr,boolean ia,real da returns nothing
set Sh[vr]=jz(Mh[vr],vr,da)
if ia then
set ui[vr]=true
set Th[vr]=mz(Mh[vr],vr,true)
endif
endfunction
function ja takes integer RA,real da returns nothing
local integer vr=(LoadInteger(r7,nh[(RA)],(d7)))
if vr==0 then
set vr=et()
call iA(vr,RA,mh[RA],ih[RA],d7)
call ha(vr,by(RA)<=.35,da)
elseif Nh[vr]!=mh[RA]then
set Nh[vr]=mh[RA]
endif
call rA(vr)
endfunction
function ka takes integer vr returns nothing
set Sh[vr]=rz(Mh[vr],vr,true)
endfunction
function ma takes integer RA returns nothing
local integer vr=(LoadInteger(r7,nh[(RA)],(Y4)))
if vr==0 then
set vr=tt()
call iA(vr,RA,mh[RA],ih[RA],Y4)
call ka(vr)
elseif Nh[vr]!=mh[RA]then
set Nh[vr]=mh[RA]
endif
call rA(vr)
endfunction
function na takes integer vr returns nothing
set Sh[vr]=rz(Mh[vr],vr,true)
endfunction
function oa takes integer m,integer RA returns nothing
local integer vr=(LoadInteger(r7,nh[(m)],(16)))
if vr==0 then
set vr=ut()
call iA(vr,m,mh[RA],ih[RA],16)
call na(vr)
elseif Nh[vr]!=mh[RA]then
set Nh[vr]=mh[RA]
endif
call rA(vr)
endfunction
function pa takes integer vr returns nothing
set Sh[vr]=(uw(oh[(Mh[vr])],(vr)))
endfunction
function qa takes integer m,integer RA returns nothing
local integer vr=(LoadInteger(r7,nh[(m)],(17)))
if vr==0 then
set vr=vt()
call iA(vr,m,mh[RA],ih[RA],17)
call pa(vr)
elseif Nh[vr]!=mh[RA]then
set Nh[vr]=mh[RA]
endif
call sA(vr,Ai[Ph[vr]])
endfunction
function ra takes integer T,boolean sa,integer ta,integer ua,real va,integer wa returns nothing
set U4[T]=Hs()
set yi[U4[T]]=sa
set zi[U4[T]]=ua>1
set Ai[U4[T]]=ta
set Bi[U4[T]]=ua
set bi[U4[T]]=va
set Ci[U4[T]]=va/ ta
set ci[U4[T]]=wa
endfunction
function ya takes integer a,unit m,player p,integer pw,integer wA returns integer
local integer vr=Gs()
set Gi[vr]=CreateUnit(p,1685417325,GetUnitX(m),GetUnitY(m),.0)
call SetUnitPathing(Gi[vr],false)
call UnitAddAbility(Gi[vr],pw)
call UnitAddType(Gi[vr],i4)
call SetUnitUserData(Gi[vr],a)
set Hi[vr]=wA
call ju("MinionDummy "+I2S(vr)+" is created.")
return vr
endfunction
function za takes integer vr,real x,real y returns nothing
call SetUnitX(Gi[vr],x)
call SetUnitY(Gi[vr],y)
endfunction
function Aa takes integer vr,real x,real y returns boolean
return IssuePointOrderById(Gi[vr],Hi[vr],x,y)
endfunction
function aa takes integer vr returns nothing
call RemoveUnit(Gi[vr])
set Gi[vr]=null
call ju("MinionDummy "+I2S(vr)+" is deleted.")
endfunction
function Ba takes integer vr returns nothing
if vr==null then
return
elseif(Fi[vr]!=-1)then
return
endif
call aa(vr)
set Fi[vr]=Di
set Di=vr
endfunction
function ba takes integer vr returns nothing
set Pi[vr]=CreateTimer()
call SaveInteger(r7,W,GetHandleId((Pi[vr])),(vr))
endfunction
function Ca takes integer vr returns nothing
call Gu(Pi[vr])
set Pi[vr]=null
endfunction
function ca takes nothing returns nothing
local integer vr=(LoadInteger(r7,W,GetHandleId(GetExpiredTimer())))
call SetImagePosition(Oi[vr],GetUnitX(Ni[vr]),GetUnitY(Ni[vr]),.0)
endfunction
function Da takes unit u,real lu returns integer
local integer vr=Fs()
set Ni[vr]=u
set Oi[vr]=Iu(lu,GetUnitX(u),GetUnitY(u),3,Li,51,51,Mi)
call ba(vr)
call TimerStart(Pi[vr],Ki,true,function ca)
return vr
endfunction
function Ea takes integer vr returns nothing
call Ca(vr)
call DestroyImage(Oi[vr])
set Oi[vr]=null
set Ni[vr]=null
endfunction
function Fa takes integer vr returns nothing
if vr==null then
return
elseif(Ji[vr]!=-1)then
return
endif
call Ea(vr)
set Ji[vr]=Ii
set Ii=vr
endfunction
function Ga takes integer vr returns nothing
if Xi[vr]==null then
set Xi[vr]=CreateTimer()
call SaveInteger(r7,W,GetHandleId((Xi[vr])),(vr))
endif
endfunction
function Ha takes integer vr returns nothing
if Xi[vr]!=null then
call Gu(Xi[vr])
set Xi[vr]=null
endif
endfunction
function Ia takes nothing returns nothing
local integer vr=(LoadInteger(r7,W,GetHandleId(GetExpiredTimer())))
set Wi[vr]=not Fk[Vi[vr]]
call xs(vr)
endfunction
function la takes integer vr returns nothing
call Ga(vr)
if Gk[Vi[vr]]>.0 then
call TimerStart(Xi[vr],Gk[Vi[vr]],Fk[Vi[vr]],function Ia)
else
set Wi[vr]=true
endif
endfunction
function Ja takes integer vr,integer m,integer T returns nothing
set ph[m]=vr
set Ui[vr]=m
set Vi[vr]=Gg[Hg[ih[m]]+T]
endfunction
function Ka takes integer vr returns nothing
set Wi[vr]=false
call TimerStart(Xi[vr],Hk[Vi[vr]],Fk[Vi[vr]],function Ia)
endfunction
function La takes integer vr,integer zs,integer as,integer Bs returns boolean
if Wi[vr]and not((ef[oh[(Ui[vr])]]))and IsUnitInRange(mh[zs],mh[Ui[vr]],Ik[Vi[vr]])then
return As(vr,zs,as,Bs)
endif
return false
endfunction
function Ma takes nothing returns nothing
local integer m=Ti
local integer vr=wt()
call Ja(vr,m,4)
call la(vr)
call ju("AbilArcaneShield "+I2S(vr)+" is created.")
endfunction
function Na takes nothing returns nothing
local integer m=Ti
local integer vr=xt()
call Ja(vr,m,1)
call ju("AbilFlexibility "+I2S(vr)+" is created.")
endfunction
function Oa takes integer vr returns nothing
set Zi[vr]=not Zi[vr]
endfunction
function Pa takes nothing returns nothing
local integer m=Ti
local integer vr=ph[m]
if Zi[vr]then
set ej[vr]=.1
set dj[vr]=0
else
set ej[vr]=.0
set dj[vr]=Yi
endif
endfunction
function Qa takes nothing returns nothing
local integer m=Ti
local integer vr=yt()
call Ja(vr,m,2)
call la(vr)
call ju("AbilShieldsUp "+I2S(vr)+" is created.")
endfunction
function Ra takes nothing returns nothing
local integer m=Ti
local integer vr=zt()
call Ja(vr,m,5)
call la(vr)
set hj[vr]=Da(mh[m],Ik[Vi[vr]])
call ju("AbilInspiration "+I2S(vr)+" is created.")
endfunction
function Sa takes nothing returns nothing
local integer m=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),h4))and IsUnitType(mh[m],UNIT_TYPE_GROUND)and qh[m]and ih[m]==ih[jj]and fo[vh[m]]then
if hh[m]!=3 then
call ea(m,jj,fj,.05)
elseif not kj then
set kj=m!=jj
endif
endif
endfunction
function Ta takes nothing returns nothing
set gj=Filter(function Sa)
endfunction
function Ua takes nothing returns nothing
local integer m=Ti
local integer vr=At()
call Ja(vr,m,3)
call ju("AbilCommander "+I2S(vr)+" is created.")
endfunction
function Va takes integer vr,integer id returns nothing
if id==ck[Vi[vr]]then
set qj[vr]=true
call UnitRemoveAbility(mh[Ui[vr]],id)
call UnitAddAbility(mh[Ui[vr]],1093677143)
call AddUnitAnimationProperties(mh[Ui[vr]],"Defend",true)
elseif id==1093677143 then
set qj[vr]=false
call UnitRemoveAbility(mh[Ui[vr]],id)
call UnitAddAbility(mh[Ui[vr]],ck[Vi[vr]])
call AddUnitAnimationProperties(mh[Ui[vr]],"Defend",false)
endif
endfunction
function Wa takes nothing returns nothing
local integer m=Ti
local integer vr=ph[m]
if qj[vr]then
set rj[vr]=mj
if Ao[ao[vh[m]]+0]then
set sj[vr]=25
endif
else
call la(vr)
set uj[vr]=Da(mh[m],Ik[Vi[vr]])
endif
endfunction
function Xa takes nothing returns nothing
local integer m=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),h4))and IsUnitType(mh[m],UNIT_TYPE_GROUND)and qh[m]and ih[m]==vj and fo[vh[m]]and hh[m]!=5 then
set wj=wj+1
endif
endfunction
function Ya takes nothing returns nothing
local integer i=0
loop
set nj[i]=3
set oj[i]=30
set i=i+1
exitwhen i==Y
endloop
set pj=Filter(function Xa)
endfunction
function Za takes nothing returns nothing
local integer i=0
loop
set yj[i]=.25
set i=i+1
exitwhen i==Y
endloop
endfunction
function dB takes nothing returns nothing
local integer m=Ti
local integer vr=at()
call Ja(vr,m,6)
call la(vr)
set zj[vr]=ya(vr,mh[m],Jf[jh[m]],1093677368,xj)
call ju("AbilHealingWave "+I2S(vr)+" is created.")
endfunction
function eB takes nothing returns nothing
local integer i=Kf[cn]
set yj[i]=yj[i]+.1
endfunction
function fB takes nothing returns nothing
local integer m=Ti
local integer vr=Bt()
call Ja(vr,m,7)
call la(vr)
call ju("AbilDivineShield "+I2S(vr)+" is created.")
endfunction
function gB takes nothing returns nothing
local integer m=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),h4))and qh[m]and ih[m]==aj and fo[vh[m]]and not yz(m)and by(m)<bj then
set bj=by(m)
set Bj=m
endif
endfunction
function hB takes integer vr returns integer
set aj=ih[Ui[vr]]
set Bj=0
set bj=2.
call GroupEnumUnitsInRange(bj_lastCreatedGroup,GetUnitX(mh[Ui[vr]]),GetUnitY(mh[Ui[vr]]),512.,Aj)
return Bj
endfunction
function iB takes nothing returns nothing
set Aj=Filter(function gB)
endfunction
function jB takes integer vr returns nothing
set Ej[vr]=CreateTimer()
call SaveInteger(r7,W,GetHandleId((Ej[vr])),(vr))
endfunction
function kB takes integer vr returns nothing
call Gu(Ej[vr])
set Ej[vr]=null
endfunction
function mB takes nothing returns nothing
local integer m=Ti
local integer vr=bt()
call Ja(vr,m,8)
call la(vr)
call jB(vr)
call ju("AbilResurrection "+I2S(vr)+" is created.")
endfunction
function nB takes nothing returns nothing
local integer m=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),h4))and ay(m)and ih[m]==Fj and lz(m)and jo[vh[m]]>Hj then
set Hj=jo[vh[m]]
set Gj=m
endif
endfunction
function oB takes integer vr returns nothing
local integer pB=Ui[vr]
set Fj=ih[pB]
set Gj=0
set Hj=.0
call GroupEnumUnitsInRange(bj_lastCreatedGroup,GetUnitX(mh[pB]),GetUnitY(mh[pB]),Ik[Vi[vr]],cj)
set pB=Gj
if pB!=0 then
call Lz(pB,Cj[Kf[ih[Ui[vr]]]])
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\Human\\Resurrect\\ResurrectTarget.mdx"),(mh[pB]),("origin")))
if(Q9[R9[(Kg[Lg[ih[pB]]+3])]+(1)])then
call qa(pB,Ui[vr])
endif
call Ka(vr)
else
call TimerStart(Ej[vr],.25,false,Dj)
endif
endfunction
function qB takes nothing returns nothing
call oB(((LoadInteger(r7,W,GetHandleId(GetExpiredTimer())))))
endfunction
function rB takes nothing returns nothing
local integer i=0
loop
set Cj[i]=.5
set i=i+1
exitwhen i==Y
endloop
set cj=Filter(function nB)
set Dj=function qB
endfunction
function sB takes nothing returns nothing
local integer i=Kf[cn]
set Cj[i]=.75
endfunction
function tB takes integer vr returns nothing
set fk[vr]=CreateTimer()
call SaveInteger(r7,W,GetHandleId((fk[vr])),(vr))
endfunction
function uB takes integer vr returns nothing
call Gu(fk[vr])
set fk[vr]=null
endfunction
function vB takes integer f,real x,real y,boolean wB returns nothing
local integer c=hw(f,x,y)
set wB=not wB
set Kd[(c)]=(wB)
set Kd[(Md[c])]=(wB)
set Kd[(Nd[Md[c]])]=(wB)
set Kd[(Nd[c])]=(wB)
endfunction
function xB takes unit u,integer p,real x,real y,integer T returns nothing
set Kj=u
set Lj=p
set Mj=x
set Nj=y
call TriggerEvaluate(bp[(T)])
endfunction
function yB takes unit u returns nothing
local real x=GetUnitX(u)
local real y=GetUnitY(u)
local integer p=(B4[GetPlayerId((GetOwningPlayer(u)))])
local integer f=Sf[p]
local integer T=GetUnitTypeId(u)-1949315120
if x>ie[f]or x<je[f]or y>320. or y<o4 then
call Hx((p),u8,od[hd]+"Error|r: "+("it is not allowed to build here."))
call RemoveUnit(u)
return
endif
if pu(R2I(x-je[f]),$80)or pu(R2I(y-o4),$80)then
call Hx((p),u8,od[hd]+"Error|r: "+("towers must be build precisely on the grid."))
call RemoveUnit(u)
return
endif
if(Vf[(p)]-(rp[(T)])>=0)then
call vB(f,x,y,true)
call iw(f)
if ue[f]then
call Mx((p),-(rp[(T)]))
call xB(u,p,x,y,T)
else
call vB(f,x,y,false)
call Hx((p),u8,od[hd]+"Error|r: "+("this tower will hinder minions' movement."))
call RemoveUnit(u)
endif
else
call Hx((p),v8,od[hd]+"Error|r: not enough Gold.")
call RemoveUnit(u)
endif
endfunction
function zB takes integer vr,integer T returns nothing
set Qj[vr]=T
set Pj[vr]=Kj
set Oj[vr]=Lj
set Vj[vr]=Mj
set Wj[vr]=Nj
set Rj[vr]=(T)
set Xj[vr]=rp[Rj[vr]]
set dk[vr]=(T)
call UnitAddAbility(Pj[vr],np[Rj[vr]])
call UnitAddAbility(Pj[vr],op[Rj[vr]])
call fv(yg[Oj[vr]],vr)
call SetUnitUserData(Pj[vr],vr)
endfunction
function AB takes integer vr,real lu,integer Ju,integer Ku,integer Lu,boolean aB returns image
call Nu(Jf[Oj[vr]],lu,Vj[vr],Wj[vr],2,Ju,Ku,Lu,f7)
call ShowImage(bj_lastCreatedImage,aB and IsUnitSelected(Pj[vr],Jf[Oj[vr]]))
return bj_lastCreatedImage
endfunction
function BB takes integer vr returns nothing
set Zj[vr]=gp[dk[vr]]
set ok[vr]=xv(Pj[vr],dp[dk[vr]])
set pk[vr]=xv(Pj[vr],1227894833)
call tB(vr)
set ek[vr]=tA(vr)
set gk[vr]=AB(vr,fp[dk[vr]],51,$CC,51,true)
endfunction
function bB takes integer vr returns nothing
call uB(vr)
call BA(ek[vr])
call DestroyImage(gk[vr])
set gk[vr]=null
set ok[vr]=null
set pk[vr]=null
endfunction
function CB takes integer vr returns real
return .0
endfunction
function cB takes integer vr returns real
return .0
endfunction
function DB takes integer vr returns real
local real Bw=es(vr)+fs(vr)
return Bw+Bw*gs(vr)
endfunction
function EB takes integer vr returns nothing
endfunction
function FB takes integer vr,integer zs,real Rs,integer Bs,boolean GB returns boolean
return Cz(zs,vr,Rs,Bs,GB)
endfunction
function HB takes integer vr returns nothing
call SetItemCharges(ok[vr],ru(DB(vr)))
endfunction
function IB takes integer vr,integer m returns boolean
local real r=DB(vr)
call SetItemCharges(ok[vr],ru(r))
return FB(vr,m,r,dp[dk[vr]],hp[dk[vr]])
endfunction
function lB takes integer vr,integer m returns boolean
return FB(vr,m,DB(vr),dp[dk[vr]],hp[dk[vr]])
endfunction
function JB takes integer vr returns nothing
call TimerStart(fk[vr],Zo[dk[vr]],false,uk)
endfunction
function KB takes integer vr,integer ks,integer as returns boolean
local integer LB=0
local integer m
loop
set m=EA(ks)
call iu(1,m,vr,dp[dk[vr]])
if(Gf[oh[(m)]])then
exitwhen(ri[(ks)]==0)
else
call AA(as,mh[m])
set LB=LB+1
if(ri[(ks)]==0)or LB==Zj[vr]then
return true
endif
endif
endloop
return false
endfunction
function MB takes integer vr returns nothing
local integer ks=JA(vr)
if ks!=0 then
call js(vr,ks)
call IA(ks)
call nt(ks)
else
call TimerStart(fk[(vr)],S4,false,uk)
endif
endfunction
function NB takes nothing returns nothing
call MB(((LoadInteger(r7,W,GetHandleId(GetExpiredTimer())))))
endfunction
function OB takes integer vr returns nothing
set Tj[vr]=kp[Rj[vr]]*mp[Rj[vr]]
set Uj[vr]=mp[Rj[vr]]
call SetUnitScale(Pj[vr],Uj[vr],Uj[vr],Uj[vr])
if pp[Rj[vr]]then
call BB(vr)
endif
set ik[vr]=vp[Rj[vr]]
if ik[vr]then
set nk[vr]=Ap[Rj[vr]]
set mk[vr]=Ap[Rj[vr]]
set qk[vr]=xv(Pj[vr],1227894839)
if wp[Rj[vr]]then
set rk[vr]=xv(Pj[vr],1227894838)
else
set rk[vr]=xv(Pj[vr],1227894842)
call UnitAddAbility(Pj[vr],1093677129)
endif
call SetItemCharges(rk[vr],nk[vr])
endif
if sp[Rj[vr]]then
set Yj[vr]=.5
call UnitAddAbility(Pj[vr],1093677119)
else
set Yj[vr]=.0
call UnitAddAbility(Pj[vr],1093677121)
endif
set Lk=vr
call TriggerEvaluate(rn[(qp[Rj[vr]])])
set Sj[vr]=true
if pp[Rj[vr]]then
call HB(vr)
call MB(vr)
endif
endfunction
function PB takes integer vr returns nothing
call gv(yg[Oj[vr]],vr)
if pp[Rj[vr]]and Sj[vr]then
call bB(vr)
endif
if sk[vr]!=0 then
call Hr(sk[vr])
endif
set qk[vr]=null
set rk[vr]=null
call SetUnitUserData(Pj[vr],0)
set Pj[vr]=null
call qs(vr)
call ju("Tower "+I2S(vr)+" is deleted.")
endfunction
function QB takes integer vr returns nothing
call RemoveUnit(Pj[vr])
call PB(vr)
endfunction
function RB takes integer vr returns nothing
call DestroyEffect(AddSpecialEffect(("Objects\\Spawnmodels\\Human\\HCancelDeath\\HCancelDeath.mdx"),((Vj[vr])*1.),((Wj[vr])*1.)))
if Yj[vr]>.0 then
set Xj[vr]=ru(Xj[vr]*Yj[vr])
call Mx(Oj[vr],Xj[vr])
call Qx(Oj[vr],I2S(Xj[vr]),Pj[vr])
endif
call vB(Sf[Oj[vr]],Vj[vr],Wj[vr],false)
call iw(Sf[Oj[vr]])
call QB(vr)
endfunction
function SB takes integer vr returns nothing
local boolean qy=not wp[Rj[vr]]
set kk[vr]=kk[vr]+1
call SetItemCharges(qk[vr],kk[vr])
call Fr(sk[vr])
if pp[Rj[vr]]then
call hs(vr)
call HB(vr)
endif
if qy then
set Xj[vr]=Xj[vr]+nk[vr]
endif
if kk[vr]<Bp[Rj[vr]]then
if kk[vr]<ap[Rj[vr]]then
set nk[vr]=xp[Rj[vr]]*nk[vr]+(kk[vr]+1)*yp[Rj[vr]]+zp[Rj[vr]]
endif
if qy then
call SetItemCharges(rk[vr],nk[vr])
else
set mk[vr]=mk[vr]+nk[vr]
endif
else
if qy then
call UnitRemoveAbility(Pj[vr],1093677129)
endif
call Av(Pj[vr],rk[vr])
set ik[vr]=false
set nk[vr]=0
set mk[vr]=0
endif
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\Items\\AIlm\\AIlmTarget.mdx"),(Pj[vr]),("origin")))
endfunction
function TB takes integer vr,integer Rs returns nothing
set jk[vr]=jk[vr]+Rs
loop
exitwhen jk[vr]<mk[vr]or(not ik[vr])
call SB(vr)
endloop
if ik[vr]then
call SetItemCharges(rk[vr],mk[vr]-jk[vr])
endif
endfunction
function UB takes nothing returns nothing
local integer t=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),f4))and Sj[t]and wp[Rj[t]]and ik[t]then
call TB(t,wk)
endif
endfunction
function VB takes integer m,integer cz returns nothing
local real x=GetUnitX(mh[m])
local real y=GetUnitY(mh[m])
set wk=qo[vh[m]]
if wp[Rj[cz]]and ik[cz]and not IsUnitInRangeXY(Pj[cz],x,y,384.)then
call TB(cz,wk)
endif
call GroupEnumUnitsInRange(bj_lastCreatedGroup,x,y,384.,vk)
endfunction
function WB takes integer vr,integer m returns nothing
set hk[vr]=hk[vr]+1
call SetItemCharges(pk[vr],hk[vr])
call Jx(Oj[vr])
if qo[vh[m]]>0 then
call VB(m,vr)
endif
endfunction
function XB takes nothing returns boolean
local integer t=GetUnitUserData(GetFilterUnit())
return((IsUnitType(GetFilterUnit(),f4))and Sj[t]and not wp[Rj[t]]and ik[t]and(Vf[(Oj[t])]-(nk[t])>=0))!=null
endfunction
function YB takes integer vr returns nothing
local integer array ZB
local integer db
local integer eb
local integer n=-1
local integer i=-1
local group g=CreateGroup()
local unit u
call GroupEnumUnitsSelected(g,Jf[Oj[vr]],xk)
loop
set u=FirstOfGroup(g)
exitwhen u==null
call GroupRemoveUnit(g,u)
set db=GetUnitUserData(u)
set n=n+1
set ZB[n]=db
loop
exitwhen i<0 or nk[ZB[i]]<=nk[db]
set eb=ZB[i]
set ZB[i]=db
set ZB[i+1]=eb
set i=i-1
endloop
set i=n
endloop
call DestroyGroup(g)
set g=null
if n>-1 then
loop
set db=ZB[n]
exitwhen not Nx(Oj[vr],nk[db])
call SB(db)
set n=n-1
exitwhen n==-1
endloop
else
call Hx((Oj[vr]),v8,od[hd]+"Error|r: not enough Gold.")
endif
endfunction
function fb takes nothing returns nothing
set uk=function NB
set vk=Filter(function UB)
set xk=Filter(function XB)
endfunction
function gb takes integer vr returns nothing
set Sh[vr]=(uw(oh[(Mh[vr])],(vr)))
endfunction
function hb takes integer m,integer RA returns nothing
local integer vr
if(Gf[oh[(m)]])then
return
endif
set vr=(LoadInteger(r7,nh[(m)],(1)))
if vr==0 then
set vr=Ct()
call iA(vr,m,Pj[RA],Oj[RA],1)
call gb(vr)
elseif Nh[vr]!=Pj[RA]then
set Nh[vr]=Pj[RA]
endif
call sA(vr,Ai[Ph[vr]])
endfunction
function ib takes integer vr,real jb returns nothing
set Sh[vr]=jz(Mh[vr],vr,jb)
endfunction
function kb takes integer m,integer RA,real jb returns nothing
local integer vr
if Ls(m)or(Gf[oh[(m)]])then
return
endif
set vr=(LoadInteger(r7,nh[(m)],(2)))
if vr==0 then
set vr=ct()
call iA(vr,m,Pj[RA],Oj[RA],2)
call ib(vr,jb)
elseif Nh[vr]!=Pj[RA]then
set Nh[vr]=Pj[RA]
endif
call rA(vr)
endfunction
function mb takes integer vr,integer Ly returns nothing
set Sh[vr]=oz(Mh[vr],vr,Ly)
endfunction
function nb takes integer m,integer RA,integer Ly returns nothing
local integer vr
if(Gf[oh[(m)]])then
return
endif
set vr=(LoadInteger(r7,nh[(m)],(3)))
if vr==0 then
set vr=Dt()
call iA(vr,m,Pj[RA],Oj[RA],3)
call mb(vr,Ly)
elseif Nh[vr]!=Pj[RA]then
set Nh[vr]=Pj[RA]
endif
call rA(vr)
endfunction
function ob takes integer vr,real jb returns nothing
set Sh[vr]=jz(Mh[vr],vr,jb)
endfunction
function pb takes integer m,integer RA,real jb returns nothing
local integer vr
if Ls(m)or(Gf[oh[(m)]])then
return
endif
set vr=(LoadInteger(r7,nh[(m)],(4)))
if vr==0 then
set vr=Et()
call iA(vr,m,Pj[RA],Oj[RA],4)
call ob(vr,jb)
elseif Nh[vr]!=Pj[RA]then
set Nh[vr]=Pj[RA]
endif
call rA(vr)
endfunction
function sb takes integer vr,integer Ly returns nothing
set Sh[vr]=oz(Mh[vr],vr,Ly)
endfunction
function tb takes integer m,integer RA,integer Ly returns nothing
local integer vr=(LoadInteger(r7,nh[(m)],(6)))
if vr==0 then
set vr=Gt()
call iA(vr,m,Pj[RA],Oj[RA],6)
call sb(vr,Ly)
elseif Nh[vr]!=Pj[RA]then
set Nh[vr]=Pj[RA]
endif
call rA(vr)
endfunction
function ub takes integer vr,real VA returns nothing
set Sh[vr]=iz(Mh[vr],vr,VA)
endfunction
function vb takes integer m,integer RA,real VA returns nothing
local integer vr=(LoadInteger(r7,nh[(m)],(Z4)))
if vr==0 then
set vr=Ht()
call iA(vr,m,Pj[RA],Oj[RA],Z4)
call ub(vr,VA)
elseif Nh[vr]!=Pj[RA]then
set Nh[vr]=Pj[RA]
endif
call rA(vr)
endfunction
function wb takes nothing returns nothing
local integer vr=It()
call ty(vr,4,fh)
call ju("Wizard "+I2S(vr)+" is created.")
endfunction
function xb takes nothing returns nothing
local integer d=Ig[lg[gh]+9]
set bi[d]=bi[d]+.5
set Ci[d]=bi[d]
endfunction
function yb takes integer m,integer p returns integer
local integer vr=lt()
set ih[vr]=p
set mh[vr]=CreateUnit(Jf[p],Xn[k7[1]],Cu(ke[Sf[p]]),cu(ke[Sf[p]]),270.)
call UnitAddAbility(mh[vr],1098282348)
call UnitMakeAbilityPermanent(mh[vr],true,1098282348)
call SetUnitUserData(mh[vr],vr)
set hh[vr]=1
set nh[vr]=GetHandleId(mh[vr])
set vh[vr]=cg[Dg[p]+1]
call UnitAddAbility(mh[vr],1093677099)
call UnitMakeAbilityPermanent(mh[vr],true,1093677099)
call UnitAddAbility(mh[vr],Zn[k7[1]])
call UnitMakeAbilityPermanent(mh[vr],true,Zn[k7[1]])
call UnitAddAbility(mh[vr],Go[Ho[n7[1]]])
call UnitMakeAbilityPermanent(mh[vr],true,Go[Ho[n7[1]]])
call UnitAddAbility(mh[vr],Go[Ho[n7[1]]+1])
call UnitMakeAbilityPermanent(mh[vr],true,Go[Ho[n7[1]]+1])
call UnitAddAbility(mh[vr],Go[Ho[n7[1]]+2])
call UnitMakeAbilityPermanent(mh[vr],true,Go[Ho[n7[1]]+2])
set Ti=vr
call TriggerEvaluate(h7[ho[vh[vr]]])
call UnitAddAbility(mh[vr],1093677122)
call UnitMakeAbilityPermanent(mh[vr],true,1093677122)
call UnitAddAbility(mh[vr],ck[g7[ho[vh[vr]]]])
call UnitMakeAbilityPermanent(mh[vr],true,ck[g7[ho[vh[vr]]]])
call hv(zg[p],vr)
set zk[vr]=m
call ju("ConscriptTwin "+I2S(vr)+" is created.")
return vr
endfunction
function zb takes nothing returns nothing
local integer vr=lt()
call ty(vr,1,fh)
if Ao[ao[vh[vr]]+2]then
set zk[vr]=yb(vr,ih[vr])
else
set zk[vr]=0
endif
call ju("Conscript "+I2S(vr)+" is created.")
endfunction
function Ab takes integer vr returns nothing
call iv(zg[ih[vr]],vr)
call Vx(ih[vr],jo[vh[vr]])
call DestroyEffect(AddSpecialEffect(("Abilities\\Spells\\Other\\Transmute\\PileofGold.mdx"),((GetUnitX(mh[vr]))*1.),((GetUnitY(mh[vr]))*1.)))
call RemoveUnit(mh[vr])
call Xs(vr)
call ju("ConscriptTwin "+I2S(vr)+" is sold.")
endfunction
function ab takes nothing returns nothing
call dA(gh,1,.1)
endfunction
function Bb takes nothing returns nothing
call fA(gh,1,yk)
endfunction
function bb takes nothing returns nothing
local integer Cb=gh
local integer cb=s9[zg[Cb]]
local real Db=jo[cg[Dg[Cb]+1]]-.5
local integer a=0
local integer m
set jo[cg[Dg[Cb]+1]]=.5
loop
exitwhen a>cb
set m=(q9[r9[(zg[Cb])]+(a)])
if hh[m]==1 then
set zk[m]=yb(m,Cb)
endif
set a=a+1
endloop
set a=0
set cb=M9[Bg[Cb]]
loop
exitwhen a>cb
set m=(K9[L9[(Bg[Cb])]+(a)])
if hh[m]==1 and qh[m]then
call Vx(Cb,Db)
endif
set a=a+1
endloop
set a=0
set cb=y9[Ag[Nf[Cb]]]
loop
exitwhen a>cb
set m=(w9[x9[(Ag[Nf[Cb]])]+(a)])
if hh[m]==1 then
call Vx(Cb,Db)
endif
set a=a+1
endloop
endfunction
function Eb takes nothing returns nothing
local integer vr=Jt()
call ty(vr,2,fh)
call ju("Footman "+I2S(vr)+" is created.")
endfunction
function Fb takes nothing returns nothing
call fA(gh,2,30)
endfunction
function Gb takes nothing returns nothing
local integer vr=Kt()
call ty(vr,3,fh)
call ju("Knight "+I2S(vr)+" is created.")
endfunction
function Hb takes nothing returns nothing
call Xz(gh,3,.5*to[cg[Dg[gh]+3]])
endfunction
function Ib takes nothing returns nothing
call eA(gh,3,ak)
endfunction
function lb takes nothing returns nothing
local integer d=Ig[lg[gh]+V4]
set bi[d]=1.
set Ci[d]=1.
endfunction
function Jb takes nothing returns nothing
local integer vr=Lt()
call ty(vr,5,fh)
call ju("Captain "+I2S(vr)+" is created.")
endfunction
function Kb takes nothing returns nothing
local integer Cb=gh
local integer a=0
local integer m
local integer b
loop
exitwhen a>M9[Bg[Cb]]
set m=(K9[L9[(Bg[Cb])]+(a)])
set b=ph[m]
if hh[m]==5 and qj[b]then
set sj[b]=25
if qh[m]then
call Uy(m)
endif
endif
set a=a+1
endloop
endfunction
function Lb takes nothing returns nothing
local integer i=Kf[gh]
set nj[i]=5
set oj[i]=50
endfunction
function Mb takes nothing returns nothing
local integer vr=Mt()
call uy(vr,6,fh)
call ju("Hero Paladin "+I2S(vr)+" is created.")
endfunction
function Nb takes nothing returns nothing
call Xz(gh,6,1.*to[cg[Dg[gh]+6]])
endfunction
function Ob takes nothing returns nothing
call eA(gh,6,25)
endfunction
function Pb takes nothing returns nothing
call Zz(gh,6,1.*uo[cg[Dg[gh]+6]])
endfunction
function Qb takes integer T,integer Rb,boolean Sb,boolean Tb,boolean Ub,real Vb,real cd,real lu returns nothing
set g7[T]=Ir()
set ck[g7[T]]=Rb
set Dk[g7[T]]=Sb
set Ek[g7[T]]=Tb
set Fk[g7[T]]=Ub
set Gk[g7[T]]=Vb
if Ub then
set Hk[g7[T]]=Vb
else
set Hk[g7[T]]=cd
endif
set Ik[g7[T]]=lu
endfunction
function Wb takes integer T,code Xb,code Yb returns nothing
if Xb!=null then
set h7[T]=CreateTrigger()
call TriggerAddCondition(h7[T],Filter(Xb))
endif
if Yb!=null then
set i7[T]=CreateTrigger()
call TriggerAddCondition(i7[T],Filter(Yb))
endif
endfunction
function dC takes integer vr returns nothing
set Wk[vr]=CreateTimer()
call SaveInteger(r7,W,GetHandleId((Wk[vr])),(vr))
endfunction
function eC takes integer vr returns nothing
call Gu(Wk[vr])
set Wk[vr]=null
endfunction
function fC takes nothing returns nothing
local integer m=Lk
local integer vr=Nt(m,1)
set Qk[vr]=xv(Pj[m],1227894835)
set Vk[vr]=ek[m]
call dC(vr)
call uA(Vk[(vr)],1093677096,Ok)
call ju("AbilFocusedAttack "+I2S(vr)+" is created.")
endfunction
function gC takes integer vr returns real
if Sk[vr]<=.0 then
return Tk[vr]
endif
return .0
endfunction
function hC takes integer vr,real r returns nothing
set Sk[vr]=Sk[vr]-r
if Sk[vr]<=.0 then
set Sk[vr]=.0
call uA(Vk[(vr)],1093677096,Ok)
call HB(Nk[vr])
call PauseTimer(Wk[vr])
endif
call SetItemCharges(Qk[vr],su(Sk[vr]))
endfunction
function iC takes nothing returns nothing
call hC(((LoadInteger(r7,W,GetHandleId(GetExpiredTimer())))),1.)
endfunction
function jC takes integer vr returns nothing
if Sk[vr]<=.0 then
set Sk[vr]=Rk[vr]
call HB(Nk[vr])
call SetItemCharges(Qk[vr],su(Sk[vr]))
call TimerStart(Wk[vr],1.,true,Pk)
else
call hC(vr,Uk[vr])
endif
endfunction
function kC takes integer vr,integer id returns nothing
if id==1093677096 then
call xA(ek[Nk[vr]])
endif
endfunction
function mC takes nothing returns nothing
set Pk=function iC
endfunction
function nC takes nothing returns nothing
local integer m=Lk
local integer vr=Ot(m,2)
call ju("AbilAcid "+I2S(vr)+" is created.")
endfunction
function oC takes integer vr,integer m returns nothing
local integer Rz=kk[Nk[vr]]
call hb(m,Nk[vr])
if Rz>=on[Mk[vr]]then
call kb(m,Nk[vr],Xk)
endif
if Rz>=pn[Mk[vr]]then
call nb(m,Nk[vr],Yk)
endif
endfunction
function pC takes nothing returns nothing
local integer m=Lk
local integer vr=Pt(m,3)
call ju("AbilLightningAttack "+I2S(vr)+" is created.")
endfunction
function qC takes integer vr,integer ks returns boolean
if KB(Nk[vr],ks,ek[Nk[vr]])then
if kk[Nk[vr]]>=pn[Mk[vr]]and(ri[(ks)]!=0)then
call KB(Nk[vr],ks,em[vr])
endif
return true
endif
return false
endfunction
function rC takes integer vr returns nothing
set nm[vr]=CreateTimer()
call SaveInteger(r7,W,GetHandleId((nm[vr])),(vr))
endfunction
function sC takes integer vr returns nothing
call Gu(nm[vr])
set nm[vr]=null
endfunction
function tC takes nothing returns nothing
local integer m=Lk
local integer vr=Qt(m,4)
set gm[vr]=CreateUnit(Jf[Oj[m]],1932537904,Vj[m],Wj[m],.0)
call SetUnitPathing(gm[vr],false)
call SetUnitFlyHeight(gm[vr],Tj[m],.0)
call SetUnitUserData(gm[vr],m)
call rC(vr)
set mm[vr]=xv(Pj[m],1227894845)
call ju("AbilEnhancedWeapon "+I2S(vr)+" is created.")
endfunction
function uC takes nothing returns nothing
local integer vr=(LoadInteger(r7,W,GetHandleId(GetExpiredTimer())))
set jm[vr]=.0
set hm[vr]=null
call HB(Nk[vr])
endfunction
function vC takes integer vr,integer m returns nothing
if kk[Nk[vr]]>=on[Mk[vr]]and hm[vr]==mh[m]then
set jm[vr]=jm[vr]+.25
if jm[vr]>1. then
set jm[vr]=1.
endif
call TimerStart(nm[vr],3.,false,function uC)
else
set jm[vr]=.0
endif
set hm[vr]=mh[m]
call IB(Nk[vr],m)
endfunction
function wC takes nothing returns nothing
local integer vr=(LoadInteger(r7,W,GetHandleId(GetExpiredTimer())))
if UnitAlive(hm[vr])then
call AA(fm[vr],hm[vr])
set km[vr]=km[vr]+1
if km[vr]==4 then
call PauseTimer(nm[vr])
endif
else
call PauseTimer(nm[vr])
endif
endfunction
function xC takes integer vr,integer m returns nothing
set hm[vr]=mh[m]
call AA(fm[vr],hm[vr])
set km[vr]=1
call TimerStart(nm[vr],.125,true,function wC)
endfunction
function yC takes integer vr,integer m returns nothing
if im[vr]then
call xC(vr,m)
else
call vC(vr,m)
endif
endfunction
function s__AbilEnhancedWeapon_filterToggleModeSelected takes nothing returns nothing
endfunction
function zC takes integer vr returns nothing
set qm[vr]=CreateTimer()
call SaveInteger(r7,W,GetHandleId((qm[vr])),(vr))
endfunction
function AC takes integer vr returns nothing
call Gu(qm[vr])
set qm[vr]=null
endfunction
function aC takes nothing returns nothing
local integer vr=(LoadInteger(r7,W,GetHandleId(GetExpiredTimer())))
call Mx(pm[vr],om[vr])
call Qx(pm[vr],I2S(om[vr]),Pj[Nk[vr]])
endfunction
function BC takes nothing returns nothing
local integer m=Lk
local integer vr=Rt(m,5)
set pm[vr]=Oj[m]
call zC(vr)
call TimerStart(qm[vr],1.,true,function aC)
call ju("AbilGoldMining "+I2S(vr)+" is created.")
endfunction
function bC takes integer vr returns nothing
set Em[vr]=CreateTimer()
call SaveInteger(r7,W,GetHandleId((Em[vr])),(vr))
endfunction
function CC takes integer vr returns nothing
call Gu(Em[vr])
set Em[vr]=null
endfunction
function cC takes nothing returns nothing
local integer m=Lk
local integer vr=St(m,6)
call bC(vr)
set zm[vr]=AB(Nk[vr],384.,rm,sm,tm,false)
set Dm[vr]=Oj[m]
call UnitAddAbility(Pj[Nk[vr]],1093677127)
call TimerStart(Em[vr],5.,true,vm)
call ju("AbilExpGenerator "+I2S(vr)+" is created.")
endfunction
function DC takes integer vr returns nothing
if bm[vr]==null then
set bm[vr]=Du(Jf[Dm[vr]],"Abilities\\Spells\\Other\\Aneu\\AneuCaster.mdx",Bm[vr],"overhead")
endif
endfunction
function EC takes integer vr returns nothing
if bm[vr]!=null then
call DestroyEffect(bm[vr])
set bm[vr]=null
endif
endfunction
function FC takes integer vr,integer Ov returns nothing
if xm[vr]==0 then
call ShowImage(zm[vr],false)
call UnitRemoveAbility(Pj[Nk[vr]],1093677106)
elseif xm[vr]==2 then
call UnitRemoveAbility(Pj[Nk[vr]],1093677111)
else
set Bm[vr]=null
call EC(vr)
call UnitRemoveAbility(Pj[Nk[vr]],1093677138)
endif
if Ov==0 then
call ShowImage(zm[vr],IsUnitSelected(Pj[Nk[vr]],Jf[Dm[vr]]))
call UnitAddAbility(Pj[Nk[vr]],1093677106)
elseif Ov==2 then
call UnitAddAbility(Pj[Nk[vr]],1093677111)
else
set Cm[vr]=xm[vr]
call UnitAddAbility(Pj[Nk[vr]],1093677138)
endif
set xm[vr]=Ov
endfunction
function GC takes integer vr,integer t returns nothing
if Bm[vr]!=Pj[t]then
if xm[vr]==1 then
call DestroyEffect(bm[vr])
else
call FC(vr,1)
endif
set Bm[vr]=Pj[t]
set bm[vr]=Du(Jf[Dm[vr]],"Abilities\\Spells\\Other\\Aneu\\AneuCaster.mdx",Pj[t],"overhead")
endif
endfunction
function HC takes nothing returns nothing
local integer t=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),f4))and Sj[t]and qq[sk[t]]==91 then
call GC(sk[t],Gm)
endif
endfunction
function IC takes integer vr,integer t returns nothing
if not Sj[t]then
call Hx((Dm[vr]),u8,od[hd]+"Error|r: "+("this tower is constructing."))
return
elseif not wp[Rj[t]]then
call Hx((Dm[vr]),u8,od[hd]+"Error|r: "+("this tower does not use experience."))
return
elseif not ik[t]then
call Hx((Dm[vr]),u8,od[hd]+"Error|r: "+("this tower can no longer be upgraded."))
return
endif
set Gm=t
call GroupEnumUnitsSelected(bj_lastCreatedGroup,Jf[Dm[vr]],Fm)
endfunction
function lC takes nothing returns nothing
local integer t=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),f4))and Sj[t]and qq[sk[t]]==91 and xm[(sk[t])]==Im then
call FC((sk[t]),lm)
endif
endfunction
function JC takes integer vr,integer id returns nothing
if id==1093677106 then
set lm=2
elseif id==1093677111 then
set lm=0
elseif id==1093677138 then
set lm=Cm[vr]
else
return
endif
set Im=xm[vr]
call GroupEnumUnitsSelected(bj_lastCreatedGroup,Jf[Dm[vr]],Hm)
endfunction
function KC takes integer vr returns nothing
call Rx(Dm[vr],cm[vr])
call Ux(Dm[vr],I2S(cm[vr]),Pj[Nk[vr]])
endfunction
function LC takes nothing returns nothing
local integer t=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),f4))and Sj[t]and wp[Rj[t]]and ik[t]then
call TB(t,ku(R2I(Km*nk[t]),um))
set Lm=false
endif
endfunction
function MC takes integer vr returns nothing
set Km=ym[vr]
set Lm=true
call GroupEnumUnitsInRange(bj_lastCreatedGroup,Vj[Nk[vr]],Wj[Nk[vr]],384.,Jm)
if Lm then
call FC(vr,2)
call KC(vr)
endif
endfunction
function NC takes integer vr returns nothing
local integer t=GetUnitUserData(Bm[vr])
call TB(t,ku(Am[vr],R2I(am[vr]*nk[t])))
endfunction
function OC takes integer vr returns nothing
if xm[vr]==0 then
call ShowImage(zm[vr],true)
elseif xm[vr]==1 and UnitAlive(Bm[vr])then
call DC(vr)
endif
endfunction
function PC takes integer vr returns nothing
if xm[vr]==0 then
call ShowImage(zm[vr],false)
elseif xm[vr]==1 and UnitAlive(Bm[vr])then
call EC(vr)
endif
endfunction
function QC takes integer vr returns boolean
if UnitAlive(Bm[vr])and ik[(GetUnitUserData(Bm[vr]))]then
return true
endif
call FC(vr,Cm[vr])
return false
endfunction
function RC takes integer vr returns nothing
if xm[vr]==1 and QC(vr)then
call NC(vr)
elseif xm[vr]==0 then
call MC(vr)
else
call KC(vr)
endif
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\NightElf\\MoonWell\\MoonWellCasterArt.mdx"),(Pj[Nk[vr]]),("origin")))
endfunction
function SC takes nothing returns nothing
call RC(((LoadInteger(r7,W,GetHandleId(GetExpiredTimer())))))
endfunction
function TC takes nothing returns nothing
local integer vr=(LoadInteger(r7,W,GetHandleId(GetExpiredTimer())))
call RC(vr)
if kk[Nk[vr]]>=qn[Mk[vr]]then
call TimerStart(Em[vr],3.,true,wm)
endif
endfunction
function UC takes nothing returns nothing
set vm=function TC
set wm=function SC
set Jm=Filter(function LC)
set Fm=Filter(function HC)
set Hm=Filter(function lC)
endfunction
function VC takes nothing returns nothing
local integer m=Lk
local integer vr=Tt(m,7)
call ju("AbilBloodlust "+I2S(vr)+" is created.")
endfunction
function WC takes integer vr returns nothing
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\Undead\\DeathPact\\DeathPactTarget.mdx"),(Pj[Nk[vr]]),("overhead")))
set Nm[vr]=Nm[vr]+1.
endfunction
function XC takes integer vr,integer m returns nothing
set Mm[vr]=(1.-by(m))*1.
if lB(Nk[vr],m)then
set Mm[vr]=.0
else
set Mm[vr]=.0
call HB(Nk[vr])
endif
endfunction
function YC takes integer vr returns nothing
set Tm[vr]=CreateTimer()
call SaveInteger(r7,W,GetHandleId((Tm[vr])),(vr))
endfunction
function ZC takes integer vr returns nothing
call Gu(Tm[vr])
set Tm[vr]=null
endfunction
function dc takes integer vr returns nothing
set Um[vr]=CreateTimer()
call SaveInteger(r7,W,GetHandleId((Um[vr])),(vr))
endfunction
function ec takes integer vr returns nothing
call Gu(Um[vr])
set Um[vr]=null
endfunction
function fc takes nothing returns nothing
local integer m=Lk
local integer vr=Ut(m,8)
call YC(vr)
call dc(vr)
call ju("AbilShockwave "+I2S(vr)+" is created.")
endfunction
function gc takes integer vr returns nothing
if kk[Nk[vr]]>=on[Mk[vr]]then
set Sm[vr]=Sm[vr]+.1
if Sm[vr]>1. then
set Sm[vr]=1.
endif
call TimerStart(Tm[vr],1.5,false,Om)
call HB(Nk[vr])
endif
endfunction
function hc takes integer vr,real cx,real cy returns nothing
if Sm[vr]!=.0 then
set Sm[vr]=.0
call HB(Nk[vr])
endif
call zA(ek[Nk[vr]],cx,cy)
if kk[Nk[vr]]>=pn[Mk[vr]]then
set Qm[vr]=cx
set Rm[vr]=cy
call TimerStart(Um[vr],.4,false,Pm)
endif
endfunction
function ic takes nothing returns nothing
local integer vr=(LoadInteger(r7,W,GetHandleId(GetExpiredTimer())))
call zA(ek[Nk[vr]],Qm[vr],Rm[vr])
endfunction
function jc takes nothing returns nothing
local integer vr=(LoadInteger(r7,W,GetHandleId(GetExpiredTimer())))
set Sm[vr]=.0
call HB(Nk[vr])
endfunction
function kc takes nothing returns nothing
set Om=function jc
set Pm=function ic
endfunction
function mc takes integer vr returns nothing
set mn[vr]=CreateTimer()
call SaveInteger(r7,W,GetHandleId((mn[vr])),(vr))
endfunction
function nc takes integer vr returns nothing
call Gu(mn[vr])
set mn[vr]=null
endfunction
function oc takes integer vr,integer m returns nothing
local integer Rz=kk[Nk[vr]]
set dn[vr]=dn[vr]-30
call SetItemCharges(jn[vr],dn[vr])
call ly(m,en[vr],"Effects\\RestoreHealthGreen.mdx","origin")
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\NightElf\\MoonWell\\MoonWellCasterArt.mdx"),(Pj[Nk[vr]]),("origin")))
if Rz>=on[Mk[vr]]then
call tb(m,Nk[vr],25)
if Rz>=pn[Mk[vr]]then
call vb(m,Nk[vr],.1*th[m])
endif
endif
endfunction
function qc takes integer vr returns nothing
local integer a=0
local real rc=.0
local real Bw=.0
local integer zs=0
local boolean sc=true
local integer tc=Bg[hn[vr]]
loop
exitwhen a>C9[tc]
if qh[(B9[b9[(tc)]+(a)])]then
set Bw=by((B9[b9[(tc)]+(a)]))
if sc or Bw<rc then
set rc=Bw
set zs=(B9[b9[(tc)]+(a)])
set sc=false
endif
endif
set a=a+1
endloop
if zs!=0 and rc<=fn[vr]then
call oc(vr,zs)
endif
endfunction
function uc takes nothing returns nothing
local integer vr=(LoadInteger(r7,W,GetHandleId(GetExpiredTimer())))
local integer Bw=dn[vr]
set kn[vr]=kn[vr]+1
if kn[vr]==3 then
set kn[vr]=0
if dn[vr]<50 and Sx(hn[vr],Xm)then
set dn[vr]=dn[vr]+Xm
if dn[vr]>50 then
set dn[vr]=50
endif
set Bw=dn[vr]-Bw
call SetItemCharges(jn[vr],dn[vr])
call Ux(hn[vr],I2S(Bw),Pj[Nk[vr]])
endif
endif
if dn[vr]>=30 and gn[vr]then
call qc(vr)
endif
endfunction
function vc takes nothing returns nothing
local integer m=Lk
local integer vr=Vt(m,9)
set hn[vr]=Oj[m]
set jn[vr]=xv(Pj[m],1227894843)
call mc(vr)
call TimerStart(mn[vr],1.,true,function uc)
call ju("AbilReplenishLife "+I2S(vr)+" is created.")
endfunction
function wc takes integer vr,integer xc returns nothing
if xc==Vm or xc==Wm then
set gn[vr]=not gn[vr]
endif
endfunction
function yc takes integer T,integer zc,integer b1,integer b2,integer b3,code Xb returns nothing
set nn[(T)]=zc
set on[(T)]=b1
set pn[(T)]=b2
set qn[(T)]=b3
set rn[(T)]=CreateTrigger()
call TriggerAddCondition(rn[(T)],Filter(Xb))
endfunction
function ac takes nothing returns nothing
local integer vr=Wt()
call zB(vr,0)
call UnitAddAbility(Pj[vr],1093677101)
call ju("Wall "+I2S(vr)+" is created.")
endfunction
function Bc takes unit u returns nothing
local integer vr=GetUnitUserData(u)
if Px(Oj[vr],rp[(sn[vr])])then
set Sj[vr]=false
else
call DisableTrigger(i8)
call IssueImmediateOrderById(Pj[vr],S)
call EnableTrigger(i8)
endif
endfunction
function bc takes unit u returns nothing
local integer vr=GetUnitUserData(u)
local integer Bw=rp[(sn[vr])]
set Sj[vr]=true
call Mx(Oj[vr],Bw)
call Qx(Oj[vr],I2S(Bw),Pj[vr])
endfunction
function Cc takes unit u returns nothing
local integer vr=GetUnitUserData(u)
local integer p=Oj[vr]
local real x=Vj[vr]
local real y=Wj[vr]
local integer T=sn[vr]
call Mx(Oj[vr],Xj[vr])
call Qx(Oj[vr],I2S(Xj[vr]),Pj[vr])
call PB(vr)
call xB(u,p,x,y,T)
call OB((GetUnitUserData(u)))
endfunction
function cc takes nothing returns nothing
local integer vr=Xt()
call zB(vr,1)
call ju("Arrow "+I2S(vr)+" is created.")
endfunction
function Dc takes nothing returns nothing
local integer vr=Yt()
call zB(vr,2)
call ju("Multishot "+I2S(vr)+" is created.")
endfunction
function Ec takes nothing returns nothing
local integer vr=Zt()
call zB(vr,3)
call ju("Lightning "+I2S(vr)+" is created.")
endfunction
function Fc takes nothing returns nothing
local integer vr=du()
call zB(vr,4)
call ju("Burst "+I2S(vr)+" is created.")
endfunction
function Gc takes nothing returns nothing
local integer vr=eu()
call zB(vr,5)
call ju("GoldMine "+I2S(vr)+" is created.")
endfunction
function Hc takes nothing returns nothing
local integer vr=fu()
call zB(vr,7)
call ju("FoE "+I2S(vr)+" is created.")
endfunction
function Ic takes nothing returns nothing
local integer vr=gu()
call zB(vr,8)
call ju("Blood "+I2S(vr)+" is created.")
endfunction
function lc takes nothing returns nothing
local integer vr=hu()
call zB(vr,9)
call ju("Wave "+I2S(vr)+" is created.")
endfunction
function Jc takes nothing returns nothing
local integer vr=rt()
call zB(vr,6)
call ju("FoL "+I2S(vr)+" is created.")
endfunction
function Kc takes integer T,integer Lc,integer Mc,integer Nc,integer Oc,string lv returns nothing
set j7[T]=yr()
set Dn[j7[T]]=Lc
set En[j7[T]]=Mc
set Fn[j7[T]]=Nc
set Gn[j7[T]]=Oc
set Hn[j7[T]]=ck[g7[Lc]]
set In[j7[T]]=lv
endfunction
function Pc takes integer T,integer Iv,integer Qc,integer Rc,integer Sc,integer Tc,code Uc,integer Vc,integer Wc,integer Xc,integer Yc,integer Zc,code dD returns nothing
set ln[Jn[j7[T]]]=Iv
set Kn[Ln[j7[T]]]=Rc
set Mn[Nn[j7[T]]]=Qc
set On[Pn[j7[T]]]=Sc
set Qn[Rn[j7[T]]]=Tc
if Uc!=null then
set Sn[Tn[j7[T]]]=CreateTrigger()
call TriggerAddAction(Sn[Tn[j7[T]]],Uc)
endif
set ln[Jn[j7[T]]+1]=Vc
set Kn[Ln[j7[T]]+1]=Xc
set Mn[Nn[j7[T]]+1]=Wc
set On[Pn[j7[T]]+1]=Yc
set Qn[Rn[j7[T]]+1]=Zc
if dD!=null then
set Sn[Tn[j7[T]]+1]=CreateTrigger()
call TriggerAddAction(Sn[Tn[j7[T]]+1],dD)
endif
endfunction
function fD takes integer p,integer id returns boolean
local string my=GetObjectName(id)
local integer T
local integer ry
if SubString(my,0,wn)=="AbilityUpgrade" then
set T=S2I(SubString(my,xn,yn))
set ry=S2I(SubString(my,zn,An))
set my=SubString(my,an,StringLength(my))
if Tx(p,Qn[Rn[j7[T]]+ry])then
set cn=p
set Q9[R9[(Kg[Lg[p]+T])]+(ry)]=(true)
call TriggerExecute(Sn[Tn[j7[T]]+ry])
call SetPlayerAbilityAvailable(Jf[p],id,false)
call SetPlayerAbilityAvailable(Jf[p],Kn[Ln[j7[T]]+ry],true)
call SetPlayerTechResearched(Jf[p],On[Pn[j7[T]]+ry],1)
call Hx((p),w8,od[fd]+"Research complete|r: "+(my)+".")
return true
endif
endif
return false
endfunction
function gD takes integer vr,integer p returns nothing
call UnitAddAbility(wg[p],En[vr])
call UnitAddAbility(wg[p],Fn[vr])
call UnitAddAbility(wg[p],ln[Jn[vr]])
call UnitAddAbility(wg[p],Kn[Ln[vr]])
call UnitAddAbility(wg[p],ln[Jn[vr]+1])
call UnitAddAbility(wg[p],Kn[Ln[vr]+1])
call UnitAddAbility(tg[p],Gn[vr])
endfunction
function hD takes integer vr,player p,boolean iD returns nothing
call SetPlayerAbilityAvailable(p,Hn[vr],iD)
call SetPlayerAbilityAvailable(p,Gn[vr],iD)
call SetPlayerAbilityAvailable(p,Mn[Nn[vr]],iD)
call SetPlayerAbilityAvailable(p,Mn[Nn[vr]+1],iD)
call SetPlayerAbilityAvailable(p,Fn[vr],iD)
call SetPlayerAbilityAvailable(p,En[vr],not iD)
endfunction
function jD takes integer vr,integer p returns nothing
call hD(Mg[p],Jf[p],false)
call hD(vr,Jf[p],true)
call Dx(p,vr)
endfunction
function kD takes integer p,integer id returns boolean
local string my=GetObjectName(id)
local integer T
if SubString(my,0,Bn)=="HeroAbility" then
set T=S2I(SubString(my,bn,Cn))
call jD(j7[T],p)
return true
endif
return false
endfunction
function mD takes integer T,integer nD,real oD,integer ny,integer pD,integer qD,integer rD,integer sD,integer tD,integer uD,integer vD,real wD,real xD,integer yD,real zD,integer AD,integer aD returns nothing
set k7[T]=xr()
set mo[k7[T]]=pD
set to[k7[T]]=wD
if xD==c4 then
set uo[k7[T]]=wD/ 180.*.25
else
set uo[k7[T]]=xD*.25
endif
set eo[k7[T]]=yD
set io[k7[T]]=nD
set no[k7[T]]=qD
set oo[k7[T]]=rD
set po[k7[T]]=sD
set qo[k7[T]]=tD
set jo[k7[T]]=oD
set ko[k7[T]]=ny
set ro[k7[T]]=uD
set so[k7[T]]=vD
set xo[k7[T]]=zD
set yo[k7[T]]=AD
set zo[k7[T]]=aD
endfunction
function BD takes integer T,integer bD,integer CD,integer cD,integer ED,boolean FD,boolean GD,real HD,code Xb returns nothing
set Xn[k7[T]]=bD
set Yn[k7[T]]=CD
set Zn[k7[T]]=cD
set ho[k7[T]]=ED
set fo[k7[T]]=FD
set go[k7[T]]=GD
set do[k7[T]]=HD
set m7[T]=CreateTrigger()
call TriggerAddCondition(m7[T],Filter(Xb))
endfunction
function lD takes integer T,integer Iv,integer Qc,integer Rc,integer Sc,integer Tc,code Uc,integer Vc,integer Wc,integer Xc,integer Yc,integer Zc,code dD,integer JD,integer KD,integer LD,integer MD,integer ND,code OD returns nothing
set n7[T]=wr()
set co[Do[n7[T]]]=Iv
set co[Do[n7[T]]+1]=Vc
set co[Do[n7[T]]+2]=JD
set Eo[Fo[n7[T]]]=Rc
set Eo[Fo[n7[T]]+1]=Xc
set Eo[Fo[n7[T]]+2]=LD
set Go[Ho[n7[T]]]=Qc
set Go[Ho[n7[T]]+1]=Wc
set Go[Ho[n7[T]]+2]=KD
set Io[lo[n7[T]]]=Sc
set Io[lo[n7[T]]+1]=Yc
set Io[lo[n7[T]]+2]=MD
set Jo[Ko[n7[T]]]=Tc
set Jo[Ko[n7[T]]+1]=Zc
set Jo[Ko[n7[T]]+2]=ND
if Uc!=null then
set Lo[Mo[n7[T]]]=CreateTrigger()
call TriggerAddAction(Lo[Mo[n7[T]]],Uc)
endif
if dD!=null then
set Lo[Mo[n7[T]]+1]=CreateTrigger()
call TriggerAddAction(Lo[Mo[n7[T]]+1],dD)
endif
if OD!=null then
set Lo[Mo[n7[T]]+2]=CreateTrigger()
call TriggerAddAction(Lo[Mo[n7[T]]+2],OD)
endif
endfunction
function QD takes integer T,integer nD,boolean RD,integer pD,boolean SD,boolean TD,integer UD,integer VD,integer WD,integer XD,integer YD,integer ZD returns nothing
set rp[(T)]=nD
set sp[(T)]=RD
set tp[(T)]=pD
set vp[(T)]=SD
set wp[(T)]=TD
set xp[(T)]=UD
set yp[(T)]=VD
set zp[(T)]=WD
set Ap[(T)]=XD
set ap[(T)]=YD
set Bp[(T)]=ZD
endfunction
function dE takes integer T,real eE,real fE,integer zc,integer gE,boolean hE,integer iE,code jE returns nothing
set kp[(T)]=eE
set mp[(T)]=fE
set np[(T)]=zc
set op[(T)]=gE
set pp[(T)]=hE
set qp[(T)]=iE
set bp[(T)]=CreateTrigger()
call TriggerAddCondition(bp[(T)],Filter(jE))
endfunction
function kE takes integer T,real mE,integer Oy,real cd,integer xx,integer nE,real lu,integer n,boolean GB,integer pw,integer xc returns nothing
set Xo[(T)]=mE
set Yo[(T)]=Oy
set Zo[(T)]=cd
set dp[(T)]=xx
set ep[(T)]=T4[nE]
set fp[(T)]=lu
set gp[(T)]=n
set hp[(T)]=GB
set ip[(T)]=pw
set jp[(T)]=xc
endfunction
function pE takes integer T,integer qE,integer rE,integer sE,integer tE,integer Oc,integer uE,integer vE,string lv returns nothing
set p7[T]=ur()
set Qo[p7[T]]=qE
set Ro[So[p7[T]]]=j7[rE]
set Ro[So[p7[T]]+1]=j7[sE]
set Ro[So[p7[T]]+2]=j7[tE]
set To[p7[T]]=Oc
set Uo[p7[T]]=uE
set Vo[p7[T]]=vE
set Wo[p7[T]]=lv
endfunction
function InitHeroData takes nothing returns nothing
call pE(1,6,1,2,3,1093677365,1093677366,1093677367,null)
endfunction
function wE takes integer vr,integer p returns nothing
local integer a=0
set Jg[p]=vr
call UnitRemoveAbility(wg[p],Uo[vr])
call UnitRemoveAbility(wg[p],Vo[vr])
call UnitRemoveAbility(tg[p],1093677364)
call UnitAddAbility(tg[p],To[vr])
loop
call gD(Ro[So[vr]+a],p)
set a=a+1
exitwhen a==3
endloop
set a=0
loop
call UnitAddAbility(wg[p],co[Do[n7[Qo[vr]]]+a])
call UnitAddAbility(wg[p],Eo[Fo[n7[Qo[vr]]]+a])
set a=a+1
exitwhen a==3
endloop
call jD(Ro[So[vr]],p)
endfunction
function InitGlobals takes nothing returns nothing
endfunction
function xE takes nothing returns nothing
local string s
set u7=u7+1
if u7==60 then
set u7=0
set v7=v7+1
endif
set x7=x7-1
if x7==0 then
set w7=w7+1
call TriggerExecute(X7)
set x7=40
endif
if v7<$A then
set s="0"
else
set s=""
endif
set s=s+I2S(v7)+":"
if u7<$A then
set s=s+"0"
endif
set s=s+I2S(u7)
call MultiboardSetItemValue(T8,s)
set s="Wave "+od[Z9]+I2S(w7)+"|r in "
if Of[d4]and x7<4 then
set s=s+od[gd]
call StartSound(y8)
else
set s=s+od[Z9]
endif
set s=s+I2S(x7)+"|r second"
if x7!=1 then
set s=s+"s"
endif
call MultiboardSetTitleText(F8,s)
endfunction
function yE takes nothing returns nothing
local integer p=(B4[GetPlayerId((GetTriggerPlayer()))])
local integer zE=Sf[p]
local real x=je[zE]
local real y=320.
local boolean AE=true
local integer t
local unit u
loop
call vB(zE,x,y,true)
set u=CreateUnit(Jf[p],1949315120,x,y,270.)
call xB(u,p,x,y,0)
set t=GetUnitUserData(u)
call OB(t)
if AE then
set x=x+128.
if x>=ie[zE]then
set y=y-256.
set AE=false
endif
else
set x=x-128.
if x<=je[zE]then
set y=y-256.
set AE=true
endif
endif
exitwhen y<o4
endloop
call iw(zE)
set u=null
endfunction
function aE takes nothing returns nothing
call PauseTimer(q7)
call DisableTrigger(A7)
call EnableTrigger(a7)
endfunction
function BE takes nothing returns nothing
call xE()
call TimerStart(q7,1.,true,function xE)
call EnableTrigger(A7)
endfunction
function bE takes nothing returns nothing
call TimerStart(q7,TimerGetRemaining(q7),false,function BE)
call DisableTrigger(a7)
endfunction
function CE takes nothing returns nothing
set x7=1
call xE()
endfunction
function cE takes nothing returns nothing
call aE()
call CE()
endfunction
function DE takes nothing returns nothing
local integer m=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),h4))and qh[m]then
call Pz(m,false)
endif
endfunction
function EE takes nothing returns nothing
call GroupEnumUnitsSelected(bj_lastCreatedGroup,GetTriggerPlayer(),c7)
endfunction
function FE takes nothing returns nothing
call oy((B4[GetPlayerId((GetTriggerPlayer()))]))
endfunction
function GE takes nothing returns nothing
local integer t=GetUnitUserData(GetFilterUnit())
local integer a=0
if(IsUnitType(GetFilterUnit(),f4))and Sj[t]and ik[t]then
loop
set a=a+1
call SB(t)
exitwhen(not ik[t])or a==H7
endloop
if ik[t]and wp[Rj[t]]then
call SetItemCharges(rk[t],mk[t]-jk[t])
endif
endif
endfunction
function HE takes nothing returns nothing
local string in=GetEventPlayerChatString()
if SubString(in,0,F7)=="inc" then
set H7=S2I(SubString(in,G7,StringLength(in)))
if H7>0 then
call GroupEnumUnitsSelected(bj_lastCreatedGroup,GetTriggerPlayer(),I7)
endif
endif
endfunction
function IE takes nothing returns nothing
local string in=GetEventPlayerChatString()
local integer p=(B4[GetPlayerId((GetTriggerPlayer()))])
local integer Rs
if SubString(in,0,J7)=="gold" then
set Rs=S2I(SubString(in,K7,StringLength(in)))
if 0<Rs and Rs<=99999 then
call Mx(p,Rs-Vf[p])
endif
endif
endfunction
function lE takes nothing returns nothing
local string in=GetEventPlayerChatString()
local integer p=(B4[GetPlayerId((GetTriggerPlayer()))])
local integer Rs
if SubString(in,0,M7)=="exp" then
set Rs=S2I(SubString(in,N7,StringLength(in)))
if 0<Rs and Rs<=99999 then
call Rx(p,Rs-Wf[p])
endif
endif
endfunction
function JE takes nothing returns nothing
local string in=GetEventPlayerChatString()
local integer p=(B4[GetPlayerId((GetTriggerPlayer()))])
local integer Rs
if SubString(in,0,P7)=="lives" then
set Rs=S2I(SubString(in,Q7,StringLength(in)))
if 0<Rs and Rs<=9999 then
call Xx(p,Rs-dg[p])
endif
endif
endfunction
function KE takes nothing returns nothing
local string in=GetEventPlayerChatString()
local integer LE
if SubString(in,0,S7)=="wave" then
set LE=S2I(SubString(in,T7,StringLength(in)))
if LE>0 then
set w7=LE
endif
endif
endfunction
function ME takes nothing returns nothing
local string in=GetEventPlayerChatString()
local integer rc
if SubString(in,0,V7)=="min" then
set rc=S2I(SubString(in,W7,StringLength(in)))
if rc>=0 then
set v7=rc
endif
endif
endfunction
function NE takes player p returns nothing
call TriggerAddAction(z7,function yE)
call TriggerAddAction(A7,function aE)
call DisableTrigger(a7)
call TriggerAddAction(a7,function bE)
call TriggerAddAction(B7,function CE)
call TriggerAddAction(b7,function cE)
call TriggerAddAction(C7,function EE)
set c7=Filter(function DE)
call TriggerAddAction(D7,function FE)
call TriggerAddAction(E7,function HE)
set I7=Filter(function GE)
call TriggerAddAction(l7,function IE)
call TriggerAddAction(L7,function lE)
call TriggerAddAction(O7,function JE)
call TriggerAddAction(R7,function KE)
call TriggerAddAction(U7,function ME)
call TriggerRegisterPlayerChatEvent(z7,p,"maze",true)
call TriggerRegisterPlayerChatEvent(A7,p,"p",true)
call TriggerRegisterPlayerChatEvent(a7,p,"r",true)
call TriggerRegisterPlayerChatEvent(B7,p,"s",true)
call TriggerRegisterPlayerChatEvent(b7,p,"ps",true)
call TriggerRegisterPlayerChatEvent(C7,p,"kill",true)
call TriggerRegisterPlayerChatEvent(D7,p,"hero",true)
call TriggerRegisterPlayerChatEvent(E7,p,"inc",false)
call TriggerRegisterPlayerChatEvent(l7,p,"gold",false)
call TriggerRegisterPlayerChatEvent(L7,p,"exp",false)
call TriggerRegisterPlayerChatEvent(O7,p,"lives",false)
call TriggerRegisterPlayerChatEvent(R7,p,"wave",false)
call TriggerRegisterPlayerChatEvent(U7,p,"min",false)
endfunction
function OE takes nothing returns nothing
local integer i=0
local integer a
local integer p
call dv()
loop
exitwhen i>d9
set p=(Z8[(i)])
call Ex(p)
set a=0
loop
exitwhen a>s9[zg[p]]
call wy((q9[r9[(zg[p])]+(a)]))
set a=a+1
endloop
set Ng[p]=0
set s9[zg[p]]=-1
call Bx(p)
if y7<=w7 and(25<w7 or nu(w7))then
call oy(p)
endif
call Fx(p)
if 21<=v7 and dg[p]>1 then
call Zx(p)
endif
call Mx(p,Zf[p])
call Hx(p,x8,"You receive "+od[T9]+I2S(Zf[p])+"|r Gold.")
set i=i+1
endloop
endfunction
function InitTrig_SendMinions takes nothing returns nothing
call TriggerAddAction(X7,function OE)
endfunction
function PE takes nothing returns nothing
local integer tc=zg[Y7]
local integer a=s9[tc]
loop
exitwhen a==-1
call Is((q9[r9[(tc)]+(a)]))
set a=a-1
endloop
call ju("forces of player "+I2S(Y7)+" is flushed.")
endfunction
function QE takes nothing returns nothing
local integer tc=Ag[Y7]
local integer a=y9[tc]
loop
exitwhen a==-1
call xy((w9[x9[(tc)]+(a)]))
set a=a-1
endloop
call ju("spawned of player "+I2S(Y7)+" is flushed.")
endfunction
function RE takes nothing returns nothing
local integer tc=ag[Y7]
local integer a=C9[tc]
loop
exitwhen a==-1
call Oz((B9[b9[(tc)]+(a)]))
set a=a-1
endloop
call ju("minions of player "+I2S(Y7)+" is flushed.")
endfunction
function SE takes nothing returns nothing
local integer tc=bg[Y7]
local integer a=H9[tc]
loop
exitwhen a==-1
call Mz((F9[G9[(tc)]+(a)]))
set a=a-1
endloop
call ju("dead of player "+I2S(Y7)+" is flushed.")
endfunction
function TE takes nothing returns nothing
local integer tc=yg[Y7]
local integer a=m9[tc]
loop
exitwhen a==-1
call QB((j9[k9[(tc)]+(a)]))
set a=a-1
endloop
call ju("towers of player "+I2S(Y7)+" is flushed.")
endfunction
function UE takes nothing returns nothing
local integer p=Y7
call Yu(p)
call ExecuteFunc("PE")
call ExecuteFunc("QE")
call ExecuteFunc("RE")
call ExecuteFunc("SE")
call ExecuteFunc("TE")
call gy(p)
call ju("Player "+I2S(p)+" is flushed.")
endfunction
function InitTrig_FlushPlayer takes nothing returns nothing
call TriggerAddAction(Z7,function UE)
endfunction
function VE takes nothing returns nothing
local integer p=(B4[GetPlayerId((GetTriggerPlayer()))])
if Of[p]then
call Ou(p)
if d9==0 then
call Pu($A)
endif
endif
endfunction
function InitTrig_LeavesGame takes nothing returns nothing
call TriggerAddAction(d8,function VE)
endfunction
function WE takes nothing returns nothing
call yB(GetConstructingStructure())
endfunction
function InitTrig_ConstructStart takes nothing returns nothing
call TriggerAddAction(e8,function WE)
endfunction
function XE takes nothing returns nothing
call RB((GetUnitUserData(GetCancelledStructure())))
endfunction
function InitTrig_ConstructCancel takes nothing returns nothing
call TriggerAddAction(f8,function XE)
endfunction
function YE takes nothing returns nothing
call OB((GetUnitUserData(GetConstructedStructure())))
endfunction
function InitTrig_ConstructEnd takes nothing returns nothing
call TriggerAddAction(g8,function YE)
endfunction
function ZE takes nothing returns nothing
call Bc(GetTriggerUnit())
endfunction
function InitTrig_UpgradeStart takes nothing returns nothing
call TriggerAddAction(h8,function ZE)
endfunction
function d3 takes nothing returns nothing
call bc(GetTriggerUnit())
endfunction
function InitTrig_UpgradeCancel takes nothing returns nothing
call TriggerAddAction(i8,function d3)
endfunction
function e3 takes nothing returns nothing
call Cc(GetTriggerUnit())
endfunction
function InitTrig_UpgradeEnd takes nothing returns nothing
call TriggerAddAction(j8,function e3)
endfunction
function f3 takes nothing returns nothing
local integer m=GetUnitUserData(GetLeavingUnit())
if rh[m]then
call Iz(m)
endif
endfunction
function InitTrig_LeaveSpawn takes nothing returns nothing
call TriggerAddAction(k8,function f3)
endfunction
function g3 takes nothing returns nothing
local integer m=GetUnitUserData(GetEnteringUnit())
local integer p=jh[m]
call Yx(p,ro[vh[m]])
call yy(m,false)
if p!=ih[m]and Of[ih[m]]then
call Yx(ih[m],so[vh[m]])
endif
call DestroyEffect(AddSpecialEffect(("Abilities\\Spells\\Undead\\DarkRitual\\DarkRitualTarget.mdx"),((GetUnitX(mh[m]))*1.),((GetUnitY(mh[m]))*1.)))
call Oz(m)
if dg[p]==0 then
call Ou(p)
if bj_isSinglePlayer!=(d9==0)then
call Pu($A)
endif
endif
endfunction
function InitTrig_EnterFinish takes nothing returns nothing
call TriggerAddAction(m8,function g3)
endfunction
function h3 takes unit b,integer id returns nothing
local integer p=(B4[GetPlayerId((GetOwningPlayer(b)))])
if id==Bd[Og[p]]then
if Pf[p]then
call ClearSelection()
call SelectUnit(ug[p],true)
endif
if Qg[p]!=Og[p]then
call Pv(p)
endif
return
endif
if id==bd[Pg[p]]then
if Pf[p]then
call ClearSelection()
call SelectUnit(ug[p],true)
endif
if Qg[p]!=Pg[p]then
call Pv(p)
endif
return
endif
if(id==To[Jg[p]]or id==1093677364)and Pf[p]then
call ClearSelection()
call SelectUnit(wg[p],true)
endif
endfunction
function i3 takes unit b,integer id returns nothing
local integer p=(B4[GetPlayerId((GetOwningPlayer(b)))])
if ky(p,id)then
return
endif
if id==1093677123 then
if Pf[p]then
call ClearSelection()
call SelectUnit(vg[p],true)
endif
if Qg[p]!=Sg[p]then
call Kv(p)
endif
return
endif
if id==1093677135 and Pf[p]then
call ClearSelection()
call SelectUnit(wg[p],true)
return
endif
if id==1093677144 and Pf[p]then
call ClearSelection()
call SelectUnit(xg[p],true)
return
endif
if id==1093677124 then
call Pv(p)
endif
endfunction
function j3 takes unit b,integer id returns nothing
local integer p=(B4[GetPlayerId((GetOwningPlayer(b)))])
if py(p,id,true)then
return
endif
if id==1093677362 then
call Kv(p)
return
endif
if id==1093677361 then
call Sz(p)
endif
endfunction
function k3 takes unit b,integer id returns nothing
local integer p=(B4[GetPlayerId((GetOwningPlayer(b)))])
if id==Uo[Ad[Og[p]]]then
call UnitRemoveAbility(b,Vo[Ad[Pg[p]]])
call wE(Ad[Og[p]],p)
return
endif
if id==Vo[Ad[Pg[p]]]then
call UnitRemoveAbility(b,Uo[Ad[Og[p]]])
call wE(Ad[Pg[p]],p)
return
endif
if py(p,id,false)then
return
endif
if kD(p,id)then
return
endif
call fD(p,id)
endfunction
function m3 takes integer RA,integer id,integer zs returns nothing
if id==1093677119 or id==1093677121 then
call RB(RA)
return
endif
if id==1093677129 then
call YB(RA)
return
endif
if id==1093677127 then
call IC(sk[RA],zs)
endif
endfunction
function n3 takes integer m,integer id returns nothing
if id==1093677122 then
call Is(m)
return
endif
if id==ck[g7[1]]then
call Oa(ph[m])
endif
endfunction
function o3 takes nothing returns nothing
local integer id=GetSpellAbilityId()
local unit u=GetSpellAbilityUnit()
local integer RA=GetUnitUserData(u)
local integer zs=GetUnitUserData(GetSpellTargetUnit())
local integer T=GetUnitTypeId(u)
if T==1112294482 then
call h3(u,id)
elseif T==1112689491 then
call i3(u,id)
elseif T==1112232788 then
call j3(u,id)
elseif T==1095521362 then
call k3(u,id)
elseif(IsUnitType((u),f4))then
call m3(RA,id,zs)
elseif(IsUnitType((u),h4))then
call n3(RA,id)
endif
set u=null
endfunction
function InitTrig_PlayerAbilityEffect takes nothing returns nothing
call TriggerAddAction(n8,function o3)
endfunction
function p3 takes integer RA,integer id returns nothing
local integer pw=sk[RA]
if qq[pw]==86 then
call kC(pw,id)
return
endif
if qq[pw]==91 then
call JC(pw,id)
endif
endfunction
function q3 takes integer m,integer id returns nothing
if id==1093677099 then
call GroupEnumUnitsSelected(bj_lastCreatedGroup,Jf[(ih[m])],Ih)
return
endif
if mq[ph[m]]==66 then
call Va(ph[m],id)
endif
endfunction
function r3 takes nothing returns nothing
local integer id=GetSpellAbilityId()
local unit u=GetSpellAbilityUnit()
local integer RA=GetUnitUserData(u)
if(IsUnitType((u),f4))or(IsUnitType((u),g4))then
call p3(RA,id)
elseif(IsUnitType((u),h4))then
call q3(RA,id)
endif
set u=null
endfunction
function InitTrig_PlayerAbilityEndcast takes nothing returns nothing
call TriggerAddAction(o8,function r3)
endfunction
function s3 takes unit b,integer id returns nothing
local integer p=(B4[GetPlayerId((GetOwningPlayer(b)))])
if id==A4 or id==a4 then
call lx(p)
endif
endfunction
function t3 takes integer RA,integer id returns nothing
local integer pw=sk[RA]
if 1949315120<=id and id<1949315120+o7 then
set sn[(RA)]=id-1949315120
return
endif
if qq[pw]==94 then
call wc(pw,id)
endif
endfunction
function u3 takes nothing returns nothing
local integer id=GetIssuedOrderId()
local unit u=GetOrderedUnit()
local integer RA=GetUnitUserData(u)
local integer T=GetUnitTypeId(u)
if T==1112294482 then
call s3(u,id)
elseif(IsUnitType((u),f4))then
call t3(RA,id)
endif
set u=null
endfunction
function InitTrig_PlayerImdtOrders takes nothing returns nothing
call TriggerAddAction(p8,function u3)
endfunction
function v3 takes unit b,integer id,real x,real y returns nothing
if id==V or(1949315120<=id and id<1949315120+o7)then
call SetUnitX(b,x)
call SetUnitY(b,y)
endif
endfunction
function w3 takes nothing returns nothing
local integer id=GetIssuedOrderId()
local unit u=GetOrderedUnit()
local integer T=GetUnitTypeId(u)
local real x=GetOrderPointX()
local real y=GetOrderPointY()
local integer zE=Sf[(B4[GetPlayerId((GetOwningPlayer(u)))])]
if T==1112294482 and he[zE]<=x and x<=ge[zE]then
call v3(u,id,x,y)
endif
set u=null
endfunction
function InitTrig_PlayerPointOrders takes nothing returns nothing
call TriggerAddAction(q8,function w3)
endfunction
function x3 takes unit u returns nothing
local integer ZB=GetUnitUserData(u)
if(IsUnitType((u),f4))then
call ns((ZB))
endif
endfunction
function y3 takes nothing returns nothing
if GetTriggerPlayer()==GetOwningPlayer(GetTriggerUnit())then
call x3(GetTriggerUnit())
endif
endfunction
function InitTrig_PlayerSelects takes nothing returns nothing
call TriggerAddAction(r8,function y3)
endfunction
function z3 takes unit u returns nothing
local integer ZB=GetUnitUserData(u)
if(IsUnitType((u),f4))then
call os((ZB))
endif
endfunction
function A3 takes nothing returns nothing
if GetTriggerPlayer()==GetOwningPlayer(GetTriggerUnit())then
call z3(GetTriggerUnit())
endif
endfunction
function InitTrig_PlayerDeselects takes nothing returns nothing
call TriggerAddAction(s8,function A3)
endfunction
function B3 takes integer t,integer m returns nothing
if not(Gf[oh[(m)]])then
call is(t,m)
endif
endfunction
function C3 takes unit DD,unit c3,real mE returns nothing
local integer dd=GetUnitUserData(DD)
local integer zs=GetUnitUserData(c3)
call SetWidgetLife(c3,GetWidgetLife(c3)+mE)
if(not UnitAlive((c3)))then
call DisplayTimedTextToPlayer(F,.0,.0,((10.)*1.),("No target took damage!"))
return
endif
if(IsUnitType((DD),g4))then
call B3(dd,zs)
elseif(IsUnitType((DD),i4))then
call ys((dd),(zs))
endif
endfunction
function D3 takes nothing returns nothing
if GetEventDamage()>.0 and(GetUnitTypeId((GetEventDamageSource()))!=0)then
call C3(GetEventDamageSource(),GetTriggerUnit(),GetEventDamage())
endif
endfunction
function InitTrig_TakeDamage takes nothing returns nothing
call TriggerAddAction(t8,function D3)
endfunction
function E3 takes nothing returns nothing
local integer i=0
local integer ai=0
local player p
loop
set p=Player(i)
if GetPlayerSlotState(p)==PLAYER_SLOT_STATE_PLAYING then
if GetPlayerController(p)==MAP_CONTROL_USER then
call ts(p,i)
endif
elseif ai<2 then
set Z[ai]=p
call SetPlayerColor(p,e4)
call FogModifierStart(CreateFogModifierRect(p,FOG_OF_WAR_VISIBLE,bj_mapInitialPlayableArea,false,true))
set ai=ai+1
endif
set i=i+1
exitwhen i==$C
endloop
if C4!=null then
set i=0
loop
exitwhen(not b4[i])and ConvertPlayerColor(i)!=e4
set i=i+1
endloop
call SetPlayerColor(C4,ConvertPlayerColor(i))
set Mf[(B4[GetPlayerId((C4))])]=i
endif
set d4=(B4[GetPlayerId((F))])
call TriggerRegisterLeaveRegion(k8,t7,k4)
call TriggerRegisterEnterRegion(m8,s7,k4)
call ju("Initialization complete!")
set p=null
endfunction
function InitTrig_Main takes nothing returns nothing
call TriggerAddAction(D8,function E3)
endfunction
function F3 takes nothing returns nothing
local integer i=0
local integer p
loop
exitwhen i>d9
set p=(Z8[(i)])
set Og[p]=n4[1]
set Pg[p]=0
call us(p)
call wE(Ad[n4[1]],p)
set i=i+1
endloop
set bj_isSinglePlayer=d9==0
call TriggerExecute(V8)
call xE()
call TimerStart(q7,1.,true,function xE)
if bj_isSinglePlayer then
call NE(Jf[(Z8[(0)])])
endif
endfunction
function H3 takes nothing returns integer
set G8=G8+1
call MultiboardSetRowCount(F8,G8)
return G8-1
endfunction
function I3 takes nothing returns nothing
local integer a
local multiboarditem l3
local integer p
set F8=CreateMultiboard()
call MultiboardSetTitleTextColor(F8,$FF,$FF,$FF,$FF)
call MultiboardSetColumnCount(F8,H8)
call MultiboardSetItemsStyle(F8,true,false)
call MultiboardSetItemsWidth(F8,I8)
set a=H3()
set l3=MultiboardGetItem(F8,a,0)
call MultiboardSetItemWidth(l3,l8)
call MultiboardSetItemValue(l3,"Player")
call MultiboardReleaseItem(l3)
set l3=MultiboardGetItem(F8,a,2)
call MultiboardSetItemWidth(l3,J8)
call MultiboardSetItemValue(l3,"")
call MultiboardReleaseItem(l3)
set l3=MultiboardGetItem(F8,a,3)
call MultiboardSetItemWidth(l3,K8)
call MultiboardSetItemValue(l3,"Foe")
call MultiboardReleaseItem(l3)
set l3=MultiboardGetItem(F8,a,4)
call MultiboardSetItemWidth(l3,L8)
call MultiboardSetItemValue(l3,"Gold")
call MultiboardReleaseItem(l3)
set l3=MultiboardGetItem(F8,a,5)
call MultiboardSetItemWidth(l3,M8)
call MultiboardSetItemValue(l3,"Exp.")
call MultiboardReleaseItem(l3)
set l3=MultiboardGetItem(F8,a,6)
call MultiboardSetItemWidth(l3,N8)
call MultiboardSetItemValue(l3,"Supply")
call MultiboardReleaseItem(l3)
set l3=MultiboardGetItem(F8,a,7)
call MultiboardSetItemWidth(l3,O8)
call MultiboardSetItemValue(l3,"Inc.")
call MultiboardReleaseItem(l3)
set l3=MultiboardGetItem(F8,a,8)
call MultiboardSetItemWidth(l3,P8)
call MultiboardSetItemValue(l3,"Lives")
call MultiboardReleaseItem(l3)
set l3=MultiboardGetItem(F8,a,9)
call MultiboardSetItemWidth(l3,Q8)
call MultiboardSetItemValue(l3,"Kills")
call MultiboardReleaseItem(l3)
set a=0
loop
exitwhen a>d9
set p=(Z8[(a)])
set fg[p]=H3()
set gg[p]=MultiboardGetItem(F8,fg[p],0)
call MultiboardSetItemWidth(gg[p],l8)
call MultiboardSetItemValue(gg[p],Lf[p])
call MultiboardSetItemValueColor(gg[p],kd[Mf[p]],md[Mf[p]],nd[Mf[p]],U8)
set hg[p]=MultiboardGetItem(F8,fg[p],2)
call MultiboardSetItemWidth(hg[p],J8)
call MultiboardSetItemValue(hg[p],"-->")
set ig[p]=MultiboardGetItem(F8,fg[p],3)
call MultiboardSetItemWidth(ig[p],K8)
call MultiboardSetItemStyle(ig[p],false,true)
call MultiboardSetItemIcon(ig[p],"Icons\\QuestionMark.blp")
set jg[p]=MultiboardGetItem(F8,fg[p],4)
call MultiboardSetItemWidth(jg[p],L8)
call MultiboardSetItemValue(jg[p],I2S(Vf[p]))
call MultiboardSetItemValueColor(jg[p],kd[T9],md[T9],nd[T9],U8)
set kg[p]=MultiboardGetItem(F8,fg[p],5)
call MultiboardSetItemWidth(kg[p],M8)
call MultiboardSetItemValue(kg[p],I2S(Wf[p]))
call MultiboardSetItemValueColor(kg[p],kd[U9],md[U9],nd[U9],U8)
set mg[p]=MultiboardGetItem(F8,fg[p],6)
call MultiboardSetItemWidth(mg[p],N8)
call MultiboardSetItemValue(mg[p],"0/"+I2S(Xf[p]))
call MultiboardSetItemValueColor(mg[p],kd[V9],md[V9],nd[V9],U8)
set ng[p]=MultiboardGetItem(F8,fg[p],7)
call MultiboardSetItemWidth(ng[p],O8)
call MultiboardSetItemValue(ng[p],I2S(Zf[p]))
call MultiboardSetItemValueColor(ng[p],kd[W9],md[W9],nd[W9],U8)
set og[p]=MultiboardGetItem(F8,fg[p],8)
call MultiboardSetItemWidth(og[p],P8)
call MultiboardSetItemValue(og[p],I2S(dg[p]))
call MultiboardSetItemValueColor(og[p],kd[X9],md[X9],nd[X9],U8)
set pg[p]=MultiboardGetItem(F8,fg[p],9)
call MultiboardSetItemWidth(pg[p],Q8)
call MultiboardSetItemValue(pg[p],"0")
call MultiboardSetItemValueColor(pg[p],kd[Y9],md[Y9],nd[Y9],U8)
set a=a+1
endloop
call H3()
set a=H3()
set l3=MultiboardGetItem(F8,a,0)
call MultiboardSetItemWidth(l3,R8)
call MultiboardSetItemValue(l3,"Game time: ")
call MultiboardReleaseItem(l3)
set l3=null
set T8=MultiboardGetItem(F8,a,1)
call MultiboardSetItemWidth(T8,S8)
call MultiboardSetItemValueColor(T8,kd[ed],md[ed],nd[ed],U8)
call MultiboardDisplay(F8,true)
call MultiboardMinimize(F8,false)
endfunction
function InitTrig_Multiboard takes nothing returns nothing
call TriggerAddAction(V8,function I3)
endfunction
function J3 takes nothing returns nothing
local string s
set x7=x7-1
set s="Game will end in "+od[Z9]+I2S(x7)+"|r second"
if x7>1 then
set s=s+"s"
endif
if x7==0 then
call MultiboardSetTitleText(F8,"Game is ending...")
call EndGame(true)
else
call MultiboardSetTitleText(F8,s)
endif
endfunction
function K3 takes nothing returns nothing
local integer i=0
loop
exitwhen i>d9
call Ou((Z8[(i)]))
set i=i+1
endloop
call ClearTextMessages()
call J3()
call TimerStart(q7,1.,true,function J3)
endfunction
function InitTrig_FinishGame takes nothing returns nothing
call TriggerAddAction(W8,function K3)
endfunction
function InitCustomPlayerSlots takes nothing returns nothing
call SetPlayerStartLocation(Player(0),0)
call SetPlayerColor(Player(0),ConvertPlayerColor(0))
call SetPlayerRacePreference(Player(0),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(0),true)
call SetPlayerController(Player(0),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(1),1)
call SetPlayerColor(Player(1),ConvertPlayerColor(1))
call SetPlayerRacePreference(Player(1),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(1),true)
call SetPlayerController(Player(1),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(2),2)
call SetPlayerColor(Player(2),ConvertPlayerColor(2))
call SetPlayerRacePreference(Player(2),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(2),true)
call SetPlayerController(Player(2),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(3),3)
call SetPlayerColor(Player(3),ConvertPlayerColor(3))
call SetPlayerRacePreference(Player(3),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(3),true)
call SetPlayerController(Player(3),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(4),4)
call SetPlayerColor(Player(4),ConvertPlayerColor(4))
call SetPlayerRacePreference(Player(4),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(4),true)
call SetPlayerController(Player(4),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(5),5)
call SetPlayerColor(Player(5),ConvertPlayerColor(5))
call SetPlayerRacePreference(Player(5),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(5),true)
call SetPlayerController(Player(5),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(6),6)
call SetPlayerColor(Player(6),ConvertPlayerColor(6))
call SetPlayerRacePreference(Player(6),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(6),true)
call SetPlayerController(Player(6),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(7),7)
call SetPlayerColor(Player(7),ConvertPlayerColor(7))
call SetPlayerRacePreference(Player(7),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(7),true)
call SetPlayerController(Player(7),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(8),8)
call SetPlayerColor(Player(8),ConvertPlayerColor(8))
call SetPlayerRacePreference(Player(8),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(8),true)
call SetPlayerController(Player(8),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(9),9)
call SetPlayerColor(Player(9),ConvertPlayerColor(9))
call SetPlayerRacePreference(Player(9),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(9),true)
call SetPlayerController(Player(9),MAP_CONTROL_USER)
endfunction
function InitCustomTeams takes nothing returns nothing
call SetPlayerTeam(Player(0),0)
call SetPlayerTeam(Player(1),0)
call SetPlayerTeam(Player(2),0)
call SetPlayerTeam(Player(3),0)
call SetPlayerTeam(Player(4),0)
call SetPlayerTeam(Player(5),0)
call SetPlayerTeam(Player(6),0)
call SetPlayerTeam(Player(7),0)
call SetPlayerTeam(Player(8),0)
call SetPlayerTeam(Player(9),0)
endfunction
function InitAllyPriorities takes nothing returns nothing
call SetStartLocPrioCount(0,2)
call SetStartLocPrio(0,0,8,MAP_LOC_PRIO_LOW)
call SetStartLocPrio(0,1,9,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(1,1)
call SetStartLocPrio(1,0,2,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(2,2)
call SetStartLocPrio(2,0,1,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(2,1,3,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(3,2)
call SetStartLocPrio(3,0,2,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(3,1,4,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(4,2)
call SetStartLocPrio(4,0,3,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(4,1,5,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(5,2)
call SetStartLocPrio(5,0,4,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(5,1,6,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(6,2)
call SetStartLocPrio(6,0,5,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(6,1,7,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(7,2)
call SetStartLocPrio(7,0,6,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(7,1,8,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(8,2)
call SetStartLocPrio(8,0,7,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(8,1,9,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(9,1)
call SetStartLocPrio(9,0,8,MAP_LOC_PRIO_HIGH)
endfunction
function main takes nothing returns nothing
call SetCameraBounds(B8+o,b8+B,6912.-A,1920.-C,B8+o,1920.-C,6912.-A,b8+B)
set bj_mapInitialPlayableArea=GetWorldBounds()
call ExecuteFunc("L3")
call ExecuteFunc("Fu")
call ExecuteFunc("Uu")
call ExecuteFunc("Wz")
call ExecuteFunc("NA")
call TriggerAddAction(X7,function OE)
call TriggerAddAction(Z7,function UE)
call TriggerAddAction(d8,function VE)
call TriggerAddAction(e8,function WE)
call TriggerAddAction(f8,function XE)
call TriggerAddAction(g8,function YE)
call TriggerAddAction(h8,function ZE)
call TriggerAddAction(i8,function d3)
call TriggerAddAction(j8,function e3)
call TriggerAddAction(k8,function f3)
call TriggerAddAction(m8,function g3)
call TriggerAddAction(n8,function o3)
call TriggerAddAction(o8,function r3)
call TriggerAddAction(p8,function u3)
call TriggerAddAction(q8,function w3)
call TriggerAddAction(r8,function y3)
call TriggerAddAction(s8,function A3)
call TriggerAddAction(t8,function D3)
call TriggerAddAction(D8,function E3)
call TriggerRegisterTimerEvent(E8,1.,false)
call TriggerAddAction(E8,function F3)
call TriggerAddAction(V8,function I3)
call TriggerAddAction(W8,function K3)
set u8=CreateSound("Sound\\Interface\\Error.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(u8,"InterfaceError")
call SetSoundDuration(u8,614)
call SetSoundVolume(u8,105)
set x8=CreateSound("Abilities\\Spells\\Items\\ResourceItems\\ReceiveGold.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(x8,"ReceiveGold")
call SetSoundDuration(x8,589)
call SetSoundChannel(x8,8)
call SetSoundVolume(x8,105)
set y8=CreateSound("Sound\\Interface\\BattleNetTick.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(y8,"ChatroomTimerTick")
call SetSoundDuration(y8,476)
call SetSoundVolume(y8,105)
set z8=CreateSound("Sound\\Interface\\Warning.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(z8,"Warning")
call SetSoundDuration(z8,$770)
call SetSoundVolume(z8,105)
set A8=CreateSound("Sound\\Interface\\ItemReceived.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(A8,"ItemReward")
call SetSoundDuration(A8,$5CB)
call SetSoundVolume(A8,105)
set a8=CreateSound("Sound\\Interface\\UpkeepRing.wav",false,false,false,$A,$A,"")
call SetSoundParamsFromLabel(a8,"UpkeepLevel")
call SetSoundDuration(a8,$62B)
call SetSoundVolume(a8,105)
set v8=CreateSound("Sound\\Interface\\Warning\\Human\\KnightNoGold1.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(v8,"NoGoldHuman")
call SetSoundDuration(v8,$5CE)
call SetSoundVolume(v8,105)
set w8=CreateSound("Sound\\Buildings\\Human\\KnightResearchComplete1.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(w8,"ResearchCompleteHuman")
call SetSoundDuration(w8,$52E)
call SetSoundVolume(w8,105)
set s7=CreateRegion()
set t7=CreateRegion()
set m4=Dr()
set td[ud[m4]]=1.
set td[ud[m4]+1]=1.075
set td[ud[m4]+2]=1.155625
set td[ud[m4]+3]=1.242297
set td[ud[m4]+4]=1.335469
set td[ud[m4]+5]=1.435629
set td[ud[m4]+6]=1.543302
set td[ud[m4]+7]=1.659049
set td[ud[m4]+8]=1.783478
set td[ud[m4]+9]=1.917239
set td[ud[m4]+$A]=2.061032
set td[ud[m4]+$B]=2.215609
set td[ud[m4]+$C]=2.38178
set td[ud[m4]+$D]=2.560413
set td[ud[m4]+$E]=2.752444
set td[ud[m4]+$F]=2.958877
set td[ud[m4]+16]=3.180793
set td[ud[m4]+17]=3.419353
set td[ud[m4]+18]=3.675804
set td[ud[m4]+19]=3.951489
set td[ud[m4]+20]=4.247851
set td[ud[m4]+21]=4.56644
set td[ud[m4]+22]=4.908923
set td[ud[m4]+23]=5.277092
set td[ud[m4]+24]=5.672874
set td[ud[m4]+25]=6.09834
set td[ud[m4]+26]=6.555715
set td[ud[m4]+27]=7.047394
set td[ud[m4]+28]=7.575948
set td[ud[m4]+29]=8.144144
set td[ud[m4]+30]=8.754955
set td[ud[m4]+31]=9.411577
set td[ud[m4]+32]=10.117445
set td[ud[m4]+33]=10.876253
set td[ud[m4]+34]=11.691972
set td[ud[m4]+35]=12.56887
set td[ud[m4]+36]=13.511536
set td[ud[m4]+37]=14.524901
set td[ud[m4]+38]=15.614268
set td[ud[m4]+39]=16.785339
set td[ud[m4]+40]=18.044239
set td[ud[m4]+41]=19.397557
set td[ud[m4]+42]=20.852374
set td[ud[m4]+43]=22.416302
set td[ud[m4]+44]=24.097524
set td[ud[m4]+45]=25.904839
set td[ud[m4]+46]=27.847702
set td[ud[m4]+47]=29.936279
set td[ud[m4]+48]=32.1815
set td[ud[m4]+49]=34.595113
set td[ud[m4]+50]=37.189746
call yc(1,1093677109,5,$A,0,function fC)
call yc(2,1093677133,5,$A,0,function nC)
call yc(3,1093677108,5,$A,0,function pC)
call yc(4,1093677130,5,$A,0,function tC)
call yc(5,1093677131,5,$A,0,function BC)
call yc(6,1093677111,1,2,3,function cC)
call yc(7,1093677136,0,0,0,function VC)
call yc(8,1093677107,5,$A,0,function fc)
call yc(9,1093677141,5,$A,0,function vc)
call QD(0,50,true,-1,false,false,0,0,0,0,0,e7)
call QD(1,100,true,-1,true,true,1,5,20,50,$A,e7)
call QD(2,$C8,true,-1,true,true,1,5,20,50,$A,e7)
call QD(3,$C8,true,-1,true,true,1,5,20,50,$A,e7)
call QD(7,750,false,3,true,false,1,0,0,750,3,3)
call QD(8,500,true,3,false,false,0,0,0,0,0,0)
call QD(9,$C8,true,-1,true,true,1,5,20,50,$A,e7)
call QD(4,$C8,true,0,true,false,0,0,0,0,$A,e7)
call QD(5,$4B0,false,0,true,false,1,0,60,$B4,$A,$A)
call QD(6,$3E8,false,0,true,false,1,0,50,$96,$A,$A)
call dE(0,60.,.4,1097609264,0,false,0,function ac)
call dE(1,145.,.6,1097609265,1098133553,true,1,function cc)
call dE(2,60.,.6,1097609266,1098133554,true,2,function Dc)
call dE(3,255.,.5,1097609267,1098133555,true,3,function Ec)
call dE(4,135.,.6,1097609268,1098133556,true,4,function Fc)
call dE(5,60.,.4,1097609269,1098133557,false,5,function Gc)
call dE(6,60.,.4,1097609270,1098133558,false,9,function Jc)
call dE(7,60.,.4,1097609271,1098133559,false,6,function Hc)
call dE(8,255.,.5,1097609272,0,true,7,function Ic)
call dE(9,135.,.6,1097609273,1098133561,true,8,function lc)
call kE(1,10.,m4,1.,1227894834,0,384.,1,false,1093677094,M)
call kE(2,6.,m4,2.,1227894834,0,384.,3,false,1093677097,M)
call kE(3,10.,m4,3.,1227894841,0,384.,1,false,1093677110,L)
call kE(8,30.,0,1.,1227894832,0,384.,1,true,1093677098,O)
call kE(9,10.,m4,3.,1227894834,1,512.,1,true,1093677137,K)
call kE(4,5.,0,.5,1227894832,0,384.,1,false,1093677126,M)
call ow(y4,m4,0)
call ra(1,true,3,3,3.,0)
call ra(2,true,1,1,3.,0)
call ra(3,true,1,1,3.,0)
call ra(4,true,1,1,.5,0)
call ra(5,true,1,1,.5,0)
call ra(6,false,1,1,3.,0)
call ra(Z4,false,1,1,3.,0)
call ra(7,false,1,1,1.5,0)
call ra(W4,false,1,1,2.,0)
call ra(8,false,1,1,2.,0)
call ra(9,false,1,1,1.5,1093677120)
call ra(V4,false,1,1,1./ 16.,0)
call ra(X4,false,1,99,5.,0)
call ra(Y4,false,1,1,3.,1093677088)
call ra(d7,false,1,1,2.,0)
call ra(16,false,1,1,3.,1093677088)
call ra(17,false,5,5,5.,1093677092)
call Qb(1,1096888368,false,false,false,.0,.0,.0)
call Qb(2,1096888369,true,false,false,.0,5.,.0)
call Qb(3,1096888370,false,false,true,.05,.0,256.)
call Qb(4,1096888371,true,true,false,.0,5.,512.)
call Qb(5,1096888372,false,false,true,.05,.0,256.)
call Qb(6,1096888373,true,true,false,10.,10.,512.)
call Qb(7,1096888374,true,false,false,10.,10.,.0)
call Qb(8,1096888375,true,false,false,10.,10.,512.)
call Wb(1,function Na,function Pa)
call Wb(2,null,function Qa)
call Wb(3,function Ua,function Wa)
call Wb(4,null,function Ma)
call Wb(5,null,function Ra)
call Wb(6,null,function dB)
call Wb(7,null,function fB)
call Wb(8,null,function mB)
call mD(1,100,1.,1,$A,5,25,5,5,-1,0,100.,c4,y4,1.,0,0)
call mD(2,$DC,2.,2,$A,$A,55,$A,$A,-1,1,210.,c4,y4,1.,$F,0)
call mD(3,440,3.,3,5,20,110,$F,$F,-2,1,390.,c4,y4,1.25,25,$A)
call mD(5,440,3.,3,5,20,110,$F,$F,-3,2,400.,c4,y4,1.,25,0)
call mD(6,0,.0,0,1,0,0,0,0,-5,3,1000.,c4,y4,1.,0,0)
call mD(4,400,2.,2,5,40,40,40,0,-1,1,100.,c4,y4,1.,0,0)
call BD(1,1831874608,1097674800,1097412656,1,true,false,60.,function zb)
call BD(2,1831874609,1097674801,1097412657,2,true,false,60.,function Eb)
call BD(3,1831874610,1097674802,1097412658,5,true,false,60.,function Gb)
call BD(4,1831874611,1097674803,1097412659,4,true,false,60.,function wb)
call BD(5,1831874612,1097674804,1097412660,3,true,false,60.,function Jb)
call BD(6,1831874613,0,1093677139,0,false,true,60.,function Mb)
set n7[0]=wr()
call lD(1,1096953904,1097019440,1097084976,1378889776,$FA,function ab,1096953905,1097019441,1097084977,1378889777,$FA,function Bb,1096953906,1097019442,1097084978,1378889778,$3E8,function bb)
call lD(2,1096953907,1097019443,1097084979,1378889779,500,function Fb,1096953908,1097019444,1097084980,1378889780,500,null,1096953909,1097019445,1097084981,1378889781,$3E8,null)
call lD(3,1096953910,1097019446,1097084982,1378889782,500,function Hb,1096953911,1097019447,1097084983,1378889783,$3E8,function Ib,1096953912,1097019448,1097084984,1378889784,500,function lb)
call lD(5,1096953913,1097019449,1097084985,1378889785,500,function Kb,1096953889,1097019425,1097084961,1378889761,500,function Lb,1096953915,1097019451,1097084987,1378889787,$3E8,null)
call lD(6,1096953914,1097019450,1097084986,1378889786,$FA,function Nb,1096953919,1097019455,1097084991,1378889791,$FA,function Ob,1096953920,1097019456,1097084992,1378889792,$FA,function Pb)
call lD(4,1096953916,1097019452,1097084988,1378889788,0,null,1096953917,1097019453,1097084989,1378889789,0,function xb,1096953918,1097019454,1097084990,1378889790,0,null)
call Kc(1,6,1097150512,1097216048,1097281584,null)
call Kc(2,7,1097150513,1097216049,1097281585,null)
call Kc(3,8,1097150514,1097216050,1097281586,null)
call Pc(1,1097347120,1097478192,1097543728,1382101040,$FA,function eB,1097347121,1097478193,1097543729,1382101041,500,null)
call Pc(2,1097347122,1097478194,1097543730,1382101042,$FA,null,1097347123,1097478195,1097543731,1382101043,500,null)
call Pc(3,1097347124,1097478196,1097543732,1382101044,$FA,function sB,1097347125,1097478197,1097543733,1382101045,500,null)
call pE(1,6,1,2,3,1093677365,1093677366,1093677367,null)
call bv(1,1,2,3,5,1,0,1093677132,1093677360,null)
call bv(2,0,0,0,4,0,0,0,0,null)
call SetGameSpeed(MAP_SPEED_FASTEST)
call SetMapFlag(MAP_LOCK_SPEED,true)
call SetMapFlag(MAP_USE_HANDICAPS,false)
call SetAllyColorFilterState(0)
call SetCreepCampFilterState(false)
call EnableMinimapFilterButtons(false,false)
call EnableWorldFogBoundary(false)
call SetFloatGameState(GAME_STATE_TIME_OF_DAY,12.)
call SuspendTimeOfDay(true)
call SetMapMusic("Music",true,0)
call TriggerExecute(D8)
endfunction
function config takes nothing returns nothing
local player p
call SetMapName("TRIGSTR_001")
call SetMapDescription("TRIGSTR_904")
call SetPlayers(Y)
call SetTeams(Y)
set p=Player(0)
call DefineStartLocation(0,C8,c8)
call SetPlayerStartLocation(p,0)
call SetPlayerColor(p,ConvertPlayerColor(0))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,0)
set p=Player(1)
call DefineStartLocation(1,C8,c8)
call SetPlayerStartLocation(p,1)
call SetPlayerColor(p,ConvertPlayerColor(1))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(2)
call DefineStartLocation(2,C8,c8)
call SetPlayerStartLocation(p,2)
call SetPlayerColor(p,ConvertPlayerColor(2))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(3)
call DefineStartLocation(3,C8,c8)
call SetPlayerStartLocation(p,3)
call SetPlayerColor(p,ConvertPlayerColor(3))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(4)
call DefineStartLocation(4,C8,c8)
call SetPlayerStartLocation(p,4)
call SetPlayerColor(p,ConvertPlayerColor(4))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(5)
call DefineStartLocation(5,C8,c8)
call SetPlayerStartLocation(p,5)
call SetPlayerColor(p,ConvertPlayerColor(5))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(6)
call DefineStartLocation(6,C8,c8)
call SetPlayerStartLocation(p,6)
call SetPlayerColor(p,ConvertPlayerColor(6))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(7)
call DefineStartLocation(7,C8,c8)
call SetPlayerStartLocation(p,7)
call SetPlayerColor(p,ConvertPlayerColor(7))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(8)
call DefineStartLocation(8,C8,c8)
call SetPlayerStartLocation(p,8)
call SetPlayerColor(p,ConvertPlayerColor(8))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(9)
call DefineStartLocation(9,C8,c8)
call SetPlayerStartLocation(p,9)
call SetPlayerColor(p,ConvertPlayerColor(9))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=null
endfunction
function M3 takes nothing returns boolean
local integer m=Jq
local integer T=Kq
local integer vr=Gr()
set Mk[vr]=(T)
set Nk[vr]=m
set sk[m]=vr
call UnitAddAbility(Pj[m],nn[Mk[vr]])
set lq=vr
return true
endfunction
function N3 takes nothing returns boolean
local integer vr=Pq
return true
endfunction
function O3 takes nothing returns boolean
local integer vr=Pq
local integer a=0
set a=0
loop
call Rv(ee[fe[vr]+a])
set a=a+1
exitwhen a==608
endloop
call ju("Neighbors are ready!")
return true
endfunction
function P3 takes nothing returns boolean
local integer vr=Pq
local integer a=0
loop
set ee[fe[vr]+a]=Qv(vr,a)
set a=a+1
exitwhen a==608
endloop
call Nr(vr)
call ju("Cells are ready!")
return true
endfunction
function Q3 takes nothing returns boolean
local integer vr=Pq
local integer a=0
loop
call Kr(ee[fe[vr]+a])
set a=a+1
exitwhen a==608
endloop
call ju("Cells destroyed!")
return true
endfunction
function R3 takes nothing returns boolean
local integer vr=Pq
local integer a=0
local boolean S3=ue[vr]
local integer c
loop
set c=ee[fe[vr]+a]
set Ld[(c)]=true
if S3 then
set Gd[(c)]=(Hd[c])
set Hd[(c)]=(0)
endif
set a=a+1
exitwhen a==w4
endloop
return true
endfunction
function T3 takes nothing returns boolean
local integer vr=Pq
local integer q=Jq
local integer c
local integer d
set ve[vr]=true
loop
set c=Tv(q)
set d=Hd[c]+1
call Wv(q,Md[c],d)
call Wv(q,Nd[c],d)
call Wv(q,Od[c],d)
call Wv(q,Pd[c],d)
exitwhen(Vd[(q)]==0)
endloop
set ue[vr]=not Ld[ee[fe[vr]]]
set ve[vr]=false
return true
endfunction
function U3 takes nothing returns boolean
local integer vr=Pq
set Qq=Xo[dk[vr]]*(td[ud[(Yo[dk[vr]])]+(kk[vr])])
return true
endfunction
function V3 takes nothing returns boolean
local integer vr=Pq
set Qq=.0
return true
endfunction
function W3 takes nothing returns boolean
local integer vr=Pq
set Qq=.0
return true
endfunction
function X3 takes nothing returns boolean
local integer vr=Pq
return true
endfunction
function Y3 takes nothing returns boolean
local integer vr=Pq
local integer m=Jq
call lB(vr,m)
return true
endfunction
function Z3 takes nothing returns boolean
local integer vr=Pq
local integer ks=Jq
if KB(vr,ks,ek[vr])then
call JB(vr)
else
call TimerStart(fk[(vr)],S4,false,uk)
endif
return true
endfunction
function dF takes nothing returns boolean
local integer vr=Pq
local integer m=Jq
set hk[vr]=hk[vr]+1
call SetItemCharges(pk[vr],hk[vr])
call Jx(Oj[vr])
if qo[vh[m]]>0 then
call VB(m,vr)
endif
return true
endfunction
function eF takes nothing returns boolean
local integer vr=Pq
if pp[Rj[vr]]then
call ShowImage(gk[vr],true)
endif
return true
endfunction
function fF takes nothing returns boolean
local integer vr=Pq
if pp[Rj[vr]]then
call ShowImage(gk[vr],false)
endif
return true
endfunction
function gF takes nothing returns boolean
local integer vr=Pq
local integer vw=Pe[vr]
local integer Bw
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=Re[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=Ue[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=Ye[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=ff[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=kf[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=of[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=rf[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=uf[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=yf[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=Bf[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
set vw=Df[vr]
loop
exitwhen vw==0
set Bw=He[vw]
call dt(Ee[vw])
set vw=Bw
endloop
call ju("MinionBuffStorage "+I2S(vr)+" is deleted.")
return true
endfunction
function hF takes nothing returns boolean
local player p=Mq
local integer i=Jq
local integer vr=ws()
local integer a
set Jf[vr]=p
set Pf[vr]=p==F
set Lf[vr]=GetPlayerName(p)
call yx(vr)
set Kf[vr]=Vu(vr)
call Xu(vr)
set B4[i]=vr
set Mf[vr]=sv(GetPlayerColor(p))
set b4[Mf[vr]]=true
if GetPlayerColor(p)==e4 then
set C4=p
endif
set Sf[vr]=Zv(vr)
set yg[vr]=zr()
set zg[vr]=Ar()
set Ag[vr]=ar()
set ag[vr]=Br()
set Bg[vr]=Cr()
set bg[vr]=br()
set a=0
loop
call SetPlayerTechMaxAllowed(p,1949315120+a,tp[(a)])
set a=a+1
exitwhen a==o7
endloop
set a=1
loop
set Ig[lg[vr]+a]=sx(a)
set a=a+1
exitwhen a==32
endloop
set a=1
loop
set Gg[Hg[vr]+a]=ux(a)
set a=a+1
exitwhen a==32
endloop
set a=1
loop
set cg[Dg[vr]+a]=vx(a)
call SetPlayerAbilityAvailable(p,Eo[Fo[n7[a]]],false)
call SetPlayerAbilityAvailable(p,Eo[Fo[n7[a]]+1],false)
call SetPlayerAbilityAvailable(p,Eo[Fo[n7[a]]+2],false)
set a=a+1
exitwhen a==32
endloop
set a=1
loop
set Kg[Lg[vr]+a]=cr()
call SetPlayerAbilityAvailable(p,Hn[j7[a]],false)
call SetPlayerAbilityAvailable(p,Gn[j7[a]],false)
call SetPlayerAbilityAvailable(p,Fn[j7[a]],false)
call SetPlayerAbilityAvailable(p,Mn[Nn[j7[a]]],false)
call SetPlayerAbilityAvailable(p,Mn[Nn[j7[a]]+1],false)
call SetPlayerAbilityAvailable(p,Kn[Ln[j7[a]]],false)
call SetPlayerAbilityAvailable(p,Kn[Ln[j7[a]]+1],false)
set a=a+1
exitwhen a==24
endloop
call TriggerRegisterPlayerUnitEvent(e8,p,EVENT_PLAYER_UNIT_CONSTRUCT_START,null)
call TriggerRegisterPlayerUnitEvent(f8,p,EVENT_PLAYER_UNIT_CONSTRUCT_CANCEL,null)
call TriggerRegisterPlayerUnitEvent(g8,p,EVENT_PLAYER_UNIT_CONSTRUCT_FINISH,null)
call TriggerRegisterPlayerUnitEvent(n8,p,EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerRegisterPlayerUnitEvent(o8,p,EVENT_PLAYER_UNIT_SPELL_ENDCAST,null)
call TriggerRegisterPlayerUnitEvent(p8,p,EVENT_PLAYER_UNIT_ISSUED_ORDER,null)
call TriggerRegisterPlayerUnitEvent(q8,p,EVENT_PLAYER_UNIT_ISSUED_POINT_ORDER,null)
call TriggerRegisterPlayerUnitEvent(h8,p,EVENT_PLAYER_UNIT_UPGRADE_START,null)
call TriggerRegisterPlayerUnitEvent(i8,p,EVENT_PLAYER_UNIT_UPGRADE_CANCEL,null)
call TriggerRegisterPlayerUnitEvent(j8,p,EVENT_PLAYER_UNIT_UPGRADE_FINISH,null)
call TriggerRegisterPlayerUnitEvent(r8,p,EVENT_PLAYER_UNIT_SELECTED,null)
call TriggerRegisterPlayerUnitEvent(s8,p,EVENT_PLAYER_UNIT_DESELECTED,null)
call TriggerRegisterPlayerEvent(d8,p,EVENT_PLAYER_LEAVE)
set lq=vr
return true
endfunction
function iF takes nothing returns boolean
local integer vr=Pq
local integer k=0
local real x=je[Sf[vr]]
local real y=320.
set Qf[vr]=Z[Kf[vr]*2/(d9+1)]
call fw(Sf[vr])
loop
set qg[rg[vr]+k]=bu(Jf[vr],"Textures\\Pavement.blp",256.,x,y,4)
set x=x+128.
if x>ge[Sf[vr]]then
set y=y-128.
set x=je[Sf[vr]]
endif
set k=k+1
exitwhen k==120
endloop
set x=(he[Sf[vr]]+ge[Sf[vr]])/ 2.
set y=(q4+1152.)/ 2.
set tg[vr]=CreateUnit(Jf[vr],1112294482,x,y,.0)
set k=Kf[vr]
call SetHeroXP(tg[vr],k,false)
if Pf[vr]then
call PanCameraToTimed(x,y,.0)
call SelectUnit(tg[vr],true)
endif
set y=1920.-64.
set ug[vr]=CreateUnit(Jf[vr],1112689491,x-128.,y,270.)
call SetUnitVertexColor(ug[vr],$FF,$FF,$FF,64)
call SetHeroXP(ug[vr],k,false)
set vg[vr]=CreateUnit(Jf[vr],1112232788,x-384.,y,270.)
call SetUnitVertexColor(vg[vr],$FF,$FF,$FF,64)
call SetHeroXP(vg[vr],k,false)
set wg[vr]=CreateUnit(Jf[vr],1095521362,x+128.,y,270.)
call SetUnitVertexColor(wg[vr],$FF,$FF,$FF,64)
call SetHeroXP(wg[vr],k,false)
set xg[vr]=CreateUnit(Jf[vr],1296256336,x+384.,y,270.)
call SetUnitVertexColor(xg[vr],$FF,$FF,$FF,64)
call SetHeroXP(xg[vr],k,false)
set Vg[vr]=xv(vg[vr],1227894850)
set Wg[vr]=xv(vg[vr],1227894851)
call SetItemCharges(Wg[vr],R4)
call wx(vr)
set Rf[vr]=true
call ju("Player "+I2S(vr)+" is ready.")
return true
endfunction
function jF takes nothing returns boolean
local integer vr=Pq
local integer a=0
loop
call DestroyImage(qg[rg[vr]+a])
set qg[rg[vr]+a]=null
set a=a+1
exitwhen a==120
endloop
return true
endfunction
function kF takes nothing returns boolean
local integer vr=Pq
call Ha(vr)
call ju("MinionAbility "+I2S(vr)+" is deleted.")
return true
endfunction
function mF takes nothing returns boolean
local integer vr=Pq
return true
endfunction
function nF takes nothing returns boolean
local integer vr=Pq
local integer zs=Jq
return true
endfunction
function oF takes nothing returns boolean
local integer vr=Pq
local integer zs=Jq
local integer as=Kq
local integer Bs=Lq
set Rq=false
return true
endfunction
function pF takes nothing returns boolean
local integer vr=Pq
local real r=Nq
if r>.0 and not(Wi[vr])then
set r=TimerGetRemaining(Xi[vr])-r
if r<=.0 then
call TimerStart((Xi[vr]),.0,false,null)
call xs(vr)
else
call TimerStart(Xi[vr],r,false,function Ia)
endif
endif
return true
endfunction
function qF takes nothing returns boolean
local integer vr=Pq
return true
endfunction
function rF takes nothing returns boolean
local integer vr=Pq
return true
endfunction
function sF takes nothing returns boolean
local integer vr=Pq
call Ca(vr)
call DestroyImage(Oi[vr])
set Oi[vr]=null
set Ni[vr]=null
return true
endfunction
function tF takes nothing returns boolean
local integer vr=Pq
call RemoveUnit(Gi[vr])
set Gi[vr]=null
call ju("MinionDummy "+I2S(vr)+" is deleted.")
return true
endfunction
function uF takes nothing returns boolean
local integer vr=Pq
if ph[vr]!=0 then
call Es(ph[vr])
endif
call SetUnitUserData(mh[vr],0)
set mh[vr]=null
return true
endfunction
function vF takes nothing returns boolean
local integer vr=Pq
call iv(zg[ih[vr]],vr)
call ax(ih[vr],hh[vr])
call Vx(ih[vr],jo[vh[vr]])
call Kx(ih[vr],-no[vh[vr]])
call Mx(ih[vr],io[vh[vr]])
if io[vh[vr]]>0 then
call Qx(ih[vr],I2S(io[vh[vr]]),mh[vr])
endif
call DestroyEffect(AddSpecialEffect(("Abilities\\Spells\\Other\\Transmute\\PileofGold.mdx"),((GetUnitX(mh[vr]))*1.),((GetUnitY(mh[vr]))*1.)))
call RemoveUnit(mh[vr])
call Xs(vr)
call ju("Minion "+I2S(vr)+" is sold.")
return true
endfunction
function wF takes nothing returns boolean
local integer vr=Pq
set Qq=xo[vh[vr]]
return true
endfunction
function xF takes nothing returns boolean
local integer vr=Pq
set lq=yo[vh[vr]]
return true
endfunction
function yF takes nothing returns boolean
local integer vr=Pq
set lq=zo[vh[vr]]
return true
endfunction
function zF takes nothing returns boolean
local integer vr=Pq
set Rq=(jf[oh[(vr)]])
return true
endfunction
function AF takes nothing returns boolean
local integer vr=Pq
set Rq=false
return true
endfunction
function aF takes nothing returns boolean
local integer vr=Pq
set Rq=false
return true
endfunction
function BF takes nothing returns boolean
local integer vr=Pq
set Rq=false
return true
endfunction
function bF takes nothing returns boolean
local integer vr=Pq
set Rq=false
return true
endfunction
function CF takes nothing returns boolean
local integer vr=Pq
local real Rs=Nq
set Qq=Az(vr,Rs)
return true
endfunction
function cF takes nothing returns boolean
local integer vr=Pq
local real Rs=Nq
set Qq=az(vr,Rs)
return true
endfunction
function DF takes nothing returns boolean
local integer vr=Pq
local real Rs=Nq
set Qq=(th[(vr)]*((Rs)*1.)/ 1000.)
return true
endfunction
function EF takes nothing returns boolean
local integer vr=Pq
local boolean Vs=Oq
call SetUnitExploded(mh[vr],Vs)
call KillUnit(mh[vr])
if Vs or go[vh[vr]]then
call Gz(vr)
else
set qh[vr]=false
call Fz(vr)
call ov(bg[jh[vr]],vr)
call Cs(ph[vr])
set ch[vr]=GetUnitX(mh[vr])
set Dh[vr]=GetUnitY(mh[vr])
set Eh[vr]=GetUnitFacing(mh[vr])
call TimerStart(Hh[vr],15.,false,function Nz)
endif
return true
endfunction
function FF takes nothing returns boolean
local integer vr=Pq
call hA(vr)
call RemoveSavedInteger(r7,nh[Mh[vr]],Lh[vr])
call UnitRemoveAbility(mh[Mh[vr]],ci[Ph[vr]])
set Nh[vr]=null
return true
endfunction
function GF takes nothing returns boolean
local integer vr=Pq
set Rq=true
return true
endfunction
function HF takes nothing returns boolean
local integer vr=Pq
call tz(Mh[vr],Sh[vr])
if ui[vr]then
call lw(oh[(Mh[vr])],(Th[vr]))
endif
set Pq=vr
return true
endfunction
function IF takes nothing returns boolean
local integer vr=Pq
call ww(oh[(Mh[vr])],(Sh[vr]))
set Pq=vr
return true
endfunction
function lF takes nothing returns boolean
local integer vr=Pq
call ly(Mh[vr],yj[Kf[Oh[vr]]],"Effects\\RestoreHealthGreen.mdx","origin")
set Rq=Qh[vr]==0
return true
endfunction
function JF takes nothing returns boolean
local integer vr=Pq
call wz(Mh[vr],Sh[vr])
call tz(Mh[vr],Th[vr])
set Pq=vr
return true
endfunction
function KF takes nothing returns boolean
local integer vr=Pq
call Uw(oh[(Mh[vr])],(Sh[vr]))
if ti[vr]then
call lw(oh[(Mh[vr])],(Th[vr]))
endif
call AddUnitAnimationProperties(mh[Mh[vr]],"Defend",false)
set Pq=vr
return true
endfunction
function LF takes nothing returns boolean
local integer vr=Pq
if Ao[ao[cg[Dg[Oh[vr]]+2]]+2]then
call WA(Mh[vr],Ke[(Sh[vr])]*.5)
endif
set Rq=true
return true
endfunction
function MF takes nothing returns boolean
local integer vr=Pq
call sz(Mh[vr],Sh[vr])
set Pq=vr
return true
endfunction
function NF takes nothing returns boolean
local integer vr=Pq
call ex(oh[(Mh[vr])],(Sh[vr]))
set Pq=vr
return true
endfunction
function OF takes nothing returns boolean
local integer vr=Pq
local integer tx=cg[Dg[Oh[vr]]+4]
if Ao[ao[tx]+2]then
call Hy(Mh[vr],Ke[(Sh[vr])]*.5,"Effects\\RestoreHealthGreen.mdx","origin")
endif
if Ao[ao[tx]+0]then
call QA(Mh[vr],Nh[vr],Oh[vr],40)
endif
set Rq=true
return true
endfunction
function PF takes nothing returns boolean
local integer vr=Pq
call vz(Mh[vr],Sh[vr])
set Pq=vr
return true
endfunction
function QF takes nothing returns boolean
local integer vr=Pq
call RemoveUnit(Zh[vr])
set Zh[vr]=null
call ju("TowerAttacker "+I2S(vr)+" is deleted.")
return true
endfunction
function RF takes nothing returns boolean
local integer vr=Pq
call xz(Mh[vr],Sh[vr])
set Pq=vr
return true
endfunction
function SF takes nothing returns boolean
local integer vr=Pq
if(Q9[R9[(Kg[Lg[Oh[vr]]+2])]+(0)])then
call ja(Mh[vr],.15)
endif
set Rq=true
return true
endfunction
function TF takes nothing returns boolean
local integer vr=Pq
call xz(Mh[vr],Sh[vr])
set Pq=vr
return true
endfunction
function UF takes nothing returns boolean
local integer vr=Pq
call ww(oh[(Mh[vr])],(Sh[vr]))
set Pq=vr
return true
endfunction
function VF takes nothing returns boolean
local integer vr=Pq
if UnitAlive(Nh[vr])then
call bs(ph[(GetUnitUserData(Nh[vr]))],1.)
set Rq=Qh[vr]==0
return true
endif
set Rq=true
return true
endfunction
function WF takes nothing returns boolean
local integer vr=Pq
local integer zs=Jq
local integer as=Kq
local integer Bs=Lq
if ih[zs]==ih[Ui[vr]]then
call TA(zs,Ui[vr])
call Ka(vr)
set Rq=true
return true
endif
set Rq=false
return true
endfunction
function XF takes nothing returns boolean
local integer vr=Pq
local integer zs=Jq
local integer as=Kq
local integer Bs=Lq
if Bs==1227894834 then
call YA(zs)
call Ka(vr)
set Rq=true
return true
endif
set Rq=false
return true
endfunction
function YF takes nothing returns boolean
local integer vr=Pq
if hj[vr]!=0 then
call Fa(hj[vr])
endif
set Pq=vr
return true
endfunction
function ZF takes nothing returns boolean
local integer vr=Pq
local integer m=Ui[vr]
set jj=m
set kj=false
call GroupEnumUnitsInRange(bj_lastCreatedGroup,GetUnitX(mh[m]),GetUnitY(mh[m]),Ik[Vi[vr]],gj)
if Ao[ao[vh[m]]+1]then
set ij[vr]=tu(kj)*Ak
call Vy(m)
endif
return true
endfunction
function dG takes nothing returns boolean
local integer vr=Pq
call Fa(hj[vr])
set hj[vr]=0
call PauseTimer(Xi[vr])
return true
endfunction
function eG takes nothing returns boolean
local integer vr=Pq
set hj[vr]=Da(mh[Ui[vr]],Ik[Vi[vr]])
call Ka(vr)
return true
endfunction
function fG takes nothing returns boolean
local integer vr=Pq
if uj[vr]!=0 then
call Fa(uj[vr])
endif
set Pq=vr
return true
endfunction
function gG takes nothing returns boolean
local integer vr=Pq
local integer m=Ui[vr]
local integer hG=Kf[ih[m]]
local integer Py
set vj=ih[m]
set wj=0
call GroupEnumUnitsInRange(bj_lastCreatedGroup,GetUnitX(mh[m]),GetUnitY(mh[m]),Ik[Vi[vr]],pj)
set Py=wj*nj[hG]
if Py>oj[hG]then
set Py=oj[hG]
endif
set tj[vr]=Py
set sj[vr]=Py
call Vy(m)
return true
endfunction
function iG takes nothing returns boolean
local integer vr=Pq
if qj[vr]then
call AddUnitAnimationProperties(mh[Ui[vr]],"Defend",false)
else
call Fa(uj[vr])
set uj[vr]=0
call PauseTimer(Xi[vr])
endif
return true
endfunction
function jG takes nothing returns boolean
local integer vr=Pq
if qj[vr]then
call AddUnitAnimationProperties(mh[Ui[vr]],"Defend",true)
else
set uj[vr]=Da(mh[Ui[vr]],Ik[Vi[vr]])
call Ka(vr)
endif
return true
endfunction
function kG takes nothing returns boolean
local integer vr=Pq
call Ba(zj[vr])
set Pq=vr
return true
endfunction
function mG takes nothing returns boolean
local integer vr=Pq
local integer zs=Jq
local integer as=Kq
local integer Bs=Lq
if zs!=Ui[vr]and ih[zs]==ih[Ui[vr]]and by(zs)<=(1.-yj[Kf[ih[Ui[vr]]]])then
call za(zj[vr],GetUnitX(mh[Ui[vr]]),GetUnitY(mh[Ui[vr]]))
call Aa(zj[vr],GetUnitX(mh[zs]),GetUnitY(mh[zs]))
call Ka(vr)
set Rq=true
return true
endif
set Rq=false
return true
endfunction
function nG takes nothing returns boolean
local integer vr=Pq
local integer zs=Jq
local integer p=ih[Ui[vr]]
if zs!=Ui[vr]and ih[zs]==p then
call ly(zs,yj[Kf[ih[Ui[vr]]]],"Effects\\RestoreHealthGreen.mdx","origin")
if(Q9[R9[(Kg[Lg[p]+1])]+(1)])then
call ga(zs,Ui[vr])
endif
endif
return true
endfunction
function oG takes nothing returns boolean
local integer vr=Pq
local integer zs=Jq
local integer as=Kq
local integer Bs=Lq
call ma(zs)
if(Q9[R9[(Kg[Lg[ih[zs]]+2])]+(1)])then
set zs=hB(vr)
if zs!=0 then
call oa(zs,Ui[vr])
endif
endif
call Ka(vr)
set Rq=true
return true
endfunction
function pG takes nothing returns boolean
local integer vr=Pq
call kB(vr)
set Pq=vr
return true
endfunction
function qG takes nothing returns boolean
local integer vr=Pq
call oB(vr)
return true
endfunction
function rG takes nothing returns boolean
local integer vr=Pq
call ww(oh[(Mh[vr])],(Sh[vr]))
set Pq=vr
return true
endfunction
function sG takes nothing returns boolean
local integer vr=Pq
local integer dd
if(GetUnitTypeId((Nh[vr]))!=0)then
set dd=GetUnitUserData(Nh[vr])
set Rq=FB(dd,Mh[vr],Zk[(sk[dd])],1227894841,false)and Qh[vr]==0
return true
endif
set Rq=true
return true
endfunction
function tG takes nothing returns boolean
local integer vr=Pq
call tz(Mh[vr],Sh[vr])
set Pq=vr
return true
endfunction
function uG takes nothing returns boolean
local integer vr=Pq
call wz(Mh[vr],Sh[vr])
set Pq=vr
return true
endfunction
function vG takes nothing returns boolean
local integer vr=Pq
call tz(Mh[vr],Sh[vr])
set Pq=vr
return true
endfunction
function wG takes nothing returns boolean
local integer vr=Pq
call uz(Mh[vr],Sh[vr])
set Pq=vr
return true
endfunction
function xG takes nothing returns boolean
local integer vr=Pq
call wz(Mh[vr],Sh[vr])
set Pq=vr
return true
endfunction
function yG takes nothing returns boolean
local integer vr=Pq
call sz(Mh[vr],Sh[vr])
set Pq=vr
return true
endfunction
function zG takes nothing returns boolean
local integer vr=Pq
set Qq=(xo[vh[(vr)]])+ej[(ph[vr])]
return true
endfunction
function AG takes nothing returns boolean
local integer vr=Pq
set lq=(yo[vh[(vr)]])+dj[(ph[vr])]
return true
endfunction
function aG takes nothing returns boolean
local integer vr=Pq
if Ao[ao[vh[vr]]+2]then
call Ab(zk[vr])
endif
call vy(vr)
return true
endfunction
function BG takes nothing returns boolean
local integer vr=Pq
set lq=(yo[vh[(vr)]])+ij[(ph[vr])]
return true
endfunction
function bG takes nothing returns boolean
local integer vr=Pq
set lq=(zo[vh[(vr)]])+ij[(ph[vr])]
return true
endfunction
function CG takes nothing returns boolean
local integer vr=Pq
set Qq=(xo[vh[(vr)]])+rj[(ph[vr])]
return true
endfunction
function cG takes nothing returns boolean
local integer vr=Pq
set lq=(yo[vh[(vr)]])+tj[(ph[vr])]
return true
endfunction
function DG takes nothing returns boolean
local integer vr=Pq
set lq=(zo[vh[(vr)]])+sj[(ph[vr])]
return true
endfunction
function EG takes nothing returns boolean
local integer vr=Pq
set Rq=(jf[oh[((vr))]])or(Ao[ao[vh[vr]]+2]and qj[(ph[vr])])
return true
endfunction
function FG takes nothing returns boolean
local integer vr=Pq
set Rq=qj[(ph[vr])]
return true
endfunction
function GG takes nothing returns boolean
local integer vr=Pq
local real Rs=Nq
if qj[(ph[vr])]then
set Qq=.0
return true
endif
set Qq=Az(vr,Rs)
return true
endfunction
function HG takes nothing returns boolean
local integer vr=Pq
call eC(vr)
set Qk[vr]=null
set Pq=vr
return true
endfunction
function IG takes nothing returns boolean
local integer vr=Pq
if kk[Nk[vr]]==on[Mk[vr]]then
set Tk[vr]=Tk[vr]+1.
elseif kk[Nk[vr]]==pn[Mk[vr]]then
set Uk[vr]=Uk[vr]+3.
endif
return true
endfunction
function lG takes nothing returns boolean
local integer vr=Pq
set Zk[vr]=Zk[vr]+Zk[vr]*.075
return true
endfunction
function JG takes nothing returns boolean
local integer vr=Pq
if em[vr]!=0 then
call BA(em[vr])
endif
set Pq=vr
return true
endfunction
function KG takes nothing returns boolean
local integer vr=Pq
if kk[Nk[vr]]==on[Mk[vr]]then
call yA(ek[Nk[vr]])
elseif kk[Nk[vr]]==pn[Mk[vr]]then
set em[vr]=tA(Nk[vr])
endif
return true
endfunction
function LG takes nothing returns boolean
local integer vr=Pq
call BA(fm[vr])
call RemoveUnit(gm[vr])
call sC(vr)
set gm[vr]=null
set hm[vr]=null
set mm[vr]=null
set Pq=vr
return true
endfunction
function MG takes nothing returns boolean
local integer vr=Pq
call AC(vr)
set Pq=vr
return true
endfunction
function NG takes nothing returns boolean
local integer vr=Pq
set om[vr]=om[vr]+1
if kk[Nk[vr]]==on[Mk[vr]]then
set om[vr]=om[vr]+1
elseif kk[Nk[vr]]==pn[Mk[vr]]then
set om[vr]=om[vr]+3
endif
return true
endfunction
function OG takes nothing returns boolean
local integer vr=Pq
call CC(vr)
call DestroyImage(zm[vr])
set zm[vr]=null
set Bm[vr]=null
call EC(vr)
set Pq=vr
return true
endfunction
function PG takes nothing returns boolean
local integer vr=Pq
if kk[Nk[vr]]==on[Mk[vr]]then
set ym[vr]=ym[vr]*2.
set am[vr]=am[vr]*2.
set cm[vr]=cm[vr]+5
elseif kk[Nk[vr]]==pn[Mk[vr]]then
set Am[vr]=Am[vr]*2
set cm[vr]=cm[vr]+5
elseif kk[Nk[vr]]==qn[Mk[vr]]and TimerGetRemaining(Em[vr])>=3. then
call TimerStart(Em[vr],3.,true,wm)
endif
return true
endfunction
function QG takes nothing returns boolean
local integer vr=Pq
call ZC(vr)
call ec(vr)
set Pq=vr
return true
endfunction
function RG takes nothing returns boolean
local integer vr=Pq
call nc(vr)
set jn[vr]=null
set Pq=vr
return true
endfunction
function SG takes nothing returns boolean
local integer vr=Pq
set en[vr]=en[vr]+.01
set fn[vr]=fn[vr]+Zm
return true
endfunction
function TG takes nothing returns boolean
local integer vr=Pq
set Qq=gC(sk[vr])+cB(vr)
return true
endfunction
function UG takes nothing returns boolean
local integer vr=Pq
local integer m=Jq
call lB(vr,m)
call jC(sk[vr])
return true
endfunction
function VG takes nothing returns boolean
local integer vr=Pq
call EB(vr)
if ou(kk[vr],5)then
set Zj[vr]=Zj[vr]+1
endif
return true
endfunction
function WG takes nothing returns boolean
local integer vr=Pq
local integer m=Jq
if lB(vr,m)then
call oC(sk[vr],m)
endif
return true
endfunction
function XG takes nothing returns boolean
local integer vr=Pq
local integer m=Jq
if lB(vr,m)then
call pb((m),Nk[(sk[vr])],dm)
endif
return true
endfunction
function YG takes nothing returns boolean
local integer vr=Pq
local integer ks=Jq
if qC(sk[vr],ks)then
call JB(vr)
else
call TimerStart(fk[(vr)],S4,false,uk)
endif
return true
endfunction
function ZG takes nothing returns boolean
local integer vr=Pq
set Qq=Xo[dk[vr]]+.5*kk[vr]
return true
endfunction
function d6 takes nothing returns boolean
local integer vr=Pq
set Qq=jm[(sk[vr])]+cB(vr)
return true
endfunction
function e6 takes nothing returns boolean
local integer vr=Pq
local integer m=Jq
call yC(sk[vr],m)
return true
endfunction
function f6 takes nothing returns boolean
local integer vr=Pq
call OC(sk[vr])
return true
endfunction
function g6 takes nothing returns boolean
local integer vr=Pq
call PC(sk[vr])
return true
endfunction
function h6 takes nothing returns boolean
local integer vr=Pq
set Qq=Nm[(sk[vr])]+CB(vr)
return true
endfunction
function i6 takes nothing returns boolean
local integer vr=Pq
set Qq=Mm[(sk[vr])]+cB(vr)
return true
endfunction
function j6 takes nothing returns boolean
local integer vr=Pq
local integer m=Jq
call WB(vr,m)
call WC(sk[vr])
return true
endfunction
function k6 takes nothing returns boolean
local integer vr=Pq
local integer m=Jq
call XC(sk[vr],m)
return true
endfunction
function m6 takes nothing returns boolean
local integer vr=Pq
set Qq=Sm[(sk[vr])]+cB(vr)
return true
endfunction
function n6 takes nothing returns boolean
local integer vr=Pq
local integer m=Jq
call lB(vr,m)
call gc(sk[vr])
return true
endfunction
function o6 takes nothing returns boolean
local integer vr=Pq
local integer ks=Jq
local integer m=EA(ks)
call hc(sk[vr],GetUnitX(mh[m]),GetUnitY(mh[m]))
call JB(vr)
return true
endfunction
function p6 takes nothing returns boolean
local integer q6=Jq
local integer as=Kq
local integer Bs=Lq
local boolean r6=not zz(q6,Bs)
local integer a
local integer tc
local integer s6
if r6 then
set r6=La(ph[q6],q6,as,Bs)
set tc=ag[jh[q6]]
set a=0
loop
exitwhen r6 or a>C9[tc]
set s6=ph[(B9[b9[(tc)]+(a)])]
if Ek[Vi[s6]]then
set r6=La(s6,q6,as,Bs)
endif
set a=a+1
endloop
endif
return true
endfunction
function t6 takes nothing returns boolean
local integer vr=Jq
local integer a=0
set a=0
loop
call Rv(ee[fe[vr]+a])
set a=a+1
exitwhen a==608
endloop
call ju("Neighbors are ready!")
return true
endfunction
function u6 takes nothing returns boolean
local integer vr=Jq
local integer a=0
loop
set ee[fe[vr]+a]=Qv(vr,a)
set a=a+1
exitwhen a==608
endloop
call Nr(vr)
call ju("Cells are ready!")
return true
endfunction
function v6 takes nothing returns boolean
local integer vr=Jq
local integer a=0
loop
call Kr(ee[fe[vr]+a])
set a=a+1
exitwhen a==608
endloop
call ju("Cells destroyed!")
return true
endfunction
function w6 takes nothing returns boolean
local integer vr=Jq
local integer q=Kq
local integer c
local integer d
set ve[vr]=true
loop
set c=Tv(q)
set d=Hd[c]+1
call Wv(q,Md[c],d)
call Wv(q,Nd[c],d)
call Wv(q,Od[c],d)
call Wv(q,Pd[c],d)
exitwhen(Vd[(q)]==0)
endloop
set ue[vr]=not Ld[ee[fe[vr]]]
set ve[vr]=false
return true
endfunction
function x6 takes nothing returns boolean
local integer vr=Jq
local integer a=0
local boolean S3=ue[vr]
local integer c
loop
set c=ee[fe[vr]+a]
set Ld[(c)]=true
if S3 then
set Gd[(c)]=(Hd[c])
set Hd[(c)]=(0)
endif
set a=a+1
exitwhen a==w4
endloop
return true
endfunction
function y6 takes nothing returns boolean
local integer vr=Jq
local integer a=0
loop
call DestroyImage(qg[rg[vr]+a])
set qg[rg[vr]+a]=null
set a=a+1
exitwhen a==120
endloop
return true
endfunction
function z6 takes nothing returns boolean
local integer vr=Jq
local boolean Vs=Oq
call SetUnitExploded(mh[vr],Vs)
call KillUnit(mh[vr])
if Vs or go[vh[vr]]then
call Gz(vr)
else
set qh[vr]=false
call Fz(vr)
call ov(bg[jh[vr]],vr)
call Cs(ph[vr])
set ch[vr]=GetUnitX(mh[vr])
set Dh[vr]=GetUnitY(mh[vr])
set Eh[vr]=GetUnitFacing(mh[vr])
call TimerStart(Hh[vr],15.,false,function Nz)
endif
return true
endfunction
function A6 takes nothing returns boolean
local integer vr=Jq
local integer ks=Kq
if KB(vr,ks,ek[vr])then
call JB(vr)
else
call TimerStart(fk[(vr)],S4,false,uk)
endif
return true
endfunction
function a6 takes nothing returns boolean
local player p=Mq
local integer i=Jq
local integer vr=ws()
local integer a
set Jf[vr]=p
set Pf[vr]=p==F
set Lf[vr]=GetPlayerName(p)
call yx(vr)
set Kf[vr]=Vu(vr)
call Xu(vr)
set B4[i]=vr
set Mf[vr]=sv(GetPlayerColor(p))
set b4[Mf[vr]]=true
if GetPlayerColor(p)==e4 then
set C4=p
endif
set Sf[vr]=Zv(vr)
set yg[vr]=zr()
set zg[vr]=Ar()
set Ag[vr]=ar()
set ag[vr]=Br()
set Bg[vr]=Cr()
set bg[vr]=br()
set a=0
loop
call SetPlayerTechMaxAllowed(p,1949315120+a,tp[(a)])
set a=a+1
exitwhen a==o7
endloop
set a=1
loop
set Ig[lg[vr]+a]=sx(a)
set a=a+1
exitwhen a==32
endloop
set a=1
loop
set Gg[Hg[vr]+a]=ux(a)
set a=a+1
exitwhen a==32
endloop
set a=1
loop
set cg[Dg[vr]+a]=vx(a)
call SetPlayerAbilityAvailable(p,Eo[Fo[n7[a]]],false)
call SetPlayerAbilityAvailable(p,Eo[Fo[n7[a]]+1],false)
call SetPlayerAbilityAvailable(p,Eo[Fo[n7[a]]+2],false)
set a=a+1
exitwhen a==32
endloop
set a=1
loop
set Kg[Lg[vr]+a]=cr()
call SetPlayerAbilityAvailable(p,Hn[j7[a]],false)
call SetPlayerAbilityAvailable(p,Gn[j7[a]],false)
call SetPlayerAbilityAvailable(p,Fn[j7[a]],false)
call SetPlayerAbilityAvailable(p,Mn[Nn[j7[a]]],false)
call SetPlayerAbilityAvailable(p,Mn[Nn[j7[a]]+1],false)
call SetPlayerAbilityAvailable(p,Kn[Ln[j7[a]]],false)
call SetPlayerAbilityAvailable(p,Kn[Ln[j7[a]]+1],false)
set a=a+1
exitwhen a==24
endloop
call TriggerRegisterPlayerUnitEvent(e8,p,EVENT_PLAYER_UNIT_CONSTRUCT_START,null)
call TriggerRegisterPlayerUnitEvent(f8,p,EVENT_PLAYER_UNIT_CONSTRUCT_CANCEL,null)
call TriggerRegisterPlayerUnitEvent(g8,p,EVENT_PLAYER_UNIT_CONSTRUCT_FINISH,null)
call TriggerRegisterPlayerUnitEvent(n8,p,EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerRegisterPlayerUnitEvent(o8,p,EVENT_PLAYER_UNIT_SPELL_ENDCAST,null)
call TriggerRegisterPlayerUnitEvent(p8,p,EVENT_PLAYER_UNIT_ISSUED_ORDER,null)
call TriggerRegisterPlayerUnitEvent(q8,p,EVENT_PLAYER_UNIT_ISSUED_POINT_ORDER,null)
call TriggerRegisterPlayerUnitEvent(h8,p,EVENT_PLAYER_UNIT_UPGRADE_START,null)
call TriggerRegisterPlayerUnitEvent(i8,p,EVENT_PLAYER_UNIT_UPGRADE_CANCEL,null)
call TriggerRegisterPlayerUnitEvent(j8,p,EVENT_PLAYER_UNIT_UPGRADE_FINISH,null)
call TriggerRegisterPlayerUnitEvent(r8,p,EVENT_PLAYER_UNIT_SELECTED,null)
call TriggerRegisterPlayerUnitEvent(s8,p,EVENT_PLAYER_UNIT_DESELECTED,null)
call TriggerRegisterPlayerEvent(d8,p,EVENT_PLAYER_LEAVE)
set lq=vr
return true
endfunction
function L3 takes nothing returns nothing
set Dq=CreateTrigger()
call TriggerAddCondition(Dq,Condition(function M3))
set eq[85]=CreateTrigger()
set eq[89]=eq[85]
set eq[92]=eq[85]
set eq[93]=eq[85]
call TriggerAddCondition(eq[85],Condition(function N3))
call TriggerAddAction(eq[85],function N3)
set rq[85]=null
set rq[87]=null
set rq[92]=null
set sq=CreateTrigger()
call TriggerAddCondition(sq,Condition(function O3))
call TriggerAddAction(sq,function O3)
set tq=CreateTrigger()
call TriggerAddCondition(tq,Condition(function P3))
call TriggerAddAction(tq,function P3)
set uq=CreateTrigger()
call TriggerAddCondition(uq,Condition(function Q3))
call TriggerAddAction(uq,function Q3)
set vq=CreateTrigger()
call TriggerAddCondition(vq,Condition(function R3))
call TriggerAddAction(vq,function R3)
set wq=CreateTrigger()
call TriggerAddCondition(wq,Condition(function T3))
call TriggerAddAction(wq,function T3)
set gq[31]=null
set gq[32]=null
set gq[33]=null
set gq[34]=null
set Sp[70]=CreateTrigger()
set Sp[96]=Sp[70]
set Sp[97]=Sp[70]
set Sp[98]=Sp[70]
set Sp[99]=Sp[70]
set Sp[101]=Sp[70]
set Sp[102]=Sp[70]
set Sp[103]=Sp[70]
set Sp[104]=Sp[70]
set Sp[105]=Sp[70]
call TriggerAddCondition(Sp[70],Condition(function U3))
call TriggerAddAction(Sp[70],function U3)
set Tp[70]=CreateTrigger()
set Tp[96]=Tp[70]
set Tp[97]=Tp[70]
set Tp[98]=Tp[70]
set Tp[99]=Tp[70]
set Tp[100]=Tp[70]
set Tp[101]=Tp[70]
set Tp[102]=Tp[70]
set Tp[104]=Tp[70]
set Tp[105]=Tp[70]
call TriggerAddCondition(Tp[70],Condition(function V3))
call TriggerAddAction(Tp[70],function V3)
set Up[70]=CreateTrigger()
set Up[96]=Up[70]
set Up[98]=Up[70]
set Up[99]=Up[70]
set Up[101]=Up[70]
set Up[102]=Up[70]
set Up[105]=Up[70]
call TriggerAddCondition(Up[70],Condition(function W3))
call TriggerAddAction(Up[70],function W3)
set Vp[70]=CreateTrigger()
set Vp[96]=Vp[70]
set Vp[97]=Vp[70]
set Vp[99]=Vp[70]
set Vp[100]=Vp[70]
set Vp[101]=Vp[70]
set Vp[102]=Vp[70]
set Vp[103]=Vp[70]
set Vp[104]=Vp[70]
set Vp[105]=Vp[70]
call TriggerAddCondition(Vp[70],Condition(function X3))
call TriggerAddAction(Vp[70],function X3)
set Wp[70]=CreateTrigger()
set Wp[96]=Wp[70]
set Wp[101]=Wp[70]
set Wp[102]=Wp[70]
set Wp[105]=Wp[70]
call TriggerAddCondition(Wp[70],Condition(function Y3))
call TriggerAddAction(Wp[70],function Y3)
set Xp[70]=CreateTrigger()
set Xp[96]=Xp[70]
set Xp[97]=Xp[70]
set Xp[98]=Xp[70]
set Xp[100]=Xp[70]
set Xp[101]=Xp[70]
set Xp[102]=Xp[70]
set Xp[103]=Xp[70]
set Xp[105]=Xp[70]
call TriggerAddCondition(Xp[70],Condition(function Z3))
call TriggerAddAction(Xp[70],function Z3)
set Yp[70]=CreateTrigger()
set Yp[96]=Yp[70]
set Yp[97]=Yp[70]
set Yp[98]=Yp[70]
set Yp[99]=Yp[70]
set Yp[100]=Yp[70]
set Yp[101]=Yp[70]
set Yp[102]=Yp[70]
set Yp[104]=Yp[70]
set Yp[105]=Yp[70]
call TriggerAddCondition(Yp[70],Condition(function dF))
call TriggerAddAction(Yp[70],function dF)
set Zp[70]=CreateTrigger()
set Zp[96]=Zp[70]
set Zp[97]=Zp[70]
set Zp[98]=Zp[70]
set Zp[99]=Zp[70]
set Zp[100]=Zp[70]
set Zp[101]=Zp[70]
set Zp[103]=Zp[70]
set Zp[104]=Zp[70]
set Zp[105]=Zp[70]
call TriggerAddCondition(Zp[70],Condition(function eF))
call TriggerAddAction(Zp[70],function eF)
set dq[70]=CreateTrigger()
set dq[96]=dq[70]
set dq[97]=dq[70]
set dq[98]=dq[70]
set dq[99]=dq[70]
set dq[100]=dq[70]
set dq[101]=dq[70]
set dq[103]=dq[70]
set dq[104]=dq[70]
set dq[105]=dq[70]
call TriggerAddCondition(dq[70],Condition(function fF))
call TriggerAddAction(dq[70],function fF)
set pq[70]=null
set pq[96]=null
set pq[97]=null
set pq[98]=null
set pq[99]=null
set pq[100]=null
set pq[101]=null
set pq[102]=null
set pq[103]=null
set pq[104]=null
set pq[105]=null
set xq=CreateTrigger()
call TriggerAddCondition(xq,Condition(function gF))
set yq=CreateTrigger()
call TriggerAddCondition(yq,Condition(function hF))
call TriggerAddAction(yq,function hF)
set zq=CreateTrigger()
call TriggerAddCondition(zq,Condition(function iF))
call TriggerAddAction(zq,function iF)
set Aq=CreateTrigger()
call TriggerAddCondition(Aq,Condition(function jF))
call TriggerAddAction(Aq,function jF)
set nq[61]=CreateTrigger()
set nq[62]=nq[61]
set nq[63]=nq[61]
set nq[64]=nq[61]
set nq[68]=nq[61]
call TriggerAddCondition(nq[61],Condition(function kF))
set Np[61]=CreateTrigger()
set Np[62]=Np[61]
set Np[63]=Np[61]
set Np[64]=Np[61]
set Np[67]=Np[61]
set Np[68]=Np[61]
call TriggerAddCondition(Np[61],Condition(function mF))
call TriggerAddAction(Np[61],function mF)
set Op[61]=CreateTrigger()
set Op[62]=Op[61]
set Op[63]=Op[61]
set Op[64]=Op[61]
set Op[65]=Op[61]
set Op[66]=Op[61]
set Op[68]=Op[61]
set Op[69]=Op[61]
call TriggerAddCondition(Op[61],Condition(function nF))
call TriggerAddAction(Op[61],function nF)
set Pp[61]=CreateTrigger()
set Pp[63]=Pp[61]
set Pp[65]=Pp[61]
set Pp[66]=Pp[61]
set Pp[69]=Pp[61]
call TriggerAddCondition(Pp[61],Condition(function oF))
call TriggerAddAction(Pp[61],function oF)
set cq=CreateTrigger()
call TriggerAddCondition(cq,Condition(function pF))
set Qp[61]=CreateTrigger()
set Qp[62]=Qp[61]
set Qp[63]=Qp[61]
set Qp[64]=Qp[61]
set Qp[67]=Qp[61]
set Qp[68]=Qp[61]
set Qp[69]=Qp[61]
call TriggerAddCondition(Qp[61],Condition(function qF))
call TriggerAddAction(Qp[61],function qF)
set Rp[61]=CreateTrigger()
set Rp[62]=Rp[61]
set Rp[63]=Rp[61]
set Rp[64]=Rp[61]
set Rp[67]=Rp[61]
set Rp[68]=Rp[61]
set Rp[69]=Rp[61]
call TriggerAddCondition(Rp[61],Condition(function rF))
call TriggerAddAction(Rp[61],function rF)
set Cq=CreateTrigger()
call TriggerAddCondition(Cq,Condition(function sF))
set bq=CreateTrigger()
call TriggerAddCondition(bq,Condition(function tF))
set iq[43]=CreateTrigger()
set iq[78]=iq[43]
set iq[79]=iq[43]
set iq[80]=iq[43]
set iq[81]=iq[43]
set iq[82]=iq[43]
set iq[83]=iq[43]
call TriggerAddCondition(iq[43],Condition(function uF))
set Cp[43]=CreateTrigger()
set Cp[78]=Cp[43]
set Cp[80]=Cp[43]
set Cp[81]=Cp[43]
set Cp[82]=Cp[43]
set Cp[83]=Cp[43]
call TriggerAddCondition(Cp[43],Condition(function vF))
call TriggerAddAction(Cp[43],function vF)
set cp[43]=CreateTrigger()
set cp[78]=cp[43]
set cp[80]=cp[43]
set cp[81]=cp[43]
set cp[83]=cp[43]
call TriggerAddCondition(cp[43],Condition(function wF))
call TriggerAddAction(cp[43],function wF)
set Dp[43]=CreateTrigger()
set Dp[78]=Dp[43]
set Dp[80]=Dp[43]
set Dp[83]=Dp[43]
call TriggerAddCondition(Dp[43],Condition(function xF))
call TriggerAddAction(Dp[43],function xF)
set Ep[43]=CreateTrigger()
set Ep[78]=Ep[43]
set Ep[79]=Ep[43]
set Ep[80]=Ep[43]
set Ep[83]=Ep[43]
call TriggerAddCondition(Ep[43],Condition(function yF))
call TriggerAddAction(Ep[43],function yF)
set Fp[43]=CreateTrigger()
set Fp[78]=Fp[43]
set Fp[79]=Fp[43]
set Fp[80]=Fp[43]
set Fp[81]=Fp[43]
set Fp[83]=Fp[43]
call TriggerAddCondition(Fp[43],Condition(function zF))
call TriggerAddAction(Fp[43],function zF)
set Gp[43]=CreateTrigger()
set Gp[78]=Gp[43]
set Gp[79]=Gp[43]
set Gp[80]=Gp[43]
set Gp[81]=Gp[43]
set Gp[82]=Gp[43]
set Gp[83]=Gp[43]
call TriggerAddCondition(Gp[43],Condition(function AF))
call TriggerAddAction(Gp[43],function AF)
set Hp[43]=CreateTrigger()
set Hp[78]=Hp[43]
set Hp[79]=Hp[43]
set Hp[80]=Hp[43]
set Hp[81]=Hp[43]
set Hp[83]=Hp[43]
call TriggerAddCondition(Hp[43],Condition(function aF))
call TriggerAddAction(Hp[43],function aF)
set Ip[43]=CreateTrigger()
set Ip[78]=Ip[43]
set Ip[79]=Ip[43]
set Ip[80]=Ip[43]
set Ip[81]=Ip[43]
set Ip[82]=Ip[43]
set Ip[83]=Ip[43]
call TriggerAddCondition(Ip[43],Condition(function BF))
call TriggerAddAction(Ip[43],function BF)
set lp[43]=CreateTrigger()
set lp[78]=lp[43]
set lp[79]=lp[43]
set lp[80]=lp[43]
set lp[81]=lp[43]
set lp[82]=lp[43]
set lp[83]=lp[43]
call TriggerAddCondition(lp[43],Condition(function bF))
call TriggerAddAction(lp[43],function bF)
set Jp[43]=CreateTrigger()
set Jp[78]=Jp[43]
set Jp[79]=Jp[43]
set Jp[80]=Jp[43]
set Jp[81]=Jp[43]
set Jp[83]=Jp[43]
call TriggerAddCondition(Jp[43],Condition(function CF))
call TriggerAddAction(Jp[43],function CF)
set Kp[43]=CreateTrigger()
set Kp[78]=Kp[43]
set Kp[79]=Kp[43]
set Kp[80]=Kp[43]
set Kp[81]=Kp[43]
set Kp[82]=Kp[43]
set Kp[83]=Kp[43]
call TriggerAddCondition(Kp[43],Condition(function cF))
call TriggerAddAction(Kp[43],function cF)
set Lp[43]=CreateTrigger()
set Lp[78]=Lp[43]
set Lp[79]=Lp[43]
set Lp[80]=Lp[43]
set Lp[81]=Lp[43]
set Lp[82]=Lp[43]
set Lp[83]=Lp[43]
call TriggerAddCondition(Lp[43],Condition(function DF))
call TriggerAddAction(Lp[43],function DF)
set aq=CreateTrigger()
call TriggerAddCondition(aq,Condition(function EF))
call TriggerAddAction(aq,function EF)
set kq[44]=CreateTrigger()
call TriggerAddCondition(kq[44],Condition(function FF))
set Mp[44]=CreateTrigger()
set Mp[48]=Mp[44]
set Mp[50]=Mp[44]
set Mp[52]=Mp[44]
set Mp[54]=Mp[44]
set Mp[56]=Mp[44]
set Mp[72]=Mp[44]
set Mp[73]=Mp[44]
set Mp[74]=Mp[44]
set Mp[75]=Mp[44]
set Mp[76]=Mp[44]
set Mp[77]=Mp[44]
call TriggerAddCondition(Mp[44],Condition(function GF))
call TriggerAddAction(Mp[44],function GF)
set kq[54]=CreateTrigger()
call TriggerAddCondition(kq[54],Condition(function HF))
call TriggerAddCondition(kq[54],Condition(function FF))
set Mp[53]=CreateTrigger()
call TriggerAddCondition(Mp[53],Condition(function lF))
call TriggerAddAction(Mp[53],function lF)
set kq[53]=CreateTrigger()
call TriggerAddCondition(kq[53],Condition(function IF))
call TriggerAddCondition(kq[53],Condition(function FF))
set kq[52]=CreateTrigger()
call TriggerAddCondition(kq[52],Condition(function JF))
call TriggerAddCondition(kq[52],Condition(function FF))
set Mp[51]=CreateTrigger()
call TriggerAddCondition(Mp[51],Condition(function LF))
call TriggerAddAction(Mp[51],function LF)
set kq[51]=CreateTrigger()
call TriggerAddCondition(kq[51],Condition(function KF))
call TriggerAddCondition(kq[51],Condition(function FF))
set kq[50]=CreateTrigger()
call TriggerAddCondition(kq[50],Condition(function MF))
call TriggerAddCondition(kq[50],Condition(function FF))
set Mp[49]=CreateTrigger()
call TriggerAddCondition(Mp[49],Condition(function OF))
call TriggerAddAction(Mp[49],function OF)
set kq[49]=CreateTrigger()
call TriggerAddCondition(kq[49],Condition(function NF))
call TriggerAddCondition(kq[49],Condition(function FF))
set kq[48]=CreateTrigger()
call TriggerAddCondition(kq[48],Condition(function PF))
call TriggerAddCondition(kq[48],Condition(function FF))
set Bq=CreateTrigger()
call TriggerAddCondition(Bq,Condition(function QF))
set Mp[55]=CreateTrigger()
call TriggerAddCondition(Mp[55],Condition(function SF))
call TriggerAddAction(Mp[55],function SF)
set kq[55]=CreateTrigger()
call TriggerAddCondition(kq[55],Condition(function RF))
call TriggerAddCondition(kq[55],Condition(function FF))
set kq[56]=CreateTrigger()
call TriggerAddCondition(kq[56],Condition(function TF))
call TriggerAddCondition(kq[56],Condition(function FF))
set Mp[57]=CreateTrigger()
call TriggerAddCondition(Mp[57],Condition(function VF))
call TriggerAddAction(Mp[57],function VF)
set kq[57]=CreateTrigger()
call TriggerAddCondition(kq[57],Condition(function UF))
call TriggerAddCondition(kq[57],Condition(function FF))
set Pp[62]=CreateTrigger()
call TriggerAddCondition(Pp[62],Condition(function WF))
call TriggerAddAction(Pp[62],function WF)
set Pp[64]=CreateTrigger()
call TriggerAddCondition(Pp[64],Condition(function XF))
call TriggerAddAction(Pp[64],function XF)
set Np[65]=CreateTrigger()
call TriggerAddCondition(Np[65],Condition(function ZF))
call TriggerAddAction(Np[65],function ZF)
set Qp[65]=CreateTrigger()
call TriggerAddCondition(Qp[65],Condition(function dG))
call TriggerAddAction(Qp[65],function dG)
set Rp[65]=CreateTrigger()
call TriggerAddCondition(Rp[65],Condition(function eG))
call TriggerAddAction(Rp[65],function eG)
set nq[65]=CreateTrigger()
call TriggerAddCondition(nq[65],Condition(function YF))
call TriggerAddCondition(nq[65],Condition(function kF))
set Np[66]=CreateTrigger()
call TriggerAddCondition(Np[66],Condition(function gG))
call TriggerAddAction(Np[66],function gG)
set Qp[66]=CreateTrigger()
call TriggerAddCondition(Qp[66],Condition(function iG))
call TriggerAddAction(Qp[66],function iG)
set Rp[66]=CreateTrigger()
call TriggerAddCondition(Rp[66],Condition(function jG))
call TriggerAddAction(Rp[66],function jG)
set nq[66]=CreateTrigger()
call TriggerAddCondition(nq[66],Condition(function fG))
call TriggerAddCondition(nq[66],Condition(function kF))
set Pp[67]=CreateTrigger()
call TriggerAddCondition(Pp[67],Condition(function mG))
call TriggerAddAction(Pp[67],function mG)
set Op[67]=CreateTrigger()
call TriggerAddCondition(Op[67],Condition(function nG))
call TriggerAddAction(Op[67],function nG)
set nq[67]=CreateTrigger()
call TriggerAddCondition(nq[67],Condition(function kG))
call TriggerAddCondition(nq[67],Condition(function kF))
set Pp[68]=CreateTrigger()
call TriggerAddCondition(Pp[68],Condition(function oG))
call TriggerAddAction(Pp[68],function oG)
set Np[69]=CreateTrigger()
call TriggerAddCondition(Np[69],Condition(function qG))
call TriggerAddAction(Np[69],function qG)
set nq[69]=CreateTrigger()
call TriggerAddCondition(nq[69],Condition(function pG))
call TriggerAddCondition(nq[69],Condition(function kF))
set Mp[71]=CreateTrigger()
call TriggerAddCondition(Mp[71],Condition(function sG))
call TriggerAddAction(Mp[71],function sG)
set kq[71]=CreateTrigger()
call TriggerAddCondition(kq[71],Condition(function rG))
call TriggerAddCondition(kq[71],Condition(function FF))
set kq[72]=CreateTrigger()
call TriggerAddCondition(kq[72],Condition(function tG))
call TriggerAddCondition(kq[72],Condition(function FF))
set kq[73]=CreateTrigger()
call TriggerAddCondition(kq[73],Condition(function uG))
call TriggerAddCondition(kq[73],Condition(function FF))
set kq[74]=CreateTrigger()
call TriggerAddCondition(kq[74],Condition(function vG))
call TriggerAddCondition(kq[74],Condition(function FF))
set kq[75]=CreateTrigger()
call TriggerAddCondition(kq[75],Condition(function wG))
call TriggerAddCondition(kq[75],Condition(function FF))
set kq[76]=CreateTrigger()
call TriggerAddCondition(kq[76],Condition(function xG))
call TriggerAddCondition(kq[76],Condition(function FF))
set kq[77]=CreateTrigger()
call TriggerAddCondition(kq[77],Condition(function yG))
call TriggerAddCondition(kq[77],Condition(function FF))
set cp[79]=CreateTrigger()
call TriggerAddCondition(cp[79],Condition(function zG))
call TriggerAddAction(cp[79],function zG)
set Dp[79]=CreateTrigger()
call TriggerAddCondition(Dp[79],Condition(function AG))
call TriggerAddAction(Dp[79],function AG)
set Cp[79]=CreateTrigger()
call TriggerAddCondition(Cp[79],Condition(function aG))
call TriggerAddAction(Cp[79],function aG)
set Dp[81]=CreateTrigger()
call TriggerAddCondition(Dp[81],Condition(function BG))
call TriggerAddAction(Dp[81],function BG)
set Ep[81]=CreateTrigger()
call TriggerAddCondition(Ep[81],Condition(function bG))
call TriggerAddAction(Ep[81],function bG)
set cp[82]=CreateTrigger()
call TriggerAddCondition(cp[82],Condition(function CG))
call TriggerAddAction(cp[82],function CG)
set Dp[82]=CreateTrigger()
call TriggerAddCondition(Dp[82],Condition(function cG))
call TriggerAddAction(Dp[82],function cG)
set Ep[82]=CreateTrigger()
call TriggerAddCondition(Ep[82],Condition(function DG))
call TriggerAddAction(Ep[82],function DG)
set Fp[82]=CreateTrigger()
call TriggerAddCondition(Fp[82],Condition(function EG))
call TriggerAddAction(Fp[82],function EG)
set Hp[82]=CreateTrigger()
call TriggerAddCondition(Hp[82],Condition(function FG))
call TriggerAddAction(Hp[82],function FG)
set Jp[82]=CreateTrigger()
call TriggerAddCondition(Jp[82],Condition(function GG))
call TriggerAddAction(Jp[82],function GG)
set eq[86]=CreateTrigger()
call TriggerAddCondition(eq[86],Condition(function IG))
call TriggerAddAction(eq[86],function IG)
set rq[86]=CreateTrigger()
call TriggerAddCondition(rq[86],Condition(function HG))
set eq[87]=CreateTrigger()
call TriggerAddCondition(eq[87],Condition(function lG))
call TriggerAddAction(eq[87],function lG)
set eq[88]=CreateTrigger()
call TriggerAddCondition(eq[88],Condition(function KG))
call TriggerAddAction(eq[88],function KG)
set rq[88]=CreateTrigger()
call TriggerAddCondition(rq[88],Condition(function JG))
set rq[89]=CreateTrigger()
call TriggerAddCondition(rq[89],Condition(function LG))
set eq[90]=CreateTrigger()
call TriggerAddCondition(eq[90],Condition(function NG))
call TriggerAddAction(eq[90],function NG)
set rq[90]=CreateTrigger()
call TriggerAddCondition(rq[90],Condition(function MG))
set eq[91]=CreateTrigger()
call TriggerAddCondition(eq[91],Condition(function PG))
call TriggerAddAction(eq[91],function PG)
set rq[91]=CreateTrigger()
call TriggerAddCondition(rq[91],Condition(function OG))
set rq[93]=CreateTrigger()
call TriggerAddCondition(rq[93],Condition(function QG))
set eq[94]=CreateTrigger()
call TriggerAddCondition(eq[94],Condition(function SG))
call TriggerAddAction(eq[94],function SG)
set rq[94]=CreateTrigger()
call TriggerAddCondition(rq[94],Condition(function RG))
set Up[97]=CreateTrigger()
call TriggerAddCondition(Up[97],Condition(function TG))
call TriggerAddAction(Up[97],function TG)
set Wp[97]=CreateTrigger()
call TriggerAddCondition(Wp[97],Condition(function UG))
call TriggerAddAction(Wp[97],function UG)
set Vp[98]=CreateTrigger()
call TriggerAddCondition(Vp[98],Condition(function VG))
call TriggerAddAction(Vp[98],function VG)
set Wp[98]=CreateTrigger()
call TriggerAddCondition(Wp[98],Condition(function WG))
call TriggerAddAction(Wp[98],function WG)
set Wp[99]=CreateTrigger()
call TriggerAddCondition(Wp[99],Condition(function XG))
call TriggerAddAction(Wp[99],function XG)
set Xp[99]=CreateTrigger()
call TriggerAddCondition(Xp[99],Condition(function YG))
call TriggerAddAction(Xp[99],function YG)
set Sp[100]=CreateTrigger()
call TriggerAddCondition(Sp[100],Condition(function ZG))
call TriggerAddAction(Sp[100],function ZG)
set Up[100]=CreateTrigger()
call TriggerAddCondition(Up[100],Condition(function d6))
call TriggerAddAction(Up[100],function d6)
set Wp[100]=CreateTrigger()
call TriggerAddCondition(Wp[100],Condition(function e6))
call TriggerAddAction(Wp[100],function e6)
set Zp[102]=CreateTrigger()
call TriggerAddCondition(Zp[102],Condition(function f6))
call TriggerAddAction(Zp[102],function f6)
set dq[102]=CreateTrigger()
call TriggerAddCondition(dq[102],Condition(function g6))
call TriggerAddAction(dq[102],function g6)
set Tp[103]=CreateTrigger()
call TriggerAddCondition(Tp[103],Condition(function h6))
call TriggerAddAction(Tp[103],function h6)
set Up[103]=CreateTrigger()
call TriggerAddCondition(Up[103],Condition(function i6))
call TriggerAddAction(Up[103],function i6)
set Yp[103]=CreateTrigger()
call TriggerAddCondition(Yp[103],Condition(function j6))
call TriggerAddAction(Yp[103],function j6)
set Wp[103]=CreateTrigger()
call TriggerAddCondition(Wp[103],Condition(function k6))
call TriggerAddAction(Wp[103],function k6)
set Up[104]=CreateTrigger()
call TriggerAddCondition(Up[104],Condition(function m6))
call TriggerAddAction(Up[104],function m6)
set Wp[104]=CreateTrigger()
call TriggerAddCondition(Wp[104],Condition(function n6))
call TriggerAddAction(Wp[104],function n6)
set Xp[104]=CreateTrigger()
call TriggerAddCondition(Xp[104],Condition(function o6))
call TriggerAddAction(Xp[104],function o6)
set Fq[1]=CreateTrigger()
call TriggerAddAction(Fq[1],function p6)
call TriggerAddCondition(Fq[1],Condition(function p6))
set Eq[1]=CreateTrigger()
call TriggerAddAction(Eq[1],function t6)
call TriggerAddCondition(Eq[1],Condition(function t6))
set Eq[2]=CreateTrigger()
call TriggerAddAction(Eq[2],function u6)
call TriggerAddCondition(Eq[2],Condition(function u6))
set Eq[3]=CreateTrigger()
call TriggerAddAction(Eq[3],function v6)
call TriggerAddCondition(Eq[3],Condition(function v6))
set Gq[1]=CreateTrigger()
call TriggerAddAction(Gq[1],function w6)
call TriggerAddCondition(Gq[1],Condition(function w6))
set Eq[4]=CreateTrigger()
call TriggerAddAction(Eq[4],function x6)
call TriggerAddCondition(Eq[4],Condition(function x6))
set Eq[5]=CreateTrigger()
call TriggerAddAction(Eq[5],function y6)
call TriggerAddCondition(Eq[5],Condition(function y6))
set Hq[1]=CreateTrigger()
call TriggerAddAction(Hq[1],function z6)
call TriggerAddCondition(Hq[1],Condition(function z6))
set Gq[2]=CreateTrigger()
call TriggerAddAction(Gq[2],function A6)
call TriggerAddCondition(Gq[2],Condition(function A6))
set Iq[1]=CreateTrigger()
call TriggerAddAction(Iq[1],function a6)
call TriggerAddCondition(Iq[1],Condition(function a6))
call ExecuteFunc("vv")
call ExecuteFunc("Ta")
call ExecuteFunc("Ya")
call ExecuteFunc("Za")
call ExecuteFunc("iB")
call ExecuteFunc("rB")
call ExecuteFunc("fb")
call ExecuteFunc("mC")
call ExecuteFunc("UC")
call ExecuteFunc("kc")
endfunction
