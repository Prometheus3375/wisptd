globals
rect h=null
rect j=null
rect o=null
rect w=null
rect z=null
rect B=null
rect C=null
rect D=null
rect E=null
rect F=null
rect G=null
rect I=null
rect J=null
rect K=null
rect M=null
rect N=null
rect O=null
rect P=null
rect Q=null
rect R=null
rect S=null
rect U=null
rect V=null
rect W=null
rect X=null
rect Y=null
rect Z=null
rect d4=null
rect e4=null
rect f4=null
rect g4=null
rect h4=null
rect i4=null
rect j4=null
rect k4=null
rect m4=null
rect n4=null
rect o4=null
rect p4=null
rect q4=null
rect r4=null
rect s4=null
rect t4=null
rect u4=null
rect v4=null
rect w4=null
rect x4=null
rect y4=null
rect z4=null
rect A4=null
rect a4=null
rect B4=null
rect b4=null
rect C4=null
rect c4=null
sound D4=null
sound E4=null
sound F4=null
sound G4=null
sound H4=null
sound I4=null
trigger l4=null
trigger J4=null
trigger K4=null
trigger L4=null
trigger M4=null
trigger N4=null
trigger O4=null
trigger P4=null
trigger Q4=null
trigger R4=null
trigger S4=null
trigger T4=null
trigger U4=null
trigger V4=null
trigger W4=null
trigger X4=null
trigger Y4=null
trigger Z4=null
trigger d7=null
trigger e7=null
trigger f7=null
trigger g7=null
trigger h7=null
trigger i7=null
trigger j7=null
trigger k7=null
trigger m7=null
trigger n7=null
trigger o7=null
trigger p7=null
trigger q7=null
trigger r7=null
trigger s7=null
trigger t7=null
trigger u7=null
trigger v7=null
trigger w7=null
trigger x7=null
trigger y7=null
trigger z7=null
trigger A7=null
trigger a7=null
trigger B7=null
trigger b7=null
trigger C7=null
trigger c7=null
trigger D7=null
trigger E7=null
trigger F7=null
trigger G7=null
trigger H7=null
trigger I7=null
trigger l7=null
trigger J7=null
trigger K7=null
trigger L7=null
trigger M7=null
trigger N7=null
boolean O7=true
integer P7=30
integer Q7=60
integer R7=1
boolean S7=true
boolean T7=true
boolean U7=true
boolean V7=true
integer W7=1
string X7
integer array Y7
integer AT=-1
multiboard Z7
string d8
unit array e8
integer C0=-1
unit array f8
integer C1=-1
unit array g8
integer C2=-1
string array h8
integer i8=0
hashtable j8
integer array k8
integer MT=0
integer m8=0
integer array n8
integer TT=0
integer o8=0
integer p8=0
unit array q8
integer array r8
integer array s8
integer array FN
integer array t8
integer array u8
integer array LN
integer array v8
boolean array w8
integer array x8
integer array TN
rect array y8
rect array z8
rect array A8
rect array a8
rect array B8
region b8
region C8
timerdialog c8
timer D8=CreateTimer()
integer E8
unit array F8
unit array G8
unit array H8
unit array I8
fogmodifier array l8
real L8=.0
real M8=.0
boolexpr P8=null
endglobals
function R8 takes nothing returns boolean
local real dx=GetDestructableX(GetFilterDestructable())-L8
local real dy=GetDestructableY(GetFilterDestructable())-M8
return(dx*dx+dy*dy<=bj_enumDestructableRadius)
endfunction
function S8 takes itemtype T8,integer U8 returns nothing
local group g
set bj_stockPickedItemType=T8
set bj_stockPickedItemLevel=U8
set g=CreateGroup()
call GroupEnumUnitsOfType(g,"marketplace",P8)
call ForGroup(g,function UpdateEachStockBuildingEnum)
call DestroyGroup(g)
set g=null
endfunction
function V8 takes nothing returns nothing
local integer pickedItemId
local itemtype W8
local integer X8=0
local integer Y8=0
local integer U8
set U8=1
loop
if(bj_stockAllowedPermanent[U8])then
set Y8=Y8+1
if(GetRandomInt(1,Y8)==1)then
set W8=ITEM_TYPE_PERMANENT
set X8=U8
endif
endif
if(bj_stockAllowedCharged[U8])then
set Y8=Y8+1
if(GetRandomInt(1,Y8)==1)then
set W8=ITEM_TYPE_CHARGED
set X8=U8
endif
endif
if(bj_stockAllowedArtifact[U8])then
set Y8=Y8+1
if(GetRandomInt(1,Y8)==1)then
set W8=ITEM_TYPE_ARTIFACT
set X8=U8
endif
endif
set U8=U8+1
exitwhen U8>$A
endloop
if(Y8==0)then
set W8=null
return
endif
call S8(W8,X8)
set W8=null
endfunction
function Z8 takes nothing returns nothing
call V8()
call TimerStart(bj_stockUpdateTimer,bj_STOCK_RESTOCK_INTERVAL,true,function V8)
endfunction
function e9 takes nothing returns boolean
return true
endfunction
function i9 takes integer i returns boolean
return GetLocalPlayer()==Player(i)
endfunction
function j9 takes unit u1,unit u2 returns real
local real x1=GetUnitX(u1)
local real x2=GetUnitX(u2)
local real y1=GetUnitY(u1)
local real y2=GetUnitY(u2)
return SquareRoot((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2))
endfunction
function k9 takes integer i,sound m9,integer n9,string s returns nothing
if i9(i)then
call StartSound(m9)
endif
call DisplayTimedTextToPlayer(Player(i),0,0,n9,s)
endfunction
function o9 takes integer i,integer p9,boolean b returns nothing
local player p=Player(i)
local integer q9=GetPlayerState(p,PLAYER_STATE_RESOURCE_GOLD)
local integer r9=GetPlayerState(p,PLAYER_STATE_GOLD_GATHERED)
if b then
call SetPlayerState(p,PLAYER_STATE_GOLD_GATHERED,r9+p9)
endif
call SetPlayerState(p,PLAYER_STATE_RESOURCE_GOLD,q9+p9)
set p=null
endfunction
function s9 takes integer i,integer t9 returns nothing
local multiboarditem u9=MultiboardGetItem(Z7,x8[i],3)
set t8[i]=t8[i]+t9
call MultiboardSetItemValue(u9,I2S(t8[i]))
call MultiboardReleaseItem(u9)
set u9=null
endfunction
function v9 takes string s,unit u,real w9,real x9,integer i,real n9,real y9 returns nothing
local texttag z9=CreateTextTag()
call SetTextTagText(z9,s,w9*.0024)
call SetTextTagPosUnit(z9,u,0)
call SetTextTagVelocity(z9,0,x9/ $640)
if(GetLocalPlayer()!=Player(i))then
call SetTextTagVisibility(z9,false)
endif
call SetTextTagPermanent(z9,false)
call SetTextTagLifespan(z9,n9)
call SetTextTagFadepoint(z9,y9)
set z9=null
endfunction
function A9 takes unit u returns real
local integer H=GetHandleId(u)
local integer T=GetUnitTypeId(u)
return LoadReal(j8,T,1097811282)+LoadReal(j8,H,1093677635)
endfunction
function a9 takes unit u returns real
local integer H=GetHandleId(u)
local integer T=GetUnitTypeId(u)
return LoadReal(j8,T,1148020545)+LoadInteger(j8,H,1265200236)*LoadReal(j8,T,1265200194)+LoadReal(j8,H,1093677633)
endfunction
function B9 takes unit t returns nothing
local integer H=GetHandleId(t)
local integer L=LoadInteger(j8,H,1281717868)
local effect e=LoadEffectHandle(j8,H,1281717868)
call SaveInteger(j8,H,1281717868,L+1)
if(e==null)then
set e=AddSpecialEffectTarget("Models\\Levelup.mdx",t,"overhead")
call SaveEffectHandle(j8,H,1281717868,e)
endif
set e=null
endfunction
function b9 takes unit t,integer C9 returns nothing
local integer H=GetHandleId(t)
local integer T=GetUnitTypeId(t)
local integer L=LoadInteger(j8,H,1281717868)
local integer a1=LoadInteger(j8,T,1096968497)
local integer a2=LoadInteger(j8,T,1096968498)
local integer e1=LoadInteger(j8,T,1165520945)
local integer e2=LoadInteger(j8,T,1165520946)
set C9=C9+LoadInteger(j8,H,1165520961)
call SaveInteger(j8,H,1165520961,C9)
if(C9>=e1)and(L<1)then
call B9(t)
call UnitAddAbility(t,a1)
endif
if(C9>=e2)and(L<2)then
call B9(t)
call UnitAddAbility(t,a2)
endif
endfunction
function c9 takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit c=LoadUnitHandle(j8,tH,0)
local integer cH=GetHandleId(c)
local integer T=GetUnitTypeId(c)
local integer n=0
call RemoveSavedHandle(j8,tH,0)
call RemoveSavedHandle(j8,cH,0)
call DestroyTimer(t)
if(T==2003985456)then
loop
exitwhen c==e8[n]
set n=n+1
endloop
set e8[n]=e8[C0]
set e8[C0]=null
set C0=C0-1
elseif(T==2003985457)then
loop
exitwhen c==f8[n]
set n=n+1
endloop
set f8[n]=f8[C1]
set f8[C1]=null
set C1=C1-1
elseif(T==2003985458)then
loop
exitwhen c==g8[n]
set n=n+1
endloop
set g8[n]=g8[C2]
set g8[C2]=null
set C2=C2-1
endif
call RemoveUnit(c)
set t=null
set c=null
endfunction
function D9 takes unit E9,integer T,real x,real y returns nothing
local integer i=(GetPlayerId(GetOwningPlayer((E9))))
local unit c=CreateUnit(Player(i),T,x,y,0)
local integer cH=GetHandleId(c)
local timer t=CreateTimer()
local integer tH=GetHandleId(t)
local real n9=4
if(GetUnitAbilityLevel((E9),(1093677637))>0)then
set n9=6
endif
if(T==2003985456)then
set C0=C0+1
set e8[C0]=c
elseif(T==2003985457)then
set C1=C1+1
set f8[C1]=c
elseif(T==2003985458)then
set C2=C2+1
set g8[C2]=c
endif
call SetUnitPathing(c,false)
call SaveUnitHandle(j8,cH,0,E9)
call SaveUnitHandle(j8,tH,0,c)
call TimerStart(t,n9,false,function c9)
set c=null
set t=null
endfunction
function F9 takes unit E9,integer G9,integer H9,string I9,integer l9,real x,real y,unit J9 returns boolean
local real x0=GetUnitX(E9)
local real y0=GetUnitY(E9)
local integer i=(GetPlayerId(GetOwningPlayer((E9))))
local integer K9=GetHandleId(E9)
local integer L9=GetUnitTypeId(E9)
local real M9=LoadReal(j8,L9,1214605684)
local unit d=LoadUnitHandle(j8,K9,G9)
local integer dH=0
local boolean b
if(d==null)then
set d=CreateUnit(Player(i),G9,x0,y0,0)
call SetUnitFlyHeight(d,M9,0)
call SetUnitPathing(d,false)
call UnitAddAbility(d,H9)
set dH=GetHandleId(d)
call SaveUnitHandle(j8,dH,0,E9)
call SaveUnitHandle(j8,K9,G9,d)
endif
if(l9==0)then
set b=IssueImmediateOrder(d,I9)
elseif(l9==1)then
set b=IssuePointOrder(d,I9,x,y)
elseif(l9==2)then
set b=IssueTargetOrder(d,I9,J9)
endif
set d=null
return b
endfunction
function rX takes rect r returns real
return GetRandomReal(GetRectMinX(r),GetRectMaxX(r))
endfunction
function rY takes rect r returns real
return GetRandomReal(GetRectMinY(r),GetRectMaxY(r))
endfunction
function N9 takes unit u returns real
return GetUnitState(u,UNIT_STATE_LIFE)/ LoadReal(j8,GetUnitTypeId(u),1214870608)*100.
endfunction
function O9 takes unit u,real P9 returns nothing
local integer T=GetUnitTypeId(u)
local real Q9=LoadReal(j8,T,1214870608)
local real R9=Q9*(P9/ 100)
if(R9>Q9)then
set R9=Q9
endif
call SetUnitState(u,UNIT_STATE_LIFE,R9)
endfunction
function S9 takes unit u,real P9 returns nothing
local integer T=GetUnitTypeId(u)
local real Q9=LoadReal(j8,T,1214870608)
local real hp=GetUnitState(u,UNIT_STATE_LIFE)
local real R9=hp+Q9*P9/ 100
if(R9>Q9)then
set R9=Q9
endif
call SetUnitState(u,UNIT_STATE_LIFE,R9)
endfunction
function T9 takes unit m returns nothing
call SetUnitAnimation(m,"stand")
call PauseUnit(m,true)
endfunction
function U9 takes unit u returns nothing
local integer H=GetHandleId(u)
local boolean b=not(LoadBoolean(j8,H,1400141166)or LoadBoolean(j8,H,1697656884))
if b then
call PauseUnit(u,false)
endif
endfunction
function V9 takes unit u returns nothing
local integer H=GetHandleId(u)
local integer T=GetUnitTypeId(u)
local real W9=GetUnitDefaultMoveSpeed(u)
local real b=LoadReal(j8,H,1093677125)+LoadReal(j8,H,1110454320)+LoadReal(j8,H,2003985456)+LoadReal(j8,H,1865429041)+LoadReal(j8,H,1093677140)
if(b!=0)then
set W9=W9*(1+b/ 100)
endif
call SetUnitMoveSpeed(u,W9)
endfunction
function X9 takes unit u returns real
local integer H=GetHandleId(u)
local integer T=GetUnitTypeId(u)
local real b=LoadInteger(j8,T,1346466413)+LoadInteger(j8,H,1093677126)+LoadInteger(j8,H,2003985457)+LoadInteger(j8,H,1093677140)
local boolean Y9=LoadBoolean(j8,T,1346466413)or LoadBoolean(j8,H,1093677146)or LoadBoolean(j8,H,1093677382)
if(b>75)then
set b=75
elseif(b<-25)then
set b=-25
endif
if Y9 then
set b=100
endif
return b/ 100
endfunction
function Z9 takes unit u returns real
local integer H=GetHandleId(u)
local integer T=GetUnitTypeId(u)
local real b=LoadInteger(j8,T,1296134765)+LoadInteger(j8,H,1110454324)+LoadInteger(j8,H,2003985457)+LoadInteger(j8,H,1093677140)
local boolean Y9=LoadBoolean(j8,T,1296134765)or LoadBoolean(j8,H,1093677128)or LoadBoolean(j8,H,1093677658)
if(b>75)then
set b=75
elseif(b<-25)then
set b=-25
endif
if Y9 then
set b=100
endif
return b/ 100
endfunction
function dd takes integer H returns boolean
return LoadBoolean(j8,H,1093677648)or LoadBoolean(j8,H,1865429044)or LoadBoolean(j8,H,1697656884)
endfunction
function ed takes unit DD,unit m,real q9,integer t returns nothing
local integer i=(GetPlayerId(GetOwningPlayer((DD))))
local integer T=GetUnitTypeId(m)
local integer H=GetHandleId(m)
local real Q9=LoadReal(j8,T,1214870608)
call SaveUnitHandle(j8,H,r8[i],DD)
call SaveInteger(j8,r8[i],1148020564,t)
if(t==1349024115)then
set q9=q9*(1-X9(m))
elseif(t==1298229091)then
set q9=q9*(1-Z9(m))
elseif(t==1348825699)then
set q9=Q9*q9/ 100
endif
if dd(H)then
set q9=0
endif
if(q9>0)then
call UnitDamageTarget(q8[i],m,q9,true,false,ATTACK_TYPE_NORMAL,DAMAGE_TYPE_UNIVERSAL,WEAPON_TYPE_WHOKNOWS)
endif
endfunction
function fd takes unit DD,unit m,real q9,integer t returns nothing
local integer i=(GetPlayerId(GetOwningPlayer((DD))))
local integer H=GetHandleId(m)
local integer n=0
if LoadBoolean(j8,H,1093677655)then
set q9=q9/(LN[i]+1)
loop
exitwhen n>LN[i]
set m=G8[(i)*(50)+n]
call ed(DD,m,q9,t)
set n=n+1
endloop
else
call ed(DD,m,q9,t)
endif
endfunction
function gd takes unit hd,unit J9,integer id returns boolean
local integer H=GetHandleId(J9)
local integer jd=GetUnitTypeId(J9)
local integer T=GetUnitTypeId(hd)
local real d=j9(J9,hd)
local boolean kd=(T==1747988531 and not LoadBoolean(j8,H,1093677648))
local boolean md=(T==1747988532 and not(GetUnitAbilityLevel((J9),(1113812070))>0)and d<=512)
local boolean nd=(T==1747988533 and not(GetUnitAbilityLevel((J9),(1097295983))>0))
local boolean od=(T==1865429043 and not HaveSavedInteger(j8,H,1093677140))
local boolean pd=(T==1865429045 and not LoadBoolean(j8,H,1093677655))
local boolean qd=(T==1865429046 and not LoadBoolean(j8,H,1093677656)and d<=$80)
local boolean rd=(T==1966092338 and not LoadBoolean(j8,H,1093677382))
local boolean sd=(T==1966092339 and d<=512)
local boolean td=(T==1966092340 and not(GetUnitAbilityLevel((J9),(1110454326))>0))and(d<=512)
local boolean ud=(T==1966092341 and not LoadBoolean(j8,H,1093677658))
local boolean vd=(T==1697656883 and LoadInteger(j8,H,1093677872)==0)
local boolean wd=(T==1697656885 and LoadInteger(j8,H,1093677143)==0)
local boolean xd=(T==1697656886 and jd!=1697656886 and d<=256)
local boolean yd=(kd or nd or od or pd or rd)and(d<=512)
local boolean zd=(kd or nd or od or pd or ud)and(d<=512)
local boolean Ad=(kd or nd or od or pd)and(d<=512)
local boolean ad=(vd or wd)and(d<=512)
local boolean Bd=md or qd or td or xd
local boolean bd=sd
local boolean b=false
if(id==1349024115)then
set b=yd
elseif(id==1298229091)then
set b=zd
elseif(id==1348825699)then
set b=Ad
elseif(id==1214603628)then
set b=ad
elseif(id==1416591218)then
set b=Bd
elseif(id==1147494756)then
set b=bd
endif
return b
endfunction
function Cd takes integer i,integer id,unit J9 returns boolean
local real x0=GetUnitX(J9)
local real y0=GetUnitY(J9)
local integer at
local string cd
local unit hd
local integer T
local integer a=0
local boolean b=false
loop
exitwhen(a>FN[i])or b
set hd=F8[(i)*(50)+a]
set T=GetUnitTypeId(hd)
set b=false
if HaveSavedInteger(j8,T,1096968556)then
set b=gd(hd,J9,id)
endif
if b then
set at=LoadInteger(j8,T,1096968532)
set cd=LoadStr(j8,T,1330869362)
if(at==0)then
set b=IssueImmediateOrder(hd,cd)
elseif(at==1)then
set b=IssuePointOrder(hd,cd,x0,y0)
elseif(at==2)then
set b=IssueTargetOrder(hd,cd,J9)
endif
endif
set a=a+1
endloop
set hd=null
return b
endfunction
function Dd takes unit m returns nothing
local integer H=GetHandleId(m)
local integer a=0
local timer t
local integer tH
loop
exitwhen a>AT
set t=CreateTimer()
set tH=GetHandleId(t)
call SaveTimerHandle(j8,H,Y7[a],t)
call SaveUnitHandle(j8,tH,0,m)
set a=a+1
endloop
set t=null
endfunction
function Ed takes unit m,integer i returns nothing
local integer H=GetHandleId(m)
local texttag z9=CreateTextTag()
if(GetLocalPlayer()!=Player(i))then
call SetTextTagVisibility(z9,false)
endif
call SaveTextTagHandle(j8,H,1415936116,z9)
set z9=null
endfunction
function Fd takes unit m returns nothing
local integer H=GetHandleId(m)
local real Gd=GetUnitState(m,UNIT_STATE_LIFE)
local integer hp=R2I(Gd)
local integer pp=R2I(X9(m)*100)
local integer mp=R2I(Z9(m)*100)
local boolean Y9=dd(H)
local real Hd=N9(m)
local texttag z9=LoadTextTagHandle(j8,H,1415936116)
local string s=""
if(pp!=0)and(pp!=100)then
set s=s+"|cffff8700"+I2S(pp)+"|r
"
elseif(pp==100)then
set s=s+"|cffff8700Immunity|r
"
endif
if(mp!=0)and(mp!=100)then
set s=s+"|cff8700ff"+I2S(mp)+"|r
"
elseif(mp==100)then
set s=s+"|cff8700ffImmunity|r
"
endif
if(Hd>=$C8/ 3)then
set s=s+"|cff00ff00"
elseif(Hd>100/ 3)then
set s=s+"|cffffff00"
else
set s=s+"|cffff0000"
endif
if(hp<Gd)then
set hp=hp+1
endif
set s=s+I2S(hp)+"|r"
if Y9 then
set s="|cffccccccProtected|r"
endif
if(z9!=null)then
call SetTextTagText(z9,s,9*.0024)
call SetTextTagPosUnit(z9,m,0)
endif
set z9=null
endfunction
function Id takes unit m returns boolean
local integer H=GetHandleId(m)
local integer n=0
loop
exitwhen(n>C0)or(j9(m,e8[n])<=$80)
set n=n+1
endloop
if(n<=C0)then
call SaveReal(j8,H,2003985456,-20)
else
call RemoveSavedReal(j8,H,2003985456)
endif
return n<=C0
endfunction
function ld takes unit m returns nothing
local integer H=GetHandleId(m)
local integer n=0
loop
exitwhen(n>C1)or(j9(m,f8[n])<=$80)
set n=n+1
endloop
if(n<=C1)then
call SaveInteger(j8,H,2003985457,-$F)
else
call RemoveSavedInteger(j8,H,2003985457)
endif
endfunction
function Jd takes unit m returns nothing
local integer n=0
local boolean b=false
local integer H=GetHandleId(m)
local integer Kd=LoadInteger(j8,H,2003985458)
local real d=0
local real d0=$80
local integer n0=0
local unit t=null
loop
exitwhen(n>C2)
set d=j9(m,g8[n])
if(d0>=d)then
set d0=d
set n0=n
if not b then
set b=true
endif
endif
set n=n+1
endloop
if b then
if(Kd==R2I(1/ .02))then
set Kd=0
endif
call SaveInteger(j8,H,2003985458,Kd+1)
if(Kd==0)then
set H=GetHandleId(g8[n0])
set t=LoadUnitHandle(j8,H,0)
call fd(t,m,3,1348825699)
set t=null
endif
else
call RemoveSavedInteger(j8,H,2003985458)
endif
endfunction
function Ld takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer H=GetHandleId(t)
local unit m=LoadUnitHandle(j8,H,0)
local integer Md=$FF
local integer Nd=$FF
local integer Od=0
set H=GetHandleId(m)
if HaveSavedReal(j8,H,1093677125)then
set Md=$80
set Nd=$CC
endif
if(GetUnitAbilityLevel((m),(1110454320))>0)then
set Md=$80
set Nd=$CC
call SaveReal(j8,H,1110454320,-20)
else
call RemoveSavedReal(j8,H,1110454320)
endif
if(GetUnitAbilityLevel((m),(1110454324))>0)then
call SaveInteger(j8,H,1110454324,-$F)
else
call RemoveSavedInteger(j8,H,1110454324)
endif
if Id(m)then
set Md=$80
set Nd=$CC
endif
call ld(m)
call Jd(m)
if(LoadBoolean(j8,H,1093677146)or LoadBoolean(j8,H,1093677128))and not IsUnitPaused(m)then
call SetUnitAnimation(m,"walk defend")
endif
set Od=LoadInteger(j8,H,1097623137)
call SetUnitVertexColor(m,Md,Nd,$FF,Od)
call V9(m)
call Fd(m)
set m=null
set t=null
endfunction
function Pd takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(j8,tH,0)
local integer mH=GetHandleId(m)
call RemoveSavedReal(j8,mH,1093677125)
set t=null
set m=null
endfunction
function Qd takes unit m returns nothing
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(j8,mH,1093677125)
local integer tH=GetHandleId(t)
call SaveReal(j8,mH,1093677125,-20)
call TimerStart(t,3,false,function Pd)
set t=null
endfunction
function Rd takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(j8,tH,0)
local integer mH=GetHandleId(m)
local effect e=LoadEffectHandle(j8,tH,1)
call RemoveSavedInteger(j8,mH,1093677126)
call RemoveSavedHandle(j8,tH,1)
call DestroyEffect(e)
set t=null
set m=null
set e=null
endfunction
function Sd takes unit m returns nothing
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(j8,mH,1093677126)
local integer tH=GetHandleId(t)
local effect e
if not HaveSavedHandle(j8,tH,1)then
call SaveInteger(j8,mH,1093677126,-$F)
set e=AddSpecialEffectTarget("Abilities\\Spells\\Other\\HowlOfTerror\\HowlTarget.mdx",m,"origin")
call SaveEffectHandle(j8,tH,1,e)
set e=null
endif
call TimerStart(t,3,false,function Rd)
set t=null
endfunction
function Td takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(j8,tH,0)
local integer mH=GetHandleId(m)
local effect e=LoadEffectHandle(j8,tH,1)
call RemoveSavedBoolean(j8,mH,1400141166)
call U9(m)
call RemoveSavedHandle(j8,tH,1)
call DestroyEffect(e)
set t=null
set m=null
set e=null
endfunction
function Ud takes unit m,real n9 returns nothing
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(j8,mH,1400141166)
local integer tH=GetHandleId(t)
local effect e
if not HaveSavedHandle(j8,tH,1)then
call T9(m)
call SaveBoolean(j8,mH,1400141166,true)
set e=AddSpecialEffectTarget("Abilities\\Spells\\Human\\Thunderclap\\ThunderclapTarget.mdl",m,"overhead")
call SaveEffectHandle(j8,tH,1,e)
set e=null
endif
call TimerStart(t,n9,false,function Td)
set t=null
endfunction
function Vd takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(j8,tH,0)
local integer mH=GetHandleId(m)
local integer T=GetUnitTypeId(m)
local integer H9=LoadInteger(j8,T,1096968556)
local effect e=LoadEffectHandle(j8,tH,1)
call UnitAddAbility(m,H9)
call RemoveSavedHandle(j8,tH,1)
call DestroyEffect(e)
set t=null
set m=null
set e=null
endfunction
function Wd takes unit m returns nothing
local integer mH=GetHandleId(m)
local integer T=GetUnitTypeId(m)
local integer H9=LoadInteger(j8,T,1096968556)
local timer t=LoadTimerHandle(j8,mH,1093677106)
local integer tH=GetHandleId(t)
local effect e
if not HaveSavedHandle(j8,tH,1)then
call UnitRemoveAbility(m,H9)
set e=AddSpecialEffectTarget("Abilities\\Spells\\Other\\Silence\\SilenceTarget.mdx",m,"overhead")
call SaveEffectHandle(j8,tH,1,e)
set e=null
endif
call TimerStart(t,3,false,function Vd)
set t=null
endfunction
function Xd takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer H=GetHandleId(t)
local unit m=LoadUnitHandle(j8,H,0)
local real r=100-N9(m)
set H=GetHandleId(m)
if(r<50)then
call SaveReal(j8,H,1865429041,r)
else
call SaveReal(j8,H,1865429041,100)
endif
set t=null
set m=null
endfunction
function Yd takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(j8,tH,0)
local integer mH=GetHandleId(m)
local effect e=LoadEffectHandle(j8,tH,1)
call RemoveSavedBoolean(j8,mH,1865429044)
call RemoveSavedHandle(j8,tH,1)
call DestroyEffect(e)
call TimerStart(t,3,false,null)
set t=null
set m=null
set e=null
endfunction
function Zd takes unit m,real de returns nothing
local real hp=GetUnitState(m,UNIT_STATE_LIFE)
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(j8,mH,1865429044)
local integer tH=GetHandleId(t)
local real ee=TimerGetRemaining(t)
local effect e
if(ee==0)then
call SetUnitState(m,UNIT_STATE_LIFE,hp+de)
call SaveBoolean(j8,mH,1865429044,true)
set e=AddSpecialEffectTarget("Abilities\\Spells\\Orc\\Voodoo\\VoodooAuraTarget.mdx",m,"overhead")
call SaveEffectHandle(j8,tH,1,e)
set e=null
call TimerStart(t,2,false,function Yd)
endif
set t=null
endfunction
function fe takes integer i,integer ix returns nothing
set G8[(i)*(50)+ix]=G8[(i)*(50)+LN[i]]
set G8[(i)*(50)+LN[i]]=null
set LN[i]=LN[i]-1
call SaveInteger(j8,GetHandleId(G8[(i)*(50)+ix]),1280199032,ix)
endfunction
function ge takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(j8,tH,0)
local integer mH=GetHandleId(m)
local integer i=LoadInteger(j8,mH,1181705552)
local integer ix=LoadInteger(j8,mH,1280199032)
local effect e=LoadEffectHandle(j8,tH,1)
call RemoveSavedBoolean(j8,mH,1093677655)
call RemoveSavedHandle(j8,tH,1)
call DestroyEffect(e)
call fe(i,ix)
set t=null
set m=null
set e=null
endfunction
function he takes unit m returns nothing
local integer mH=GetHandleId(m)
local integer i=LoadInteger(j8,mH,1181705552)
local timer t=LoadTimerHandle(j8,mH,1093677655)
local integer tH=GetHandleId(t)
local effect e
if not HaveSavedHandle(j8,tH,1)then
call SaveBoolean(j8,mH,1093677655,true)
set e=AddSpecialEffectTarget("Abilities\\Spells\\Orc\\SpiritLink\\SpiritLinkTarget.mdx",m,"chest")
call SaveEffectHandle(j8,tH,1,e)
set e=null
set LN[i]=LN[i]+1
set G8[(i)*(50)+LN[i]]=m
call SaveInteger(j8,mH,1280199032,LN[i])
endif
call TimerStart(t,3,false,function ge)
set t=null
endfunction
function ie takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(j8,tH,0)
call S9(m,5*.25)
set t=null
set m=null
endfunction
function je takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(j8,tH,0)
local integer mH=GetHandleId(m)
local real x=LoadReal(j8,mH,1131570264)
local real y=LoadReal(j8,mH,1131570265)
call RemoveSavedBoolean(j8,mH,1697656884)
call U9(m)
call O9(m,100)
call IssuePointOrderById(m,$D0012,x,y)
call SaveInteger(j8,mH,1097623137,$FF)
call TimerStart(t,3,false,null)
set t=null
set m=null
endfunction
function ke takes unit m,real de returns nothing
local real hp=GetUnitState(m,UNIT_STATE_LIFE)
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(j8,mH,1697656884)
local integer tH=GetHandleId(t)
local real ee=TimerGetRemaining(t)
if(ee==0)then
call SetUnitState(m,UNIT_STATE_LIFE,hp+de)
call SaveInteger(j8,mH,1097623137,0)
call T9(m)
call SaveBoolean(j8,mH,1697656884,true)
call TimerStart(t,1,false,function je)
endif
set t=null
endfunction
function me takes integer H returns nothing
local unit array d
local integer k=-1
local integer a=0
local integer dH
set k=k+1
set d[k]=LoadUnitHandle(j8,H,1685417264)
set k=k+1
set d[k]=LoadUnitHandle(j8,H,1685417265)
set k=k+1
set d[k]=LoadUnitHandle(j8,H,1685417266)
loop
exitwhen a>k
if(d[k]!=null)then
set dH=GetHandleId(d[k])
call FlushChildHashtable(j8,dH)
call RemoveUnit(d[k])
set d[k]=null
endif
set a=a+1
endloop
endfunction
function ne takes integer i,integer ix returns nothing
set F8[(i)*(50)+ix]=F8[(i)*(50)+FN[i]]
set F8[(i)*(50)+FN[i]]=null
set FN[i]=FN[i]-1
call SaveInteger(j8,GetHandleId(F8[(i)*(50)+ix]),1179535736,ix)
endfunction
function oe takes integer H returns nothing
local integer a=0
local timer t
local integer tH
local effect e
loop
exitwhen a>AT
set t=LoadTimerHandle(j8,H,Y7[a])
if(t!=null)then
set tH=GetHandleId(t)
set e=LoadEffectHandle(j8,tH,1)
if(e!=null)then
call DestroyEffect(e)
endif
set e=LoadEffectHandle(j8,tH,2)
if(e!=null)then
call DestroyEffect(e)
endif
call FlushChildHashtable(j8,tH)
call DestroyTimer(t)
endif
set a=a+1
endloop
set t=null
set e=null
endfunction
function pe takes unit m returns nothing
local integer n=GetHandleId(m)
local texttag z9=LoadTextTagHandle(j8,n,1415936116)
local integer i=LoadInteger(j8,n,1333227088)
local integer ix=LoadInteger(j8,n,1179535736)
local boolean b=LoadBoolean(j8,n,1093677655)
call DestroyTextTag(z9)
call oe(n)
call FlushChildHashtable(j8,n)
call SetUnitAnimation(m,"death")
call ne(i,ix)
if b then
set ix=LoadInteger(j8,n,1280199032)
call fe(i,ix)
endif
set z9=null
set n=0
loop
exitwhen n>$A or(w8[n]and FN[n]!=-1)
set n=n+1
endloop
if n>$A then
call TriggerExecute(e7)
endif
endfunction
function qe takes integer i,integer ix returns nothing
set I8[(i)*(56)+ix]=I8[(i)*(56)+TN[i]]
set I8[(i)*(56)+TN[i]]=null
set TN[i]=TN[i]-1
call SaveInteger(j8,GetHandleId(I8[(i)*(56)+ix]),1414416760,ix)
endfunction
function re takes integer H returns nothing
local timer t=LoadTimerHandle(j8,H,1093677656)
local integer tH=GetHandleId(t)
local effect e=LoadEffectHandle(j8,tH,1)
if(t!=null)then
call DestroyEffect(e)
call FlushChildHashtable(j8,tH)
call DestroyTimer(t)
endif
set t=null
set e=null
endfunction
function se takes unit t returns nothing
local integer H=GetHandleId(t)
local integer i=(GetPlayerId(GetOwningPlayer((t))))
local integer ix=LoadInteger(j8,H,1414416760)
local effect e=LoadEffectHandle(j8,H,1281717868)
if(e!=null)then
call DestroyEffect(e)
endif
call re(H)
call me(H)
call FlushChildHashtable(j8,H)
call qe(i,ix)
call RemoveUnit(t)
set e=null
endfunction
function te takes unit t,boolean ue returns nothing
local integer T=GetUnitTypeId(t)
local integer c=LoadInteger(j8,T,1131377524)
local integer i=(GetPlayerId(GetOwningPlayer((t))))
local real x=GetUnitX(t)
local real y=GetUnitY(t)
local effect e
if ue then
set e=AddSpecialEffect("Objects\\Spawnmodels\\Human\\HCancelDeath\\HCancelDeath.mdx",x,y)
call DestroyEffect(e)
set e=null
endif
if(c>0)then
call o9(i,c,false)
if ue then
call v9("|cFFFFFF00+"+I2S(c)+"|r",t,8.5,64,i,2,1)
endif
endif
endfunction
function ve takes nothing returns nothing
set m8=m8-1
if(m8==0)then
call PauseTimer(D8)
call MultiboardSetTitleText(Z7,"Wave "+I2S(o8))
call MultiboardSetItemsIcon(Z7,"Icons\\PASReadiness.blp")
call TriggerExecute(d7)
return
endif
if(m8<4)then
set d8="|cFFFF0000"+I2S(m8)+"|r"
call StartSound(G4)
else
set d8=I2S(m8)
endif
if(m8==1)then
set d8=d8+" second"
else
set d8=d8+" seconds"
endif
set d8="Wave "+I2S(o8)+" starts in "+d8
call MultiboardSetTitleText(Z7,d8)
endfunction
function xe takes integer i,boolean b returns nothing
local player p=Player(i)
local integer a=0
if b then
set a=-1
endif
call SetPlayerAbilityAvailable(p,1093677105,b)
call SetPlayerAbilityAvailable(p,1093677110,b)
call SetPlayerAbilityAvailable(p,1093677113,b)
call SetPlayerTechMaxAllowed(p,1953462065,a)
call SetPlayerTechMaxAllowed(p,1953462066,a)
call SetPlayerTechMaxAllowed(p,1953462067,a)
call SetPlayerTechMaxAllowed(p,1953462068,a)
call SetPlayerTechMaxAllowed(p,1953462069,a)
call SetPlayerTechMaxAllowed(p,1953462070,a)
call SetPlayerTechMaxAllowed(p,1953462071,a)
call SetPlayerTechMaxAllowed(p,1953462072,a)
call SetPlayerTechMaxAllowed(p,1953462073,a)
call SetPlayerAbilityAvailable(p,1093677141,b)
call SetPlayerAbilityAvailable(p,1097347120,b)
call SetPlayerAbilityAvailable(p,1097347121,b)
call SetPlayerAbilityAvailable(p,1097347122,b)
call SetPlayerAbilityAvailable(p,1097347123,b)
call SetPlayerAbilityAvailable(p,1097347124,b)
call SetPlayerAbilityAvailable(p,1097347125,b)
call SetPlayerAbilityAvailable(p,1097347126,b)
call SetPlayerAbilityAvailable(p,1093677366,b)
call SetPlayerAbilityAvailable(p,1097805872,b)
call SetPlayerAbilityAvailable(p,1097805873,b)
call SetPlayerAbilityAvailable(p,1097805874,b)
call SetPlayerAbilityAvailable(p,1097805875,b)
call SetPlayerAbilityAvailable(p,1097805876,b)
call SetPlayerAbilityAvailable(p,1097805877,b)
call SetPlayerAbilityAvailable(p,1097805878,b)
call SetPlayerAbilityAvailable(p,1093677381,b)
call SetPlayerAbilityAvailable(p,1098199088,b)
call SetPlayerAbilityAvailable(p,1098199089,b)
call SetPlayerAbilityAvailable(p,1098199090,b)
call SetPlayerAbilityAvailable(p,1098199091,b)
call SetPlayerAbilityAvailable(p,1098199092,b)
call SetPlayerAbilityAvailable(p,1098199093,b)
call SetPlayerAbilityAvailable(p,1098199094,b)
call SetPlayerAbilityAvailable(p,1093677367,b)
call SetPlayerAbilityAvailable(p,1097150512,b)
call SetPlayerAbilityAvailable(p,1097150513,b)
call SetPlayerAbilityAvailable(p,1097150514,b)
call SetPlayerAbilityAvailable(p,1097150515,b)
call SetPlayerAbilityAvailable(p,1097150516,b)
call SetPlayerAbilityAvailable(p,1097150517,b)
call SetPlayerAbilityAvailable(p,1097150518,b)
set p=null
endfunction
function ye takes integer i,boolean b returns nothing
local integer a=0
local integer ze=0
if b then
set ze=$FF
endif
loop
exitwhen a>55
call SetUnitVertexColor(H8[(i)*(56)+a],$FF,$FF,$FF,ze)
set a=a+1
endloop
endfunction
function Ae takes trigger t,playerunitevent ev returns nothing
call TriggerRegisterPlayerUnitEvent(t,Player(0),ev,null)
call TriggerRegisterPlayerUnitEvent(t,Player(1),ev,null)
call TriggerRegisterPlayerUnitEvent(t,Player(2),ev,null)
call TriggerRegisterPlayerUnitEvent(t,Player(3),ev,null)
call TriggerRegisterPlayerUnitEvent(t,Player(4),ev,null)
call TriggerRegisterPlayerUnitEvent(t,Player(5),ev,null)
call TriggerRegisterPlayerUnitEvent(t,Player(6),ev,null)
call TriggerRegisterPlayerUnitEvent(t,Player(7),ev,null)
call TriggerRegisterPlayerUnitEvent(t,Player(8),ev,null)
call TriggerRegisterPlayerUnitEvent(t,Player(9),ev,null)
call TriggerRegisterPlayerUnitEvent(t,Player($A),ev,null)
endfunction
function ae takes integer i returns string
return h8[i]+GetPlayerName(Player(i))+"|r"
endfunction
function Be takes nothing returns nothing
local integer i=0
local integer n=0
loop
exitwhen i>$A
if w8[i]and LoadBoolean(j8,i,1382375780)then
set n=n+1
endif
set i=i+1
endloop
if(n==p8)and(m8>4)then
set m8=4
endif
endfunction
function be takes integer i returns nothing
set E8=i
call TriggerExecute(Z4)
endfunction
function Ce takes nothing returns nothing
local integer a=0
local player p
local fogmodifier f
loop
exitwhen a>$A
set p=Player(a)
if(GetPlayerSlotState(p)==PLAYER_SLOT_STATE_PLAYING)then
set p8=p8+1
set w8[a]=true
set FN[a]=-1
set LN[a]=-1
set TN[a]=-1
call TriggerRegisterPlayerEvent(Y4,p,EVENT_PLAYER_LEAVE)
endif
set a=a+1
endloop
set a=0
loop
exitwhen w8[a]
set a=a+1
endloop
set p=Player(a)
call TriggerRegisterPlayerChatEvent(T4,p,"start",true)
call TriggerRegisterPlayerChatEvent(S4,p,"",false)
set p=Player($B)
set f=CreateFogModifierRect(p,FOG_OF_WAR_VISIBLE,bj_mapInitialPlayableArea,false,true)
call FogModifierStart(f)
call EnableWorldFogBoundary(false)
call SetGameSpeed(MAP_SPEED_FASTEST)
call SetMapFlag(MAP_LOCK_SPEED,true)
call SetMapFlag(MAP_USE_HANDICAPS,false)
call SetFloatGameState(GAME_STATE_TIME_OF_DAY,$C)
call SuspendTimeOfDay(true)
call SetAllyColorFilterState(0)
call SetCreepCampFilterState(false)
call EnableMinimapFilterButtons(true,false)
call EnablePreSelect(true,false)
call SetWaterBaseColor(0,0,0,$FF)
if(bj_dayAmbientSound!=null)then
call StopSound(bj_dayAmbientSound,true,false)
endif
call StopMusic(false)
call ClearMapMusic()
set j8=InitHashtable()
call TriggerExecute(J4)
call TriggerExecute(K4)
call TriggerExecute(L4)
call TriggerExecute(M4)
call TriggerExecute(N4)
call TriggerExecute(O4)
call TriggerExecute(P4)
call TriggerExecute(Q4)
set p=null
set f=null
endfunction
function De takes string s returns nothing
local integer l=StringLength(s)
local string Ee=SubString(s,l-3,l-1)
local effect e
call Preload(s)
if(Ee=="md")or(Ee=="MD")then
set e=AddSpecialEffect(s,0,0)
call DestroyEffect(e)
endif
set e=null
endfunction
function Fe takes nothing returns nothing
call De("Sound\\Ambient\\LordaeronSummer\\LordaeronSummerDay.mid")
call De("ReplaceableTextures\\CommandButtons\\BTNMove.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNMove.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNStop.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNStop.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNPatrol.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNPatrol.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNHoldPosition.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNHoldPosition.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNCancel.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNCancel.blp")
call De("Icons\\ArrowTower\\PASBash.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASBash.blp")
call De("Icons\\ArrowTower\\PASChillingArrows.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASChillingArrows.blp")
call De("Icons\\ArrowTower\\PASCriticalShot.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASCriticalShot.blp")
call De("Icons\\ArrowTower\\PASMultishot.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASMultishot.blp")
call De("Icons\\ArrowTower\\PASPiercingArrows.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASPiercingArrows.blp")
call De("Icons\\ArrowTower\\PASPoisonedArrows.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASPoisonedArrows.blp")
call De("Icons\\ArrowTower\\PASSearingArrows.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASSearingArrows.blp")
call De("Icons\\ArcaneTower\\PASBloodMagic.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASBloodMagic.blp")
call De("Icons\\ArcaneTower\\PASHarmfulMagic.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASHarmfulMagic.blp")
call De("Icons\\ArcaneTower\\PASPainStrike.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASPainStrike.blp")
call De("Icons\\ArcaneTower\\PASRainOfFire.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASRainOfFire.blp")
call De("Icons\\ArcaneTower\\PASRainOfIce.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASRainOfIce.blp")
call De("Icons\\ArcaneTower\\PASRainOfMagic.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASRainOfMagic.blp")
call De("Icons\\ArcaneTower\\PASSilenceStrike.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASSilenceStrike.blp")
call De("Icons\\ArcaneTower\\PASThunderStrike.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASThunderStrike.blp")
call De("Icons\\CannonTower\\PASBlackPowder.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASBlackPowder.blp")
call De("Icons\\CannonTower\\PASCloudOfDestruction.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASCloudOfDestruction.blp")
call De("Icons\\CannonTower\\PASCloudOfIce.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASCloudOfIce.blp")
call De("Icons\\CannonTower\\PASCloudOfPoison.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASCloudOfPoison.blp")
call De("Icons\\CannonTower\\PASCloudiness.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASCloudiness.blp")
call De("Icons\\CannonTower\\PASHeavyImpact.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASHeavyImpact.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNAntiMagicShell.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNAntiMagicShell.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNBloodLustOn.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNBloodLustOn.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNCannibalize.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNCannibalize.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNCloudOfFog.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNCloudOfFog.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNDefend.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNDefend.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNInnerFireOn.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNInnerFireOn.blp")
call De("ReplaceableTextures\\PassiveButtons\\PASBTNFreezingBreath.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASBTNFreezingBreath.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNFrostArmor.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNFrostArmor.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNInvisibility.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNInvisibility.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNSpellBreakerMagicDefend.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNSpellBreakerMagicDefend.blp")
call De("ReplaceableTextures\\PassiveButtons\\PASBTNSmash.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISPASBTNSmash.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNRaiseDeadOn.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNRaiseDeadOn.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNRejuvenation.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNRejuvenation.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNBattleRoar.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNBattleRoar.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNSpiritLink.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNSpiritLink.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNTaunt.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNTaunt.blp")
call De("Icons\\BTNHuman.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNHuman.blp")
call De("Icons\\BTNNightElf.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNNightElf.blp")
call De("Icons\\BTNOrc.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNOrc.blp")
call De("Icons\\BTNUndead.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNUndead.blp")
call De("Icons\\BTNDeleteMinion.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNDeleteMinion.blp")
call De("Icons\\BTNDestroyTower.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNDestroyTower.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNMilitia.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNMilitia.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNFootman.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNFootman.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNKnight.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNKnight.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNPriest.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNPriest.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNDragonHawk.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNDragonHawk.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNSorceress.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNSorceress.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNSpellBreaker.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNSpellBreaker.blp")
call De("Icons\\ArcaneTower\\BTNArcaneTower1.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNArcaneTower1.blp")
call De("Icons\\ArcaneTower\\BTNRainOfIce.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNRainOfIce.blp")
call De("Icons\\ArcaneTower\\BTNRainOfMagic.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNRainOfMagic.blp")
call De("Icons\\ArcaneTower\\BTNRainOfFire.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNRainOfFire.blp")
call De("Icons\\ArrowTower\\BTNArrowTower1.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNArrowTower1.blp")
call De("Icons\\ArrowTower\\BTNChillingArrows.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNChillingArrows.blp")
call De("Icons\\ArrowTower\\BTNPiercingArrows.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNPiercingArrows.blp")
call De("Icons\\ArrowTower\\BTNPoisonedArrows.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNPoisonedArrows.blp")
call De("Icons\\CannonTower\\BTNCannonTower1.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNCannonTower1.blp")
call De("Icons\\CannonTower\\BTNCloudOfIce.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNCloudOfIce.blp")
call De("Icons\\CannonTower\\BTNCloudOfDestruction.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNCloudOfDestruction.blp")
call De("Icons\\CannonTower\\BTNCloudOfPoison.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNCloudOfPoison.blp")
call De("Icons\\ArcaneTower\\BTNArcaneTower2.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNArcaneTower2.blp")
call De("Icons\\ArcaneTower\\BTNSilenceStrike.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNSilenceStrike.blp")
call De("Icons\\ArcaneTower\\BTNPainStrike.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNPainStrike.blp")
call De("Icons\\ArcaneTower\\BTNThunderStrike.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNThunderStrike.blp")
call De("Icons\\ArrowTower\\BTNArrowTower2.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNArrowTower2.blp")
call De("Icons\\ArrowTower\\BTNSearingArrows.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNSearingArrows.blp")
call De("Icons\\ArrowTower\\BTNCriticalShot.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNCriticalShot.blp")
call De("Icons\\ArrowTower\\BTNBash.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNBash.blp")
call De("Icons\\CannonTower\\BTNCannonTower2.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNCannonTower2.blp")
call De("Icons\\CannonTower\\BTNBlackPowder.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNBlackPowder.blp")
call De("Icons\\CannonTower\\BTNHeavyImpact.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNHeavyImpact.blp")
call De("Icons\\CannonTower\\BTNCloudiness.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNCloudiness.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNArcher.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNArcher.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNEnt.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNEnt.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNDryad.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNDryad.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNDruidOfTheTalon.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNDruidOfTheTalon.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNFaerieDragon.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNFaerieDragon.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNDruidOfTheClaw.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNDruidOfTheClaw.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNMountainGiant.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNMountainGiant.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNGrunt.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNGrunt.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNHeadHunterBerserker.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNHeadHunterBerserker.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNRaider.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNRaider.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNShaman.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNShaman.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNTrollBatRider.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNTrollBatRider.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNSpiritWalker.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNSpiritWalker.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNTauren.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNTauren.blp")
call De("Icons\\BTNReadiness.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNReadiness.blp")
call De("Icons\\BTNShowDetails.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNShowDetails.blp")
call De("Icons\\BTNGrid.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNGrid.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNSkeletonWarrior.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNSkeletonWarrior.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNGhoul.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNGhoul.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNAbomination.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNAbomination.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNBanshee.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNBanshee.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNFrostWyrm.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNFrostWyrm.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNNecromancer.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNNecromancer.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNCryptFiend.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNCryptFiend.blp")
call De("Splats\\SplatData.slk")
call De("ReplaceableTextures\\Splats\\Splat01Mature.blp")
call De("UI\\TipStrings.txt")
call De("UI\\Widgets\\EscMenu\\Human\\human-options-button-border-up.blp")
call De("UI\\Widgets\\EscMenu\\Human\\human-options-button-border-down.blp")
call De("UI\\Widgets\\EscMenu\\Human\\human-options-button-background-disabled.blp")
call De("UI\\Widgets\\EscMenu\\Human\\editbox-background.blp")
call De("UI\\Widgets\\EscMenu\\Human\\editbox-border.blp")
call De("UI\\Widgets\\EscMenu\\Human\\slider-background.blp")
call De("UI\\Widgets\\EscMenu\\Human\\slider-border.blp")
call De("UI\\Widgets\\EscMenu\\Human\\slider-knob.blp")
call De("UI\\HelpStrings.txt")
call De("UI\\Widgets\\EscMenu\\Human\\slider-borderdisabled.blp")
call De("UI\\Widgets\\EscMenu\\Human\\slider-knobdisabled.blp")
call De("UI\\Widgets\\EscMenu\\Human\\checkbox-background.blp")
call De("UI\\Widgets\\EscMenu\\Human\\checkbox-depressed.blp")
call De("UI\\Widgets\\EscMenu\\Human\\checkbox-background-disabled.blp")
call De("UI\\Widgets\\Glues\\GlueScreen-PullDown-Arrow.blp")
call De("UI\\Widgets\\EscMenu\\Human\\radiobutton-background.blp")
call De("UI\\Widgets\\EscMenu\\Human\\radiobutton-background-disabled.blp")
call De("UI\\Widgets\\EscMenu\\Human\\alliance-gold.blp")
call De("UI\\Widgets\\EscMenu\\Human\\alliance-lumber.blp")
call De("UI\\Minimap\\minimap-hero.blp")
call De("war3mapMap.blp")
call De("UI\\Minimap\\minimap-gold.blp")
call De("UI\\Minimap\\minimap-gold-entangled.blp")
call De("UI\\Minimap\\minimap-gold-haunted.blp")
call De("UI\\Minimap\\minimap-neutralbuilding.blp")
call De("UI\\Minimap\\MinimapIconCircleOfPower.blp")
call De("Models\\Pavement1.mdx")
call De("Models\\Pavement2.mdx")
call De("units\\nightelf\\Wisp\\Wisp.mdx")
call De("Textures\\star2_32.blp")
call De("Textures\\Shockwave10.blp")
call De("Textures\\Dust3.blp")
call De("ReplaceableTextures\\Weather\\CloudSingleFlat.blp")
call De("Textures\\Dust5.blp")
call De("Textures\\GenericGlow2c.blp")
call De("Icons\\BTNBuildTower.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNBuildTower.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNAdvancedFrostTower.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNAdvancedFrostTower.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNHumanArcaneTower.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNHumanArcaneTower.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNGuardTower.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNGuardTower.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNAdvancedEnergyTower.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNAdvancedEnergyTower.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNDalaranGuardTower.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNDalaranGuardTower.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNTrollBurrow.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNTrollBurrow.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNAdvancedRockTower.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNAdvancedRockTower.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNAdvancedFlameTower.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNAdvancedFlameTower.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNOrcTower.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNOrcTower.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNHumanWatchTower.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNHumanWatchTower.blp")
call De("units\\nightelf\\Wisp\\Wisp_portrait.mdx")
call De("units\\NightElf\\Wisp\\Wisp.blp")
call De("Textures\\Dust5A.blp")
call De("ReplaceableTextures\\Selection\\SelectionCircleSmall.blp")
call De("UI\\Widgets\\Glues\\Gluescreen-Scrollbar-UpArrow.blp")
call De("UI\\Widgets\\EscMenu\\Human\\human-options-menu-background.blp")
call De("UI\\Widgets\\Console\\Human\\CommandButton\\human-activebutton.blp")
call De("UI\\Widgets\\Console\\Human\\infocard-armor-hero.blp")
call De("UI\\Widgets\\Console\\Human\\CommandButton\\human-button-lvls-overlay.blp")
call De("UI\\Widgets\\EscMenu\\Human\\observer-icon.blp")
call De("Icons\\PASPreparation.blp")
call De("Sound\\Interface\\MouseClick1.wav")
call De("buildings\\human\\HumanTower\\HumanTower.mdx")
call De("Textures\\Doodads0.blp")
call De("Textures\\Tower.blp")
call De("Textures\\DeathSmug.blp")
call De("Textures\\CloudSingle.blp")
call De("Textures\\Clouds8x8.blp")
call De("Textures\\CartoonCloud.blp")
call De("Textures\\Dust3x.blp")
call De("Textures\\RockParticle.blp")
call De("Textures\\SpinningBoard.blp")
call De("Textures\\HumanBase.blp")
call De("buildings\\Human\\HumanTower\\HumanMagicTower.blp")
call De("Textures\\Blue_Glow2.blp")
call De("Textures\\GenericGlow2b.blp")
call De("Sound\\Buildings\\Shared\\BuildingConstruction.wav")
call De("Units\\NightElf\\Wisp\\WispYes1.wav")
call De("Splats\\UberSplatData.slk")
call De("ReplaceableTextures\\Splats\\HumanUberSplat.blp")
call De("Units\\NightElf\\Wisp\\WispYes2.wav")
call De("Units\\NightElf\\Wisp\\WispYes3.wav")
call De("Sound\\Interface\\Warning\\Human\\PeasantCannotBuildThere1.wav")
call De("Sound\\Interface\\Error.wav")
call De("Sound\\Interface\\BattleNetTick.wav")
call De("Buildings\\Human\\HumanTower\\WatchTowerWhat1.wav")
call De("UI\\Widgets\\ToolTips\\Human\\ToolTipGoldIcon.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNAttack.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNAttack.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNAttackGround.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNAttackGround.blp")
call De("UI\\Widgets\\Console\\Human\\infocard-neutral-attack-piercing.blp")
call De("Icons\\PASReadiness.blp")
call De("Abilities\\Spells\\Items\\ResourceItems\\ReceiveGold.wav")
call De("Sound\\Interface\\Warning\\Human\\KnightNoGold1.wav")
call De("Sound\\Interface\\GamePause.wav")
call De("UI\\Widgets\\EscMenu\\Human\\human-options-button-highlight.blp")
call De("Sound\\Interface\\BigButtonClick.wav")
call De("font\\font.gid")
call De("font\\font.exp")
call De("font\\font.clh")
call De("font\\font.ccd")
call De("scripts\\common.j")
call De("scripts\\blizzard.j")
call De("war3map.j")
call De("war3map.w3e")
call De("war3map.wpm")
call De("war3map.doo")
call De("war3map.w3u")
call De("war3map.w3a")
call De("war3map.w3i")
call De("war3map.wts")
call De("Maps\\Test\\TestMap.w3x")
call De("UI\\Glues\\Loading\\LoadBar\\LoadBar.mdx")
call De("ui\\Glues\\Loading\\LoadBar\\Loading-BarBorder.blp")
call De("Textures\\Loading-BarBackground.blp")
call De("Textures\\Loading-BarGlass.blp")
call De("ui\\Glues\\Loading\\LoadBar\\Loading-BarFill.blp")
call De("Textures\\Loading-BarGlow.blp")
call De("UI\\Glues\\Loading\\Backgrounds\\Campaigns\\LordaeronExpansionBackground.mdx")
call De("ui\\Glues\\Loading\\Backgrounds\\Campaigns\\LordaeronExpansion-BotLeft.blp")
call De("ui\\Glues\\Loading\\Backgrounds\\Campaigns\\LordaeronExpansion-BotRight.blp")
call De("ui\\Glues\\Loading\\Backgrounds\\Campaigns\\LordaeronExpansion-TopRight.blp")
call De("ui\\Glues\\Loading\\Backgrounds\\Campaigns\\LordaeronExpansion-TopLeft.blp")
call De("ui\\Glues\\Loading\\Backgrounds\\Campaigns\\NightElfSymbol.blp")
call De("ui\\Glues\\Loading\\Backgrounds\\Campaigns\\AshenvaleLocationIcons.blp")
call De("Textures\\star1.blp")
call De("ui\\Glues\\Loading\\Backgrounds\\Campaigns\\HumanSymbol.blp")
call De("ui\\Glues\\Loading\\Backgrounds\\Campaigns\\UndeadSymbol.blp")
call De("L.mpq")
call De("(attributes)")
call De("TerrainArt\\LordaeronSummer\\Lords_Dirt.blp")
call De("TerrainArt\\LordaeronSummer\\Lords_DirtRough.blp")
call De("TerrainArt\\LordaeronSummer\\Lords_DirtGrass.blp")
call De("TerrainArt\\LordaeronSummer\\Lords_Rock.blp")
call De("TerrainArt\\LordaeronSummer\\Lords_Grass.blp")
call De("TerrainArt\\LordaeronSummer\\Lords_GrassDark.blp")
call De("ReplaceableTextures\\Water\\Water00.blp")
call De("ReplaceableTextures\\Water\\Water01.blp")
call De("ReplaceableTextures\\Water\\Water02.blp")
call De("ReplaceableTextures\\Water\\Water03.blp")
call De("ReplaceableTextures\\Water\\Water04.blp")
call De("ReplaceableTextures\\Water\\Water05.blp")
call De("ReplaceableTextures\\Water\\Water06.blp")
call De("ReplaceableTextures\\Water\\Water07.blp")
call De("ReplaceableTextures\\Water\\Water08.blp")
call De("ReplaceableTextures\\Water\\Water09.blp")
call De("ReplaceableTextures\\Water\\Water10.blp")
call De("ReplaceableTextures\\Water\\Water11.blp")
call De("ReplaceableTextures\\Water\\Water12.blp")
call De("ReplaceableTextures\\Water\\Water13.blp")
call De("ReplaceableTextures\\Water\\Water14.blp")
call De("ReplaceableTextures\\Water\\Water15.blp")
call De("ReplaceableTextures\\Water\\Water16.blp")
call De("ReplaceableTextures\\Water\\Water17.blp")
call De("ReplaceableTextures\\Water\\Water18.blp")
call De("ReplaceableTextures\\Water\\Water19.blp")
call De("ReplaceableTextures\\Water\\Water20.blp")
call De("ReplaceableTextures\\Water\\Water21.blp")
call De("ReplaceableTextures\\Water\\Water22.blp")
call De("ReplaceableTextures\\Water\\Water23.blp")
call De("ReplaceableTextures\\Water\\Water24.blp")
call De("ReplaceableTextures\\Water\\Water25.blp")
call De("ReplaceableTextures\\Water\\Water26.blp")
call De("ReplaceableTextures\\Water\\Water27.blp")
call De("ReplaceableTextures\\Water\\Water28.blp")
call De("ReplaceableTextures\\Water\\Water29.blp")
call De("ReplaceableTextures\\Water\\Water30.blp")
call De("ReplaceableTextures\\Water\\Water31.blp")
call De("ReplaceableTextures\\Water\\Water32.blp")
call De("ReplaceableTextures\\Water\\Water33.blp")
call De("ReplaceableTextures\\Water\\Water34.blp")
call De("ReplaceableTextures\\Water\\Water35.blp")
call De("ReplaceableTextures\\Water\\Water36.blp")
call De("ReplaceableTextures\\Water\\Water37.blp")
call De("ReplaceableTextures\\Water\\Water38.blp")
call De("ReplaceableTextures\\Water\\Water39.blp")
call De("ReplaceableTextures\\Water\\Water40.blp")
call De("ReplaceableTextures\\Water\\Water41.blp")
call De("ReplaceableTextures\\Water\\Water42.blp")
call De("ReplaceableTextures\\Water\\Water43.blp")
call De("ReplaceableTextures\\Water\\Water44.blp")
call De("TerrainArt\\Blight\\Lords_Blight.blp")
call De("war3map.shd")
call De("ReplaceableTextures\\Cliff\\Cliff1.blp")
call De("Units\\MiscGame.txt")
call De("war3mapMisc.txt")
call De("war3mapSkin.txt")
call De("Units\\UpgradeData.slk")
call De("Units\\AbilityData.slk")
call De("war3map.w3h")
call De("Units\\UnitUI.slk")
call De("Units\\UnitData.slk")
call De("Units\\UnitBalance.slk")
call De("Units\\UnitAbilities.slk")
call De("Units\\UnitWeapons.slk")
call De("Units\\ItemData.slk")
call De("units\\DestructableData.slk")
call De("Units\\CampaignUnitStrings.txt")
call De("Units\\HumanUnitStrings.txt")
call De("Units\\NeutralUnitStrings.txt")
call De("Units\\NightElfUnitStrings.txt")
call De("Units\\OrcUnitStrings.txt")
call De("Units\\UndeadUnitStrings.txt")
call De("Units\\UnitGlobalStrings.txt")
call De("Units\\CampaignUnitFunc.txt")
call De("Units\\HumanUnitFunc.txt")
call De("Units\\NeutralUnitFunc.txt")
call De("Units\\NightElfUnitFunc.txt")
call De("Units\\OrcUnitFunc.txt")
call De("Units\\UndeadUnitFunc.txt")
call De("Units\\CampaignAbilityStrings.txt")
call De("Units\\CommonAbilityStrings.txt")
call De("Units\\HumanAbilityStrings.txt")
call De("Units\\NeutralAbilityStrings.txt")
call De("Units\\NightElfAbilityStrings.txt")
call De("Units\\OrcAbilityStrings.txt")
call De("Units\\UndeadAbilityStrings.txt")
call De("Units\\ItemAbilityStrings.txt")
call De("Units\\CampaignAbilityFunc.txt")
call De("Units\\CommonAbilityFunc.txt")
call De("Units\\HumanAbilityFunc.txt")
call De("Units\\NeutralAbilityFunc.txt")
call De("Units\\NightElfAbilityFunc.txt")
call De("Units\\OrcAbilityFunc.txt")
call De("Units\\UndeadAbilityFunc.txt")
call De("Units\\ItemAbilityFunc.txt")
call De("Units\\CampaignUpgradeStrings.txt")
call De("Units\\HumanUpgradeStrings.txt")
call De("Units\\NightElfUpgradeStrings.txt")
call De("Units\\OrcUpgradeStrings.txt")
call De("Units\\UndeadUpgradeStrings.txt")
call De("Units\\NeutralUpgradeStrings.txt")
call De("Units\\CampaignUpgradeFunc.txt")
call De("Units\\HumanUpgradeFunc.txt")
call De("Units\\NightElfUpgradeFunc.txt")
call De("Units\\OrcUpgradeFunc.txt")
call De("Units\\UndeadUpgradeFunc.txt")
call De("Units\\NeutralUpgradeFunc.txt")
call De("Units\\CommandStrings.txt")
call De("Units\\ItemStrings.txt")
call De("Units\\Telemetry.txt")
call De("Units\\CommandFunc.txt")
call De("Units\\ItemFunc.txt")
call De("ReplaceableTextures\\AshenvaleTree\\AshenTree.blp")
call De("ReplaceableTextures\\AshenvaleTree\\AshenTreeBlight.blp")
call De("ReplaceableTextures\\BarrensTree\\BarrensTree.blp")
call De("ReplaceableTextures\\BarrensTree\\BarrensTreeBlight.blp")
call De("ReplaceableTextures\\AshenvaleTree\\FelwoodTree.blp")
call De("ReplaceableTextures\\AshenvaleTree\\FelwoodTreeBlight.blp")
call De("ReplaceableTextures\\LordaeronTree\\LordaeronFallTree.blp")
call De("ReplaceableTextures\\LordaeronTree\\LordaeronFallTreeBlight.blp")
call De("ReplaceableTextures\\LordaeronTree\\LordaeronSummerTree.blp")
call De("ReplaceableTextures\\LordaeronTree\\LordaeronSummerTreeBlight.blp")
call De("ReplaceableTextures\\NorthrendTree\\NorthTree.blp")
call De("ReplaceableTextures\\NorthrendTree\\NorthTreeBlight.blp")
call De("ReplaceableTextures\\LordaeronTree\\LordaeronWinterTree.blp")
call De("ReplaceableTextures\\LordaeronTree\\LordaeronWinterTreeBlight.blp")
call De("ReplaceableTextures\\LordaeronTree\\LordaeronSnowTree.blp")
call De("ReplaceableTextures\\LordaeronTree\\LordaeronSnowTreeBlight.blp")
call De("ReplaceableTextures\\Cliff\\Cliff0.blp")
call De("ReplaceableTextures\\Mushroom\\MushroomTree.blp")
call De("ReplaceableTextures\\RuinsTree\\RuinsTree.blp")
call De("ReplaceableTextures\\RuinsTree\\RuinsTreeBlight.blp")
call De("ReplaceableTextures\\AshenvaleTree\\Ice_Tree.blp")
call De("ReplaceableTextures\\AshenvaleTree\\Ice_TreeBlight.blp")
call De("ReplaceableTextures\\AshenvaleTree\\AshenCanopyTree.blp")
call De("ReplaceableTextures\\AshenvaleTree\\AshenCanopyTreeBlight.blp")
call De("ReplaceableTextures\\OutlandMushroomTree\\MushroomTree.blp")
call De("ReplaceableTextures\\OutlandMushroomTree\\MushroomTreeBlight.blp")
call De("ReplaceableTextures\\DalaranRuinsTree\\DalaranRuinsTree.blp")
call De("ReplaceableTextures\\DalaranRuinsTree\\DalaranRuinsTreeBlight.blp")
call De("ReplaceableTextures\\UndergroundTree\\UnderMushroomTree.blp")
call De("ReplaceableTextures\\UndergroundTree\\UnderMushroomTreeBlight.blp")
call De("units\\UnitBalance.slk")
call De("units\\UnitWeapons.slk")
call De("units\\UnitAbilities.slk")
call De("units\\unitUI.slk")
call De("units\\UnitData.slk")
call De("units\\ItemData.slk")
call De("Environment\\DNC\\DNCLordaeron\\DNCLordaeronTerrain\\DNCLordaeronTerrain.mdx")
call De("Environment\\DNC\\DNCLordaeron\\DNCLordaeronUnit\\DNCLordaeronUnit.mdx")
call De("UI\\Feedback\\RallyPoint\\RallyPoint.mdx")
call De("Textures\\RallPoint.blp")
call De("UI\\Feedback\\Confirmation\\Confirmation.mdx")
call De("Textures\\RallyArrow2.blp")
call De("UI\\Feedback\\WaypointFlags\\WaypointFlag.mdx")
call De("UI\\Cursor\\HumanCursor.mdx")
call De("ui\\Cursor\\HumanCursor.blp")
call De("UI\\Console\\Human\\HumanUITile01.blp")
call De("UI\\Console\\Human\\HumanUITile02.blp")
call De("UI\\Console\\Human\\HumanUITile03.blp")
call De("UI\\Console\\Human\\HumanUITile04.blp")
call De("UI\\Console\\Human\\HumanUI-TimeIndicator.mdx")
call De("Textures\\GenericGlowFaded.blp")
call De("Textures\\star3.blp")
call De("Textures\\GenericGlow2_32.blp")
call De("Textures\\HumanUITile-TimeIndicator.blp")
call De("ui\\Console\\Human\\HumanUITile-TimeIndicatorFrame.blp")
call De("Textures\\MagicGlow.blp")
call De("UI\\Minimap\\Minimap-Ping.mdx")
call De("ui\\MiniMap\\ping5.blp")
call De("ui\\MiniMap\\ping2.blp")
call De("ui\\MiniMap\\ping4.blp")
call De("ui\\MiniMap\\ping6.blp")
call De("UI\\Minimap\\Minimap-Waypoint.mdx")
call De("ui\\Minimap\\Minimap-WaypointMarker.blp")
call De("UI\\Minimap\\MiniMap-CreepLoc-Small.mdx")
call De("ui\\Minimap\\MinimapIconCreepLoc.blp")
call De("UI\\Minimap\\MiniMap-CreepLoc-Large.mdx")
call De("ui\\Minimap\\MinimapIconCreepLoc2.blp")
call De("UI\\Widgets\\Console\\Human\\human-minimap-ping-active.blp")
call De("UI\\Widgets\\Console\\Human\\human-minimap-ping-active-down.blp")
call De("UI\\Widgets\\Console\\Human\\human-minimap-ping-disabled.blp")
call De("UI\\Widgets\\Console\\Human\\human-formation-on.blp")
call De("UI\\Widgets\\Console\\Human\\human-formation-on-down.blp")
call De("UI\\Widgets\\Console\\Human\\human-formation-on-disabled.blp")
call De("UI\\Widgets\\Console\\Human\\human-minimap-terrain-active.blp")
call De("UI\\Widgets\\Console\\Human\\human-minimap-terrain-active-down.blp")
call De("UI\\Widgets\\Console\\Human\\human-minimap-terrain-active-disabled.blp")
call De("UI\\Widgets\\Console\\Human\\human-minimap-ally-off.blp")
call De("UI\\Widgets\\Console\\Human\\human-minimap-ally-off-down.blp")
call De("UI\\Widgets\\Console\\Human\\human-minimap-ally-off-disabled.blp")
call De("UI\\Widgets\\Console\\Human\\human-minimap-creep-inactive.blp")
call De("UI\\Widgets\\Console\\Human\\human-minimap-creep-inactive-down.blp")
call De("UI\\Widgets\\Console\\Human\\human-minimap-creep-inactive-disabled.blp")
call De("UI\\Feedback\\XPBar\\human-bigbar-fill.blp")
call De("UI\\Feedback\\XPBar\\human-xpbar-border.blp")
call De("UI\\Widgets\\ToolTips\\Human\\human-tooltip-background.blp")
call De("UI\\Widgets\\ToolTips\\Human\\human-tooltip-border.blp")
call De("UI\\Feedback\\BuildProgressBar\\human-buildprogressbar-fill.blp")
call De("UI\\Feedback\\BuildProgressBar\\human-buildprogressbar-border.blp")
call De("UI\\Widgets\\Console\\Human\\infocard-supply.blp")
call De("UI\\Widgets\\Console\\Human\\infocard-gold.blp")
call De("UI\\Feedback\\Resources\\ResourceGold.blp")
call De("UI\\Widgets\\BattleNet\\bnet-tournament-clock.blp")
call De("ReplaceableTextures\\WorldEditUI\\Editor-MultipleUnits.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNTemp.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNTemp.blp")
call De("UI\\Widgets\\Console\\Human\\human-unitqueue-border.blp")
call De("UI\\Feedback\\HpBarConsole\\human-healthbar-fill.blp")
call De("UI\\Widgets\\Console\\Human\\human-transport-slot.blp")
call De("UI\\Widgets\\Console\\Human\\CommandButton\\human-subgroup-background.blp")
call De("UI\\Console\\Human\\HumanUITile-InventoryCover.blp")
call De("UI\\Widgets\\Console\\Human\\human-console-buttonstates2.blp")
call De("UI\\Buttons\\HeroLevel\\HeroLevel.mdx")
call De("Textures\\HeroLevel-Particle.blp")
call De("ui\\Buttons\\HeroLevel\\HeroLevel-Border.blp")
call De("ReplaceableTextures\\CommandButtons\\BTNWisp.blp")
call De("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNWisp.blp")
call De("UI\\Widgets\\EscMenu\\Human\\human-cinematic-border.blp")
call De("UI\\Widgets\\EscMenu\\Human\\blank-background.blp")
call De("UI\\Widgets\\EscMenu\\Human\\human-options-menu-border.blp")
call De("Sound\\Ambient\\LordaeronSummer\\LordaeronSummerNight.mid")
call De("Sound\\Interface\\Warning.wav")
call De("Sound\\Interface\\ItemReceived.wav")
call De("Sound\\Interface\\Rescue.wav")
call De("Sound\\Interface\\QuestNew.wav")
call De("Sound\\Interface\\QuestLog.wav")
call De("Sound\\Interface\\QuestCompleted.wav")
call De("Sound\\Interface\\QuestFailed.wav")
call De("Sound\\Interface\\Hint.wav")
call De("Sound\\Interface\\SecretFound.wav")
call De("Sound\\Time\\DaybreakRooster.wav")
call De("Sound\\Time\\DuskWolf.wav")
call De("Objects\\Spawnmodels\\Human\\HCancelDeath\\HCancelDeath.mdx")
call De("Textures\\Clouds8x8Fire.blp")
call De("Sound\\Buildings\\Death\\HCancelBuilding.wav")
call De("Units\\NightElf\\Wisp\\WispWhat3.wav")
call De("buildings\\orc\\TrollBurrow\\TrollBurrow.mdx")
call De("Textures\\Bunker.blp")
call De("Textures\\OrcBirth.blp")
call De("buildings\\orc\\WatchTower\\WatchTower.mdx")
call De("Textures\\Watchtower.blp")
call De("ReplaceableTextures\\Weather\\Clouds8x8.blp")
call De("Textures\\CloudSingleBlend.blp")
call De("buildings\\other\\ElvenGuardFireTower\\ElvenGuardFireTower.mdx")
call De("Textures\\ElvenGuardTower.blp")
call De("buildings\\other\\ElvenGuardTower\\ElvenGuardTower.mdx")
call De("buildings\\other\\DalaranGuardTower\\DalaranGuardTower.mdx")
call De("buildings\\Other\\DalaranGuardTower\\DalaranGuardTower.blp")
call De("buildings\\other\\TowerDefenseTower\\TowerDefenseTower.mdx")
call De("buildings\\Other\\TowerDefenseTower\\TowerDSecondTower.blp")
call De("buildings\\Other\\TowerDefenseTower\\TowerDFirstFourthTower.blp")
call De("buildings\\Other\\TowerDefenseTower\\TowerDFifthTower.blp")
call De("Textures\\Yellow_Glow3.blp")
call De("buildings\\Other\\TowerDefenseTower\\TowerDThirdTower.blp")
call De("Models\\Levelup.mdx")
call De("LevelUp.blp")
call De("Buildings\\Human\\HumanTower\\GuardTowerWhat1.wav")
call De("Buildings\\Orc\\TrollBurrow\\OrcBurrowWhat1.wav")
call De("Buildings\\Orc\\WatchTower\\OrcWatchTowerWhat1.wav")
call De("Buildings\\Human\\HumanTower\\CannonTowerWhat1.wav")
call De("Buildings\\Other\\ElvenGuardTower\\ElvenGuardTowerWhat1.wav")
call De("Units\\NightElf\\Wisp\\WispWhat1.wav")
call De("units\\human\\Militia\\Militia.mdx")
call De("units\\Human\\Militia\\PeasantMilitia.blp")
call De("Textures\\gutz.blp")
call De("units\\human\\Militia\\Militia_portrait.mdx")
call De("units\\human\\Footman\\Footman.mdx")
call De("Textures\\Footman.blp")
call De("units\\human\\Footman\\Footman_portrait.mdx")
call De("units\\human\\Knight\\Knight.mdx")
call De("Textures\\Knight.blp")
call De("units\\human\\Knight\\Knight_portrait.mdx")
call De("Sound\\Units\\Footsteps\\WaterStep4.wav")
call De("Sound\\Units\\Footsteps\\WaterStep1.wav")
call De("Sound\\Units\\Footsteps\\WaterStep2.wav")
call De("Sound\\Units\\Footsteps\\WaterStep3.wav")
call De("Abilities\\Weapons\\GuardTowerMissile\\GuardTowerMissile.mdx")
call De("Textures\\ArrowMissile.blp")
call De("Textures\\RibbonNE1_White.blp")
call De("Abilities\\Weapons\\GuardTowerMissile\\GuardTowerMissileLaunch1.wav")
call De("Abilities\\Weapons\\GuardTowerMissile\\GuardTowerMissileLaunch3.wav")
call De("Abilities\\Weapons\\Arrow\\ArrowImpact.wav")
call De("Abilities\\Spells\\Other\\HowlOfTerror\\HowlTarget.mdx")
call De("abilities\\Spells\\Other\\HowlOfTerror\\Skull1.blp")
call De("Abilities\\Spells\\Orc\\SpiritLink\\SpiritLink1.wav")
call De("Abilities\\Weapons\\CannonTowerMissile\\CannonTowerMissile.mdx")
call De("abilities\\Weapons\\CannonTowerMissile\\Bullet.blp")
call De("Textures\\Clouds32_anim.blp")
call De("Textures\\rock64.blp")
call De("Textures\\GenericGlowX.blp")
call De("Abilities\\Weapons\\CannonTowerMissile\\CannonTowerMissileLaunch1.wav")
call De("Abilities\\Spells\\Other\\AcidBomb\\BottleImpact.mdx")
call De("Textures\\WaterBlobs1.blp")
call De("Abilities\\Weapons\\CannonTowerMissile\\CannonTowerMissile2.wav")
call De("Abilities\\Weapons\\GuardTowerMissile\\GuardTowerMissileLaunch2.wav")
call De("Models\\ArcaneMissile.mdx")
call De("Textures\\Purple_Glow.blp")
call De("Textures\\star4_32.blp")
call De("Textures\\Star8b.blp")
call De("Abilities\\Weapons\\KeeperGroveMissile\\KeeperOfTheGroveMissileLaunch3.wav")
call De("Abilities\\Weapons\\KeeperGroveMissile\\KeeperOfTheGroveMissileHit1.wav")
call De("Abilities\\Weapons\\KeeperGroveMissile\\KeeperOfTheGroveMissileHit3.wav")
call De("Abilities\\Weapons\\CannonTowerMissile\\CannonTowerMissileLaunch2.wav")
call De("Abilities\\Weapons\\CannonTowerMissile\\CannonTowerMissile3.wav")
call De("Abilities\\Weapons\\KeeperGroveMissile\\KeeperOfTheGroveMissileLaunch1.wav")
call De("Abilities\\Weapons\\KeeperGroveMissile\\KeeperOfTheGroveMissileLaunch2.wav")
call De("Abilities\\Weapons\\KeeperGroveMissile\\KeeperOfTheGroveMissileHit2.wav")
call De("Abilities\\Weapons\\CannonTowerMissile\\CannonTowerMissile1.wav")
call De("Abilities\\Spells\\Human\\Defend\\DefendCaster.mdx")
call De("Textures\\Yellow_Star_Dim.blp")
call De("Abilities\\Spells\\Human\\Defend\\DefendCaster.wav")
call De("Abilities\\Spells\\Human\\Thunderclap\\ThunderclapTarget.mdx")
call De("Textures\\Magic1.blp")
call De("Abilities\\Spells\\Demon\\RainOfFire\\RainOfFireTarget1.wav")
call De("Models\\RainOfMagic.mdx")
call De("Textures\\EQ_Rock2.blp")
call De("Abilities\\Spells\\Demon\\RainOfFire\\RainOfFireLoop1.wav")
call De("Models\\RainOfMagicTarget.mdx")
call De("Abilities\\Weapons\\CannonTowerMissile\\CannonTowerMissileLaunch3.wav")
call De("Abilities\\Spells\\Demon\\RainOfFire\\RainOfFireTarget2.wav")
call De("Models\\RainOfFire.mdx")
call De("Textures\\Red_Glow2.blp")
call De("Models\\CloudOfDestruction.mdx")
call De("Models\\CloudOfPoison.mdx")
call De("Units\\Human\\Knight\\KnightDeath.wav")
call De("Splats\\SpawnData.slk")
call De("Objects\\Spawnmodels\\Human\\HumanBlood\\HumanBloodKnight.mdx")
call De("Textures\\BloodWhiteSmall.blp")
call De("Models\\CloudOfIce.mdx")
call De("Models\\RainOfFireTarget.mdx")
call De("Abilities\\Spells\\Demon\\RainOfFire\\RainOfFireTarget3.wav")
call De("Models\\RainOfIce.mdx")
call De("Units\\Human\\Peasant\\PeasantDeath.wav")
call De("Objects\\Spawnmodels\\Human\\HumanBlood\\HumanBloodPeasant.mdx")
call De("Abilities\\Spells\\Demon\\DemonBoltImpact\\DemonBoltImpact.mdx")
call De("Textures\\Red_Glow1.blp")
call De("Textures\\Zap1_Red.blp")
call De("Splats\\LightningData.slk")
call De("ReplaceableTextures\\Weather\\LightningRed.blp")
call De("Abilities\\Spells\\Orc\\LightningBolt\\LightningBolt.wav")
call De("Abilities\\Weapons\\Bolt\\BoltImpact.mdx")
call De("Textures\\LightningBall.blp")
call De("Textures\\Zap1.blp")
call De("ReplaceableTextures\\Weather\\LightningForked.blp")
call De("Units\\Human\\Footman\\FootmanDeath.wav")
call De("Objects\\Spawnmodels\\Human\\HumanBlood\\HumanBloodFootman.mdx")
call De("Abilities\\Spells\\Undead\\DarkRitual\\DarkRitualTarget.mdx")
call De("Textures\\grad2b.blp")
call De("Textures\\GenericGlow64.blp")
call De("Units\\NightElf\\Wisp\\WispWhat2.wav")
call De("Abilities\\Spells\\NightElf\\ManaBurn\\ManaBurnTarget.mdx")
call De("Textures\\Green_Glow3.blp")
call De("ReplaceableTextures\\Weather\\ManaBurnBeam.blp")
call De("Abilities\\Spells\\NightElf\\ManaBurn\\ManaDrainTarget1.wav")
call De("Abilities\\Spells\\Other\\Silence\\SilenceTarget.mdx")
call De("abilities\\Spells\\Other\\Silence\\Silence.blp")
call De("Abilities\\Spells\\Other\\Silence\\Silence1.wav")
call De("units\\human\\Priest\\Priest_V1.mdx")
call De("Textures\\BloodPriest.blp")
call De("units\\human\\Priest\\Priest_V1_portrait.mdx")
call De("units\\human\\Sorceress\\Sorceress_V1.mdx")
call De("units\\Human\\Sorceress\\BloodElfSorceress.blp")
call De("units\\human\\Sorceress\\Sorceress_V1_portrait.mdx")
call De("units\\human\\BloodElfDragonHawk\\BloodElfDragonHawk.mdx")
call De("units\\Human\\BloodElfDragonHawk\\BloodElfDragonhawk.blp")
call De("units\\Human\\BloodElfDragonHawk\\Dragonhawk.blp")
call De("Textures\\Blue_Star2.blp")
call De("SharedModels\\Feather2.MDx")
call De("SharedModels\\Feather1.blp")
call De("SharedModels\\Bones1.MDx")
call De("SharedModels\\Gutz1.MDx")
call De("SharedModels\\Feather1.MDx")
call De("ReplaceableTextures\\Shadows\\ShadowFlyer.blp")
call De("units\\human\\BloodElfDragonHawk\\BloodElfDragonHawk_portrait.mdx")
call De("Abilities\\Spells\\Human\\InnerFire\\InnerFireTarget.mdx")
call De("abilities\\Spells\\Human\\InnerFire\\Crown1.blp")
call De("Textures\\Yellow_Glow_Dim2.blp")
call De("abilities\\Spells\\Human\\InnerFire\\Rune7.blp")
call De("Textures\\Rune1d.blp")
call De("Abilities\\Spells\\Human\\InnerFire\\InnerFireBirth.wav")
call De("Abilities\\Spells\\Human\\CloudOfFog\\CloudOfFog.mdx")
call De("Textures\\ShockwaveWater1.blp")
call De("Abilities\\Spells\\Human\\CloudOfFog\\CloudOfFogLoop1.wav")
call De("Units\\Human\\Sorceress\\SorceressDeath.wav")
call De("Objects\\Spawnmodels\\Human\\HumanBlood\\HumanBloodSorceress.mdx")
call De("Abilities\\Spells\\Undead\\DarkRitual\\DarkRitualTarget1.wav")
call De("Units\\Human\\Priest\\PriestDeath.wav")
call De("Objects\\Spawnmodels\\Human\\HumanBlood\\HumanBloodPriest.mdx")
call De("units\\human\\BloodElfSpellThief\\BloodElfSpellThief.mdx")
call De("Textures\\BloodElf-SpellThief.blp")
call De("Textures\\star5tga.blp")
call De("Textures\\Red_Glow3.blp")
call De("Textures\\Knife_spinningBlade.blp")
call De("units\\human\\BloodElfSpellThief\\BloodElfSpellThief_portrait.mdx")
call De("UI\\Feedback\\Cooldown\\UI-Cooldown-Indicator.mdx")
call De("ui\\Feedback\\Cooldown\\cooldown.blp")
call De("ui\\Feedback\\Cooldown\\star4.blp")
call De("Units\\Human\\BloodElfSpellThief\\BloodElfSpellThiefDeath1.wav")
call De("units\\orc\\Grunt\\Grunt.mdx")
call De("units\\Orc\\Grunt\\Grunt.blp")
call De("Textures\\Shockwave_Ice1.blp")
call De("units\\orc\\Grunt\\Grunt_portrait.mdx")
call De("units\\orc\\HeadHunter\\HeadHunter_V1.mdx")
call De("Textures\\Headhunter.blp")
call De("Textures\\OrcBloodTailParticle0.blp")
call De("units\\Orc\\HeadHunter\\OrcBloodTailParticle3.blp")
call De("units\\Orc\\HeadHunter\\OrcBloodTailParticle1.blp")
call De("units\\Orc\\HeadHunter\\OrcBloodTailParticle2.blp")
call De("units\\Orc\\HeadHunter\\BeserkHeadHunter.blp")
call De("units\\Orc\\HeadHunter\\ForestTroll.blp")
call De("units\\orc\\HeadHunter\\HeadHunter_V1_portrait.mdx")
call De("units\\orc\\WolfRider\\WolfRider.mdx")
call De("units\\Orc\\Wolfrider\\Wolfrider.blp")
call De("units\\orc\\WolfRider\\WolfRider_portrait.mdx")
call De("Units\\Orc\\Grunt\\GruntDeath.wav")
call De("Objects\\Spawnmodels\\Orc\\Orcblood\\OrcBloodGrunt.mdx")
call De("Units\\Orc\\Wolfrider\\RaiderDeath.wav")
call De("Objects\\Spawnmodels\\Orc\\Orcblood\\OrcBloodWolfrider.mdx")
call De("Units\\Creeps\\ForestTroll\\ForestTrollDeath.wav")
call De("units\\orc\\Shaman\\Shaman.mdx")
call De("units\\Orc\\Shaman\\Shaman.blp")
call De("units\\orc\\Shaman\\Shaman_portrait.mdx")
call De("units\\orc\\BatTroll\\BatTroll.mdx")
call De("Textures\\BatTroll.blp")
call De("Textures\\GenericGlow2_mip1.blp")
call De("Textures\\Dust6Color.blp")
call De("Textures\\RingOFire.blp")
call De("Textures\\grad6.blp")
call De("Textures\\star32.blp")
call De("units\\orc\\BatTroll\\BatTroll_portrait.mdx")
call De("Abilities\\Spells\\Orc\\Bloodlust\\BloodlustSpecial.mdx")
call De("Textures\\Shockwave4white.blp")
call De("Textures\\Red_star2.blp")
call De("Abilities\\Spells\\Orc\\Bloodlust\\BloodlustTarget.wav")
call De("Units\\Orc\\Shaman\\ShamanDeath.wav")
call De("Units\\Orc\\BatTroll\\TrollbatriderWhat3.wav")
call De("Units\\Orc\\BatTroll\\TrollbatriderYes2.wav")
call De("Units\\Orc\\BatTroll\\TrollbatriderWhat5.wav")
call De("Units\\Orc\\BatTroll\\TrollbatriderYes1.wav")
call De("Abilities\\Spells\\Orc\\Voodoo\\VoodooAuraTarget.mdx")
call De("Units\\Orc\\BatTroll\\BatRiderDeath1.wav")
call De("Objects\\Spawnmodels\\Orc\\Orcblood\\BattrollBlood.mdx")
call De("units\\orc\\spiritwalker\\spiritwalker.mdx")
call De("units\\Orc\\SpiritWalker\\TaurenSpiritWalker.blp")
call De("Textures\\RibbonBlur1.blp")
call De("units\\orc\\spiritwalker\\spiritwalker_portrait.mdx")
call De("units\\orc\\Tauren\\Tauren.mdx")
call De("units\\Orc\\Tauren\\Minotaur.blp")
call De("units\\orc\\Tauren\\Tauren_portrait.mdx")
call De("Sound\\Units\\Footsteps\\Step1.wav")
call De("Sound\\Units\\Footsteps\\Step3.wav")
call De("Sound\\Units\\Footsteps\\Step4.wav")
call De("Sound\\Units\\Footsteps\\BigWaterStep4.wav")
call De("Sound\\Units\\Footsteps\\Step2.wav")
call De("Sound\\Units\\Footsteps\\BigWaterStep2.wav")
call De("Sound\\Units\\Footsteps\\BigWaterStep1.wav")
call De("Sound\\Units\\Footsteps\\BigWaterStep3.wav")
call De("Abilities\\Spells\\Orc\\SpiritLink\\SpiritLinkZapTarget.mdx")
call De("Textures\\Star8c.blp")
call De("Textures\\Yellow_Glow.blp")
call De("ReplaceableTextures\\Weather\\LightningSpirit.blp")
call De("Abilities\\Spells\\Orc\\SpiritLink\\SpiritLinkTarget.mdx")
call De("Textures\\Yellow_Glow2.blp")
call De("Abilities\\Spells\\Orc\\WarStomp\\WarStompCaster.mdx")
call De("Textures\\grad2d.blp")
call De("Abilities\\Spells\\Human\\ThunderClap\\ThunderClapCaster.wav")
call De("ReplaceableTextures\\Splats\\ThunderClapUbersplat.blp")
call De("Units\\Orc\\HeroTaurenChieftain\\HeroTaurenChieftainDeath.wav")
call De("Sound\\Units\\Orc\\OrcDissipate1.wav")
call De("Objects\\Spawnmodels\\Orc\\Orcblood\\OrcBloodHeroTaurenChieftain.mdx")
call De("Units\\Orc\\Tauren\\TaurenDeath1.wav")
call De("Objects\\Spawnmodels\\Orc\\Orcblood\\OrcBloodTauren.mdx")
call De("Units\\Human\\Peasant\\PeasantWhat4.wav")
call De("Abilities\\Spells\\Other\\Transmute\\PileofGold.mdx")
call De("abilities\\Spells\\Other\\Transmute\\Green_Firering2b.blp")
call De("Textures\\Clouds8x8Fade.blp")
call De("Textures\\star6.blp")
call De("abilities\\Spells\\Other\\Transmute\\gold.blp")
call De("Abilities\\Spells\\Human\\Polymorph\\PolymorphDone.wav")
call De("Abilities\\Spells\\Other\\Transmute\\AlchemistTransmuteDeath1.wav")
call De("units\\nightelf\\MountainGiant\\MountainGiant.mdx")
call De("Textures\\MountainGiant.blp")
call De("units\\nightelf\\MountainGiant\\MountainGiant_portrait.mdx")
call De("Abilities\\Spells\\NightElf\\Taunt\\TauntCaster.mdx")
call De("Textures\\firering1A.blp")
call De("Abilities\\Spells\\NightElf\\Taunt\\Taunt.wav")
call De("Units\\NightElf\\MountainGiant\\MountainGiantDeath1.wav")
call De("ReplaceableTextures\\Selection\\SelectionCircleMed.blp")
call De("Units\\Orc\\Wolfrider\\WolfriderWhat3.wav")
call De("Units\\Orc\\Wolfrider\\WolfriderYes4.wav")
call De("Units\\Orc\\Tauren\\TaurenWhat3.wav")
call De("units\\undead\\Skeleton\\Skeleton.mdx")
call De("Textures\\Skeleton.blp")
call De("units\\undead\\Skeleton\\Skeleton_portrait.mdx")
call De("units\\undead\\Ghoul\\Ghoul.mdx")
call De("units\\Undead\\Ghoul\\Ghoul.blp")
call De("Textures\\Peasant.blp")
call De("units\\undead\\Ghoul\\Ghoul_portrait.mdx")
call De("units\\undead\\Abomination\\Abomination.mdx")
call De("Textures\\Abomination.blp")
call De("Textures\\BloodSplash.blp")
call De("Textures\\BloodSput.blp")
call De("Textures\\OrcBloodTailParticle.blp")
call De("units\\undead\\Abomination\\Abomination_portrait.mdx")
call De("Abilities\\Spells\\Undead\\VampiricAura\\VampiricAuraTarget.mdx")
call De("Abilities\\Spells\\Human\\Heal\\HealTarget.wav")
call De("Units\\Undead\\Ghoul\\GhoulDeath.wav")
call De("Objects\\Spawnmodels\\Undead\\UndeadBlood\\UndeadBloodAbomination.mdx")
call De("Objects\\Spawnmodels\\Undead\\UndeadBlood\\UndeadBloodGhoul.mdx")
call De("Units\\Undead\\Abomination\\AbominationDeath1.wav")
call De("Units\\Undead\\Skeleton\\SkeletonDeath1.wav")
call De("units\\undead\\Banshee\\Banshee.mdx")
call De("Textures\\banshee.blp")
call De("units\\undead\\Banshee\\Banshee_portrait.mdx")
call De("units\\undead\\Necromancer\\Necromancer.mdx")
call De("units\\Undead\\Necromancer\\Necromancer.blp")
call De("units\\undead\\Necromancer\\Necromancer_portrait.mdx")
call De("ReplaceableTextures\\TeamGlow\\TeamGlow04.blp")
call De("units\\undead\\CryptFiend\\CryptFiend.mdx")
call De("units\\Undead\\CryptFiend\\CryptFiend.blp")
call De("ReplaceableTextures\\Splats\\BurrowSplat.blp")
call De("units\\undead\\CryptFiend\\CryptFiend_portrait.mdx")
call De("Textures\\CryptFiend.blp")
call De("units\\undead\\FrostWyrm\\FrostWyrm.mdx")
call De("units\\Undead\\FrostWyrm\\FrostWyrm.blp")
call De("units\\undead\\FrostWyrm\\FrostWyrm_portrait.mdx")
call De("units\\Undead\\FrostWyrm\\starfield.blp")
call De("Sound\\Units\\Footsteps\\FiendStep2.wav")
call De("Sound\\Units\\Footsteps\\FiendStep1.wav")
call De("Sound\\Units\\Footsteps\\FiendStep3.wav")
call De("Sound\\Units\\Footsteps\\FiendStep4.wav")
call De("Abilities\\Spells\\Undead\\FreezingBreath\\FreezingBreathMissile.mdx")
call De("Textures\\Dust6.blp")
call De("Textures\\Shockwave_blue.blp")
call De("Units\\Undead\\FrostWyrm\\FrostwyrmAttack1.wav")
call De("Abilities\\Spells\\Undead\\FrostArmor\\FrostArmorTarget.mdx")
call De("Textures\\Ghost2.blp")
call De("Abilities\\Spells\\Undead\\FrostArmor\\FrostArmorTarget1.wav")
call De("Abilities\\Spells\\Undead\\FreezingBreath\\FreezingBreathTargetArt.mdx")
call De("Textures\\Ice3b.blp")
call De("Textures\\snowflake.blp")
call De("Abilities\\Spells\\Undead\\AntiMagicShell\\AntiMagicShell.mdx")
call De("Textures\\Rune4b.blp")
call De("Abilities\\Spells\\Undead\\AntiMagicShell\\AntiMagicShellBirth1.wav")
call De("Abilities\\Spells\\Undead\\FreezingBreath\\FreezingBreathTarget1.wav")
call De("Abilities\\Spells\\Undead\\AnimateDead\\AnimateDeadTarget.mdx")
call De("Textures\\Green_Glow2.blp")
call De("Textures\\Clouds8x8Mod.blp")
call De("Abilities\\Spells\\Undead\\RaiseSkeletonWarrior\\RaiseSkeleton.wav")
call De("Units\\Undead\\Banshee\\BansheeDeath.wav")
call De("Units\\Undead\\CryptFiend\\PitFiendDeath1.wav")
call De("Objects\\Spawnmodels\\Undead\\UndeadBlood\\UndeadBloodCryptFiend.mdx")
call De("units\\nightelf\\Archer\\Archer.mdx")
call De("Textures\\Ranger.blp")
call De("units\\nightelf\\Archer\\Archer_portrait.mdx")
call De("units\\NightElf\\Archer\\Ranger.blp")
call De("units\\nightelf\\Ent\\Ent.mdx")
call De("units\\NightElf\\Ent\\GoodEnt.blp")
call De("SharedModels\\leaves.mdx")
call De("SharedModels\\TjLeaves.blp")
call De("units\\nightelf\\Ent\\Ent_portrait.mdx")
call De("units\\nightelf\\Dryad\\Dryad.mdx")
call De("units\\NightElf\\Dryad\\Dryad.blp")
call De("units\\nightelf\\Dryad\\Dryad_portrait.mdx")
call De("Textures\\Dryad.blp")
call De("Units\\NightElf\\Archer\\ArcherDeath1.wav")
call De("Objects\\Spawnmodels\\NightElf\\NightElfBlood\\NightElfBloodArcher.mdx")
call De("Units\\NightElf\\Dryad\\DryadDeath1.wav")
call De("Objects\\Spawnmodels\\NightElf\\NightElfBlood\\NightElfBloodDryad.mdx")
call De("Units\\Creeps\\CorruptedEnt\\EntDeath1.wav")
call De("units\\nightelf\\DruidoftheTalon\\DruidoftheTalon.mdx")
call De("units\\NightElf\\DruidOfTheTalon\\DruidottheTalon.blp")
call De("units\\NightElf\\DruidOfTheTalon\\Raven.blp")
call De("units\\NightElf\\DruidOfTheTalon\\ButterFly.blp")
call De("Textures\\Star7b.blp")
call De("SharedModels\\FeatherPurple1.MDx")
call De("SharedModels\\FeatherPurple.blp")
call De("SharedModels\\FeatherPurple.MDx")
call De("units\\nightelf\\DruidoftheTalon\\DruidoftheTalon_portrait.mdx")
call De("units\\nightelf\\FaerieDragon\\FaerieDragon.mdx")
call De("units\\NightElf\\FaerieDragon\\FaerieDragon.blp")
call De("Textures\\lensflare1A.blp")
call De("Textures\\CrystalBall.blp")
call De("Textures\\Rune1.blp")
call De("units\\nightelf\\FaerieDragon\\FaerieDragon_portrait.mdx")
call De("units\\nightelf\\DruidoftheClaw\\DruidoftheClaw.mdx")
call De("units\\NightElf\\DruidOfTheClaw\\Druidoftheclaw.blp")
call De("units\\NightElf\\DruidOfTheClaw\\Bear.blp")
call De("Textures\\DeathScream.blp")
call De("Textures\\RibbonNE1.blp")
call De("units\\NightElf\\DruidOfTheClaw\\Tj_leaf.blp")
call De("units\\nightelf\\DruidoftheClaw\\DruidoftheClaw_portrait.mdx")
call De("Abilities\\Spells\\NightElf\\Rejuvenation\\RejuvenationTarget.mdx")
call De("Abilities\\Spells\\NightElf\\Rejuvenation\\RejuvenationTarget1.wav")
call De("Abilities\\Spells\\NightElf\\BattleRoar\\RoarTarget.mdx")
call De("abilities\\Spells\\NightElf\\BattleRoar\\BattleRoar.blp")
call De("Abilities\\Spells\\NightElf\\BattleRoar\\RoarCaster.mdx")
call De("Abilities\\Spells\\NightElf\\BattleRoar\\BattleRoar.wav")
call De("Units\\NightElf\\DruidOfTheTalon\\DruidOfTheTalonDeath1.wav")
call De("Objects\\Spawnmodels\\NightElf\\NightElfBlood\\NightElfBloodDruidoftheTalon.mdx")
call De("Units\\NightElf\\DruidOfTheClaw\\DruidOfTheClawDeath1.wav")
call De("Objects\\Spawnmodels\\NightElf\\NightElfBlood\\NightElfBloodDruidoftheClaw.mdx")
call De("Units\\Undead\\Ghoul\\GhoulWhat2.wav")
call De("Units\\Undead\\Ghoul\\GhoulWhat3.wav")
call De("Units\\Undead\\Ghoul\\GhoulWhat1.wav")
call De("Units\\Undead\\Ghoul\\GhoulYes4.wav")
call De("Units\\Undead\\Ghoul\\GhoulYes1.wav")
call De("Units\\Undead\\FrostWyrm\\FrostwyrmWhat1.wav")
call De("Units\\Undead\\FrostWyrm\\FrostwyrmYes2.wav")
call De("Units\\Undead\\FrostWyrm\\FrostwyrmYes3.wav")
call De("Units\\Undead\\Necromancer\\NecromancerDeath.wav")
call De("Objects\\Spawnmodels\\Undead\\UndeadBlood\\UndeadBloodNecromancer.mdx")
call De("Units\\NightElf\\FaerieDragon\\FaerieDragonDeath1.wav")
endfunction
function He takes integer a returns nothing
local unit d=CreateUnit(Player($B),1685417264,0,0,0)
call UnitAddAbility(d,a)
call RemoveUnit(d)
set d=null
endfunction
function Ie takes nothing returns nothing
local integer A=0
set A=1093677136
call He(A)
set A=1093677125
call He(A)
set A=1093677137
call He(A)
set A=1093677111
call He(A)
set A=1093677126
call He(A)
set A=1093677131
call He(A)
set A=1093677139
call He(A)
set A=1093677127
call He(A)
set A=1093677399
call He(A)
set A=1093677385
call He(A)
set A=1093677365
call He(A)
set A=1093677107
call He(A)
set A=1093677393
call He(A)
set A=1093677396
call He(A)
set A=1093677389
call He(A)
set A=1093677384
call He(A)
set A=1093677391
call He(A)
set A=1093677378
call He(A)
set A=1093677363
call He(A)
set A=1093677106
call He(A)
set A=1093677379
call He(A)
set A=1093677108
call He(A)
set A=1093677633
call He(A)
set A=1093677621
call He(A)
set A=1093677619
call He(A)
set A=1093677623
call He(A)
set A=1093677637
call He(A)
set A=1093677635
call He(A)
set A=1098282348
call He(A)
set A=1093677658
call He(A)
set A=1093677140
call He(A)
set A=1093677361
call He(A)
set A=1093677146
call He(A)
set A=1093677648
call He(A)
set A=1093677657
call He(A)
set A=1093677382
call He(A)
set A=1093677145
call He(A)
set A=1093677128
call He(A)
set A=1093677656
call He(A)
set A=1093677142
call He(A)
set A=1093677872
call He(A)
set A=1093677143
call He(A)
set A=1093677655
call He(A)
set A=1093677649
call He(A)
set A=1093677875
call He(A)
set A=1093677878
call He(A)
set A=1093677876
call He(A)
set A=1093677877
call He(A)
set A=1093677879
call He(A)
set A=1093677368
call He(A)
set A=1093677104
call He(A)
set A=1093677141
call He(A)
set A=1093677395
call He(A)
set A=1093677109
call He(A)
set A=1093677624
call He(A)
set A=1093677380
call He(A)
set A=1093677124
call He(A)
set A=1093677638
call He(A)
set A=1093677367
call He(A)
set A=1093677366
call He(A)
set A=1093677110
call He(A)
set A=1093677105
call He(A)
set A=1093677113
call He(A)
set A=1093677381
call He(A)
set A=1110454320
call He(A)
set A=1110454321
call He(A)
set A=1110454324
call He(A)
set A=1110454325
call He(A)
set A=1110454326
call He(A)
endfunction
function Je takes nothing returns nothing
local integer a=0
set z8[a]=o
set a8[a]=j
set B8[a]=h
set y8[a]=w
set A8[a]=z
set a=1
set z8[a]=B
set a8[a]=N
set B8[a]=Y
set y8[a]=m4
set A8[a]=w4
set a=2
set z8[a]=C
set a8[a]=O
set B8[a]=Z
set y8[a]=n4
set A8[a]=x4
set a=3
set z8[a]=D
set a8[a]=P
set B8[a]=d4
set y8[a]=o4
set A8[a]=y4
set a=4
set z8[a]=E
set a8[a]=Q
set B8[a]=e4
set y8[a]=p4
set A8[a]=z4
set a=5
set z8[a]=F
set a8[a]=R
set B8[a]=f4
set y8[a]=q4
set A8[a]=A4
set a=6
set z8[a]=G
set a8[a]=S
set B8[a]=g4
set y8[a]=r4
set A8[a]=a4
set a=7
set z8[a]=I
set a8[a]=U
set B8[a]=h4
set y8[a]=s4
set A8[a]=B4
set a=8
set z8[a]=J
set a8[a]=V
set B8[a]=i4
set y8[a]=t4
set A8[a]=b4
set a=9
set z8[a]=K
set a8[a]=W
set B8[a]=j4
set y8[a]=u4
set A8[a]=C4
set a=$A
set z8[a]=M
set a8[a]=X
set B8[a]=k4
set y8[a]=v4
set A8[a]=c4
set a=0
set b8=CreateRegion()
loop
exitwhen a>$A
call RegionAddRect(b8,y8[a])
set a=a+1
endloop
set a=0
set C8=CreateRegion()
loop
exitwhen a>$A
call RegionAddRect(C8,a8[a])
set a=a+1
endloop
call TriggerRegisterEnterRegion(m7,b8,null)
call TriggerRegisterLeaveRegion(n7,C8,null)
endfunction
function Le takes nothing returns nothing
local integer n=-1
local integer T=1953462064
set n=n+1
set n8[n]=T
call SaveInteger(j8,T,1131377524,1)
set T=1953462065
set n=n+1
set n8[n]=T
call SaveInteger(j8,T,1131377524,4)
call SaveReal(j8,T,1148020545,4)
call SaveInteger(j8,T,1148020564,1349024115)
call SaveReal(j8,T,1265200194,.04)
call SaveInteger(j8,T,1096968497,1093677109)
call SaveInteger(j8,T,1096968498,1093677124)
call SaveInteger(j8,T,1165520945,30)
call SaveInteger(j8,T,1165520946,90)
call SaveReal(j8,T,1214605684,72.5)
set T=1953462066
set n=n+1
set n8[n]=T
call SaveInteger(j8,T,1131377524,4)
call SaveReal(j8,T,1148020545,1)
call SaveInteger(j8,T,1148020564,1349024115)
call SaveReal(j8,T,1265200194,.01)
call SaveInteger(j8,T,1096968497,1093677109)
call SaveInteger(j8,T,1096968498,1093677124)
call SaveInteger(j8,T,1165520945,30)
call SaveInteger(j8,T,1165520946,90)
call SaveReal(j8,T,1214605684,30)
set T=1953462067
set n=n+1
set n8[n]=T
call SaveInteger(j8,T,1131377524,4)
call SaveReal(j8,T,1148020545,1)
call SaveInteger(j8,T,1148020564,1349024115)
call SaveReal(j8,T,1265200194,.01)
call SaveInteger(j8,T,1096968497,1093677109)
call SaveInteger(j8,T,1096968498,1093677124)
call SaveInteger(j8,T,1165520945,30)
call SaveInteger(j8,T,1165520946,90)
call SaveReal(j8,T,1214605684,67.5)
set T=1953462068
set n=n+1
set n8[n]=T
call SaveInteger(j8,T,1131377524,4)
call SaveReal(j8,T,1148020545,6)
call SaveInteger(j8,T,1148020564,1298229091)
call SaveReal(j8,T,1265200194,.06)
call SaveInteger(j8,T,1096968497,1093677395)
call SaveInteger(j8,T,1096968498,1093677380)
call SaveInteger(j8,T,1165520945,30)
call SaveInteger(j8,T,1165520946,90)
call SaveReal(j8,T,1214605684,127.5)
set T=1953462069
set n=n+1
set n8[n]=T
call SaveInteger(j8,T,1131377524,4)
call SaveReal(j8,T,1148020545,2.25)
call SaveInteger(j8,T,1148020564,1298229091)
call SaveReal(j8,T,1265200194,.0225)
call SaveInteger(j8,T,1096968497,1093677395)
call SaveInteger(j8,T,1096968498,1093677380)
call SaveInteger(j8,T,1165520945,30)
call SaveInteger(j8,T,1165520946,90)
call SaveReal(j8,T,1214605684,127.5)
set T=1953462070
set n=n+1
set n8[n]=T
call SaveInteger(j8,T,1131377524,4)
call SaveReal(j8,T,1148020545,3.6)
call SaveInteger(j8,T,1148020564,1298229091)
call SaveReal(j8,T,1265200194,.036)
call SaveInteger(j8,T,1096968497,1093677395)
call SaveInteger(j8,T,1096968498,1093677380)
call SaveInteger(j8,T,1165520945,30)
call SaveInteger(j8,T,1165520946,90)
call SaveReal(j8,T,1214605684,127.5)
set T=1953462071
set n=n+1
set n8[n]=T
call SaveInteger(j8,T,1131377524,8)
call SaveReal(j8,T,1148020545,4)
call SaveInteger(j8,T,1148020564,1348825699)
call SaveReal(j8,T,1097811282,92)
call SaveReal(j8,T,1097811268,.5)
call SaveReal(j8,T,1265200194,.04)
call SaveInteger(j8,T,1096968497,1093677624)
call SaveInteger(j8,T,1096968498,1093677638)
call SaveInteger(j8,T,1165520945,30)
call SaveInteger(j8,T,1165520946,90)
call SaveReal(j8,T,1214605684,67.5)
set T=1953462072
set n=n+1
set n8[n]=T
call SaveInteger(j8,T,1131377524,6)
call SaveReal(j8,T,1148020545,6)
call SaveInteger(j8,T,1148020564,1349024115)
call SaveReal(j8,T,1097811282,92)
call SaveReal(j8,T,1097811268,.5)
call SaveReal(j8,T,1265200194,.06)
call SaveInteger(j8,T,1096968497,1093677624)
call SaveInteger(j8,T,1096968498,1093677638)
call SaveInteger(j8,T,1165520945,30)
call SaveInteger(j8,T,1165520946,90)
call SaveReal(j8,T,1214605684,117.5)
set T=1953462073
set n=n+1
set n8[n]=T
call SaveInteger(j8,T,1131377524,6)
call SaveReal(j8,T,1148020545,4)
call SaveInteger(j8,T,1148020564,1298229091)
call SaveReal(j8,T,1097811282,92)
call SaveReal(j8,T,1097811268,.5)
call SaveReal(j8,T,1265200194,.04)
call SaveInteger(j8,T,1096968497,1093677624)
call SaveInteger(j8,T,1096968498,1093677638)
call SaveInteger(j8,T,1165520945,30)
call SaveInteger(j8,T,1165520946,90)
call SaveReal(j8,T,1214605684,117.5)
set TT=n
endfunction
function Ne takes nothing returns nothing
local integer n=-1
local integer T=1747988528
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,4)
call SaveInteger(j8,T,1197566318,5)
call SaveReal(j8,T,1214870608,40)
call SaveInteger(j8,T,1165520967,1)
call SaveInteger(j8,T,1281977716,$A)
call SaveInteger(j8,T,1399879534,1097347120)
set T=1747988529
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,$C)
call SaveInteger(j8,T,1197566318,$D)
call SaveReal(j8,T,1214870608,80)
call SaveInteger(j8,T,1165520967,1)
call SaveInteger(j8,T,1281977716,$A)
call SaveInteger(j8,T,1399879534,1097347121)
call SaveInteger(j8,T,1096968556,1093677146)
call SaveStr(j8,T,1330869362,"magicdefense")
set T=1747988530
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,20)
call SaveInteger(j8,T,1197566318,21)
call SaveReal(j8,T,1214870608,$A0)
call SaveInteger(j8,T,1346466413,75)
call SaveInteger(j8,T,1165520967,1)
call SaveInteger(j8,T,1281977716,$A)
call SaveInteger(j8,T,1399879534,1097347122)
set T=1747988531
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,16)
call SaveInteger(j8,T,1197566318,16)
call SaveReal(j8,T,1214870608,60)
call SaveInteger(j8,T,1296134765,20)
call SaveInteger(j8,T,1165520967,2)
call SaveInteger(j8,T,1281977716,5)
call SaveInteger(j8,T,1399879534,1097347123)
call SaveInteger(j8,T,1096968556,1093677648)
call SaveInteger(j8,T,1096968532,2)
call SaveStr(j8,T,1330869362,"innerfire")
set T=1747988532
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,36)
call SaveInteger(j8,T,1197566318,36)
call SaveReal(j8,T,1214870608,120)
call SaveInteger(j8,T,1165520967,2)
call SaveInteger(j8,T,1281977716,4)
call SaveInteger(j8,T,1399879534,1097347124)
call SaveInteger(j8,T,1096968556,1093677361)
call SaveInteger(j8,T,1096968532,1)
call SaveStr(j8,T,1330869362,"cloudoffog")
set T=1747988533
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,8)
call SaveInteger(j8,T,1197566318,8)
call SaveReal(j8,T,1214870608,50)
call SaveInteger(j8,T,1296134765,20)
call SaveInteger(j8,T,1165520967,2)
call SaveInteger(j8,T,1281977716,5)
call SaveInteger(j8,T,1399879534,1097347125)
call SaveInteger(j8,T,1096968556,1093677145)
call SaveInteger(j8,T,1096968532,2)
call SaveStr(j8,T,1330869362,"invisibility")
set T=1747988534
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,20)
call SaveInteger(j8,T,1197566318,21)
call SaveReal(j8,T,1214870608,125)
call SaveInteger(j8,T,1346466413,20)
call SaveInteger(j8,T,1165520967,1)
call SaveInteger(j8,T,1281977716,$A)
call SaveInteger(j8,T,1399879534,1097347126)
call SaveInteger(j8,T,1096968556,1093677128)
call SaveStr(j8,T,1330869362,"magicdefense")
set T=1865429040
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,4)
call SaveInteger(j8,T,1197566318,5)
call SaveReal(j8,T,1214870608,35)
call SaveInteger(j8,T,1346466413,45)
call SaveInteger(j8,T,1165520967,1)
call SaveInteger(j8,T,1281977716,$A)
call SaveInteger(j8,T,1399879534,1097805872)
set T=1865429041
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,$C)
call SaveInteger(j8,T,1197566318,$D)
call SaveReal(j8,T,1214870608,90)
call SaveInteger(j8,T,1346466413,20)
call SaveInteger(j8,T,1165520967,1)
call SaveInteger(j8,T,1281977716,$A)
call SaveInteger(j8,T,1399879534,1097805873)
set T=1865429042
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,16)
call SaveInteger(j8,T,1197566318,17)
call SaveReal(j8,T,1214870608,120)
call SaveInteger(j8,T,1346466413,20)
call SaveInteger(j8,T,1165520967,1)
call SaveInteger(j8,T,1281977716,$A)
call SaveInteger(j8,T,1399879534,1097805874)
set T=1865429043
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,$C)
call SaveInteger(j8,T,1197566318,$C)
call SaveReal(j8,T,1214870608,65)
call SaveInteger(j8,T,1296134765,20)
call SaveInteger(j8,T,1165520967,2)
call SaveInteger(j8,T,1281977716,5)
call SaveInteger(j8,T,1399879534,1097805875)
call SaveInteger(j8,T,1096968556,1093677140)
call SaveInteger(j8,T,1096968532,2)
call SaveStr(j8,T,1330869362,"bloodlust")
set T=1865429044
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,48)
call SaveInteger(j8,T,1197566318,48)
call SaveReal(j8,T,1214870608,90)
call SaveInteger(j8,T,1346466413,20)
call SaveInteger(j8,T,1165520967,2)
call SaveInteger(j8,T,1281977716,4)
call SaveInteger(j8,T,1399879534,1097805876)
set T=1865429045
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,28)
call SaveInteger(j8,T,1197566318,28)
call SaveReal(j8,T,1214870608,110)
call SaveBoolean(j8,T,1346466413,true)
call SaveInteger(j8,T,1165520967,2)
call SaveInteger(j8,T,1281977716,5)
call SaveInteger(j8,T,1399879534,1097805877)
call SaveInteger(j8,T,1096968556,1093677655)
call SaveInteger(j8,T,1096968532,2)
call SaveStr(j8,T,1330869362,"healingwave")
set T=1865429046
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,44)
call SaveInteger(j8,T,1197566318,44)
call SaveReal(j8,T,1214870608,260)
call SaveInteger(j8,T,1346466413,40)
call SaveInteger(j8,T,1165520967,2)
call SaveInteger(j8,T,1281977716,5)
call SaveInteger(j8,T,1399879534,1097805878)
call SaveInteger(j8,T,1096968556,1093677656)
call SaveStr(j8,T,1330869362,"stomp")
set T=1966092336
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,4)
call SaveInteger(j8,T,1197566318,5)
call SaveReal(j8,T,1214870608,35)
call SaveInteger(j8,T,1346466413,30)
call SaveInteger(j8,T,1296134765,30)
call SaveInteger(j8,T,1165520967,1)
call SaveInteger(j8,T,1281977716,$A)
call SaveInteger(j8,T,1399879534,1098199088)
set T=1966092337
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,$C)
call SaveInteger(j8,T,1197566318,$D)
call SaveReal(j8,T,1214870608,70)
call SaveInteger(j8,T,1346466413,20)
call SaveInteger(j8,T,1296134765,20)
call SaveInteger(j8,T,1165520967,1)
call SaveInteger(j8,T,1281977716,$A)
call SaveInteger(j8,T,1399879534,1098199089)
set T=1966092338
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,20)
call SaveInteger(j8,T,1197566318,20)
call SaveReal(j8,T,1214870608,110)
call SaveInteger(j8,T,1346466413,30)
call SaveInteger(j8,T,1296134765,30)
call SaveInteger(j8,T,1165520967,2)
call SaveInteger(j8,T,1281977716,5)
call SaveInteger(j8,T,1399879534,1098199090)
call SaveInteger(j8,T,1096968556,1093677382)
call SaveInteger(j8,T,1096968532,2)
call SaveStr(j8,T,1330869362,"frostarmor")
set T=1966092339
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,20)
call SaveInteger(j8,T,1197566318,20)
call SaveReal(j8,T,1214870608,60)
call SaveInteger(j8,T,1296134765,30)
call SaveInteger(j8,T,1165520967,2)
call SaveInteger(j8,T,1281977716,5)
call SaveInteger(j8,T,1399879534,1098199091)
call SaveInteger(j8,T,1096968556,1093677142)
call SaveStr(j8,T,1330869362,"raisedead")
set T=1966092340
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,72)
call SaveInteger(j8,T,1197566318,72)
call SaveReal(j8,T,1214870608,$F0)
call SaveInteger(j8,T,1165520967,2)
call SaveInteger(j8,T,1281977716,4)
call SaveInteger(j8,T,1399879534,1098199092)
call SaveInteger(j8,T,1096968556,1093677657)
call SaveInteger(j8,T,1096968532,2)
call SaveStr(j8,T,1330869362,"thunderbolt")
set T=1966092341
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,8)
call SaveInteger(j8,T,1197566318,8)
call SaveReal(j8,T,1214870608,50)
call SaveInteger(j8,T,1296134765,20)
call SaveInteger(j8,T,1165520967,2)
call SaveInteger(j8,T,1281977716,5)
call SaveInteger(j8,T,1399879534,1098199093)
call SaveInteger(j8,T,1096968556,1093677658)
call SaveInteger(j8,T,1096968532,2)
call SaveStr(j8,T,1330869362,"antimagicshell")
set T=1966092342
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,36)
call SaveInteger(j8,T,1197566318,37)
call SaveReal(j8,T,1214870608,$C8)
call SaveInteger(j8,T,1346466413,30)
call SaveInteger(j8,T,1296134765,30)
call SaveInteger(j8,T,1165520967,1)
call SaveInteger(j8,T,1281977716,$A)
call SaveInteger(j8,T,1399879534,1098199094)
set T=1697656880
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,4)
call SaveInteger(j8,T,1197566318,5)
call SaveReal(j8,T,1214870608,35)
call SaveInteger(j8,T,1296134765,45)
call SaveInteger(j8,T,1165520967,1)
call SaveInteger(j8,T,1281977716,$A)
call SaveInteger(j8,T,1399879534,1097150512)
set T=1697656881
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,$C)
call SaveInteger(j8,T,1197566318,$D)
call SaveReal(j8,T,1214870608,80)
call SaveInteger(j8,T,1296134765,60)
call SaveInteger(j8,T,1165520967,1)
call SaveInteger(j8,T,1281977716,$A)
call SaveInteger(j8,T,1399879534,1097150513)
set T=1697656882
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,16)
call SaveInteger(j8,T,1197566318,17)
call SaveReal(j8,T,1214870608,80)
call SaveBoolean(j8,T,1296134765,true)
call SaveInteger(j8,T,1165520967,1)
call SaveInteger(j8,T,1281977716,$A)
call SaveInteger(j8,T,1399879534,1097150514)
set T=1697656883
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,8)
call SaveInteger(j8,T,1197566318,8)
call SaveReal(j8,T,1214870608,50)
call SaveInteger(j8,T,1296134765,20)
call SaveInteger(j8,T,1165520967,2)
call SaveInteger(j8,T,1281977716,5)
call SaveInteger(j8,T,1399879534,1097150515)
call SaveInteger(j8,T,1096968556,1093677872)
call SaveInteger(j8,T,1096968532,2)
call SaveStr(j8,T,1330869362,"rejuvination")
set T=1697656884
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,48)
call SaveInteger(j8,T,1197566318,48)
call SaveReal(j8,T,1214870608,90)
call SaveInteger(j8,T,1296134765,20)
call SaveInteger(j8,T,1165520967,2)
call SaveInteger(j8,T,1281977716,4)
call SaveInteger(j8,T,1399879534,1097150516)
set T=1697656885
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,$C)
call SaveInteger(j8,T,1197566318,$C)
call SaveReal(j8,T,1214870608,80)
call SaveInteger(j8,T,1165520967,2)
call SaveInteger(j8,T,1281977716,5)
call SaveInteger(j8,T,1399879534,1097150517)
call SaveInteger(j8,T,1096968556,1093677143)
call SaveStr(j8,T,1330869362,"roar")
set T=1697656886
set n=n+1
set k8[n]=T
call SaveInteger(j8,T,1131377524,60)
call SaveInteger(j8,T,1197566318,60)
call SaveReal(j8,T,1214870608,280)
call SaveInteger(j8,T,1346466413,75)
call SaveInteger(j8,T,1296134765,55)
call SaveInteger(j8,T,1165520967,2)
call SaveInteger(j8,T,1281977716,5)
call SaveInteger(j8,T,1399879534,1097150518)
call SaveInteger(j8,T,1096968556,1093677649)
call SaveStr(j8,T,1330869362,"taunt")
set MT=n
endfunction
function Pe takes nothing returns nothing
set AT=AT+1
set Y7[AT]=1953063787
set AT=AT+1
set Y7[AT]=1651861094
set AT=AT+1
set Y7[AT]=1093677125
set AT=AT+1
set Y7[AT]=1093677126
set AT=AT+1
set Y7[AT]=1400141166
set AT=AT+1
set Y7[AT]=1093677106
set AT=AT+1
set Y7[AT]=1865429041
set AT=AT+1
set Y7[AT]=1865429044
set AT=AT+1
set Y7[AT]=1093677655
set AT=AT+1
set Y7[AT]=1697656881
set AT=AT+1
set Y7[AT]=1697656884
set AT=AT+1
set Y7[AT]=1093677146
set AT=AT+1
set Y7[AT]=1093677648
set AT=AT+1
set Y7[AT]=1093677145
set AT=AT+1
set Y7[AT]=1093677128
set AT=AT+1
set Y7[AT]=1093677140
set AT=AT+1
set Y7[AT]=1093677382
set AT=AT+1
set Y7[AT]=1093677658
set AT=AT+1
set Y7[AT]=1093677872
set AT=AT+1
set Y7[AT]=1093677143
endfunction
function Re takes nothing returns nothing
local integer n=0
set h8[n]="|cffff0000"
set n=1
set h8[n]="|cff0000ff"
set n=2
set h8[n]="|cff00ffff"
set n=3
set h8[n]="|cff691e87"
set n=4
set h8[n]="|cffffff00"
set n=5
set h8[n]="|cffd25a1e"
set n=6
set h8[n]="|cff00ff00"
set n=7
set h8[n]="|cffff8080"
set n=8
set h8[n]="|cff808080"
set n=9
set h8[n]="|cff8080ff"
set n=$A
set h8[n]="|cff008080"
endfunction
function Te takes nothing returns nothing
local string s
local quest q=CreateQuest()
local questitem Ue=QuestCreateItem(q)
set X7="Default game settings:
|cffcc8080"+I2S(P7)+"|r lives.
|cff0080ff1|r race per player.
"
set X7=X7+"|cffffff00"+I2S(60)+"|r starting gold.
All races are |cff00ff00allowed|r.
"
set X7=X7+"Players send their minions to a random foe."
call QuestItemSetDescription(Ue,"Choose race of your minions.")
set Ue=QuestCreateItem(q)
call QuestItemSetDescription(Ue,"Send minions to your foes to defeat them.")
set Ue=QuestCreateItem(q)
call QuestItemSetDescription(Ue,"Build towers to protect yourself from enemy minions.")
set Ue=QuestCreateItem(q)
call QuestItemSetDescription(Ue,"You will lose when amount of your lives becomes |cffcc80800|r.")
set s="Players should choose the race of their minions, then build a maze of towers, spawn some minions and press button 'Readiness' in their builder. "
set s=s+"When the wave starts, minions will be teleported to upper watery area of every player. "
set s=s+"If minion reaches bottom watery area, player will lose |cffcc80801|r life. If player loses all lives, he will be defeated. "
set s=s+"As soon as all minions will be killed, players should prepare and then confirm their readiness.
"
set s=s+"Players receive all gold when wave ends. This amount depends on type and number of minions that player has spawned before wave started. "
set s=s+"In addition, kills of minions increases income of player-killer by |cffff8000"+I2S(1)+"|r."
call QuestSetTitle(q,"How to play")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNSelectHeroOn.blp")
set s="Here is next types of damage:
"
set s=s+"|cffff8700Physical|r - deals |cffff8700Physical|r damage. Can be reduced by |cffff8700Physical|r armor and blocked by |cffff8700Immunity|r or |cffccccccProtection|r.
"
set s=s+"|cff8700ffMagical|r - deals |cff8700ffMagical|r damage. Can be reduced by |cff8700ffMagical|r armor and blocked by |cff8700ffImmunity|r or |cffccccccProtection|r.
"
set s=s+"|cffccccccPercetage|r - deals damage that equals some percents of target's maximum health. Cannot be reduced and can be blocked by |cffccccccProtection|r.

"
set s=s+"If minion has |cffff8700x|r\\|cff8700ffy|r armor, incoming |cffff8700Physical|r damage will be reduced by |cffff8700x%|r and |cff8700ffMagical|r by |cff8700ffy%|r. "
set s=s+"For example, if armor is |cffff870020|r\\|cff8700ff30|r and incoming |cffff8700Physical|r damage is |cffff00004|r, minion will receive only |cffff00003.2|r damage.
"
set s=s+"Upper bound of armor is |cffff870075|r\\|cff8700ff75|r and lower bound is |cffff8700-25|r\\|cff8700ff-25|r.
"
set s=s+"Some more words about |cffccccccPercetage|r damage. If incoming amount of damage is |cffff0000p|r of |cffccccccPercetage|r type, "
set s=s+"minion with |cff33cc33h|r maximum health will receive damage equals |cffff0000p%|r of |cff33cc33h|r. "
set s=s+"For example, if maximum health is |cff33cc33200|r and incoming |cffccccccPersentage|r damage is |cffff00008|r, minion will receive |cffff000016|r damage.
"
set q=CreateQuest()
call QuestSetTitle(q,"Armor & damage system")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNHelmutPurple.blp")
set s="Tower description:
"
set s=s+"Class - class of tower. Affects on tower's abilities.
"
set s=s+"Damage - default damage of tower.
"
set s=s+"Type - damage type.
"
set s=s+"Cooldown - time between two consistent attacks.
"
set s=s+"Range - attack range of tower (in cells).
"
set s=s+"AoE - AoE radius (in cells)\\AoE damage factor.
"
set s=s+"Targets - types of targets that tower can hit.
"
set s=s+"Kill Damage - amount of bonus damage per kill.
"
set s=s+"Levels - amount of experience required to reach 1st\\2nd level.
"
set s=s+"Ability - tower's passive ability.

"
set s=s+"Tower details:
"
set s=s+"Exp. - current amount of experience.
"
set s=s+"Kills - current amount of kills.
"
set s=s+"Damage - current damage of tower.
"
set s=s+"Type - current damage type.

"
set s=s+"Players can build towers only on grinded area and only between waves.
When tower kills minion, it and the nearest ones get experience. "
set s=s+"After reaching certain amount of experience towers get level. On every level player can choose only one ability to improve tower."
set q=CreateQuest()
call QuestSetTitle(q,"Towers")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNHumanWatchTower.blp")
set s="Minion description:
"
set s=s+"Gold - gold cost.
"
set s=s+"Income - income increment.
"
set s=s+"Armor - amount of |cffff8700Physical|r\\|cff8700ffMagical|r armor.
"
set s=s+"Movement - movement speed (cells per second)\\movement type.
"
set s=s+"Exp. - amount of experience gained when killed.
"
set s=s+"Limit - maximum amount of minions of this type that player can spawn.
"
set s=s+"Ability - description minion's ability.

"
set s=s+"When you spawn a minion, it appears in top enclosed watery area. When wave begins, minions will be teleported maintaining their positions relative to each other.
"
set s=s+"Players cannot control their minions. Minion casts their abilities automatically."
set q=CreateQuest()
call QuestSetTitle(q,"Minions")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\WorldEditUI\\Editor-MultipleUnits.blp")
set s="This is list of commands to modify game settings. Type them into chat to set up game.
# - integer number.

"
set s=s+"These commands can be used only by modemaker before game starts:
"
set s=s+"lif# - sets amount of lives (0 < #).
"
set s=s+"gol# - sets amount of starting gold ("+I2S(60)+" < #).
"
set s=s+"rac# - sets amount of races per player (0 < # < 5)
"
set s=s+"hum - allows to select Human race.
"
set s=s+"orc - allows to select Orc race.
"
set s=s+"und - allows to select Undead race.
"
set s=s+"elf - allows to select Night Elf race.
"
set s=s+"-hum - bans Human race.
"
set s=s+"-orc - bans Orc race.
"
set s=s+"-und - bans Undead race.
"
set s=s+"-elf - bans Night Elf race.
"
set s=s+"next - players send their minions to next player.
"
set s=s+"rand - players send their minions to random foe.
"
set s=s+"rans - players send their minions to random foe or themselves.
"
set s=s+"self - players send their minions to themselves.
"
set s=s+"start - starts game.
"
set q=CreateQuest()
call QuestSetTitle(q,"Mode commands")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNLesserRejuvScroll.blp")
set s="Special thanks for |cffccccccxgm.guru/p/wc3|r and |cffff3333youtube.com/2kxaoc|r.

"
set s=s+"Map created by |cff80ccffvk.com/prometheus3375|r.
All original icons and models created by |cff0080ffus.blizzard.com|r.

"
set s=s+"Additional contacts:
Twitter: |cff80ccfftwitter.com/prometheus3375|r.
Telegram: |cff80ccff@Prometheus3375|r.
E-mail: |cff80ccffprometheus3375@gmail.com|r."
set q=CreateQuest()
call QuestSetRequired(q,false)
call QuestSetTitle(q,"Credits")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNScrollOfHealing.blp")
call TriggerExecute(N7)
call TriggerExecute(G7)
call TriggerExecute(H7)
call TriggerExecute(I7)
call TriggerExecute(l7)
call TriggerExecute(J7)
call TriggerExecute(K7)
call TriggerExecute(L7)
call TriggerExecute(M7)
set q=CreateQuest()
call QuestSetRequired(q,false)
call QuestSetTitle(q,"|cff909090version 1.0|r")
call QuestSetDescription(q,"Release.")
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNScroll.blp")
set q=null
set Ue=null
endfunction
function We takes nothing returns nothing
local integer i=0
set i8=i8+1
loop
exitwhen i>$A
if w8[i]then
call SetPlayerState(Player(i),PLAYER_STATE_RESOURCE_LUMBER,i8)
endif
set i=i+1
endloop
endfunction
function Xe takes nothing returns nothing
local integer a=0
local player p
local string s="Welcome to WispTD!
You are a modemaker this game. You have 60 seconds to modify game settings. Type 'start' to start game immediately.
Click Help (|cffffcc00F9|r) to read tutorials, mode commands and tips."
call TimerStart(CreateTimer(),60,true,function We)
loop
exitwhen w8[a]
set a=a+1
endloop
set p=Player(a)
call DisplayTimedTextToPlayer(p,0,0,60,s+"

"+X7)
set s=ae(a)
set s="Welcome to WispTD!
"+s+" is a modemaker this game. Wait 60 seconds or until "+s+" types 'start'.
Click Help (|cffffcc00F9|r) to read tutorials and tips."
set a=a+1
loop
exitwhen a>$A
if w8[a]then
set p=Player(a)
call DisplayTimedTextToPlayer(p,0,0,60,s+"

"+X7)
endif
set a=a+1
endloop
set c8=CreateTimerDialog(D8)
call TimerDialogSetTitle(c8,"Game starts in:")
call TimerDialogSetTimeColor(c8,$FF,$FF,$FF,$FF)
call TimerDialogSetTitleColor(c8,$FF,$FF,$FF,$FF)
call TimerStart(D8,60,false,null)
set p=null
call TriggerSleepAction(.4)
call TimerDialogDisplay(c8,true)
endfunction
function Ze takes nothing returns nothing
local string s=GetEventPlayerChatString()
local string Ee=SubString(s,3,4)
local string df
local integer l=StringLength(s)
local player p=GetTriggerPlayer()
local boolean b=true
if(Ee=="-")then
set b=false
endif
if b then
set df=" is |cff00ff00allowed|r."
else
set df=" is |cffff0000banned|r."
endif
set Ee=SubString(s,0,3)
if(Ee=="lif")then
set Ee=SubString(s,3,l)
set l=S2I(Ee)
if(l>0)then
set P7=l
call DisplayTimedTextToPlayer(p,0,0,4,"Amount of lives is set to |cffcc8080"+Ee+"|r.")
endif
elseif(Ee=="gol")then
set Ee=SubString(s,3,l)
set l=S2I(Ee)
if(l>=60)then
set Q7=l
call DisplayTimedTextToPlayer(p,0,0,4,"Starting gold is set to |cffffff00"+Ee+"|r.")
endif
elseif(Ee=="rac")then
set Ee=SubString(s,3,4)
set l=S2I(Ee)
if(l>0)and(l<5)then
set R7=l
call DisplayTimedTextToPlayer(p,0,0,4,"Amount of races per palyer is set to |cff0080ff"+Ee+"|r.")
endif
elseif(Ee=="hum")then
set S7=b
call DisplayTimedTextToPlayer(p,0,0,4,"Human race"+df)
elseif(Ee=="orc")then
set T7=b
call DisplayTimedTextToPlayer(p,0,0,4,"Orc race"+df)
elseif(Ee=="und")then
set U7=b
call DisplayTimedTextToPlayer(p,0,0,4,"Undead race"+df)
elseif(Ee=="elf")then
set V7=b
call DisplayTimedTextToPlayer(p,0,0,4,"Night Elf race"+df)
endif
if(s=="next")then
set W7=0
call DisplayTimedTextToPlayer(p,0,0,4,"Players send their minions to next player.")
elseif(s=="rand")then
set W7=1
call DisplayTimedTextToPlayer(p,0,0,4,"Players send their minions to random foe.")
elseif(s=="rans")then
set W7=2
call DisplayTimedTextToPlayer(p,0,0,4,"Players send their minions to random foe or themselves.")
elseif(s=="self")then
set W7=3
call DisplayTimedTextToPlayer(p,0,0,4,"Players send their minions to themselves.")
endif
set p=null
endfunction
function ff takes nothing returns nothing
local integer a=0
local integer T=0
local integer gf=0
local integer k=0
local real x1=0
local real x2=0
local real x=0
local real y=0
local real r1=0
local real r2=0
local player p=null
local string s="Current game settings:
|cffcc8080"
call DestroyTimerDialog(c8)
set c8=null
if(P7==1)then
set s=s+"1|r life."
else
set s=s+I2S(P7)+"|r lives."
endif
set s=s+"
|cffffff00"+I2S(Q7)+"|r starting gold.
|cff0080ff"
if(R7==1)then
set s=s+"1|r race per player.
Human race"
else
set s=s+I2S(R7)+"|r races per player.
Human race"
endif
if S7 then
set s=s+" is |cff00ff00allowed|r.
Orc race"
else
set s=s+" is |cffff0000banned|r.
Orc race"
endif
if T7 then
set s=s+" is |cff00ff00allowed|r.
Undead race"
else
set s=s+" is |cffff0000banned|r.
Undead race"
endif
if U7 then
set s=s+" is |cff00ff00allowed|r.
Night Elf race"
else
set s=s+" is |cffff0000banned|r.
Night Elf race"
endif
if V7 then
set s=s+" is |cff00ff00allowed|r.
"
else
set s=s+" is |cffff0000banned|r.
"
endif
if bj_isSinglePlayer then
set s=s+"Singleplayer game. You send your minions to yourself."
elseif(W7==0)then
set s=s+"Players send their minions to next player."
elseif(W7==1)then
set s=s+"Players send their minions to random foe."
elseif(W7==2)then
set s=s+"Players send their minions to random foe or themselves."
elseif(W7==3)then
set s=s+"Players send their minions to themselves."
endif
call ClearTextMessages()
loop
exitwhen a>$A
if w8[a]then
set p=Player(a)
call DisplayTimedTextToPlayer(p,0,0,$F,s)
set v8[a]=P7
call o9(a,Q7,true)
call SetPlayerState(p,PLAYER_STATE_RESOURCE_FOOD_CAP,40)
set k=0
loop
exitwhen k>$A
call SetPlayerAlliance(p,Player(k),ALLIANCE_SHARED_VISION,true)
set k=k+1
endloop
set l8[(a)*(2)+0]=CreateFogModifierRect(p,FOG_OF_WAR_VISIBLE,A8[a],true,true)
call FogModifierStart(l8[(a)*(2)+0])
set l8[(a)*(2)+1]=CreateFogModifierRect(p,FOG_OF_WAR_VISIBLE,z8[a],true,true)
call FogModifierStart(l8[(a)*(2)+1])
set k=0
set gf=0
set x1=GetRectMinX(B8[a])
set x2=GetRectMaxX(B8[a])
set x=GetRectMinX(B8[a])
set y=GetRectMaxY(B8[a])
call SaveBoolean(j8,a,1198680420,true)
loop
exitwhen k>55
set r1=k*.5
set r2=gf*.5
if(R2I(r1)<r1 and R2I(r2)==r2)or(R2I(r1)==r1 and R2I(r2)<r2)then
set T=1885435442
elseif(R2I(r1)<r1 and R2I(r2)<r2)or(R2I(r1)==r1 and R2I(r2)==r2)then
set T=1885435441
endif
set H8[(a)*(56)+k]=CreateUnit(p,T,x,y,0)
call SetUnitPathing(H8[(a)*(56)+k],false)
set x=x+$80
if(x>x2)then
set y=y-$80
set x=x1
set gf=gf+1
endif
set k=k+1
endloop
set x=GetRectCenterX(B8[a])
set y=GetRectCenterY(B8[a])
set q8[a]=CreateUnit(p,1651270770,x,y,0)
if S7 then
call UnitAddAbility(q8[a],1093677875)
endif
if T7 then
call UnitAddAbility(q8[a],1093677876)
endif
if U7 then
call UnitAddAbility(q8[a],1093677877)
endif
if V7 then
call UnitAddAbility(q8[a],1093677878)
endif
call SetUnitUserData(q8[a],R7)
set r8[a]=GetHandleId(q8[a])
if i9(a)then
call PanCameraToTimed(x,y,0)
call SelectUnit(q8[a],true)
endif
call TriggerRegisterUnitEvent(f7,q8[a],EVENT_UNIT_ISSUED_POINT_ORDER)
call TriggerRegisterUnitEvent(g7,q8[a],EVENT_UNIT_SPELL_EFFECT)
call TriggerRegisterUnitEvent(h7,q8[a],EVENT_UNIT_SPELL_EFFECT)
call TriggerRegisterUnitEvent(j7,q8[a],EVENT_UNIT_SPELL_EFFECT)
call TriggerRegisterUnitEvent(k7,q8[a],EVENT_UNIT_SPELL_EFFECT)
endif
set a=a+1
endloop
set p=null
call TriggerExecute(U4)
set o8=o8+1
set m8=90
call MultiboardSetTitleText(Z7,"Wave "+I2S(o8)+" starts in "+I2S(m8)+" seconds")
call MultiboardDisplay(Z7,true)
call MultiboardMinimize(Z7,false)
call TimerStart(D8,1,true,function ve)
call DisableTrigger(T4)
call DisableTrigger(S4)
endfunction
function jf takes nothing returns nothing
local integer p9
local integer i=0
local multiboarditem u9
loop
exitwhen i>$A
if w8[i]then
set p9=GetPlayerState(Player(i),PLAYER_STATE_RESOURCE_GOLD)
set u9=MultiboardGetItem(Z7,x8[i],2)
call MultiboardSetItemValue(u9,I2S(p9))
call MultiboardReleaseItem(u9)
endif
set i=i+1
endloop
set u9=null
endfunction
function kf takes nothing returns nothing
local integer n=0
local integer a=0
local integer gf=1
local integer mf=0
local real L0=$D
local real L1=5
local real L2=5
local multiboarditem u9
set Z7=CreateMultiboard()
call MultiboardDisplay(Z7,false)
call MultiboardSetColumnCount(Z7,5)
call MultiboardSetTitleTextColor(Z7,$FF,$FF,$FF,$FF)
loop
exitwhen a>$A
if w8[a]then
set n=n+1
set x8[a]=n
set mf=StringLength(GetPlayerName(Player(a)))+3
call MultiboardSetRowCount(Z7,n+1)
set u9=MultiboardGetItem(Z7,n,0)
call MultiboardSetItemValue(u9,ae(a))
call MultiboardReleaseItem(u9)
if L0<mf then
set L0=mf
endif
endif
set a=a+1
endloop
set L0=L0*.005
set mf=StringLength(I2S(P7))
if L1<mf then
set L1=mf
endif
set L1=L1*.005
set mf=StringLength(I2S(Q7))
if L2<mf then
set L2=mf
endif
set L2=L2*.005
set u9=MultiboardGetItem(Z7,0,0)
call MultiboardSetItemValue(u9,"Status/Player")
call MultiboardSetItemValueColor(u9,$FF,$CC,$80,$FF)
call MultiboardSetItemWidth(u9,L0)
call MultiboardReleaseItem(u9)
set u9=MultiboardGetItem(Z7,0,1)
call MultiboardSetItemValue(u9,"Lives")
call MultiboardSetItemValueColor(u9,$FF,$CC,$80,$FF)
call MultiboardSetItemWidth(u9,L1)
call MultiboardReleaseItem(u9)
set u9=MultiboardGetItem(Z7,0,2)
call MultiboardSetItemValue(u9,"Gold")
call MultiboardSetItemValueColor(u9,$FF,$CC,$80,$FF)
call MultiboardSetItemWidth(u9,L2)
call MultiboardReleaseItem(u9)
set u9=MultiboardGetItem(Z7,0,3)
call MultiboardSetItemValue(u9,"Income")
call MultiboardSetItemValueColor(u9,$FF,$CC,$80,$FF)
call MultiboardSetItemWidth(u9,.035)
call MultiboardReleaseItem(u9)
set u9=MultiboardGetItem(Z7,0,4)
call MultiboardSetItemValue(u9,"Kills")
call MultiboardSetItemValueColor(u9,$FF,$CC,$80,$FF)
call MultiboardSetItemWidth(u9,.025)
call MultiboardReleaseItem(u9)
call MultiboardSetItemsStyle(Z7,true,false)
call MultiboardSetItemsIcon(Z7,"Icons\\PASPreparation.blp")
loop
exitwhen gf>n
set a=0
loop
exitwhen a>4
set u9=MultiboardGetItem(Z7,gf,a)
if(a==0)then
call MultiboardSetItemStyle(u9,true,true)
call MultiboardSetItemWidth(u9,L0)
elseif(a==1)then
call MultiboardSetItemValue(u9,I2S(P7))
call MultiboardSetItemValueColor(u9,$CC,$80,$80,$FF)
call MultiboardSetItemWidth(u9,L1)
elseif(a==2)then
call MultiboardSetItemValue(u9,I2S(Q7))
call MultiboardSetItemValueColor(u9,$FF,$FF,0,$FF)
call MultiboardSetItemWidth(u9,L2)
elseif(a==3)then
call MultiboardSetItemValue(u9,"0")
call MultiboardSetItemValueColor(u9,$FF,$80,0,$FF)
call MultiboardSetItemWidth(u9,.035)
elseif(a==4)then
call MultiboardSetItemValue(u9,"0")
call MultiboardSetItemValueColor(u9,$FF,51,51,$FF)
call MultiboardSetItemWidth(u9,.025)
endif
call MultiboardReleaseItem(u9)
set a=a+1
endloop
set gf=gf+1
endloop
set u9=null
call TimerStart(CreateTimer(),.25,true,function jf)
endfunction
function of takes unit t,unit m returns nothing
local integer i=(LoadInteger(j8,GetHandleId((m)),1333227088))
local integer H9=LoadInteger(j8,GetUnitTypeId(m),1096968556)
local integer tH=GetHandleId(t)
local integer tT=GetUnitTypeId(t)
local integer pf=LoadInteger(j8,tH,1131574321)
local integer qf=LoadInteger(j8,tH,1131574322)
local real de=a9(t)
local integer rf=LoadInteger(j8,tH,1148020564)
local real sf=A9(t)
local real tf=de*LoadReal(j8,tT,1097811268)
local real hp=GetUnitState(m,UNIT_STATE_LIFE)
local real x=GetUnitX(m)
local real y=GetUnitY(m)
local real uf=hp-de
local integer n=0
local unit m0
call SaveInteger(j8,tH,1131574321,pf+1)
call SaveInteger(j8,tH,1131574322,qf+1)
if(GetUnitAbilityLevel((t),(1093677125))>0)then
call Qd(m)
elseif(GetUnitAbilityLevel((t),(1093677126))>0)then
call Sd(m)
elseif(GetUnitAbilityLevel((t),(1093677131))>0)and(uf>=2)then
call F9(t,1685417264,1093677139,"acidbomb",2,0,0,m)
elseif(GetUnitAbilityLevel((t),(1093677389))>0)and(pf==4)then
call SaveInteger(j8,tH,1131574321,1)
call F9(t,1685417265,1093677384,"rainoffire",1,x,y,null)
elseif(GetUnitAbilityLevel((t),(1093677391))>0)and(pf==4)then
call SaveInteger(j8,tH,1131574321,1)
call F9(t,1685417265,1093677378,"rainoffire",1,x,y,null)
elseif(GetUnitAbilityLevel((t),(1093677393))>0)and(pf==4)then
call SaveInteger(j8,tH,1131574321,1)
call F9(t,1685417265,1093677396,"rainoffire",1,x,y,null)
elseif(GetUnitAbilityLevel((t),(1093677619))>0)and(pf==3)then
call SaveInteger(j8,tH,1131574321,1)
call D9(t,2003985456,x,y)
elseif(GetUnitAbilityLevel((t),(1093677621))>0)and(pf==3)then
call SaveInteger(j8,tH,1131574321,1)
call D9(t,2003985457,x,y)
elseif(GetUnitAbilityLevel((t),(1093677623))>0)and(pf==3)then
call SaveInteger(j8,tH,1131574321,1)
call D9(t,2003985458,x,y)
endif
if(GetUnitAbilityLevel((t),(1093677127))>0)then
call fd(t,m,1,1348825699)
elseif(GetUnitAbilityLevel((t),(1093677137))>0)and(pf==4)then
call SaveInteger(j8,tH,1131574321,1)
set de=de+4
elseif(GetUnitAbilityLevel((t),(1093677136))>0)and(pf==$A)then
call SaveInteger(j8,tH,1131574321,1)
call Ud(m,1)
elseif(GetUnitAbilityLevel((t),(1093677363))>0)and(qf==$A)then
if(H9>0)and(uf>=2)then
call SaveInteger(j8,tH,1131574322,1)
call F9(t,1685417266,1093677106,"chainlightning",2,0,0,m)
else
call SaveInteger(j8,tH,1131574322,$A)
endif
elseif(GetUnitAbilityLevel((t),(1093677365))>0)and(qf==$A)then
if(uf>=2)then
call SaveInteger(j8,tH,1131574322,1)
call F9(t,1685417266,1093677107,"chainlightning",2,0,0,m)
else
call SaveInteger(j8,tH,1131574322,$A)
endif
elseif(GetUnitAbilityLevel((t),(1093677379))>0)and(qf==$A)then
if(uf>=2)then
call SaveInteger(j8,tH,1131574322,1)
call F9(t,1685417266,1093677108,"chainlightning",2,0,0,m)
else
call SaveInteger(j8,tH,1131574322,$A)
endif
endif
if(sf>0)then
loop
exitwhen n>FN[i]
set m0=F8[(i)*(50)+n]
if(j9(m,m0)<=sf)then
if(m0==m)then
call fd(t,m0,de,rf)
else
call fd(t,m0,tf,rf)
endif
endif
set n=n+1
endloop
else
call fd(t,m,de,rf)
endif
set m0=null
endfunction
function vf takes unit d,unit m,real de returns nothing
local integer dH=GetHandleId(d)
local unit t=LoadUnitHandle(j8,dH,0)
local integer tH=GetHandleId(t)
local integer H9=LoadInteger(j8,GetUnitTypeId(m),1096968556)
local real b=1
if(GetUnitAbilityLevel((t),(1093677385))>0)then
set b=2
elseif(GetUnitAbilityLevel((t),(1093677399))>0)then
set b=1+LoadInteger(j8,tH,1265200236)*.02
endif
if(GetUnitAbilityLevel((d),(1093677139))>0)then
call fd(t,m,3,1348825699)
elseif(GetUnitAbilityLevel((d),(1093677384))>0)then
call fd(t,m,2*b,1298229091)
elseif(GetUnitAbilityLevel((d),(1093677378))>0)then
call fd(t,m,2*b,1298229091)
elseif(GetUnitAbilityLevel((d),(1093677396))>0)and(de==.02)then
call fd(t,m,2*b,1298229091)
elseif(GetUnitAbilityLevel((d),(1093677396))>0)and(de==.01)then
call fd(t,m,1*b,1298229091)
elseif(GetUnitAbilityLevel((d),(1093677106))>0)and(H9>0)then
call Wd(m)
elseif(GetUnitAbilityLevel((d),(1093677107))>0)then
call fd(t,m,6*b,1298229091)
elseif(GetUnitAbilityLevel((d),(1093677108))>0)then
call Ud(m,1)
endif
set t=null
endfunction
function wf takes nothing returns nothing
local unit xf=GetEventDamageSource()
local integer yf=GetUnitTypeId(xf)
local integer i=(GetPlayerId(GetOwningPlayer((xf))))
local unit m=GetTriggerUnit()
local integer T=GetUnitTypeId(m)
local real hp=GetUnitState(m,UNIT_STATE_LIFE)
local real de=GetEventDamage()
local integer rf=LoadInteger(j8,r8[i],1148020564)
if(yf!=1651270770)then
call SetUnitState(m,UNIT_STATE_LIFE,hp+de)
endif
if(yf==1651270770)and(hp-de<2)then
if(T==1865429044)then
call Zd(m,de)
elseif(T==1697656884)then
call ke(m,de)
endif
elseif IsUnitType(xf,UNIT_TYPE_STRUCTURE)then
call of(xf,m)
elseif IsUnitType(xf,UNIT_TYPE_ANCIENT)then
call vf(xf,m,de)
elseif(yf==1865429045)then
call he(m)
endif
set xf=null
set m=null
endfunction
function zf takes nothing returns boolean
return GetEventDamage()>0
endfunction
function af takes nothing returns nothing
local integer a=0
local integer array Bf
local integer n=-1
local integer u=-1
local integer array bf
local integer s=-1
local integer array Cf
local integer f=-1
local integer i=0
local integer r=0
loop
exitwhen a>$A
if w8[a]then
set n=n+1
set Bf[n]=a
endif
set a=a+1
endloop
if(n==0)or(W7==3)then
loop
exitwhen u==n
set u=u+1
set s8[Bf[u]]=Bf[u]
endloop
return
endif
if(W7==0)then
loop
exitwhen u==n
set u=u+1
if(u!=n)then
set s8[Bf[u]]=Bf[u+1]
else
set s8[Bf[n]]=Bf[0]
endif
endloop
return
endif
loop
exitwhen s==n
set s=s+1
set bf[s]=Bf[s]
endloop
if(W7==2)then
loop
exitwhen u==n
set u=u+1
set r=GetRandomInt(0,s)
set s8[Bf[u]]=bf[r]
set bf[r]=bf[s]
set s=s-1
endloop
return
endif
loop
exitwhen s==1
set u=u+1
set f=-1
loop
exitwhen f==s
set f=f+1
set Cf[f]=bf[f]
endloop
set i=0
loop
exitwhen(Cf[i]==Bf[u])or(i>f)
set i=i+1
endloop
if(i<=f)then
set Cf[i]=Cf[f]
set f=f-1
endif
set r=GetRandomInt(0,f)
set s8[Bf[u]]=Cf[r]
set i=0
loop
exitwhen bf[i]==Cf[r]
set i=i+1
endloop
set bf[i]=bf[s]
set s=s-1
endloop
set u=u+1
set i=0
loop
exitwhen(Bf[u]==bf[i])or(i==2)
set i=i+1
endloop
if(i==0)then
set s8[Bf[u]]=bf[1]
set s8[Bf[u+1]]=bf[0]
return
elseif(i==1)then
set s8[Bf[u]]=bf[0]
set s8[Bf[u+1]]=bf[1]
return
endif
set i=0
loop
exitwhen(Bf[u+1]==bf[i])or(i==2)
set i=i+1
endloop
if(i==0)then
set s8[Bf[u]]=bf[0]
set s8[Bf[u+1]]=bf[1]
return
elseif(i==1)then
set s8[Bf[u]]=bf[1]
set s8[Bf[u+1]]=bf[0]
return
endif
set r=GetRandomInt(0,1)
set s8[Bf[u]]=bf[r]
if(r==0)then
set r=1
else
set r=0
endif
set s8[Bf[u+1]]=bf[r]
endfunction
function Df takes nothing returns nothing
local unit t=GetKillingUnit()
local integer i=(GetPlayerId(GetOwningPlayer((t))))
local unit m=GetDyingUnit()
local integer mH=GetHandleId(m)
local integer T=GetUnitTypeId(m)
local integer C9=LoadInteger(j8,T,1165520967)
local unit Ef=LoadUnitHandle(j8,mH,r8[i])
local integer Ff=GetHandleId(Ef)
local integer Gf=LoadInteger(j8,Ff,1265200236)+1
local multiboarditem u9=MultiboardGetItem(Z7,x8[i],4)
local string s=""
local integer a=0
call SaveInteger(j8,Ff,1265200236,Gf)
set u8[i]=u8[i]+1
call MultiboardSetItemValue(u9,I2S(u8[i]))
call MultiboardReleaseItem(u9)
call s9(i,1)
if(C9>0)then
loop
exitwhen a>TN[i]
set t=I8[(i)*(56)+a]
set T=GetUnitTypeId(t)
if(t!=Ef)and(T!=1953462064)and(j9(t,m)<=384)then
call v9("|cFF80CCFF+"+I2S(C9)+"|r",t,8.5,64,i,2,1)
call b9(t,C9)
endif
set a=a+1
endloop
call b9(Ef,C9)
set s="|cFF80CCFF+"+I2S(C9)+"|r
"
endif
call v9(s+"|cFFFF3333+1|r",Ef,8.5,64,i,2,1)
call pe(m)
set t=null
set m=null
set Ef=null
set u9=null
endfunction
function If takes nothing returns nothing
local integer i=GetPlayerId(GetTriggerPlayer())
if w8[i]then
call be(i)
endif
endfunction
function Jf takes nothing returns nothing
set m8=m8-1
set d8="Game will end in "+I2S(m8)+" second"
if m8!=1 then
set d8=d8+"s"
endif
if m8==0 then
call MultiboardSetTitleText(Z7,"Game is ending...")
call EndGame(true)
else
call MultiboardSetTitleText(Z7,d8)
endif
endfunction
function Kf takes nothing returns nothing
local integer i=0
set O7=false
call ClearTextMessages()
loop
if w8[i]then
call be(i)
endif
call DisplayTimedTextToPlayer(Player(i),.0,.0,7.,"Game will end in 7 seconds.")
set i=i+1
exitwhen i==$B
endloop
set m8=8
call Jf()
call TimerStart(D8,1.,true,function Jf)
endfunction
function Lf takes nothing returns nothing
local integer i=E8
local integer H=r8[i]
local boolean Mf=true
local texttag z9
local effect e
local multiboarditem u9
local integer a=0
set w8[i]=false
set p8=p8-1
if O7 then
call Be()
endif
call FlushChildHashtable(j8,i)
call FlushChildHashtable(j8,H)
call RemoveUnit(q8[i])
set q8[i]=null
loop
if a<=FN[i]then
set H=GetHandleId(F8[(i)*(50)+a])
set z9=LoadTextTagHandle(j8,H,1415936116)
if(z9!=null)then
call DestroyTextTag(z9)
endif
call oe(H)
call FlushChildHashtable(j8,H)
call RemoveUnit(F8[(i)*(50)+a])
set F8[(i)*(50)+a]=null
if Mf then
set Mf=false
endif
endif
if a<=LN[i]then
set G8[(i)*(50)+a]=null
if Mf then
set Mf=false
endif
endif
if a<=55 then
call RemoveUnit(H8[(i)*(56)+a])
set H8[(i)*(56)+a]=null
if Mf then
set Mf=false
endif
endif
if a<=TN[i]then
set H=GetHandleId(I8[(i)*(56)+a])
set e=LoadEffectHandle(j8,H,1281717868)
if e!=null then
call DestroyEffect(e)
endif
call re(H)
call me(H)
call FlushChildHashtable(j8,H)
call RemoveUnit(I8[(i)*(56)+a])
set I8[(i)*(56)+a]=null
if Mf then
set Mf=false
endif
endif
if a<=MultiboardGetColumnCount(Z7)-1 then
set u9=MultiboardGetItem(Z7,x8[i],a)
call MultiboardSetItemValueColor(u9,51,51,51,$80)
call MultiboardReleaseItem(u9)
if Mf then
set Mf=false
endif
endif
set a=a+1
exitwhen Mf
set Mf=true
endloop
set u9=MultiboardGetItem(Z7,x8[i],0)
call MultiboardSetItemStyle(u9,true,false)
call MultiboardReleaseItem(u9)
set z9=null
set e=null
set u9=null
if p8<=1 and O7 then
call Kf()
endif
endfunction
function Of takes unit m,integer i returns nothing
local integer Pf=s8[i]
local integer H=GetHandleId(m)
local integer T=GetUnitTypeId(m)
local integer H9=LoadInteger(j8,T,1096968556)
local real x1=GetRectCenterX(z8[i])
local real y1=GetRectCenterY(z8[i])
local real x2=GetRectCenterX(a8[Pf])
local real y2=GetRectCenterY(a8[Pf])
local real x=GetUnitX(m)
local real y=GetUnitY(m)
local timer Qf=null
local integer Rf=0
call SetUnitX(m,x-x1+x2)
call SetUnitY(m,y-y1+y2)
call SetUnitFacing(m,270)
call SetUnitOwner(m,Player($B),false)
call Ed(m,Pf)
set x=rX(y8[Pf])
set y=GetRectMinY(y8[Pf])
call UnitAddAbility(m,H9)
call SaveInteger(j8,H,1333227088,i)
call SaveInteger(j8,H,1181705552,Pf)
if(T==1865429045)then
call SaveInteger(j8,H,1097623137,$80)
else
call SaveInteger(j8,H,1097623137,$FF)
endif
call SaveReal(j8,H,1131570264,x)
call SaveReal(j8,H,1131570265,y)
call TriggerRegisterUnitEvent(V4,m,EVENT_UNIT_DAMAGED)
call Dd(m)
set Qf=LoadTimerHandle(j8,H,1651861094)
call TimerStart(Qf,.02,true,function Ld)
if(T==1865429041)then
set Qf=LoadTimerHandle(j8,H,1865429041)
call TimerStart(Qf,.02,true,function Xd)
elseif(T==1697656881)then
set Qf=LoadTimerHandle(j8,H,1697656881)
call TimerStart(Qf,.25,true,function ie)
endif
call V9(m)
call IssuePointOrderById(m,$D0012,x,y)
set Qf=null
endfunction
function Sf takes nothing returns nothing
local integer i=0
local integer a=0
local boolean b=true
call TriggerExecute(W4)
loop
exitwhen i>$A
if w8[i]then
set a=0
loop
exitwhen a>FN[i]
call Of(F8[(i)*(50)+a],i)
set a=a+1
endloop
call xe(i,false)
if LoadBoolean(j8,i,1198680420)then
call ye(i,false)
endif
if b and(FN[i]>-1)then
set b=false
endif
endif
set i=i+1
endloop
if b then
set o8=o8-1
call TriggerExecute(e7)
endif
endfunction
function Uf takes nothing returns nothing
local integer a=0
local boolean b
loop
exitwhen a>$A
if w8[a]then
call o9(a,t8[a],true)
call k9(a,F4,5,"You receive |cFFFFFF00"+I2S(t8[a])+"|r gold for surviving this wave.")
call s9(a,-t8[a])
set b=LoadBoolean(j8,a,1198680420)
call FlushChildHashtable(j8,a)
if b then
call SaveBoolean(j8,a,1198680420,true)
call ye(a,true)
endif
call xe(a,true)
endif
set a=a+1
endloop
set o8=o8+1
set m8=90
call MultiboardSetItemsIcon(Z7,"Icons\\PASPreparation.blp")
call MultiboardSetTitleText(Z7,"Wave "+I2S(o8)+" starts in "+I2S(m8)+" seconds")
call TimerStart(D8,1,true,function ve)
endfunction
function Vf takes nothing returns nothing
call TimerStart(D8,1.25,false,function Uf)
endfunction
function Xf takes nothing returns nothing
local unit b=GetOrderedUnit()
local real x0=GetOrderPointX()
local real y0=GetOrderPointY()
call SetUnitX(b,x0)
call SetUnitY(b,y0)
set b=null
endfunction
function Yf takes nothing returns boolean
local integer cd=GetIssuedOrderId()
local integer E9=0
local integer n=0
loop
set E9=n8[n]
exitwhen(E9==cd)or(n>TT)
set n=n+1
endloop
return n<=TT
endfunction
function dg takes nothing returns nothing
local unit u=GetSpellAbilityUnit()
local integer T=GetSpellAbilityId()
local integer r=GetUnitUserData(u)-1
call UnitRemoveAbility(u,T)
call SetUnitUserData(u,r)
if(T==1093677875)then
call UnitAddAbility(u,1093677141)
elseif(T==1093677876)then
call UnitAddAbility(u,1093677366)
elseif(T==1093677877)then
call UnitAddAbility(u,1093677381)
elseif(T==1093677878)then
call UnitAddAbility(u,1093677367)
endif
if(r==0)then
call UnitRemoveAbility(u,1093677875)
call UnitRemoveAbility(u,1093677876)
call UnitRemoveAbility(u,1093677877)
call UnitRemoveAbility(u,1093677878)
endif
set u=null
endfunction
function eg takes nothing returns boolean
local integer T=GetSpellAbilityId()
local boolean b=(T==1093677875)or(T==1093677876)or(T==1093677877)or(T==1093677878)
return b
endfunction
function gg takes nothing returns nothing
local integer i=(GetPlayerId(GetOwningPlayer((GetSpellAbilityUnit()))))
local player p=Player(i)
local integer a=GetSpellAbilityId()
local integer p9=GetPlayerState(p,PLAYER_STATE_RESOURCE_GOLD)
local integer hg=GetPlayerState(p,PLAYER_STATE_RESOURCE_FOOD_USED)+1
local unit m
local integer Kd
local integer ig
local integer T
local integer c=0
loop
set T=LoadInteger(j8,k8[c],1399879534)
exitwhen a==T
set c=c+1
endloop
set T=k8[c]
set c=LoadInteger(j8,T,1131377524)
set Kd=LoadInteger(j8,i,T)+1
set ig=LoadInteger(j8,T,1281977716)
if(hg>40)then
call k9(i,D4,5,"|cffcc3333Error|r: you cannot spawn more minions.")
set p=null
return
endif
if(p9-c>=0)then
set m=CreateUnit(p,T,rX(z8[i]),rY(z8[i]),270)
set FN[i]=FN[i]+1
set F8[(i)*(50)+FN[i]]=m
call SaveInteger(j8,GetHandleId(m),1179535736,FN[i])
call O9(m,100)
call SetUnitInvulnerable(m,true)
call UnitAddAbility(m,1093677368)
call SetUnitMoveSpeed(m,512)
call SaveInteger(j8,i,T,Kd)
call o9(i,-c,false)
set c=LoadInteger(j8,T,1197566318)
call s9(i,c)
if(Kd==ig)then
call SetPlayerAbilityAvailable(p,a,false)
endif
set m=null
else
call k9(i,E4,5,"|cffcc3333Warning|r: not enough gold.")
endif
set p=null
endfunction
function jg takes nothing returns boolean
local integer a=GetSpellAbilityId()
local integer kg
local integer n=0
loop
set kg=LoadInteger(j8,k8[n],1399879534)
exitwhen(kg==a)or(n>MT)
set n=n+1
endloop
return n<=MT
endfunction
function ng takes nothing returns nothing
local unit m=GetSpellAbilityUnit()
local integer T=GetUnitTypeId(m)
local integer H=GetHandleId(m)
local integer c=LoadInteger(j8,T,1131377524)
local integer kg=LoadInteger(j8,T,1399879534)
local integer i=(GetPlayerId(GetOwningPlayer((m))))
local integer a=LoadInteger(j8,H,1179535736)
local integer Kd=LoadInteger(j8,i,T)
local effect e=AddSpecialEffect("Abilities\\Spells\\Other\\Transmute\\PileofGold.mdx",GetUnitX(m),GetUnitY(m))
call DestroyEffect(e)
call FlushChildHashtable(j8,H)
call ne(i,a)
call SaveInteger(j8,i,T,Kd-1)
call SetPlayerAbilityAvailable(Player(i),kg,true)
call o9(i,c,false)
call v9("|cFFFFFF00+"+I2S(c)+"|r",m,$A,64,i,2,1)
set c=LoadInteger(j8,T,1197566318)
call s9(i,-c)
call RemoveUnit(m)
set e=null
set m=null
endfunction
function og takes nothing returns boolean
return GetSpellAbilityId()==1093677368
endfunction
function qg takes nothing returns nothing
local integer i=(GetPlayerId(GetOwningPlayer((GetSpellAbilityUnit()))))
local boolean b=not LoadBoolean(j8,i,1198680420)
call ye(i,b)
call SaveBoolean(j8,i,1198680420,b)
endfunction
function rg takes nothing returns boolean
return GetSpellAbilityId()==1093677113
endfunction
function tg takes nothing returns nothing
local integer i=(GetPlayerId(GetOwningPlayer((GetSpellAbilityUnit()))))
local multiboarditem u9=MultiboardGetItem(Z7,x8[i],0)
call SetPlayerAbilityAvailable(Player(i),1093677110,false)
call SaveBoolean(j8,i,1382375780,true)
call MultiboardSetItemIcon(u9,"Icons\\PASReadiness.blp")
call MultiboardReleaseItem(u9)
set u9=null
call Be()
endfunction
function ug takes nothing returns boolean
return GetSpellAbilityId()==1093677110
endfunction
function wg takes integer i,integer xg returns nothing
local multiboarditem u9=MultiboardGetItem(Z7,x8[i],1)
set v8[i]=v8[i]+xg
if(v8[i]<0)then
set v8[i]=0
endif
call MultiboardSetItemValue(u9,I2S(v8[i]))
call MultiboardReleaseItem(u9)
set u9=null
endfunction
function yg takes nothing returns nothing
local unit m=GetEnteringUnit()
local integer H=GetHandleId(m)
local integer i=LoadInteger(j8,H,1333227088)
local integer f=LoadInteger(j8,H,1181705552)
call DestroyEffect(AddSpecialEffect("Abilities\\Spells\\Undead\\DarkRitual\\DarkRitualTarget.mdx",GetUnitX(m),GetUnitY(m)))
if w8[f]then
call k9(f,H4,3,"|cffcc3333Warning|r: you have lost a life.")
call wg(f,-1)
if v8[f]==0 then
call be(f)
endif
endif
if O7 then
call pe(m)
call RemoveUnit(m)
endif
set m=null
endfunction
function zg takes nothing returns boolean
return GetOwningPlayer(GetEnteringUnit())==Player($B)
endfunction
function ag takes nothing returns nothing
call SetUnitInvulnerable(GetLeavingUnit(),false)
endfunction
function Bg takes nothing returns boolean
return GetOwningPlayer(GetLeavingUnit())==Player($B)
endfunction
function Cg takes nothing returns nothing
local unit m=GetTriggerUnit()
local integer mH=GetHandleId(m)
local integer T=GetUnitTypeId(m)
local integer i=LoadInteger(j8,mH,1333227088)
local string cd=LoadStr(j8,T,1330869362)
local timer cg=LoadTimerHandle(j8,mH,1953063787)
local real Dg=N9(m)
local unit t=GetAttacker()
local integer tH=GetHandleId(t)
local integer rf=LoadInteger(j8,tH,1148020564)
local boolean yd=(rf==1349024115)and(X9(m)!=1)
local boolean zd=(rf==1298229091)and(Z9(m)!=1)
local boolean Ad=(rf==1348825699)
local boolean Eg=(T==1747988529)and(rf==1349024115)
local boolean Fg=(T==1747988534)and(rf==1298229091)
local boolean b=(yd or zd or Ad)and not dd(mH)
if b then
call TimerStart(cg,.5,false,null)
set b=(Eg or Fg)
if b then
set b=IssueImmediateOrder(m,cd)
endif
if not b then
set b=Cd(i,rf,m)
endif
if not b and(Dg<67)then
set b=Cd(i,1214603628,m)
endif
if not b then
set b=Cd(i,1416591218,t)
endif
endif
set m=null
set t=null
set cg=null
endfunction
function Gg takes nothing returns boolean
return TimerGetRemaining(LoadTimerHandle(j8,GetHandleId(GetTriggerUnit()),1953063787))<=.0
endfunction
function Ig takes nothing returns nothing
local unit m=GetSpellAbilityUnit()
local integer mH=GetHandleId(m)
local real x=LoadReal(j8,mH,1131570264)
local real y=LoadReal(j8,mH,1131570265)
call IssuePointOrderById(m,$D0012,x,y)
set m=null
endfunction
function Jg takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(j8,tH,0)
local integer mH=GetHandleId(m)
call RemoveSavedBoolean(j8,mH,1093677146)
call SetUnitAnimation(m,"walk")
set t=null
set m=null
endfunction
function Kg takes nothing returns nothing
local unit m=GetSpellAbilityUnit()
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(j8,mH,1093677146)
local integer tH=GetHandleId(t)
call SaveBoolean(j8,mH,1093677146,true)
call TimerStart(t,2,false,function Jg)
set m=null
set t=null
endfunction
function Lg takes nothing returns boolean
return GetSpellAbilityId()==1093677146
endfunction
function Ng takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(j8,tH,0)
local integer mH=GetHandleId(m)
local effect e=LoadEffectHandle(j8,tH,1)
call RemoveSavedBoolean(j8,mH,1093677648)
call RemoveSavedHandle(j8,tH,1)
call DestroyEffect(e)
set t=null
set m=null
set e=null
endfunction
function Og takes nothing returns nothing
local unit m=GetSpellTargetUnit()
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(j8,mH,1093677648)
local integer tH=GetHandleId(t)
local effect e
if not HaveSavedHandle(j8,tH,1)then
call SaveBoolean(j8,mH,1093677648,true)
set e=AddSpecialEffectTarget("Abilities\\Spells\\Human\\InnerFire\\InnerFireTarget.mdx",m,"overhead")
call SaveEffectHandle(j8,tH,1,e)
set e=null
endif
call TimerStart(t,2,false,function Ng)
set m=null
set t=null
endfunction
function Pg takes nothing returns boolean
return GetSpellAbilityId()==1093677648
endfunction
function Rg takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(j8,tH,0)
local integer mH=GetHandleId(m)
call UnitRemoveAbility(m,1097295983)
set t=null
set m=null
endfunction
function Sg takes nothing returns nothing
local unit m=GetSpellTargetUnit()
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(j8,mH,1093677145)
local integer tH=GetHandleId(t)
if not(GetUnitAbilityLevel((m),(1097295983))>0)then
call UnitAddAbility(m,1097295983)
endif
call TimerStart(t,2,false,function Rg)
set m=null
set t=null
endfunction
function Tg takes nothing returns boolean
return GetSpellAbilityId()==1093677145
endfunction
function Vg takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(j8,tH,0)
local integer mH=GetHandleId(m)
call RemoveSavedBoolean(j8,mH,1093677128)
call SetUnitAnimation(m,"walk")
set t=null
set m=null
endfunction
function Wg takes nothing returns nothing
local unit m=GetSpellAbilityUnit()
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(j8,mH,1093677128)
local integer tH=GetHandleId(t)
call SaveBoolean(j8,mH,1093677128,true)
call TimerStart(t,2,false,function Vg)
set m=null
set t=null
endfunction
function Xg takes nothing returns boolean
return GetSpellAbilityId()==1093677128
endfunction
function Zg takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(j8,tH,0)
local integer mH=GetHandleId(m)
local effect e=LoadEffectHandle(j8,tH,1)
call DestroyEffect(e)
set e=LoadEffectHandle(j8,tH,2)
call DestroyEffect(e)
call RemoveSavedInteger(j8,mH,1093677140)
call RemoveSavedReal(j8,mH,1093677140)
call SetUnitScale(m,.7,.7,.7)
call RemoveSavedHandle(j8,tH,1)
call RemoveSavedHandle(j8,tH,2)
set t=null
set m=null
set e=null
endfunction
function dh takes nothing returns nothing
local unit m=GetSpellTargetUnit()
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(j8,mH,1093677140)
local integer tH=GetHandleId(t)
local effect e
if not HaveSavedHandle(j8,tH,1)then
call SaveInteger(j8,mH,1093677140,30)
call SaveReal(j8,mH,1093677140,25)
call SetUnitScale(m,.9,.9,.9)
set e=AddSpecialEffectTarget("Abilities\\Spells\\Orc\\Bloodlust\\BloodlustSpecial.mdx",m,"hand left")
call SaveEffectHandle(j8,tH,1,e)
set e=AddSpecialEffectTarget("Abilities\\Spells\\Orc\\Bloodlust\\BloodlustSpecial.mdx",m,"hand right")
call SaveEffectHandle(j8,tH,2,e)
set e=null
endif
call TimerStart(t,2,false,function Zg)
set m=null
set t=null
endfunction
function eh takes nothing returns boolean
return GetSpellAbilityId()==1093677140
endfunction
function gh takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit E9=LoadUnitHandle(j8,tH,0)
local integer H=GetHandleId(E9)
local effect e=LoadEffectHandle(j8,tH,1)
call RemoveSavedBoolean(j8,H,1093677656)
call RemoveSavedHandle(j8,H,1093677656)
call PauseUnit(E9,false)
call FlushChildHashtable(j8,tH)
call DestroyEffect(e)
call DestroyTimer(t)
set t=null
set E9=null
set e=null
endfunction
function hh takes nothing returns nothing
local unit m=GetSpellAbilityUnit()
local integer n=GetHandleId(m)
local integer i=LoadInteger(j8,n,1181705552)
local unit E9
local integer T
local timer t
local effect e
local integer tH
local integer H
set n=0
loop
exitwhen n>TN[i]
set E9=I8[(i)*(56)+n]
set T=GetUnitTypeId(E9)
if(j9(m,E9)<=$C0)and(T!=1953462064)then
set H=GetHandleId(E9)
set t=LoadTimerHandle(j8,H,1093677656)
if(t==null)then
set t=CreateTimer()
set tH=GetHandleId(t)
call PauseUnit(E9,true)
call SaveBoolean(j8,H,1093677656,true)
call SaveTimerHandle(j8,H,1093677656,t)
call SaveUnitHandle(j8,tH,0,E9)
set e=AddSpecialEffectTarget("Abilities\\Spells\\Human\\Thunderclap\\ThunderclapTarget.mdx",E9,"overhead")
call SaveEffectHandle(j8,tH,1,e)
endif
call TimerStart(t,2,false,function gh)
endif
set n=n+1
endloop
set m=null
set E9=null
set t=null
set e=null
endfunction
function ih takes nothing returns boolean
return GetSpellAbilityId()==1093677656
endfunction
function kh takes nothing returns nothing
local unit m=GetDyingUnit()
local integer i=(LoadInteger(j8,GetHandleId((m)),1333227088))
local integer a=0
local unit c
local integer T
local effect e
loop
exitwhen a>FN[i]
set c=F8[(i)*(50)+a]
set T=GetUnitTypeId(c)
if(j9(m,c)<=512)and(N9(c)<100)and(T==1966092337 or T==1966092342)then
set e=AddSpecialEffectTarget("Abilities\\Spells\\Undead\\VampiricAura\\VampiricAuraTarget.mdx",c,"origin")
call S9(c,20)
call DestroyEffect(e)
endif
set a=a+1
endloop
set e=null
set m=null
set c=null
endfunction
function mh takes nothing returns boolean
local unit m=GetDyingUnit()
local integer T=GetUnitTypeId(m)
local boolean b=IsUnitType(m,UNIT_TYPE_GROUND)and(T!=1966092336)
set m=null
return b
endfunction
function oh takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(j8,tH,0)
local integer mH=GetHandleId(m)
local effect e=LoadEffectHandle(j8,tH,1)
call RemoveSavedBoolean(j8,mH,1093677658)
call RemoveSavedHandle(j8,tH,1)
call DestroyEffect(e)
set t=null
set m=null
set e=null
endfunction
function ph takes nothing returns nothing
local unit m=GetSpellTargetUnit()
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(j8,mH,1093677658)
local integer tH=GetHandleId(t)
local effect e
if not HaveSavedHandle(j8,tH,1)then
call SaveBoolean(j8,mH,1093677658,true)
set e=AddSpecialEffectTarget("Abilities\\Spells\\Undead\\AntiMagicShell\\AntiMagicShell.mdx",m,"overhead")
call SaveEffectHandle(j8,tH,1,e)
set e=null
endif
call TimerStart(t,2,false,function oh)
set m=null
set t=null
endfunction
function qh takes nothing returns boolean
return GetSpellAbilityId()==1093677658
endfunction
function sh takes real x,real y,real th,integer i,integer Pf returns nothing
local unit m=CreateUnit(Player($B),1966092336,x,y,th)
local integer H=GetHandleId(m)
local timer Qf=CreateTimer()
local integer Rf=GetHandleId(Qf)
local effect e=AddSpecialEffectTarget("Abilities\\Spells\\Undead\\AnimateDead\\AnimateDeadTarget.mdx",m,"origin")
call DestroyEffect(e)
set FN[i]=FN[i]+1
set F8[(i)*(50)+FN[i]]=m
call SaveInteger(j8,H,1179535736,FN[i])
call O9(m,100)
call Ed(m,Pf)
set x=rX(y8[Pf])
set y=GetRectMinY(y8[Pf])
call SaveInteger(j8,H,1333227088,i)
call SaveInteger(j8,H,1181705552,Pf)
call SaveInteger(j8,H,1097623137,$FF)
call TriggerRegisterUnitEvent(V4,m,EVENT_UNIT_DAMAGED)
call SaveUnitHandle(j8,Rf,0,m)
call SaveTimerHandle(j8,H,1651861094,Qf)
call TimerStart(Qf,.02,true,function Ld)
call V9(m)
call IssuePointOrderById(m,$D0012,x,y)
set m=null
set e=null
set Qf=null
endfunction
function uh takes nothing returns nothing
local unit m=GetDyingUnit()
local real x=GetUnitX(m)
local real y=GetUnitY(m)
local real th=GetUnitFacing(m)
local integer H=GetHandleId(m)
local integer vh=LoadInteger(j8,H,1333227088)
local integer Pf=LoadInteger(j8,H,1181705552)
local boolean b=Cd(vh,1147494756,m)
if b then
call sh(x,y,th,vh,Pf)
call s9(vh,1)
endif
set m=null
endfunction
function wh takes nothing returns boolean
local unit m=GetDyingUnit()
local integer T=GetUnitTypeId(m)
local boolean b=IsUnitType(m,UNIT_TYPE_GROUND)and(T!=1966092336)
set m=null
return b
endfunction
function yh takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(j8,tH,0)
local integer mH=GetHandleId(m)
local effect e=LoadEffectHandle(j8,tH,1)
call RemoveSavedBoolean(j8,mH,1093677382)
call RemoveSavedHandle(j8,tH,1)
call DestroyEffect(e)
set t=null
set m=null
set e=null
endfunction
function zh takes nothing returns nothing
local unit m=GetSpellTargetUnit()
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(j8,mH,1093677382)
local integer tH=GetHandleId(t)
local effect e
if not HaveSavedHandle(j8,tH,1)then
call SaveBoolean(j8,mH,1093677382,true)
set e=AddSpecialEffectTarget("Abilities\\Spells\\Undead\\FrostArmor\\FrostArmorTarget.mdx",m,"chest")
call SaveEffectHandle(j8,tH,1,e)
set e=null
endif
call TimerStart(t,2,false,function yh)
set m=null
set t=null
endfunction
function Ah takes nothing returns boolean
return GetSpellAbilityId()==1093677382
endfunction
function Bh takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(j8,tH,0)
local integer mH=GetHandleId(m)
local effect e=LoadEffectHandle(j8,tH,1)
local integer bh=LoadInteger(j8,mH,1093677872)
call S9(m,25)
call SaveInteger(j8,mH,1093677872,bh+1)
if(bh==2)then
call RemoveSavedInteger(j8,mH,1093677872)
call RemoveSavedHandle(j8,tH,1)
call DestroyEffect(e)
call PauseTimer(t)
endif
set t=null
set m=null
set e=null
endfunction
function Ch takes nothing returns nothing
local unit m=GetSpellTargetUnit()
local integer mH=GetHandleId(m)
local integer bh=LoadInteger(j8,mH,1093677872)
local timer t=LoadTimerHandle(j8,mH,1093677872)
local integer tH=GetHandleId(t)
local effect e
if(bh==0)then
set e=AddSpecialEffectTarget("Abilities\\Spells\\NightElf\\Rejuvenation\\RejuvenationTarget.mdx",m,"chest")
call SaveEffectHandle(j8,tH,1,e)
set e=null
call TimerStart(t,1,true,function Bh)
endif
call S9(m,20)
call SaveInteger(j8,mH,1093677872,1)
set m=null
set t=null
endfunction
function ch takes nothing returns boolean
return GetSpellAbilityId()==1093677872
endfunction
function Eh takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(j8,tH,0)
local integer mH=GetHandleId(m)
local effect e=LoadEffectHandle(j8,tH,1)
local integer bh=LoadInteger(j8,mH,1093677143)
call S9(m,$A)
call SaveInteger(j8,mH,1093677143,bh+1)
if(bh==4)then
call RemoveSavedInteger(j8,mH,1093677143)
call RemoveSavedHandle(j8,tH,1)
call DestroyEffect(e)
call PauseTimer(t)
endif
set t=null
set m=null
set e=null
endfunction
function Fh takes nothing returns nothing
local unit c=GetSpellAbilityUnit()
local integer H=GetHandleId(c)
local integer i=LoadInteger(j8,H,1333227088)
local unit m
local timer t
local effect e
local integer tH
local integer bh
local integer n=0
loop
exitwhen n>FN[i]
set m=F8[(i)*(50)+n]
if(j9(c,m)<=512)then
set H=GetHandleId(m)
set bh=LoadInteger(j8,H,1093677143)
set t=LoadTimerHandle(j8,H,1093677143)
set tH=GetHandleId(t)
if(bh==0)then
set e=AddSpecialEffectTarget("Abilities\\Spells\\NightElf\\BattleRoar\\RoarTarget.mdx",m,"overhead")
call SaveEffectHandle(j8,tH,1,e)
call TimerStart(t,1,true,function Eh)
endif
call S9(m,$A)
call SaveInteger(j8,H,1093677143,1)
endif
set n=n+1
endloop
set c=null
set m=null
set t=null
set e=null
endfunction
function Gh takes nothing returns boolean
return GetSpellAbilityId()==1093677143
endfunction
function Ih takes nothing returns nothing
local unit m=GetSpellAbilityUnit()
local integer n=GetHandleId(m)
local integer i=LoadInteger(j8,n,1181705552)
local unit E9
local integer T
set n=0
loop
exitwhen n>TN[i]
set E9=I8[(i)*(56)+n]
set T=GetUnitTypeId(E9)
if(j9(m,E9)<=384)and(T!=1953462064)then
call IssueTargetOrderById(E9,$D000F,m)
endif
set n=n+1
endloop
set m=null
set E9=null
endfunction
function lh takes nothing returns boolean
return GetSpellAbilityId()==1093677649
endfunction
function Kh takes integer i,real x0,real y0 returns boolean
local real Lh=GetRectMinX(B8[i])
local real Mh=GetRectMaxX(B8[i])
local real x=0
local real y=0
local real d=0
local real array Nh
local real array Oh
local real array Ph
local real array Qh
local integer n=-1
local integer a=0
local integer ns=0
local boolean Rh=false
local boolean Sh=false
loop
exitwhen n==TN[i]
set n=n+1
set Nh[n]=GetUnitX(I8[(i)*(56)+n])
set Oh[n]=GetUnitY(I8[(i)*(56)+n])
endloop
set Ph[0]=x0
set Qh[0]=y0
set i=0
loop
exitwhen(i>ns)
set x0=Ph[i]
set y0=Qh[i]
if(x0==Mh)then
set Rh=true
elseif(x0==Lh)then
set Sh=true
endif
exitwhen(Sh and Rh)
set a=0
loop
exitwhen a>n
set x=Nh[a]
set y=Oh[a]
set d=SquareRoot((x0-x)*(x0-x)+(y0-y)*(y0-y))
if(d<$C0)then
set ns=ns+1
set Ph[ns]=x
set Qh[ns]=y
set Nh[a]=Nh[n]
set Oh[a]=Oh[n]
set n=n-1
else
set a=a+1
endif
endloop
set i=i+1
endloop
return Sh and Rh
endfunction
function Th takes nothing returns nothing
local unit t=GetConstructingStructure()
local real x=GetUnitX(t)
local real y=GetUnitY(t)
local integer T=GetUnitTypeId(t)
local integer H=GetHandleId(t)
local integer rf=LoadInteger(j8,T,1148020564)
local integer i=(GetPlayerId(GetOwningPlayer((t))))
local real x1=GetRectMinX(B8[i])
local real x2=GetRectMaxX(B8[i])
local real y1=GetRectMinY(B8[i])
local real y2=GetRectMaxY(B8[i])
if not(x>=x1 and x<=x2 and y>=y1 and y<=y2)then
call k9(i,D4,5,"|cffcc3333Error|r: you cannot build there.")
call te(t,false)
call RemoveUnit(t)
set t=null
return
endif
if Kh(i,x,y)then
call k9(i,D4,5,"|cffcc3333Error|r: this tower will block movement.")
call te(t,false)
call RemoveUnit(t)
set t=null
return
endif
if(T!=1953462064)then
call SaveInteger(j8,H,1148020564,rf)
endif
set TN[i]=TN[i]+1
set I8[(i)*(56)+TN[i]]=t
call SaveInteger(j8,GetHandleId(t),1414416760,TN[i])
call SetUnitFacing(t,270)
set t=null
endfunction
function Vh takes nothing returns nothing
local unit t=GetCancelledStructure()
call te(t,true)
call se(t)
set t=null
endfunction
function Xh takes nothing returns nothing
local unit t=GetSpellAbilityUnit()
call te(t,true)
call se(t)
set t=null
endfunction
function Yh takes nothing returns boolean
return GetSpellAbilityId()==1093677104
endfunction
function di takes nothing returns nothing
local unit E9=GetSpellAbilityUnit()
local integer H=GetHandleId(E9)
local integer i=(GetPlayerId(GetOwningPlayer((E9))))
local integer rf=LoadInteger(j8,H,1148020564)
local integer C9=LoadInteger(j8,H,1165520961)
local integer Gf=LoadInteger(j8,H,1265200236)
local real de=a9(E9)
local string s="Exp.: |cff80ccff"+I2S(C9)+"|r
Kills: |cffff3333"+I2S(Gf)+"|r
Dmg.: |cffff0000"
set s=s+R2SW(de,0,2)+"|r
Type: "
if(rf==1349024115)then
set s=s+"|cffff8700Phys.|r"
elseif(rf==1298229091)then
set s=s+"|cff8700ffMagc.|r"
elseif(rf==1348825699)then
set s=s+"|cffccccccPerc.|r"
endif
call v9(s,E9,8.5,0,i,4.5,4)
set E9=null
endfunction
function ei takes nothing returns boolean
return GetSpellAbilityId()==1093677105
endfunction
function gi takes nothing returns nothing
local unit t=GetSpellAbilityUnit()
local integer a=GetSpellAbilityId()
local integer H=GetHandleId(t)
local integer T=GetUnitTypeId(t)
local integer a1=LoadInteger(j8,T,1096968497)
local integer a2=LoadInteger(j8,T,1096968498)
local effect e=LoadEffectHandle(j8,H,1281717868)
if(a==1093677121)then
call UnitAddAbility(t,1093677125)
call UnitRemoveAbility(t,a1)
elseif(a==1093677122)then
call UnitAddAbility(t,1093677126)
call UnitRemoveAbility(t,a1)
elseif(a==1093677130)then
call UnitAddAbility(t,1093677131)
call UnitRemoveAbility(t,a1)
elseif(a==1093677123)then
call UnitAddAbility(t,1093677127)
call UnitRemoveAbility(t,a2)
elseif(a==1093677133)then
call SaveInteger(j8,H,1131574321,1)
call UnitAddAbility(t,1093677136)
call UnitRemoveAbility(t,a2)
elseif(a==1093677134)then
call SaveInteger(j8,H,1131574321,1)
call UnitAddAbility(t,1093677137)
call UnitRemoveAbility(t,a2)
elseif(a==1093677390)then
call SaveInteger(j8,H,1131574321,1)
call UnitAddAbility(t,1093677389)
call UnitRemoveAbility(t,a1)
elseif(a==1093677392)then
call SaveInteger(j8,H,1131574321,1)
call UnitAddAbility(t,1093677391)
call UnitRemoveAbility(t,a1)
elseif(a==1093677394)then
call SaveInteger(j8,H,1131574321,1)
call UnitAddAbility(t,1093677393)
call UnitRemoveAbility(t,a1)
elseif(a==1093677360)then
call SaveInteger(j8,H,1131574322,1)
call UnitAddAbility(t,1093677363)
call UnitRemoveAbility(t,a2)
elseif(a==1093677364)then
call SaveInteger(j8,H,1131574322,1)
call UnitAddAbility(t,1093677365)
call UnitRemoveAbility(t,a2)
elseif(a==1093677377)then
call SaveInteger(j8,H,1131574322,1)
call UnitAddAbility(t,1093677379)
call UnitRemoveAbility(t,a2)
elseif(a==1093677618)then
call SaveInteger(j8,H,1131574321,1)
call UnitAddAbility(t,1093677619)
call UnitRemoveAbility(t,a1)
elseif(a==1093677620)then
call SaveInteger(j8,H,1131574321,1)
call UnitAddAbility(t,1093677621)
call UnitRemoveAbility(t,a1)
elseif(a==1093677622)then
call SaveInteger(j8,H,1131574321,1)
call UnitAddAbility(t,1093677623)
call UnitRemoveAbility(t,a1)
elseif(a==1093677625)then
call SaveReal(j8,H,1093677633,2)
call UnitAddAbility(t,1093677633)
call UnitRemoveAbility(t,a2)
elseif(a==1093677634)then
call SaveReal(j8,H,1093677635,64)
call UnitAddAbility(t,1093677635)
call UnitRemoveAbility(t,a2)
elseif(a==1093677636)then
call UnitAddAbility(t,1093677637)
call UnitRemoveAbility(t,a2)
endif
if not((GetUnitAbilityLevel((t),(a1))>0)or(GetUnitAbilityLevel((t),(a2))>0))then
call RemoveSavedHandle(j8,H,1281717868)
call DestroyEffect(e)
endif
set t=null
set e=null
endfunction
function hi takes nothing returns boolean
local integer a=GetSpellAbilityId()
local boolean b=(a!=1093677104)and(a!=1093677105)
return b
endfunction
function ji takes nothing returns nothing
local unit t=GetTriggerUnit()
local integer i=(GetPlayerId(GetOwningPlayer((t))))
local integer T=GetUnitTypeId(t)
local integer H=GetHandleId(t)
local integer rf=LoadInteger(j8,T,1148020564)
call SaveInteger(j8,H,1148020564,rf)
call o9(i,1,false)
call v9("|cFFFFFF00+1|r",t,8.5,64,i,2,1)
set t=null
endfunction
function mi takes nothing returns nothing
local quest q=CreateQuest()
local string s="General.
"
set s=s+" - Minor code improvements.
"
set s=s+" - Now game will end in 7 seconds when only one player stays alive.
"
set s=s+" - Fixed issue when \"Toggle Grid\" ability affects grid of other players.
"
set s=s+"Modes.
"
set s=s+" - Lifesteal mode removed.
"
set s=s+"    - When this mode is enabled, game can last for several hours without any hope to end.
"
set s=s+" - Default amount of lives increased from |cffcc808030|r to |cffcc808050|r.
"
set s=s+" - Now players send their minions to random foe or themselves by default.
"
call QuestSetRequired(q,false)
call QuestSetTitle(q,"|cff909090version 2.4.1|r")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNScroll.blp")
set q=null
endfunction
function oi takes nothing returns nothing
local quest q=CreateQuest()
local string s="General.
"
set s=s+" - Performance improvements.
"
set s=s+" - Preparation time increased to 90 seconds.
"
set s=s+"Towers.
"
set s=s+" - Level requirements set to |cff80ccff30|r\\|cff80ccff90|r.
"
set s=s+"Minions.
"
set s=s+" - Maximum number of minions set to 40.
"
set s=s+" - Movement speed of air minions set to |cff0080ff1.5|r cells per second.
"
set s=s+" - Shaman.
"
set s=s+"    - Updated ability.
"
set s=s+" - Troll Batrider.
"
set s=s+"    - Income increment reduced to |cffff800048|r.
"
set s=s+"    - Experience cost increased to |cff80ccff2|r.
"
set s=s+" - Spirit Walker.
"
set s=s+"    - Updated ability.
"
set s=s+" - Ghoul and Abomination.
"
set s=s+"    - Ability healing increased to |cff80ff0020%|r.
"
set s=s+" - Necromancer.
"
set s=s+"    - Ability cooldown increased to |cff00ff805|r seconds.
"
set s=s+" - Druid of the Talon.
"
set s=s+"    - Updated ability.
"
set s=s+" - Faerie Dragon.
"
set s=s+"    - Gold cost and income increment increased to |cffff800048|r.
"
set s=s+"    - Armor changed to |cffff87000|r\\|cff8700ff20|r.
"
set s=s+"    - Experience cost increased to |cff80ccff2|r.
"
set s=s+"Minor inprovements."
call QuestSetRequired(q,false)
call QuestSetTitle(q,"|cff909090version 2.4|r")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNScroll.blp")
set q=null
endfunction
function qi takes nothing returns nothing
local quest q=CreateQuest()
local string s="Towers.
"
set s=s+" - Damage and Kill Damage of all towers were reduced.
"
set s=s+" - Fixed issue when towers did not receive Kill Damage from kills.
"
set s=s+"Minions.
"
set s=s+" - Ghoul and Abomination.
"
set s=s+"    - Changed ability.
"
set s=s+" - Fixed issue when Necromancer did not cast his ability.
"
set s=s+"Minor inprovements."
call QuestSetRequired(q,false)
call QuestSetTitle(q,"|cff909090version 2.3|r")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNScroll.blp")
set q=null
endfunction
function si takes nothing returns nothing
local quest q=CreateQuest()
local string s="General.
"
set s=s+" - Performance improvements.
"
set s=s+" - Fixed issue when player lost gold due to incorrect order.
"
set s=s+"Towers.
"
set s=s+" - Guard Tower.
"
set s=s+"    - Kill Damage increased to |cffff33330.09|r.
"
set s=s+" - Multishot Tower.
"
set s=s+"    - Kill Damage reduced to |cffff33330.025|r.
"
set s=s+" - Watch Tower.
"
set s=s+"    - Kill Damage reduced to |cffff33330.025|r.
"
set s=s+" - Magic Tower.
"
set s=s+"    - Kill Damage increased to |cffff33330.12|r.
"
set s=s+" - Blood Tower.
"
set s=s+"    - Kill Damage increased to |cffff33330.168|r.
"
set s=s+" - Boulder Tower.
"
set s=s+"    - Gold cost reduced to |cffffff008|r.
"
set s=s+"    - Kill Damage increased to |cffff33330.2|r.
"
set s=s+" - Artillery Tower.
"
set s=s+"    - Gold cost reduced to |cffffff006|r.
"
set s=s+"    - Kill Damage increased to |cffff33330.12|r.
"
set s=s+" - Energy Tower.
"
set s=s+"    - Gold cost reduced to |cffffff006|r.
"
set s=s+"    - Kill Damage increased to |cffff33330.08|r.
"
set s=s+" - Fixed issue when towers could be build on unbuildable area.
"
set s=s+"Minions.
"
set s=s+" - Necromancer.
"
set s=s+"    - Income increment from ability reduced to |cffff80001|r.
"
set s=s+"Minor inprovements."
call QuestSetRequired(q,false)
call QuestSetTitle(q,"|cff909090version 2.2|r")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNScroll.blp")
set q=null
endfunction
function ui takes nothing returns nothing
local quest q=CreateQuest()
local string s="General.
"
set s=s+" - Buildable space was reduced.
"
set s=s+" - Kills of minions increases income by |cffff80001|r.
"
set s=s+"Towers.
"
set s=s+" - Cannon class.
"
set s=s+"    - Heavy Impact.
"
set s=s+"       - AoE increment was changed to |cff80ff000.5|r cells.
"
set s=s+"Minions.
"
set s=s+" - Maximum number of minions is reduced to 30.
"
set s=s+" - Income increment of many minions was changed.
"
set s=s+" - Experience cost of many minions was reduced.
"
set s=s+" - Fixed issue when towers-affecting minions did not cast their abilities.
"
set s=s+"Minor inprovements."
call QuestSetRequired(q,false)
call QuestSetTitle(q,"|cff909090version 2.1|r")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNScroll.blp")
set q=null
endfunction
function wi takes nothing returns nothing
local quest q=CreateQuest()
local string s="General.
"
set s=s+" - Changed gold income system.
"
set s=s+"    - Every minion has its own number of income increment.
"
set s=s+"    - Players will not receive extra gold from kills and life absorption.
"
set s=s+" - Hotkeys of all basic and custom orders were updated.
"
set s=s+"Modes.
"
set s=s+" - Lifesteal is enabled as default.
"
set s=s+" - Default number of lives is |cffcc808030|r.
"
set s=s+"Towers.
"
set s=s+" - Level 3 of all towers was reassembled as individual towers.
"
set s=s+"    - New 9 towers.
"
set s=s+"    - Old towers were removed.
"
set s=s+" - Introducing tower classes.
"
set s=s+"    - 3 tower classes.
"
set s=s+"    - Each class has 2 levels and 3 passive abilities on both levels.
"
set s=s+"    - Classes' abilities based on abilities of old Arrow, Arcane and Cannon towers.
"
set s=s+"    - Some abilities were slightly changed.
"
set s=s+" - Fixed issue when level 2 abilities of Arcane tower did not bounce.
"
set s=s+" - Fixed issue when reduction of movement speed became permanent.
"
set s=s+"Minions.
"
set s=s+" - Movement speed of all minions was increased by |cff0080ff0.5|r cells per second.
"
set s=s+" - Troll Berserker and Raider.
"
set s=s+"    - Updated abilities due to overall movement speed increment.
"
set s=s+" - Troll Batrider.
"
set s=s+"    - Ability does not increases movement speed anymore.
"
set s=s+" - Necromancer.
"
set s=s+"    - Ability also increases income of player by |cffff80002|r.
"
set s=s+"Minor inprovements."
call QuestSetRequired(q,false)
call QuestSetTitle(q,"|cff909090version 2.0|r")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNScroll.blp")
set q=null
endfunction
function yi takes nothing returns nothing
local quest q=CreateQuest()
local string s="Unreleased version.

General.
"
set s=s+" - Now if players send minions to themselves, players will get only cost of minion if it absorbed life.
"
set s=s+" - Updated wave start.
"
set s=s+"    - Now after wave ends, new wave will start in 60 seconds.
"
set s=s+"    - Players can start wave earlier, if they all press button 'Readiness' in his builder.
"
set s=s+" - Fixed issue when wave could not start.
"
set s=s+"Minor updates and improvements."
call QuestSetRequired(q,false)
call QuestSetTitle(q,"|cff909090version 1.1.1|r")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNScroll.blp")
set q=null
endfunction
function Ai takes nothing returns nothing
local quest q=CreateQuest()
local string s="General.
"
set s=s+" - Gold from kills increased to |cffffff002|r.
"
set s=s+" - Now players will not get gold from life absorbtion if they send minions to themselves.
"
set s=s+" - Updated wave start.
"
set s=s+"    - Player can prepare for wave until he press button 'Readiness' in his builder.
"
set s=s+"    - When all players are ready, wave starts after some seconds.
"
set s=s+"    - Readiness status added to board.
"
set s=s+"Modes.
"
set s=s+" - New mode Lifesteal.
"
set s=s+"    - When enabled, minions will give absorbed lives to their owner.
"
set s=s+"Towers.
"
set s=s+" - Experience requirements changed to |cff80ccff27|r\\|cff80ccff81|r\\|cff80ccff243|r.
"
set s=s+" - Minimum distance between towers reduced to |cff0080ff0.5|r cells.
"
set s=s+" - Watch Tower.
"
set s=s+"    - Gold cost increased to |cffffff001|r.
"
set s=s+" - Arcane Tower.
"
set s=s+"    - Abilities of level 1 have updated names, icons and effects.
"
set s=s+"    - Abilities of level 2 were fully changed.
"
set s=s+"    - Updated Blood Magic.
"
set s=s+" - Cannon Tower.
"
set s=s+"    - Changed Artillery and Refined Powder.
"
set s=s+" - New towers.
"
set s=s+"    - Arrow Tower I, Arrow Tower III, Arcane Tower I, Arcane Tower III and Cannon Tower I.
"
set s=s+"Minions.
"
set s=s+" - Experience cost of all minions increased by |cff80ccff1|r.
"
set s=s+" - Troll Berserker, Troll Batrider and Faerie Dragon.
"
set s=s+"    - Changed abilities.
"
set s=s+" - Tauren and Mountain Giant.
"
set s=s+"    - Limits increased to |cff0080ff5|r.
"
set s=s+"Minor updates and improvements."
call QuestSetRequired(q,false)
call QuestSetTitle(q,"|cff909090version 1.1|r")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNScroll.blp")
set q=null
endfunction
function Bi takes nothing returns nothing
local quest q=CreateQuest()
local string s="General.
"
set s=s+" - Fixed issue that causes the game to end in 3 seconds.
"
set s=s+" - Fixed issue that causes the game to end if several minions enter finish area of defeated player.
"
set s=s+"Modes.
"
set s=s+" - Default amount of lives decreased from |cffcc808050|r to |cffcc808030|r.
"
set s=s+" - Now players send their minions to random foe by default.
"
call QuestSetRequired(q,false)
call QuestSetTitle(q,"|cff909090version 2.4.2|r")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNScroll.blp")
set q=null
endfunction
function RunInitializationTriggers takes nothing returns nothing
call ConditionalTriggerExecute(l4)
endfunction
function InitCustomPlayerSlots takes nothing returns nothing
call SetPlayerStartLocation(Player(0),0)
call ForcePlayerStartLocation(Player(0),0)
call SetPlayerColor(Player(0),ConvertPlayerColor(0))
call SetPlayerRacePreference(Player(0),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(0),false)
call SetPlayerController(Player(0),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(1),1)
call ForcePlayerStartLocation(Player(1),1)
call SetPlayerColor(Player(1),ConvertPlayerColor(1))
call SetPlayerRacePreference(Player(1),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(1),false)
call SetPlayerController(Player(1),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(2),2)
call ForcePlayerStartLocation(Player(2),2)
call SetPlayerColor(Player(2),ConvertPlayerColor(2))
call SetPlayerRacePreference(Player(2),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(2),false)
call SetPlayerController(Player(2),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(3),3)
call ForcePlayerStartLocation(Player(3),3)
call SetPlayerColor(Player(3),ConvertPlayerColor(3))
call SetPlayerRacePreference(Player(3),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(3),false)
call SetPlayerController(Player(3),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(4),4)
call ForcePlayerStartLocation(Player(4),4)
call SetPlayerColor(Player(4),ConvertPlayerColor(4))
call SetPlayerRacePreference(Player(4),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(4),false)
call SetPlayerController(Player(4),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(5),5)
call ForcePlayerStartLocation(Player(5),5)
call SetPlayerColor(Player(5),ConvertPlayerColor(5))
call SetPlayerRacePreference(Player(5),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(5),false)
call SetPlayerController(Player(5),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(6),6)
call ForcePlayerStartLocation(Player(6),6)
call SetPlayerColor(Player(6),ConvertPlayerColor(6))
call SetPlayerRacePreference(Player(6),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(6),false)
call SetPlayerController(Player(6),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(7),7)
call ForcePlayerStartLocation(Player(7),7)
call SetPlayerColor(Player(7),ConvertPlayerColor(7))
call SetPlayerRacePreference(Player(7),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(7),false)
call SetPlayerController(Player(7),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(8),8)
call ForcePlayerStartLocation(Player(8),8)
call SetPlayerColor(Player(8),ConvertPlayerColor(8))
call SetPlayerRacePreference(Player(8),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(8),false)
call SetPlayerController(Player(8),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(9),9)
call ForcePlayerStartLocation(Player(9),9)
call SetPlayerColor(Player(9),ConvertPlayerColor(9))
call SetPlayerRacePreference(Player(9),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(9),false)
call SetPlayerController(Player(9),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player($A),$A)
call ForcePlayerStartLocation(Player($A),$A)
call SetPlayerColor(Player($A),ConvertPlayerColor($A))
call SetPlayerRacePreference(Player($A),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player($A),false)
call SetPlayerController(Player($A),MAP_CONTROL_USER)
endfunction
function InitCustomTeams takes nothing returns nothing
call SetPlayerTeam(Player(0),0)
call SetPlayerTeam(Player(1),0)
call SetPlayerTeam(Player(2),0)
call SetPlayerTeam(Player(3),0)
call SetPlayerTeam(Player(4),1)
call SetPlayerTeam(Player(5),1)
call SetPlayerTeam(Player(6),1)
call SetPlayerTeam(Player(7),1)
call SetPlayerTeam(Player(8),2)
call SetPlayerTeam(Player(9),2)
call SetPlayerTeam(Player($A),2)
endfunction
function InitAllyPriorities takes nothing returns nothing
call SetStartLocPrioCount(0,1)
call SetStartLocPrio(0,0,1,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(1,2)
call SetStartLocPrio(1,0,0,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(1,1,2,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(2,2)
call SetStartLocPrio(2,0,1,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(2,1,3,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(3,2)
call SetStartLocPrio(3,0,2,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(3,1,4,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(4,2)
call SetStartLocPrio(4,0,3,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(4,1,5,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(5,2)
call SetStartLocPrio(5,0,4,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(5,1,6,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(6,2)
call SetStartLocPrio(6,0,5,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(6,1,7,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(7,2)
call SetStartLocPrio(7,0,6,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(7,1,8,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(8,2)
call SetStartLocPrio(8,0,7,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(8,1,9,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(9,2)
call SetStartLocPrio(9,0,8,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(9,1,$A,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount($A,1)
call SetStartLocPrio($A,0,9,MAP_LOC_PRIO_HIGH)
endfunction
function main takes nothing returns nothing
local weathereffect we
local integer g9
local integer h9
local version v
local integer U8
call SetCameraBounds(-7936.+GetCameraMargin(CAMERA_MARGIN_LEFT),-896.+GetCameraMargin(CAMERA_MARGIN_BOTTOM),8448.-GetCameraMargin(CAMERA_MARGIN_RIGHT),1920.-GetCameraMargin(CAMERA_MARGIN_TOP),-7936.+GetCameraMargin(CAMERA_MARGIN_LEFT),1920.-GetCameraMargin(CAMERA_MARGIN_TOP),8448.-GetCameraMargin(CAMERA_MARGIN_RIGHT),-896.+GetCameraMargin(CAMERA_MARGIN_BOTTOM))
call SetDayNightModels("Environment\\DNC\\DNCLordaeron\\DNCLordaeronTerrain\\DNCLordaeronTerrain.mdl","Environment\\DNC\\DNCLordaeron\\DNCLordaeronUnit\\DNCLordaeronUnit.mdl")
call NewSoundEnvironment("Default")
call SetAmbientDaySound("LordaeronSummerDay")
call SetAmbientNightSound("LordaeronSummerNight")
call SetMapMusic("Music",true,0)
set D4=CreateSound("Sound\\Interface\\Error.wav",false,false,false,$A,$A,"")
call SetSoundParamsFromLabel(D4,"InterfaceError")
call SetSoundDuration(D4,614)
set E4=CreateSound("Sound\\Interface\\Warning\\Human\\KnightNoGold1.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(E4,"NoGoldHuman")
call SetSoundDuration(E4,$5CE)
set F4=CreateSound("Abilities\\Spells\\Items\\ResourceItems\\ReceiveGold.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(F4,"ReceiveGold")
call SetSoundDuration(F4,589)
call SetSoundChannel(F4,8)
call SetSoundVolume(F4,105)
set G4=CreateSound("Sound\\Interface\\BattleNetTick.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(G4,"ChatroomTimerTick")
call SetSoundDuration(G4,476)
call SetSoundVolume(G4,105)
set H4=CreateSound("Sound\\Interface\\Warning.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(H4,"Warning")
call SetSoundDuration(H4,$770)
call SetSoundVolume(H4,105)
set I4=CreateSound("Sound\\Interface\\ItemReceived.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(I4,"ItemReward")
call SetSoundDuration(I4,$5CB)
set h=Rect(-7872.,-448.,-6976.,320.)
set j=Rect(-7936.,640.,-6912.,1152.)
set o=Rect(-7936.,1408.,-6912.,1920.)
set w=Rect(-7936.,-896.,-6912.,-768.)
set z=Rect(-7936.,-896.,-6912.,1152.)
set B=Rect(-6400.,1408.,-5376.,1920.)
set C=Rect(-4864.,1408.,-3840.,1920.)
set D=Rect(-3328.,1408.,-2304.,1920.)
set E=Rect(-1792.,1408.,-768.,1920.)
set F=Rect(-256.,1408.,768.,1920.)
set G=Rect(1280.,1408.,2304.,1920.)
set I=Rect(2816.,1408.,3840.,1920.)
set J=Rect(4352.,1408.,5376.,1920.)
set K=Rect(5888.,1408.,6912.,1920.)
set M=Rect(7424.,1408.,8448.,1920.)
set N=Rect(-6400.,640.,-5376.,1152.)
set O=Rect(-4864.,640.,-3840.,1152.)
set P=Rect(-3328.,640.,-2304.,1152.)
set Q=Rect(-1792.,640.,-768.,1152.)
set R=Rect(-256.,640.,768.,1152.)
set S=Rect(1280.,640.,2304.,1152.)
set U=Rect(2816.,640.,3840.,1152.)
set V=Rect(4352.,640.,5376.,1152.)
set W=Rect(5888.,640.,6912.,1152.)
set X=Rect(7424.,640.,8448.,1152.)
set Y=Rect(-6336.,-448.,-5440.,320.)
set Z=Rect(-4800.,-448.,-3904.,320.)
set d4=Rect(-3264.,-448.,-2368.,320.)
set e4=Rect(-1728.,-448.,-832.,320.)
set f4=Rect(-192.,-448.,704.,320.)
set g4=Rect(1344.,-448.,2240.,320.)
set h4=Rect(2880.,-448.,3776.,320.)
set i4=Rect(4416.,-448.,5312.,320.)
set j4=Rect(5952.,-448.,6848.,320.)
set k4=Rect(7488.,-448.,8384.,320.)
set m4=Rect(-6400.,-896.,-5376.,-768.)
set n4=Rect(-4864.,-896.,-3840.,-768.)
set o4=Rect(-3328.,-896.,-2304.,-768.)
set p4=Rect(-1792.,-896.,-768.,-768.)
set q4=Rect(-256.,-896.,768.,-768.)
set r4=Rect(1280.,-896.,2304.,-768.)
set s4=Rect(2816.,-896.,3840.,-768.)
set t4=Rect(4352.,-896.,5376.,-768.)
set u4=Rect(5888.,-896.,6912.,-768.)
set v4=Rect(7424.,-896.,8448.,-768.)
set w4=Rect(-6400.,-896.,-5376.,1152.)
set x4=Rect(-4864.,-896.,-3840.,1152.)
set y4=Rect(-3328.,-896.,-2304.,1152.)
set z4=Rect(-1792.,-896.,-768.,1152.)
set A4=Rect(-256.,-896.,768.,1152.)
set a4=Rect(1280.,-896.,2304.,1152.)
set B4=Rect(2816.,-896.,3840.,1152.)
set b4=Rect(4352.,-896.,5376.,1152.)
set C4=Rect(5888.,-896.,6912.,1152.)
set c4=Rect(7424.,-896.,8448.,1152.)
call ConfigureNeutralVictim()
set P8=Filter(function e9)
set filterIssueHauntOrderAtLocBJ=Filter(function IssueHauntOrderAtLocBJFilter)
set filterEnumDestructablesInCircleBJ=Filter(function R8)
set filterGetUnitsInRectOfPlayer=Filter(function GetUnitsInRectOfPlayerFilter)
set filterGetUnitsOfTypeIdAll=Filter(function GetUnitsOfTypeIdAllFilter)
set filterGetUnitsOfPlayerAndTypeId=Filter(function GetUnitsOfPlayerAndTypeIdFilter)
set filterMeleeTrainedUnitIsHeroBJ=Filter(function MeleeTrainedUnitIsHeroBJFilter)
set filterLivingPlayerUnitsOfTypeId=Filter(function LivingPlayerUnitsOfTypeIdFilter)
set g9=0
loop
exitwhen g9==16
set bj_FORCE_PLAYER[g9]=CreateForce()
call ForceAddPlayer(bj_FORCE_PLAYER[g9],Player(g9))
set g9=g9+1
endloop
set bj_FORCE_ALL_PLAYERS=CreateForce()
call ForceEnumPlayers(bj_FORCE_ALL_PLAYERS,null)
set bj_cineModePriorSpeed=GetGameSpeed()
set bj_cineModePriorFogSetting=IsFogEnabled()
set bj_cineModePriorMaskSetting=IsFogMaskEnabled()
set g9=0
loop
exitwhen g9>=bj_MAX_QUEUED_TRIGGERS
set bj_queuedExecTriggers[g9]=null
set bj_queuedExecUseConds[g9]=false
set g9=g9+1
endloop
set bj_isSinglePlayer=false
set h9=0
set g9=0
loop
exitwhen g9>=$C
if(GetPlayerController(Player(g9))==MAP_CONTROL_USER and GetPlayerSlotState(Player(g9))==PLAYER_SLOT_STATE_PLAYING)then
set h9=h9+1
endif
set g9=g9+1
endloop
set bj_isSinglePlayer=(h9==1)
set bj_rescueSound=CreateSoundFromLabel("Rescue",false,false,false,$2710,$2710)
set bj_questDiscoveredSound=CreateSoundFromLabel("QuestNew",false,false,false,$2710,$2710)
set bj_questUpdatedSound=CreateSoundFromLabel("QuestUpdate",false,false,false,$2710,$2710)
set bj_questCompletedSound=CreateSoundFromLabel("QuestCompleted",false,false,false,$2710,$2710)
set bj_questFailedSound=CreateSoundFromLabel("QuestFailed",false,false,false,$2710,$2710)
set bj_questHintSound=CreateSoundFromLabel("Hint",false,false,false,$2710,$2710)
set bj_questSecretSound=CreateSoundFromLabel("SecretFound",false,false,false,$2710,$2710)
set bj_questItemAcquiredSound=CreateSoundFromLabel("ItemReward",false,false,false,$2710,$2710)
set bj_questWarningSound=CreateSoundFromLabel("Warning",false,false,false,$2710,$2710)
set bj_victoryDialogSound=CreateSoundFromLabel("QuestCompleted",false,false,false,$2710,$2710)
set bj_defeatDialogSound=CreateSoundFromLabel("QuestFailed",false,false,false,$2710,$2710)
call DelayedSuspendDecayCreate()
set v=VersionGet()
if(v==VERSION_REIGN_OF_CHAOS)then
set bj_MELEE_MAX_TWINKED_HEROES=bj_MELEE_MAX_TWINKED_HEROES_V0
else
set bj_MELEE_MAX_TWINKED_HEROES=bj_MELEE_MAX_TWINKED_HEROES_V1
endif
call InitQueuedTriggers()
call InitRescuableBehaviorBJ()
call InitDNCSounds()
call InitMapRects()
call InitSummonableCaps()
set U8=0
loop
set bj_stockAllowedPermanent[U8]=false
set bj_stockAllowedCharged[U8]=false
set bj_stockAllowedArtifact[U8]=false
set U8=U8+1
exitwhen U8>$A
endloop
call SetAllItemTypeSlots($B)
call SetAllUnitTypeSlots($B)
set bj_stockUpdateTimer=CreateTimer()
call TimerStart(bj_stockUpdateTimer,bj_STOCK_RESTOCK_INITIAL_DELAY,false,function Z8)
set bj_stockItemPurchased=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(bj_stockItemPurchased,Player($F),EVENT_PLAYER_UNIT_SELL_ITEM,null)
call TriggerAddAction(bj_stockItemPurchased,function RemovePurchasedItem)
call DetectGameStarted()
call ExecuteFunc("Ci")
set l4=CreateTrigger()
call TriggerAddAction(l4,function Ce)
set J4=CreateTrigger()
call TriggerAddAction(J4,function Fe)
set K4=CreateTrigger()
call TriggerAddAction(K4,function Ie)
set L4=CreateTrigger()
call TriggerAddAction(L4,function Je)
set M4=CreateTrigger()
call TriggerAddAction(M4,function Le)
set N4=CreateTrigger()
call TriggerAddAction(N4,function Ne)
set O4=CreateTrigger()
call TriggerAddAction(O4,function Pe)
set P4=CreateTrigger()
call TriggerAddAction(P4,function Re)
set Q4=CreateTrigger()
call TriggerAddAction(Q4,function Te)
set R4=CreateTrigger()
call TriggerRegisterTimerEvent(R4,0,false)
call TriggerAddAction(R4,function Xe)
set S4=CreateTrigger()
call TriggerAddAction(S4,function Ze)
set T4=CreateTrigger()
call TriggerRegisterTimerExpireEvent(T4,D8)
call TriggerAddAction(T4,function ff)
set U4=CreateTrigger()
call TriggerAddAction(U4,function kf)
set V4=CreateTrigger()
call TriggerAddCondition(V4,Condition(function zf))
call TriggerAddAction(V4,function wf)
set W4=CreateTrigger()
call TriggerAddAction(W4,function af)
set X4=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(X4,Player($B),EVENT_PLAYER_UNIT_DEATH,null)
call TriggerAddAction(X4,function Df)
set Y4=CreateTrigger()
call TriggerAddAction(Y4,function If)
set Z4=CreateTrigger()
call TriggerAddAction(Z4,function Lf)
set d7=CreateTrigger()
call TriggerAddAction(d7,function Sf)
set e7=CreateTrigger()
call TriggerAddAction(e7,function Vf)
set f7=CreateTrigger()
call TriggerAddCondition(f7,Condition(function Yf))
call TriggerAddAction(f7,function Xf)
set g7=CreateTrigger()
call TriggerAddCondition(g7,Condition(function eg))
call TriggerAddAction(g7,function dg)
set h7=CreateTrigger()
call TriggerAddCondition(h7,Condition(function jg))
call TriggerAddAction(h7,function gg)
set i7=CreateTrigger()
call Ae(i7,EVENT_PLAYER_UNIT_SPELL_EFFECT)
call TriggerAddCondition(i7,Condition(function og))
call TriggerAddAction(i7,function ng)
set j7=CreateTrigger()
call TriggerAddCondition(j7,Condition(function rg))
call TriggerAddAction(j7,function qg)
set k7=CreateTrigger()
call TriggerAddAction(k7,function tg)
call TriggerAddCondition(k7,Condition(function ug))
set m7=CreateTrigger()
call TriggerAddCondition(m7,Condition(function zg))
call TriggerAddAction(m7,function yg)
set n7=CreateTrigger()
call TriggerAddCondition(n7,Condition(function Bg))
call TriggerAddAction(n7,function ag)
set o7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(o7,Player($B),EVENT_PLAYER_UNIT_ATTACKED,null)
call TriggerAddCondition(o7,Condition(function Gg))
call TriggerAddAction(o7,function Cg)
set p7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(p7,Player($B),EVENT_PLAYER_UNIT_SPELL_ENDCAST,null)
call TriggerRegisterPlayerUnitEvent(p7,Player($B),EVENT_PLAYER_UNIT_SPELL_FINISH,null)
call TriggerAddAction(p7,function Ig)
set q7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(q7,Player($B),EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerAddCondition(q7,Condition(function Lg))
call TriggerAddAction(q7,function Kg)
set r7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(r7,Player($B),EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerAddCondition(r7,Condition(function Pg))
call TriggerAddAction(r7,function Og)
set s7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(s7,Player($B),EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerAddCondition(s7,Condition(function Tg))
call TriggerAddAction(s7,function Sg)
set t7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(t7,Player($B),EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerAddCondition(t7,Condition(function Xg))
call TriggerAddAction(t7,function Wg)
set u7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(u7,Player($B),EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerAddCondition(u7,Condition(function eh))
call TriggerAddAction(u7,function dh)
set v7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(v7,Player($B),EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerAddCondition(v7,Condition(function ih))
call TriggerAddAction(v7,function hh)
set w7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(w7,Player($B),EVENT_PLAYER_UNIT_DEATH,null)
call TriggerAddCondition(w7,Condition(function mh))
call TriggerAddAction(w7,function kh)
set x7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(x7,Player($B),EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerAddCondition(x7,Condition(function qh))
call TriggerAddAction(x7,function ph)
set y7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(y7,Player($B),EVENT_PLAYER_UNIT_DEATH,null)
call TriggerAddCondition(y7,Condition(function wh))
call TriggerAddAction(y7,function uh)
set z7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(z7,Player($B),EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerAddCondition(z7,Condition(function Ah))
call TriggerAddAction(z7,function zh)
set A7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(A7,Player($B),EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerAddCondition(A7,Condition(function ch))
call TriggerAddAction(A7,function Ch)
set a7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(a7,Player($B),EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerAddCondition(a7,Condition(function Gh))
call TriggerAddAction(a7,function Fh)
set B7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(B7,Player($B),EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerAddCondition(B7,Condition(function lh))
call TriggerAddAction(B7,function Ih)
set b7=CreateTrigger()
call Ae(b7,EVENT_PLAYER_UNIT_CONSTRUCT_START)
call TriggerAddAction(b7,function Th)
set C7=CreateTrigger()
call Ae(C7,EVENT_PLAYER_UNIT_CONSTRUCT_CANCEL)
call TriggerAddAction(C7,function Vh)
set c7=CreateTrigger()
call Ae(c7,EVENT_PLAYER_UNIT_SPELL_EFFECT)
call TriggerAddCondition(c7,Condition(function Yh))
call TriggerAddAction(c7,function Xh)
set D7=CreateTrigger()
call Ae(D7,EVENT_PLAYER_UNIT_SPELL_EFFECT)
call TriggerAddCondition(D7,Condition(function ei))
call TriggerAddAction(D7,function di)
set E7=CreateTrigger()
call Ae(E7,EVENT_PLAYER_UNIT_SPELL_EFFECT)
call TriggerAddCondition(E7,Condition(function hi))
call TriggerAddAction(E7,function gi)
set F7=CreateTrigger()
call Ae(F7,EVENT_PLAYER_UNIT_UPGRADE_FINISH)
call TriggerAddAction(F7,function ji)
set G7=CreateTrigger()
call TriggerAddAction(G7,function mi)
set H7=CreateTrigger()
call TriggerAddAction(H7,function oi)
set I7=CreateTrigger()
call TriggerAddAction(I7,function qi)
set l7=CreateTrigger()
call TriggerAddAction(l7,function si)
set J7=CreateTrigger()
call TriggerAddAction(J7,function ui)
set K7=CreateTrigger()
call TriggerAddAction(K7,function wi)
set L7=CreateTrigger()
call TriggerAddAction(L7,function yi)
set M7=CreateTrigger()
call TriggerAddAction(M7,function Ai)
set N7=CreateTrigger()
call TriggerAddAction(N7,function Bi)
call ConditionalTriggerExecute(l4)
endfunction
function config takes nothing returns nothing
local player p
call SetMapName("TRIGSTR_001")
call SetMapDescription("TRIGSTR_1020")
call SetPlayers($B)
call SetTeams(3)
set p=Player(0)
call DefineStartLocation(0,.0,1408.)
call SetPlayerStartLocation(p,0)
call SetPlayerColor(p,ConvertPlayerColor(0))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,0)
set p=Player(1)
call DefineStartLocation(1,.0,1408.)
call SetPlayerStartLocation(p,1)
call SetPlayerColor(p,ConvertPlayerColor(1))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,0)
set p=Player(2)
call DefineStartLocation(2,.0,1408.)
call SetPlayerStartLocation(p,2)
call SetPlayerColor(p,ConvertPlayerColor(2))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,0)
set p=Player(3)
call DefineStartLocation(3,.0,1408.)
call SetPlayerStartLocation(p,3)
call SetPlayerColor(p,ConvertPlayerColor(3))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,0)
set p=Player(4)
call DefineStartLocation(4,.0,1408.)
call SetPlayerStartLocation(p,4)
call SetPlayerColor(p,ConvertPlayerColor(4))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(5)
call DefineStartLocation(5,.0,1408.)
call SetPlayerStartLocation(p,5)
call SetPlayerColor(p,ConvertPlayerColor(5))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(6)
call DefineStartLocation(6,.0,1408.)
call SetPlayerStartLocation(p,6)
call SetPlayerColor(p,ConvertPlayerColor(6))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(7)
call DefineStartLocation(7,.0,1408.)
call SetPlayerStartLocation(p,7)
call SetPlayerColor(p,ConvertPlayerColor(7))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(8)
call DefineStartLocation(8,.0,1408.)
call SetPlayerStartLocation(p,8)
call SetPlayerColor(p,ConvertPlayerColor(8))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,2)
set p=Player(9)
call DefineStartLocation(9,.0,1408.)
call SetPlayerStartLocation(p,9)
call SetPlayerColor(p,ConvertPlayerColor(9))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,2)
set p=Player($A)
call DefineStartLocation($A,.0,1408.)
call SetPlayerStartLocation(p,$A)
call SetPlayerColor(p,ConvertPlayerColor($A))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,2)
set p=null
endfunction
function Ci takes nothing returns nothing
endfunction
