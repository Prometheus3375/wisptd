globals
constant integer o=$D00A6
constant integer A=$D00A5
constant integer B=$D00FA
constant integer C=$D0097
constant integer D=$D00FE
constant integer E=$D0057
constant integer F=$D0106
constant integer G=$D0012
constant integer H=$D0004
constant integer I=$D0058
constant integer J=$D0008
constant integer K=$D0005
constant integer L=$D028B
constant real M=GetCameraMargin(CAMERA_MARGIN_LEFT)
constant real N=GetCameraMargin(CAMERA_MARGIN_RIGHT)
constant real O=GetCameraMargin(CAMERA_MARGIN_BOTTOM)
constant real P=GetCameraMargin(CAMERA_MARGIN_TOP)
constant real Q=.023/ 10.
constant real R=.071/ 128.
player S
boolean U=true
constant integer V=$A
player array W
integer X
constant playercolor Y=PLAYER_COLOR_RED
constant unittype Z=UNIT_TYPE_MECHANICAL
constant unittype d4=UNIT_TYPE_GIANT
constant unittype e4=UNIT_TYPE_SAPPER
constant unittype f4=UNIT_TYPE_TAUREN
constant unittype g4=UNIT_TYPE_ANCIENT
boolexpr h4
integer i4
integer array j4
constant real k4=-1472.
constant real m4=-1792.
constant real n4=-1920.
constant real o4=-8448.
constant real p4=m4
constant integer q4=256/ 64
constant integer r4=R2I(1024.)/ 64
constant integer s4=R2I(640.-p4)/ 64
constant integer t4=608-r4*q4
constant integer u4=t4+r4
integer v4=0
constant integer w4=$7D0
constant integer x4=E
constant integer y4=I
integer array z4
boolean array A4
player a4=null
constant integer B4=0+StringLength("Spawn")
constant integer b4=B4
constant integer C4=b4+2
constant integer c4=0+StringLength("Upgrade")
constant integer D4=c4
constant integer E4=D4+2
constant integer F4=E4+1
constant integer G4=F4+1
constant integer H4=G4+1
constant integer I4=-25
constant integer l4=-25
integer J4=0
integer K4=0
constant integer L4=51-1
integer array M4
integer N4
constant real O4=1./ 16.
boolexpr array P4
integer array Q4
constant integer R4=$A
constant integer S4=$B
constant integer T4=$C
constant integer U4=$D
constant integer V4=$E
constant integer W4=$F
constant integer X4=51-1
constant integer Y4=$80
integer array Z4
trigger array d7
trigger array e7
integer array f7
integer array g7
trigger array h7
integer array i7
constant integer j7=$A
integer array k7
constant timer m7=CreateTimer()
constant hashtable n7=InitHashtable()
region o7
region p7
integer q7=-1
integer r7=0
integer s7=1
integer t7=61
constant integer u7=$B
constant trigger v7=CreateTrigger()
constant trigger w7=CreateTrigger()
constant trigger x7=CreateTrigger()
constant trigger y7=CreateTrigger()
constant trigger z7=CreateTrigger()
constant trigger A7=CreateTrigger()
boolexpr a7
constant trigger B7=CreateTrigger()
constant trigger b7=CreateTrigger()
constant integer C7=StringLength("inc")
constant integer c7=C7+1
integer D7
boolexpr E7
constant trigger F7=CreateTrigger()
constant integer G7=StringLength("gold")
constant integer H7=G7+1
constant trigger I7=CreateTrigger()
constant integer l7=StringLength("exp")
constant integer J7=l7+1
constant trigger K7=CreateTrigger()
constant integer L7=StringLength("lives")
constant integer M7=L7+1
constant trigger N7=CreateTrigger()
constant integer O7=StringLength("wave")
constant integer P7=O7+1
constant trigger Q7=CreateTrigger()
integer R7
constant trigger S7=CreateTrigger()
constant trigger T7=CreateTrigger()
constant trigger U7=CreateTrigger()
constant trigger V7=CreateTrigger()
constant trigger W7=CreateTrigger()
constant trigger X7=CreateTrigger()
constant trigger Y7=CreateTrigger()
constant trigger Z7=CreateTrigger()
constant trigger d8=CreateTrigger()
constant trigger e8=CreateTrigger()
constant trigger f8=CreateTrigger()
constant trigger g8=CreateTrigger()
constant trigger h8=CreateTrigger()
constant trigger i8=CreateTrigger()
constant trigger j8=CreateTrigger()
constant trigger k8=CreateTrigger()
constant trigger m8=CreateTrigger()
sound n8
sound o8
sound p8
sound q8
sound r8
sound s8
sound t8
sound u8
constant real v8=-8960.
constant real w8=-1920.
constant real x8=(v8+6912.)/ 2.
constant real y8=(w8+1920.)/ 2.
constant trigger z8=CreateTrigger()
constant trigger A8=CreateTrigger()
multiboard a8
integer B8=0
constant real b8=2.*.0041
constant real C8=14.*.0041
constant real c8=6.*.0041
constant real D8=6.*.0041
constant real E8=6.*.0041
constant real F8=5.*.0041
constant real G8=5.*.0041
constant real H8=4.*.0041
constant real I8=9.*.0041
constant real l8=6.*.0041
multiboarditem J8
constant integer K8=$FF
constant trigger L8=CreateTrigger()
constant trigger M8=CreateTrigger()
integer array N8
integer O8=-1
integer array P8
integer Q8=-1
integer array R8
integer S8
integer T8=0
integer U8=0
integer array V8
integer array W8
integer array X8
integer array Y8
integer Z8=0
integer d9=0
integer array e9
integer array f9
integer array g9
integer array h9
integer i9=0
integer j9=0
integer array k9
integer array m9
integer array n9
integer array o9
integer p9=0
integer q9=0
integer array r9
integer array s9
integer array t9
integer array u9
integer v9=0
integer w9=0
integer array x9
integer array y9
integer array z9
integer array A9
integer a9=0
integer B9=0
integer array b9
integer array C9
integer array c9
integer array D9
integer E9=0
integer F9=0
integer array G9
boolean array H9
integer array I9
integer l9=-1
integer J9
integer K9
integer L9
integer M9
integer N9
integer O9
integer P9
integer Q9
integer R9
integer S9
integer T9
integer U9
integer array V9
integer array W9
integer array X9
string array Y9
integer Z9=0
integer ed=0
integer array fd
real array gd
integer array hd
integer jd=0
integer kd=0
integer array md
integer array nd
integer array od
integer array pd
integer array qd
integer array rd
integer array sd
string array td
integer ud=0
integer vd=0
integer array wd
integer array xd
integer array yd
integer array zd
integer array Ad
integer array ad
boolean array Bd
boolean array bd
integer array Cd
integer array Dd
integer array Ed
integer array Fd
integer array Gd
integer array Hd
integer Id=0
integer ld=0
integer array Jd
integer array Kd
integer array Ld
integer Md=0
integer Nd=0
integer array Od
integer array Pd
integer array Qd
integer array Rd
real array Sd
real array Td
real array Ud
real array Vd
rect array Wd
rect array Xd
rect array Yd
rect array Zd
rect array de
fogmodifier array ee
integer array fe
fogmodifier array ge
integer array he
boolean array ie
boolean array je
integer ke=0
integer me=0
integer array ne
unit array oe
integer array pe
timer array qe
integer array re
integer array se
integer te=0
integer ue=0
integer array ve
integer array we
integer array xe
integer array ye
integer array ze
real array Ae
integer array ae
integer array Be
real array be
integer Ce=0
integer ce=0
integer array De
integer array Ee
integer array Fe
integer array Ge
integer array He
integer array Ie
real array le
integer array Je
integer array Ke
integer array Le
real array Me
integer array Ne
integer array Oe
integer array Pe
boolean array Qe
integer array Re
integer array Se
integer array Te
boolean array Ue
integer array Ve
integer array We
integer array Xe
integer array Ye
integer array Ze
integer array df
integer array ef
integer array ff
integer array gf
integer array hf
integer array jf
integer array kf
boolean array mf
integer array nf
integer array of
integer array pf
boolean array qf
integer array rf
integer array sf
integer array tf
boolean array uf
integer array vf
integer array wf
integer array xf
boolean array yf
integer zf=0
integer Af=0
integer array af
player array Bf
integer array bf
string array Cf
integer array cf
integer array Df
boolean array Ef
boolean array Ff
player array Gf
boolean array Hf
integer array If
integer array lf
integer array Jf
integer array Kf
integer array Lf
integer array Mf
real array Nf
integer array Of
integer array Pf
integer array Qf
integer array Rf
multiboarditem array Sf
multiboarditem array Tf
multiboarditem array Uf
multiboarditem array Vf
multiboarditem array Wf
multiboarditem array Xf
multiboarditem array Yf
image array Zf
integer array dg
boolean array eg
unit array fg
unit array gg
unit array hg
unit array ig
unit array jg
integer array kg
integer array mg
integer array ng
integer array og
integer array pg
integer array qg
integer array rg
integer array sg
integer array tg
integer array ug
integer array vg
integer array wg
integer array xg
integer array yg
integer array zg
integer array Ag
integer array ag
integer array Bg
integer array bg
integer array Cg
integer array cg
integer array Dg
integer array Eg
integer array Fg
integer array Gg
integer array Hg
item array Ig
item array lg
item array Jg
integer array Kg
timer array Lg
integer Mg=0
integer Ng=0
integer array Og
integer Pg
integer Qg
integer array Rg
integer array Sg
integer array Tg
integer array Ug
unit array Vg
integer array Wg
integer array Xg
integer array Yg
boolean array Zg
boolean array dh
real array eh
real array fh
real array gh
integer array hh
real array ih
real array jh
real array kh
item array mh
item array nh
item array oh
item array ph
item array qh
item array rh
real array sh
real array th
real array uh
integer array vh
integer array wh
timer array xh
boolexpr yh
integer zh=0
integer Ah=0
integer array ah
integer array Bh
integer array bh
unit array Ch
integer array ch
integer array Dh
integer array Eh
boolean array Fh
integer array Gh
integer array Hh
timer array Ih
integer lh=0
integer Jh=0
integer array Kh
integer array Lh
unit array Mh
integer array Nh
integer array Oh
integer Ph=0
integer Qh=0
integer array Rh
integer array Sh
integer array Th
real array Uh
integer array Vh
integer array Wh
integer Xh=0
integer Yh=0
integer array Zh
integer array di
integer array ei
boolean array fi
boolean array gi
integer hi=0
integer ii=0
integer array ji
boolean array ki
boolean array mi
integer array ni
integer array oi
real array pi
real array qi
integer array ri
integer si=0
integer ti=0
integer array ui
unit array vi
integer array wi
integer xi=0
integer yi=0
integer array zi
constant real Ai=1./ 64.
constant integer Bi=$CC
constant integer bi=$CC
unit array Ci
image array ci
timer array Di
integer Ei=0
integer Fi=0
integer array Gi
integer Hi
integer array Ii
integer array li
boolean array Ji
timer array Ki
constant integer Li=$A
boolean array Mi
integer array Ni
real array Oi
constant integer Pi=$F
boolexpr Qi
integer array Ri
integer array Si
integer Ti
boolean Ui
constant real Vi=-.15
integer array Wi
integer array Xi
boolexpr Yi
boolean array Zi
real array dj
integer array ej
integer array fj
integer array gj
integer hj
integer ij
constant integer jj=B
real array kj
integer array mj
boolexpr nj
integer oj
integer pj
real qj
real array rj
boolexpr sj
code tj
timer array uj
integer vj
integer wj
real xj
integer yj=0
integer zj=0
integer array Aj
unit aj
integer Bj
real bj
real Cj
integer array cj
unit array Dj
integer array Ej
integer array Fj
boolean array Gj
real array Hj
real array Ij
real array lj
real array Jj
integer array Kj
real array Lj
integer array Mj
integer array Nj
integer array Oj
timer array Pj
image array Qj
integer array Rj
boolean array Sj
integer array Tj
integer array Uj
integer array Vj
integer array Wj
item array Xj
item array Yj
item array Zj
item array dk
integer array ek
integer array fk
code gk
boolexpr hk
integer ik
boolexpr jk
constant integer kk=$A
integer array mk
constant integer nk=$A
constant integer ok=$A
integer pk=0
integer qk=0
integer array rk
integer array sk
boolean array tk
boolean array uk
boolean array vk
real array wk
real array xk
real array yk
integer zk=0
integer Ak=0
integer array ak
integer Bk
integer array bk
integer array Ck
constant integer ck=D
code Dk
item array Ek
real array Fk
real array Gk
real array Hk
real array Ik
integer array lk
timer array Jk
constant real Kk=-.3
constant integer Lk=-25
real array Mk
constant real Nk=-.75
integer array Ok
integer array Pk
unit array Qk
unit array Rk
boolean array Sk
real array Tk
integer array Uk
item array Vk
timer array Wk
integer array Xk
integer array Yk
timer array Zk
constant integer dm=$80
constant integer em=$CC
constant integer fm=$FF
code gm
code hm
integer array im
real array jm
real array km
image array mm
integer array nm
real array om
unit array pm
effect array qm
integer array rm
integer array sm
integer array tm
timer array um
boolexpr vm
integer wm
boolexpr xm
integer ym
integer zm
boolexpr Am
real am
boolean Bm
real array bm
real array Cm
real array cm
code Dm
code Em
real array Fm
real array Gm
real array Hm
timer array Im
timer array lm
constant integer Jm=A
constant integer Km=o
constant integer Lm=$A
constant real Mm=1.-.1
constant real Nm=-.01
integer array Om
real array Pm
real array Qm
boolean array Rm
integer array Sm
item array Tm
integer array Um
timer array Vm
integer array Wm
integer array Xm
integer array Ym
integer array Zm
trigger array dn
integer array en
integer fn=0
integer gn=0
integer array hn
constant integer jn=0+StringLength("AbilityUpgrade")
constant integer kn=jn
constant integer mn=kn+2
constant integer nn=mn+1
constant integer on=nn+1
constant integer pn=on+1
constant integer qn=0+StringLength("HeroAbility")
constant integer rn=qn
constant integer sn=rn+2
integer tn
integer array un
integer array vn
integer array wn
integer array xn
integer array yn
string array zn
integer array An
integer array an
integer array Bn
integer array bn
integer array Cn
integer array cn
integer array Dn
integer array En
integer array Fn
integer array Gn
trigger array Hn
integer array In
integer ln=0
integer Jn=0
integer array Kn
integer array Ln
integer array Mn
integer array Nn
real array On
integer array Pn
boolean array Qn
boolean array Rn
integer array Sn
integer array Tn
real array Un
integer array Vn
integer array Wn
integer array Xn
integer array Yn
integer array Zn
integer array do
integer array eo
integer array fo
real array go
real array ho
real array io
real array jo
real array ko
integer array mo
integer array no
boolean array oo
integer array po
integer qo=0
integer ro=0
integer array so
integer array to
integer array uo
integer array vo
integer array wo
integer array xo
integer array yo
integer array zo
integer array Ao
integer array ao
integer array Bo
trigger array bo
integer array Co
integer co=0
integer Do=0
integer array Eo
integer array Fo
integer array Go
integer array Ho
integer array Io
integer array lo
integer array Jo
string array Ko
real array Lo
integer array Mo
real array No
integer array Oo
boolexpr array Po
real array Qo
integer array Ro
boolean array So
integer array To
integer array Uo
real array Vo
real array Wo
integer array Xo
integer array Yo
boolean array Zo
integer array dp
integer array ep
boolean array fp
integer array gp
boolean array hp
boolean array ip
integer array jp
integer array kp
integer array mp
integer array np
integer array op
integer array pp
trigger array qp
trigger array rp
trigger array sp
trigger array tp
trigger array vp
trigger array wp
trigger array xp
trigger array yp
trigger array zp
trigger array Ap
trigger array ap
trigger array Bp
trigger array bp
trigger array Cp
trigger array cp
trigger array Dp
trigger array Ep
trigger array Fp
trigger array Gp
trigger array Hp
trigger array Ip
trigger array lp
trigger array Jp
trigger array Kp
trigger array Lp
trigger array Mp
trigger array Np
trigger array Op
trigger array Pp
integer array Qp
trigger array Rp
integer array Sp
trigger array Tp
integer array Up
trigger array Vp
integer array Wp
trigger array Xp
integer array Yp
trigger array Zp
integer array dq
trigger array eq
trigger fq
trigger gq
trigger hq
trigger iq
trigger jq
trigger kq
trigger mq
trigger nq
trigger oq
trigger pq
trigger qq
trigger rq
trigger sq
trigger array tq
trigger array uq
trigger array vq
trigger array wq
integer xq
integer yq
integer zq
real Aq
boolean aq
integer Bq
real bq
integer Cq
boolean cq
endglobals
native UnitAlive takes unit id returns boolean
function Yq takes nothing returns integer
local integer Zq=co
if(Zq!=0)then
set co=Eo[Zq]
else
set Do=Do+1
set Zq=Do
endif
if(Zq>$AA9)then
return 0
endif
set Ho[Zq]=(Zq-1)*3
set Eo[Zq]=-1
return Zq
endfunction
function dr takes nothing returns integer
local integer Zq=qo
if(Zq!=0)then
set qo=so[Zq]
else
set ro=ro+1
set Zq=ro
endif
if(Zq>$AA9)then
return 0
endif
set uo[Zq]=(Zq-1)*3
set wo[Zq]=(Zq-1)*3
set yo[Zq]=(Zq-1)*3
set Ao[Zq]=(Zq-1)*3
set Bo[Zq]=(Zq-1)*3
set Co[Zq]=(Zq-1)*3
set so[Zq]=-1
return Zq
endfunction
function er takes nothing returns integer
local integer Zq=ln
if(Zq!=0)then
set ln=Kn[Zq]
else
set Jn=Jn+1
set Zq=Jn
endif
if(Zq>$AA9)then
return 0
endif
set po[Zq]=(Zq-1)*3
set Kn[Zq]=-1
return Zq
endfunction
function fr takes nothing returns integer
local integer Zq=fn
if(Zq!=0)then
set fn=hn[Zq]
else
set gn=gn+1
set Zq=gn
endif
if(Zq>$FFE)then
return 0
endif
set an[Zq]=(Zq-1)*2
set bn[Zq]=(Zq-1)*2
set cn[Zq]=(Zq-1)*2
set En[Zq]=(Zq-1)*2
set Gn[Zq]=(Zq-1)*2
set In[Zq]=(Zq-1)*2
set hn[Zq]=-1
return Zq
endfunction
function gr takes nothing returns integer
local integer Zq=T8
if(Zq!=0)then
set T8=V8[Zq]
else
set U8=U8+1
set Zq=U8
endif
if(Zq>67)then
return 0
endif
set X8[Zq]=(Zq-1)*120
set Y8[Zq]=-1
set V8[Zq]=-1
return Zq
endfunction
function hr takes nothing returns integer
local integer Zq=Z8
if(Zq!=0)then
set Z8=e9[Zq]
else
set d9=d9+1
set Zq=d9
endif
if(Zq>77)then
return 0
endif
set g9[Zq]=(Zq-1)*105
set h9[Zq]=-1
set e9[Zq]=-1
return Zq
endfunction
function jr takes nothing returns integer
local integer Zq=i9
if(Zq!=0)then
set i9=k9[Zq]
else
set j9=j9+1
set Zq=j9
endif
if(Zq>77)then
return 0
endif
set n9[Zq]=(Zq-1)*105
set o9[Zq]=-1
set k9[Zq]=-1
return Zq
endfunction
function kr takes nothing returns integer
local integer Zq=p9
if(Zq!=0)then
set p9=r9[Zq]
else
set q9=q9+1
set Zq=q9
endif
if(Zq>77)then
return 0
endif
set t9[Zq]=(Zq-1)*105
set u9[Zq]=-1
set r9[Zq]=-1
return Zq
endfunction
function mr takes nothing returns integer
local integer Zq=v9
if(Zq!=0)then
set v9=x9[Zq]
else
set w9=w9+1
set Zq=w9
endif
if(Zq>77)then
return 0
endif
set z9[Zq]=(Zq-1)*105
set A9[Zq]=-1
set x9[Zq]=-1
return Zq
endfunction
function nr takes nothing returns integer
local integer Zq=a9
if(Zq!=0)then
set a9=b9[Zq]
else
set B9=B9+1
set Zq=B9
endif
if(Zq>77)then
return 0
endif
set c9[Zq]=(Zq-1)*105
set D9[Zq]=-1
set b9[Zq]=-1
return Zq
endfunction
function pr takes nothing returns integer
local integer Zq=E9
if(Zq!=0)then
set E9=G9[Zq]
else
set F9=F9+1
set Zq=F9
endif
if(Zq>$FFE)then
return 0
endif
set I9[Zq]=(Zq-1)*2
set G9[Zq]=-1
return Zq
endfunction
function qr takes nothing returns integer
local integer Zq=Z9
if(Zq!=0)then
set Z9=fd[Zq]
else
set ed=ed+1
set Zq=ed
endif
if(Zq>$9F)then
return 0
endif
set hd[Zq]=(Zq-1)*51
set fd[Zq]=-1
return Zq
endfunction
function rr takes integer m,integer T returns integer
set xq=m
set yq=T
call TriggerEvaluate(sq)
return Cq
endfunction
function sr takes integer Zq returns nothing
set Bq=Zq
call TriggerEvaluate(Pp[dq[Zq]])
endfunction
function tr takes nothing returns integer
local integer Zq=zk
if(Zq!=0)then
set zk=ak[Zq]
else
set Ak=Ak+1
set Zq=Ak
endif
if(Zq>8190)then
return 0
endif
set dq[Zq]=85
set ak[Zq]=-1
return Zq
endfunction
function ur takes integer Zq returns nothing
if Zq==null then
return
elseif(ak[Zq]!=-1)then
return
endif
set Bq=Zq
call TriggerEvaluate(eq[dq[Zq]])
set ak[Zq]=zk
set zk=Zq
endfunction
function vr takes nothing returns integer
local integer Zq=pk
if(Zq!=0)then
set pk=rk[Zq]
else
set qk=qk+1
set Zq=qk
endif
if(Zq>8190)then
return 0
endif
set rk[Zq]=-1
return Zq
endfunction
function wr takes nothing returns integer
local integer Zq=jd
if(Zq!=0)then
set jd=md[Zq]
else
set kd=kd+1
set Zq=kd
endif
if(Zq>$7FE)then
return 0
endif
set od[Zq]=(Zq-1)*4
set md[Zq]=-1
return Zq
endfunction
function xr takes nothing returns integer
local integer Zq=ud
if(Zq!=0)then
set ud=wd[Zq]
else
set vd=vd+1
set Zq=vd
endif
if(Zq>8190)then
return 0
endif
set Bd[Zq]=true
set wd[Zq]=-1
return Zq
endfunction
function yr takes integer Zq returns nothing
if Zq==null then
return
elseif(wd[Zq]!=-1)then
return
endif
set wd[Zq]=ud
set ud=Zq
endfunction
function zr takes nothing returns integer
local integer Zq=Id
if(Zq!=0)then
set Id=Jd[Zq]
else
set ld=ld+1
set Zq=ld
endif
if(Zq>8190)then
return 0
endif
set Kd[Zq]=0
set Jd[Zq]=-1
return Zq
endfunction
function Ar takes integer Zq returns nothing
if Zq==null then
return
elseif(Jd[Zq]!=-1)then
return
endif
set Jd[Zq]=Id
set Id=Zq
endfunction
function ar takes integer Zq returns nothing
set Bq=Zq
call TriggerExecute(fq)
endfunction
function Br takes integer Zq returns nothing
set Bq=Zq
call TriggerExecute(gq)
endfunction
function br takes integer Zq returns nothing
set Bq=Zq
call TriggerExecute(hq)
endfunction
function Cr takes integer Zq returns nothing
set Bq=Zq
call TriggerExecute(iq)
endfunction
function cr takes integer Zq,integer q returns nothing
set Bq=Zq
set xq=q
call TriggerExecute(jq)
endfunction
function Dr takes nothing returns integer
local integer Zq=Md
if(Zq!=0)then
set Md=Od[Zq]
else
set Nd=Nd+1
set Zq=Nd
endif
if(Zq>$C)then
return 0
endif
set Rd[Zq]=(Zq-1)*608
set fe[Zq]=(Zq-1)*$A
set he[Zq]=(Zq-1)*$A
set Od[Zq]=-1
return Zq
endfunction
function Er takes nothing returns integer
local integer Zq=ke
if(Zq!=0)then
set ke=ne[Zq]
else
set me=me+1
set Zq=me
endif
if(Zq>8190)then
return 0
endif
set pe[Zq]=$F7
set ne[Zq]=-1
return Zq
endfunction
function Fr takes integer Zq returns nothing
if Zq==null then
return
elseif(ne[Zq]!=-1)then
return
endif
set ne[Zq]=ke
set ke=Zq
endfunction
function Gr takes nothing returns integer
local integer Zq=te
if(Zq!=0)then
set te=ve[Zq]
else
set ue=ue+1
set Zq=ue
endif
if(Zq>8190)then
return 0
endif
set ye[Zq]=0
set ze[Zq]=0
set Qp[Zq]=31
set ve[Zq]=-1
return Zq
endfunction
function Hr takes integer Zq returns nothing
if Zq==null then
return
elseif(ve[Zq]!=-1)then
return
endif
set Bq=Zq
call TriggerEvaluate(Rp[Qp[Zq]])
set ve[Zq]=te
set te=Zq
endfunction
function Ir takes nothing returns integer
local integer Zq=Gr()
local integer lr
if(Zq==0)then
return 0
endif
set Qp[Zq]=32
set lr=Zq
set Ae[Zq]=.0
return Zq
endfunction
function Jr takes nothing returns integer
local integer Zq=Gr()
local integer lr
if(Zq==0)then
return 0
endif
set Qp[Zq]=33
set lr=Zq
set ae[Zq]=0
return Zq
endfunction
function Kr takes integer Zq returns real
set Bq=Zq
call TriggerEvaluate(Hp[Yp[Zq]])
return bq
endfunction
function Lr takes integer Zq returns real
set Bq=Zq
call TriggerEvaluate(Ip[Yp[Zq]])
return bq
endfunction
function Mr takes integer Zq returns real
set Bq=Zq
call TriggerEvaluate(lp[Yp[Zq]])
return bq
endfunction
function Nr takes integer Zq returns nothing
set Bq=Zq
call TriggerEvaluate(Jp[Yp[Zq]])
endfunction
function Pr takes integer Zq,integer m returns nothing
set Bq=Zq
set xq=m
call TriggerEvaluate(Kp[Yp[Zq]])
endfunction
function Qr takes integer Zq,integer Rr returns nothing
set Bq=Zq
set xq=Rr
call TriggerExecute(Lp[Yp[Zq]])
endfunction
function Sr takes integer Zq,integer m returns nothing
set Bq=Zq
set xq=m
call TriggerExecute(Mp[Yp[Zq]])
endfunction
function Tr takes integer Zq returns nothing
set Bq=Zq
call TriggerEvaluate(Np[Yp[Zq]])
endfunction
function Ur takes integer Zq returns nothing
set Bq=Zq
call TriggerEvaluate(Op[Yp[Zq]])
endfunction
function Vr takes nothing returns integer
local integer Zq=yj
if(Zq!=0)then
set yj=Aj[Zq]
else
set zj=zj+1
set Zq=zj
endif
if(Zq>8190)then
return 0
endif
set Gj[Zq]=false
set Lj[Zq]=1.
set Rj[Zq]=0
set Tj[Zq]=0
set Uj[Zq]=0
set ek[Zq]=0
set Yp[Zq]=70
set Aj[Zq]=-1
return Zq
endfunction
function Wr takes integer Zq returns nothing
if Zq==null then
return
elseif(Aj[Zq]!=-1)then
return
endif
set Bq=Zq
call TriggerEvaluate(Zp[Yp[Zq]])
set Aj[Zq]=yj
set yj=Zq
endfunction
function Xr takes nothing returns integer
local integer Zq=Gr()
local integer lr
if(Zq==0)then
return 0
endif
set Qp[Zq]=34
set lr=Zq
set Be[Zq]=0
set be[Zq]=.0
return Zq
endfunction
function Yr takes nothing returns integer
local integer Zq=Ce
if(Zq!=0)then
set Ce=De[Zq]
else
set ce=ce+1
set Zq=ce
endif
if(Zq>8190)then
return 0
endif
set Ee[Zq]=0
set Ge[Zq]=0
set Ie[Zq]=0
set le[Zq]=.0
set Le[Zq]=0
set Me[Zq]=.0
set Oe[Zq]=0
set Pe[Zq]=0
set Qe[Zq]=false
set Se[Zq]=0
set Te[Zq]=0
set Ue[Zq]=false
set We[Zq]=0
set Xe[Zq]=0
set Ze[Zq]=0
set df[Zq]=0
set ff[Zq]=0
set gf[Zq]=0
set jf[Zq]=0
set kf[Zq]=0
set mf[Zq]=false
set of[Zq]=0
set pf[Zq]=0
set qf[Zq]=false
set sf[Zq]=0
set tf[Zq]=0
set uf[Zq]=false
set wf[Zq]=0
set xf[Zq]=0
set yf[Zq]=false
set De[Zq]=-1
return Zq
endfunction
function Zr takes integer Zq returns nothing
set Bq=Zq
call TriggerExecute(mq)
endfunction
function ds takes nothing returns integer
local integer Zq=zf
if(Zq!=0)then
set zf=af[Zq]
else
set Af=Af+1
set Zq=Af
endif
if(Zq>67)then
return 0
endif
set dg[Zq]=(Zq-1)*120
set tg[Zq]=(Zq-1)*32
set vg[Zq]=(Zq-1)*32
set xg[Zq]=(Zq-1)*32
set zg[Zq]=(Zq-1)*32
set Bg[Zq]=(Zq-1)*24
set Ef[Zq]=true
set Hf[Zq]=false
set Kf[Zq]=w4
set Lf[Zq]=0
set Mf[Zq]=40
set Nf[Zq]=.0
set Of[Zq]=500
set Pf[Zq]=50
set Qf[Zq]=0
set eg[Zq]=true
set rg[Zq]=0
set Ag[Zq]=0
set bg[Zq]=0
set Cg[Zq]=0
set Kg[Zq]=0
set af[Zq]=-1
return Zq
endfunction
function es takes integer Zq returns nothing
set Bq=Zq
call TriggerEvaluate(cp[Wp[Zq]])
endfunction
function fs takes integer Zq,integer gs returns nothing
set Bq=Zq
set xq=gs
call TriggerEvaluate(Dp[Wp[Zq]])
endfunction
function hs takes integer Zq,integer gs,integer is,integer js returns boolean
set Bq=Zq
set xq=gs
set yq=is
set zq=js
call TriggerEvaluate(Ep[Wp[Zq]])
return cq
endfunction
function ks takes integer Zq,real r returns nothing
set Bq=Zq
set Aq=r
call TriggerEvaluate(rq)
endfunction
function ms takes integer Zq returns nothing
set Bq=Zq
call TriggerEvaluate(Fp[Wp[Zq]])
endfunction
function ns takes integer Zq returns nothing
set Bq=Zq
call TriggerEvaluate(Gp[Wp[Zq]])
endfunction
function os takes nothing returns integer
local integer Zq=Ei
if(Zq!=0)then
set Ei=Gi[Zq]
else
set Fi=Fi+1
set Zq=Fi
endif
if(Zq>8190)then
return 0
endif
set Ji[Zq]=false
set Wp[Zq]=61
set Gi[Zq]=-1
return Zq
endfunction
function ps takes integer Zq returns nothing
if Zq==null then
return
elseif(Gi[Zq]!=-1)then
return
endif
set Bq=Zq
call TriggerEvaluate(Xp[Wp[Zq]])
set Gi[Zq]=Ei
set Ei=Zq
endfunction
function qs takes nothing returns integer
local integer Zq=xi
if(Zq!=0)then
set xi=zi[Zq]
else
set yi=yi+1
set Zq=yi
endif
if(Zq>8190)then
return 0
endif
set zi[Zq]=-1
return Zq
endfunction
function rs takes nothing returns integer
local integer Zq=si
if(Zq!=0)then
set si=ui[Zq]
else
set ti=ti+1
set Zq=ti
endif
if(Zq>8190)then
return 0
endif
set ui[Zq]=-1
return Zq
endfunction
function ss takes nothing returns integer
local integer Zq=hi
if(Zq!=0)then
set hi=ji[Zq]
else
set ii=ii+1
set Zq=ii
endif
if(Zq>8190)then
return 0
endif
set ji[Zq]=-1
return Zq
endfunction
function ts takes integer Zq returns nothing
set Bq=Zq
call TriggerEvaluate(rp[Sp[Zq]])
endfunction
function us takes integer Zq returns real
set Bq=Zq
call TriggerEvaluate(sp[Sp[Zq]])
return bq
endfunction
function vs takes integer Zq returns integer
set Bq=Zq
call TriggerEvaluate(tp[Sp[Zq]])
return Cq
endfunction
function ws takes integer Zq returns integer
set Bq=Zq
call TriggerEvaluate(vp[Sp[Zq]])
return Cq
endfunction
function xs takes integer Zq returns boolean
set Bq=Zq
call TriggerEvaluate(wp[Sp[Zq]])
return cq
endfunction
function ys takes integer Zq returns boolean
set Bq=Zq
call TriggerEvaluate(xp[Sp[Zq]])
return cq
endfunction
function zs takes integer Zq returns boolean
set Bq=Zq
call TriggerEvaluate(yp[Sp[Zq]])
return cq
endfunction
function As takes integer Zq returns boolean
set Bq=Zq
call TriggerEvaluate(zp[Sp[Zq]])
return cq
endfunction
function as takes integer Zq returns boolean
set Bq=Zq
call TriggerEvaluate(Ap[Sp[Zq]])
return cq
endfunction
function Bs takes integer Zq,real bs returns real
set Bq=Zq
set Aq=bs
call TriggerEvaluate(ap[Sp[Zq]])
return bq
endfunction
function Cs takes integer Zq,real bs returns real
set Bq=Zq
set Aq=bs
call TriggerEvaluate(Bp[Sp[Zq]])
return bq
endfunction
function cs takes integer Zq,real bs returns real
set Bq=Zq
set Aq=bs
call TriggerEvaluate(bp[Sp[Zq]])
return bq
endfunction
function Ds takes integer Zq,boolean Es returns nothing
set Bq=Zq
set aq=Es
call TriggerExecute(nq)
endfunction
function Fs takes nothing returns integer
local integer Zq=Mg
if(Zq!=0)then
set Mg=Og[Zq]
else
set Ng=Ng+1
set Zq=Ng
endif
if(Zq>8190)then
return 0
endif
set Yg[Zq]=0
set Zg[Zq]=false
set dh[Zq]=true
set Sp[Zq]=43
set Og[Zq]=-1
return Zq
endfunction
function Gs takes integer Zq returns nothing
if Zq==null then
return
elseif(Og[Zq]!=-1)then
return
endif
set Bq=Zq
call TriggerEvaluate(Tp[Sp[Zq]])
set Og[Zq]=Mg
set Mg=Zq
endfunction
function Hs takes integer Zq returns boolean
set Bq=Zq
call TriggerEvaluate(Cp[Up[Zq]])
return cq
endfunction
function Is takes nothing returns integer
local integer Zq=zh
if(Zq!=0)then
set zh=ah[Zq]
else
set Ah=Ah+1
set Zq=Ah
endif
if(Zq>8190)then
return 0
endif
set Eh[Zq]=0
set Fh[Zq]=false
set Up[Zq]=44
set ah[Zq]=-1
return Zq
endfunction
function ls takes integer Zq returns nothing
if Zq==null then
return
elseif(ah[Zq]!=-1)then
return
endif
set Bq=Zq
call TriggerEvaluate(Vp[Up[Zq]])
set ah[Zq]=zh
set zh=Zq
endfunction
function Js takes nothing returns integer
local integer Zq=Is()
local integer lr
if(Zq==0)then
return 0
endif
set Up[Zq]=54
set lr=Zq
set gi[Zq]=false
return Zq
endfunction
function Ks takes nothing returns integer
local integer Zq=Is()
local integer lr
if(Zq==0)then
return 0
endif
set Up[Zq]=53
set lr=Zq
return Zq
endfunction
function Ls takes nothing returns integer
local integer Zq=Is()
local integer lr
if(Zq==0)then
return 0
endif
set Up[Zq]=52
set lr=Zq
return Zq
endfunction
function Ms takes nothing returns integer
local integer Zq=Is()
local integer lr
if(Zq==0)then
return 0
endif
set Up[Zq]=51
set lr=Zq
set fi[Zq]=false
return Zq
endfunction
function Ns takes nothing returns integer
local integer Zq=Is()
local integer lr
if(Zq==0)then
return 0
endif
set Up[Zq]=50
set lr=Zq
return Zq
endfunction
function Os takes nothing returns integer
local integer Zq=Is()
local integer lr
if(Zq==0)then
return 0
endif
set Up[Zq]=49
set lr=Zq
return Zq
endfunction
function Ps takes nothing returns integer
local integer Zq=Is()
local integer lr
if(Zq==0)then
return 0
endif
set Up[Zq]=48
set lr=Zq
return Zq
endfunction
function Qs takes nothing returns integer
local integer Zq=Xh
if(Zq!=0)then
set Xh=Zh[Zq]
else
set Yh=Yh+1
set Zq=Yh
endif
if(Zq>8190)then
return 0
endif
set Zh[Zq]=-1
return Zq
endfunction
function Rs takes integer Zq returns nothing
if Zq==null then
return
elseif(Zh[Zq]!=-1)then
return
endif
set Zh[Zq]=Xh
set Xh=Zq
endfunction
function Ss takes nothing returns integer
local integer Zq=Ph
if(Zq!=0)then
set Ph=Rh[Zq]
else
set Qh=Qh+1
set Zq=Qh
endif
if(Zq>8190)then
return 0
endif
set Vh[Zq]=0
set Wh[Zq]=0
set Rh[Zq]=-1
return Zq
endfunction
function Ts takes integer Zq returns nothing
if Zq==null then
return
elseif(Rh[Zq]!=-1)then
return
endif
set Rh[Zq]=Ph
set Ph=Zq
endfunction
function Us takes nothing returns integer
local integer Zq=lh
if(Zq!=0)then
set lh=Kh[Zq]
else
set Jh=Jh+1
set Zq=Jh
endif
if(Zq>8190)then
return 0
endif
set Kh[Zq]=-1
return Zq
endfunction
function Vs takes nothing returns integer
local integer Zq=Vr()
local integer lr
if(Zq==0)then
return 0
endif
set Yp[Zq]=105
set lr=Zq
return Zq
endfunction
function Ws takes nothing returns integer
local integer Zq=Is()
local integer lr
if(Zq==0)then
return 0
endif
set Up[Zq]=55
set lr=Zq
return Zq
endfunction
function Xs takes nothing returns integer
local integer Zq=Is()
local integer lr
if(Zq==0)then
return 0
endif
set Up[Zq]=56
set lr=Zq
return Zq
endfunction
function Ys takes nothing returns integer
local integer Zq=Is()
local integer lr
if(Zq==0)then
return 0
endif
set Up[Zq]=57
set lr=Zq
return Zq
endfunction
function Zs takes nothing returns integer
local integer Zq=os()
local integer lr
if(Zq==0)then
return 0
endif
set Wp[Zq]=62
set lr=Zq
return Zq
endfunction
function dt takes nothing returns integer
local integer Zq=os()
local integer lr
if(Zq==0)then
return 0
endif
set Wp[Zq]=63
set lr=Zq
set Mi[Zq]=false
set Ni[Zq]=0
set Oi[Zq]=.0
return Zq
endfunction
function et takes nothing returns integer
local integer Zq=os()
local integer lr
if(Zq==0)then
return 0
endif
set Wp[Zq]=64
set lr=Zq
return Zq
endfunction
function ft takes nothing returns integer
local integer Zq=os()
local integer lr
if(Zq==0)then
return 0
endif
set Wp[Zq]=65
set lr=Zq
return Zq
endfunction
function gt takes nothing returns integer
local integer Zq=os()
local integer lr
if(Zq==0)then
return 0
endif
set Wp[Zq]=66
set lr=Zq
set Zi[Zq]=false
set dj[Zq]=.0
set ej[Zq]=0
set fj[Zq]=0
set gj[Zq]=0
return Zq
endfunction
function ht takes nothing returns integer
local integer Zq=os()
local integer lr
if(Zq==0)then
return 0
endif
set Wp[Zq]=67
set lr=Zq
return Zq
endfunction
function it takes nothing returns integer
local integer Zq=os()
local integer lr
if(Zq==0)then
return 0
endif
set Wp[Zq]=68
set lr=Zq
return Zq
endfunction
function jt takes nothing returns integer
local integer Zq=os()
local integer lr
if(Zq==0)then
return 0
endif
set Wp[Zq]=69
set lr=Zq
return Zq
endfunction
function kt takes nothing returns integer
local integer Zq=Is()
local integer lr
if(Zq==0)then
return 0
endif
set Up[Zq]=71
set lr=Zq
return Zq
endfunction
function mt takes nothing returns integer
local integer Zq=Is()
local integer lr
if(Zq==0)then
return 0
endif
set Up[Zq]=72
set lr=Zq
return Zq
endfunction
function nt takes nothing returns integer
local integer Zq=Is()
local integer lr
if(Zq==0)then
return 0
endif
set Up[Zq]=73
set lr=Zq
return Zq
endfunction
function ot takes nothing returns integer
local integer Zq=Is()
local integer lr
if(Zq==0)then
return 0
endif
set Up[Zq]=74
set lr=Zq
return Zq
endfunction
function pt takes nothing returns integer
local integer Zq=Is()
local integer lr
if(Zq==0)then
return 0
endif
set Up[Zq]=75
set lr=Zq
return Zq
endfunction
function qt takes nothing returns integer
local integer Zq=Is()
local integer lr
if(Zq==0)then
return 0
endif
set Up[Zq]=76
set lr=Zq
return Zq
endfunction
function rt takes nothing returns integer
local integer Zq=Is()
local integer lr
if(Zq==0)then
return 0
endif
set Up[Zq]=77
set lr=Zq
return Zq
endfunction
function tt takes nothing returns integer
local integer Zq=Fs()
local integer lr
if(Zq==0)then
return 0
endif
set Sp[Zq]=78
set lr=Zq
return Zq
endfunction
function ut takes nothing returns integer
local integer Zq=Fs()
local integer lr
if(Zq==0)then
return 0
endif
set Sp[Zq]=79
set lr=Zq
return Zq
endfunction
function vt takes nothing returns integer
local integer Zq=Fs()
local integer lr
if(Zq==0)then
return 0
endif
set Sp[Zq]=80
set lr=Zq
return Zq
endfunction
function wt takes nothing returns integer
local integer Zq=Fs()
local integer lr
if(Zq==0)then
return 0
endif
set Sp[Zq]=81
set lr=Zq
return Zq
endfunction
function xt takes nothing returns integer
local integer Zq=Fs()
local integer lr
if(Zq==0)then
return 0
endif
set Sp[Zq]=82
set lr=Zq
return Zq
endfunction
function yt takes nothing returns integer
local integer Zq=Fs()
local integer lr
if(Zq==0)then
return 0
endif
set Sp[Zq]=83
set lr=Zq
return Zq
endfunction
function zt takes integer m,integer T returns integer
local integer Zq=rr(m,T)
local integer lr
if(Zq==0)then
return 0
endif
set dq[Zq]=86
set lr=Zq
set Fk[Zq]=10.
set Gk[Zq]=.0
set Hk[Zq]=1.
set Ik[Zq]=1.
return Zq
endfunction
function At takes integer m,integer T returns integer
local integer Zq=rr(m,T)
local integer lr
if(Zq==0)then
return 0
endif
set dq[Zq]=87
set lr=Zq
set Mk[Zq]=3.
return Zq
endfunction
function at takes integer m,integer T returns integer
local integer Zq=rr(m,T)
local integer lr
if(Zq==0)then
return 0
endif
set dq[Zq]=88
set lr=Zq
set Ok[Zq]=0
return Zq
endfunction
function Bt takes integer m,integer T returns integer
local integer Zq=rr(m,T)
local integer lr
if(Zq==0)then
return 0
endif
set dq[Zq]=89
set lr=Zq
set Rk[Zq]=null
set Sk[Zq]=false
set Tk[Zq]=.0
return Zq
endfunction
function bt takes integer m,integer T returns integer
local integer Zq=rr(m,T)
local integer lr
if(Zq==0)then
return 0
endif
set dq[Zq]=90
set lr=Zq
set Xk[Zq]=1
return Zq
endfunction
function Ct takes integer m,integer T returns integer
local integer Zq=rr(m,T)
local integer lr
if(Zq==0)then
return 0
endif
set dq[Zq]=91
set lr=Zq
set im[Zq]=2
set jm[Zq]=.04
set km[Zq]=384.
set nm[Zq]=25
set om[Zq]=.1
set sm[Zq]=5
return Zq
endfunction
function ct takes integer m,integer T returns integer
local integer Zq=rr(m,T)
local integer lr
if(Zq==0)then
return 0
endif
set dq[Zq]=92
set lr=Zq
set bm[Zq]=1.
set Cm[Zq]=.0
set cm[Zq]=.0
return Zq
endfunction
function Dt takes integer m,integer T returns integer
local integer Zq=rr(m,T)
local integer lr
if(Zq==0)then
return 0
endif
set dq[Zq]=93
set lr=Zq
set Hm[Zq]=.0
return Zq
endfunction
function Et takes integer m,integer T returns integer
local integer Zq=rr(m,T)
local integer lr
if(Zq==0)then
return 0
endif
set dq[Zq]=94
set lr=Zq
set Om[Zq]=0
set Pm[Zq]=.1
set Qm[Zq]=Mm
set Rm[Zq]=false
set Um[Zq]=0
return Zq
endfunction
function Ft takes nothing returns integer
local integer Zq=Vr()
local integer lr
if(Zq==0)then
return 0
endif
set Yp[Zq]=96
set lr=Zq
return Zq
endfunction
function Gt takes nothing returns integer
local integer Zq=Vr()
local integer lr
if(Zq==0)then
return 0
endif
set Yp[Zq]=97
set lr=Zq
return Zq
endfunction
function Ht takes nothing returns integer
local integer Zq=Vr()
local integer lr
if(Zq==0)then
return 0
endif
set Yp[Zq]=98
set lr=Zq
return Zq
endfunction
function It takes nothing returns integer
local integer Zq=Vr()
local integer lr
if(Zq==0)then
return 0
endif
set Yp[Zq]=99
set lr=Zq
return Zq
endfunction
function lt takes nothing returns integer
local integer Zq=Vr()
local integer lr
if(Zq==0)then
return 0
endif
set Yp[Zq]=100
set lr=Zq
return Zq
endfunction
function Jt takes nothing returns integer
local integer Zq=Vr()
local integer lr
if(Zq==0)then
return 0
endif
set Yp[Zq]=101
set lr=Zq
return Zq
endfunction
function Kt takes nothing returns integer
local integer Zq=Vr()
local integer lr
if(Zq==0)then
return 0
endif
set Yp[Zq]=102
set lr=Zq
return Zq
endfunction
function Lt takes nothing returns integer
local integer Zq=Vr()
local integer lr
if(Zq==0)then
return 0
endif
set Yp[Zq]=103
set lr=Zq
return Zq
endfunction
function Mt takes nothing returns integer
local integer Zq=Vr()
local integer lr
if(Zq==0)then
return 0
endif
set Yp[Zq]=104
set lr=Zq
return Zq
endfunction
function Nt takes integer i,integer a1,integer a2,integer a3 returns nothing
set xq=a1
set yq=a2
set zq=a3
call TriggerExecute(uq[i])
endfunction
function Ot takes string s returns nothing
endfunction
function Pt takes integer z1,integer z2 returns integer
if z1<z2 then
return z1
endif
return z2
endfunction
function Rt takes integer a returns boolean
return a/ 2*2!=a
endfunction
function St takes integer a,integer n returns boolean
return a/ n*n==a
endfunction
function Tt takes integer a,integer n returns boolean
return a/ n*n!=a
endfunction
function Vt takes real r returns integer
local integer i=R2I(r)
if r-i==1. then
return i+1
endif
return i
endfunction
function Wt takes real r returns integer
local integer i=R2I(r)
if r==i then
return i
endif
return i+1
endfunction
function Xt takes boolean b returns integer
if b then
return 1
endif
return 0
endfunction
function Yt takes nothing returns nothing
call PauseUnit(GetFilterUnit(),bj_pauseAllUnitsFlag)
endfunction
function Zt takes string s,unit u,player p,real du,real eu,real fu,real gu returns texttag
set bj_lastCreatedTextTag=CreateTextTag()
call SetTextTagText(bj_lastCreatedTextTag,s,du*Q)
call SetTextTagPosUnit(bj_lastCreatedTextTag,u,.0)
call SetTextTagVelocity(bj_lastCreatedTextTag,.0,eu*R)
call SetTextTagVisibility(bj_lastCreatedTextTag,S==p)
call SetTextTagPermanent(bj_lastCreatedTextTag,false)
call SetTextTagLifespan(bj_lastCreatedTextTag,fu)
call SetTextTagFadepoint(bj_lastCreatedTextTag,gu)
return bj_lastCreatedTextTag
endfunction
function hu takes string iu,real du,real x,real y,integer ju returns image
set bj_lastCreatedImage=CreateImage(iu,du,du,.0,x,y,.0,du/ 2.,du/ 2.,.0,ju)
call SetImageRenderAlways(bj_lastCreatedImage,true)
return bj_lastCreatedImage
endfunction
function ku takes player p,string iu,real du,real x,real y,integer ju returns image
set bj_lastCreatedImage=CreateImage(iu,du,du,.0,x,y,.0,du/ 2.,du/ 2.,.0,ju)
call SetImageRenderAlways(bj_lastCreatedImage,S==p)
return bj_lastCreatedImage
endfunction
function mu takes rect r returns real
return GetRandomReal(GetRectMinX(r),GetRectMaxX(r))
endfunction
function nu takes rect r returns real
return GetRandomReal(GetRectMinY(r),GetRectMaxY(r))
endfunction
function ou takes player p,string s,widget w,string pu returns effect
if S!=p then
set s=""
endif
return AddSpecialEffectTarget(s,w,pu)
endfunction
function EndThread takes nothing returns nothing
call I2R(1/ 0)
endfunction
function qu takes nothing returns nothing
set S=GetLocalPlayer()
endfunction
function GetCustomTimer takes nothing returns integer
return LoadInteger(n7,4,GetHandleId(GetExpiredTimer()))
endfunction
function ru takes timer t returns nothing
call PauseTimer(t)
call RemoveSavedInteger(n7,4,GetHandleId(t))
call DestroyTimer(t)
endfunction
function IsFilterUnitTower takes nothing returns boolean
return(IsUnitType(GetFilterUnit(),Z))!=null
endfunction
function su takes nothing returns boolean
return(IsUnitType(GetFilterUnit(),e4))!=null
endfunction
function tu takes real uu,real x,real y,integer ju,integer vu,integer wu,integer xu,integer yu returns image
call hu("Textures\\RangeIndicator.blp",uu+uu,x,y,ju)
call SetImageColor(bj_lastCreatedImage,vu,wu,xu,yu)
return bj_lastCreatedImage
endfunction
function zu takes player p,real uu,real x,real y,integer ju,integer vu,integer wu,integer xu,integer yu returns image
call ku(p,"Textures\\RangeIndicator.blp",uu+uu,x,y,ju)
call SetImageColor(bj_lastCreatedImage,vu,wu,xu,yu)
return bj_lastCreatedImage
endfunction
function Au takes integer p returns nothing
set R7=p
call TriggerExecute(S7)
endfunction
function au takes integer Bu returns nothing
set U=false
set t7=Bu+1
call TriggerExecute(M8)
endfunction
function bu takes string Cu,string cu returns nothing
if U then
call PreloadGenClear()
call Preload("\" )        "+(Cu)+".                                //")
call PreloadGenEnd("WispTD\\Errors\\"+cu+".txt")
call au(7)
call DisplayTimedTextToPlayer(S,.0,.0,((7.)*1.),("An error occurred. Game will end in "+Y9[P9]+"7|r seconds."))
endif
call I2R(1/ 0)
endfunction
function Du takes nothing returns nothing
set h4=Filter(function su)
endfunction
function Eu takes integer Fu returns integer
set O8=O8+1
set N8[(O8)]=Fu
return O8
endfunction
function Gu takes integer Fu returns nothing
set Q8=Q8+1
set P8[(Q8)]=Fu
set lf[Fu]=Q8
endfunction
function Hu takes integer Fu returns nothing
local integer i=lf[Fu]
if Q8>i then
set P8[(i)]=P8[(Q8)]
set lf[P8[(i)]]=i
endif
set Q8=Q8-1
endfunction
function Iu takes integer Fu returns nothing
local integer i=Jf[Fu]
if S8>i then
set R8[(i)]=R8[(S8)]
set Jf[R8[(i)]]=i
endif
set S8=S8-1
endfunction
function lu takes nothing returns nothing
set S8=-1
loop
set S8=S8+1
set R8[(S8)]=(P8[(S8)])
set Jf[R8[(S8)]]=S8
exitwhen S8==Q8
endloop
endfunction
function Ju takes integer p returns nothing
set Df[p]=(R8[(GetRandomInt(0,S8))])
call Iu(Df[p])
endfunction
function Ku takes integer Zq,integer Fu returns nothing
set Y8[Zq]=Y8[Zq]+1
set W8[X8[Zq]+Y8[Zq]]=Fu
set fk[Fu]=Y8[Zq]
endfunction
function Lu takes integer Zq,integer Fu returns nothing
local integer i=fk[Fu]
if Y8[Zq]>i then
set W8[X8[Zq]+i]=W8[X8[Zq]+Y8[Zq]]
set fk[W8[X8[Zq]+i]]=i
endif
set Y8[Zq]=Y8[Zq]-1
endfunction
function Mu takes integer Zq,integer Fu returns nothing
set h9[Zq]=h9[Zq]+1
if h9[Zq]==105 then
call bu("Error: not enough cells in ForceArray "+I2S(Zq)+". Current amount of cells is "+I2S(105),"ForceArrayError")
endif
set f9[g9[Zq]+h9[Zq]]=Fu
set vh[Fu]=h9[Zq]
endfunction
function Nu takes integer Zq,integer Fu returns nothing
local integer i=vh[Fu]
if h9[Zq]>i then
set f9[g9[Zq]+i]=f9[g9[Zq]+h9[Zq]]
set vh[f9[g9[Zq]+i]]=i
endif
set h9[Zq]=h9[Zq]-1
endfunction
function Ou takes integer Zq,integer Fu returns nothing
set o9[Zq]=o9[Zq]+1
if o9[Zq]==105 then
call bu("Error: not enough cells in SpawnArray "+I2S(Zq)+". Current amount of cells is "+I2S(105),"SpawnArrayError")
endif
set m9[n9[Zq]+o9[Zq]]=Fu
set vh[Fu]=o9[Zq]
endfunction
function Pu takes integer Zq,integer Fu returns nothing
local integer i=vh[Fu]
if o9[Zq]>i then
set m9[n9[Zq]+i]=m9[n9[Zq]+o9[Zq]]
set vh[m9[n9[Zq]+i]]=i
endif
set o9[Zq]=o9[Zq]-1
endfunction
function Qu takes integer Zq,integer Fu returns nothing
set u9[Zq]=u9[Zq]+1
if u9[Zq]==105 then
call bu("Error: not enough cells in MinionArray "+I2S(Zq)+". Current amount of cells is "+I2S(105),"MinionArrayError")
endif
set s9[t9[Zq]+u9[Zq]]=Fu
set vh[Fu]=u9[Zq]
endfunction
function Ru takes integer Zq,integer Fu returns nothing
local integer i=vh[Fu]
if u9[Zq]>i then
set s9[t9[Zq]+i]=s9[t9[Zq]+u9[Zq]]
set vh[s9[t9[Zq]+i]]=i
endif
set u9[Zq]=u9[Zq]-1
endfunction
function Su takes integer Zq,integer Fu returns nothing
set A9[Zq]=A9[Zq]+1
if A9[Zq]==105 then
call bu("Error: not enough cells in DeadArray "+I2S(Zq)+". Current amount of cells is "+I2S(105),"DeadArrayError")
endif
set y9[z9[Zq]+A9[Zq]]=Fu
set vh[Fu]=A9[Zq]
endfunction
function Tu takes integer Zq,integer Fu returns nothing
local integer i=vh[Fu]
if A9[Zq]>i then
set y9[z9[Zq]+i]=y9[z9[Zq]+A9[Zq]]
set vh[y9[z9[Zq]+i]]=i
endif
set A9[Zq]=A9[Zq]-1
endfunction
function Uu takes integer Zq,integer Fu returns nothing
set D9[Zq]=D9[Zq]+1
if D9[Zq]==105 then
call bu("Error: not enough cells in SentMinionArray "+I2S(Zq)+". Current amount of cells is "+I2S(105),"SentMinionArrayError")
endif
set C9[c9[Zq]+D9[Zq]]=Fu
set wh[Fu]=D9[Zq]
endfunction
function Vu takes integer Zq,integer Fu returns nothing
local integer i=wh[Fu]
if D9[Zq]>i then
set C9[c9[Zq]+i]=C9[c9[Zq]+D9[Zq]]
set wh[C9[c9[Zq]+i]]=i
endif
set D9[Zq]=D9[Zq]-1
endfunction
function Wu takes playercolor pc returns integer
local integer i=0
loop
exitwhen ConvertPlayerColor(i)==pc
set i=i+1
endloop
return i
endfunction
function Xu takes nothing returns integer
set l9=l9+1
return l9
endfunction
function Yu takes nothing returns nothing
local integer i=Xu()
set V9[i]=$FF
set W9[i]=3
set X9[i]=3
set Y9[i]="|cffff0303"
set i=Xu()
set V9[i]=0
set W9[i]=66
set X9[i]=$FF
set Y9[i]="|cff0042ff"
set i=Xu()
set V9[i]=28
set W9[i]=$E6
set X9[i]=$B9
set Y9[i]="|cff1ce6b9"
set i=Xu()
set V9[i]=84
set W9[i]=0
set X9[i]=$81
set Y9[i]="|cff540081"
set i=Xu()
set V9[i]=$FF
set W9[i]=$FC
set X9[i]=0
set Y9[i]="|cfffffc00"
set i=Xu()
set V9[i]=$FE
set W9[i]=$8A
set X9[i]=$E
set Y9[i]="|cfffe8a0e"
set i=Xu()
set V9[i]=32
set W9[i]=$C0
set X9[i]=0
set Y9[i]="|cff20c000"
set i=Xu()
set V9[i]=$E5
set W9[i]=91
set X9[i]=$B0
set Y9[i]="|cffe55bb0"
set i=Xu()
set V9[i]=$95
set W9[i]=$96
set X9[i]=$97
set Y9[i]="|cff959697"
set i=Xu()
set V9[i]=$7E
set W9[i]=$BF
set X9[i]=$F1
set Y9[i]="|cff7ebff1"
set i=Xu()
set V9[i]=16
set W9[i]=98
set X9[i]=70
set Y9[i]="|cff106246"
set i=Xu()
set V9[i]=78
set W9[i]=42
set X9[i]=4
set Y9[i]="|cff4e2a04"
set i=Xu()
set V9[i]=$9B
set W9[i]=0
set X9[i]=0
set Y9[i]="|cff9b0000"
set i=Xu()
set V9[i]=0
set W9[i]=0
set X9[i]=$C3
set Y9[i]="|cff0000c3"
set i=Xu()
set V9[i]=0
set W9[i]=$EA
set X9[i]=$FF
set Y9[i]="|cff00eaff"
set i=Xu()
set V9[i]=$BE
set W9[i]=0
set X9[i]=$FE
set Y9[i]="|cffbe00fe"
set i=Xu()
set V9[i]=$EB
set W9[i]=$CD
set X9[i]=$87
set Y9[i]="|cffebcd87"
set i=Xu()
set V9[i]=$F8
set W9[i]=$A4
set X9[i]=$8B
set Y9[i]="|cfff8a48b"
set i=Xu()
set V9[i]=$BF
set W9[i]=$FF
set X9[i]=$80
set Y9[i]="|cffbfff80"
set i=Xu()
set V9[i]=$DC
set W9[i]=$B9
set X9[i]=$EB
set Y9[i]="|cffdcb9eb"
set i=Xu()
set V9[i]=40
set W9[i]=40
set X9[i]=40
set Y9[i]="|cff282828"
set i=Xu()
set V9[i]=$EB
set W9[i]=$F0
set X9[i]=$FF
set Y9[i]="|cffebf0ff"
set i=Xu()
set V9[i]=0
set W9[i]=120
set X9[i]=30
set Y9[i]="|cff00781e"
set i=Xu()
set V9[i]=$A4
set W9[i]=111
set X9[i]=51
set Y9[i]="|cffa46f33"
endfunction
function Zu takes nothing returns nothing
call Yu()
set J9=Xu()
set V9[J9]=$E2
set W9[J9]=$B0
set X9[J9]=7
set Y9[J9]="|cffe2b007"
set K9=Xu()
set V9[K9]=$80
set W9[K9]=$CC
set X9[K9]=$FF
set Y9[K9]="|cff80ccff"
set L9=Xu()
set V9[L9]=$D1
set W9[L9]=91
set X9[L9]=$8F
set Y9[L9]="|cffd15b8f"
set M9=Xu()
set V9[M9]=118
set W9[M9]=$A5
set X9[M9]=$AF
set Y9[M9]="|cff76a5af"
set N9=Xu()
set V9[N9]=0
set W9[N9]=$BB
set X9[N9]=46
set Y9[N9]="|cff00bb2e"
set O9=Xu()
set V9[O9]=$CC
set W9[O9]=33
set X9[O9]=33
set Y9[O9]="|cffcc3333"
set P9=Xu()
set V9[P9]=0
set W9[P9]=$CC
set X9[P9]=$FF
set Y9[P9]="|cff00ccff"
set Q9=Xu()
set V9[Q9]=$FF
set W9[Q9]=$CC
set X9[Q9]=$80
set Y9[Q9]="|cffffcc80"
set R9=Xu()
set V9[R9]=51
set W9[R9]=$CC
set X9[R9]=51
set Y9[R9]="|cff33cc33"
set U9=Xu()
set V9[U9]=$80
set W9[U9]=$80
set X9[U9]=$80
set Y9[U9]="|cff808080"
set S9=O9
set T9=O9
endfunction
function ev takes unit u,integer id returns item
call IncUnitAbilityLevel(u,1093677104)
set bj_lastCreatedItem=UnitAddItemById(u,id)
call SetItemUserData(bj_lastCreatedItem,(GetUnitAbilityLevel((u),1093677104)-2))
return bj_lastCreatedItem
endfunction
function hv takes unit u,item i returns nothing
local integer gv=GetItemUserData(i)
local integer iv=(GetUnitAbilityLevel((u),1093677104)-2)
local integer jv
call RemoveItem(i)
loop
exitwhen gv==iv
set jv=gv
set gv=gv+1
set i=UnitItemInSlot(u,gv)
call SetItemDroppable(i,true)
call UnitDropItemSlot(u,i,jv)
call SetItemUserData(i,jv)
call SetItemDroppable(i,false)
endloop
call DecUnitAbilityLevel(u,1093677104)
endfunction
function kv takes integer T,integer mv,integer nv,integer ov,integer pv,integer qv,integer rv,integer sv,integer tv,string uv returns nothing
set j4[T]=wr()
set nd[od[j4[T]]]=mv
set nd[od[j4[T]]+1]=nv
set nd[od[j4[T]]+2]=ov
set nd[od[j4[T]]+3]=pv
set pd[j4[T]]=k7[qv]
set qd[j4[T]]=rv
set rd[j4[T]]=sv
set sd[j4[T]]=tv
set td[j4[T]]=uv
endfunction
function wv takes integer p returns nothing
local integer a=0
local integer b
local integer xv
local integer yv
local integer zv=Gg[p]
local integer Av=Hg[p]
loop
set xv=nd[od[zv]+a]
set yv=nd[od[Av]+a]
set b=0
loop
call UnitRemoveAbility(hg[p],to[uo[i7[xv]]+b])
call UnitRemoveAbility(hg[p],vo[wo[i7[xv]]+b])
call UnitAddAbility(hg[p],to[uo[i7[yv]]+b])
call UnitAddAbility(hg[p],vo[wo[i7[yv]]+b])
set b=b+1
exitwhen b==3
endloop
set a=a+1
exitwhen a==4
endloop
set Gg[p]=Av
set Hg[p]=zv
endfunction
function av takes integer p returns nothing
local integer a=0
local integer zv=Eg[p]
local integer Av=Fg[p]
loop
call UnitRemoveAbility(gg[p],Mn[g7[nd[od[zv]+a]]])
call UnitAddAbility(gg[p],Mn[g7[nd[od[Av]+a]]])
set a=a+1
exitwhen a==4
endloop
call UnitRemoveAbility(gg[p],qd[zv])
call UnitAddAbility(gg[p],qd[Av])
set Eg[p]=Av
set Fg[p]=zv
endfunction
function Bv takes integer f,integer i returns integer
local integer Zq=xr()
set Ad[Zq]=f
set ad[Zq]=i
set zd[Zq]=s4-i/ r4
set xd[Zq]=zd[Zq]
if i<t4 then
set bd[Zq]=true
else
set bd[Zq]=false
set yd[Zq]=zd[Zq]
endif
return Zq
endfunction
function bv takes integer Zq returns nothing
if ad[Zq]<r4 then
set Cd[Zq]=0
else
set Cd[Zq]=Qd[Rd[Ad[Zq]]+ad[Zq]-r4]
endif
if ad[Zq]+r4>608 then
set Fd[Zq]=0
else
set Fd[Zq]=Qd[Rd[Ad[Zq]]+ad[Zq]+r4]
endif
if St(ad[Zq]+1,r4)then
set Ed[Zq]=0
else
set Ed[Zq]=Qd[Rd[Ad[Zq]]+ad[Zq]+1]
endif
if St(ad[Zq],r4)then
set Dd[Zq]=0
else
set Dd[Zq]=Qd[Rd[Ad[Zq]]+ad[Zq]-1]
endif
endfunction
function Cv takes integer Zq returns boolean
return Zq!=0 and bd[Zq]and Bd[Zq]
endfunction
function cv takes integer Zq returns integer
local integer Dv=Kd[Zq]
set Kd[Zq]=Gd[Dv]
return Dv
endfunction
function Ev takes integer Zq,integer c returns nothing
if(Kd[(Zq)]==0)then
set Kd[Zq]=c
set Ld[Zq]=c
set Gd[c]=0
else
set Gd[Ld[Zq]]=c
set Hd[c]=Ld[Zq]
set Gd[c]=0
set Ld[Zq]=c
endif
endfunction
function Fv takes integer Zq,integer c,integer Gv returns nothing
if Cv(c)then
call Ev(Zq,c)
set bd[(c)]=false
set yd[(c)]=(Gv)
endif
endfunction
function Hv takes integer f returns integer
local integer Zq=zr()
local integer a=u4
loop
set a=a-1
call Ev(Zq,Qd[Rd[f]+a])
exitwhen a<t4
endloop
return Zq
endfunction
function Iv takes integer p returns integer
local integer Zq=Dr()
local real lv=o4+bf[p]*(1024.+512.)
local real Jv=lv+1024.
set Pd[Zq]=p
set Sd[Zq]=Jv
set Td[Zq]=lv
set Ud[Zq]=Jv-64.
set Vd[Zq]=lv+64.
set Wd[Zq]=Rect(lv,1408.,Jv,1920.)
set Xd[Zq]=Rect(lv,640.,Jv,1152.)
set Zd[Zq]=Rect(lv,n4,Jv,m4)
set Yd[Zq]=Rect(Vd[Zq],k4,Ud[Zq],320.)
set de[Zq]=Rect(lv,n4,Jv,1152.)
call RegionAddRect(p7,Xd[Zq])
call RegionAddRect(o7,Zd[Zq])
call Br(Zq)
return Zq
endfunction
function Kv takes integer Zq returns nothing
local integer a=-1
local integer p
loop
set a=a+1
set p=(N8[(a)])
if Ef[p]then
set ee[fe[Zq]+a]=CreateFogModifierRect(Bf[Pd[Zq]],FOG_OF_WAR_VISIBLE,de[If[p]],true,true)
set ge[he[Zq]+a]=CreateFogModifierRect(Bf[Pd[Zq]],FOG_OF_WAR_VISIBLE,Wd[If[p]],true,true)
call FogModifierStart(ee[fe[Zq]+a])
call FogModifierStart(ge[he[Zq]+a])
endif
exitwhen a==O8
endloop
endfunction
function Lv takes integer Zq returns nothing
local integer a=-1
local integer Wq=bf[Pd[Zq]]
local integer f
local integer p
loop
set a=a+1
set p=(N8[(a)])
if Hf[p]then
set f=If[p]
call DestroyFogModifier(ee[fe[f]+Wq])
set ee[fe[f]+Wq]=null
call DestroyFogModifier(ge[he[f]+Wq])
set ge[he[f]+Wq]=null
call SetFogStateRect(Bf[p],FOG_OF_WAR_MASKED,de[Zq],true)
call SetFogStateRect(Bf[p],FOG_OF_WAR_MASKED,Wd[Zq],true)
endif
exitwhen a==O8
endloop
call RegionClearRect(p7,Xd[Zq])
call RegionClearRect(o7,Zd[Zq])
call RemoveRect(de[Zq])
set de[Zq]=null
call RemoveRect(Wd[Zq])
set Wd[Zq]=null
call RemoveRect(Xd[Zq])
set Xd[Zq]=null
call RemoveRect(Zd[Zq])
set Zd[Zq]=null
call RemoveRect(Yd[Zq])
set Yd[Zq]=null
call br(Zq)
endfunction
function Mv takes integer Zq,real x,real y returns integer
local integer ix
local integer iy
set ix=R2I(x-Td[Zq])/ 64
if ix==r4 then
set ix=r4-1
endif
set iy=R2I(640.-y)/ 64
if iy==s4 then
set iy=s4-1
endif
return Qd[Rd[Zq]+iy*r4+ix]
endfunction
function Nv takes integer Zq returns nothing
local integer q=Hv(Zq)
call cr(Zq,q)
if je[Zq]then
call bu("Error: updating ground distaces in field "+I2S(Zq)+" was not finished","BFSError")
endif
call Ar(q)
call Cr(Zq)
endfunction
function Ov takes integer Zq returns nothing
set qe[Zq]=CreateTimer()
call SaveInteger(n7,4,GetHandleId((qe[Zq])),(Zq))
endfunction
function Pv takes integer Zq returns nothing
call ru(qe[Zq])
set qe[Zq]=null
endfunction
function Qv takes nothing returns nothing
local integer Zq=(LoadInteger(n7,4,GetHandleId(GetExpiredTimer())))
set pe[Zq]=pe[Zq]-8
if pe[Zq]>0 then
call SetUnitVertexColor(oe[Zq],$FF,$FF,$FF,pe[Zq])
else
call RemoveUnit(oe[Zq])
set oe[Zq]=null
call Pv(Zq)
call Fr(Zq)
call Ot("MinionDeleter "+I2S(Zq)+" is ended.")
endif
endfunction
function Rv takes unit m returns nothing
local integer Zq=Er()
set oe[Zq]=m
call Ov(Zq)
call SetUnitInvulnerable(oe[Zq],true)
call PauseUnit(oe[Zq],true)
call SetUnitVertexColor(oe[Zq],$FF,$FF,$FF,pe[Zq])
call TimerStart(qe[Zq],.05,true,function Qv)
call Ot("MinionDeleter "+I2S(Zq)+" is started.")
endfunction
function Sv takes integer Zq,integer f,integer Tv returns nothing
set re[Zq]=f
set se[Zq]=Tv
endfunction
function Uv takes integer Zq,integer Vv,integer Wv returns nothing
if Zq==0 then
call bu("Error: MBSNode struct has run out of indexes. Current amount of indexes is 8190. Error detected on field of player "+I2S(ch[Vv]),"MBSNodeError")
endif
set we[Zq]=Vv
set xe[Zq]=Wv
endfunction
function Xv takes integer Zq returns nothing
if ye[Zq]!=0 then
set ze[ye[Zq]]=ze[Zq]
endif
if ze[Zq]!=0 then
set ye[ze[Zq]]=ye[Zq]
endif
endfunction
function Yv takes integer Zq,integer Fu returns integer
local integer Zv=Gr()
if Ge[Zq]==0 then
set Fe[Zq]=Zv
else
set ze[Zv]=Fe[Zq]
set ye[Fe[Zq]]=Zv
set Fe[Zq]=Zv
endif
set Ee[Zq]=Ee[Zq]+1
set Ge[Zq]=Ge[Zq]+1
call Uv(Zv,Fu,Ee[Zq])
return Zv
endfunction
function dw takes integer Zq,integer Zv returns nothing
set Ge[Zq]=Ge[Zq]-1
if Fe[Zq]==Zv then
set Fe[Zq]=ze[Zv]
endif
call Xv(Zv)
call Hr(Zv)
endfunction
function ew takes integer Zq,integer Fu returns integer
local integer Zv=Ir()
if Ie[Zq]==0 then
set He[Zq]=Zv
else
set ze[Zv]=He[Zq]
set ye[He[Zq]]=Zv
set He[Zq]=Zv
endif
set Ee[Zq]=Ee[Zq]+1
set Ie[Zq]=Ie[Zq]+1
call Uv(Zv,Fu,Ee[Zq])
return Zv
endfunction
function fw takes integer Zq,integer Zv returns nothing
set Ie[Zq]=Ie[Zq]-1
if He[Zq]==Zv then
set He[Zq]=ze[Zv]
endif
set le[Zq]=le[Zq]-Ae[Zv]
call Xv(Zv)
call Hr(Zv)
endfunction
function gw takes integer Zq,integer Zv,real hw returns nothing
set le[Zq]=le[Zq]-Ae[Zv]+hw
set Ae[Zv]=hw
endfunction
function iw takes integer Zq,real bs returns real
local integer Zv=He[Zq]
local integer jw
loop
exitwhen Zv==0
set jw=ze[Zv]
set bs=bs-Ae[Zv]
if bs>.0 then
call ls(we[Zv])
else
call gw(Zq,Zv,-bs)
return .0
endif
set Zv=jw
endloop
return bs
endfunction
function kw takes integer Zq,integer Fu returns integer
local integer Zv=Ir()
if Le[Zq]==0 then
set Je[Zq]=Zv
set Ke[Zq]=Zv
else
set ze[Zv]=Je[Zq]
set ye[Je[Zq]]=Zv
set Je[Zq]=Zv
endif
set Ee[Zq]=Ee[Zq]+1
set Le[Zq]=Le[Zq]+1
call Uv(Zv,Fu,Ee[Zq])
return Zv
endfunction
function mw takes integer Zq returns nothing
if Le[Zq]==0 then
set Me[Zq]=.0
elseif Ae[Ke[Zq]]<=.0 then
set Me[Zq]=Ae[Je[Zq]]
elseif Ae[Je[Zq]]>=.0 then
set Me[Zq]=Ae[Ke[Zq]]
else
set Me[Zq]=Ae[Je[Zq]]+Ae[Ke[Zq]]
endif
endfunction
function nw takes integer Zq,integer Zv returns nothing
set Le[Zq]=Le[Zq]-1
if Je[Zq]==Zv then
set Je[Zq]=ze[Zv]
endif
if Ke[Zq]==Zv then
set Ke[Zq]=ye[Zv]
endif
call mw(Zq)
call Xv(Zv)
call Hr(Zv)
endfunction
function ow takes integer Zq,integer Zv returns nothing
local integer n=ze[Zv]
local integer p=ye[Zv]
if n!=0 and Ae[Zv]>Ae[n]then
if p!=0 then
set ze[p]=n
endif
set ye[n]=p
set p=n
set n=ze[n]
set ze[p]=Zv
set ze[Zv]=n
set ye[Zv]=p
if p==Ke[Zq]then
set Ke[Zq]=Zv
endif
if Zv==Je[Zq]then
set Je[Zq]=p
endif
loop
exitwhen n==0
set ye[n]=Zv
exitwhen Ae[Zv]<=Ae[n]
set ze[p]=n
set ye[n]=p
set p=n
set n=ze[n]
set ze[p]=Zv
set ze[Zv]=n
set ye[Zv]=p
if p==Ke[Zq]then
set Ke[Zq]=Zv
endif
endloop
endif
if p!=0 and Ae[Zv]<Ae[p]then
if n!=0 then
set ye[n]=p
endif
set ze[p]=n
set n=p
set p=ye[p]
set ye[n]=Zv
set ye[Zv]=p
set ze[Zv]=n
if n==Je[Zq]then
set Je[Zq]=Zv
endif
if Zv==Ke[Zq]then
set Ke[Zq]=n
endif
loop
exitwhen p==0
set ze[p]=Zv
exitwhen Ae[Zv]>=Ae[p]
set ye[n]=p
set ze[p]=n
set n=p
set p=ye[p]
set ye[n]=Zv
set ye[Zv]=p
set ze[Zv]=n
if n==Je[Zq]then
set Je[Zq]=Zv
endif
endloop
endif
endfunction
function pw takes integer Zq,integer Zv,real hw returns nothing
set Ae[Zv]=hw
call ow(Zq,Zv)
call mw(Zq)
endfunction
function rw takes integer Zq,integer Zv returns nothing
set Oe[Zq]=Oe[Zq]-1
if Ne[Zq]==Zv then
set Ne[Zq]=ze[Zv]
endif
set Pe[Zq]=Pe[Zq]-ae[Zv]
set Qe[Zq]=Pe[Zq]>0
call Xv(Zv)
call Hr(Zv)
endfunction
function tw takes integer Zq,integer Fu returns integer
local integer Zv=Jr()
if Se[Zq]==0 then
set Re[Zq]=Zv
else
set ze[Zv]=Re[Zq]
set ye[Re[Zq]]=Zv
set Re[Zq]=Zv
endif
set Ee[Zq]=Ee[Zq]+1
set Se[Zq]=Se[Zq]+1
call Uv(Zv,Fu,Ee[Zq])
return Zv
endfunction
function uw takes integer Zq,integer Zv returns nothing
set Se[Zq]=Se[Zq]-1
if Re[Zq]==Zv then
set Re[Zq]=ze[Zv]
endif
set Te[Zq]=Te[Zq]-ae[Zv]
set Ue[Zq]=Te[Zq]>0
call Xv(Zv)
call Hr(Zv)
endfunction
function vw takes integer Zq,integer Zv,boolean hw returns nothing
local integer v=Xt(hw)
set Te[Zq]=Te[Zq]-ae[Zv]+v
set Ue[Zq]=Te[Zq]>0
set ae[Zv]=v
endfunction
function zw takes integer Zq,integer Fu returns integer
local integer Zv=Jr()
if Ze[Zq]==0 then
set Ye[Zq]=Zv
else
set ze[Zv]=Ye[Zq]
set ye[Ye[Zq]]=Zv
set Ye[Zq]=Zv
endif
set Ee[Zq]=Ee[Zq]+1
set Ze[Zq]=Ze[Zq]+1
call Uv(Zv,Fu,Ee[Zq])
return Zv
endfunction
function Aw takes integer Zq,integer Zv returns nothing
set Ze[Zq]=Ze[Zq]-1
if Ye[Zq]==Zv then
set Ye[Zq]=ze[Zv]
endif
set df[Zq]=df[Zq]-ae[Zv]
call Xv(Zv)
call Hr(Zv)
endfunction
function aw takes integer Zq,integer Zv,integer hw returns nothing
set df[Zq]=df[Zq]-ae[Zv]+hw
set ae[Zv]=hw
endfunction
function Bw takes integer Zq,integer Fu returns integer
local integer Zv=Jr()
if ff[Zq]==0 then
set ef[Zq]=Zv
else
set ze[Zv]=ef[Zq]
set ye[ef[Zq]]=Zv
set ef[Zq]=Zv
endif
set Ee[Zq]=Ee[Zq]+1
set ff[Zq]=ff[Zq]+1
call Uv(Zv,Fu,Ee[Zq])
return Zv
endfunction
function bw takes integer Zq,integer Zv returns nothing
set ff[Zq]=ff[Zq]-1
if ef[Zq]==Zv then
set ef[Zq]=ze[Zv]
endif
set gf[Zq]=gf[Zq]-ae[Zv]
call Xv(Zv)
call Hr(Zv)
endfunction
function Cw takes integer Zq,integer Zv,integer hw returns nothing
set gf[Zq]=gf[Zq]-ae[Zv]+hw
set ae[Zv]=hw
endfunction
function cw takes integer Zq,integer Fu returns integer
local integer Zv=Xr()
if jf[Zq]==0 then
set hf[Zq]=Zv
else
set ze[Zv]=hf[Zq]
set ye[hf[Zq]]=Zv
set hf[Zq]=Zv
endif
set Ee[Zq]=Ee[Zq]+1
set jf[Zq]=jf[Zq]+1
call Uv(Zv,Fu,Ee[Zq])
return Zv
endfunction
function Dw takes integer Zq,integer Zv returns nothing
set jf[Zq]=jf[Zq]-1
if hf[Zq]==Zv then
set hf[Zq]=ze[Zv]
endif
set kf[Zq]=kf[Zq]-Be[Zv]
set mf[Zq]=kf[Zq]>0
call Xv(Zv)
call Hr(Zv)
endfunction
function Ew takes integer Zq,integer Zv,boolean hw returns nothing
local integer v=Xt(hw)
set kf[Zq]=kf[Zq]-Be[Zv]+v
set mf[Zq]=kf[Zq]>0
set Be[Zv]=v
endfunction
function Iw takes integer Zq,integer Fu returns integer
local integer Zv=Xr()
if sf[Zq]==0 then
set rf[Zq]=Zv
else
set ze[Zv]=rf[Zq]
set ye[rf[Zq]]=Zv
set rf[Zq]=Zv
endif
set Ee[Zq]=Ee[Zq]+1
set sf[Zq]=sf[Zq]+1
call Uv(Zv,Fu,Ee[Zq])
return Zv
endfunction
function lw takes integer Zq,integer Zv returns nothing
set sf[Zq]=sf[Zq]-1
if rf[Zq]==Zv then
set rf[Zq]=ze[Zv]
endif
set tf[Zq]=tf[Zq]-Be[Zv]
set uf[Zq]=tf[Zq]>0
call Xv(Zv)
call Hr(Zv)
endfunction
function Jw takes integer Zq,integer Zv,boolean hw returns nothing
local integer v=Xt(hw)
set tf[Zq]=tf[Zq]-Be[Zv]+v
set uf[Zq]=tf[Zq]>0
set Be[Zv]=v
endfunction
function Kw takes integer Zq,integer Fu returns integer
local integer Zv=Jr()
if wf[Zq]==0 then
set vf[Zq]=Zv
else
set ze[Zv]=vf[Zq]
set ye[vf[Zq]]=Zv
set vf[Zq]=Zv
endif
set Ee[Zq]=Ee[Zq]+1
set wf[Zq]=wf[Zq]+1
call Uv(Zv,Fu,Ee[Zq])
return Zv
endfunction
function Lw takes integer Zq,integer Zv returns nothing
set wf[Zq]=wf[Zq]-1
if vf[Zq]==Zv then
set vf[Zq]=ze[Zv]
endif
set xf[Zq]=xf[Zq]-ae[Zv]
set yf[Zq]=xf[Zq]>0
call Xv(Zv)
call Hr(Zv)
endfunction
function Mw takes integer Zq,integer Zv,boolean hw returns nothing
local integer v=Xt(hw)
set xf[Zq]=xf[Zq]-ae[Zv]+v
set yf[Zq]=xf[Zq]>0
set ae[Zv]=v
endfunction
function Nw takes integer Zq,real bs returns nothing
if jf[Zq]>0 and(sf[Zq]==0 or xe[hf[Zq]]>xe[rf[Zq]])then
set be[hf[Zq]]=be[hf[Zq]]+bs
else
set be[rf[Zq]]=be[rf[Zq]]+bs
endif
endfunction
function Ow takes integer Zq,real bs returns nothing
if of[Zq]>0 and(sf[Zq]==0 or xe[nf[Zq]]>xe[rf[Zq]])then
set be[nf[Zq]]=be[nf[Zq]]+bs
else
set be[rf[Zq]]=be[rf[Zq]]+bs
endif
endfunction
function Pw takes integer Zq,real bs returns nothing
set be[rf[Zq]]=be[rf[Zq]]+bs
endfunction
function Qw takes integer Zq returns nothing
local integer Zv=Je[Zq]
local integer jw
loop
exitwhen Zv==0
set jw=ze[Zv]
if ki[Dh[we[Zv]]]then
call ls(we[Zv])
endif
set Zv=jw
endloop
set Zv=Ne[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
endfunction
function Rw takes integer Zq returns nothing
local integer Zv=Fe[Zq]
local integer jw
loop
exitwhen Zv==0
set jw=ze[Zv]
if ki[Dh[we[Zv]]]then
call ls(we[Zv])
endif
set Zv=jw
endloop
set Zv=Je[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
if ki[Dh[we[Zv]]]then
call ls(we[Zv])
endif
set Zv=jw
endloop
set Zv=Ne[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=Ve[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
if ki[Dh[we[Zv]]]then
call ls(we[Zv])
endif
set Zv=jw
endloop
set Zv=Ye[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
if ki[Dh[we[Zv]]]then
call ls(we[Zv])
endif
set Zv=jw
endloop
set Zv=ef[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
if ki[Dh[we[Zv]]]then
call ls(we[Zv])
endif
set Zv=jw
endloop
endfunction
function Sw takes integer Zq returns nothing
local integer Zv=Fe[Zq]
local integer jw
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=He[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=Je[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=Ne[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=Re[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=Ve[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=Ye[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=ef[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=hf[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=nf[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=rf[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=vf[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
call Ot("MinionBuffStorage "+I2S(Zq)+" is deleted.")
endfunction
function Tw takes integer Zq returns nothing
if Zq==null then
return
elseif(De[Zq]!=-1)then
return
endif
call Sw(Zq)
set De[Zq]=Ce
set Ce=Zq
endfunction
function Uw takes integer T returns integer
local integer Vw=ss()
set ki[Vw]=ki[Q4[T]]
set mi[Vw]=mi[Q4[T]]
set ni[Vw]=ni[Q4[T]]
set oi[Vw]=oi[Q4[T]]
set pi[Vw]=pi[Q4[T]]
set qi[Vw]=qi[Q4[T]]
set ri[Vw]=ri[Q4[T]]
return Vw
endfunction
function Ww takes integer T returns integer
local integer Vw=vr()
set sk[Vw]=sk[Z4[T]]
set tk[Vw]=tk[Z4[T]]
set uk[Vw]=uk[Z4[T]]
set vk[Vw]=vk[Z4[T]]
set wk[Vw]=wk[Z4[T]]
set xk[Vw]=xk[Z4[T]]
set yk[Vw]=yk[Z4[T]]
return Vw
endfunction
function Xw takes integer T returns integer
local integer Vw=er()
set Ln[Vw]=Ln[g7[T]]
set Mn[Vw]=Mn[g7[T]]
set Nn[Vw]=Nn[g7[T]]
set On[Vw]=On[g7[T]]
set Pn[Vw]=Pn[g7[T]]
set Qn[Vw]=Qn[g7[T]]
set Rn[Vw]=Rn[g7[T]]
set Sn[Vw]=Sn[g7[T]]
set Tn[Vw]=Tn[g7[T]]
set Un[Vw]=Un[g7[T]]
set Vn[Vw]=Vn[g7[T]]
set Wn[Vw]=Wn[g7[T]]
set Xn[Vw]=Xn[g7[T]]
set Yn[Vw]=Yn[g7[T]]
set Zn[Vw]=Zn[g7[T]]
set do[Vw]=do[g7[T]]
set eo[Vw]=eo[g7[T]]
set fo[Vw]=fo[g7[T]]
set go[Vw]=go[g7[T]]
set ho[Vw]=ho[g7[T]]
set io[Vw]=go[Vw]
set jo[Vw]=ho[Vw]
set ko[Vw]=ko[g7[T]]
set mo[Vw]=mo[g7[T]]
set no[Vw]=no[g7[T]]
return Vw
endfunction
function Yw takes integer p returns nothing
local integer a=0
local integer b
local integer Zw
local integer f1=cg[p]
local integer f2=Dg[p]
call UnitAddAbility(fg[p],rd[f1])
call UnitAddAbility(fg[p],sd[f2])
call UnitAddAbility(gg[p],qd[f1])
call UnitAddAbility(ig[p],lo[pd[f1]])
call UnitAddAbility(ig[p],Jo[pd[f2]])
loop
set Zw=nd[od[f1]+a]
call UnitAddAbility(gg[p],Mn[g7[Zw]])
set b=0
loop
call UnitAddAbility(hg[p],to[uo[i7[Zw]]+b])
call UnitAddAbility(hg[p],vo[wo[i7[Zw]]+b])
set b=b+1
exitwhen b==3
endloop
set a=a+1
exitwhen a==4
endloop
set Eg[p]=f1
set Gg[p]=f1
set Fg[p]=f2
set Hg[p]=f2
endfunction
function ex takes integer Zq returns nothing
set Lg[Zq]=CreateTimer()
call SaveInteger(n7,4,GetHandleId((Lg[Zq])),(Zq))
endfunction
function fx takes integer Zq returns nothing
call ru(Lg[Zq])
set Lg[Zq]=null
endfunction
function gx takes player p,integer i returns integer
local integer Zq=ds()
local integer a
set Bf[Zq]=p
set Ff[Zq]=p==S
set Cf[Zq]=GetPlayerName(p)
call ex(Zq)
set bf[Zq]=Eu(Zq)
call Gu(Zq)
set z4[i]=Zq
set cf[Zq]=Wu(GetPlayerColor(p))
set A4[cf[Zq]]=true
if GetPlayerColor(p)==Y then
set a4=p
endif
set If[Zq]=Iv(Zq)
set kg[Zq]=gr()
set mg[Zq]=hr()
set ng[Zq]=jr()
set og[Zq]=kr()
set pg[Zq]=nr()
set qg[Zq]=mr()
set a=0
loop
call SetPlayerTechMaxAllowed(p,1949315120+a,gp[(a)])
set a=a+1
exitwhen a==j7
endloop
set a=1
loop
set yg[zg[Zq]+a]=Uw(a)
set a=a+1
exitwhen a==32
endloop
set a=1
loop
set wg[xg[Zq]+a]=Ww(a)
set a=a+1
exitwhen a==32
endloop
set a=1
loop
set sg[tg[Zq]+a]=Xw(a)
call SetPlayerAbilityAvailable(p,vo[wo[i7[a]]],false)
call SetPlayerAbilityAvailable(p,vo[wo[i7[a]]+1],false)
call SetPlayerAbilityAvailable(p,vo[wo[i7[a]]+2],false)
set a=a+1
exitwhen a==32
endloop
set a=1
loop
set ag[Bg[Zq]+a]=pr()
call SetPlayerAbilityAvailable(p,yn[f7[a]],false)
call SetPlayerAbilityAvailable(p,xn[f7[a]],false)
call SetPlayerAbilityAvailable(p,wn[f7[a]],false)
call SetPlayerAbilityAvailable(p,Cn[cn[f7[a]]],false)
call SetPlayerAbilityAvailable(p,Cn[cn[f7[a]]+1],false)
call SetPlayerAbilityAvailable(p,Bn[bn[f7[a]]],false)
call SetPlayerAbilityAvailable(p,Bn[bn[f7[a]]+1],false)
set a=a+1
exitwhen a==24
endloop
call TriggerRegisterPlayerUnitEvent(U7,p,EVENT_PLAYER_UNIT_CONSTRUCT_START,null)
call TriggerRegisterPlayerUnitEvent(V7,p,EVENT_PLAYER_UNIT_CONSTRUCT_CANCEL,null)
call TriggerRegisterPlayerUnitEvent(W7,p,EVENT_PLAYER_UNIT_CONSTRUCT_FINISH,null)
call TriggerRegisterPlayerUnitEvent(f8,p,EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerRegisterPlayerUnitEvent(g8,p,EVENT_PLAYER_UNIT_SPELL_ENDCAST,null)
call TriggerRegisterPlayerUnitEvent(h8,p,EVENT_PLAYER_UNIT_ISSUED_ORDER,null)
call TriggerRegisterPlayerUnitEvent(i8,p,EVENT_PLAYER_UNIT_ISSUED_POINT_ORDER,null)
call TriggerRegisterPlayerUnitEvent(X7,p,EVENT_PLAYER_UNIT_UPGRADE_START,null)
call TriggerRegisterPlayerUnitEvent(Y7,p,EVENT_PLAYER_UNIT_UPGRADE_CANCEL,null)
call TriggerRegisterPlayerUnitEvent(Z7,p,EVENT_PLAYER_UNIT_UPGRADE_FINISH,null)
call TriggerRegisterPlayerUnitEvent(j8,p,EVENT_PLAYER_UNIT_SELECTED,null)
call TriggerRegisterPlayerUnitEvent(k8,p,EVENT_PLAYER_UNIT_DESELECTED,null)
call TriggerRegisterPlayerEvent(T7,p,EVENT_PLAYER_LEAVE)
return Zq
endfunction
function hx takes integer Zq returns nothing
local integer k=0
local real x=Vd[If[Zq]]
local real y=320.
set Gf[Zq]=W[bf[Zq]*2/(Q8+1)]
call Kv(If[Zq])
loop
set Zf[dg[Zq]+k]=ku(Bf[Zq],"Textures\\Pavement.blp",256.,x,y,4)
set x=x+128.
if x>Sd[If[Zq]]then
set y=y-128.
set x=Vd[If[Zq]]
endif
set k=k+1
exitwhen k==120
endloop
set x=(Td[If[Zq]]+Sd[If[Zq]])/ 2.
set y=(n4+1152.)/ 2.
set fg[Zq]=CreateUnit(Bf[Zq],1112294482,x,y,.0)
set k=bf[Zq]
call SetHeroXP(fg[Zq],k,false)
if Ff[Zq]then
call PanCameraToTimed(x,y,.0)
call SelectUnit(fg[Zq],true)
endif
set y=1920.-64.
set gg[Zq]=CreateUnit(Bf[Zq],1112689491,x-128.,y,270.)
call SetUnitVertexColor(gg[Zq],$FF,$FF,$FF,64)
call SetHeroXP(gg[Zq],k,false)
set hg[Zq]=CreateUnit(Bf[Zq],1112232788,x-384.,y,270.)
call SetUnitVertexColor(hg[Zq],$FF,$FF,$FF,64)
call SetHeroXP(hg[Zq],k,false)
set ig[Zq]=CreateUnit(Bf[Zq],1095521362,x+128.,y,270.)
call SetUnitVertexColor(ig[Zq],$FF,$FF,$FF,64)
call SetHeroXP(ig[Zq],k,false)
set jg[Zq]=CreateUnit(Bf[Zq],1296256336,x+384.,y,270.)
call SetUnitVertexColor(jg[Zq],$FF,$FF,$FF,64)
call SetHeroXP(jg[Zq],k,false)
set lg[Zq]=ev(hg[Zq],1227894850)
set Jg[Zq]=ev(hg[Zq],1227894851)
call SetItemCharges(Jg[Zq],N4)
call Yw(Zq)
set Hf[Zq]=true
call Ot("Player "+I2S(Zq)+" is ready.")
endfunction
function jx takes integer Zq,integer T returns nothing
set ug[vg[Zq]+T]=ug[vg[Zq]+T]+1
if ug[vg[Zq]+T]==Wn[sg[tg[Zq]+T]]then
call SetPlayerAbilityAvailable(Bf[Zq],Mn[g7[T]],false)
endif
endfunction
function kx takes integer Zq,integer T returns nothing
set ug[vg[Zq]+T]=ug[vg[Zq]+T]-1
call SetPlayerAbilityAvailable(Bf[Zq],Mn[g7[T]],true)
endfunction
function mx takes integer Zq returns nothing
local integer a=0
loop
set ug[vg[Zq]+a]=0
call SetPlayerAbilityAvailable(Bf[Zq],Mn[g7[a]],true)
set a=a+1
exitwhen a==32
endloop
endfunction
function nx takes integer Zq,integer T,real bs returns real
set go[sg[tg[Zq]+T]]=go[sg[tg[Zq]+T]]+bs
set bs=(gd[hd[re[(Pn[sg[tg[Zq]+T]])]]+(rg[Zq])])*bs
set io[sg[tg[Zq]+T]]=io[sg[tg[Zq]+T]]+bs
return bs
endfunction
function ox takes integer Zq,integer T,real bs returns real
set ho[sg[tg[Zq]+T]]=ho[sg[tg[Zq]+T]]+bs
set bs=(gd[hd[re[(Pn[sg[tg[Zq]+T]])]]+(rg[Zq])])*bs
set jo[sg[tg[Zq]+T]]=jo[sg[tg[Zq]+T]]+bs
return bs
endfunction
function px takes integer Zq,integer Tv returns nothing
set Sn[sg[tg[Zq]+Fo[Ag[Zq]]]]=un[Tv]
if Cg[Zq]!=0 then
if Yg[Cg[Zq]]!=0 then
call ps(Yg[Cg[Zq]])
endif
set Hi=Zq
call TriggerEvaluate(d7[un[Tv]])
endif
set bg[Zq]=Tv
endfunction
function qx takes integer Zq returns nothing
set Kg[Zq]=0
if Ff[Zq]then
call ClearTextMessages()
endif
endfunction
function rx takes nothing returns nothing
set Kg[((LoadInteger(n7,4,GetHandleId(GetExpiredTimer()))))]=0
endfunction
function sx takes integer Zq,sound tx,string s returns nothing
if Ff[Zq]then
if Kg[Zq]==5 then
set Kg[Zq]=0
call ClearTextMessages()
endif
call StartSound(tx)
call DisplayTimedTextToPlayer(Bf[Zq],.0,.0,5.,s)
set Kg[Zq]=Kg[Zq]+1
endif
call TimerStart(Lg[Zq],5.,false,function rx)
endfunction
function ux takes integer Zq returns nothing
local integer a=0
set eg[Zq]=not eg[Zq]
loop
call ShowImage(Zf[dg[Zq]+a],eg[Zq])
set a=a+1
exitwhen a==120
endloop
endfunction
function vx takes integer Zq returns nothing
set Qf[Zq]=Qf[Zq]+1
call MultiboardSetItemValue(Yf[Zq],I2S(Qf[Zq]))
endfunction
function wx takes integer Zq,integer xx returns nothing
set Of[Zq]=Of[Zq]+xx
call MultiboardSetItemValue(Wf[Zq],I2S(Of[Zq]))
endfunction
function yx takes integer Zq,integer hw returns nothing
set Kf[Zq]=Kf[Zq]+hw
if Kf[Zq]>99999 then
set Kf[Zq]=99999
endif
call MultiboardSetItemValue(Tf[Zq],I2S(Kf[Zq]))
endfunction
function zx takes integer Zq,integer bs returns boolean
local boolean Ax=(Kf[(Zq)]-(bs)>=0)
if Ax then
call yx((Zq),-(bs))
endif
return Ax
endfunction
function ax takes integer Zq,integer bs returns boolean
local boolean Ax=(Kf[(Zq)]-(bs)>=0)
if Ax then
call yx((Zq),-(bs))
else
call sx((Zq),o8,Y9[T9]+"Error|r: not enough Gold.")
endif
return Ax
endfunction
function Bx takes integer Zq,string s,unit u returns nothing
call Zt(("+"+s),(u),(Bf[Zq]),9.,48.,3.,2.)
call SetTextTagColor(bj_lastCreatedTextTag,V9[J9],W9[J9],X9[J9],$FF)
endfunction
function bx takes integer Zq,integer hw returns nothing
set Lf[Zq]=Lf[Zq]+hw
if Lf[Zq]>99999 then
set Lf[Zq]=99999
endif
call MultiboardSetItemValue(Uf[Zq],I2S(Lf[Zq]))
endfunction
function Cx takes integer Zq,integer bs returns boolean
local boolean Ax=(Lf[(Zq)]-(bs)>=0)
if Ax then
call bx((Zq),-(bs))
endif
return Ax
endfunction
function Dx takes integer Zq,integer bs returns boolean
local boolean Ax=(Lf[(Zq)]-(bs)>=0)
if Ax then
call bx((Zq),-(bs))
else
call sx((Zq),n8,Y9[T9]+"Error|r: not enough Experience.")
endif
return Ax
endfunction
function Ex takes integer Zq,string s,unit u returns nothing
call Zt(("+"+s),(u),(Bf[Zq]),9.,48.,3.,2.)
call SetTextTagColor(bj_lastCreatedTextTag,V9[K9],W9[K9],X9[K9],$FF)
endfunction
function Fx takes integer Zq,real hw returns nothing
set Nf[Zq]=Nf[Zq]-hw
call MultiboardSetItemValue(Vf[Zq],I2S(Wt(Nf[Zq]))+"/"+I2S(Mf[Zq]))
endfunction
function Gx takes integer Zq,real hw returns boolean
return Nf[Zq]+hw<=Mf[Zq]
endfunction
function Hx takes integer Zq,integer l returns nothing
set Pf[Zq]=Pf[Zq]+l
if Pf[Zq]<0 then
set Pf[Zq]=0
elseif Pf[Zq]>9999 then
set Pf[Zq]=9999
endif
call MultiboardSetItemValue(Xf[Zq],I2S(Pf[Zq]))
endfunction
function Ix takes integer Zq,integer l returns nothing
if l==0 then
return
elseif l<-1 then
call sx(Zq,s8,Y9[S9]+"Warning|r: you lost "+Y9[P9]+I2S(-l)+"|r lives.")
elseif l==-1 then
call sx(Zq,s8,Y9[S9]+"Warning|r: you lost "+Y9[P9]+"1|r life.")
elseif l==1 then
call sx(Zq,t8,Y9[R9]+"Note|r: you received "+Y9[P9]+"1|r life.")
elseif l>1 then
call sx(Zq,t8,Y9[R9]+"Note|r: you received "+Y9[P9]+I2S(l)+"|r lives.")
endif
call Hx(Zq,l)
endfunction
function lx takes integer Zq returns nothing
local integer Jx=Wt(Pf[Zq]*.5)
if Pf[Zq]-Jx<1 then
set Jx=Pf[Zq]-1
endif
if Jx==1 then
call sx(Zq,u8,Y9[S9]+"Attention|r: the game lasts too long! You lost "+Y9[P9]+"1|r life.")
else
call sx(Zq,u8,Y9[S9]+"Attention|r: the game lasts too long! You lost "+Y9[P9]+I2S(Jx)+"|r lives.")
endif
call Hx(Zq,-Jx)
endfunction
function Kx takes integer Zq returns nothing
local integer a=0
local multiboarditem Lx
loop
set Lx=MultiboardGetItem(a8,Rf[Zq],a)
call MultiboardSetItemValueColor(Lx,V9[U9],W9[U9],X9[U9],K8)
set a=a+1
exitwhen a==8
endloop
set Lx=null
endfunction
function Mx takes integer Zq returns nothing
call fx(Zq)
call Lv(If[Zq])
if Hf[Zq]then
call Zr(Zq)
call Kx(Zq)
call RemoveUnit(fg[Zq])
call RemoveUnit(gg[Zq])
call RemoveUnit(ig[Zq])
call RemoveUnit(hg[Zq])
call RemoveUnit(jg[Zq])
set fg[Zq]=null
set gg[Zq]=null
set hg[Zq]=null
set ig[Zq]=null
set jg[Zq]=null
set Ig[Zq]=null
set lg[Zq]=null
set Jg[Zq]=null
endif
set Ef[Zq]=false
endfunction
function Nx takes integer Zq returns nothing
set xh[Zq]=CreateTimer()
call SaveInteger(n7,4,GetHandleId((xh[Zq])),(Zq))
endfunction
function Ox takes integer Zq returns nothing
call ru(xh[Zq])
set xh[Zq]=null
endfunction
function Px takes integer p,integer id returns boolean
local string Qx=GetObjectName(id)
local integer T
local real Rx
if SubString(Qx,0,B4)=="Spawn" then
set T=S2I(SubString(Qx,b4,C4))
set Rx=Vn[sg[tg[p]+T]]
if Gx(p,Rx)then
if ax(p,Tn[sg[tg[p]+T]])then
call Fx((p),-((Rx)*1.))
set Pg=p
call TriggerEvaluate(h7[T])
return true
endif
else
call sx(((p)),n8,Y9[T9]+"Error|r: "+("unable to create a minion due to the maximum supply limit."))
endif
endif
return false
endfunction
function Sx takes integer p returns nothing
if Ag[p]!=0 and Cg[p]==0 then
set Pg=p
call TriggerEvaluate(h7[Fo[Ag[p]]])
set Cg[p]=(f9[g9[(mg[p])]+(h9[mg[p]])])
endif
endfunction
function Tx takes integer p,integer id,boolean Ux returns boolean
local string Qx=GetObjectName(id)
local integer T
local integer Vx
if SubString(Qx,0,c4)=="Upgrade" then
set T=S2I(SubString(Qx,D4,E4))
set Vx=S2I(SubString(Qx,F4,G4))
set Qx=SubString(Qx,H4,StringLength(Qx))
if(Ux and ax(p,ao[Bo[i7[T]]+Vx]))or((not Ux)and Dx(p,ao[Bo[i7[T]]+Vx]))then
set Qg=p
set oo[po[sg[tg[p]+T]]+Vx]=true
call TriggerExecute(bo[Co[i7[T]]+Vx])
call SetPlayerAbilityAvailable(Bf[p],id,false)
call SetPlayerAbilityAvailable(Bf[p],vo[wo[i7[T]]+Vx],true)
call SetPlayerTechResearched(Bf[p],zo[Ao[i7[T]]+Vx],1)
call sx((p),p8,Y9[R9]+"Research complete|r: "+(Qx)+".")
return true
endif
endif
return false
endfunction
function Wx takes nothing returns nothing
if(IsUnitType(GetFilterUnit(),e4))then
call IssueImmediateOrderById(GetFilterUnit(),H)
endif
endfunction
function Xx takes integer Zq,integer T,integer p returns nothing
set Sg[Zq]=p
set Vg[Zq]=CreateUnit(Bf[p],Ln[g7[T]],mu(Wd[If[p]]),nu(Wd[If[p]]),270.)
call UnitAddAbility(Vg[Zq],1098282348)
call UnitMakeAbilityPermanent(Vg[Zq],true,1098282348)
call SetUnitUserData(Vg[Zq],Zq)
set Rg[Zq]=T
set Wg[Zq]=GetHandleId(Vg[Zq])
set hh[Zq]=sg[tg[p]+T]
call UnitAddAbility(Vg[Zq],1093677099)
call UnitMakeAbilityPermanent(Vg[Zq],true,1093677099)
call UnitAddAbility(Vg[Zq],Nn[g7[T]])
call UnitMakeAbilityPermanent(Vg[Zq],true,Nn[g7[T]])
call UnitAddAbility(Vg[Zq],xo[yo[i7[T]]])
call UnitMakeAbilityPermanent(Vg[Zq],true,xo[yo[i7[T]]])
call UnitAddAbility(Vg[Zq],xo[yo[i7[T]]+1])
call UnitMakeAbilityPermanent(Vg[Zq],true,xo[yo[i7[T]]+1])
call UnitAddAbility(Vg[Zq],xo[yo[i7[T]]+2])
call UnitMakeAbilityPermanent(Vg[Zq],true,xo[yo[i7[T]]+2])
set Hi=Zq
call TriggerEvaluate(d7[Sn[hh[Zq]]])
call UnitAddAbility(Vg[Zq],1093677122)
call UnitMakeAbilityPermanent(Vg[Zq],true,1093677122)
call UnitAddAbility(Vg[Zq],sk[Z4[Sn[hh[Zq]]]])
call UnitMakeAbilityPermanent(Vg[Zq],true,sk[Z4[Sn[hh[Zq]]]])
call Mu(mg[p],Zq)
call wx(p,Xn[hh[Zq]])
call jx(p,T)
endfunction
function Yx takes integer Zq,integer T,integer p returns nothing
local integer a
set Sg[Zq]=p
set Vg[Zq]=CreateUnit(Bf[p],Ln[g7[T]],mu(Wd[If[p]]),nu(Wd[If[p]]),270.)
call UnitAddAbility(Vg[Zq],1098282348)
call UnitMakeAbilityPermanent(Vg[Zq],true,1098282348)
call SetUnitUserData(Vg[Zq],Zq)
set Rg[Zq]=T
set Wg[Zq]=GetHandleId(Vg[Zq])
set hh[Zq]=sg[tg[p]+T]
call UnitAddAbility(Vg[Zq],1093677099)
call UnitMakeAbilityPermanent(Vg[Zq],true,1093677099)
call UnitAddAbility(Vg[Zq],Nn[g7[T]])
call UnitMakeAbilityPermanent(Vg[Zq],true,Nn[g7[T]])
call UnitAddAbility(Vg[Zq],xo[yo[i7[T]]])
call UnitMakeAbilityPermanent(Vg[Zq],true,xo[yo[i7[T]]])
call UnitAddAbility(Vg[Zq],xo[yo[i7[T]]+1])
call UnitMakeAbilityPermanent(Vg[Zq],true,xo[yo[i7[T]]+1])
call UnitAddAbility(Vg[Zq],xo[yo[i7[T]]+2])
call UnitMakeAbilityPermanent(Vg[Zq],true,xo[yo[i7[T]]+2])
set Hi=Zq
call TriggerEvaluate(d7[Sn[hh[Zq]]])
call Mu(mg[p],Zq)
set T=0
loop
set a=Go[Ho[Ag[p]]+T]
call UnitAddAbility(Vg[Zq],yn[a])
call UnitMakeAbilityPermanent(Vg[Zq],true,yn[a])
call UnitAddAbility(Vg[Zq],Cn[cn[a]])
call UnitMakeAbilityPermanent(Vg[Zq],true,Cn[cn[a]])
call UnitAddAbility(Vg[Zq],Cn[cn[a]+1])
call UnitMakeAbilityPermanent(Vg[Zq],true,Cn[cn[a]+1])
set T=T+1
exitwhen T==3
endloop
endfunction
function Zx takes integer Zq returns nothing
call Nu(mg[Sg[Zq]],Zq)
call kx(Sg[Zq],Rg[Zq])
call Fx(Sg[Zq],Un[hh[Zq]])
call wx(Sg[Zq],-Xn[hh[Zq]])
call yx(Sg[Zq],Tn[hh[Zq]])
call Bx(Sg[Zq],I2S(Tn[hh[Zq]]),Vg[Zq])
call DestroyEffect(AddSpecialEffect(("Abilities\\Spells\\Other\\Transmute\\PileofGold.mdx"),((GetUnitX(Vg[Zq]))*1.),((GetUnitY(Vg[Zq]))*1.)))
call RemoveUnit(Vg[Zq])
call Gs(Zq)
call Ot("Minion "+I2S(Zq)+" is sold.")
endfunction
function ey takes integer Zq returns nothing
call UnitRemoveAbility(Vg[Zq],1093677099)
set Tg[Zq]=Df[Sg[Zq]]
set Ug[Zq]=If[Tg[Zq]]
call Ou(ng[Tg[Zq]],Zq)
call SetUnitMoveSpeed(Vg[Zq],256.)
call SetUnitOwner(Vg[Zq],Gf[Tg[Zq]],false)
call SetUnitX(Vg[Zq],GetUnitX(Vg[Zq])-GetRectCenterX(Wd[If[Sg[Zq]]])+GetRectCenterX(Xd[Ug[Zq]]))
call SetUnitY(Vg[Zq],GetUnitY(Vg[Zq])-GetRectCenterY(Wd[If[Sg[Zq]]])+GetRectCenterY(Xd[Ug[Zq]]))
set ih[Zq]=mu(Zd[Ug[Zq]])
set jh[Zq]=GetRectMinY(Zd[Ug[Zq]])
call IssuePointOrderById(Vg[Zq],G,ih[Zq],jh[Zq])
call Ot("Minion "+I2S(Zq)+" is sent.")
endfunction
function fy takes integer Zq returns nothing
call Pu(ng[Tg[Zq]],Zq)
call Fx(Sg[Zq],Un[hh[Zq]])
call RemoveUnit(Vg[Zq])
call Gs(Zq)
call Ot("Spawned "+I2S(Zq)+" is deleted.")
endfunction
function gy takes integer Zq,boolean hy returns nothing
local string jy
if hy then
set jy=Y9[O9]+"+1"
else
set jy=""
endif
if Yn[hh[Zq]]>0 then
call yx(Tg[Zq],Yn[hh[Zq]])
set jy=jy+"|r
"+Y9[J9]+"+"+I2S(Yn[hh[Zq]])
endif
if Zn[hh[Zq]]>0 then
call bx(Tg[Zq],Zn[hh[Zq]])
set jy=jy+"|r
"+Y9[K9]+"+"+I2S(Zn[hh[Zq]])
endif
call Zt((jy),(Vg[Zq]),(Bf[Tg[Zq]]),9.,48.,3.,2.)
endfunction
function ky takes integer Zq returns boolean
return not(Zg[Zq]or dh[Zq])
endfunction
function my takes integer Zq returns nothing
local real jw=(le[Xg[(Zq)]])
if jw>.0 then
call UnitAddAbility(Vg[Zq],1093677105)
else
call UnitRemoveAbility(Vg[Zq],1093677105)
endif
call SetItemCharges(nh[Zq],Wt(jw))
endfunction
function ny takes integer Zq returns real
return eh[Zq]/ fh[Zq]
endfunction
function oy takes integer Zq,real hw returns nothing
if hw<.0 then
set hw=.0
elseif hw>fh[Zq]then
set hw=fh[Zq]
endif
set eh[Zq]=hw
call SetItemCharges(mh[Zq],Wt(eh[Zq]))
call SetWidgetLife(Vg[Zq],3.+eh[Zq]/ fh[Zq]*1000.)
endfunction
function py takes integer Zq,real hw returns nothing
call oy(Zq,eh[Zq]+hw)
endfunction
function qy takes integer Zq,real ry,real sy returns nothing
set fh[Zq]=fh[Zq]+ry
set gh[Zq]=gh[Zq]+sy
if Zg[Zq]then
call py(Zq,ry)
endif
endfunction
function ty takes integer Zq,real hw,string uy,string pu returns nothing
if eh[Zq]<fh[Zq]then
call py(Zq,hw)
call DestroyEffect(AddSpecialEffectTarget((uy),(Vg[Zq]),(pu)))
endif
endfunction
function vy takes integer Zq,real wy,string uy,string pu returns nothing
call ty(Zq,fh[Zq]*wy,uy,pu)
endfunction
function xy takes integer Zq returns integer
local integer yy
set J4=vs(Zq)
set yy=J4+Xe[Xg[Zq]]+gf[Xg[Zq]]
if yy>75 then
return 75
elseif yy<I4 then
return I4
endif
return yy
endfunction
function zy takes integer Zq returns integer
local integer yy
set K4=ws(Zq)
set yy=K4+df[Xg[Zq]]+gf[Xg[Zq]]
if yy>75 then
return 75
elseif yy<l4 then
return l4
endif
return yy
endfunction
function Ay takes integer Zq returns nothing
local real ay=us(Zq)
local real By=Me[Xg[Zq]]
local real by
call SetUnitMoveSpeed(Vg[Zq],256.*(ay+By))
set by=GetUnitMoveSpeed(Vg[Zq])
set By=by/ 256.-ay
if By>.0 then
call UnitRemoveAbility(Vg[Zq],1093677134)
call UnitAddAbility(Vg[Zq],1093677128)
elseif By<.0 then
call UnitRemoveAbility(Vg[Zq],1093677128)
call UnitAddAbility(Vg[Zq],1093677134)
else
call UnitRemoveAbility(Vg[Zq],1093677134)
call UnitRemoveAbility(Vg[Zq],1093677128)
endif
call SetItemCharges(qh[Zq],Vt(by/ 256.*100.))
endfunction
function Cy takes integer Zq returns nothing
if(Qe[Xg[(Zq)]])then
call IssueImmediateOrderById(Vg[Zq],K)
call UnitAddAbility(Vg[Zq],1093677142)
else
call IssuePointOrderById(Vg[Zq],G,ih[Zq],jh[Zq])
call UnitRemoveAbility(Vg[Zq],1093677142)
endif
endfunction
function Dy takes integer Zq returns nothing
local integer yy=xy(Zq)
local integer Ey=J4
if yy>0 then
call SetItemCharges(oh[Zq],yy)
call UnitRemoveAbility(Vg[Zq],1093677115)
call UnitRemoveAbility(Vg[Zq],1110454321)
call UnitAddAbility(Vg[Zq],1093677114)
elseif yy<0 then
call SetItemCharges(oh[Zq],-yy)
call UnitRemoveAbility(Vg[Zq],1093677114)
call UnitRemoveAbility(Vg[Zq],1110454320)
call UnitAddAbility(Vg[Zq],1093677115)
else
call SetItemCharges(oh[Zq],0)
call UnitRemoveAbility(Vg[Zq],1093677114)
call UnitRemoveAbility(Vg[Zq],1110454320)
call UnitRemoveAbility(Vg[Zq],1093677115)
call UnitRemoveAbility(Vg[Zq],1110454321)
endif
if yy>Ey then
call UnitRemoveAbility(Vg[Zq],1093677089)
call UnitAddAbility(Vg[Zq],1093677147)
elseif yy<Ey then
call UnitRemoveAbility(Vg[Zq],1093677147)
call UnitAddAbility(Vg[Zq],1093677089)
else
call UnitRemoveAbility(Vg[Zq],1093677089)
call UnitRemoveAbility(Vg[Zq],1093677147)
endif
endfunction
function Fy takes integer Zq returns nothing
local integer yy=zy(Zq)
local integer Ey=K4
if yy>0 then
call SetItemCharges(ph[Zq],yy)
call UnitRemoveAbility(Vg[Zq],1093677117)
call UnitRemoveAbility(Vg[Zq],1110454323)
call UnitAddAbility(Vg[Zq],1093677116)
elseif yy<0 then
call SetItemCharges(ph[Zq],-yy)
call UnitRemoveAbility(Vg[Zq],1093677116)
call UnitRemoveAbility(Vg[Zq],1110454322)
call UnitAddAbility(Vg[Zq],1093677117)
else
call SetItemCharges(ph[Zq],0)
call UnitRemoveAbility(Vg[Zq],1093677116)
call UnitRemoveAbility(Vg[Zq],1110454322)
call UnitRemoveAbility(Vg[Zq],1093677117)
call UnitRemoveAbility(Vg[Zq],1110454323)
endif
if yy>Ey then
call UnitRemoveAbility(Vg[Zq],1093677150)
call UnitAddAbility(Vg[Zq],1093677149)
elseif yy<Ey then
call UnitRemoveAbility(Vg[Zq],1093677149)
call UnitAddAbility(Vg[Zq],1093677150)
else
call UnitRemoveAbility(Vg[Zq],1093677150)
call UnitRemoveAbility(Vg[Zq],1093677149)
endif
endfunction
function Gy takes integer Zq returns nothing
local integer yy=xy(Zq)
local integer Ey=J4
if yy>0 then
call SetItemCharges(oh[Zq],yy)
call UnitRemoveAbility(Vg[Zq],1093677115)
call UnitRemoveAbility(Vg[Zq],1110454321)
call UnitAddAbility(Vg[Zq],1093677114)
elseif yy<0 then
call SetItemCharges(oh[Zq],-yy)
call UnitRemoveAbility(Vg[Zq],1093677114)
call UnitRemoveAbility(Vg[Zq],1110454320)
call UnitAddAbility(Vg[Zq],1093677115)
else
call SetItemCharges(oh[Zq],0)
call UnitRemoveAbility(Vg[Zq],1093677114)
call UnitRemoveAbility(Vg[Zq],1110454320)
call UnitRemoveAbility(Vg[Zq],1093677115)
call UnitRemoveAbility(Vg[Zq],1110454321)
endif
if yy>Ey then
call UnitRemoveAbility(Vg[Zq],1093677089)
call UnitAddAbility(Vg[Zq],1093677147)
elseif yy<Ey then
call UnitRemoveAbility(Vg[Zq],1093677147)
call UnitAddAbility(Vg[Zq],1093677089)
else
call UnitRemoveAbility(Vg[Zq],1093677089)
call UnitRemoveAbility(Vg[Zq],1093677147)
endif
set yy=zy(Zq)
set Ey=K4
if yy>0 then
call SetItemCharges(ph[Zq],yy)
call UnitRemoveAbility(Vg[Zq],1093677117)
call UnitRemoveAbility(Vg[Zq],1110454323)
call UnitAddAbility(Vg[Zq],1093677116)
elseif yy<0 then
call SetItemCharges(ph[Zq],-yy)
call UnitRemoveAbility(Vg[Zq],1093677116)
call UnitRemoveAbility(Vg[Zq],1110454322)
call UnitAddAbility(Vg[Zq],1093677117)
else
call SetItemCharges(ph[Zq],0)
call UnitRemoveAbility(Vg[Zq],1093677116)
call UnitRemoveAbility(Vg[Zq],1110454322)
call UnitRemoveAbility(Vg[Zq],1093677117)
call UnitRemoveAbility(Vg[Zq],1110454323)
endif
if yy>Ey then
call UnitRemoveAbility(Vg[Zq],1093677150)
call UnitAddAbility(Vg[Zq],1093677149)
elseif yy<Ey then
call UnitRemoveAbility(Vg[Zq],1093677149)
call UnitAddAbility(Vg[Zq],1093677150)
else
call UnitRemoveAbility(Vg[Zq],1093677150)
call UnitRemoveAbility(Vg[Zq],1093677149)
endif
endfunction
function Hy takes integer Zq,integer Zv,real hw returns nothing
call gw(Xg[Zq],Zv,hw)
call my(Zq)
endfunction
function Iy takes integer Zq,integer Zv,real hw returns nothing
call pw(Xg[Zq],Zv,hw)
call Ay(Zq)
endfunction
function Jy takes integer Zq,integer Zv,boolean hw returns nothing
local boolean Ky=not(Ue[Xg[(Zq)]])
call vw(Xg[Zq],Zv,hw)
if hw and Ky then
call Qw(Xg[Zq])
call Ay(Zq)
call Cy(Zq)
call Ot("Minion is Unstoppable!")
endif
endfunction
function My takes integer Zq,integer Zv,integer hw returns nothing
call aw(Xg[Zq],Zv,hw)
call Fy(Zq)
endfunction
function Ny takes integer Zq,integer Zv,integer hw returns nothing
call Cw(Xg[Zq],Zv,hw)
call Gy(Zq)
endfunction
function Oy takes integer Zq,integer Zv,boolean hw returns nothing
local boolean Ky=not(yf[Xg[(Zq)]])
call Mw(Xg[Zq],Zv,hw)
if hw then
if Ky then
call Rw(Xg[Zq])
call Ay(Zq)
call Cy(Zq)
call Gy(Zq)
call UnitAddType(Vg[Zq],g4)
call Ot("Minion is Invulnerable!")
endif
elseif not(yf[Xg[(Zq)]])then
call UnitRemoveType(Vg[(Zq)],g4)
endif
endfunction
function Py takes integer Zq,integer Vv,real hw returns integer
local integer Zv=ew(Xg[Zq],Vv)
call Hy(Zq,Zv,hw)
return Zv
endfunction
function Qy takes integer Zq,integer Vv,real hw returns integer
local integer Zv=kw(Xg[Zq],Vv)
call Iy(Zq,Zv,hw)
return Zv
endfunction
function Sy takes integer Zq,integer Vv,boolean hw returns integer
local integer Zv=tw(Xg[Zq],Vv)
call Jy(Zq,Zv,hw)
return Zv
endfunction
function Ty takes integer Zq,integer Vv,integer hw returns integer
local integer Zv=zw(Xg[Zq],Vv)
call My(Zq,Zv,hw)
return Zv
endfunction
function Uy takes integer Zq,integer Vv,integer hw returns integer
local integer Zv=Bw(Xg[Zq],Vv)
call Ny(Zq,Zv,hw)
return Zv
endfunction
function Vy takes integer Zq,integer Vv,boolean hw returns integer
local integer Zv=cw(Xg[Zq],Vv)
call Ew(Xg[(Zq)],(Zv),(hw))
return Zv
endfunction
function Wy takes integer Zq,integer Vv,boolean hw returns integer
local integer Zv=Iw(Xg[Zq],Vv)
call Jw(Xg[(Zq)],(Zv),(hw))
return Zv
endfunction
function Xy takes integer Zq,integer Vv,boolean hw returns integer
local integer Zv=Kw(Xg[Zq],Vv)
call Oy(Zq,Zv,hw)
return Zv
endfunction
function Yy takes integer Zq,integer Zv returns nothing
call fw(Xg[Zq],Zv)
call my(Zq)
endfunction
function Zy takes integer Zq,integer Zv returns nothing
call nw(Xg[Zq],Zv)
call Ay(Zq)
endfunction
function dz takes integer Zq,integer Zv returns nothing
call rw(Xg[Zq],Zv)
call Cy(Zq)
endfunction
function ez takes integer Zq,integer Zv returns nothing
call Aw(Xg[Zq],Zv)
call Fy(Zq)
endfunction
function fz takes integer Zq,integer Zv returns nothing
call bw(Xg[Zq],Zv)
call Gy(Zq)
endfunction
function gz takes integer Zq,integer Zv returns nothing
call Lw(Xg[Zq],Zv)
call UnitRemoveType(Vg[(Zq)],g4)
endfunction
function hz takes integer Zq returns boolean
return(yf[Xg[(Zq)]])or(uf[Xg[(Zq)]])or ys(Zq)
endfunction
function iz takes integer Zq,integer js returns boolean
return(yf[Xg[(Zq)]])or(uf[Xg[(Zq)]])or ys(Zq)or(js==1227894834 and((mf[Xg[(Zq)]])or zs(Zq)))or(js==1227894841 and((qf[Xg[(Zq)]])or As(Zq)))or(js==1227894832 and as(Zq))
endfunction
function jz takes integer Zq,real bs returns real
return(100-xy(Zq))*bs/ 100.
endfunction
function kz takes integer Zq,real bs returns real
return(100-zy(Zq))*bs/ 100.
endfunction
function mz takes integer Zq,real bs,integer js returns real
if(yf[Xg[(Zq)]])then
return .0
endif
if js==1227894834 then
if(mf[Xg[(Zq)]])or(uf[Xg[(Zq)]])then
call Nw(Xg[Zq],bs)
return .0
endif
return Bs(Zq,bs)
endif
if js==1227894841 then
if(qf[Xg[(Zq)]])or(uf[Xg[(Zq)]])then
call Ow(Xg[Zq],bs)
return .0
endif
return Cs(Zq,bs)
endif
if js==1227894832 then
if(uf[Xg[(Zq)]])then
call Pw(Xg[Zq],(fh[(Zq)]*((bs)*1.)/ 1000.))
return .0
endif
return cs(Zq,bs)
endif
call Ot("Unrecognized damage type!")
return .0
endfunction
function nz takes integer Zq,real bs returns real
set bs=iw(Xg[Zq],bs)
call my(Zq)
return bs
endfunction
function oz takes integer Zq,integer pz,real bs,integer js,boolean qz returns boolean
if not iz(Zq,js)then
call Nt(1,Zq,pz,js)
endif
set bs=mz(Zq,bs,js)
if bs<=.0 then
return true
endif
set bs=nz(Zq,bs)
if bs>.0 then
call py(Zq,-bs)
endif
if eh[Zq]>.0 then
return true
endif
call gy(Zq,true)
call Sr(pz,Zq)
call Ds(Zq,qz)
return false
endfunction
function rz takes integer Zq returns integer
if IsUnitType(Vg[Zq],UNIT_TYPE_FLYING)then
return zd[Mv(Ug[Zq],GetUnitX(Vg[Zq]),GetUnitY(Vg[Zq]))]
endif
return xd[Mv(Ug[Zq],GetUnitX(Vg[Zq]),GetUnitY(Vg[Zq]))]
endfunction
function sz takes integer Zq returns nothing
call Fx(Sg[Zq],Un[hh[Zq]])
call Ru(og[Tg[Zq]],Zq)
call Tw(Xg[Zq])
call RemoveItem(mh[Zq])
set mh[Zq]=null
call RemoveItem(nh[Zq])
set nh[Zq]=null
call RemoveItem(oh[Zq])
set oh[Zq]=null
call RemoveItem(ph[Zq])
set ph[Zq]=null
call RemoveItem(qh[Zq])
set qh[Zq]=null
call RemoveItem(rh[Zq])
set rh[Zq]=null
call UnitRemoveAbility(Vg[Zq],1093677105)
call UnitRemoveAbility(Vg[Zq],1093677128)
call UnitRemoveAbility(Vg[Zq],1093677134)
call UnitRemoveAbility(Vg[Zq],1093677142)
call UnitRemoveAbility(Vg[Zq],1093677149)
call UnitRemoveAbility(Vg[Zq],1093677150)
call UnitRemoveAbility(Vg[Zq],1093677147)
call UnitRemoveAbility(Vg[Zq],1093677089)
endfunction
function tz takes integer Zq returns nothing
call Ox(Zq)
call Vu(pg[Sg[Zq]],Zq)
call sz(Zq)
call Gs(Zq)
call Ot("Minion "+I2S(Zq)+" is deleted.")
endfunction
function uz takes nothing returns nothing
local integer Zq=(LoadInteger(n7,4,GetHandleId(GetExpiredTimer())))
if gh[Zq]>.0 then
call py(Zq,gh[Zq])
endif
set kh[Zq]=kh[Zq]-.25
if kh[Zq]>.0 then
call SetItemCharges(rh[Zq],Wt(kh[Zq]))
else
call Rv(Vg[Zq])
call tz(Zq)
endif
endfunction
function vz takes integer Zq returns nothing
call Pu(ng[Tg[Zq]],Zq)
set fh[Zq]=io[hh[Zq]]
set gh[Zq]=jo[hh[Zq]]
set kh[Zq]=On[hh[Zq]]
call Nx(Zq)
call UnitAddAbility(Vg[Zq],1093677113)
call UnitAddAbility(Vg[Zq],1093677118)
set mh[Zq]=UnitAddItemById(Vg[Zq],1227894836)
set nh[Zq]=UnitAddItemById(Vg[Zq],1227894847)
set oh[Zq]=UnitAddItemById(Vg[Zq],1227894840)
set ph[Zq]=UnitAddItemById(Vg[Zq],1227894837)
set qh[Zq]=UnitAddItemById(Vg[Zq],1227894848)
set rh[Zq]=UnitAddItemById(Vg[Zq],1227894849)
call SetItemCharges(rh[Zq],Wt(kh[Zq]))
call TriggerRegisterUnitEvent(m8,Vg[Zq],EVENT_UNIT_DAMAGED)
call TimerStart(xh[Zq],.25,true,function uz)
set Xg[Zq]=Yr()
call Qu(og[Tg[Zq]],Zq)
call Uu(pg[Sg[Zq]],Zq)
set Hi=Zq
call TriggerEvaluate(e7[Sn[hh[Zq]]])
call oy(Zq,fh[Zq])
call Ay(Zq)
call Gy(Zq)
set Zg[Zq]=true
call SetUnitInvulnerable(Vg[Zq],false)
set dh[Zq]=false
endfunction
function wz takes integer Zq returns boolean
return Gx(Sg[Zq],Un[hh[Zq]])
endfunction
function xz takes integer Zq,integer yz,real wy returns nothing
call Tu(qg[Tg[Zq]],Zq)
call RemoveUnit(Vg[Zq])
set Vg[Zq]=CreateUnit(Gf[Tg[Zq]],yz,sh[Zq],th[Zq],uh[Zq])
set Wg[Zq]=GetHandleId(Vg[Zq])
call SetUnitColor(Vg[Zq],GetPlayerColor(Bf[Sg[Zq]]))
call SetUnitUserData(Vg[Zq],Zq)
call Fx((Sg[Zq]),-((Un[hh[Zq]])*1.))
set kh[Zq]=On[hh[Zq]]
call UnitAddAbility(Vg[Zq],1093677113)
call UnitAddAbility(Vg[Zq],1093677118)
set mh[Zq]=UnitAddItemById(Vg[Zq],1227894836)
set nh[Zq]=UnitAddItemById(Vg[Zq],1227894847)
set oh[Zq]=UnitAddItemById(Vg[Zq],1227894840)
set ph[Zq]=UnitAddItemById(Vg[Zq],1227894837)
set qh[Zq]=UnitAddItemById(Vg[Zq],1227894848)
set rh[Zq]=UnitAddItemById(Vg[Zq],1227894849)
call SetItemCharges(rh[Zq],Wt(kh[Zq]))
call TriggerRegisterUnitEvent(m8,Vg[Zq],EVENT_UNIT_DAMAGED)
call TimerStart(xh[Zq],.25,true,function uz)
set Xg[Zq]=Yr()
call Qu(og[Tg[Zq]],Zq)
call ns(Yg[Zq])
call oy(Zq,fh[Zq]*wy)
call Ay(Zq)
call Gy(Zq)
set Zg[Zq]=true
call IssuePointOrderById(Vg[Zq],G,ih[Zq],jh[Zq])
endfunction
function zz takes integer Zq,real wy returns nothing
call xz(Zq,GetUnitTypeId(Vg[Zq]),wy)
endfunction
function Az takes integer Zq returns nothing
call Tu(qg[Tg[Zq]],Zq)
call Vu(pg[Sg[Zq]],Zq)
call Ox(Zq)
call RemoveUnit(Vg[Zq])
call Gs(Zq)
call Ot("Corpse "+I2S(Zq)+" is removed.")
endfunction
function az takes nothing returns nothing
call Az(((LoadInteger(n7,4,GetHandleId(GetExpiredTimer())))))
endfunction
function Bz takes integer Zq returns nothing
call RemoveUnit(Vg[Zq])
call tz(Zq)
endfunction
function bz takes integer Zq,boolean Es returns nothing
call SetUnitExploded(Vg[Zq],Es)
call KillUnit(Vg[Zq])
if Es or Rn[hh[Zq]]then
call tz(Zq)
else
set Zg[Zq]=false
call sz(Zq)
call Su(qg[Tg[Zq]],Zq)
call ms(Yg[Zq])
set sh[Zq]=GetUnitX(Vg[Zq])
set th[Zq]=GetUnitY(Vg[Zq])
set uh[Zq]=GetUnitFacing(Vg[Zq])
call TimerStart(xh[Zq],15.,false,function az)
endif
endfunction
function s__Minion_on_init takes nothing returns nothing
set yh=Filter(function Wx)
endfunction
function Cz takes integer cz returns integer
if cz<=9 then
return M4[cz]
endif
return M4[9]
endfunction
function Dz takes integer p returns nothing
local integer m
local integer a=0
local real ay
local real array Ez
local real array Fz
local integer Gz=Cz(rg[p])
if ax(p,Gz)then
set rg[p]=rg[p]+1
loop
set ay=(gd[hd[re[(Pn[sg[tg[p]+a]])]]+(rg[p])])
set Ez[a]=go[sg[tg[p]+a]]*ay-io[sg[tg[p]+a]]
set io[sg[tg[p]+a]]=io[sg[tg[p]+a]]+Ez[a]
set Fz[a]=ho[sg[tg[p]+a]]*ay-jo[sg[tg[p]+a]]
set jo[sg[tg[p]+a]]=jo[sg[tg[p]+a]]+Fz[a]
set a=a+1
exitwhen a==32
endloop
set a=0
loop
exitwhen a>D9[pg[p]]
set m=(C9[c9[(pg[p])]+(a)])
call qy(m,Ez[Rg[m]],Fz[Rg[m]])
set a=a+1
endloop
call SetItemCharges(lg[p],rg[p])
if rg[p]==L4 then
call hv(hg[p],Jg[p])
else
call SetItemCharges(Jg[p],Cz(rg[p])+1)
endif
call sx((p),p8,Y9[R9]+"Research complete|r: "+(("Minion Health Upgrade (level "+I2S((rg[p]))+")"))+".")
else
call SetItemCharges(Jg[p],Gz+1)
endif
endfunction
function Hz takes nothing returns nothing
set yh=Filter(function Wx)
set M4[0]=$3E8
set M4[1]=$438
set M4[2]=$48D
set M4[3]=$4EC
set M4[4]=$550
set M4[5]=$5BE
set M4[6]=$631
set M4[7]=$6AE
set M4[8]=$73A
set M4[9]=$7D0
set N4=M4[0]
endfunction
function Iz takes integer p,integer id,real hw returns nothing
local integer a=0
local integer m
local real lz=nx(p,id,hw)
loop
exitwhen a>D9[pg[p]]
set m=(C9[c9[(pg[p])]+(a)])
if Rg[m]==id then
set fh[m]=fh[m]+lz
if Zg[m]then
call py(m,lz)
endif
endif
set a=a+1
endloop
endfunction
function Jz takes integer p,integer id,real hw returns nothing
local integer a=0
local integer m
local real Fz=ox(p,id,hw)
loop
exitwhen a>D9[pg[p]]
set m=(C9[c9[(pg[p])]+(a)])
if Rg[m]==id then
set gh[m]=gh[m]+Fz
endif
set a=a+1
endloop
endfunction
function Kz takes integer p,integer id,real hw returns nothing
local integer a=0
local integer m
set ko[sg[tg[p]+id]]=ko[sg[tg[p]+id]]+hw
loop
exitwhen a>D9[pg[p]]
set m=(C9[c9[(pg[p])]+(a)])
if Rg[m]==id and Zg[m]then
call Ay(m)
endif
set a=a+1
endloop
endfunction
function Lz takes integer p,integer id,integer hw returns nothing
local integer a=0
local integer m
set mo[sg[tg[p]+id]]=mo[sg[tg[p]+id]]+hw
set no[sg[tg[p]+id]]=no[sg[tg[p]+id]]+hw
loop
exitwhen a>D9[pg[p]]
set m=(C9[c9[(pg[p])]+(a)])
if Rg[m]==id and Zg[m]then
call Gy(m)
endif
set a=a+1
endloop
endfunction
function Mz takes integer p,integer id,integer hw returns nothing
local integer a=0
local integer m
set mo[sg[tg[p]+id]]=mo[sg[tg[p]+id]]+hw
loop
exitwhen a>D9[pg[p]]
set m=(C9[c9[(pg[p])]+(a)])
if Rg[m]==id and Zg[m]then
call Dy(m)
endif
set a=a+1
endloop
endfunction
function Nz takes integer Zq returns nothing
set Ih[Zq]=CreateTimer()
call SaveInteger(n7,4,GetHandleId((Ih[Zq])),(Zq))
endfunction
function Oz takes integer Zq returns nothing
call ru(Ih[Zq])
set Ih[Zq]=null
endfunction
function Pz takes integer Zq,integer Qz,unit Rz,integer p,integer T returns nothing
if Zq==0 then
call bu("Error: MinionBuff struct has run out of indexes. Current amount of indexes is 8190. Error detected on field of player "+I2S(p),"MinionBuffError")
endif
set Bh[Zq]=T
set bh[Zq]=Qz
set Ch[Zq]=Rz
set ch[Zq]=p
set Dh[Zq]=yg[zg[p]+T]
call Nz(Zq)
call SaveInteger(n7,Wg[bh[Zq]],T,Zq)
endfunction
function Sz takes nothing returns nothing
local integer Zq=(LoadInteger(n7,4,GetHandleId(GetExpiredTimer())))
set Eh[Zq]=Eh[Zq]-1
if Hs(Zq)then
call ls(Zq)
endif
endfunction
function Tz takes integer Zq,integer Uz returns nothing
set Eh[Zq]=Eh[Zq]+Uz
if Eh[Zq]>oi[Dh[Zq]]then
set Eh[Zq]=oi[Dh[Zq]]
endif
endfunction
function Vz takes integer Zq,real Wz returns nothing
set Eh[Zq]=ni[Dh[Zq]]
call UnitAddAbility(Vg[bh[Zq]],ri[Dh[Zq]])
call TimerStart(Ih[Zq],Wz,mi[Dh[Zq]],function Sz)
set Fh[Zq]=true
endfunction
function Xz takes integer Zq returns nothing
call Vz(Zq,qi[Dh[Zq]])
endfunction
function Yz takes integer Zq,integer Uz returns nothing
if Fh[Zq]then
call Tz(Zq,Uz)
else
call Xz(Zq)
endif
endfunction
function Zz takes integer m returns integer
local integer Zq=Us()
set Lh[Zq]=m
set Mh[Zq]=CreateUnit(Bf[cj[m]],1685417325,lj[m],Jj[m],.0)
call SetUnitPathing(Mh[Zq],false)
call SetUnitFlyHeight(Mh[Zq],Hj[m],.0)
call UnitAddType(Mh[Zq],d4)
set Oh[Zq]=To[Nj[m]]
call UnitAddAbility(Mh[Zq],Oh[Zq])
call SetUnitUserData(Mh[Zq],m)
set Nh[Zq]=Uo[Nj[m]]
call Ot("TowerAttacker "+I2S(Zq)+" is created.")
return Zq
endfunction
function dA takes integer Zq,integer eA,integer fA returns nothing
call UnitRemoveAbility(Mh[Zq],Oh[Zq])
set Oh[Zq]=eA
call UnitAddAbility(Mh[Zq],eA)
set Nh[Zq]=fA
endfunction
function gA takes integer Zq returns nothing
call dA(Zq,To[Nj[Lh[Zq]]],Uo[Nj[Lh[Zq]]])
endfunction
function hA takes integer Zq returns nothing
call IncUnitAbilityLevel(Mh[Zq],Oh[Zq])
endfunction
function iA takes integer Zq,real x,real y returns boolean
return IssuePointOrderById(Mh[Zq],Nh[Zq],x,y)
endfunction
function jA takes integer Zq,widget gs returns boolean
return IssueTargetOrderById(Mh[Zq],Nh[Zq],gs)
endfunction
function kA takes integer Zq returns nothing
call RemoveUnit(Mh[Zq])
set Mh[Zq]=null
call Ot("TowerAttacker "+I2S(Zq)+" is deleted.")
endfunction
function mA takes integer Zq returns nothing
if Zq==null then
return
elseif(Kh[Zq]!=-1)then
return
endif
call kA(Zq)
set Kh[Zq]=lh
set lh=Zq
endfunction
function nA takes integer m returns integer
local integer Zq=Ss()
set Sh[Zq]=m
set Th[Zq]=rz(m)
set Uh[Zq]=eh[m]
return Zq
endfunction
function oA takes integer Zq,integer pA returns nothing
set Wh[Zq]=pA
set Vh[Zq]=Vh[pA]
if Vh[Zq]!=0 then
set Wh[Vh[Zq]]=Zq
endif
set Vh[pA]=Zq
endfunction
function qA takes integer Zq,integer pA returns nothing
set Vh[Zq]=pA
set Wh[Zq]=Wh[pA]
if Wh[Zq]!=0 then
set Vh[Wh[Zq]]=Zq
endif
set Wh[pA]=Zq
endfunction
function rA takes integer Zq returns integer
local integer Dv=di[Zq]
local integer m=Sh[Dv]
set di[Zq]=Wh[Dv]
if(di[(Zq)]!=0)then
set Vh[di[Zq]]=0
endif
call Ts(Dv)
return m
endfunction
function sA takes integer tA,integer uA returns integer
local integer Zq=Qs()
set di[Zq]=tA
set ei[Zq]=uA
return Zq
endfunction
function vA takes integer Zq returns nothing
local integer n=di[Zq]
local integer wA
loop
exitwhen n==0
set wA=Wh[n]
call Ts(n)
set n=wA
endloop
endfunction
function xA takes integer Qz returns integer
local group g=CreateGroup()
local unit u
local integer c
local integer Zv
local integer uA=0
local integer tA=0
call GroupEnumUnitsInRange(g,lj[Qz],Jj[Qz],Qo[Nj[Qz]],Po[Nj[Qz]])
loop
set u=FirstOfGroup(g)
exitwhen u==null
call GroupRemoveUnit(g,u)
set c=nA(GetUnitUserData(u))
set Zv=uA
loop
if Zv==0 then
if tA==0 then
set uA=c
set tA=c
else
call oA(c,tA)
set tA=c
endif
exitwhen true
elseif Th[c]>=Th[Zv]then
call qA(c,Zv)
if Zv==uA then
set uA=c
endif
exitwhen true
endif
set Zv=Vh[Zv]
endloop
endloop
call DestroyGroup(g)
set g=null
if tA==0 then
return 0
endif
return sA(tA,uA)
endfunction
function yA takes nothing returns boolean
local integer m=GetUnitUserData(GetFilterUnit())
return((IsUnitType(GetFilterUnit(),e4))and Zg[m]and not(yf[Xg[(m)]]))!=null
endfunction
function zA takes nothing returns boolean
local integer m=GetUnitUserData(GetFilterUnit())
return((IsUnitType(GetFilterUnit(),e4))and IsUnitType(Vg[m],UNIT_TYPE_GROUND)and Zg[m]and not(yf[Xg[(m)]]))!=null
endfunction
function AA takes nothing returns boolean
local integer m=GetUnitUserData(GetFilterUnit())
return((IsUnitType(GetFilterUnit(),e4))and IsUnitType(Vg[m],UNIT_TYPE_FLYING)and Zg[m]and not(yf[Xg[(m)]]))!=null
endfunction
function aA takes nothing returns nothing
set P4[0]=Filter(function yA)
set P4[1]=Filter(function zA)
set P4[2]=Filter(function AA)
endfunction
function BA takes integer Zq,integer bA returns nothing
set Gh[Zq]=Ty(bh[Zq],Zq,bA)
endfunction
function CA takes integer m,unit cA,integer p,integer bA returns nothing
local integer Zq=(LoadInteger(n7,Wg[(m)],(8)))
if Zq==0 then
set Zq=Ps()
call Pz(Zq,m,cA,p,8)
call BA(Zq,bA)
elseif Ch[Zq]!=cA then
set Ch[Zq]=cA
endif
call Xz(Zq)
endfunction
function DA takes integer Zq returns nothing
set Gh[Zq]=Wy(bh[Zq],Zq,true)
endfunction
function EA takes integer m,integer cA returns nothing
local integer Zq=(LoadInteger(n7,Wg[(m)],(9)))
if Zq==0 then
set Zq=Os()
call Pz(Zq,m,Vg[cA],Sg[cA],9)
call DA(Zq)
elseif Ch[Zq]!=Vg[cA]then
set Ch[Zq]=Vg[cA]
endif
call Xz(Zq)
endfunction
function FA takes integer Zq,real GA returns nothing
set Gh[Zq]=Py(bh[Zq],Zq,GA)
endfunction
function HA takes integer cA,real GA returns nothing
local integer Zq=(LoadInteger(n7,Wg[(cA)],(S4)))
if Zq==0 then
set Zq=Ns()
call Pz(Zq,cA,Vg[cA],Sg[cA],S4)
call FA(Zq,GA)
elseif Ch[Zq]!=Vg[cA]then
set Ch[Zq]=Vg[cA]
endif
call Xz(Zq)
endfunction
function IA takes integer Zq,boolean up returns nothing
set Gh[Zq]=Vy(bh[Zq],Zq,true)
if up then
set fi[Zq]=true
set Hh[Zq]=Sy(bh[Zq],Zq,true)
endif
call AddUnitAnimationProperties(Vg[bh[Zq]],"Defend",true)
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\Human\\Defend\\DefendCaster.mdx"),(Vg[bh[Zq]]),("origin")))
endfunction
function lA takes integer cA returns nothing
local integer Zq=(LoadInteger(n7,Wg[(cA)],(7)))
if Zq==0 then
set Zq=Ms()
call Pz(Zq,cA,Vg[cA],Sg[cA],7)
call IA(Zq,oo[po[hh[cA]]+1])
elseif Ch[Zq]!=Vg[cA]then
set Ch[Zq]=Vg[cA]
endif
call Xz(Zq)
endfunction
function JA takes integer Zq,integer yy,real KA returns nothing
set Gh[Zq]=Uy(bh[Zq],Zq,yy)
set Hh[Zq]=Qy(bh[Zq],Zq,KA)
endfunction
function LA takes integer m,integer cA,integer yy,real KA returns nothing
local integer Zq=(LoadInteger(n7,Wg[(m)],(R4)))
if Zq==0 then
set Zq=Ls()
call Pz(Zq,m,Vg[cA],Sg[cA],R4)
call JA(Zq,yy,KA)
elseif Ch[Zq]!=Vg[cA]then
set Ch[Zq]=Vg[cA]
endif
call Xz(Zq)
endfunction
function MA takes integer Zq returns nothing
set Gh[Zq]=(Yv(Xg[(bh[Zq])],(Zq)))
endfunction
function NA takes integer m,integer cA returns nothing
local integer Zq=(LoadInteger(n7,Wg[(m)],(T4)))
if Zq==0 then
set Zq=Ks()
call Pz(Zq,m,Vg[cA],Sg[cA],T4)
call MA(Zq)
elseif Ch[Zq]!=Vg[cA]then
set Ch[Zq]=Vg[cA]
endif
call Yz(Zq,ni[Dh[Zq]])
endfunction
function OA takes integer Zq,boolean PA,real KA returns nothing
set Gh[Zq]=Qy(bh[Zq],Zq,KA)
if PA then
set gi[Zq]=true
set Hh[Zq]=Sy(bh[Zq],Zq,true)
endif
endfunction
function QA takes integer cA,real KA returns nothing
local integer Zq=(LoadInteger(n7,Wg[(cA)],(W4)))
if Zq==0 then
set Zq=Js()
call Pz(Zq,cA,Vg[cA],Sg[cA],W4)
call OA(Zq,ny(cA)<=.35,KA)
elseif Ch[Zq]!=Vg[cA]then
set Ch[Zq]=Vg[cA]
endif
call Xz(Zq)
endfunction
function RA takes integer Zq returns nothing
set Gh[Zq]=Xy(bh[Zq],Zq,true)
endfunction
function SA takes integer cA returns nothing
local integer Zq=(LoadInteger(n7,Wg[(cA)],(U4)))
if Zq==0 then
set Zq=Ws()
call Pz(Zq,cA,Vg[cA],Sg[cA],U4)
call RA(Zq)
elseif Ch[Zq]!=Vg[cA]then
set Ch[Zq]=Vg[cA]
endif
call Xz(Zq)
endfunction
function TA takes integer Zq returns nothing
set Gh[Zq]=Xy(bh[Zq],Zq,true)
endfunction
function UA takes integer m,integer cA returns nothing
local integer Zq=(LoadInteger(n7,Wg[(m)],(16)))
if Zq==0 then
set Zq=Xs()
call Pz(Zq,m,Vg[cA],Sg[cA],16)
call TA(Zq)
elseif Ch[Zq]!=Vg[cA]then
set Ch[Zq]=Vg[cA]
endif
call Xz(Zq)
endfunction
function VA takes integer Zq returns nothing
set Gh[Zq]=(Yv(Xg[(bh[Zq])],(Zq)))
endfunction
function WA takes integer m,integer cA returns nothing
local integer Zq=(LoadInteger(n7,Wg[(m)],(17)))
if Zq==0 then
set Zq=Ys()
call Pz(Zq,m,Vg[cA],Sg[cA],17)
call VA(Zq)
elseif Ch[Zq]!=Vg[cA]then
set Ch[Zq]=Vg[cA]
endif
call Yz(Zq,ni[Dh[Zq]])
endfunction
function XA takes integer T,boolean YA,integer ZA,integer da,real ea,integer fa returns nothing
set Q4[T]=ss()
set ki[Q4[T]]=YA
set mi[Q4[T]]=da>1
set ni[Q4[T]]=ZA
set oi[Q4[T]]=da
set pi[Q4[T]]=ea
set qi[Q4[T]]=ea/ ZA
set ri[Q4[T]]=fa
endfunction
function ha takes integer a,unit m,player p,integer Tv,integer fA returns integer
local integer Zq=rs()
set vi[Zq]=CreateUnit(p,1685417325,GetUnitX(m),GetUnitY(m),.0)
call SetUnitPathing(vi[Zq],false)
call UnitAddAbility(vi[Zq],Tv)
call UnitAddType(vi[Zq],f4)
call SetUnitUserData(vi[Zq],a)
set wi[Zq]=fA
call Ot("MinionDummy "+I2S(Zq)+" is created.")
return Zq
endfunction
function ia takes integer Zq,real x,real y returns nothing
call SetUnitX(vi[Zq],x)
call SetUnitY(vi[Zq],y)
endfunction
function ja takes integer Zq,real x,real y returns boolean
return IssuePointOrderById(vi[Zq],wi[Zq],x,y)
endfunction
function ka takes integer Zq returns nothing
call RemoveUnit(vi[Zq])
set vi[Zq]=null
call Ot("MinionDummy "+I2S(Zq)+" is deleted.")
endfunction
function ma takes integer Zq returns nothing
if Zq==null then
return
elseif(ui[Zq]!=-1)then
return
endif
call ka(Zq)
set ui[Zq]=si
set si=Zq
endfunction
function na takes integer Zq returns nothing
set Di[Zq]=CreateTimer()
call SaveInteger(n7,4,GetHandleId((Di[Zq])),(Zq))
endfunction
function oa takes integer Zq returns nothing
call ru(Di[Zq])
set Di[Zq]=null
endfunction
function pa takes nothing returns nothing
local integer Zq=(LoadInteger(n7,4,GetHandleId(GetExpiredTimer())))
call SetImagePosition(ci[Zq],GetUnitX(Ci[Zq]),GetUnitY(Ci[Zq]),.0)
endfunction
function qa takes unit u,real uu returns integer
local integer Zq=qs()
set Ci[Zq]=u
set ci[Zq]=tu(uu,GetUnitX(u),GetUnitY(u),3,Bi,51,51,bi)
call na(Zq)
call TimerStart(Di[Zq],Ai,true,function pa)
return Zq
endfunction
function ra takes integer Zq returns nothing
call oa(Zq)
call DestroyImage(ci[Zq])
set ci[Zq]=null
set Ci[Zq]=null
endfunction
function sa takes integer Zq returns nothing
if Zq==null then
return
elseif(zi[Zq]!=-1)then
return
endif
call ra(Zq)
set zi[Zq]=xi
set xi=Zq
endfunction
function ta takes integer Zq returns nothing
if Ki[Zq]==null then
set Ki[Zq]=CreateTimer()
call SaveInteger(n7,4,GetHandleId((Ki[Zq])),(Zq))
endif
endfunction
function ua takes integer Zq returns nothing
if Ki[Zq]!=null then
call ru(Ki[Zq])
set Ki[Zq]=null
endif
endfunction
function va takes nothing returns nothing
local integer Zq=(LoadInteger(n7,4,GetHandleId(GetExpiredTimer())))
set Ji[Zq]=not vk[li[Zq]]
call es(Zq)
endfunction
function wa takes integer Zq returns nothing
call ta(Zq)
if wk[li[Zq]]>.0 then
call TimerStart(Ki[Zq],wk[li[Zq]],vk[li[Zq]],function va)
else
set Ji[Zq]=true
endif
endfunction
function xa takes integer Zq,integer m,integer T returns nothing
set Yg[m]=Zq
set Ii[Zq]=m
set li[Zq]=wg[xg[Sg[m]]+T]
endfunction
function ya takes integer Zq returns nothing
set Ji[Zq]=false
call TimerStart(Ki[Zq],xk[li[Zq]],vk[li[Zq]],function va)
endfunction
function za takes integer Zq,integer gs,integer is,integer js returns boolean
if Ji[Zq]and not((Qe[Xg[(Ii[Zq])]]))and IsUnitInRange(Vg[gs],Vg[Ii[Zq]],yk[li[Zq]])then
return hs(Zq,gs,is,js)
endif
return false
endfunction
function Aa takes nothing returns nothing
local integer m=Hi
local integer Zq=Zs()
call xa(Zq,m,4)
call wa(Zq)
call Ot("AbilArcaneShield "+I2S(Zq)+" is created.")
endfunction
function aa takes nothing returns nothing
local integer m=Hi
local integer Zq=dt()
call xa(Zq,m,1)
call Ot("AbilFlexibility "+I2S(Zq)+" is created.")
endfunction
function Ba takes integer Zq returns nothing
set Mi[Zq]=not Mi[Zq]
endfunction
function ba takes nothing returns nothing
local integer m=Hi
local integer Zq=Yg[m]
if Mi[Zq]then
set Oi[Zq]=.1
set Ni[Zq]=0
else
set Oi[Zq]=.0
set Ni[Zq]=Li
endif
endfunction
function Ca takes nothing returns nothing
local integer m=Hi
local integer Zq=et()
call xa(Zq,m,2)
call wa(Zq)
call Ot("AbilShieldsUp "+I2S(Zq)+" is created.")
endfunction
function ca takes nothing returns nothing
local integer m=Hi
local integer Zq=ft()
call xa(Zq,m,5)
call wa(Zq)
set Ri[Zq]=qa(Vg[m],yk[li[Zq]])
call Ot("AbilInspiration "+I2S(Zq)+" is created.")
endfunction
function Da takes nothing returns nothing
local integer m=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),e4))and IsUnitType(Vg[m],UNIT_TYPE_GROUND)and Zg[m]and Sg[m]==Sg[Ti]and Qn[hh[m]]then
if Rg[m]!=3 then
call LA(m,Ti,Pi,.05)
elseif not Ui then
set Ui=m!=Ti
endif
endif
endfunction
function Ea takes nothing returns nothing
set Qi=Filter(function Da)
endfunction
function Fa takes nothing returns nothing
local integer m=Hi
local integer Zq=gt()
call xa(Zq,m,3)
call Ot("AbilCommander "+I2S(Zq)+" is created.")
endfunction
function Ga takes integer Zq,integer id returns nothing
if id==sk[li[Zq]]then
set Zi[Zq]=true
call UnitRemoveAbility(Vg[Ii[Zq]],id)
call UnitAddAbility(Vg[Ii[Zq]],1093677143)
call AddUnitAnimationProperties(Vg[Ii[Zq]],"Defend",true)
elseif id==1093677143 then
set Zi[Zq]=false
call UnitRemoveAbility(Vg[Ii[Zq]],id)
call UnitAddAbility(Vg[Ii[Zq]],sk[li[Zq]])
call AddUnitAnimationProperties(Vg[Ii[Zq]],"Defend",false)
endif
endfunction
function Ha takes nothing returns nothing
local integer m=Hi
local integer Zq=Yg[m]
if Zi[Zq]then
set dj[Zq]=Vi
if oo[po[hh[m]]+0]then
set ej[Zq]=25
endif
else
call wa(Zq)
set gj[Zq]=qa(Vg[m],yk[li[Zq]])
endif
endfunction
function Ia takes nothing returns nothing
local integer m=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),e4))and IsUnitType(Vg[m],UNIT_TYPE_GROUND)and Zg[m]and Sg[m]==hj and Qn[hh[m]]and Rg[m]!=5 then
set ij=ij+1
endif
endfunction
function la takes nothing returns nothing
local integer i=0
loop
set Wi[i]=3
set Xi[i]=30
set i=i+1
exitwhen i==V
endloop
set Yi=Filter(function Ia)
endfunction
function Ja takes nothing returns nothing
local integer i=0
loop
set kj[i]=.25
set i=i+1
exitwhen i==V
endloop
endfunction
function Ka takes nothing returns nothing
local integer m=Hi
local integer Zq=ht()
call xa(Zq,m,6)
call wa(Zq)
set mj[Zq]=ha(Zq,Vg[m],Bf[Tg[m]],1093677368,jj)
call Ot("AbilHealingWave "+I2S(Zq)+" is created.")
endfunction
function La takes nothing returns nothing
local integer i=bf[tn]
set kj[i]=kj[i]+.1
endfunction
function Ma takes nothing returns nothing
local integer m=Hi
local integer Zq=it()
call xa(Zq,m,7)
call wa(Zq)
call Ot("AbilDivineShield "+I2S(Zq)+" is created.")
endfunction
function Na takes nothing returns nothing
local integer m=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),e4))and Zg[m]and Sg[m]==oj and Qn[hh[m]]and not hz(m)and ny(m)<qj then
set qj=ny(m)
set pj=m
endif
endfunction
function Oa takes integer Zq returns integer
set oj=Sg[Ii[Zq]]
set pj=0
set qj=2.
call GroupEnumUnitsInRange(bj_lastCreatedGroup,GetUnitX(Vg[Ii[Zq]]),GetUnitY(Vg[Ii[Zq]]),512.,nj)
return pj
endfunction
function Pa takes nothing returns nothing
set nj=Filter(function Na)
endfunction
function Qa takes integer Zq returns nothing
set uj[Zq]=CreateTimer()
call SaveInteger(n7,4,GetHandleId((uj[Zq])),(Zq))
endfunction
function Ra takes integer Zq returns nothing
call ru(uj[Zq])
set uj[Zq]=null
endfunction
function Sa takes nothing returns nothing
local integer m=Hi
local integer Zq=jt()
call xa(Zq,m,8)
call wa(Zq)
call Qa(Zq)
call Ot("AbilResurrection "+I2S(Zq)+" is created.")
endfunction
function Ta takes nothing returns nothing
local integer m=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),e4))and ky(m)and Sg[m]==vj and wz(m)and Un[hh[m]]>xj then
set xj=Un[hh[m]]
set wj=m
endif
endfunction
function Ua takes integer Zq returns nothing
local integer Va=Ii[Zq]
set vj=Sg[Va]
set wj=0
set xj=.0
call GroupEnumUnitsInRange(bj_lastCreatedGroup,GetUnitX(Vg[Va]),GetUnitY(Vg[Va]),yk[li[Zq]],sj)
set Va=wj
if Va!=0 then
call zz(Va,rj[bf[Sg[Ii[Zq]]]])
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\Human\\Resurrect\\ResurrectTarget.mdx"),(Vg[Va]),("origin")))
if(H9[I9[(ag[Bg[Sg[Va]]+3])]+(1)])then
call WA(Va,Ii[Zq])
endif
call ya(Zq)
else
call TimerStart(uj[Zq],.25,false,tj)
endif
endfunction
function Wa takes nothing returns nothing
call Ua(((LoadInteger(n7,4,GetHandleId(GetExpiredTimer())))))
endfunction
function Xa takes nothing returns nothing
local integer i=0
loop
set rj[i]=.5
set i=i+1
exitwhen i==V
endloop
set sj=Filter(function Ta)
set tj=function Wa
endfunction
function Ya takes nothing returns nothing
local integer i=bf[tn]
set rj[i]=.75
endfunction
function Za takes integer Zq returns nothing
set Pj[Zq]=CreateTimer()
call SaveInteger(n7,4,GetHandleId((Pj[Zq])),(Zq))
endfunction
function dB takes integer Zq returns nothing
call ru(Pj[Zq])
set Pj[Zq]=null
endfunction
function eB takes integer f,real x,real y,boolean fB returns nothing
local integer c=Mv(f,x,y)
set fB=not fB
set Bd[(c)]=(fB)
set Bd[(Cd[c])]=(fB)
set Bd[(Dd[Cd[c]])]=(fB)
set Bd[(Dd[c])]=(fB)
endfunction
function gB takes unit u,integer p,real x,real y,integer T returns nothing
set aj=u
set Bj=p
set bj=x
set Cj=y
call TriggerEvaluate(qp[(T)])
endfunction
function hB takes unit u returns nothing
local real x=GetUnitX(u)
local real y=GetUnitY(u)
local integer p=(z4[GetPlayerId((GetOwningPlayer(u)))])
local integer f=If[p]
local integer T=GetUnitTypeId(u)-1949315120
if x>Ud[f]or x<Vd[f]or y>320. or y<k4 then
call sx((p),n8,Y9[T9]+"Error|r: "+("it is not allowed to build here."))
call RemoveUnit(u)
return
endif
if Tt(R2I(x-Vd[f]),$80)or Tt(R2I(y-k4),$80)then
call sx((p),n8,Y9[T9]+"Error|r: "+("towers must be build precisely on the grid."))
call RemoveUnit(u)
return
endif
if not ax(p,ep[(T)])then
call RemoveUnit(u)
return
endif
call eB(f,x,y,true)
call Nv(f)
if ie[f]then
call eB(f,x,y,false)
call sx((p),n8,Y9[T9]+"Error|r: "+("this tower will hinder minions' movement."))
call RemoveUnit(u)
return
endif
call gB(u,p,x,y,T)
endfunction
function iB takes integer Zq,integer T returns nothing
set Ej[Zq]=T
set Dj[Zq]=aj
set cj[Zq]=Bj
set lj[Zq]=bj
set Jj[Zq]=Cj
set Fj[Zq]=(T)
set Kj[Zq]=ep[Fj[Zq]]
set Nj[Zq]=(T)
call UnitAddAbility(Dj[Zq],Xo[Fj[Zq]])
call UnitAddAbility(Dj[Zq],Yo[Fj[Zq]])
call Ku(kg[cj[Zq]],Zq)
call SetUnitUserData(Dj[Zq],Zq)
endfunction
function jB takes integer Zq,real uu,integer vu,integer wu,integer xu,boolean kB returns image
call zu(Bf[cj[Zq]],uu,lj[Zq],Jj[Zq],2,vu,wu,xu,Y4)
call ShowImage(bj_lastCreatedImage,kB and IsUnitSelected(Dj[Zq],Bf[cj[Zq]]))
return bj_lastCreatedImage
endfunction
function mB takes integer Zq returns nothing
set Mj[Zq]=Ro[Nj[Zq]]
set Xj[Zq]=ev(Dj[Zq],Oo[Nj[Zq]])
set Yj[Zq]=ev(Dj[Zq],1227894833)
call Za(Zq)
set Oj[Zq]=Zz(Zq)
set Qj[Zq]=jB(Zq,Qo[Nj[Zq]],51,$CC,51,true)
endfunction
function nB takes integer Zq returns nothing
call dB(Zq)
call mA(Oj[Zq])
call DestroyImage(Qj[Zq])
set Qj[Zq]=null
set Xj[Zq]=null
set Yj[Zq]=null
endfunction
function oB takes integer Zq returns real
return .0
endfunction
function pB takes integer Zq returns real
return .0
endfunction
function qB takes integer Zq returns real
local real jw=Kr(Zq)+Lr(Zq)
return jw+jw*Mr(Zq)
endfunction
function rB takes integer Zq returns nothing
endfunction
function sB takes integer Zq,integer gs,real bs,integer js,boolean tB returns boolean
return oz(gs,Zq,bs,js,tB)
endfunction
function uB takes integer Zq returns nothing
call SetItemCharges(Xj[Zq],Vt(qB(Zq)))
endfunction
function vB takes integer Zq,integer m returns boolean
local real r=qB(Zq)
call SetItemCharges(Xj[Zq],Vt(r))
return sB(Zq,m,r,Oo[Nj[Zq]],So[Nj[Zq]])
endfunction
function wB takes integer Zq,integer m returns boolean
return sB(Zq,m,qB(Zq),Oo[Nj[Zq]],So[Nj[Zq]])
endfunction
function xB takes integer Zq returns nothing
call TimerStart(Pj[Zq],No[Nj[Zq]],false,gk)
endfunction
function yB takes integer Zq,integer Rr,integer is returns boolean
local integer zB=0
local integer m
loop
set m=rA(Rr)
call Nt(1,m,Zq,Oo[Nj[Zq]])
if(yf[Xg[(m)]])then
exitwhen(di[(Rr)]==0)
else
call jA(is,Vg[m])
set zB=zB+1
if(di[(Rr)]==0)or zB==Mj[Zq]then
return true
endif
endif
endloop
return false
endfunction
function AB takes integer Zq returns nothing
local integer Rr=xA(Zq)
if Rr!=0 then
call Qr(Zq,Rr)
call vA(Rr)
call Rs(Rr)
else
call TimerStart(Pj[(Zq)],O4,false,gk)
endif
endfunction
function aB takes nothing returns nothing
call AB(((LoadInteger(n7,4,GetHandleId(GetExpiredTimer())))))
endfunction
function BB takes integer Zq returns nothing
set Hj[Zq]=Vo[Fj[Zq]]*Wo[Fj[Zq]]
set Ij[Zq]=Wo[Fj[Zq]]
call SetUnitScale(Dj[Zq],Ij[Zq],Ij[Zq],Ij[Zq])
if Zo[Fj[Zq]]then
call mB(Zq)
endif
set Sj[Zq]=hp[Fj[Zq]]
if Sj[Zq]then
set Wj[Zq]=np[Fj[Zq]]
set Vj[Zq]=np[Fj[Zq]]
set Zj[Zq]=ev(Dj[Zq],1227894839)
if ip[Fj[Zq]]then
set dk[Zq]=ev(Dj[Zq],1227894838)
else
set dk[Zq]=ev(Dj[Zq],1227894842)
call UnitAddAbility(Dj[Zq],1093677129)
endif
call SetItemCharges(dk[Zq],Wj[Zq])
endif
if fp[Fj[Zq]]then
set Lj[Zq]=.5
call UnitAddAbility(Dj[Zq],1093677119)
else
set Lj[Zq]=.0
call UnitAddAbility(Dj[Zq],1093677121)
endif
set Bk=Zq
call TriggerEvaluate(dn[(dp[Fj[Zq]])])
set Gj[Zq]=true
if Zo[Fj[Zq]]then
call uB(Zq)
call AB(Zq)
endif
endfunction
function bB takes integer Zq returns nothing
call Lu(kg[cj[Zq]],Zq)
if Zo[Fj[Zq]]and Gj[Zq]then
call nB(Zq)
endif
if ek[Zq]!=0 then
call ur(ek[Zq])
endif
set Zj[Zq]=null
set dk[Zq]=null
call SetUnitUserData(Dj[Zq],0)
set Dj[Zq]=null
call Wr(Zq)
call Ot("Tower "+I2S(Zq)+" is deleted.")
endfunction
function CB takes integer Zq returns nothing
call RemoveUnit(Dj[Zq])
call bB(Zq)
endfunction
function cB takes integer Zq returns nothing
call DestroyEffect(AddSpecialEffect(("Objects\\Spawnmodels\\Human\\HCancelDeath\\HCancelDeath.mdx"),((lj[Zq])*1.),((Jj[Zq])*1.)))
if Lj[Zq]>.0 then
set Kj[Zq]=Vt(Kj[Zq]*Lj[Zq])
call yx(cj[Zq],Kj[Zq])
call Bx(cj[Zq],I2S(Kj[Zq]),Dj[Zq])
endif
call eB(If[cj[Zq]],lj[Zq],Jj[Zq],false)
call Nv(If[cj[Zq]])
call CB(Zq)
endfunction
function DB takes integer Zq returns nothing
local boolean Ux=not ip[Fj[Zq]]
set Uj[Zq]=Uj[Zq]+1
call SetItemCharges(Zj[Zq],Uj[Zq])
call sr(ek[Zq])
if Zo[Fj[Zq]]then
call Nr(Zq)
call uB(Zq)
endif
if Ux then
set Kj[Zq]=Kj[Zq]+Wj[Zq]
endif
if Uj[Zq]<pp[Fj[Zq]]then
if Uj[Zq]<op[Fj[Zq]]then
set Wj[Zq]=jp[Fj[Zq]]*Wj[Zq]+(Uj[Zq]+1)*kp[Fj[Zq]]+mp[Fj[Zq]]
endif
if Ux then
call SetItemCharges(dk[Zq],Wj[Zq])
else
set Vj[Zq]=Vj[Zq]+Wj[Zq]
endif
else
if Ux then
call UnitRemoveAbility(Dj[Zq],1093677129)
endif
call hv(Dj[Zq],dk[Zq])
set Sj[Zq]=false
set Wj[Zq]=0
set Vj[Zq]=0
endif
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\Items\\AIlm\\AIlmTarget.mdx"),(Dj[Zq]),("origin")))
endfunction
function EB takes integer Zq,integer bs returns nothing
set Tj[Zq]=Tj[Zq]+bs
loop
exitwhen Tj[Zq]<Vj[Zq]or(not Sj[Zq])
call DB(Zq)
endloop
if Sj[Zq]then
call SetItemCharges(dk[Zq],Vj[Zq]-Tj[Zq])
endif
endfunction
function FB takes nothing returns nothing
local integer t=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),Z))and Gj[t]and ip[Fj[t]]and Sj[t]then
call EB(t,ik)
endif
endfunction
function GB takes integer m,integer pz returns nothing
local real x=GetUnitX(Vg[m])
local real y=GetUnitY(Vg[m])
set ik=do[hh[m]]
if ip[Fj[pz]]and Sj[pz]and not IsUnitInRangeXY(Dj[pz],x,y,384.)then
call EB(pz,ik)
endif
call GroupEnumUnitsInRange(bj_lastCreatedGroup,x,y,384.,hk)
endfunction
function HB takes integer Zq,integer m returns nothing
set Rj[Zq]=Rj[Zq]+1
call SetItemCharges(Yj[Zq],Rj[Zq])
call vx(cj[Zq])
if do[hh[m]]>0 then
call GB(m,Zq)
endif
endfunction
function IB takes nothing returns boolean
local integer t=GetUnitUserData(GetFilterUnit())
return((IsUnitType(GetFilterUnit(),Z))and Gj[t]and not ip[Fj[t]]and Sj[t]and(Kf[(cj[t])]-(Wj[t])>=0))!=null
endfunction
function lB takes integer Zq returns nothing
local integer array JB
local integer KB
local integer LB
local integer n=-1
local integer i=-1
local group g=CreateGroup()
local unit u
call GroupEnumUnitsSelected(g,Bf[cj[Zq]],jk)
loop
set u=FirstOfGroup(g)
exitwhen u==null
call GroupRemoveUnit(g,u)
set KB=GetUnitUserData(u)
set n=n+1
set JB[n]=KB
loop
exitwhen i<0 or Wj[JB[i]]<=Wj[KB]
set LB=JB[i]
set JB[i]=KB
set JB[i+1]=LB
set i=i-1
endloop
set i=n
endloop
call DestroyGroup(g)
set g=null
if n>-1 then
loop
set KB=JB[n]
exitwhen not zx(cj[Zq],Wj[KB])
call DB(KB)
set n=n-1
exitwhen n==-1
endloop
else
call sx((cj[Zq]),o8,Y9[T9]+"Error|r: not enough Gold.")
endif
endfunction
function MB takes nothing returns nothing
set gk=function aB
set hk=Filter(function FB)
set jk=Filter(function IB)
endfunction
function NB takes integer Zq returns nothing
set Gh[Zq]=(Yv(Xg[(bh[Zq])],(Zq)))
endfunction
function OB takes integer m,integer cA returns nothing
local integer Zq
if(yf[Xg[(m)]])then
return
endif
set Zq=(LoadInteger(n7,Wg[(m)],(1)))
if Zq==0 then
set Zq=kt()
call Pz(Zq,m,Dj[cA],cj[cA],1)
call NB(Zq)
elseif Ch[Zq]!=Dj[cA]then
set Ch[Zq]=Dj[cA]
endif
call Yz(Zq,ni[Dh[Zq]])
endfunction
function PB takes integer Zq,real QB returns nothing
set Gh[Zq]=Qy(bh[Zq],Zq,QB)
endfunction
function RB takes integer m,integer cA,real QB returns nothing
local integer Zq
if xs(m)or(yf[Xg[(m)]])then
return
endif
set Zq=(LoadInteger(n7,Wg[(m)],(2)))
if Zq==0 then
set Zq=mt()
call Pz(Zq,m,Dj[cA],cj[cA],2)
call PB(Zq,QB)
elseif Ch[Zq]!=Dj[cA]then
set Ch[Zq]=Dj[cA]
endif
call Xz(Zq)
endfunction
function SB takes integer Zq,integer yy returns nothing
set Gh[Zq]=Uy(bh[Zq],Zq,yy)
endfunction
function TB takes integer m,integer cA,integer yy returns nothing
local integer Zq
if(yf[Xg[(m)]])then
return
endif
set Zq=(LoadInteger(n7,Wg[(m)],(3)))
if Zq==0 then
set Zq=nt()
call Pz(Zq,m,Dj[cA],cj[cA],3)
call SB(Zq,yy)
elseif Ch[Zq]!=Dj[cA]then
set Ch[Zq]=Dj[cA]
endif
call Xz(Zq)
endfunction
function UB takes integer Zq,real QB returns nothing
set Gh[Zq]=Qy(bh[Zq],Zq,QB)
endfunction
function VB takes integer m,integer cA,real QB returns nothing
local integer Zq
if xs(m)or(yf[Xg[(m)]])then
return
endif
set Zq=(LoadInteger(n7,Wg[(m)],(4)))
if Zq==0 then
set Zq=ot()
call Pz(Zq,m,Dj[cA],cj[cA],4)
call UB(Zq,QB)
elseif Ch[Zq]!=Dj[cA]then
set Ch[Zq]=Dj[cA]
endif
call Xz(Zq)
endfunction
function YB takes integer Zq,integer yy returns nothing
set Gh[Zq]=Uy(bh[Zq],Zq,yy)
endfunction
function ZB takes integer m,integer cA,integer yy returns nothing
local integer Zq=(LoadInteger(n7,Wg[(m)],(6)))
if Zq==0 then
set Zq=qt()
call Pz(Zq,m,Dj[cA],cj[cA],6)
call YB(Zq,yy)
elseif Ch[Zq]!=Dj[cA]then
set Ch[Zq]=Dj[cA]
endif
call Xz(Zq)
endfunction
function db takes integer Zq,real GA returns nothing
set Gh[Zq]=Py(bh[Zq],Zq,GA)
endfunction
function eb takes integer m,integer cA,real GA returns nothing
local integer Zq=(LoadInteger(n7,Wg[(m)],(V4)))
if Zq==0 then
set Zq=rt()
call Pz(Zq,m,Dj[cA],cj[cA],V4)
call db(Zq,GA)
elseif Ch[Zq]!=Dj[cA]then
set Ch[Zq]=Dj[cA]
endif
call Xz(Zq)
endfunction
function fb takes nothing returns nothing
local integer Zq=tt()
call Xx(Zq,4,Pg)
call Ot("Wizard "+I2S(Zq)+" is created.")
endfunction
function gb takes nothing returns nothing
local integer d=yg[zg[Qg]+9]
set pi[d]=pi[d]+.5
set qi[d]=pi[d]
endfunction
function hb takes integer m,integer p returns integer
local integer Zq=ut()
set Sg[Zq]=p
set Vg[Zq]=CreateUnit(Bf[p],Ln[g7[1]],mu(Wd[If[p]]),nu(Wd[If[p]]),270.)
call UnitAddAbility(Vg[Zq],1098282348)
call UnitMakeAbilityPermanent(Vg[Zq],true,1098282348)
call SetUnitUserData(Vg[Zq],Zq)
set Rg[Zq]=1
set Wg[Zq]=GetHandleId(Vg[Zq])
set hh[Zq]=sg[tg[p]+1]
call UnitAddAbility(Vg[Zq],1093677099)
call UnitMakeAbilityPermanent(Vg[Zq],true,1093677099)
call UnitAddAbility(Vg[Zq],Nn[g7[1]])
call UnitMakeAbilityPermanent(Vg[Zq],true,Nn[g7[1]])
call UnitAddAbility(Vg[Zq],xo[yo[i7[1]]])
call UnitMakeAbilityPermanent(Vg[Zq],true,xo[yo[i7[1]]])
call UnitAddAbility(Vg[Zq],xo[yo[i7[1]]+1])
call UnitMakeAbilityPermanent(Vg[Zq],true,xo[yo[i7[1]]+1])
call UnitAddAbility(Vg[Zq],xo[yo[i7[1]]+2])
call UnitMakeAbilityPermanent(Vg[Zq],true,xo[yo[i7[1]]+2])
set Hi=Zq
call TriggerEvaluate(d7[Sn[hh[Zq]]])
call UnitAddAbility(Vg[Zq],1093677122)
call UnitMakeAbilityPermanent(Vg[Zq],true,1093677122)
call UnitAddAbility(Vg[Zq],sk[Z4[Sn[hh[Zq]]]])
call UnitMakeAbilityPermanent(Vg[Zq],true,sk[Z4[Sn[hh[Zq]]]])
call Mu(mg[p],Zq)
set mk[Zq]=m
call Ot("ConscriptTwin "+I2S(Zq)+" is created.")
return Zq
endfunction
function ib takes nothing returns nothing
local integer Zq=ut()
call Xx(Zq,1,Pg)
if oo[po[hh[Zq]]+2]then
set mk[Zq]=hb(Zq,Sg[Zq])
else
set mk[Zq]=0
endif
call Ot("Conscript "+I2S(Zq)+" is created.")
endfunction
function jb takes integer Zq returns nothing
call Nu(mg[Sg[Zq]],Zq)
call Fx(Sg[Zq],Un[hh[Zq]])
call DestroyEffect(AddSpecialEffect(("Abilities\\Spells\\Other\\Transmute\\PileofGold.mdx"),((GetUnitX(Vg[Zq]))*1.),((GetUnitY(Vg[Zq]))*1.)))
call RemoveUnit(Vg[Zq])
call Gs(Zq)
call Ot("ConscriptTwin "+I2S(Zq)+" is sold.")
endfunction
function kb takes nothing returns nothing
call Kz(Qg,1,.1)
endfunction
function mb takes nothing returns nothing
call Mz(Qg,1,kk)
endfunction
function nb takes nothing returns nothing
local integer ob=Qg
local integer pb=h9[mg[ob]]
local real qb=Un[sg[tg[ob]+1]]-.5
local integer a=0
local integer m
set Un[sg[tg[ob]+1]]=.5
loop
exitwhen a>pb
set m=(f9[g9[(mg[ob])]+(a)])
if Rg[m]==1 then
set mk[m]=hb(m,ob)
endif
set a=a+1
endloop
set a=0
set pb=D9[pg[ob]]
loop
exitwhen a>pb
set m=(C9[c9[(pg[ob])]+(a)])
if Rg[m]==1 and Zg[m]then
call Fx(ob,qb)
endif
set a=a+1
endloop
set a=0
set pb=o9[ng[Df[ob]]]
loop
exitwhen a>pb
set m=(m9[n9[(ng[Df[ob]])]+(a)])
if Rg[m]==1 then
call Fx(ob,qb)
endif
set a=a+1
endloop
endfunction
function rb takes nothing returns nothing
local integer Zq=vt()
call Xx(Zq,2,Pg)
call Ot("Footman "+I2S(Zq)+" is created.")
endfunction
function sb takes nothing returns nothing
call Mz(Qg,2,30)
endfunction
function tb takes nothing returns nothing
local integer Zq=wt()
call Xx(Zq,3,Pg)
call Ot("Knight "+I2S(Zq)+" is created.")
endfunction
function ub takes nothing returns nothing
call Iz(Qg,3,105.)
endfunction
function vb takes nothing returns nothing
call Lz(Qg,3,ok)
endfunction
function wb takes nothing returns nothing
local integer d=yg[zg[Qg]+R4]
set pi[d]=1.
set qi[d]=1.
endfunction
function xb takes nothing returns nothing
local integer Zq=xt()
call Xx(Zq,5,Pg)
call Ot("Captain "+I2S(Zq)+" is created.")
endfunction
function yb takes nothing returns nothing
local integer ob=Qg
local integer a=0
local integer m
local integer b
loop
exitwhen a>D9[pg[ob]]
set m=(C9[c9[(pg[ob])]+(a)])
set b=Yg[m]
if Rg[m]==5 and Zi[b]then
set ej[b]=25
if Zg[m]then
call Fy(m)
endif
endif
set a=a+1
endloop
endfunction
function zb takes nothing returns nothing
local integer i=bf[Qg]
set Wi[i]=5
set Xi[i]=50
endfunction
function Ab takes nothing returns nothing
local integer Zq=yt()
call Yx(Zq,6,Pg)
call Ot("Hero Paladin "+I2S(Zq)+" is created.")
endfunction
function ab takes nothing returns nothing
call Iz(Qg,6,1.*go[sg[tg[Qg]+6]])
endfunction
function Bb takes nothing returns nothing
call Lz(Qg,6,25)
endfunction
function bb takes nothing returns nothing
call Jz(Qg,6,1.*ho[sg[tg[Qg]+6]])
endfunction
function Cb takes integer T,integer cb,boolean Db,boolean Eb,boolean Fb,real Gb,real cd,real uu returns nothing
set Z4[T]=vr()
set sk[Z4[T]]=cb
set tk[Z4[T]]=Db
set uk[Z4[T]]=Eb
set vk[Z4[T]]=Fb
set wk[Z4[T]]=Gb
if Fb then
set xk[Z4[T]]=Gb
else
set xk[Z4[T]]=cd
endif
set yk[Z4[T]]=uu
endfunction
function Hb takes integer T,code Ib,code lb returns nothing
if Ib!=null then
set d7[T]=CreateTrigger()
call TriggerAddCondition(d7[T],Filter(Ib))
endif
if lb!=null then
set e7[T]=CreateTrigger()
call TriggerAddCondition(e7[T],Filter(lb))
endif
endfunction
function Kb takes integer Zq returns nothing
set Jk[Zq]=CreateTimer()
call SaveInteger(n7,4,GetHandleId((Jk[Zq])),(Zq))
endfunction
function Lb takes integer Zq returns nothing
call ru(Jk[Zq])
set Jk[Zq]=null
endfunction
function Mb takes nothing returns nothing
local integer m=Bk
local integer Zq=zt(m,1)
set Ek[Zq]=ev(Dj[m],1227894835)
set lk[Zq]=Oj[m]
call Kb(Zq)
call dA(lk[(Zq)],1093677096,ck)
call Ot("AbilFocusedAttack "+I2S(Zq)+" is created.")
endfunction
function Nb takes integer Zq returns real
if Gk[Zq]<=.0 then
return Hk[Zq]
endif
return .0
endfunction
function Ob takes integer Zq,real r returns nothing
set Gk[Zq]=Gk[Zq]-r
if Gk[Zq]<=.0 then
set Gk[Zq]=.0
call dA(lk[(Zq)],1093677096,ck)
call uB(Ck[Zq])
call PauseTimer(Jk[Zq])
endif
call SetItemCharges(Ek[Zq],Wt(Gk[Zq]))
endfunction
function Pb takes nothing returns nothing
call Ob(((LoadInteger(n7,4,GetHandleId(GetExpiredTimer())))),.2)
endfunction
function Qb takes integer Zq returns nothing
if Gk[Zq]<=.0 then
set Gk[Zq]=Fk[Zq]
call uB(Ck[Zq])
call SetItemCharges(Ek[Zq],Wt(Gk[Zq]))
call TimerStart(Jk[Zq],.2,true,Dk)
else
call Ob(Zq,Ik[Zq])
endif
endfunction
function Rb takes integer Zq,integer id returns nothing
if id==1093677096 then
call gA(Oj[Ck[Zq]])
endif
endfunction
function Sb takes nothing returns nothing
set Dk=function Pb
endfunction
function Tb takes nothing returns nothing
local integer m=Bk
local integer Zq=At(m,2)
call Ot("AbilAcid "+I2S(Zq)+" is created.")
endfunction
function Ub takes integer Zq,integer m returns nothing
local integer cz=Uj[Ck[Zq]]
call OB(m,Ck[Zq])
if cz>=Xm[bk[Zq]]then
call RB(m,Ck[Zq],Kk)
endif
if cz>=Ym[bk[Zq]]then
call TB(m,Ck[Zq],Lk)
endif
endfunction
function Vb takes nothing returns nothing
local integer m=Bk
local integer Zq=at(m,3)
call Ot("AbilLightningAttack "+I2S(Zq)+" is created.")
endfunction
function Wb takes integer Zq,integer Rr returns boolean
if yB(Ck[Zq],Rr,Oj[Ck[Zq]])then
if Uj[Ck[Zq]]>=Ym[bk[Zq]]and(di[(Rr)]!=0)then
call yB(Ck[Zq],Rr,Ok[Zq])
endif
return true
endif
return false
endfunction
function Xb takes integer Zq returns nothing
set Wk[Zq]=CreateTimer()
call SaveInteger(n7,4,GetHandleId((Wk[Zq])),(Zq))
endfunction
function Yb takes integer Zq returns nothing
call ru(Wk[Zq])
set Wk[Zq]=null
endfunction
function Zb takes nothing returns nothing
local integer m=Bk
local integer Zq=Bt(m,4)
set Qk[Zq]=CreateUnit(Bf[cj[m]],1932537904,lj[m],Jj[m],.0)
call SetUnitPathing(Qk[Zq],false)
call SetUnitFlyHeight(Qk[Zq],Hj[m],.0)
call SetUnitUserData(Qk[Zq],m)
call Xb(Zq)
set Vk[Zq]=ev(Dj[m],1227894845)
call Ot("AbilEnhancedWeapon "+I2S(Zq)+" is created.")
endfunction
function dC takes nothing returns nothing
local integer Zq=(LoadInteger(n7,4,GetHandleId(GetExpiredTimer())))
set Tk[Zq]=.0
set Rk[Zq]=null
call uB(Ck[Zq])
endfunction
function eC takes integer Zq,integer m returns nothing
if Uj[Ck[Zq]]>=Xm[bk[Zq]]and Rk[Zq]==Vg[m]then
set Tk[Zq]=Tk[Zq]+.25
if Tk[Zq]>1. then
set Tk[Zq]=1.
endif
call TimerStart(Wk[Zq],3.,false,function dC)
else
set Tk[Zq]=.0
endif
set Rk[Zq]=Vg[m]
call vB(Ck[Zq],m)
endfunction
function fC takes nothing returns nothing
local integer Zq=(LoadInteger(n7,4,GetHandleId(GetExpiredTimer())))
if UnitAlive(Rk[Zq])then
call jA(Pk[Zq],Rk[Zq])
set Uk[Zq]=Uk[Zq]+1
if Uk[Zq]==4 then
call PauseTimer(Wk[Zq])
endif
else
call PauseTimer(Wk[Zq])
endif
endfunction
function gC takes integer Zq,integer m returns nothing
set Rk[Zq]=Vg[m]
call jA(Pk[Zq],Rk[Zq])
set Uk[Zq]=1
call TimerStart(Wk[Zq],.125,true,function fC)
endfunction
function hC takes integer Zq,integer m returns nothing
if Sk[Zq]then
call gC(Zq,m)
else
call eC(Zq,m)
endif
endfunction
function s__AbilEnhancedWeapon_filterToggleModeSelected takes nothing returns nothing
endfunction
function iC takes integer Zq returns nothing
set Zk[Zq]=CreateTimer()
call SaveInteger(n7,4,GetHandleId((Zk[Zq])),(Zq))
endfunction
function jC takes integer Zq returns nothing
call ru(Zk[Zq])
set Zk[Zq]=null
endfunction
function kC takes nothing returns nothing
local integer Zq=(LoadInteger(n7,4,GetHandleId(GetExpiredTimer())))
call yx(Yk[Zq],Xk[Zq])
call Bx(Yk[Zq],I2S(Xk[Zq]),Dj[Ck[Zq]])
endfunction
function mC takes nothing returns nothing
local integer m=Bk
local integer Zq=bt(m,5)
set Yk[Zq]=cj[m]
call iC(Zq)
call TimerStart(Zk[Zq],1.,true,function kC)
call Ot("AbilGoldMining "+I2S(Zq)+" is created.")
endfunction
function nC takes integer Zq returns nothing
set um[Zq]=CreateTimer()
call SaveInteger(n7,4,GetHandleId((um[Zq])),(Zq))
endfunction
function oC takes integer Zq returns nothing
call ru(um[Zq])
set um[Zq]=null
endfunction
function pC takes nothing returns nothing
local integer m=Bk
local integer Zq=Ct(m,6)
call nC(Zq)
set mm[Zq]=jB(Ck[Zq],km[Zq],dm,em,fm,false)
set tm[Zq]=cj[m]
call UnitAddAbility(Dj[Ck[Zq]],1093677127)
call TimerStart(um[Zq],5.,true,gm)
call Ot("AbilExpGenerator "+I2S(Zq)+" is created.")
endfunction
function qC takes integer Zq returns nothing
if qm[Zq]==null then
set qm[Zq]=ou(Bf[tm[Zq]],"Abilities\\Spells\\Other\\Aneu\\AneuCaster.mdx",pm[Zq],"overhead")
endif
endfunction
function rC takes integer Zq returns nothing
if qm[Zq]!=null then
call DestroyEffect(qm[Zq])
set qm[Zq]=null
endif
endfunction
function sC takes integer Zq,integer Av returns nothing
if im[Zq]==0 then
call ShowImage(mm[Zq],false)
call UnitRemoveAbility(Dj[Ck[Zq]],1093677106)
elseif im[Zq]==2 then
call UnitRemoveAbility(Dj[Ck[Zq]],1093677111)
else
set pm[Zq]=null
call rC(Zq)
call UnitRemoveAbility(Dj[Ck[Zq]],1093677138)
endif
if Av==0 then
call ShowImage(mm[Zq],IsUnitSelected(Dj[Ck[Zq]],Bf[tm[Zq]]))
call UnitAddAbility(Dj[Ck[Zq]],1093677106)
elseif Av==2 then
call UnitAddAbility(Dj[Ck[Zq]],1093677111)
else
set rm[Zq]=im[Zq]
call UnitAddAbility(Dj[Ck[Zq]],1093677138)
endif
set im[Zq]=Av
endfunction
function tC takes integer Zq,integer t returns nothing
if pm[Zq]!=Dj[t]then
if im[Zq]==1 then
call DestroyEffect(qm[Zq])
else
call sC(Zq,1)
endif
set pm[Zq]=Dj[t]
set qm[Zq]=ou(Bf[tm[Zq]],"Abilities\\Spells\\Other\\Aneu\\AneuCaster.mdx",Dj[t],"overhead")
endif
endfunction
function uC takes nothing returns nothing
local integer t=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),Z))and Gj[t]and dq[ek[t]]==91 then
call tC(ek[t],wm)
endif
endfunction
function vC takes integer Zq,integer t returns nothing
if not Gj[t]then
call sx((tm[Zq]),n8,Y9[T9]+"Error|r: "+("this tower is constructing."))
return
elseif not ip[Fj[t]]then
call sx((tm[Zq]),n8,Y9[T9]+"Error|r: "+("this tower does not use experience."))
return
elseif not Sj[t]then
call sx((tm[Zq]),n8,Y9[T9]+"Error|r: "+("this tower can no longer be upgraded."))
return
endif
set wm=t
call GroupEnumUnitsSelected(bj_lastCreatedGroup,Bf[tm[Zq]],vm)
endfunction
function wC takes nothing returns nothing
local integer t=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),Z))and Gj[t]and dq[ek[t]]==91 and im[(ek[t])]==ym then
call sC((ek[t]),zm)
endif
endfunction
function xC takes integer Zq,integer id returns nothing
if id==1093677106 then
set zm=2
elseif id==1093677111 then
set zm=0
elseif id==1093677138 then
set zm=rm[Zq]
else
return
endif
set ym=im[Zq]
call GroupEnumUnitsSelected(bj_lastCreatedGroup,Bf[tm[Zq]],xm)
endfunction
function yC takes integer Zq returns nothing
call bx(tm[Zq],sm[Zq])
call Ex(tm[Zq],I2S(sm[Zq]),Dj[Ck[Zq]])
endfunction
function zC takes nothing returns nothing
local integer t=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),Z))and Gj[t]and ip[Fj[t]]and Sj[t]then
call EB(t,R2I(am*Wj[t]))
set Bm=false
endif
endfunction
function AC takes integer Zq returns nothing
set am=jm[Zq]
set Bm=true
call GroupEnumUnitsInRange(bj_lastCreatedGroup,lj[Ck[Zq]],Jj[Ck[Zq]],km[Zq],Am)
if Bm then
call sC(Zq,2)
call yC(Zq)
endif
endfunction
function aC takes integer Zq returns nothing
local integer t=GetUnitUserData(pm[Zq])
call EB(t,Pt(nm[Zq],R2I(om[Zq]*Wj[t])))
endfunction
function BC takes integer Zq returns nothing
if im[Zq]==0 then
call ShowImage(mm[Zq],true)
elseif im[Zq]==1 and UnitAlive(pm[Zq])then
call qC(Zq)
endif
endfunction
function bC takes integer Zq returns nothing
if im[Zq]==0 then
call ShowImage(mm[Zq],false)
elseif im[Zq]==1 and UnitAlive(pm[Zq])then
call rC(Zq)
endif
endfunction
function CC takes integer Zq returns boolean
if UnitAlive(pm[Zq])and Sj[(GetUnitUserData(pm[Zq]))]then
return true
endif
call sC(Zq,rm[Zq])
return false
endfunction
function cC takes integer Zq returns nothing
if im[Zq]==1 and CC(Zq)then
call aC(Zq)
elseif im[Zq]==0 then
call AC(Zq)
else
call yC(Zq)
endif
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\NightElf\\MoonWell\\MoonWellCasterArt.mdx"),(Dj[Ck[Zq]]),("origin")))
endfunction
function DC takes nothing returns nothing
call cC(((LoadInteger(n7,4,GetHandleId(GetExpiredTimer())))))
endfunction
function EC takes nothing returns nothing
local integer Zq=(LoadInteger(n7,4,GetHandleId(GetExpiredTimer())))
call cC(Zq)
if Uj[Ck[Zq]]>=Zm[bk[Zq]]then
call TimerStart(um[Zq],3.,true,hm)
endif
endfunction
function FC takes nothing returns nothing
set gm=function EC
set hm=function DC
set Am=Filter(function zC)
set vm=Filter(function uC)
set xm=Filter(function wC)
endfunction
function GC takes nothing returns nothing
local integer m=Bk
local integer Zq=ct(m,7)
call Ot("AbilBloodlust "+I2S(Zq)+" is created.")
endfunction
function HC takes integer Zq returns nothing
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\Undead\\DeathPact\\DeathPactTarget.mdx"),(Dj[Ck[Zq]]),("overhead")))
set cm[Zq]=cm[Zq]+.5
if Rj[Ck[Zq]]==Xm[bk[Zq]]then
set cm[Zq]=cm[Zq]+5.
elseif Rj[Ck[Zq]]==Ym[bk[Zq]]then
set bm[Zq]=2.
endif
endfunction
function IC takes integer Zq,integer m returns nothing
set Cm[Zq]=(1.-ny(m))*bm[Zq]
if wB(Ck[Zq],m)then
set Cm[Zq]=.0
else
set Cm[Zq]=.0
call uB(Ck[Zq])
endif
endfunction
function lC takes integer Zq returns nothing
set Im[Zq]=CreateTimer()
call SaveInteger(n7,4,GetHandleId((Im[Zq])),(Zq))
endfunction
function JC takes integer Zq returns nothing
call ru(Im[Zq])
set Im[Zq]=null
endfunction
function KC takes integer Zq returns nothing
set lm[Zq]=CreateTimer()
call SaveInteger(n7,4,GetHandleId((lm[Zq])),(Zq))
endfunction
function LC takes integer Zq returns nothing
call ru(lm[Zq])
set lm[Zq]=null
endfunction
function MC takes nothing returns nothing
local integer m=Bk
local integer Zq=Dt(m,8)
call lC(Zq)
call KC(Zq)
call Ot("AbilShockwave "+I2S(Zq)+" is created.")
endfunction
function NC takes integer Zq returns nothing
if Uj[Ck[Zq]]>=Xm[bk[Zq]]then
set Hm[Zq]=Hm[Zq]+.1
if Hm[Zq]>1. then
set Hm[Zq]=1.
endif
call TimerStart(Im[Zq],1.5,false,Dm)
call uB(Ck[Zq])
endif
endfunction
function OC takes integer Zq,real cx,real cy returns nothing
if Hm[Zq]!=.0 then
set Hm[Zq]=.0
call uB(Ck[Zq])
endif
call iA(Oj[Ck[Zq]],cx,cy)
if Uj[Ck[Zq]]>=Ym[bk[Zq]]then
set Fm[Zq]=cx
set Gm[Zq]=cy
call TimerStart(lm[Zq],.4,false,Em)
endif
endfunction
function PC takes nothing returns nothing
local integer Zq=(LoadInteger(n7,4,GetHandleId(GetExpiredTimer())))
call iA(Oj[Ck[Zq]],Fm[Zq],Gm[Zq])
endfunction
function QC takes nothing returns nothing
local integer Zq=(LoadInteger(n7,4,GetHandleId(GetExpiredTimer())))
set Hm[Zq]=.0
call uB(Ck[Zq])
endfunction
function RC takes nothing returns nothing
set Dm=function QC
set Em=function PC
endfunction
function SC takes integer Zq returns nothing
set Vm[Zq]=CreateTimer()
call SaveInteger(n7,4,GetHandleId((Vm[Zq])),(Zq))
endfunction
function TC takes integer Zq returns nothing
call ru(Vm[Zq])
set Vm[Zq]=null
endfunction
function UC takes integer Zq,integer m returns nothing
local integer cz=Uj[Ck[Zq]]
set Om[Zq]=Om[Zq]-30
call SetItemCharges(Tm[Zq],Om[Zq])
call vy(m,Pm[Zq],"Effects\\RestoreHealthGreen.mdx","origin")
call DestroyEffect(AddSpecialEffectTarget(("Abilities\\Spells\\NightElf\\MoonWell\\MoonWellCasterArt.mdx"),(Dj[Ck[Zq]]),("origin")))
if cz>=Xm[bk[Zq]]then
call ZB(m,Ck[Zq],25)
if cz>=Ym[bk[Zq]]then
call eb(m,Ck[Zq],.1*fh[m])
endif
endif
endfunction
function VC takes integer Zq returns nothing
local integer a=0
local real WC=.0
local real jw=.0
local integer gs=0
local boolean XC=true
local integer YC=pg[Sm[Zq]]
loop
exitwhen a>u9[YC]
if Zg[(s9[t9[(YC)]+(a)])]then
set jw=ny((s9[t9[(YC)]+(a)]))
if XC or jw<WC then
set WC=jw
set gs=(s9[t9[(YC)]+(a)])
set XC=false
endif
endif
set a=a+1
endloop
if gs!=0 and WC<=Qm[Zq]then
call UC(Zq,gs)
endif
endfunction
function ZC takes nothing returns nothing
local integer Zq=(LoadInteger(n7,4,GetHandleId(GetExpiredTimer())))
local integer jw=Om[Zq]
set Um[Zq]=Um[Zq]+1
if Um[Zq]==3 then
set Um[Zq]=0
if Om[Zq]<50 and Cx(Sm[Zq],Lm)then
set Om[Zq]=Om[Zq]+Lm
if Om[Zq]>50 then
set Om[Zq]=50
endif
set jw=Om[Zq]-jw
call SetItemCharges(Tm[Zq],Om[Zq])
call Ex(Sm[Zq],I2S(jw),Dj[Ck[Zq]])
endif
endif
if Om[Zq]>=30 and Rm[Zq]then
call VC(Zq)
endif
endfunction
function dc takes nothing returns nothing
local integer m=Bk
local integer Zq=Et(m,9)
set Sm[Zq]=cj[m]
set Tm[Zq]=ev(Dj[m],1227894843)
call SC(Zq)
call TimerStart(Vm[Zq],1.,true,function ZC)
call Ot("AbilReplenishLife "+I2S(Zq)+" is created.")
endfunction
function ec takes integer Zq,integer fc returns nothing
if fc==Jm or fc==Km then
set Rm[Zq]=not Rm[Zq]
endif
endfunction
function gc takes integer T,integer hc,integer b1,integer b2,integer b3,code Ib returns nothing
set Wm[(T)]=hc
set Xm[(T)]=b1
set Ym[(T)]=b2
set Zm[(T)]=b3
set dn[(T)]=CreateTrigger()
call TriggerAddCondition(dn[(T)],Filter(Ib))
endfunction
function jc takes nothing returns nothing
local integer Zq=Ft()
call iB(Zq,0)
call UnitAddAbility(Dj[Zq],1093677101)
call Ot("Wall "+I2S(Zq)+" is created.")
endfunction
function kc takes unit u returns nothing
local integer Zq=GetUnitUserData(u)
if ax(cj[Zq],ep[(en[Zq])])then
set Gj[Zq]=false
else
call DisableTrigger(Y7)
call IssueImmediateOrderById(Dj[Zq],J)
call EnableTrigger(Y7)
endif
endfunction
function mc takes unit u returns nothing
local integer Zq=GetUnitUserData(u)
local integer jw=ep[(en[Zq])]
set Gj[Zq]=true
call yx(cj[Zq],jw)
call Bx(cj[Zq],I2S(jw),Dj[Zq])
endfunction
function nc takes unit u returns nothing
local integer Zq=GetUnitUserData(u)
local integer p=cj[Zq]
local real x=lj[Zq]
local real y=Jj[Zq]
local integer T=en[Zq]
call yx(cj[Zq],Kj[Zq])
call Bx(cj[Zq],I2S(Kj[Zq]),Dj[Zq])
call bB(Zq)
call gB(u,p,x,y,T)
call BB((GetUnitUserData(u)))
endfunction
function oc takes nothing returns nothing
local integer Zq=Gt()
call iB(Zq,1)
call Ot("Arrow "+I2S(Zq)+" is created.")
endfunction
function qc takes nothing returns nothing
local integer Zq=Ht()
call iB(Zq,2)
call Ot("Multishot "+I2S(Zq)+" is created.")
endfunction
function rc takes nothing returns nothing
local integer Zq=It()
call iB(Zq,3)
call Ot("Lightning "+I2S(Zq)+" is created.")
endfunction
function sc takes nothing returns nothing
local integer Zq=lt()
call iB(Zq,4)
call Ot("Burst "+I2S(Zq)+" is created.")
endfunction
function tc takes nothing returns nothing
local integer Zq=Jt()
call iB(Zq,5)
call Ot("GoldMine "+I2S(Zq)+" is created.")
endfunction
function uc takes nothing returns nothing
local integer Zq=Kt()
call iB(Zq,7)
call Ot("FoE "+I2S(Zq)+" is created.")
endfunction
function vc takes nothing returns nothing
local integer Zq=Lt()
call iB(Zq,8)
call Ot("Blood "+I2S(Zq)+" is created.")
endfunction
function wc takes nothing returns nothing
local integer Zq=Mt()
call iB(Zq,9)
call Ot("Wave "+I2S(Zq)+" is created.")
endfunction
function xc takes nothing returns nothing
local integer Zq=Vs()
call iB(Zq,6)
call Ot("FoL "+I2S(Zq)+" is created.")
endfunction
function yc takes integer T,integer zc,integer Ac,integer ac,integer Bc,string uv returns nothing
set f7[T]=fr()
set un[f7[T]]=zc
set vn[f7[T]]=Ac
set wn[f7[T]]=ac
set xn[f7[T]]=Bc
set yn[f7[T]]=sk[Z4[zc]]
set zn[f7[T]]=uv
endfunction
function bc takes integer T,integer tv,integer Cc,integer cc,integer Dc,integer Ec,code Fc,integer Gc,integer Hc,integer Ic,integer lc,integer Jc,code Kc returns nothing
set An[an[f7[T]]]=tv
set Bn[bn[f7[T]]]=cc
set Cn[cn[f7[T]]]=Cc
set Dn[En[f7[T]]]=Dc
set Fn[Gn[f7[T]]]=Ec
if Fc!=null then
set Hn[In[f7[T]]]=CreateTrigger()
call TriggerAddAction(Hn[In[f7[T]]],Fc)
endif
set An[an[f7[T]]+1]=Gc
set Bn[bn[f7[T]]+1]=Ic
set Cn[cn[f7[T]]+1]=Hc
set Dn[En[f7[T]]+1]=lc
set Fn[Gn[f7[T]]+1]=Jc
if Kc!=null then
set Hn[In[f7[T]]+1]=CreateTrigger()
call TriggerAddAction(Hn[In[f7[T]]+1],Kc)
endif
endfunction
function Mc takes integer p,integer id returns boolean
local string Qx=GetObjectName(id)
local integer T
local integer Vx
if SubString(Qx,0,jn)=="AbilityUpgrade" then
set T=S2I(SubString(Qx,kn,mn))
set Vx=S2I(SubString(Qx,nn,on))
set Qx=SubString(Qx,pn,StringLength(Qx))
if Dx(p,Fn[Gn[f7[T]]+Vx])then
set tn=p
set H9[I9[(ag[Bg[p]+T])]+(Vx)]=(true)
call TriggerExecute(Hn[In[f7[T]]+Vx])
call SetPlayerAbilityAvailable(Bf[p],id,false)
call SetPlayerAbilityAvailable(Bf[p],Bn[bn[f7[T]]+Vx],true)
call SetPlayerTechResearched(Bf[p],Dn[En[f7[T]]+Vx],1)
call sx((p),p8,Y9[R9]+"Research complete|r: "+(Qx)+".")
return true
endif
endif
return false
endfunction
function Nc takes integer Zq,integer p returns nothing
call UnitAddAbility(ig[p],vn[Zq])
call UnitAddAbility(ig[p],wn[Zq])
call UnitAddAbility(ig[p],An[an[Zq]])
call UnitAddAbility(ig[p],Bn[bn[Zq]])
call UnitAddAbility(ig[p],An[an[Zq]+1])
call UnitAddAbility(ig[p],Bn[bn[Zq]+1])
call UnitAddAbility(fg[p],xn[Zq])
endfunction
function Oc takes integer Zq,player p,boolean Pc returns nothing
call SetPlayerAbilityAvailable(p,yn[Zq],Pc)
call SetPlayerAbilityAvailable(p,xn[Zq],Pc)
call SetPlayerAbilityAvailable(p,Cn[cn[Zq]],Pc)
call SetPlayerAbilityAvailable(p,Cn[cn[Zq]+1],Pc)
call SetPlayerAbilityAvailable(p,wn[Zq],Pc)
call SetPlayerAbilityAvailable(p,vn[Zq],not Pc)
endfunction
function Qc takes integer Zq,integer p returns nothing
call Oc(bg[p],Bf[p],false)
call Oc(Zq,Bf[p],true)
call px(p,Zq)
endfunction
function Rc takes integer p,integer id returns boolean
local string Qx=GetObjectName(id)
local integer T
if SubString(Qx,0,qn)=="HeroAbility" then
set T=S2I(SubString(Qx,rn,sn))
call Qc(f7[T],p)
return true
endif
return false
endfunction
function Sc takes integer T,integer Tc,real Uc,integer Rx,integer Vc,integer Wc,integer Xc,integer Yc,integer Zc,integer dD,integer eD,real fD,real gD,integer hD,real iD,integer jD,integer kD returns nothing
set g7[T]=er()
set Wn[g7[T]]=Vc
set go[g7[T]]=fD
if gD==5 then
set ho[g7[T]]=fD/ 180.*.25
else
set ho[g7[T]]=gD*.25
endif
set Pn[g7[T]]=hD
set Tn[g7[T]]=Tc
set Xn[g7[T]]=Wc
set Yn[g7[T]]=Xc
set Zn[g7[T]]=Yc
set do[g7[T]]=Zc
set Un[g7[T]]=Uc
set Vn[g7[T]]=Rx
set eo[g7[T]]=dD
set fo[g7[T]]=eD
set ko[g7[T]]=iD
set mo[g7[T]]=jD
set no[g7[T]]=kD
endfunction
function mD takes integer T,integer nD,integer oD,integer pD,integer qD,boolean rD,boolean sD,real tD,code Ib returns nothing
set Ln[g7[T]]=nD
set Mn[g7[T]]=oD
set Nn[g7[T]]=pD
set Sn[g7[T]]=qD
set Qn[g7[T]]=rD
set Rn[g7[T]]=sD
set On[g7[T]]=tD
set h7[T]=CreateTrigger()
call TriggerAddCondition(h7[T],Filter(Ib))
endfunction
function vD takes integer T,integer tv,integer Cc,integer cc,integer Dc,integer Ec,code Fc,integer Gc,integer Hc,integer Ic,integer lc,integer Jc,code Kc,integer wD,integer xD,integer yD,integer zD,integer AD,code aD returns nothing
set i7[T]=dr()
set to[uo[i7[T]]]=tv
set to[uo[i7[T]]+1]=Gc
set to[uo[i7[T]]+2]=wD
set vo[wo[i7[T]]]=cc
set vo[wo[i7[T]]+1]=Ic
set vo[wo[i7[T]]+2]=yD
set xo[yo[i7[T]]]=Cc
set xo[yo[i7[T]]+1]=Hc
set xo[yo[i7[T]]+2]=xD
set zo[Ao[i7[T]]]=Dc
set zo[Ao[i7[T]]+1]=lc
set zo[Ao[i7[T]]+2]=zD
set ao[Bo[i7[T]]]=Ec
set ao[Bo[i7[T]]+1]=Jc
set ao[Bo[i7[T]]+2]=AD
if Fc!=null then
set bo[Co[i7[T]]]=CreateTrigger()
call TriggerAddAction(bo[Co[i7[T]]],Fc)
endif
if Kc!=null then
set bo[Co[i7[T]]+1]=CreateTrigger()
call TriggerAddAction(bo[Co[i7[T]]+1],Kc)
endif
if aD!=null then
set bo[Co[i7[T]]+2]=CreateTrigger()
call TriggerAddAction(bo[Co[i7[T]]+2],aD)
endif
endfunction
function bD takes integer T,integer Tc,boolean CD,integer Vc,boolean cD,boolean ED,integer FD,integer GD,integer HD,integer ID,integer lD,integer JD returns nothing
set ep[(T)]=Tc
set fp[(T)]=CD
set gp[(T)]=Vc
set hp[(T)]=cD
set ip[(T)]=ED
set jp[(T)]=FD
set kp[(T)]=GD
set mp[(T)]=HD
set np[(T)]=ID
set op[(T)]=lD
set pp[(T)]=JD
endfunction
function KD takes integer T,real LD,real MD,integer hc,integer ND,boolean OD,integer PD,code QD returns nothing
set Vo[(T)]=LD
set Wo[(T)]=MD
set Xo[(T)]=hc
set Yo[(T)]=ND
set Zo[(T)]=OD
set dp[(T)]=PD
set qp[(T)]=CreateTrigger()
call TriggerAddCondition(qp[(T)],Filter(QD))
endfunction
function RD takes integer T,real SD,integer ay,real cd,integer Zw,integer TD,real uu,integer n,boolean tB,integer Tv,integer fc returns nothing
set Lo[(T)]=SD
set Mo[(T)]=ay
set No[(T)]=cd
set Oo[(T)]=Zw
set Po[(T)]=P4[TD]
set Qo[(T)]=uu
set Ro[(T)]=n
set So[(T)]=tB
set To[(T)]=Tv
set Uo[(T)]=fc
endfunction
function VD takes integer T,integer WD,integer XD,integer YD,integer ZD,integer Bc,integer dE,integer eE,string uv returns nothing
set k7[T]=Yq()
set Fo[k7[T]]=WD
set Go[Ho[k7[T]]]=f7[XD]
set Go[Ho[k7[T]]+1]=f7[YD]
set Go[Ho[k7[T]]+2]=f7[ZD]
set Io[k7[T]]=Bc
set lo[k7[T]]=dE
set Jo[k7[T]]=eE
set Ko[k7[T]]=uv
endfunction
function InitHeroData takes nothing returns nothing
call VD(1,6,1,2,3,1093677365,1093677366,1093677367,null)
endfunction
function fE takes integer Zq,integer p returns nothing
local integer a=0
set Ag[p]=Zq
call UnitRemoveAbility(ig[p],lo[Zq])
call UnitRemoveAbility(ig[p],Jo[Zq])
call UnitRemoveAbility(fg[p],1093677364)
call UnitAddAbility(fg[p],Io[Zq])
loop
call Nc(Go[Ho[Zq]+a],p)
set a=a+1
exitwhen a==3
endloop
set a=0
loop
call UnitAddAbility(ig[p],to[uo[i7[Fo[Zq]]]+a])
call UnitAddAbility(ig[p],vo[wo[i7[Fo[Zq]]]+a])
set a=a+1
exitwhen a==3
endloop
call Qc(Go[Ho[Zq]],p)
endfunction
function InitGlobals takes nothing returns nothing
endfunction
function gE takes nothing returns nothing
local string s
set q7=q7+1
if q7==60 then
set q7=0
set r7=r7+1
endif
set t7=t7-1
if t7==0 then
set s7=s7+1
call TriggerExecute(Q7)
set t7=30
endif
if r7<$A then
set s="0"
else
set s=""
endif
set s=s+I2S(r7)+":"
if q7<$A then
set s=s+"0"
endif
set s=s+I2S(q7)
call MultiboardSetItemValue(J8,s)
set s="Wave "+Y9[P9]+I2S(s7)+"|r in "
if Ef[X]and t7<4 then
set s=s+Y9[S9]
call StartSound(r8)
else
set s=s+Y9[P9]
endif
set s=s+I2S(t7)+"|r second"
if t7!=1 then
set s=s+"s"
endif
call MultiboardSetTitleText(a8,s)
endfunction
function hE takes nothing returns nothing
local integer p=(z4[GetPlayerId((GetTriggerPlayer()))])
local integer iE=If[p]
local real x=Vd[iE]
local real y=320.
local boolean jE=true
local integer t
local unit u
loop
call eB(iE,x,y,true)
set u=CreateUnit(Bf[p],1949315120,x,y,270.)
call gB(u,p,x,y,0)
set t=GetUnitUserData(u)
call BB(t)
if jE then
set x=x+128.
if x>=Ud[iE]then
set y=y-256.
set jE=false
endif
else
set x=x-128.
if x<=Vd[iE]then
set y=y-256.
set jE=true
endif
endif
exitwhen y<k4
endloop
call Nv(iE)
set u=null
endfunction
function kE takes nothing returns nothing
call PauseTimer(m7)
call DisableTrigger(w7)
call EnableTrigger(x7)
endfunction
function mE takes nothing returns nothing
call gE()
call TimerStart(m7,1.,true,function gE)
call EnableTrigger(w7)
endfunction
function nE takes nothing returns nothing
call TimerStart(m7,TimerGetRemaining(m7),false,function mE)
call DisableTrigger(x7)
endfunction
function oE takes nothing returns nothing
set t7=1
call gE()
endfunction
function pE takes nothing returns nothing
call kE()
call oE()
endfunction
function qE takes nothing returns nothing
local integer m=GetUnitUserData(GetFilterUnit())
if(IsUnitType(GetFilterUnit(),e4))and Zg[m]then
call bz(m,false)
endif
endfunction
function rE takes nothing returns nothing
call GroupEnumUnitsSelected(bj_lastCreatedGroup,GetTriggerPlayer(),a7)
endfunction
function sE takes nothing returns nothing
call Sx((z4[GetPlayerId((GetTriggerPlayer()))]))
endfunction
function tE takes nothing returns nothing
local integer t=GetUnitUserData(GetFilterUnit())
local integer a=0
if(IsUnitType(GetFilterUnit(),Z))and Gj[t]and Sj[t]then
loop
set a=a+1
call DB(t)
exitwhen(not Sj[t])or a==D7
endloop
if Sj[t]and ip[Fj[t]]then
call SetItemCharges(dk[t],Vj[t]-Tj[t])
endif
endif
endfunction
function uE takes nothing returns nothing
local string in=GetEventPlayerChatString()
if SubString(in,0,C7)=="inc" then
set D7=S2I(SubString(in,c7,StringLength(in)))
if D7>0 then
call GroupEnumUnitsSelected(bj_lastCreatedGroup,GetTriggerPlayer(),E7)
endif
endif
endfunction
function vE takes nothing returns nothing
local string in=GetEventPlayerChatString()
local integer p=(z4[GetPlayerId((GetTriggerPlayer()))])
local integer bs
if SubString(in,0,G7)=="gold" then
set bs=S2I(SubString(in,H7,StringLength(in)))
if 0<bs and bs<=99999 then
call yx(p,bs-Kf[p])
endif
endif
endfunction
function wE takes nothing returns nothing
local string in=GetEventPlayerChatString()
local integer p=(z4[GetPlayerId((GetTriggerPlayer()))])
local integer bs
if SubString(in,0,l7)=="exp" then
set bs=S2I(SubString(in,J7,StringLength(in)))
if 0<bs and bs<=99999 then
call bx(p,bs-Lf[p])
endif
endif
endfunction
function xE takes nothing returns nothing
local string in=GetEventPlayerChatString()
local integer p=(z4[GetPlayerId((GetTriggerPlayer()))])
local integer bs
if SubString(in,0,L7)=="lives" then
set bs=S2I(SubString(in,M7,StringLength(in)))
if 0<bs and bs<=9999 then
call Hx(p,bs-Pf[p])
endif
endif
endfunction
function yE takes nothing returns nothing
local string in=GetEventPlayerChatString()
local integer zE
if SubString(in,0,O7)=="wave" then
set zE=S2I(SubString(in,P7,StringLength(in)))
if zE>0 then
set s7=zE
endif
endif
endfunction
function AE takes player p returns nothing
call TriggerAddAction(v7,function hE)
call TriggerAddAction(w7,function kE)
call DisableTrigger(x7)
call TriggerAddAction(x7,function nE)
call TriggerAddAction(y7,function oE)
call TriggerAddAction(z7,function pE)
call TriggerAddAction(A7,function rE)
set a7=Filter(function qE)
call TriggerAddAction(B7,function sE)
call TriggerAddAction(b7,function uE)
set E7=Filter(function tE)
call TriggerAddAction(F7,function vE)
call TriggerAddAction(I7,function wE)
call TriggerAddAction(K7,function xE)
call TriggerAddAction(N7,function yE)
call TriggerRegisterPlayerChatEvent(v7,p,"maze",true)
call TriggerRegisterPlayerChatEvent(w7,p,"p",true)
call TriggerRegisterPlayerChatEvent(x7,p,"r",true)
call TriggerRegisterPlayerChatEvent(y7,p,"s",true)
call TriggerRegisterPlayerChatEvent(z7,p,"ps",true)
call TriggerRegisterPlayerChatEvent(A7,p,"kill",true)
call TriggerRegisterPlayerChatEvent(B7,p,"hero",true)
call TriggerRegisterPlayerChatEvent(b7,p,"inc",false)
call TriggerRegisterPlayerChatEvent(F7,p,"gold",false)
call TriggerRegisterPlayerChatEvent(I7,p,"exp",false)
call TriggerRegisterPlayerChatEvent(K7,p,"lives",false)
call TriggerRegisterPlayerChatEvent(N7,p,"wave",false)
endfunction
function aE takes nothing returns nothing
local integer i=0
local integer a
local integer p
call lu()
loop
exitwhen i>Q8
set p=(P8[(i)])
call Ju(p)
set a=0
loop
exitwhen a>h9[mg[p]]
call ey((f9[g9[(mg[p])]+(a)]))
set a=a+1
endloop
set Cg[p]=0
set h9[mg[p]]=-1
call mx(p)
if u7<=s7 and(25<s7 or Rt(s7))then
call Sx(p)
endif
call qx(p)
if 41<=s7 and Pf[p]>1 then
call lx(p)
endif
call yx(p,Of[p])
call sx(p,q8,"You receive "+Y9[J9]+I2S(Of[p])+"|r Gold.")
set i=i+1
endloop
endfunction
function InitTrig_SendMinions takes nothing returns nothing
call TriggerAddAction(Q7,function aE)
endfunction
function BE takes nothing returns nothing
local integer YC=mg[R7]
local integer a=h9[YC]
loop
exitwhen a==-1
call ts((f9[g9[(YC)]+(a)]))
set a=a-1
endloop
call Ot("forces of player "+I2S(R7)+" is flushed.")
endfunction
function bE takes nothing returns nothing
local integer YC=ng[R7]
local integer a=o9[YC]
loop
exitwhen a==-1
call fy((m9[n9[(YC)]+(a)]))
set a=a-1
endloop
call Ot("spawned of player "+I2S(R7)+" is flushed.")
endfunction
function CE takes nothing returns nothing
local integer YC=og[R7]
local integer a=u9[YC]
loop
exitwhen a==-1
call Bz((s9[t9[(YC)]+(a)]))
set a=a-1
endloop
call Ot("minions of player "+I2S(R7)+" is flushed.")
endfunction
function cE takes nothing returns nothing
local integer YC=qg[R7]
local integer a=A9[YC]
loop
exitwhen a==-1
call Az((y9[z9[(YC)]+(a)]))
set a=a-1
endloop
call Ot("dead of player "+I2S(R7)+" is flushed.")
endfunction
function DE takes nothing returns nothing
local integer YC=kg[R7]
local integer a=Y8[YC]
loop
exitwhen a==-1
call CB((W8[X8[(YC)]+(a)]))
set a=a-1
endloop
call Ot("towers of player "+I2S(R7)+" is flushed.")
endfunction
function EE takes nothing returns nothing
local integer p=R7
local integer a=120-1
call Hu(p)
call ExecuteFunc("BE")
call ExecuteFunc("bE")
call ExecuteFunc("CE")
call ExecuteFunc("cE")
call ExecuteFunc("DE")
call Mx(p)
call Ot("Player "+I2S(p)+" is flushed.")
endfunction
function InitTrig_FlushPlayer takes nothing returns nothing
call TriggerAddAction(S7,function EE)
endfunction
function FE takes nothing returns nothing
local integer p=(z4[GetPlayerId((GetTriggerPlayer()))])
if Ef[p]then
call Au(p)
if Q8==0 then
call au($A)
endif
endif
endfunction
function InitTrig_LeavesGame takes nothing returns nothing
call TriggerAddAction(T7,function FE)
endfunction
function GE takes nothing returns nothing
call hB(GetConstructingStructure())
endfunction
function InitTrig_ConstructStart takes nothing returns nothing
call TriggerAddAction(U7,function GE)
endfunction
function HE takes nothing returns nothing
call cB((GetUnitUserData(GetCancelledStructure())))
endfunction
function InitTrig_ConstructCancel takes nothing returns nothing
call TriggerAddAction(V7,function HE)
endfunction
function IE takes nothing returns nothing
call BB((GetUnitUserData(GetConstructedStructure())))
endfunction
function InitTrig_ConstructEnd takes nothing returns nothing
call TriggerAddAction(W7,function IE)
endfunction
function lE takes nothing returns nothing
call kc(GetTriggerUnit())
endfunction
function InitTrig_UpgradeStart takes nothing returns nothing
call TriggerAddAction(X7,function lE)
endfunction
function JE takes nothing returns nothing
call mc(GetTriggerUnit())
endfunction
function InitTrig_UpgradeCancel takes nothing returns nothing
call TriggerAddAction(Y7,function JE)
endfunction
function KE takes nothing returns nothing
call nc(GetTriggerUnit())
endfunction
function InitTrig_UpgradeEnd takes nothing returns nothing
call TriggerAddAction(Z7,function KE)
endfunction
function LE takes nothing returns nothing
local integer m=GetUnitUserData(GetLeavingUnit())
if dh[m]then
call vz(m)
endif
endfunction
function InitTrig_LeaveSpawn takes nothing returns nothing
call TriggerAddAction(d8,function LE)
endfunction
function ME takes nothing returns nothing
local integer m=GetUnitUserData(GetEnteringUnit())
local integer p=Tg[m]
call Ix(p,eo[hh[m]])
call gy(m,false)
if p!=Sg[m]and Ef[Sg[m]]then
call Ix(Sg[m],fo[hh[m]])
endif
call DestroyEffect(AddSpecialEffect(("Abilities\\Spells\\Undead\\DarkRitual\\DarkRitualTarget.mdx"),((GetUnitX(Vg[m]))*1.),((GetUnitY(Vg[m]))*1.)))
call Bz(m)
if Pf[p]==0 then
call Au(p)
if bj_isSinglePlayer!=(Q8==0)then
call au($A)
endif
endif
endfunction
function InitTrig_EnterFinish takes nothing returns nothing
call TriggerAddAction(e8,function ME)
endfunction
function NE takes unit b,integer id returns nothing
local integer p=(z4[GetPlayerId((GetOwningPlayer(b)))])
if id==rd[cg[p]]then
if Ff[p]then
call ClearSelection()
call SelectUnit(gg[p],true)
endif
if Eg[p]!=cg[p]then
call av(p)
endif
return
endif
if id==sd[Dg[p]]then
if Ff[p]then
call ClearSelection()
call SelectUnit(gg[p],true)
endif
if Eg[p]!=Dg[p]then
call av(p)
endif
return
endif
if(id==Io[Ag[p]]or id==1093677364)and Ff[p]then
call ClearSelection()
call SelectUnit(ig[p],true)
endif
endfunction
function OE takes unit b,integer id returns nothing
local integer p=(z4[GetPlayerId((GetOwningPlayer(b)))])
if Px(p,id)then
return
endif
if id==1093677123 then
if Ff[p]then
call ClearSelection()
call SelectUnit(hg[p],true)
endif
if Eg[p]!=Gg[p]then
call wv(p)
endif
return
endif
if id==1093677135 and Ff[p]then
call ClearSelection()
call SelectUnit(ig[p],true)
return
endif
if id==1093677144 and Ff[p]then
call ClearSelection()
call SelectUnit(jg[p],true)
return
endif
if id==1093677124 then
call av(p)
endif
endfunction
function PE takes unit b,integer id returns nothing
local integer p=(z4[GetPlayerId((GetOwningPlayer(b)))])
if Tx(p,id,true)then
return
endif
if id==1093677362 then
call wv(p)
return
endif
if id==1093677361 then
call Dz(p)
endif
endfunction
function QE takes unit b,integer id returns nothing
local integer p=(z4[GetPlayerId((GetOwningPlayer(b)))])
if id==lo[pd[cg[p]]]then
call UnitRemoveAbility(b,Jo[pd[Dg[p]]])
call fE(pd[cg[p]],p)
return
endif
if id==Jo[pd[Dg[p]]]then
call UnitRemoveAbility(b,lo[pd[cg[p]]])
call fE(pd[Dg[p]],p)
return
endif
if Tx(p,id,false)then
return
endif
if Rc(p,id)then
return
endif
call Mc(p,id)
endfunction
function RE takes integer cA,integer id,integer gs returns nothing
if id==1093677119 or id==1093677121 then
call cB(cA)
return
endif
if id==1093677129 then
call lB(cA)
return
endif
if id==1093677127 then
call vC(ek[cA],gs)
endif
endfunction
function SE takes integer m,integer id returns nothing
if id==1093677122 then
call ts(m)
return
endif
if id==sk[Z4[1]]then
call Ba(Yg[m])
endif
endfunction
function TE takes nothing returns nothing
local integer id=GetSpellAbilityId()
local unit u=GetSpellAbilityUnit()
local integer cA=GetUnitUserData(u)
local integer gs=GetUnitUserData(GetSpellTargetUnit())
local integer T=GetUnitTypeId(u)
if T==1112294482 then
call NE(u,id)
elseif T==1112689491 then
call OE(u,id)
elseif T==1112232788 then
call PE(u,id)
elseif T==1095521362 then
call QE(u,id)
elseif(IsUnitType((u),Z))then
call RE(cA,id,gs)
elseif(IsUnitType((u),e4))then
call SE(cA,id)
endif
set u=null
endfunction
function InitTrig_PlayerAbilityEffect takes nothing returns nothing
call TriggerAddAction(f8,function TE)
endfunction
function UE takes integer cA,integer id returns nothing
local integer Tv=ek[cA]
if dq[Tv]==86 then
call Rb(Tv,id)
return
endif
if dq[Tv]==91 then
call xC(Tv,id)
endif
endfunction
function VE takes integer m,integer id returns nothing
if id==1093677099 then
call GroupEnumUnitsSelected(bj_lastCreatedGroup,Bf[(Sg[m])],yh)
return
endif
if Wp[Yg[m]]==66 then
call Ga(Yg[m],id)
endif
endfunction
function WE takes nothing returns nothing
local integer id=GetSpellAbilityId()
local unit u=GetSpellAbilityUnit()
local integer cA=GetUnitUserData(u)
if(IsUnitType((u),Z))or(IsUnitType((u),d4))then
call UE(cA,id)
elseif(IsUnitType((u),e4))then
call VE(cA,id)
endif
set u=null
endfunction
function InitTrig_PlayerAbilityEndcast takes nothing returns nothing
call TriggerAddAction(g8,function WE)
endfunction
function XE takes unit b,integer id returns nothing
local integer p=(z4[GetPlayerId((GetOwningPlayer(b)))])
if id==x4 or id==y4 then
call ux(p)
endif
endfunction
function YE takes integer cA,integer id returns nothing
local integer Tv=ek[cA]
if 1949315120<=id and id<1949315120+j7 then
set en[(cA)]=id-1949315120
return
endif
if dq[Tv]==94 then
call ec(Tv,id)
endif
endfunction
function ZE takes nothing returns nothing
local integer id=GetIssuedOrderId()
local unit u=GetOrderedUnit()
local integer cA=GetUnitUserData(u)
local integer T=GetUnitTypeId(u)
if T==1112294482 then
call XE(u,id)
elseif(IsUnitType((u),Z))then
call YE(cA,id)
endif
set u=null
endfunction
function InitTrig_PlayerImdtOrders takes nothing returns nothing
call TriggerAddAction(h8,function ZE)
endfunction
function d3 takes unit b,integer id,real x,real y returns nothing
if id==L or(1949315120<=id and id<1949315120+j7)then
call SetUnitX(b,x)
call SetUnitY(b,y)
endif
endfunction
function e3 takes nothing returns nothing
local integer id=GetIssuedOrderId()
local unit u=GetOrderedUnit()
local integer T=GetUnitTypeId(u)
local real x=GetOrderPointX()
local real y=GetOrderPointY()
local integer iE=If[(z4[GetPlayerId((GetOwningPlayer(u)))])]
if T==1112294482 and Td[iE]<=x and x<=Sd[iE]then
call d3(u,id,x,y)
endif
set u=null
endfunction
function InitTrig_PlayerPointOrders takes nothing returns nothing
call TriggerAddAction(i8,function e3)
endfunction
function f3 takes unit u returns nothing
local integer JB=GetUnitUserData(u)
if(IsUnitType((u),Z))then
call Tr((JB))
endif
endfunction
function g3 takes nothing returns nothing
if GetTriggerPlayer()==GetOwningPlayer(GetTriggerUnit())then
call f3(GetTriggerUnit())
endif
endfunction
function InitTrig_PlayerSelects takes nothing returns nothing
call TriggerAddAction(j8,function g3)
endfunction
function h3 takes unit u returns nothing
local integer JB=GetUnitUserData(u)
if(IsUnitType((u),Z))then
call Ur((JB))
endif
endfunction
function i3 takes nothing returns nothing
if GetTriggerPlayer()==GetOwningPlayer(GetTriggerUnit())then
call h3(GetTriggerUnit())
endif
endfunction
function InitTrig_PlayerDeselects takes nothing returns nothing
call TriggerAddAction(k8,function i3)
endfunction
function j3 takes integer t,integer m returns nothing
if not(yf[Xg[(m)]])then
call Pr(t,m)
endif
endfunction
function k3 takes unit DD,unit m3,real SD returns nothing
local integer dd=GetUnitUserData(DD)
local integer gs=GetUnitUserData(m3)
call SetWidgetLife(m3,GetWidgetLife(m3)+SD)
if(not UnitAlive((m3)))then
call DisplayTimedTextToPlayer(S,.0,.0,((10.)*1.),("No target took damage!"))
return
endif
if(IsUnitType((DD),d4))then
call j3(dd,gs)
elseif(IsUnitType((DD),f4))then
call fs((dd),(gs))
endif
endfunction
function n3 takes nothing returns nothing
if GetEventDamage()>.0 and(GetUnitTypeId((GetEventDamageSource()))!=0)then
call k3(GetEventDamageSource(),GetTriggerUnit(),GetEventDamage())
endif
endfunction
function InitTrig_TakeDamage takes nothing returns nothing
call TriggerAddAction(m8,function n3)
endfunction
function o3 takes nothing returns nothing
local integer i=0
local integer ai=0
local player p
loop
set p=Player(i)
if GetPlayerSlotState(p)==PLAYER_SLOT_STATE_PLAYING then
if GetPlayerController(p)==MAP_CONTROL_USER then
call gx(p,i)
endif
elseif ai<2 then
set W[ai]=p
call SetPlayerColor(p,Y)
call FogModifierStart(CreateFogModifierRect(p,FOG_OF_WAR_VISIBLE,bj_mapInitialPlayableArea,false,true))
set ai=ai+1
endif
set i=i+1
exitwhen i==$C
endloop
if a4!=null then
set i=0
loop
exitwhen(not A4[i])and ConvertPlayerColor(i)!=Y
set i=i+1
endloop
call SetPlayerColor(a4,ConvertPlayerColor(i))
set cf[(z4[GetPlayerId((a4))])]=i
endif
set X=(z4[GetPlayerId((S))])
call TriggerRegisterLeaveRegion(d8,p7,h4)
call TriggerRegisterEnterRegion(e8,o7,h4)
call Ot("Initialization complete!")
set p=null
endfunction
function InitTrig_Main takes nothing returns nothing
call TriggerAddAction(z8,function o3)
endfunction
function p3 takes nothing returns nothing
local integer i=0
local integer p
loop
exitwhen i>Q8
set p=(P8[(i)])
set cg[p]=j4[1]
set Dg[p]=0
call hx(p)
call fE(pd[j4[1]],p)
set i=i+1
endloop
set bj_isSinglePlayer=Q8==0
call TriggerExecute(L8)
call gE()
call TimerStart(m7,1.,true,function gE)
if bj_isSinglePlayer then
call AE(Bf[(P8[(0)])])
endif
endfunction
function r3 takes nothing returns integer
set B8=B8+1
call MultiboardSetRowCount(a8,B8)
return B8-1
endfunction
function s3 takes nothing returns nothing
local integer a
local multiboarditem Lx
local integer p
set a8=CreateMultiboard()
call MultiboardSetTitleTextColor(a8,$FF,$FF,$FF,$FF)
call MultiboardSetColumnCount(a8,8)
call MultiboardSetItemsStyle(a8,true,false)
call MultiboardSetItemsWidth(a8,b8)
set a=r3()
set Lx=MultiboardGetItem(a8,a,0)
call MultiboardSetItemWidth(Lx,C8)
call MultiboardSetItemValue(Lx,"Player")
set Lx=MultiboardGetItem(a8,a,2)
call MultiboardSetItemWidth(Lx,c8)
call MultiboardSetItemValue(Lx,"Gold")
set Lx=MultiboardGetItem(a8,a,3)
call MultiboardSetItemWidth(Lx,D8)
call MultiboardSetItemValue(Lx,"Exp.")
set Lx=MultiboardGetItem(a8,a,4)
call MultiboardSetItemWidth(Lx,E8)
call MultiboardSetItemValue(Lx,"Supply")
set Lx=MultiboardGetItem(a8,a,5)
call MultiboardSetItemWidth(Lx,F8)
call MultiboardSetItemValue(Lx,"Inc.")
set Lx=MultiboardGetItem(a8,a,6)
call MultiboardSetItemWidth(Lx,G8)
call MultiboardSetItemValue(Lx,"Lives")
set Lx=MultiboardGetItem(a8,a,7)
call MultiboardSetItemWidth(Lx,H8)
call MultiboardSetItemValue(Lx,"Kills")
set a=0
loop
exitwhen a>Q8
set p=(P8[(a)])
set Rf[p]=r3()
set Sf[p]=MultiboardGetItem(a8,Rf[p],0)
call MultiboardSetItemWidth(Sf[p],C8)
call MultiboardSetItemValue(Sf[p],Cf[p])
call MultiboardSetItemValueColor(Sf[p],V9[cf[p]],W9[cf[p]],X9[cf[p]],K8)
set Tf[p]=MultiboardGetItem(a8,Rf[p],2)
call MultiboardSetItemWidth(Tf[p],c8)
call MultiboardSetItemValue(Tf[p],I2S(Kf[p]))
call MultiboardSetItemValueColor(Tf[p],V9[J9],W9[J9],X9[J9],K8)
set Uf[p]=MultiboardGetItem(a8,Rf[p],3)
call MultiboardSetItemWidth(Uf[p],D8)
call MultiboardSetItemValue(Uf[p],I2S(Lf[p]))
call MultiboardSetItemValueColor(Uf[p],V9[K9],W9[K9],X9[K9],K8)
set Vf[p]=MultiboardGetItem(a8,Rf[p],4)
call MultiboardSetItemWidth(Vf[p],E8)
call MultiboardSetItemValue(Vf[p],"0/"+I2S(Mf[p]))
call MultiboardSetItemValueColor(Vf[p],V9[L9],W9[L9],X9[L9],K8)
set Wf[p]=MultiboardGetItem(a8,Rf[p],5)
call MultiboardSetItemWidth(Wf[p],F8)
call MultiboardSetItemValue(Wf[p],I2S(Of[p]))
call MultiboardSetItemValueColor(Wf[p],V9[M9],W9[M9],X9[M9],K8)
set Xf[p]=MultiboardGetItem(a8,Rf[p],6)
call MultiboardSetItemWidth(Xf[p],G8)
call MultiboardSetItemValue(Xf[p],I2S(Pf[p]))
call MultiboardSetItemValueColor(Xf[p],V9[N9],W9[N9],X9[N9],K8)
set Yf[p]=MultiboardGetItem(a8,Rf[p],7)
call MultiboardSetItemWidth(Yf[p],H8)
call MultiboardSetItemValue(Yf[p],"0")
call MultiboardSetItemValueColor(Yf[p],V9[O9],W9[O9],X9[O9],K8)
set a=a+1
endloop
call r3()
set a=r3()
set Lx=MultiboardGetItem(a8,a,0)
call MultiboardSetItemWidth(Lx,I8)
call MultiboardSetItemValue(Lx,"Game time: ")
set Lx=null
set J8=MultiboardGetItem(a8,a,1)
call MultiboardSetItemWidth(J8,l8)
call MultiboardSetItemValueColor(J8,V9[Q9],W9[Q9],X9[Q9],K8)
call MultiboardDisplay(a8,true)
call MultiboardMinimize(a8,false)
endfunction
function InitTrig_Multiboard takes nothing returns nothing
call TriggerAddAction(L8,function s3)
endfunction
function t3 takes nothing returns nothing
local string s
set t7=t7-1
set s="Game will end in "+Y9[P9]+I2S(t7)+"|r second"
if t7>1 then
set s=s+"s"
endif
if t7==0 then
call MultiboardSetTitleText(a8,"Game is ending...")
call EndGame(true)
else
call MultiboardSetTitleText(a8,s)
endif
endfunction
function u3 takes nothing returns nothing
local integer i=0
loop
exitwhen i>Q8
call Au((P8[(i)]))
set i=i+1
endloop
call ClearTextMessages()
call t3()
call TimerStart(m7,1.,true,function t3)
endfunction
function InitTrig_FinishGame takes nothing returns nothing
call TriggerAddAction(M8,function u3)
endfunction
function InitCustomPlayerSlots takes nothing returns nothing
call SetPlayerStartLocation(Player(0),0)
call SetPlayerColor(Player(0),ConvertPlayerColor(0))
call SetPlayerRacePreference(Player(0),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(0),true)
call SetPlayerController(Player(0),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(1),1)
call SetPlayerColor(Player(1),ConvertPlayerColor(1))
call SetPlayerRacePreference(Player(1),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(1),true)
call SetPlayerController(Player(1),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(2),2)
call SetPlayerColor(Player(2),ConvertPlayerColor(2))
call SetPlayerRacePreference(Player(2),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(2),true)
call SetPlayerController(Player(2),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(3),3)
call SetPlayerColor(Player(3),ConvertPlayerColor(3))
call SetPlayerRacePreference(Player(3),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(3),true)
call SetPlayerController(Player(3),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(4),4)
call SetPlayerColor(Player(4),ConvertPlayerColor(4))
call SetPlayerRacePreference(Player(4),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(4),true)
call SetPlayerController(Player(4),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(5),5)
call SetPlayerColor(Player(5),ConvertPlayerColor(5))
call SetPlayerRacePreference(Player(5),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(5),true)
call SetPlayerController(Player(5),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(6),6)
call SetPlayerColor(Player(6),ConvertPlayerColor(6))
call SetPlayerRacePreference(Player(6),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(6),true)
call SetPlayerController(Player(6),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(7),7)
call SetPlayerColor(Player(7),ConvertPlayerColor(7))
call SetPlayerRacePreference(Player(7),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(7),true)
call SetPlayerController(Player(7),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(8),8)
call SetPlayerColor(Player(8),ConvertPlayerColor(8))
call SetPlayerRacePreference(Player(8),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(8),true)
call SetPlayerController(Player(8),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(9),9)
call SetPlayerColor(Player(9),ConvertPlayerColor(9))
call SetPlayerRacePreference(Player(9),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(9),true)
call SetPlayerController(Player(9),MAP_CONTROL_USER)
endfunction
function InitCustomTeams takes nothing returns nothing
call SetPlayerTeam(Player(0),0)
call SetPlayerTeam(Player(1),0)
call SetPlayerTeam(Player(2),0)
call SetPlayerTeam(Player(3),0)
call SetPlayerTeam(Player(4),0)
call SetPlayerTeam(Player(5),0)
call SetPlayerTeam(Player(6),0)
call SetPlayerTeam(Player(7),0)
call SetPlayerTeam(Player(8),0)
call SetPlayerTeam(Player(9),0)
endfunction
function InitAllyPriorities takes nothing returns nothing
call SetStartLocPrioCount(0,2)
call SetStartLocPrio(0,0,8,MAP_LOC_PRIO_LOW)
call SetStartLocPrio(0,1,9,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(1,1)
call SetStartLocPrio(1,0,2,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(2,2)
call SetStartLocPrio(2,0,1,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(2,1,3,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(3,2)
call SetStartLocPrio(3,0,2,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(3,1,4,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(4,2)
call SetStartLocPrio(4,0,3,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(4,1,5,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(5,2)
call SetStartLocPrio(5,0,4,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(5,1,6,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(6,2)
call SetStartLocPrio(6,0,5,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(6,1,7,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(7,2)
call SetStartLocPrio(7,0,6,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(7,1,8,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(8,2)
call SetStartLocPrio(8,0,7,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(8,1,9,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(9,1)
call SetStartLocPrio(9,0,8,MAP_LOC_PRIO_HIGH)
endfunction
function main takes nothing returns nothing
call SetCameraBounds(v8+M,w8+O,6912.-N,1920.-P,v8+M,1920.-P,6912.-N,w8+O)
set bj_mapInitialPlayableArea=GetWorldBounds()
call ExecuteFunc("v3")
call ExecuteFunc("qu")
call ExecuteFunc("Du")
call ExecuteFunc("Hz")
call ExecuteFunc("aA")
call TriggerAddAction(Q7,function aE)
call TriggerAddAction(S7,function EE)
call TriggerAddAction(T7,function FE)
call TriggerAddAction(U7,function GE)
call TriggerAddAction(V7,function HE)
call TriggerAddAction(W7,function IE)
call TriggerAddAction(X7,function lE)
call TriggerAddAction(Y7,function JE)
call TriggerAddAction(Z7,function KE)
call TriggerAddAction(d8,function LE)
call TriggerAddAction(e8,function ME)
call TriggerAddAction(f8,function TE)
call TriggerAddAction(g8,function WE)
call TriggerAddAction(h8,function ZE)
call TriggerAddAction(i8,function e3)
call TriggerAddAction(j8,function g3)
call TriggerAddAction(k8,function i3)
call TriggerAddAction(m8,function n3)
call TriggerAddAction(z8,function o3)
call TriggerRegisterTimerEvent(A8,1.,false)
call TriggerAddAction(A8,function p3)
call TriggerAddAction(L8,function s3)
call TriggerAddAction(M8,function u3)
set n8=CreateSound("Sound\\Interface\\Error.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(n8,"InterfaceError")
call SetSoundDuration(n8,614)
call SetSoundVolume(n8,105)
set q8=CreateSound("Abilities\\Spells\\Items\\ResourceItems\\ReceiveGold.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(q8,"ReceiveGold")
call SetSoundDuration(q8,589)
call SetSoundChannel(q8,8)
call SetSoundVolume(q8,105)
set r8=CreateSound("Sound\\Interface\\BattleNetTick.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(r8,"ChatroomTimerTick")
call SetSoundDuration(r8,476)
call SetSoundVolume(r8,105)
set s8=CreateSound("Sound\\Interface\\Warning.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(s8,"Warning")
call SetSoundDuration(s8,$770)
call SetSoundVolume(s8,105)
set t8=CreateSound("Sound\\Interface\\ItemReceived.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(t8,"ItemReward")
call SetSoundDuration(t8,$5CB)
call SetSoundVolume(t8,105)
set u8=CreateSound("Sound\\Interface\\UpkeepRing.wav",false,false,false,$A,$A,"")
call SetSoundParamsFromLabel(u8,"UpkeepLevel")
call SetSoundDuration(u8,$62B)
call SetSoundVolume(u8,105)
set o8=CreateSound("Sound\\Interface\\Warning\\Human\\KnightNoGold1.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(o8,"NoGoldHuman")
call SetSoundDuration(o8,$5CE)
call SetSoundVolume(o8,105)
set p8=CreateSound("Sound\\Buildings\\Human\\KnightResearchComplete1.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(p8,"ResearchCompleteHuman")
call SetSoundDuration(p8,$52E)
call SetSoundVolume(p8,105)
set o7=CreateRegion()
set p7=CreateRegion()
set i4=qr()
set gd[hd[i4]]=1.
set gd[hd[i4]+1]=1.075
set gd[hd[i4]+2]=1.155625
set gd[hd[i4]+3]=1.242297
set gd[hd[i4]+4]=1.335469
set gd[hd[i4]+5]=1.435629
set gd[hd[i4]+6]=1.543302
set gd[hd[i4]+7]=1.659049
set gd[hd[i4]+8]=1.783478
set gd[hd[i4]+9]=1.917239
set gd[hd[i4]+$A]=2.061032
set gd[hd[i4]+$B]=2.215609
set gd[hd[i4]+$C]=2.38178
set gd[hd[i4]+$D]=2.560413
set gd[hd[i4]+$E]=2.752444
set gd[hd[i4]+$F]=2.958877
set gd[hd[i4]+16]=3.180793
set gd[hd[i4]+17]=3.419353
set gd[hd[i4]+18]=3.675804
set gd[hd[i4]+19]=3.951489
set gd[hd[i4]+20]=4.247851
set gd[hd[i4]+21]=4.56644
set gd[hd[i4]+22]=4.908923
set gd[hd[i4]+23]=5.277092
set gd[hd[i4]+24]=5.672874
set gd[hd[i4]+25]=6.09834
set gd[hd[i4]+26]=6.555715
set gd[hd[i4]+27]=7.047394
set gd[hd[i4]+28]=7.575948
set gd[hd[i4]+29]=8.144144
set gd[hd[i4]+30]=8.754955
set gd[hd[i4]+31]=9.411577
set gd[hd[i4]+32]=10.117445
set gd[hd[i4]+33]=10.876253
set gd[hd[i4]+34]=11.691972
set gd[hd[i4]+35]=12.56887
set gd[hd[i4]+36]=13.511536
set gd[hd[i4]+37]=14.524901
set gd[hd[i4]+38]=15.614268
set gd[hd[i4]+39]=16.785339
set gd[hd[i4]+40]=18.044239
set gd[hd[i4]+41]=19.397557
set gd[hd[i4]+42]=20.852374
set gd[hd[i4]+43]=22.416302
set gd[hd[i4]+44]=24.097524
set gd[hd[i4]+45]=25.904839
set gd[hd[i4]+46]=27.847702
set gd[hd[i4]+47]=29.936279
set gd[hd[i4]+48]=32.1815
set gd[hd[i4]+49]=34.595113
set gd[hd[i4]+50]=37.189746
call gc(1,1093677109,5,$A,0,function Mb)
call gc(2,1093677133,5,$A,0,function Tb)
call gc(3,1093677108,5,$A,0,function Vb)
call gc(4,1093677130,5,$A,0,function Zb)
call gc(5,1093677131,5,$A,0,function mC)
call gc(6,1093677111,1,2,3,function pC)
call gc(7,1093677136,$A,20,0,function GC)
call gc(8,1093677107,5,$A,0,function MC)
call gc(9,1093677141,5,$A,0,function dc)
call bD(0,50,true,-1,false,false,0,0,0,0,0,X4)
call bD(1,100,true,-1,true,true,1,5,20,50,$A,X4)
call bD(2,100,true,-1,true,true,1,5,20,50,$A,X4)
call bD(3,100,true,-1,true,true,1,5,20,50,$A,X4)
call bD(7,500,false,4,true,false,1,0,0,500,3,3)
call bD(8,$C8,true,-1,false,false,0,0,0,0,0,0)
call bD(9,$C8,true,-1,true,true,1,5,20,50,$A,X4)
call bD(4,$C8,true,0,true,false,0,0,0,0,$A,X4)
call bD(5,$4B0,false,0,true,false,1,0,60,$B4,$A,$A)
call bD(6,$3E8,false,0,true,false,1,0,50,$96,$A,$A)
call KD(0,60.,.4,1097609264,0,false,0,function jc)
call KD(1,145.,.6,1097609265,1098133553,true,1,function oc)
call KD(2,60.,.6,1097609266,1098133554,true,2,function qc)
call KD(3,255.,.5,1097609267,1098133555,true,3,function rc)
call KD(4,135.,.6,1097609268,1098133556,true,4,function sc)
call KD(5,60.,.4,1097609269,1098133557,false,5,function tc)
call KD(6,60.,.4,1097609270,1098133558,false,9,function xc)
call KD(7,60.,.4,1097609271,1098133559,false,6,function uc)
call KD(8,255.,.5,1097609272,0,true,7,function vc)
call KD(9,135.,.6,1097609273,1098133561,true,8,function wc)
call RD(1,10.,i4,1.,1227894834,0,384.,1,false,1093677094,D)
call RD(2,6.,i4,2.,1227894834,0,384.,3,false,1093677097,D)
call RD(3,15.,i4,3.,1227894841,0,384.,1,false,1093677110,C)
call RD(8,30.,0,1.,1227894832,0,384.,1,true,1093677098,F)
call RD(9,10.,i4,3.,1227894834,1,512.,1,true,1093677137,B)
call RD(4,5.,0,.5,1227894832,0,384.,1,false,1093677126,D)
call Sv(v4,i4,0)
call XA(1,true,3,3,3.,0)
call XA(2,true,1,1,3.,0)
call XA(3,true,1,1,3.,0)
call XA(4,true,1,1,.5,0)
call XA(5,true,1,1,.5,0)
call XA(6,false,1,1,3.,0)
call XA(V4,false,1,1,3.,0)
call XA(7,false,1,1,1.5,0)
call XA(S4,false,1,1,2.,0)
call XA(8,false,1,1,2.,0)
call XA(9,false,1,1,1.5,1093677120)
call XA(R4,false,1,1,1./ 16.,0)
call XA(T4,false,1,99,5.,0)
call XA(U4,false,1,1,3.,1093677088)
call XA(W4,false,1,1,2.,0)
call XA(16,false,1,1,3.,1093677088)
call XA(17,false,5,5,5.,1093677092)
call Cb(1,1096888368,false,false,false,.0,.0,.0)
call Cb(2,1096888369,true,false,false,.0,5.,.0)
call Cb(3,1096888370,false,false,true,.05,.0,256.)
call Cb(4,1096888371,true,true,false,.0,5.,512.)
call Cb(5,1096888372,false,false,true,.05,.0,256.)
call Cb(6,1096888373,true,true,false,10.,10.,512.)
call Cb(7,1096888374,true,false,false,10.,10.,.0)
call Cb(8,1096888375,true,false,false,10.,10.,512.)
call Hb(1,function aa,function ba)
call Hb(2,null,function Ca)
call Hb(3,function Fa,function Ha)
call Hb(4,null,function Aa)
call Hb(5,null,function ca)
call Hb(6,null,function Ka)
call Hb(7,null,function Ma)
call Hb(8,null,function Sa)
call Sc(1,100,1.,1,$A,5,25,$A,25,-1,0,50.,5,v4,1.,0,0)
call Sc(2,$DC,2.,2,$A,$B,55,20,50,-1,1,105.,5,v4,1.,$F,0)
call Sc(3,440,3.,3,5,22,110,40,75,-2,1,195.,5,v4,1.25,25,$A)
call Sc(5,440,3.,3,5,22,110,40,75,-3,2,200.,5,v4,1.,25,0)
call Sc(6,0,.0,0,1,0,0,0,0,-5,3,500.,5,v4,1.,0,0)
call Sc(4,400,2.,2,5,40,40,40,0,-1,1,50.,5,v4,1.,0,0)
call mD(1,1831874608,1097674800,1097412656,1,true,false,60.,function ib)
call mD(2,1831874609,1097674801,1097412657,2,true,false,60.,function rb)
call mD(3,1831874610,1097674802,1097412658,5,true,false,60.,function tb)
call mD(4,1831874611,1097674803,1097412659,4,true,false,60.,function fb)
call mD(5,1831874612,1097674804,1097412660,3,true,false,60.,function xb)
call mD(6,1831874613,0,1093677139,0,false,true,60.,function Ab)
set i7[0]=dr()
call vD(1,1096953904,1097019440,1097084976,1378889776,500,function kb,1096953905,1097019441,1097084977,1378889777,500,function mb,1096953906,1097019442,1097084978,1378889778,$5DC,function nb)
call vD(2,1096953907,1097019443,1097084979,1378889779,$3E8,function sb,1096953908,1097019444,1097084980,1378889780,$3E8,null,1096953909,1097019445,1097084981,1378889781,$5DC,null)
call vD(3,1096953910,1097019446,1097084982,1378889782,$3E8,function ub,1096953911,1097019447,1097084983,1378889783,$5DC,function vb,1096953912,1097019448,1097084984,1378889784,$3E8,function wb)
call vD(5,1096953913,1097019449,1097084985,1378889785,$3E8,function yb,1096953889,1097019425,1097084961,1378889761,$3E8,function zb,1096953915,1097019451,1097084987,1378889787,$5DC,null)
call vD(6,1096953914,1097019450,1097084986,1378889786,$FA,function ab,1096953919,1097019455,1097084991,1378889791,$FA,function Bb,1096953920,1097019456,1097084992,1378889792,$FA,function bb)
call vD(4,1096953916,1097019452,1097084988,1378889788,0,null,1096953917,1097019453,1097084989,1378889789,0,function gb,1096953918,1097019454,1097084990,1378889790,0,null)
call yc(1,6,1097150512,1097216048,1097281584,null)
call yc(2,7,1097150513,1097216049,1097281585,null)
call yc(3,8,1097150514,1097216050,1097281586,null)
call bc(1,1097347120,1097478192,1097543728,1382101040,$FA,function La,1097347121,1097478193,1097543729,1382101041,500,null)
call bc(2,1097347122,1097478194,1097543730,1382101042,$FA,null,1097347123,1097478195,1097543731,1382101043,500,null)
call bc(3,1097347124,1097478196,1097543732,1382101044,$FA,function Ya,1097347125,1097478197,1097543733,1382101045,500,null)
call VD(1,6,1,2,3,1093677365,1093677366,1093677367,null)
call kv(1,1,2,3,5,1,0,1093677132,1093677360,null)
call kv(2,0,0,0,4,0,0,0,0,null)
call SetGameSpeed(MAP_SPEED_FASTEST)
call SetMapFlag(MAP_LOCK_SPEED,true)
call SetMapFlag(MAP_USE_HANDICAPS,false)
call SetAllyColorFilterState(0)
call SetCreepCampFilterState(false)
call EnableMinimapFilterButtons(false,false)
call EnableWorldFogBoundary(false)
call SetFloatGameState(GAME_STATE_TIME_OF_DAY,12.)
call SuspendTimeOfDay(true)
call SetMapMusic("Music",true,0)
call TriggerExecute(z8)
endfunction
function config takes nothing returns nothing
local player p
call SetMapName("TRIGSTR_001")
call SetMapDescription("TRIGSTR_904")
call SetPlayers(V)
call SetTeams(V)
set p=Player(0)
call DefineStartLocation(0,x8,y8)
call SetPlayerStartLocation(p,0)
call SetPlayerColor(p,ConvertPlayerColor(0))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,0)
set p=Player(1)
call DefineStartLocation(1,x8,y8)
call SetPlayerStartLocation(p,1)
call SetPlayerColor(p,ConvertPlayerColor(1))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(2)
call DefineStartLocation(2,x8,y8)
call SetPlayerStartLocation(p,2)
call SetPlayerColor(p,ConvertPlayerColor(2))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(3)
call DefineStartLocation(3,x8,y8)
call SetPlayerStartLocation(p,3)
call SetPlayerColor(p,ConvertPlayerColor(3))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(4)
call DefineStartLocation(4,x8,y8)
call SetPlayerStartLocation(p,4)
call SetPlayerColor(p,ConvertPlayerColor(4))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(5)
call DefineStartLocation(5,x8,y8)
call SetPlayerStartLocation(p,5)
call SetPlayerColor(p,ConvertPlayerColor(5))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(6)
call DefineStartLocation(6,x8,y8)
call SetPlayerStartLocation(p,6)
call SetPlayerColor(p,ConvertPlayerColor(6))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(7)
call DefineStartLocation(7,x8,y8)
call SetPlayerStartLocation(p,7)
call SetPlayerColor(p,ConvertPlayerColor(7))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(8)
call DefineStartLocation(8,x8,y8)
call SetPlayerStartLocation(p,8)
call SetPlayerColor(p,ConvertPlayerColor(8))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(9)
call DefineStartLocation(9,x8,y8)
call SetPlayerStartLocation(p,9)
call SetPlayerColor(p,ConvertPlayerColor(9))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=null
endfunction
function w3 takes nothing returns boolean
local integer m=xq
local integer T=yq
local integer Zq=tr()
set bk[Zq]=(T)
set Ck[Zq]=m
set ek[m]=Zq
call UnitAddAbility(Dj[m],Wm[bk[Zq]])
set Cq=Zq
return true
endfunction
function x3 takes nothing returns boolean
local integer Zq=Bq
return true
endfunction
function y3 takes nothing returns boolean
local integer Zq=Bq
local integer a=0
set a=0
loop
call bv(Qd[Rd[Zq]+a])
set a=a+1
exitwhen a==608
endloop
call Ot("Neighbors are ready!")
return true
endfunction
function z3 takes nothing returns boolean
local integer Zq=Bq
local integer a=0
loop
set Qd[Rd[Zq]+a]=Bv(Zq,a)
set a=a+1
exitwhen a==608
endloop
call ar(Zq)
call Ot("Cells are ready!")
return true
endfunction
function A3 takes nothing returns boolean
local integer Zq=Bq
local integer a=0
loop
call yr(Qd[Rd[Zq]+a])
set a=a+1
exitwhen a==608
endloop
call Ot("Cells destroyed!")
return true
endfunction
function B3 takes nothing returns boolean
local integer Zq=Bq
local integer a=0
local boolean C3=not ie[Zq]
local integer c
loop
set c=Qd[Rd[Zq]+a]
set bd[(c)]=true
if C3 then
set xd[(c)]=(yd[c])
set yd[(c)]=(0)
endif
set a=a+1
exitwhen a==t4
endloop
return true
endfunction
function c3 takes nothing returns boolean
local integer Zq=Bq
local integer q=xq
local integer c
local integer d
set je[Zq]=true
loop
set c=cv(q)
set d=yd[c]+1
call Fv(q,Cd[c],d)
call Fv(q,Dd[c],d)
call Fv(q,Ed[c],d)
call Fv(q,Fd[c],d)
exitwhen(Kd[(q)]==0)
endloop
set ie[Zq]=bd[Qd[Rd[Zq]]]
set je[Zq]=false
return true
endfunction
function D3 takes nothing returns boolean
local integer Zq=Bq
set bq=Lo[Nj[Zq]]*(gd[hd[(Mo[Nj[Zq]])]+(Uj[Zq])])
return true
endfunction
function E3 takes nothing returns boolean
local integer Zq=Bq
set bq=.0
return true
endfunction
function F3 takes nothing returns boolean
local integer Zq=Bq
set bq=.0
return true
endfunction
function G3 takes nothing returns boolean
local integer Zq=Bq
return true
endfunction
function H3 takes nothing returns boolean
local integer Zq=Bq
local integer m=xq
call wB(Zq,m)
return true
endfunction
function I3 takes nothing returns boolean
local integer Zq=Bq
local integer Rr=xq
if yB(Zq,Rr,Oj[Zq])then
call xB(Zq)
else
call TimerStart(Pj[(Zq)],O4,false,gk)
endif
return true
endfunction
function l3 takes nothing returns boolean
local integer Zq=Bq
local integer m=xq
set Rj[Zq]=Rj[Zq]+1
call SetItemCharges(Yj[Zq],Rj[Zq])
call vx(cj[Zq])
if do[hh[m]]>0 then
call GB(m,Zq)
endif
return true
endfunction
function J3 takes nothing returns boolean
local integer Zq=Bq
if Zo[Fj[Zq]]then
call ShowImage(Qj[Zq],true)
endif
return true
endfunction
function K3 takes nothing returns boolean
local integer Zq=Bq
if Zo[Fj[Zq]]then
call ShowImage(Qj[Zq],false)
endif
return true
endfunction
function L3 takes nothing returns boolean
local integer Zq=Bq
local integer Zv=Fe[Zq]
local integer jw
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=He[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=Je[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=Ne[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=Re[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=Ve[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=Ye[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=ef[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=hf[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=nf[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=rf[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
set Zv=vf[Zq]
loop
exitwhen Zv==0
set jw=ze[Zv]
call ls(we[Zv])
set Zv=jw
endloop
call Ot("MinionBuffStorage "+I2S(Zq)+" is deleted.")
return true
endfunction
function M3 takes nothing returns boolean
local integer Zq=Bq
local integer a=0
loop
call DestroyImage(Zf[dg[Zq]+a])
set Zf[dg[Zq]+a]=null
set a=a+1
exitwhen a==120
endloop
return true
endfunction
function N3 takes nothing returns boolean
local integer Zq=Bq
call ua(Zq)
call Ot("MinionAbility "+I2S(Zq)+" is deleted.")
return true
endfunction
function O3 takes nothing returns boolean
local integer Zq=Bq
return true
endfunction
function P3 takes nothing returns boolean
local integer Zq=Bq
local integer gs=xq
return true
endfunction
function Q3 takes nothing returns boolean
local integer Zq=Bq
local integer gs=xq
local integer is=yq
local integer js=zq
set cq=false
return true
endfunction
function R3 takes nothing returns boolean
local integer Zq=Bq
local real r=Aq
if r>.0 and not(Ji[Zq])then
set r=TimerGetRemaining(Ki[Zq])-r
if r<=.0 then
call TimerStart((Ki[Zq]),.0,false,null)
call es(Zq)
else
call TimerStart(Ki[Zq],r,false,function va)
endif
endif
return true
endfunction
function S3 takes nothing returns boolean
local integer Zq=Bq
return true
endfunction
function T3 takes nothing returns boolean
local integer Zq=Bq
return true
endfunction
function U3 takes nothing returns boolean
local integer Zq=Bq
call oa(Zq)
call DestroyImage(ci[Zq])
set ci[Zq]=null
set Ci[Zq]=null
return true
endfunction
function V3 takes nothing returns boolean
local integer Zq=Bq
call RemoveUnit(vi[Zq])
set vi[Zq]=null
call Ot("MinionDummy "+I2S(Zq)+" is deleted.")
return true
endfunction
function W3 takes nothing returns boolean
local integer Zq=Bq
if Yg[Zq]!=0 then
call ps(Yg[Zq])
endif
call SetUnitUserData(Vg[Zq],0)
set Vg[Zq]=null
return true
endfunction
function X3 takes nothing returns boolean
local integer Zq=Bq
call Nu(mg[Sg[Zq]],Zq)
call kx(Sg[Zq],Rg[Zq])
call Fx(Sg[Zq],Un[hh[Zq]])
call wx(Sg[Zq],-Xn[hh[Zq]])
call yx(Sg[Zq],Tn[hh[Zq]])
call Bx(Sg[Zq],I2S(Tn[hh[Zq]]),Vg[Zq])
call DestroyEffect(AddSpecialEffect(("Abilities\\Spells\\Other\\Transmute\\PileofGold.mdx"),((GetUnitX(Vg[Zq]))*1.),((GetUnitY(Vg[Zq]))*1.)))
call RemoveUnit(Vg[Zq])
call Gs(Zq)
call Ot("Minion "+I2S(Zq)+" is sold.")
return true
endfunction
function Y3 takes nothing returns boolean
local integer Zq=Bq
set bq=ko[hh[Zq]]
return true
endfunction
function Z3 takes nothing returns boolean
local integer Zq=Bq
set Cq=mo[hh[Zq]]
return true
endfunction
function dF takes nothing returns boolean
local integer Zq=Bq
set Cq=no[hh[Zq]]
return true
endfunction
function eF takes nothing returns boolean
local integer Zq=Bq
set cq=(Ue[Xg[(Zq)]])
return true
endfunction
function fF takes nothing returns boolean
local integer Zq=Bq
set cq=false
return true
endfunction
function gF takes nothing returns boolean
local integer Zq=Bq
set cq=false
return true
endfunction
function hF takes nothing returns boolean
local integer Zq=Bq
set cq=false
return true
endfunction
function iF takes nothing returns boolean
local integer Zq=Bq
set cq=false
return true
endfunction
function jF takes nothing returns boolean
local integer Zq=Bq
local real bs=Aq
set bq=jz(Zq,bs)
return true
endfunction
function kF takes nothing returns boolean
local integer Zq=Bq
local real bs=Aq
set bq=kz(Zq,bs)
return true
endfunction
function mF takes nothing returns boolean
local integer Zq=Bq
local real bs=Aq
set bq=(fh[(Zq)]*((bs)*1.)/ 1000.)
return true
endfunction
function nF takes nothing returns boolean
local integer Zq=Bq
local boolean Es=aq
call SetUnitExploded(Vg[Zq],Es)
call KillUnit(Vg[Zq])
if Es or Rn[hh[Zq]]then
call tz(Zq)
else
set Zg[Zq]=false
call sz(Zq)
call Su(qg[Tg[Zq]],Zq)
call ms(Yg[Zq])
set sh[Zq]=GetUnitX(Vg[Zq])
set th[Zq]=GetUnitY(Vg[Zq])
set uh[Zq]=GetUnitFacing(Vg[Zq])
call TimerStart(xh[Zq],15.,false,function az)
endif
return true
endfunction
function oF takes nothing returns boolean
local integer Zq=Bq
call Oz(Zq)
call RemoveSavedInteger(n7,Wg[bh[Zq]],Bh[Zq])
call UnitRemoveAbility(Vg[bh[Zq]],ri[Dh[Zq]])
set Ch[Zq]=null
return true
endfunction
function pF takes nothing returns boolean
local integer Zq=Bq
set cq=true
return true
endfunction
function qF takes nothing returns boolean
local integer Zq=Bq
call Zy(bh[Zq],Gh[Zq])
if gi[Zq]then
call uw(Xg[(bh[Zq])],(Hh[Zq]))
endif
set Bq=Zq
return true
endfunction
function rF takes nothing returns boolean
local integer Zq=Bq
call dw(Xg[(bh[Zq])],(Gh[Zq]))
set Bq=Zq
return true
endfunction
function sF takes nothing returns boolean
local integer Zq=Bq
call vy(bh[Zq],kj[bf[ch[Zq]]],"Effects\\RestoreHealthGreen.mdx","origin")
set cq=Eh[Zq]==0
return true
endfunction
function tF takes nothing returns boolean
local integer Zq=Bq
call fz(bh[Zq],Gh[Zq])
call Zy(bh[Zq],Hh[Zq])
set Bq=Zq
return true
endfunction
function uF takes nothing returns boolean
local integer Zq=Bq
call Dw(Xg[(bh[Zq])],(Gh[Zq]))
if fi[Zq]then
call uw(Xg[(bh[Zq])],(Hh[Zq]))
endif
call AddUnitAnimationProperties(Vg[bh[Zq]],"Defend",false)
set Bq=Zq
return true
endfunction
function vF takes nothing returns boolean
local integer Zq=Bq
if oo[po[sg[tg[ch[Zq]]+2]]+2]then
call HA(bh[Zq],be[(Gh[Zq])]*.5)
endif
set cq=true
return true
endfunction
function wF takes nothing returns boolean
local integer Zq=Bq
call Yy(bh[Zq],Gh[Zq])
set Bq=Zq
return true
endfunction
function xF takes nothing returns boolean
local integer Zq=Bq
call lw(Xg[(bh[Zq])],(Gh[Zq]))
set Bq=Zq
return true
endfunction
function yF takes nothing returns boolean
local integer Zq=Bq
local integer Vw=sg[tg[ch[Zq]]+4]
if oo[po[Vw]+2]then
call ty(bh[Zq],be[(Gh[Zq])]*.5,"Effects\\RestoreHealthGreen.mdx","origin")
endif
if oo[po[Vw]+0]then
call CA(bh[Zq],Ch[Zq],ch[Zq],40)
endif
set cq=true
return true
endfunction
function zF takes nothing returns boolean
local integer Zq=Bq
call ez(bh[Zq],Gh[Zq])
set Bq=Zq
return true
endfunction
function AF takes nothing returns boolean
local integer Zq=Bq
call RemoveUnit(Mh[Zq])
set Mh[Zq]=null
call Ot("TowerAttacker "+I2S(Zq)+" is deleted.")
return true
endfunction
function aF takes nothing returns boolean
local integer Zq=Bq
call gz(bh[Zq],Gh[Zq])
set Bq=Zq
return true
endfunction
function BF takes nothing returns boolean
local integer Zq=Bq
if(H9[I9[(ag[Bg[ch[Zq]]+2])]+(0)])then
call QA(bh[Zq],.15)
endif
set cq=true
return true
endfunction
function bF takes nothing returns boolean
local integer Zq=Bq
call gz(bh[Zq],Gh[Zq])
set Bq=Zq
return true
endfunction
function CF takes nothing returns boolean
local integer Zq=Bq
call dw(Xg[(bh[Zq])],(Gh[Zq]))
set Bq=Zq
return true
endfunction
function cF takes nothing returns boolean
local integer Zq=Bq
if UnitAlive(Ch[Zq])then
call ks(Yg[(GetUnitUserData(Ch[Zq]))],1.)
set cq=Eh[Zq]==0
return true
endif
set cq=true
return true
endfunction
function DF takes nothing returns boolean
local integer Zq=Bq
local integer gs=xq
local integer is=yq
local integer js=zq
if Sg[gs]==Sg[Ii[Zq]]then
call EA(gs,Ii[Zq])
call ya(Zq)
set cq=true
return true
endif
set cq=false
return true
endfunction
function EF takes nothing returns boolean
local integer Zq=Bq
local integer gs=xq
local integer is=yq
local integer js=zq
if js==1227894834 then
call lA(gs)
call ya(Zq)
set cq=true
return true
endif
set cq=false
return true
endfunction
function FF takes nothing returns boolean
local integer Zq=Bq
if Ri[Zq]!=0 then
call sa(Ri[Zq])
endif
set Bq=Zq
return true
endfunction
function GF takes nothing returns boolean
local integer Zq=Bq
local integer m=Ii[Zq]
set Ti=m
set Ui=false
call GroupEnumUnitsInRange(bj_lastCreatedGroup,GetUnitX(Vg[m]),GetUnitY(Vg[m]),yk[li[Zq]],Qi)
if oo[po[hh[m]]+1]then
set Si[Zq]=Xt(Ui)*nk
call Gy(m)
endif
return true
endfunction
function HF takes nothing returns boolean
local integer Zq=Bq
call sa(Ri[Zq])
set Ri[Zq]=0
call PauseTimer(Ki[Zq])
return true
endfunction
function IF takes nothing returns boolean
local integer Zq=Bq
set Ri[Zq]=qa(Vg[Ii[Zq]],yk[li[Zq]])
call ya(Zq)
return true
endfunction
function lF takes nothing returns boolean
local integer Zq=Bq
if gj[Zq]!=0 then
call sa(gj[Zq])
endif
set Bq=Zq
return true
endfunction
function JF takes nothing returns boolean
local integer Zq=Bq
local integer m=Ii[Zq]
local integer KF=bf[Sg[m]]
local integer By
set hj=Sg[m]
set ij=0
call GroupEnumUnitsInRange(bj_lastCreatedGroup,GetUnitX(Vg[m]),GetUnitY(Vg[m]),yk[li[Zq]],Yi)
set By=ij*Wi[KF]
if By>Xi[KF]then
set By=Xi[KF]
endif
set fj[Zq]=By
set ej[Zq]=By
call Gy(m)
return true
endfunction
function LF takes nothing returns boolean
local integer Zq=Bq
if Zi[Zq]then
call AddUnitAnimationProperties(Vg[Ii[Zq]],"Defend",false)
else
call sa(gj[Zq])
set gj[Zq]=0
call PauseTimer(Ki[Zq])
endif
return true
endfunction
function MF takes nothing returns boolean
local integer Zq=Bq
if Zi[Zq]then
call AddUnitAnimationProperties(Vg[Ii[Zq]],"Defend",true)
else
set gj[Zq]=qa(Vg[Ii[Zq]],yk[li[Zq]])
call ya(Zq)
endif
return true
endfunction
function NF takes nothing returns boolean
local integer Zq=Bq
call ma(mj[Zq])
set Bq=Zq
return true
endfunction
function OF takes nothing returns boolean
local integer Zq=Bq
local integer gs=xq
local integer is=yq
local integer js=zq
if gs!=Ii[Zq]and Sg[gs]==Sg[Ii[Zq]]and ny(gs)<=(1.-kj[bf[Sg[Ii[Zq]]]])then
call ia(mj[Zq],GetUnitX(Vg[Ii[Zq]]),GetUnitY(Vg[Ii[Zq]]))
call ja(mj[Zq],GetUnitX(Vg[gs]),GetUnitY(Vg[gs]))
call ya(Zq)
set cq=true
return true
endif
set cq=false
return true
endfunction
function PF takes nothing returns boolean
local integer Zq=Bq
local integer gs=xq
local integer p=Sg[Ii[Zq]]
if gs!=Ii[Zq]and Sg[gs]==p then
call vy(gs,kj[bf[Sg[Ii[Zq]]]],"Effects\\RestoreHealthGreen.mdx","origin")
if(H9[I9[(ag[Bg[p]+1])]+(1)])then
call NA(gs,Ii[Zq])
endif
endif
return true
endfunction
function QF takes nothing returns boolean
local integer Zq=Bq
local integer gs=xq
local integer is=yq
local integer js=zq
call SA(gs)
if(H9[I9[(ag[Bg[Sg[gs]]+2])]+(1)])then
set gs=Oa(Zq)
if gs!=0 then
call UA(gs,Ii[Zq])
endif
endif
call ya(Zq)
set cq=true
return true
endfunction
function RF takes nothing returns boolean
local integer Zq=Bq
call Ra(Zq)
set Bq=Zq
return true
endfunction
function SF takes nothing returns boolean
local integer Zq=Bq
call Ua(Zq)
return true
endfunction
function TF takes nothing returns boolean
local integer Zq=Bq
call dw(Xg[(bh[Zq])],(Gh[Zq]))
set Bq=Zq
return true
endfunction
function UF takes nothing returns boolean
local integer Zq=Bq
local integer dd
if(GetUnitTypeId((Ch[Zq]))!=0)then
set dd=GetUnitUserData(Ch[Zq])
set cq=sB(dd,bh[Zq],Mk[(ek[dd])],1227894841,false)and Eh[Zq]==0
return true
endif
set cq=true
return true
endfunction
function VF takes nothing returns boolean
local integer Zq=Bq
call Zy(bh[Zq],Gh[Zq])
set Bq=Zq
return true
endfunction
function WF takes nothing returns boolean
local integer Zq=Bq
call fz(bh[Zq],Gh[Zq])
set Bq=Zq
return true
endfunction
function XF takes nothing returns boolean
local integer Zq=Bq
call Zy(bh[Zq],Gh[Zq])
set Bq=Zq
return true
endfunction
function YF takes nothing returns boolean
local integer Zq=Bq
call dz(bh[Zq],Gh[Zq])
set Bq=Zq
return true
endfunction
function ZF takes nothing returns boolean
local integer Zq=Bq
call fz(bh[Zq],Gh[Zq])
set Bq=Zq
return true
endfunction
function dG takes nothing returns boolean
local integer Zq=Bq
call Yy(bh[Zq],Gh[Zq])
set Bq=Zq
return true
endfunction
function eG takes nothing returns boolean
local integer Zq=Bq
set bq=(ko[hh[(Zq)]])+Oi[(Yg[Zq])]
return true
endfunction
function fG takes nothing returns boolean
local integer Zq=Bq
set Cq=(mo[hh[(Zq)]])+Ni[(Yg[Zq])]
return true
endfunction
function gG takes nothing returns boolean
local integer Zq=Bq
if oo[po[hh[Zq]]+2]then
call jb(mk[Zq])
endif
call Zx(Zq)
return true
endfunction
function hG takes nothing returns boolean
local integer Zq=Bq
set Cq=(mo[hh[(Zq)]])+Si[(Yg[Zq])]
return true
endfunction
function iG takes nothing returns boolean
local integer Zq=Bq
set Cq=(no[hh[(Zq)]])+Si[(Yg[Zq])]
return true
endfunction
function jG takes nothing returns boolean
local integer Zq=Bq
set bq=(ko[hh[(Zq)]])+dj[(Yg[Zq])]
return true
endfunction
function kG takes nothing returns boolean
local integer Zq=Bq
set Cq=(mo[hh[(Zq)]])+fj[(Yg[Zq])]
return true
endfunction
function mG takes nothing returns boolean
local integer Zq=Bq
set Cq=(no[hh[(Zq)]])+ej[(Yg[Zq])]
return true
endfunction
function nG takes nothing returns boolean
local integer Zq=Bq
set cq=(Ue[Xg[((Zq))]])or(oo[po[hh[Zq]]+2]and Zi[(Yg[Zq])])
return true
endfunction
function oG takes nothing returns boolean
local integer Zq=Bq
set cq=Zi[(Yg[Zq])]
return true
endfunction
function pG takes nothing returns boolean
local integer Zq=Bq
local real bs=Aq
if Zi[(Yg[Zq])]then
set bq=.0
return true
endif
set bq=jz(Zq,bs)
return true
endfunction
function qG takes nothing returns boolean
local integer Zq=Bq
call Lb(Zq)
set Ek[Zq]=null
set Bq=Zq
return true
endfunction
function rG takes nothing returns boolean
local integer Zq=Bq
local integer cz=Uj[Ck[Zq]]
if cz<=Ym[bk[Zq]]then
set Hk[Zq]=Hk[Zq]+.1
set Ik[Zq]=Ik[Zq]+.2
if cz==Xm[bk[Zq]]then
set Hk[Zq]=Hk[Zq]+1.
elseif cz==Ym[bk[Zq]]then
set Ik[Zq]=Ik[Zq]+2.
endif
call uB(Ck[Zq])
endif
return true
endfunction
function sG takes nothing returns boolean
local integer Zq=Bq
set Mk[Zq]=Mk[Zq]+Mk[Zq]*.075
return true
endfunction
function tG takes nothing returns boolean
local integer Zq=Bq
if Ok[Zq]!=0 then
call mA(Ok[Zq])
endif
set Bq=Zq
return true
endfunction
function uG takes nothing returns boolean
local integer Zq=Bq
if Uj[Ck[Zq]]==Xm[bk[Zq]]then
call hA(Oj[Ck[Zq]])
elseif Uj[Ck[Zq]]==Ym[bk[Zq]]then
set Ok[Zq]=Zz(Ck[Zq])
endif
return true
endfunction
function vG takes nothing returns boolean
local integer Zq=Bq
call mA(Pk[Zq])
call RemoveUnit(Qk[Zq])
call Yb(Zq)
set Qk[Zq]=null
set Rk[Zq]=null
set Vk[Zq]=null
set Bq=Zq
return true
endfunction
function wG takes nothing returns boolean
local integer Zq=Bq
call jC(Zq)
set Bq=Zq
return true
endfunction
function xG takes nothing returns boolean
local integer Zq=Bq
set Xk[Zq]=Xk[Zq]+1
if Uj[Ck[Zq]]==Xm[bk[Zq]]then
set Xk[Zq]=Xk[Zq]+1
elseif Uj[Ck[Zq]]==Ym[bk[Zq]]then
set Xk[Zq]=Xk[Zq]+3
endif
return true
endfunction
function yG takes nothing returns boolean
local integer Zq=Bq
call oC(Zq)
call DestroyImage(mm[Zq])
set mm[Zq]=null
set pm[Zq]=null
call rC(Zq)
set Bq=Zq
return true
endfunction
function zG takes nothing returns boolean
local integer Zq=Bq
if Uj[Ck[Zq]]==Xm[bk[Zq]]then
set jm[Zq]=jm[Zq]*2.
set om[Zq]=om[Zq]*2.
set sm[Zq]=sm[Zq]+5
elseif Uj[Ck[Zq]]==Ym[bk[Zq]]then
set km[Zq]=km[Zq]+128.
set nm[Zq]=nm[Zq]*2
set sm[Zq]=sm[Zq]+5
call DestroyImage(mm[Zq])
set mm[Zq]=jB(Ck[Zq],km[Zq],dm,em,fm,im[Zq]==0)
elseif Uj[Ck[Zq]]==Zm[bk[Zq]]and TimerGetRemaining(um[Zq])>=3. then
call TimerStart(um[Zq],3.,true,hm)
endif
return true
endfunction
function AG takes nothing returns boolean
local integer Zq=Bq
call JC(Zq)
call LC(Zq)
set Bq=Zq
return true
endfunction
function aG takes nothing returns boolean
local integer Zq=Bq
call TC(Zq)
set Tm[Zq]=null
set Bq=Zq
return true
endfunction
function BG takes nothing returns boolean
local integer Zq=Bq
set Pm[Zq]=Pm[Zq]+.01
set Qm[Zq]=Qm[Zq]+Nm
return true
endfunction
function bG takes nothing returns boolean
local integer Zq=Bq
set bq=Nb(ek[Zq])+pB(Zq)
return true
endfunction
function CG takes nothing returns boolean
local integer Zq=Bq
local integer m=xq
call wB(Zq,m)
call Qb(ek[Zq])
return true
endfunction
function cG takes nothing returns boolean
local integer Zq=Bq
call rB(Zq)
if St(Uj[Zq],5)then
set Mj[Zq]=Mj[Zq]+1
endif
return true
endfunction
function DG takes nothing returns boolean
local integer Zq=Bq
local integer m=xq
if wB(Zq,m)then
call Ub(ek[Zq],m)
endif
return true
endfunction
function EG takes nothing returns boolean
local integer Zq=Bq
local integer m=xq
if wB(Zq,m)then
call VB((m),Ck[(ek[Zq])],Nk)
endif
return true
endfunction
function FG takes nothing returns boolean
local integer Zq=Bq
local integer Rr=xq
if Wb(ek[Zq],Rr)then
call xB(Zq)
else
call TimerStart(Pj[(Zq)],O4,false,gk)
endif
return true
endfunction
function GG takes nothing returns boolean
local integer Zq=Bq
set bq=Lo[Nj[Zq]]+.5*Uj[Zq]
return true
endfunction
function HG takes nothing returns boolean
local integer Zq=Bq
set bq=Tk[(ek[Zq])]+pB(Zq)
return true
endfunction
function IG takes nothing returns boolean
local integer Zq=Bq
local integer m=xq
call hC(ek[Zq],m)
return true
endfunction
function lG takes nothing returns boolean
local integer Zq=Bq
call BC(ek[Zq])
return true
endfunction
function JG takes nothing returns boolean
local integer Zq=Bq
call bC(ek[Zq])
return true
endfunction
function KG takes nothing returns boolean
local integer Zq=Bq
set bq=cm[(ek[Zq])]+oB(Zq)
return true
endfunction
function LG takes nothing returns boolean
local integer Zq=Bq
set bq=Cm[(ek[Zq])]+pB(Zq)
return true
endfunction
function MG takes nothing returns boolean
local integer Zq=Bq
local integer m=xq
call HB(Zq,m)
call HC(ek[Zq])
return true
endfunction
function NG takes nothing returns boolean
local integer Zq=Bq
local integer m=xq
call IC(ek[Zq],m)
return true
endfunction
function OG takes nothing returns boolean
local integer Zq=Bq
set bq=Hm[(ek[Zq])]+pB(Zq)
return true
endfunction
function PG takes nothing returns boolean
local integer Zq=Bq
local integer m=xq
call wB(Zq,m)
call NC(ek[Zq])
return true
endfunction
function QG takes nothing returns boolean
local integer Zq=Bq
local integer Rr=xq
local integer m=rA(Rr)
call OC(ek[Zq],GetUnitX(Vg[m]),GetUnitY(Vg[m]))
call xB(Zq)
return true
endfunction
function RG takes nothing returns boolean
local integer SG=xq
local integer is=yq
local integer js=zq
local boolean TG=not iz(SG,js)
local integer a
local integer YC
local integer UG
if TG then
set TG=za(Yg[SG],SG,is,js)
set YC=og[Tg[SG]]
set a=0
loop
exitwhen TG or a>u9[YC]
set UG=Yg[(s9[t9[(YC)]+(a)])]
if uk[li[UG]]then
set TG=za(UG,SG,is,js)
endif
set a=a+1
endloop
endif
return true
endfunction
function VG takes nothing returns boolean
local integer Zq=xq
local integer a=0
set a=0
loop
call bv(Qd[Rd[Zq]+a])
set a=a+1
exitwhen a==608
endloop
call Ot("Neighbors are ready!")
return true
endfunction
function WG takes nothing returns boolean
local integer Zq=xq
local integer a=0
loop
set Qd[Rd[Zq]+a]=Bv(Zq,a)
set a=a+1
exitwhen a==608
endloop
call ar(Zq)
call Ot("Cells are ready!")
return true
endfunction
function XG takes nothing returns boolean
local integer Zq=xq
local integer a=0
loop
call yr(Qd[Rd[Zq]+a])
set a=a+1
exitwhen a==608
endloop
call Ot("Cells destroyed!")
return true
endfunction
function YG takes nothing returns boolean
local integer Zq=xq
local integer q=yq
local integer c
local integer d
set je[Zq]=true
loop
set c=cv(q)
set d=yd[c]+1
call Fv(q,Cd[c],d)
call Fv(q,Dd[c],d)
call Fv(q,Ed[c],d)
call Fv(q,Fd[c],d)
exitwhen(Kd[(q)]==0)
endloop
set ie[Zq]=bd[Qd[Rd[Zq]]]
set je[Zq]=false
return true
endfunction
function ZG takes nothing returns boolean
local integer Zq=xq
local integer a=0
local boolean C3=not ie[Zq]
local integer c
loop
set c=Qd[Rd[Zq]+a]
set bd[(c)]=true
if C3 then
set xd[(c)]=(yd[c])
set yd[(c)]=(0)
endif
set a=a+1
exitwhen a==t4
endloop
return true
endfunction
function d6 takes nothing returns boolean
local integer Zq=xq
local integer a=0
loop
call DestroyImage(Zf[dg[Zq]+a])
set Zf[dg[Zq]+a]=null
set a=a+1
exitwhen a==120
endloop
return true
endfunction
function e6 takes nothing returns boolean
local integer Zq=xq
local boolean Es=aq
call SetUnitExploded(Vg[Zq],Es)
call KillUnit(Vg[Zq])
if Es or Rn[hh[Zq]]then
call tz(Zq)
else
set Zg[Zq]=false
call sz(Zq)
call Su(qg[Tg[Zq]],Zq)
call ms(Yg[Zq])
set sh[Zq]=GetUnitX(Vg[Zq])
set th[Zq]=GetUnitY(Vg[Zq])
set uh[Zq]=GetUnitFacing(Vg[Zq])
call TimerStart(xh[Zq],15.,false,function az)
endif
return true
endfunction
function f6 takes nothing returns boolean
local integer Zq=xq
local integer Rr=yq
if yB(Zq,Rr,Oj[Zq])then
call xB(Zq)
else
call TimerStart(Pj[(Zq)],O4,false,gk)
endif
return true
endfunction
function v3 takes nothing returns nothing
set sq=CreateTrigger()
call TriggerAddCondition(sq,Condition(function w3))
set Pp[85]=CreateTrigger()
set Pp[89]=Pp[85]
set Pp[92]=Pp[85]
set Pp[93]=Pp[85]
call TriggerAddCondition(Pp[85],Condition(function x3))
call TriggerAddAction(Pp[85],function x3)
set eq[85]=null
set eq[87]=null
set eq[92]=null
set fq=CreateTrigger()
call TriggerAddCondition(fq,Condition(function y3))
call TriggerAddAction(fq,function y3)
set gq=CreateTrigger()
call TriggerAddCondition(gq,Condition(function z3))
call TriggerAddAction(gq,function z3)
set hq=CreateTrigger()
call TriggerAddCondition(hq,Condition(function A3))
call TriggerAddAction(hq,function A3)
set iq=CreateTrigger()
call TriggerAddCondition(iq,Condition(function B3))
call TriggerAddAction(iq,function B3)
set jq=CreateTrigger()
call TriggerAddCondition(jq,Condition(function c3))
call TriggerAddAction(jq,function c3)
set Rp[31]=null
set Rp[32]=null
set Rp[33]=null
set Rp[34]=null
set Hp[70]=CreateTrigger()
set Hp[96]=Hp[70]
set Hp[97]=Hp[70]
set Hp[98]=Hp[70]
set Hp[99]=Hp[70]
set Hp[101]=Hp[70]
set Hp[102]=Hp[70]
set Hp[103]=Hp[70]
set Hp[104]=Hp[70]
set Hp[105]=Hp[70]
call TriggerAddCondition(Hp[70],Condition(function D3))
call TriggerAddAction(Hp[70],function D3)
set Ip[70]=CreateTrigger()
set Ip[96]=Ip[70]
set Ip[97]=Ip[70]
set Ip[98]=Ip[70]
set Ip[99]=Ip[70]
set Ip[100]=Ip[70]
set Ip[101]=Ip[70]
set Ip[102]=Ip[70]
set Ip[104]=Ip[70]
set Ip[105]=Ip[70]
call TriggerAddCondition(Ip[70],Condition(function E3))
call TriggerAddAction(Ip[70],function E3)
set lp[70]=CreateTrigger()
set lp[96]=lp[70]
set lp[98]=lp[70]
set lp[99]=lp[70]
set lp[101]=lp[70]
set lp[102]=lp[70]
set lp[105]=lp[70]
call TriggerAddCondition(lp[70],Condition(function F3))
call TriggerAddAction(lp[70],function F3)
set Jp[70]=CreateTrigger()
set Jp[96]=Jp[70]
set Jp[97]=Jp[70]
set Jp[99]=Jp[70]
set Jp[100]=Jp[70]
set Jp[101]=Jp[70]
set Jp[102]=Jp[70]
set Jp[103]=Jp[70]
set Jp[104]=Jp[70]
set Jp[105]=Jp[70]
call TriggerAddCondition(Jp[70],Condition(function G3))
call TriggerAddAction(Jp[70],function G3)
set Kp[70]=CreateTrigger()
set Kp[96]=Kp[70]
set Kp[101]=Kp[70]
set Kp[102]=Kp[70]
set Kp[105]=Kp[70]
call TriggerAddCondition(Kp[70],Condition(function H3))
call TriggerAddAction(Kp[70],function H3)
set Lp[70]=CreateTrigger()
set Lp[96]=Lp[70]
set Lp[97]=Lp[70]
set Lp[98]=Lp[70]
set Lp[100]=Lp[70]
set Lp[101]=Lp[70]
set Lp[102]=Lp[70]
set Lp[103]=Lp[70]
set Lp[105]=Lp[70]
call TriggerAddCondition(Lp[70],Condition(function I3))
call TriggerAddAction(Lp[70],function I3)
set Mp[70]=CreateTrigger()
set Mp[96]=Mp[70]
set Mp[97]=Mp[70]
set Mp[98]=Mp[70]
set Mp[99]=Mp[70]
set Mp[100]=Mp[70]
set Mp[101]=Mp[70]
set Mp[102]=Mp[70]
set Mp[104]=Mp[70]
set Mp[105]=Mp[70]
call TriggerAddCondition(Mp[70],Condition(function l3))
call TriggerAddAction(Mp[70],function l3)
set Np[70]=CreateTrigger()
set Np[96]=Np[70]
set Np[97]=Np[70]
set Np[98]=Np[70]
set Np[99]=Np[70]
set Np[100]=Np[70]
set Np[101]=Np[70]
set Np[103]=Np[70]
set Np[104]=Np[70]
set Np[105]=Np[70]
call TriggerAddCondition(Np[70],Condition(function J3))
call TriggerAddAction(Np[70],function J3)
set Op[70]=CreateTrigger()
set Op[96]=Op[70]
set Op[97]=Op[70]
set Op[98]=Op[70]
set Op[99]=Op[70]
set Op[100]=Op[70]
set Op[101]=Op[70]
set Op[103]=Op[70]
set Op[104]=Op[70]
set Op[105]=Op[70]
call TriggerAddCondition(Op[70],Condition(function K3))
call TriggerAddAction(Op[70],function K3)
set Zp[70]=null
set Zp[96]=null
set Zp[97]=null
set Zp[98]=null
set Zp[99]=null
set Zp[100]=null
set Zp[101]=null
set Zp[102]=null
set Zp[103]=null
set Zp[104]=null
set Zp[105]=null
set kq=CreateTrigger()
call TriggerAddCondition(kq,Condition(function L3))
set mq=CreateTrigger()
call TriggerAddCondition(mq,Condition(function M3))
call TriggerAddAction(mq,function M3)
set Xp[61]=CreateTrigger()
set Xp[62]=Xp[61]
set Xp[63]=Xp[61]
set Xp[64]=Xp[61]
set Xp[68]=Xp[61]
call TriggerAddCondition(Xp[61],Condition(function N3))
set cp[61]=CreateTrigger()
set cp[62]=cp[61]
set cp[63]=cp[61]
set cp[64]=cp[61]
set cp[67]=cp[61]
set cp[68]=cp[61]
call TriggerAddCondition(cp[61],Condition(function O3))
call TriggerAddAction(cp[61],function O3)
set Dp[61]=CreateTrigger()
set Dp[62]=Dp[61]
set Dp[63]=Dp[61]
set Dp[64]=Dp[61]
set Dp[65]=Dp[61]
set Dp[66]=Dp[61]
set Dp[68]=Dp[61]
set Dp[69]=Dp[61]
call TriggerAddCondition(Dp[61],Condition(function P3))
call TriggerAddAction(Dp[61],function P3)
set Ep[61]=CreateTrigger()
set Ep[63]=Ep[61]
set Ep[65]=Ep[61]
set Ep[66]=Ep[61]
set Ep[69]=Ep[61]
call TriggerAddCondition(Ep[61],Condition(function Q3))
call TriggerAddAction(Ep[61],function Q3)
set rq=CreateTrigger()
call TriggerAddCondition(rq,Condition(function R3))
set Fp[61]=CreateTrigger()
set Fp[62]=Fp[61]
set Fp[63]=Fp[61]
set Fp[64]=Fp[61]
set Fp[67]=Fp[61]
set Fp[68]=Fp[61]
set Fp[69]=Fp[61]
call TriggerAddCondition(Fp[61],Condition(function S3))
call TriggerAddAction(Fp[61],function S3)
set Gp[61]=CreateTrigger()
set Gp[62]=Gp[61]
set Gp[63]=Gp[61]
set Gp[64]=Gp[61]
set Gp[67]=Gp[61]
set Gp[68]=Gp[61]
set Gp[69]=Gp[61]
call TriggerAddCondition(Gp[61],Condition(function T3))
call TriggerAddAction(Gp[61],function T3)
set qq=CreateTrigger()
call TriggerAddCondition(qq,Condition(function U3))
set pq=CreateTrigger()
call TriggerAddCondition(pq,Condition(function V3))
set Tp[43]=CreateTrigger()
set Tp[78]=Tp[43]
set Tp[79]=Tp[43]
set Tp[80]=Tp[43]
set Tp[81]=Tp[43]
set Tp[82]=Tp[43]
set Tp[83]=Tp[43]
call TriggerAddCondition(Tp[43],Condition(function W3))
set rp[43]=CreateTrigger()
set rp[78]=rp[43]
set rp[80]=rp[43]
set rp[81]=rp[43]
set rp[82]=rp[43]
set rp[83]=rp[43]
call TriggerAddCondition(rp[43],Condition(function X3))
call TriggerAddAction(rp[43],function X3)
set sp[43]=CreateTrigger()
set sp[78]=sp[43]
set sp[80]=sp[43]
set sp[81]=sp[43]
set sp[83]=sp[43]
call TriggerAddCondition(sp[43],Condition(function Y3))
call TriggerAddAction(sp[43],function Y3)
set tp[43]=CreateTrigger()
set tp[78]=tp[43]
set tp[80]=tp[43]
set tp[83]=tp[43]
call TriggerAddCondition(tp[43],Condition(function Z3))
call TriggerAddAction(tp[43],function Z3)
set vp[43]=CreateTrigger()
set vp[78]=vp[43]
set vp[79]=vp[43]
set vp[80]=vp[43]
set vp[83]=vp[43]
call TriggerAddCondition(vp[43],Condition(function dF))
call TriggerAddAction(vp[43],function dF)
set wp[43]=CreateTrigger()
set wp[78]=wp[43]
set wp[79]=wp[43]
set wp[80]=wp[43]
set wp[81]=wp[43]
set wp[83]=wp[43]
call TriggerAddCondition(wp[43],Condition(function eF))
call TriggerAddAction(wp[43],function eF)
set xp[43]=CreateTrigger()
set xp[78]=xp[43]
set xp[79]=xp[43]
set xp[80]=xp[43]
set xp[81]=xp[43]
set xp[82]=xp[43]
set xp[83]=xp[43]
call TriggerAddCondition(xp[43],Condition(function fF))
call TriggerAddAction(xp[43],function fF)
set yp[43]=CreateTrigger()
set yp[78]=yp[43]
set yp[79]=yp[43]
set yp[80]=yp[43]
set yp[81]=yp[43]
set yp[83]=yp[43]
call TriggerAddCondition(yp[43],Condition(function gF))
call TriggerAddAction(yp[43],function gF)
set zp[43]=CreateTrigger()
set zp[78]=zp[43]
set zp[79]=zp[43]
set zp[80]=zp[43]
set zp[81]=zp[43]
set zp[82]=zp[43]
set zp[83]=zp[43]
call TriggerAddCondition(zp[43],Condition(function hF))
call TriggerAddAction(zp[43],function hF)
set Ap[43]=CreateTrigger()
set Ap[78]=Ap[43]
set Ap[79]=Ap[43]
set Ap[80]=Ap[43]
set Ap[81]=Ap[43]
set Ap[82]=Ap[43]
set Ap[83]=Ap[43]
call TriggerAddCondition(Ap[43],Condition(function iF))
call TriggerAddAction(Ap[43],function iF)
set ap[43]=CreateTrigger()
set ap[78]=ap[43]
set ap[79]=ap[43]
set ap[80]=ap[43]
set ap[81]=ap[43]
set ap[83]=ap[43]
call TriggerAddCondition(ap[43],Condition(function jF))
call TriggerAddAction(ap[43],function jF)
set Bp[43]=CreateTrigger()
set Bp[78]=Bp[43]
set Bp[79]=Bp[43]
set Bp[80]=Bp[43]
set Bp[81]=Bp[43]
set Bp[82]=Bp[43]
set Bp[83]=Bp[43]
call TriggerAddCondition(Bp[43],Condition(function kF))
call TriggerAddAction(Bp[43],function kF)
set bp[43]=CreateTrigger()
set bp[78]=bp[43]
set bp[79]=bp[43]
set bp[80]=bp[43]
set bp[81]=bp[43]
set bp[82]=bp[43]
set bp[83]=bp[43]
call TriggerAddCondition(bp[43],Condition(function mF))
call TriggerAddAction(bp[43],function mF)
set nq=CreateTrigger()
call TriggerAddCondition(nq,Condition(function nF))
call TriggerAddAction(nq,function nF)
set Vp[44]=CreateTrigger()
call TriggerAddCondition(Vp[44],Condition(function oF))
set Cp[44]=CreateTrigger()
set Cp[48]=Cp[44]
set Cp[50]=Cp[44]
set Cp[52]=Cp[44]
set Cp[54]=Cp[44]
set Cp[56]=Cp[44]
set Cp[72]=Cp[44]
set Cp[73]=Cp[44]
set Cp[74]=Cp[44]
set Cp[75]=Cp[44]
set Cp[76]=Cp[44]
set Cp[77]=Cp[44]
call TriggerAddCondition(Cp[44],Condition(function pF))
call TriggerAddAction(Cp[44],function pF)
set Vp[54]=CreateTrigger()
call TriggerAddCondition(Vp[54],Condition(function qF))
call TriggerAddCondition(Vp[54],Condition(function oF))
set Cp[53]=CreateTrigger()
call TriggerAddCondition(Cp[53],Condition(function sF))
call TriggerAddAction(Cp[53],function sF)
set Vp[53]=CreateTrigger()
call TriggerAddCondition(Vp[53],Condition(function rF))
call TriggerAddCondition(Vp[53],Condition(function oF))
set Vp[52]=CreateTrigger()
call TriggerAddCondition(Vp[52],Condition(function tF))
call TriggerAddCondition(Vp[52],Condition(function oF))
set Cp[51]=CreateTrigger()
call TriggerAddCondition(Cp[51],Condition(function vF))
call TriggerAddAction(Cp[51],function vF)
set Vp[51]=CreateTrigger()
call TriggerAddCondition(Vp[51],Condition(function uF))
call TriggerAddCondition(Vp[51],Condition(function oF))
set Vp[50]=CreateTrigger()
call TriggerAddCondition(Vp[50],Condition(function wF))
call TriggerAddCondition(Vp[50],Condition(function oF))
set Cp[49]=CreateTrigger()
call TriggerAddCondition(Cp[49],Condition(function yF))
call TriggerAddAction(Cp[49],function yF)
set Vp[49]=CreateTrigger()
call TriggerAddCondition(Vp[49],Condition(function xF))
call TriggerAddCondition(Vp[49],Condition(function oF))
set Vp[48]=CreateTrigger()
call TriggerAddCondition(Vp[48],Condition(function zF))
call TriggerAddCondition(Vp[48],Condition(function oF))
set oq=CreateTrigger()
call TriggerAddCondition(oq,Condition(function AF))
set Cp[55]=CreateTrigger()
call TriggerAddCondition(Cp[55],Condition(function BF))
call TriggerAddAction(Cp[55],function BF)
set Vp[55]=CreateTrigger()
call TriggerAddCondition(Vp[55],Condition(function aF))
call TriggerAddCondition(Vp[55],Condition(function oF))
set Vp[56]=CreateTrigger()
call TriggerAddCondition(Vp[56],Condition(function bF))
call TriggerAddCondition(Vp[56],Condition(function oF))
set Cp[57]=CreateTrigger()
call TriggerAddCondition(Cp[57],Condition(function cF))
call TriggerAddAction(Cp[57],function cF)
set Vp[57]=CreateTrigger()
call TriggerAddCondition(Vp[57],Condition(function CF))
call TriggerAddCondition(Vp[57],Condition(function oF))
set Ep[62]=CreateTrigger()
call TriggerAddCondition(Ep[62],Condition(function DF))
call TriggerAddAction(Ep[62],function DF)
set Ep[64]=CreateTrigger()
call TriggerAddCondition(Ep[64],Condition(function EF))
call TriggerAddAction(Ep[64],function EF)
set cp[65]=CreateTrigger()
call TriggerAddCondition(cp[65],Condition(function GF))
call TriggerAddAction(cp[65],function GF)
set Fp[65]=CreateTrigger()
call TriggerAddCondition(Fp[65],Condition(function HF))
call TriggerAddAction(Fp[65],function HF)
set Gp[65]=CreateTrigger()
call TriggerAddCondition(Gp[65],Condition(function IF))
call TriggerAddAction(Gp[65],function IF)
set Xp[65]=CreateTrigger()
call TriggerAddCondition(Xp[65],Condition(function FF))
call TriggerAddCondition(Xp[65],Condition(function N3))
set cp[66]=CreateTrigger()
call TriggerAddCondition(cp[66],Condition(function JF))
call TriggerAddAction(cp[66],function JF)
set Fp[66]=CreateTrigger()
call TriggerAddCondition(Fp[66],Condition(function LF))
call TriggerAddAction(Fp[66],function LF)
set Gp[66]=CreateTrigger()
call TriggerAddCondition(Gp[66],Condition(function MF))
call TriggerAddAction(Gp[66],function MF)
set Xp[66]=CreateTrigger()
call TriggerAddCondition(Xp[66],Condition(function lF))
call TriggerAddCondition(Xp[66],Condition(function N3))
set Ep[67]=CreateTrigger()
call TriggerAddCondition(Ep[67],Condition(function OF))
call TriggerAddAction(Ep[67],function OF)
set Dp[67]=CreateTrigger()
call TriggerAddCondition(Dp[67],Condition(function PF))
call TriggerAddAction(Dp[67],function PF)
set Xp[67]=CreateTrigger()
call TriggerAddCondition(Xp[67],Condition(function NF))
call TriggerAddCondition(Xp[67],Condition(function N3))
set Ep[68]=CreateTrigger()
call TriggerAddCondition(Ep[68],Condition(function QF))
call TriggerAddAction(Ep[68],function QF)
set cp[69]=CreateTrigger()
call TriggerAddCondition(cp[69],Condition(function SF))
call TriggerAddAction(cp[69],function SF)
set Xp[69]=CreateTrigger()
call TriggerAddCondition(Xp[69],Condition(function RF))
call TriggerAddCondition(Xp[69],Condition(function N3))
set Cp[71]=CreateTrigger()
call TriggerAddCondition(Cp[71],Condition(function UF))
call TriggerAddAction(Cp[71],function UF)
set Vp[71]=CreateTrigger()
call TriggerAddCondition(Vp[71],Condition(function TF))
call TriggerAddCondition(Vp[71],Condition(function oF))
set Vp[72]=CreateTrigger()
call TriggerAddCondition(Vp[72],Condition(function VF))
call TriggerAddCondition(Vp[72],Condition(function oF))
set Vp[73]=CreateTrigger()
call TriggerAddCondition(Vp[73],Condition(function WF))
call TriggerAddCondition(Vp[73],Condition(function oF))
set Vp[74]=CreateTrigger()
call TriggerAddCondition(Vp[74],Condition(function XF))
call TriggerAddCondition(Vp[74],Condition(function oF))
set Vp[75]=CreateTrigger()
call TriggerAddCondition(Vp[75],Condition(function YF))
call TriggerAddCondition(Vp[75],Condition(function oF))
set Vp[76]=CreateTrigger()
call TriggerAddCondition(Vp[76],Condition(function ZF))
call TriggerAddCondition(Vp[76],Condition(function oF))
set Vp[77]=CreateTrigger()
call TriggerAddCondition(Vp[77],Condition(function dG))
call TriggerAddCondition(Vp[77],Condition(function oF))
set sp[79]=CreateTrigger()
call TriggerAddCondition(sp[79],Condition(function eG))
call TriggerAddAction(sp[79],function eG)
set tp[79]=CreateTrigger()
call TriggerAddCondition(tp[79],Condition(function fG))
call TriggerAddAction(tp[79],function fG)
set rp[79]=CreateTrigger()
call TriggerAddCondition(rp[79],Condition(function gG))
call TriggerAddAction(rp[79],function gG)
set tp[81]=CreateTrigger()
call TriggerAddCondition(tp[81],Condition(function hG))
call TriggerAddAction(tp[81],function hG)
set vp[81]=CreateTrigger()
call TriggerAddCondition(vp[81],Condition(function iG))
call TriggerAddAction(vp[81],function iG)
set sp[82]=CreateTrigger()
call TriggerAddCondition(sp[82],Condition(function jG))
call TriggerAddAction(sp[82],function jG)
set tp[82]=CreateTrigger()
call TriggerAddCondition(tp[82],Condition(function kG))
call TriggerAddAction(tp[82],function kG)
set vp[82]=CreateTrigger()
call TriggerAddCondition(vp[82],Condition(function mG))
call TriggerAddAction(vp[82],function mG)
set wp[82]=CreateTrigger()
call TriggerAddCondition(wp[82],Condition(function nG))
call TriggerAddAction(wp[82],function nG)
set yp[82]=CreateTrigger()
call TriggerAddCondition(yp[82],Condition(function oG))
call TriggerAddAction(yp[82],function oG)
set ap[82]=CreateTrigger()
call TriggerAddCondition(ap[82],Condition(function pG))
call TriggerAddAction(ap[82],function pG)
set Pp[86]=CreateTrigger()
call TriggerAddCondition(Pp[86],Condition(function rG))
call TriggerAddAction(Pp[86],function rG)
set eq[86]=CreateTrigger()
call TriggerAddCondition(eq[86],Condition(function qG))
set Pp[87]=CreateTrigger()
call TriggerAddCondition(Pp[87],Condition(function sG))
call TriggerAddAction(Pp[87],function sG)
set Pp[88]=CreateTrigger()
call TriggerAddCondition(Pp[88],Condition(function uG))
call TriggerAddAction(Pp[88],function uG)
set eq[88]=CreateTrigger()
call TriggerAddCondition(eq[88],Condition(function tG))
set eq[89]=CreateTrigger()
call TriggerAddCondition(eq[89],Condition(function vG))
set Pp[90]=CreateTrigger()
call TriggerAddCondition(Pp[90],Condition(function xG))
call TriggerAddAction(Pp[90],function xG)
set eq[90]=CreateTrigger()
call TriggerAddCondition(eq[90],Condition(function wG))
set Pp[91]=CreateTrigger()
call TriggerAddCondition(Pp[91],Condition(function zG))
call TriggerAddAction(Pp[91],function zG)
set eq[91]=CreateTrigger()
call TriggerAddCondition(eq[91],Condition(function yG))
set eq[93]=CreateTrigger()
call TriggerAddCondition(eq[93],Condition(function AG))
set Pp[94]=CreateTrigger()
call TriggerAddCondition(Pp[94],Condition(function BG))
call TriggerAddAction(Pp[94],function BG)
set eq[94]=CreateTrigger()
call TriggerAddCondition(eq[94],Condition(function aG))
set lp[97]=CreateTrigger()
call TriggerAddCondition(lp[97],Condition(function bG))
call TriggerAddAction(lp[97],function bG)
set Kp[97]=CreateTrigger()
call TriggerAddCondition(Kp[97],Condition(function CG))
call TriggerAddAction(Kp[97],function CG)
set Jp[98]=CreateTrigger()
call TriggerAddCondition(Jp[98],Condition(function cG))
call TriggerAddAction(Jp[98],function cG)
set Kp[98]=CreateTrigger()
call TriggerAddCondition(Kp[98],Condition(function DG))
call TriggerAddAction(Kp[98],function DG)
set Kp[99]=CreateTrigger()
call TriggerAddCondition(Kp[99],Condition(function EG))
call TriggerAddAction(Kp[99],function EG)
set Lp[99]=CreateTrigger()
call TriggerAddCondition(Lp[99],Condition(function FG))
call TriggerAddAction(Lp[99],function FG)
set Hp[100]=CreateTrigger()
call TriggerAddCondition(Hp[100],Condition(function GG))
call TriggerAddAction(Hp[100],function GG)
set lp[100]=CreateTrigger()
call TriggerAddCondition(lp[100],Condition(function HG))
call TriggerAddAction(lp[100],function HG)
set Kp[100]=CreateTrigger()
call TriggerAddCondition(Kp[100],Condition(function IG))
call TriggerAddAction(Kp[100],function IG)
set Np[102]=CreateTrigger()
call TriggerAddCondition(Np[102],Condition(function lG))
call TriggerAddAction(Np[102],function lG)
set Op[102]=CreateTrigger()
call TriggerAddCondition(Op[102],Condition(function JG))
call TriggerAddAction(Op[102],function JG)
set Ip[103]=CreateTrigger()
call TriggerAddCondition(Ip[103],Condition(function KG))
call TriggerAddAction(Ip[103],function KG)
set lp[103]=CreateTrigger()
call TriggerAddCondition(lp[103],Condition(function LG))
call TriggerAddAction(lp[103],function LG)
set Mp[103]=CreateTrigger()
call TriggerAddCondition(Mp[103],Condition(function MG))
call TriggerAddAction(Mp[103],function MG)
set Kp[103]=CreateTrigger()
call TriggerAddCondition(Kp[103],Condition(function NG))
call TriggerAddAction(Kp[103],function NG)
set lp[104]=CreateTrigger()
call TriggerAddCondition(lp[104],Condition(function OG))
call TriggerAddAction(lp[104],function OG)
set Kp[104]=CreateTrigger()
call TriggerAddCondition(Kp[104],Condition(function PG))
call TriggerAddAction(Kp[104],function PG)
set Lp[104]=CreateTrigger()
call TriggerAddCondition(Lp[104],Condition(function QG))
call TriggerAddAction(Lp[104],function QG)
set uq[1]=CreateTrigger()
call TriggerAddAction(uq[1],function RG)
call TriggerAddCondition(uq[1],Condition(function RG))
set tq[1]=CreateTrigger()
call TriggerAddAction(tq[1],function VG)
call TriggerAddCondition(tq[1],Condition(function VG))
set tq[2]=CreateTrigger()
call TriggerAddAction(tq[2],function WG)
call TriggerAddCondition(tq[2],Condition(function WG))
set tq[3]=CreateTrigger()
call TriggerAddAction(tq[3],function XG)
call TriggerAddCondition(tq[3],Condition(function XG))
set vq[1]=CreateTrigger()
call TriggerAddAction(vq[1],function YG)
call TriggerAddCondition(vq[1],Condition(function YG))
set tq[4]=CreateTrigger()
call TriggerAddAction(tq[4],function ZG)
call TriggerAddCondition(tq[4],Condition(function ZG))
set tq[5]=CreateTrigger()
call TriggerAddAction(tq[5],function d6)
call TriggerAddCondition(tq[5],Condition(function d6))
set wq[1]=CreateTrigger()
call TriggerAddAction(wq[1],function e6)
call TriggerAddCondition(wq[1],Condition(function e6))
set vq[2]=CreateTrigger()
call TriggerAddAction(vq[2],function f6)
call TriggerAddCondition(vq[2],Condition(function f6))
call ExecuteFunc("Zu")
call ExecuteFunc("Ea")
call ExecuteFunc("la")
call ExecuteFunc("Ja")
call ExecuteFunc("Pa")
call ExecuteFunc("Xa")
call ExecuteFunc("MB")
call ExecuteFunc("Sb")
call ExecuteFunc("FC")
call ExecuteFunc("RC")
endfunction
