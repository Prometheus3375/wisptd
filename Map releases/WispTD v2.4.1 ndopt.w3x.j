globals
rect h=null
rect j=null
rect o=null
rect w=null
rect z=null
rect B=null
rect C=null
rect D=null
rect E=null
rect F=null
rect G=null
rect I=null
rect J=null
rect K=null
rect M=null
rect N=null
rect O=null
rect P=null
rect Q=null
rect R=null
rect S=null
rect U=null
rect V=null
rect W=null
rect X=null
rect Y=null
rect Z=null
rect d4=null
rect e4=null
rect f4=null
rect g4=null
rect h4=null
rect i4=null
rect j4=null
rect k4=null
rect m4=null
rect n4=null
rect o4=null
rect p4=null
rect q4=null
rect r4=null
rect s4=null
rect t4=null
rect u4=null
rect v4=null
rect w4=null
rect x4=null
rect y4=null
rect z4=null
rect A4=null
rect a4=null
rect B4=null
rect b4=null
rect C4=null
rect c4=null
sound D4=null
sound E4=null
sound F4=null
sound G4=null
sound H4=null
sound I4=null
trigger l4=null
trigger J4=null
trigger K4=null
trigger L4=null
trigger M4=null
trigger N4=null
trigger O4=null
trigger P4=null
trigger Q4=null
trigger R4=null
trigger S4=null
trigger T4=null
trigger U4=null
trigger V4=null
trigger W4=null
trigger X4=null
trigger Y4=null
trigger Z4=null
trigger d7=null
trigger e7=null
trigger f7=null
trigger g7=null
trigger h7=null
trigger i7=null
trigger j7=null
trigger k7=null
trigger m7=null
trigger n7=null
trigger o7=null
trigger p7=null
trigger q7=null
trigger r7=null
trigger s7=null
trigger t7=null
trigger u7=null
trigger v7=null
trigger w7=null
trigger x7=null
trigger y7=null
trigger z7=null
trigger A7=null
trigger a7=null
trigger B7=null
trigger b7=null
trigger C7=null
trigger c7=null
trigger D7=null
trigger E7=null
trigger F7=null
trigger G7=null
trigger H7=null
trigger I7=null
trigger l7=null
trigger J7=null
trigger K7=null
trigger L7=null
trigger M7=null
boolean N7=true
integer O7=50
integer P7=60
integer Q7=1
boolean R7=true
boolean S7=true
boolean T7=true
boolean U7=true
integer V7=2
string W7
integer array X7
integer AT=-1
multiboard Y7
string Z7
unit array d8
integer C0=-1
unit array e8
integer C1=-1
unit array f8
integer C2=-1
string array g8
integer h8=0
hashtable i8
integer array j8
integer MT=0
integer k8=0
integer array m8
integer TT=0
integer n8=0
integer o8=0
unit array p8
integer array q8
integer array r8
integer array FN
integer array s8
integer array t8
integer array LN
integer array u8
boolean array v8
integer array w8
integer array TN
rect array x8
rect array y8
rect array z8
rect array A8
rect array a8
region B8
region b8
timerdialog C8
timer c8=CreateTimer()
integer D8
unit array E8
unit array F8
unit array G8
unit array H8
fogmodifier array I8
real K8=.0
real L8=.0
boolexpr O8=null
endglobals
function Q8 takes nothing returns boolean
local real dx=GetDestructableX(GetFilterDestructable())-K8
local real dy=GetDestructableY(GetFilterDestructable())-L8
return(dx*dx+dy*dy<=bj_enumDestructableRadius)
endfunction
function R8 takes itemtype S8,integer T8 returns nothing
local group g
set bj_stockPickedItemType=S8
set bj_stockPickedItemLevel=T8
set g=CreateGroup()
call GroupEnumUnitsOfType(g,"marketplace",O8)
call ForGroup(g,function UpdateEachStockBuildingEnum)
call DestroyGroup(g)
set g=null
endfunction
function U8 takes nothing returns nothing
local integer pickedItemId
local itemtype V8
local integer W8=0
local integer X8=0
local integer T8
set T8=1
loop
if(bj_stockAllowedPermanent[T8])then
set X8=X8+1
if(GetRandomInt(1,X8)==1)then
set V8=ITEM_TYPE_PERMANENT
set W8=T8
endif
endif
if(bj_stockAllowedCharged[T8])then
set X8=X8+1
if(GetRandomInt(1,X8)==1)then
set V8=ITEM_TYPE_CHARGED
set W8=T8
endif
endif
if(bj_stockAllowedArtifact[T8])then
set X8=X8+1
if(GetRandomInt(1,X8)==1)then
set V8=ITEM_TYPE_ARTIFACT
set W8=T8
endif
endif
set T8=T8+1
exitwhen T8>$A
endloop
if(X8==0)then
set V8=null
return
endif
call R8(V8,W8)
set V8=null
endfunction
function Y8 takes nothing returns nothing
call U8()
call TimerStart(bj_stockUpdateTimer,bj_STOCK_RESTOCK_INTERVAL,true,function U8)
endfunction
function d9 takes nothing returns boolean
return true
endfunction
function h9 takes integer i returns boolean
return GetLocalPlayer()==Player(i)
endfunction
function i9 takes unit u1,unit u2 returns real
local real x1=GetUnitX(u1)
local real x2=GetUnitX(u2)
local real y1=GetUnitY(u1)
local real y2=GetUnitY(u2)
return SquareRoot((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2))
endfunction
function j9 takes integer i,sound k9,integer m9,string s returns nothing
if h9(i)then
call StartSound(k9)
endif
call DisplayTimedTextToPlayer(Player(i),0,0,m9,s)
endfunction
function n9 takes integer i,integer o9,boolean b returns nothing
local player p=Player(i)
local integer p9=GetPlayerState(p,PLAYER_STATE_RESOURCE_GOLD)
local integer q9=GetPlayerState(p,PLAYER_STATE_GOLD_GATHERED)
if b then
call SetPlayerState(p,PLAYER_STATE_GOLD_GATHERED,q9+o9)
endif
call SetPlayerState(p,PLAYER_STATE_RESOURCE_GOLD,p9+o9)
set p=null
endfunction
function r9 takes integer i,integer s9 returns nothing
local multiboarditem t9=MultiboardGetItem(Y7,w8[i],3)
set s8[i]=s8[i]+s9
call MultiboardSetItemValue(t9,I2S(s8[i]))
call MultiboardReleaseItem(t9)
set t9=null
endfunction
function u9 takes string s,unit u,real v9,real w9,integer i,real m9,real x9 returns nothing
local texttag y9=CreateTextTag()
call SetTextTagText(y9,s,v9*.0024)
call SetTextTagPosUnit(y9,u,0)
call SetTextTagVelocity(y9,0,w9/ $640)
if(GetLocalPlayer()!=Player(i))then
call SetTextTagVisibility(y9,false)
endif
call SetTextTagPermanent(y9,false)
call SetTextTagLifespan(y9,m9)
call SetTextTagFadepoint(y9,x9)
set y9=null
endfunction
function z9 takes unit u returns real
local integer H=GetHandleId(u)
local integer T=GetUnitTypeId(u)
return LoadReal(i8,T,1097811282)+LoadReal(i8,H,1093677635)
endfunction
function A9 takes unit u returns real
local integer H=GetHandleId(u)
local integer T=GetUnitTypeId(u)
return LoadReal(i8,T,1148020545)+LoadInteger(i8,H,1265200236)*LoadReal(i8,T,1265200194)+LoadReal(i8,H,1093677633)
endfunction
function a9 takes unit t returns nothing
local integer H=GetHandleId(t)
local integer L=LoadInteger(i8,H,1281717868)
local effect e=LoadEffectHandle(i8,H,1281717868)
call SaveInteger(i8,H,1281717868,L+1)
if(e==null)then
set e=AddSpecialEffectTarget("Models\\Levelup.mdx",t,"overhead")
call SaveEffectHandle(i8,H,1281717868,e)
endif
set e=null
endfunction
function B9 takes unit t,integer b9 returns nothing
local integer H=GetHandleId(t)
local integer T=GetUnitTypeId(t)
local integer L=LoadInteger(i8,H,1281717868)
local integer a1=LoadInteger(i8,T,1096968497)
local integer a2=LoadInteger(i8,T,1096968498)
local integer e1=LoadInteger(i8,T,1165520945)
local integer e2=LoadInteger(i8,T,1165520946)
set b9=b9+LoadInteger(i8,H,1165520961)
call SaveInteger(i8,H,1165520961,b9)
if(b9>=e1)and(L<1)then
call a9(t)
call UnitAddAbility(t,a1)
endif
if(b9>=e2)and(L<2)then
call a9(t)
call UnitAddAbility(t,a2)
endif
endfunction
function C9 takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit c=LoadUnitHandle(i8,tH,0)
local integer cH=GetHandleId(c)
local integer T=GetUnitTypeId(c)
local integer n=0
call RemoveSavedHandle(i8,tH,0)
call RemoveSavedHandle(i8,cH,0)
call DestroyTimer(t)
if(T==2003985456)then
loop
exitwhen c==d8[n]
set n=n+1
endloop
set d8[n]=d8[C0]
set d8[C0]=null
set C0=C0-1
elseif(T==2003985457)then
loop
exitwhen c==e8[n]
set n=n+1
endloop
set e8[n]=e8[C1]
set e8[C1]=null
set C1=C1-1
elseif(T==2003985458)then
loop
exitwhen c==f8[n]
set n=n+1
endloop
set f8[n]=f8[C2]
set f8[C2]=null
set C2=C2-1
endif
call RemoveUnit(c)
set t=null
set c=null
endfunction
function c9 takes unit D9,integer T,real x,real y returns nothing
local integer i=(GetPlayerId(GetOwningPlayer((D9))))
local unit c=CreateUnit(Player(i),T,x,y,0)
local integer cH=GetHandleId(c)
local timer t=CreateTimer()
local integer tH=GetHandleId(t)
local real m9=4
if(GetUnitAbilityLevel((D9),(1093677637))>0)then
set m9=6
endif
if(T==2003985456)then
set C0=C0+1
set d8[C0]=c
elseif(T==2003985457)then
set C1=C1+1
set e8[C1]=c
elseif(T==2003985458)then
set C2=C2+1
set f8[C2]=c
endif
call SetUnitPathing(c,false)
call SaveUnitHandle(i8,cH,0,D9)
call SaveUnitHandle(i8,tH,0,c)
call TimerStart(t,m9,false,function C9)
set c=null
set t=null
endfunction
function E9 takes unit D9,integer F9,integer G9,string H9,integer I9,real x,real y,unit l9 returns boolean
local real x0=GetUnitX(D9)
local real y0=GetUnitY(D9)
local integer i=(GetPlayerId(GetOwningPlayer((D9))))
local integer J9=GetHandleId(D9)
local integer K9=GetUnitTypeId(D9)
local real L9=LoadReal(i8,K9,1214605684)
local unit d=LoadUnitHandle(i8,J9,F9)
local integer dH=0
local boolean b
if(d==null)then
set d=CreateUnit(Player(i),F9,x0,y0,0)
call SetUnitFlyHeight(d,L9,0)
call SetUnitPathing(d,false)
call UnitAddAbility(d,G9)
set dH=GetHandleId(d)
call SaveUnitHandle(i8,dH,0,D9)
call SaveUnitHandle(i8,J9,F9,d)
endif
if(I9==0)then
set b=IssueImmediateOrder(d,H9)
elseif(I9==1)then
set b=IssuePointOrder(d,H9,x,y)
elseif(I9==2)then
set b=IssueTargetOrder(d,H9,l9)
endif
set d=null
return b
endfunction
function rX takes rect r returns real
return GetRandomReal(GetRectMinX(r),GetRectMaxX(r))
endfunction
function rY takes rect r returns real
return GetRandomReal(GetRectMinY(r),GetRectMaxY(r))
endfunction
function M9 takes unit u returns real
return GetUnitState(u,UNIT_STATE_LIFE)/ LoadReal(i8,GetUnitTypeId(u),1214870608)*100.
endfunction
function N9 takes unit u,real O9 returns nothing
local integer T=GetUnitTypeId(u)
local real P9=LoadReal(i8,T,1214870608)
local real Q9=P9*(O9/ 100)
if(Q9>P9)then
set Q9=P9
endif
call SetUnitState(u,UNIT_STATE_LIFE,Q9)
endfunction
function R9 takes unit u,real O9 returns nothing
local integer T=GetUnitTypeId(u)
local real P9=LoadReal(i8,T,1214870608)
local real hp=GetUnitState(u,UNIT_STATE_LIFE)
local real Q9=hp+P9*O9/ 100
if(Q9>P9)then
set Q9=P9
endif
call SetUnitState(u,UNIT_STATE_LIFE,Q9)
endfunction
function S9 takes unit m returns nothing
call SetUnitAnimation(m,"stand")
call PauseUnit(m,true)
endfunction
function T9 takes unit u returns nothing
local integer H=GetHandleId(u)
local boolean b=not(LoadBoolean(i8,H,1400141166)or LoadBoolean(i8,H,1697656884))
if b then
call PauseUnit(u,false)
endif
endfunction
function U9 takes unit u returns nothing
local integer H=GetHandleId(u)
local integer T=GetUnitTypeId(u)
local real V9=GetUnitDefaultMoveSpeed(u)
local real b=LoadReal(i8,H,1093677125)+LoadReal(i8,H,1110454320)+LoadReal(i8,H,2003985456)+LoadReal(i8,H,1865429041)+LoadReal(i8,H,1093677140)
if(b!=0)then
set V9=V9*(1+b/ 100)
endif
call SetUnitMoveSpeed(u,V9)
endfunction
function W9 takes unit u returns real
local integer H=GetHandleId(u)
local integer T=GetUnitTypeId(u)
local real b=LoadInteger(i8,T,1346466413)+LoadInteger(i8,H,1093677126)+LoadInteger(i8,H,2003985457)+LoadInteger(i8,H,1093677140)
local boolean X9=LoadBoolean(i8,T,1346466413)or LoadBoolean(i8,H,1093677146)or LoadBoolean(i8,H,1093677382)
if(b>75)then
set b=75
elseif(b<-25)then
set b=-25
endif
if X9 then
set b=100
endif
return b/ 100
endfunction
function Y9 takes unit u returns real
local integer H=GetHandleId(u)
local integer T=GetUnitTypeId(u)
local real b=LoadInteger(i8,T,1296134765)+LoadInteger(i8,H,1110454324)+LoadInteger(i8,H,2003985457)+LoadInteger(i8,H,1093677140)
local boolean X9=LoadBoolean(i8,T,1296134765)or LoadBoolean(i8,H,1093677128)or LoadBoolean(i8,H,1093677658)
if(b>75)then
set b=75
elseif(b<-25)then
set b=-25
endif
if X9 then
set b=100
endif
return b/ 100
endfunction
function Z9 takes integer H returns boolean
return LoadBoolean(i8,H,1093677648)or LoadBoolean(i8,H,1865429044)or LoadBoolean(i8,H,1697656884)
endfunction
function dd takes unit DD,unit m,real p9,integer t returns nothing
local integer i=(GetPlayerId(GetOwningPlayer((DD))))
local integer T=GetUnitTypeId(m)
local integer H=GetHandleId(m)
local real P9=LoadReal(i8,T,1214870608)
call SaveUnitHandle(i8,H,q8[i],DD)
call SaveInteger(i8,q8[i],1148020564,t)
if(t==1349024115)then
set p9=p9*(1-W9(m))
elseif(t==1298229091)then
set p9=p9*(1-Y9(m))
elseif(t==1348825699)then
set p9=P9*p9/ 100
endif
if Z9(H)then
set p9=0
endif
if(p9>0)then
call UnitDamageTarget(p8[i],m,p9,true,false,ATTACK_TYPE_NORMAL,DAMAGE_TYPE_UNIVERSAL,WEAPON_TYPE_WHOKNOWS)
endif
endfunction
function ed takes unit DD,unit m,real p9,integer t returns nothing
local integer i=(GetPlayerId(GetOwningPlayer((DD))))
local integer H=GetHandleId(m)
local integer n=0
if LoadBoolean(i8,H,1093677655)then
set p9=p9/(LN[i]+1)
loop
exitwhen n>LN[i]
set m=F8[(i)*(50)+n]
call dd(DD,m,p9,t)
set n=n+1
endloop
else
call dd(DD,m,p9,t)
endif
endfunction
function fd takes unit gd,unit l9,integer hd returns boolean
local integer H=GetHandleId(l9)
local integer id=GetUnitTypeId(l9)
local integer T=GetUnitTypeId(gd)
local real d=i9(l9,gd)
local boolean jd=(T==1747988531 and not LoadBoolean(i8,H,1093677648))
local boolean kd=(T==1747988532 and not(GetUnitAbilityLevel((l9),(1113812070))>0)and d<=512)
local boolean md=(T==1747988533 and not(GetUnitAbilityLevel((l9),(1097295983))>0))
local boolean nd=(T==1865429043 and not HaveSavedInteger(i8,H,1093677140))
local boolean od=(T==1865429045 and not LoadBoolean(i8,H,1093677655))
local boolean pd=(T==1865429046 and not LoadBoolean(i8,H,1093677656)and d<=$80)
local boolean qd=(T==1966092338 and not LoadBoolean(i8,H,1093677382))
local boolean rd=(T==1966092339 and d<=512)
local boolean sd=(T==1966092340 and not(GetUnitAbilityLevel((l9),(1110454326))>0))and(d<=512)
local boolean td=(T==1966092341 and not LoadBoolean(i8,H,1093677658))
local boolean ud=(T==1697656883 and LoadInteger(i8,H,1093677872)==0)
local boolean vd=(T==1697656885 and LoadInteger(i8,H,1093677143)==0)
local boolean wd=(T==1697656886 and id!=1697656886 and d<=256)
local boolean xd=(jd or md or nd or od or qd)and(d<=512)
local boolean yd=(jd or md or nd or od or td)and(d<=512)
local boolean zd=(jd or md or nd or od)and(d<=512)
local boolean Ad=(ud or vd)and(d<=512)
local boolean ad=kd or pd or sd or wd
local boolean Bd=rd
local boolean b=false
if(hd==1349024115)then
set b=xd
elseif(hd==1298229091)then
set b=yd
elseif(hd==1348825699)then
set b=zd
elseif(hd==1214603628)then
set b=Ad
elseif(hd==1416591218)then
set b=ad
elseif(hd==1147494756)then
set b=Bd
endif
return b
endfunction
function bd takes integer i,integer hd,unit l9 returns boolean
local real x0=GetUnitX(l9)
local real y0=GetUnitY(l9)
local integer at
local string Cd
local unit gd
local integer T
local integer a=0
local boolean b=false
loop
exitwhen(a>FN[i])or b
set gd=E8[(i)*(50)+a]
set T=GetUnitTypeId(gd)
set b=false
if HaveSavedInteger(i8,T,1096968556)then
set b=fd(gd,l9,hd)
endif
if b then
set at=LoadInteger(i8,T,1096968532)
set Cd=LoadStr(i8,T,1330869362)
if(at==0)then
set b=IssueImmediateOrder(gd,Cd)
elseif(at==1)then
set b=IssuePointOrder(gd,Cd,x0,y0)
elseif(at==2)then
set b=IssueTargetOrder(gd,Cd,l9)
endif
endif
set a=a+1
endloop
set gd=null
return b
endfunction
function cd takes unit m returns nothing
local integer H=GetHandleId(m)
local integer a=0
local timer t
local integer tH
loop
exitwhen a>AT
set t=CreateTimer()
set tH=GetHandleId(t)
call SaveTimerHandle(i8,H,X7[a],t)
call SaveUnitHandle(i8,tH,0,m)
set a=a+1
endloop
set t=null
endfunction
function Dd takes unit m,integer i returns nothing
local integer H=GetHandleId(m)
local texttag y9=CreateTextTag()
if(GetLocalPlayer()!=Player(i))then
call SetTextTagVisibility(y9,false)
endif
call SaveTextTagHandle(i8,H,1415936116,y9)
set y9=null
endfunction
function Ed takes unit m returns nothing
local integer H=GetHandleId(m)
local real Fd=GetUnitState(m,UNIT_STATE_LIFE)
local integer hp=R2I(Fd)
local integer pp=R2I(W9(m)*100)
local integer mp=R2I(Y9(m)*100)
local boolean X9=Z9(H)
local real Gd=M9(m)
local texttag y9=LoadTextTagHandle(i8,H,1415936116)
local string s=""
if(pp!=0)and(pp!=100)then
set s=s+"|cffff8700"+I2S(pp)+"|r
"
elseif(pp==100)then
set s=s+"|cffff8700Immunity|r
"
endif
if(mp!=0)and(mp!=100)then
set s=s+"|cff8700ff"+I2S(mp)+"|r
"
elseif(mp==100)then
set s=s+"|cff8700ffImmunity|r
"
endif
if(Gd>=$C8/ 3)then
set s=s+"|cff00ff00"
elseif(Gd>100/ 3)then
set s=s+"|cffffff00"
else
set s=s+"|cffff0000"
endif
if(hp<Fd)then
set hp=hp+1
endif
set s=s+I2S(hp)+"|r"
if X9 then
set s="|cffccccccProtected|r"
endif
if(y9!=null)then
call SetTextTagText(y9,s,9*.0024)
call SetTextTagPosUnit(y9,m,0)
endif
set y9=null
endfunction
function Hd takes unit m returns boolean
local integer H=GetHandleId(m)
local integer n=0
loop
exitwhen(n>C0)or(i9(m,d8[n])<=$80)
set n=n+1
endloop
if(n<=C0)then
call SaveReal(i8,H,2003985456,-20)
else
call RemoveSavedReal(i8,H,2003985456)
endif
return n<=C0
endfunction
function Id takes unit m returns nothing
local integer H=GetHandleId(m)
local integer n=0
loop
exitwhen(n>C1)or(i9(m,e8[n])<=$80)
set n=n+1
endloop
if(n<=C1)then
call SaveInteger(i8,H,2003985457,-$F)
else
call RemoveSavedInteger(i8,H,2003985457)
endif
endfunction
function ld takes unit m returns nothing
local integer n=0
local boolean b=false
local integer H=GetHandleId(m)
local integer Jd=LoadInteger(i8,H,2003985458)
local real d=0
local real d0=$80
local integer n0=0
local unit t=null
loop
exitwhen(n>C2)
set d=i9(m,f8[n])
if(d0>=d)then
set d0=d
set n0=n
if not b then
set b=true
endif
endif
set n=n+1
endloop
if b then
if(Jd==R2I(1/ .02))then
set Jd=0
endif
call SaveInteger(i8,H,2003985458,Jd+1)
if(Jd==0)then
set H=GetHandleId(f8[n0])
set t=LoadUnitHandle(i8,H,0)
call ed(t,m,3,1348825699)
set t=null
endif
else
call RemoveSavedInteger(i8,H,2003985458)
endif
endfunction
function Kd takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer H=GetHandleId(t)
local unit m=LoadUnitHandle(i8,H,0)
local integer Ld=$FF
local integer Md=$FF
local integer Nd=0
set H=GetHandleId(m)
if HaveSavedReal(i8,H,1093677125)then
set Ld=$80
set Md=$CC
endif
if(GetUnitAbilityLevel((m),(1110454320))>0)then
set Ld=$80
set Md=$CC
call SaveReal(i8,H,1110454320,-20)
else
call RemoveSavedReal(i8,H,1110454320)
endif
if(GetUnitAbilityLevel((m),(1110454324))>0)then
call SaveInteger(i8,H,1110454324,-$F)
else
call RemoveSavedInteger(i8,H,1110454324)
endif
if Hd(m)then
set Ld=$80
set Md=$CC
endif
call Id(m)
call ld(m)
if(LoadBoolean(i8,H,1093677146)or LoadBoolean(i8,H,1093677128))and not IsUnitPaused(m)then
call SetUnitAnimation(m,"walk defend")
endif
set Nd=LoadInteger(i8,H,1097623137)
call SetUnitVertexColor(m,Ld,Md,$FF,Nd)
call U9(m)
call Ed(m)
set m=null
set t=null
endfunction
function Od takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(i8,tH,0)
local integer mH=GetHandleId(m)
call RemoveSavedReal(i8,mH,1093677125)
set t=null
set m=null
endfunction
function Pd takes unit m returns nothing
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(i8,mH,1093677125)
local integer tH=GetHandleId(t)
call SaveReal(i8,mH,1093677125,-20)
call TimerStart(t,3,false,function Od)
set t=null
endfunction
function Qd takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(i8,tH,0)
local integer mH=GetHandleId(m)
local effect e=LoadEffectHandle(i8,tH,1)
call RemoveSavedInteger(i8,mH,1093677126)
call RemoveSavedHandle(i8,tH,1)
call DestroyEffect(e)
set t=null
set m=null
set e=null
endfunction
function Rd takes unit m returns nothing
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(i8,mH,1093677126)
local integer tH=GetHandleId(t)
local effect e
if not HaveSavedHandle(i8,tH,1)then
call SaveInteger(i8,mH,1093677126,-$F)
set e=AddSpecialEffectTarget("Abilities\\Spells\\Other\\HowlOfTerror\\HowlTarget.mdx",m,"origin")
call SaveEffectHandle(i8,tH,1,e)
set e=null
endif
call TimerStart(t,3,false,function Qd)
set t=null
endfunction
function Sd takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(i8,tH,0)
local integer mH=GetHandleId(m)
local effect e=LoadEffectHandle(i8,tH,1)
call RemoveSavedBoolean(i8,mH,1400141166)
call T9(m)
call RemoveSavedHandle(i8,tH,1)
call DestroyEffect(e)
set t=null
set m=null
set e=null
endfunction
function Td takes unit m,real m9 returns nothing
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(i8,mH,1400141166)
local integer tH=GetHandleId(t)
local effect e
if not HaveSavedHandle(i8,tH,1)then
call S9(m)
call SaveBoolean(i8,mH,1400141166,true)
set e=AddSpecialEffectTarget("Abilities\\Spells\\Human\\Thunderclap\\ThunderclapTarget.mdl",m,"overhead")
call SaveEffectHandle(i8,tH,1,e)
set e=null
endif
call TimerStart(t,m9,false,function Sd)
set t=null
endfunction
function Ud takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(i8,tH,0)
local integer mH=GetHandleId(m)
local integer T=GetUnitTypeId(m)
local integer G9=LoadInteger(i8,T,1096968556)
local effect e=LoadEffectHandle(i8,tH,1)
call UnitAddAbility(m,G9)
call RemoveSavedHandle(i8,tH,1)
call DestroyEffect(e)
set t=null
set m=null
set e=null
endfunction
function Vd takes unit m returns nothing
local integer mH=GetHandleId(m)
local integer T=GetUnitTypeId(m)
local integer G9=LoadInteger(i8,T,1096968556)
local timer t=LoadTimerHandle(i8,mH,1093677106)
local integer tH=GetHandleId(t)
local effect e
if not HaveSavedHandle(i8,tH,1)then
call UnitRemoveAbility(m,G9)
set e=AddSpecialEffectTarget("Abilities\\Spells\\Other\\Silence\\SilenceTarget.mdx",m,"overhead")
call SaveEffectHandle(i8,tH,1,e)
set e=null
endif
call TimerStart(t,3,false,function Ud)
set t=null
endfunction
function Wd takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer H=GetHandleId(t)
local unit m=LoadUnitHandle(i8,H,0)
local real r=100-M9(m)
set H=GetHandleId(m)
if(r<50)then
call SaveReal(i8,H,1865429041,r)
else
call SaveReal(i8,H,1865429041,100)
endif
set t=null
set m=null
endfunction
function Xd takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(i8,tH,0)
local integer mH=GetHandleId(m)
local effect e=LoadEffectHandle(i8,tH,1)
call RemoveSavedBoolean(i8,mH,1865429044)
call RemoveSavedHandle(i8,tH,1)
call DestroyEffect(e)
call TimerStart(t,3,false,null)
set t=null
set m=null
set e=null
endfunction
function Yd takes unit m,real Zd returns nothing
local real hp=GetUnitState(m,UNIT_STATE_LIFE)
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(i8,mH,1865429044)
local integer tH=GetHandleId(t)
local real de=TimerGetRemaining(t)
local effect e
if(de==0)then
call SetUnitState(m,UNIT_STATE_LIFE,hp+Zd)
call SaveBoolean(i8,mH,1865429044,true)
set e=AddSpecialEffectTarget("Abilities\\Spells\\Orc\\Voodoo\\VoodooAuraTarget.mdx",m,"overhead")
call SaveEffectHandle(i8,tH,1,e)
set e=null
call TimerStart(t,2,false,function Xd)
endif
set t=null
endfunction
function ee takes integer i,integer ix returns nothing
set F8[(i)*(50)+ix]=F8[(i)*(50)+LN[i]]
set F8[(i)*(50)+LN[i]]=null
set LN[i]=LN[i]-1
call SaveInteger(i8,GetHandleId(F8[(i)*(50)+ix]),1280199032,ix)
endfunction
function fe takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(i8,tH,0)
local integer mH=GetHandleId(m)
local integer i=LoadInteger(i8,mH,1181705552)
local integer ix=LoadInteger(i8,mH,1280199032)
local effect e=LoadEffectHandle(i8,tH,1)
call RemoveSavedBoolean(i8,mH,1093677655)
call RemoveSavedHandle(i8,tH,1)
call DestroyEffect(e)
call ee(i,ix)
set t=null
set m=null
set e=null
endfunction
function ge takes unit m returns nothing
local integer mH=GetHandleId(m)
local integer i=LoadInteger(i8,mH,1181705552)
local timer t=LoadTimerHandle(i8,mH,1093677655)
local integer tH=GetHandleId(t)
local effect e
if not HaveSavedHandle(i8,tH,1)then
call SaveBoolean(i8,mH,1093677655,true)
set e=AddSpecialEffectTarget("Abilities\\Spells\\Orc\\SpiritLink\\SpiritLinkTarget.mdx",m,"chest")
call SaveEffectHandle(i8,tH,1,e)
set e=null
set LN[i]=LN[i]+1
set F8[(i)*(50)+LN[i]]=m
call SaveInteger(i8,mH,1280199032,LN[i])
endif
call TimerStart(t,3,false,function fe)
set t=null
endfunction
function he takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(i8,tH,0)
call R9(m,5*.25)
set t=null
set m=null
endfunction
function ie takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(i8,tH,0)
local integer mH=GetHandleId(m)
local real x=LoadReal(i8,mH,1131570264)
local real y=LoadReal(i8,mH,1131570265)
call RemoveSavedBoolean(i8,mH,1697656884)
call T9(m)
call N9(m,100)
call IssuePointOrderById(m,$D0012,x,y)
call SaveInteger(i8,mH,1097623137,$FF)
call TimerStart(t,3,false,null)
set t=null
set m=null
endfunction
function je takes unit m,real Zd returns nothing
local real hp=GetUnitState(m,UNIT_STATE_LIFE)
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(i8,mH,1697656884)
local integer tH=GetHandleId(t)
local real de=TimerGetRemaining(t)
if(de==0)then
call SetUnitState(m,UNIT_STATE_LIFE,hp+Zd)
call SaveInteger(i8,mH,1097623137,0)
call S9(m)
call SaveBoolean(i8,mH,1697656884,true)
call TimerStart(t,1,false,function ie)
endif
set t=null
endfunction
function ke takes integer H returns nothing
local unit array d
local integer k=-1
local integer a=0
local integer dH
set k=k+1
set d[k]=LoadUnitHandle(i8,H,1685417264)
set k=k+1
set d[k]=LoadUnitHandle(i8,H,1685417265)
set k=k+1
set d[k]=LoadUnitHandle(i8,H,1685417266)
loop
exitwhen a>k
if(d[k]!=null)then
set dH=GetHandleId(d[k])
call FlushChildHashtable(i8,dH)
call RemoveUnit(d[k])
set d[k]=null
endif
set a=a+1
endloop
endfunction
function me takes integer i,integer ix returns nothing
set E8[(i)*(50)+ix]=E8[(i)*(50)+FN[i]]
set E8[(i)*(50)+FN[i]]=null
set FN[i]=FN[i]-1
call SaveInteger(i8,GetHandleId(E8[(i)*(50)+ix]),1179535736,ix)
endfunction
function ne takes integer H returns nothing
local integer a=0
local timer t
local integer tH
local effect e
loop
exitwhen a>AT
set t=LoadTimerHandle(i8,H,X7[a])
if(t!=null)then
set tH=GetHandleId(t)
set e=LoadEffectHandle(i8,tH,1)
if(e!=null)then
call DestroyEffect(e)
endif
set e=LoadEffectHandle(i8,tH,2)
if(e!=null)then
call DestroyEffect(e)
endif
call FlushChildHashtable(i8,tH)
call DestroyTimer(t)
endif
set a=a+1
endloop
set t=null
set e=null
endfunction
function oe takes unit m returns nothing
local integer n=GetHandleId(m)
local texttag y9=LoadTextTagHandle(i8,n,1415936116)
local integer i=LoadInteger(i8,n,1333227088)
local integer ix=LoadInteger(i8,n,1179535736)
local boolean b=LoadBoolean(i8,n,1093677655)
call DestroyTextTag(y9)
call ne(n)
call FlushChildHashtable(i8,n)
call SetUnitAnimation(m,"death")
call me(i,ix)
if b then
set ix=LoadInteger(i8,n,1280199032)
call ee(i,ix)
endif
set y9=null
set n=0
loop
exitwhen(n>$A)
exitwhen v8[n]and(FN[n]!=-1)
set n=n+1
endloop
if(n>$A)then
call TriggerExecute(e7)
endif
endfunction
function pe takes integer i,integer ix returns nothing
set H8[(i)*(56)+ix]=H8[(i)*(56)+TN[i]]
set H8[(i)*(56)+TN[i]]=null
set TN[i]=TN[i]-1
call SaveInteger(i8,GetHandleId(H8[(i)*(56)+ix]),1414416760,ix)
endfunction
function qe takes integer H returns nothing
local timer t=LoadTimerHandle(i8,H,1093677656)
local integer tH=GetHandleId(t)
local effect e=LoadEffectHandle(i8,tH,1)
if(t!=null)then
call DestroyEffect(e)
call FlushChildHashtable(i8,tH)
call DestroyTimer(t)
endif
set t=null
set e=null
endfunction
function re takes unit t returns nothing
local integer H=GetHandleId(t)
local integer i=(GetPlayerId(GetOwningPlayer((t))))
local integer ix=LoadInteger(i8,H,1414416760)
local effect e=LoadEffectHandle(i8,H,1281717868)
if(e!=null)then
call DestroyEffect(e)
endif
call qe(H)
call ke(H)
call FlushChildHashtable(i8,H)
call pe(i,ix)
call RemoveUnit(t)
set e=null
endfunction
function se takes unit t,boolean te returns nothing
local integer T=GetUnitTypeId(t)
local integer c=LoadInteger(i8,T,1131377524)
local integer i=(GetPlayerId(GetOwningPlayer((t))))
local real x=GetUnitX(t)
local real y=GetUnitY(t)
local effect e
if te then
set e=AddSpecialEffect("Objects\\Spawnmodels\\Human\\HCancelDeath\\HCancelDeath.mdx",x,y)
call DestroyEffect(e)
set e=null
endif
if(c>0)then
call n9(i,c,false)
if te then
call u9("|cFFFFFF00+"+I2S(c)+"|r",t,8.5,64,i,2,1)
endif
endif
endfunction
function ue takes nothing returns nothing
set k8=k8-1
if(k8==0)then
call PauseTimer(c8)
call MultiboardSetTitleText(Y7,"Wave "+I2S(n8))
call MultiboardSetItemsIcon(Y7,"Icons\\PASReadiness.blp")
call TriggerExecute(d7)
return
endif
if(k8<4)then
set Z7="|cFFFF0000"+I2S(k8)+"|r"
call StartSound(G4)
else
set Z7=I2S(k8)
endif
if(k8==1)then
set Z7=Z7+" second"
else
set Z7=Z7+" seconds"
endif
set Z7="Wave "+I2S(n8)+" starts in "+Z7
call MultiboardSetTitleText(Y7,Z7)
endfunction
function ve takes integer i,boolean b returns nothing
local player p=Player(i)
local integer a=0
if b then
set a=-1
endif
call SetPlayerAbilityAvailable(p,1093677105,b)
call SetPlayerAbilityAvailable(p,1093677110,b)
call SetPlayerAbilityAvailable(p,1093677113,b)
call SetPlayerTechMaxAllowed(p,1953462065,a)
call SetPlayerTechMaxAllowed(p,1953462066,a)
call SetPlayerTechMaxAllowed(p,1953462067,a)
call SetPlayerTechMaxAllowed(p,1953462068,a)
call SetPlayerTechMaxAllowed(p,1953462069,a)
call SetPlayerTechMaxAllowed(p,1953462070,a)
call SetPlayerTechMaxAllowed(p,1953462071,a)
call SetPlayerTechMaxAllowed(p,1953462072,a)
call SetPlayerTechMaxAllowed(p,1953462073,a)
call SetPlayerAbilityAvailable(p,1093677141,b)
call SetPlayerAbilityAvailable(p,1097347120,b)
call SetPlayerAbilityAvailable(p,1097347121,b)
call SetPlayerAbilityAvailable(p,1097347122,b)
call SetPlayerAbilityAvailable(p,1097347123,b)
call SetPlayerAbilityAvailable(p,1097347124,b)
call SetPlayerAbilityAvailable(p,1097347125,b)
call SetPlayerAbilityAvailable(p,1097347126,b)
call SetPlayerAbilityAvailable(p,1093677366,b)
call SetPlayerAbilityAvailable(p,1097805872,b)
call SetPlayerAbilityAvailable(p,1097805873,b)
call SetPlayerAbilityAvailable(p,1097805874,b)
call SetPlayerAbilityAvailable(p,1097805875,b)
call SetPlayerAbilityAvailable(p,1097805876,b)
call SetPlayerAbilityAvailable(p,1097805877,b)
call SetPlayerAbilityAvailable(p,1097805878,b)
call SetPlayerAbilityAvailable(p,1093677381,b)
call SetPlayerAbilityAvailable(p,1098199088,b)
call SetPlayerAbilityAvailable(p,1098199089,b)
call SetPlayerAbilityAvailable(p,1098199090,b)
call SetPlayerAbilityAvailable(p,1098199091,b)
call SetPlayerAbilityAvailable(p,1098199092,b)
call SetPlayerAbilityAvailable(p,1098199093,b)
call SetPlayerAbilityAvailable(p,1098199094,b)
call SetPlayerAbilityAvailable(p,1093677367,b)
call SetPlayerAbilityAvailable(p,1097150512,b)
call SetPlayerAbilityAvailable(p,1097150513,b)
call SetPlayerAbilityAvailable(p,1097150514,b)
call SetPlayerAbilityAvailable(p,1097150515,b)
call SetPlayerAbilityAvailable(p,1097150516,b)
call SetPlayerAbilityAvailable(p,1097150517,b)
call SetPlayerAbilityAvailable(p,1097150518,b)
set p=null
endfunction
function xe takes integer i,boolean b returns nothing
local integer a=0
local integer ye=0
if b then
set ye=$FF
endif
loop
exitwhen a>55
call SetUnitVertexColor(G8[(i)*(56)+a],$FF,$FF,$FF,ye)
set a=a+1
endloop
endfunction
function ze takes trigger t,playerunitevent ev returns nothing
call TriggerRegisterPlayerUnitEvent(t,Player(0),ev,null)
call TriggerRegisterPlayerUnitEvent(t,Player(1),ev,null)
call TriggerRegisterPlayerUnitEvent(t,Player(2),ev,null)
call TriggerRegisterPlayerUnitEvent(t,Player(3),ev,null)
call TriggerRegisterPlayerUnitEvent(t,Player(4),ev,null)
call TriggerRegisterPlayerUnitEvent(t,Player(5),ev,null)
call TriggerRegisterPlayerUnitEvent(t,Player(6),ev,null)
call TriggerRegisterPlayerUnitEvent(t,Player(7),ev,null)
call TriggerRegisterPlayerUnitEvent(t,Player(8),ev,null)
call TriggerRegisterPlayerUnitEvent(t,Player(9),ev,null)
call TriggerRegisterPlayerUnitEvent(t,Player($A),ev,null)
endfunction
function Ae takes integer i returns string
return g8[i]+GetPlayerName(Player(i))+"|r"
endfunction
function ae takes nothing returns nothing
local integer i=0
local integer n=0
loop
exitwhen i>$A
if v8[i]and LoadBoolean(i8,i,1382375780)then
set n=n+1
endif
set i=i+1
endloop
if(n==o8)and(k8>4)then
set k8=4
endif
endfunction
function Be takes integer i returns nothing
set D8=i
call TriggerExecute(Z4)
endfunction
function be takes nothing returns nothing
local integer a=0
local player p
local fogmodifier f
loop
exitwhen a>$A
set p=Player(a)
if(GetPlayerSlotState(p)==PLAYER_SLOT_STATE_PLAYING)then
set o8=o8+1
set v8[a]=true
set FN[a]=-1
set LN[a]=-1
set TN[a]=-1
call TriggerRegisterPlayerEvent(Y4,p,EVENT_PLAYER_LEAVE)
endif
set a=a+1
endloop
set a=0
loop
exitwhen v8[a]
set a=a+1
endloop
set p=Player(a)
call TriggerRegisterPlayerChatEvent(T4,p,"start",true)
call TriggerRegisterPlayerChatEvent(S4,p,"",false)
set p=Player($B)
set f=CreateFogModifierRect(p,FOG_OF_WAR_VISIBLE,bj_mapInitialPlayableArea,false,true)
call FogModifierStart(f)
call EnableWorldFogBoundary(false)
call SetGameSpeed(MAP_SPEED_FASTEST)
call SetMapFlag(MAP_LOCK_SPEED,true)
call SetMapFlag(MAP_USE_HANDICAPS,false)
call SetFloatGameState(GAME_STATE_TIME_OF_DAY,$C)
call SuspendTimeOfDay(true)
call SetAllyColorFilterState(0)
call SetCreepCampFilterState(false)
call EnableMinimapFilterButtons(true,false)
call EnablePreSelect(true,false)
call SetWaterBaseColor(0,0,0,$FF)
if(bj_dayAmbientSound!=null)then
call StopSound(bj_dayAmbientSound,true,false)
endif
call StopMusic(false)
call ClearMapMusic()
set i8=InitHashtable()
call TriggerExecute(J4)
call TriggerExecute(K4)
call TriggerExecute(L4)
call TriggerExecute(M4)
call TriggerExecute(N4)
call TriggerExecute(O4)
call TriggerExecute(P4)
call TriggerExecute(Q4)
set p=null
set f=null
endfunction
function ce takes string s returns nothing
local integer l=StringLength(s)
local string De=SubString(s,l-3,l-1)
local effect e
call Preload(s)
if(De=="md")or(De=="MD")then
set e=AddSpecialEffect(s,0,0)
call DestroyEffect(e)
endif
set e=null
endfunction
function Ee takes nothing returns nothing
call ce("Sound\\Ambient\\LordaeronSummer\\LordaeronSummerDay.mid")
call ce("ReplaceableTextures\\CommandButtons\\BTNMove.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNMove.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNStop.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNStop.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNPatrol.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNPatrol.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNHoldPosition.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNHoldPosition.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNCancel.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNCancel.blp")
call ce("Icons\\ArrowTower\\PASBash.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASBash.blp")
call ce("Icons\\ArrowTower\\PASChillingArrows.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASChillingArrows.blp")
call ce("Icons\\ArrowTower\\PASCriticalShot.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASCriticalShot.blp")
call ce("Icons\\ArrowTower\\PASMultishot.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASMultishot.blp")
call ce("Icons\\ArrowTower\\PASPiercingArrows.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASPiercingArrows.blp")
call ce("Icons\\ArrowTower\\PASPoisonedArrows.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASPoisonedArrows.blp")
call ce("Icons\\ArrowTower\\PASSearingArrows.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASSearingArrows.blp")
call ce("Icons\\ArcaneTower\\PASBloodMagic.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASBloodMagic.blp")
call ce("Icons\\ArcaneTower\\PASHarmfulMagic.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASHarmfulMagic.blp")
call ce("Icons\\ArcaneTower\\PASPainStrike.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASPainStrike.blp")
call ce("Icons\\ArcaneTower\\PASRainOfFire.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASRainOfFire.blp")
call ce("Icons\\ArcaneTower\\PASRainOfIce.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASRainOfIce.blp")
call ce("Icons\\ArcaneTower\\PASRainOfMagic.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASRainOfMagic.blp")
call ce("Icons\\ArcaneTower\\PASSilenceStrike.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASSilenceStrike.blp")
call ce("Icons\\ArcaneTower\\PASThunderStrike.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASThunderStrike.blp")
call ce("Icons\\CannonTower\\PASBlackPowder.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASBlackPowder.blp")
call ce("Icons\\CannonTower\\PASCloudOfDestruction.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASCloudOfDestruction.blp")
call ce("Icons\\CannonTower\\PASCloudOfIce.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASCloudOfIce.blp")
call ce("Icons\\CannonTower\\PASCloudOfPoison.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASCloudOfPoison.blp")
call ce("Icons\\CannonTower\\PASCloudiness.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASCloudiness.blp")
call ce("Icons\\CannonTower\\PASHeavyImpact.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASHeavyImpact.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNAntiMagicShell.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNAntiMagicShell.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNBloodLustOn.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNBloodLustOn.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNCannibalize.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNCannibalize.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNCloudOfFog.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNCloudOfFog.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNDefend.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNDefend.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNInnerFireOn.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNInnerFireOn.blp")
call ce("ReplaceableTextures\\PassiveButtons\\PASBTNFreezingBreath.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASBTNFreezingBreath.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNFrostArmor.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNFrostArmor.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNInvisibility.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNInvisibility.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNSpellBreakerMagicDefend.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNSpellBreakerMagicDefend.blp")
call ce("ReplaceableTextures\\PassiveButtons\\PASBTNSmash.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISPASBTNSmash.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNRaiseDeadOn.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNRaiseDeadOn.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNRejuvenation.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNRejuvenation.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNBattleRoar.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNBattleRoar.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNSpiritLink.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNSpiritLink.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNTaunt.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNTaunt.blp")
call ce("Icons\\BTNHuman.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNHuman.blp")
call ce("Icons\\BTNNightElf.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNNightElf.blp")
call ce("Icons\\BTNOrc.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNOrc.blp")
call ce("Icons\\BTNUndead.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNUndead.blp")
call ce("Icons\\BTNDeleteMinion.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNDeleteMinion.blp")
call ce("Icons\\BTNDestroyTower.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNDestroyTower.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNMilitia.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNMilitia.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNFootman.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNFootman.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNKnight.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNKnight.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNPriest.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNPriest.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNDragonHawk.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNDragonHawk.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNSorceress.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNSorceress.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNSpellBreaker.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNSpellBreaker.blp")
call ce("Icons\\ArcaneTower\\BTNArcaneTower1.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNArcaneTower1.blp")
call ce("Icons\\ArcaneTower\\BTNRainOfIce.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNRainOfIce.blp")
call ce("Icons\\ArcaneTower\\BTNRainOfMagic.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNRainOfMagic.blp")
call ce("Icons\\ArcaneTower\\BTNRainOfFire.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNRainOfFire.blp")
call ce("Icons\\ArrowTower\\BTNArrowTower1.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNArrowTower1.blp")
call ce("Icons\\ArrowTower\\BTNChillingArrows.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNChillingArrows.blp")
call ce("Icons\\ArrowTower\\BTNPiercingArrows.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNPiercingArrows.blp")
call ce("Icons\\ArrowTower\\BTNPoisonedArrows.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNPoisonedArrows.blp")
call ce("Icons\\CannonTower\\BTNCannonTower1.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNCannonTower1.blp")
call ce("Icons\\CannonTower\\BTNCloudOfIce.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNCloudOfIce.blp")
call ce("Icons\\CannonTower\\BTNCloudOfDestruction.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNCloudOfDestruction.blp")
call ce("Icons\\CannonTower\\BTNCloudOfPoison.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNCloudOfPoison.blp")
call ce("Icons\\ArcaneTower\\BTNArcaneTower2.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNArcaneTower2.blp")
call ce("Icons\\ArcaneTower\\BTNSilenceStrike.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNSilenceStrike.blp")
call ce("Icons\\ArcaneTower\\BTNPainStrike.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNPainStrike.blp")
call ce("Icons\\ArcaneTower\\BTNThunderStrike.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNThunderStrike.blp")
call ce("Icons\\ArrowTower\\BTNArrowTower2.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNArrowTower2.blp")
call ce("Icons\\ArrowTower\\BTNSearingArrows.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNSearingArrows.blp")
call ce("Icons\\ArrowTower\\BTNCriticalShot.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNCriticalShot.blp")
call ce("Icons\\ArrowTower\\BTNBash.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNBash.blp")
call ce("Icons\\CannonTower\\BTNCannonTower2.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNCannonTower2.blp")
call ce("Icons\\CannonTower\\BTNBlackPowder.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNBlackPowder.blp")
call ce("Icons\\CannonTower\\BTNHeavyImpact.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNHeavyImpact.blp")
call ce("Icons\\CannonTower\\BTNCloudiness.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNCloudiness.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNArcher.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNArcher.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNEnt.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNEnt.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNDryad.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNDryad.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNDruidOfTheTalon.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNDruidOfTheTalon.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNFaerieDragon.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNFaerieDragon.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNDruidOfTheClaw.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNDruidOfTheClaw.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNMountainGiant.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNMountainGiant.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNGrunt.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNGrunt.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNHeadHunterBerserker.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNHeadHunterBerserker.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNRaider.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNRaider.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNShaman.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNShaman.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNTrollBatRider.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNTrollBatRider.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNSpiritWalker.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNSpiritWalker.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNTauren.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNTauren.blp")
call ce("Icons\\BTNReadiness.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNReadiness.blp")
call ce("Icons\\BTNShowDetails.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNShowDetails.blp")
call ce("Icons\\BTNGrid.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNGrid.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNSkeletonWarrior.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNSkeletonWarrior.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNGhoul.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNGhoul.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNAbomination.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNAbomination.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNBanshee.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNBanshee.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNFrostWyrm.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNFrostWyrm.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNNecromancer.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNNecromancer.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNCryptFiend.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNCryptFiend.blp")
call ce("Splats\\SplatData.slk")
call ce("ReplaceableTextures\\Splats\\Splat01Mature.blp")
call ce("UI\\TipStrings.txt")
call ce("UI\\Widgets\\EscMenu\\Human\\human-options-button-border-up.blp")
call ce("UI\\Widgets\\EscMenu\\Human\\human-options-button-border-down.blp")
call ce("UI\\Widgets\\EscMenu\\Human\\human-options-button-background-disabled.blp")
call ce("UI\\Widgets\\EscMenu\\Human\\editbox-background.blp")
call ce("UI\\Widgets\\EscMenu\\Human\\editbox-border.blp")
call ce("UI\\Widgets\\EscMenu\\Human\\slider-background.blp")
call ce("UI\\Widgets\\EscMenu\\Human\\slider-border.blp")
call ce("UI\\Widgets\\EscMenu\\Human\\slider-knob.blp")
call ce("UI\\HelpStrings.txt")
call ce("UI\\Widgets\\EscMenu\\Human\\slider-borderdisabled.blp")
call ce("UI\\Widgets\\EscMenu\\Human\\slider-knobdisabled.blp")
call ce("UI\\Widgets\\EscMenu\\Human\\checkbox-background.blp")
call ce("UI\\Widgets\\EscMenu\\Human\\checkbox-depressed.blp")
call ce("UI\\Widgets\\EscMenu\\Human\\checkbox-background-disabled.blp")
call ce("UI\\Widgets\\Glues\\GlueScreen-PullDown-Arrow.blp")
call ce("UI\\Widgets\\EscMenu\\Human\\radiobutton-background.blp")
call ce("UI\\Widgets\\EscMenu\\Human\\radiobutton-background-disabled.blp")
call ce("UI\\Widgets\\EscMenu\\Human\\alliance-gold.blp")
call ce("UI\\Widgets\\EscMenu\\Human\\alliance-lumber.blp")
call ce("UI\\Minimap\\minimap-hero.blp")
call ce("war3mapMap.blp")
call ce("UI\\Minimap\\minimap-gold.blp")
call ce("UI\\Minimap\\minimap-gold-entangled.blp")
call ce("UI\\Minimap\\minimap-gold-haunted.blp")
call ce("UI\\Minimap\\minimap-neutralbuilding.blp")
call ce("UI\\Minimap\\MinimapIconCircleOfPower.blp")
call ce("Models\\Pavement1.mdx")
call ce("Models\\Pavement2.mdx")
call ce("units\\nightelf\\Wisp\\Wisp.mdx")
call ce("Textures\\star2_32.blp")
call ce("Textures\\Shockwave10.blp")
call ce("Textures\\Dust3.blp")
call ce("ReplaceableTextures\\Weather\\CloudSingleFlat.blp")
call ce("Textures\\Dust5.blp")
call ce("Textures\\GenericGlow2c.blp")
call ce("Icons\\BTNBuildTower.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNBuildTower.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNAdvancedFrostTower.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNAdvancedFrostTower.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNHumanArcaneTower.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNHumanArcaneTower.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNGuardTower.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNGuardTower.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNAdvancedEnergyTower.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNAdvancedEnergyTower.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNDalaranGuardTower.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNDalaranGuardTower.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNTrollBurrow.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNTrollBurrow.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNAdvancedRockTower.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNAdvancedRockTower.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNAdvancedFlameTower.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNAdvancedFlameTower.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNOrcTower.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNOrcTower.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNHumanWatchTower.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNHumanWatchTower.blp")
call ce("units\\nightelf\\Wisp\\Wisp_portrait.mdx")
call ce("units\\NightElf\\Wisp\\Wisp.blp")
call ce("Textures\\Dust5A.blp")
call ce("ReplaceableTextures\\Selection\\SelectionCircleSmall.blp")
call ce("UI\\Widgets\\Glues\\Gluescreen-Scrollbar-UpArrow.blp")
call ce("UI\\Widgets\\EscMenu\\Human\\human-options-menu-background.blp")
call ce("UI\\Widgets\\Console\\Human\\CommandButton\\human-activebutton.blp")
call ce("UI\\Widgets\\Console\\Human\\infocard-armor-hero.blp")
call ce("UI\\Widgets\\Console\\Human\\CommandButton\\human-button-lvls-overlay.blp")
call ce("UI\\Widgets\\EscMenu\\Human\\observer-icon.blp")
call ce("Icons\\PASPreparation.blp")
call ce("Sound\\Interface\\MouseClick1.wav")
call ce("buildings\\human\\HumanTower\\HumanTower.mdx")
call ce("Textures\\Doodads0.blp")
call ce("Textures\\Tower.blp")
call ce("Textures\\DeathSmug.blp")
call ce("Textures\\CloudSingle.blp")
call ce("Textures\\Clouds8x8.blp")
call ce("Textures\\CartoonCloud.blp")
call ce("Textures\\Dust3x.blp")
call ce("Textures\\RockParticle.blp")
call ce("Textures\\SpinningBoard.blp")
call ce("Textures\\HumanBase.blp")
call ce("buildings\\Human\\HumanTower\\HumanMagicTower.blp")
call ce("Textures\\Blue_Glow2.blp")
call ce("Textures\\GenericGlow2b.blp")
call ce("Sound\\Buildings\\Shared\\BuildingConstruction.wav")
call ce("Units\\NightElf\\Wisp\\WispYes1.wav")
call ce("Splats\\UberSplatData.slk")
call ce("ReplaceableTextures\\Splats\\HumanUberSplat.blp")
call ce("Units\\NightElf\\Wisp\\WispYes2.wav")
call ce("Units\\NightElf\\Wisp\\WispYes3.wav")
call ce("Sound\\Interface\\Warning\\Human\\PeasantCannotBuildThere1.wav")
call ce("Sound\\Interface\\Error.wav")
call ce("Sound\\Interface\\BattleNetTick.wav")
call ce("Buildings\\Human\\HumanTower\\WatchTowerWhat1.wav")
call ce("UI\\Widgets\\ToolTips\\Human\\ToolTipGoldIcon.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNAttack.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNAttack.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNAttackGround.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNAttackGround.blp")
call ce("UI\\Widgets\\Console\\Human\\infocard-neutral-attack-piercing.blp")
call ce("Icons\\PASReadiness.blp")
call ce("Abilities\\Spells\\Items\\ResourceItems\\ReceiveGold.wav")
call ce("Sound\\Interface\\Warning\\Human\\KnightNoGold1.wav")
call ce("Sound\\Interface\\GamePause.wav")
call ce("UI\\Widgets\\EscMenu\\Human\\human-options-button-highlight.blp")
call ce("Sound\\Interface\\BigButtonClick.wav")
call ce("font\\font.gid")
call ce("font\\font.exp")
call ce("font\\font.clh")
call ce("font\\font.ccd")
call ce("scripts\\common.j")
call ce("scripts\\blizzard.j")
call ce("war3map.j")
call ce("war3map.w3e")
call ce("war3map.wpm")
call ce("war3map.doo")
call ce("war3map.w3u")
call ce("war3map.w3a")
call ce("war3map.w3i")
call ce("war3map.wts")
call ce("Maps\\Test\\TestMap.w3x")
call ce("UI\\Glues\\Loading\\LoadBar\\LoadBar.mdx")
call ce("ui\\Glues\\Loading\\LoadBar\\Loading-BarBorder.blp")
call ce("Textures\\Loading-BarBackground.blp")
call ce("Textures\\Loading-BarGlass.blp")
call ce("ui\\Glues\\Loading\\LoadBar\\Loading-BarFill.blp")
call ce("Textures\\Loading-BarGlow.blp")
call ce("UI\\Glues\\Loading\\Backgrounds\\Campaigns\\LordaeronExpansionBackground.mdx")
call ce("ui\\Glues\\Loading\\Backgrounds\\Campaigns\\LordaeronExpansion-BotLeft.blp")
call ce("ui\\Glues\\Loading\\Backgrounds\\Campaigns\\LordaeronExpansion-BotRight.blp")
call ce("ui\\Glues\\Loading\\Backgrounds\\Campaigns\\LordaeronExpansion-TopRight.blp")
call ce("ui\\Glues\\Loading\\Backgrounds\\Campaigns\\LordaeronExpansion-TopLeft.blp")
call ce("ui\\Glues\\Loading\\Backgrounds\\Campaigns\\NightElfSymbol.blp")
call ce("ui\\Glues\\Loading\\Backgrounds\\Campaigns\\AshenvaleLocationIcons.blp")
call ce("Textures\\star1.blp")
call ce("ui\\Glues\\Loading\\Backgrounds\\Campaigns\\HumanSymbol.blp")
call ce("ui\\Glues\\Loading\\Backgrounds\\Campaigns\\UndeadSymbol.blp")
call ce("L.mpq")
call ce("(attributes)")
call ce("TerrainArt\\LordaeronSummer\\Lords_Dirt.blp")
call ce("TerrainArt\\LordaeronSummer\\Lords_DirtRough.blp")
call ce("TerrainArt\\LordaeronSummer\\Lords_DirtGrass.blp")
call ce("TerrainArt\\LordaeronSummer\\Lords_Rock.blp")
call ce("TerrainArt\\LordaeronSummer\\Lords_Grass.blp")
call ce("TerrainArt\\LordaeronSummer\\Lords_GrassDark.blp")
call ce("ReplaceableTextures\\Water\\Water00.blp")
call ce("ReplaceableTextures\\Water\\Water01.blp")
call ce("ReplaceableTextures\\Water\\Water02.blp")
call ce("ReplaceableTextures\\Water\\Water03.blp")
call ce("ReplaceableTextures\\Water\\Water04.blp")
call ce("ReplaceableTextures\\Water\\Water05.blp")
call ce("ReplaceableTextures\\Water\\Water06.blp")
call ce("ReplaceableTextures\\Water\\Water07.blp")
call ce("ReplaceableTextures\\Water\\Water08.blp")
call ce("ReplaceableTextures\\Water\\Water09.blp")
call ce("ReplaceableTextures\\Water\\Water10.blp")
call ce("ReplaceableTextures\\Water\\Water11.blp")
call ce("ReplaceableTextures\\Water\\Water12.blp")
call ce("ReplaceableTextures\\Water\\Water13.blp")
call ce("ReplaceableTextures\\Water\\Water14.blp")
call ce("ReplaceableTextures\\Water\\Water15.blp")
call ce("ReplaceableTextures\\Water\\Water16.blp")
call ce("ReplaceableTextures\\Water\\Water17.blp")
call ce("ReplaceableTextures\\Water\\Water18.blp")
call ce("ReplaceableTextures\\Water\\Water19.blp")
call ce("ReplaceableTextures\\Water\\Water20.blp")
call ce("ReplaceableTextures\\Water\\Water21.blp")
call ce("ReplaceableTextures\\Water\\Water22.blp")
call ce("ReplaceableTextures\\Water\\Water23.blp")
call ce("ReplaceableTextures\\Water\\Water24.blp")
call ce("ReplaceableTextures\\Water\\Water25.blp")
call ce("ReplaceableTextures\\Water\\Water26.blp")
call ce("ReplaceableTextures\\Water\\Water27.blp")
call ce("ReplaceableTextures\\Water\\Water28.blp")
call ce("ReplaceableTextures\\Water\\Water29.blp")
call ce("ReplaceableTextures\\Water\\Water30.blp")
call ce("ReplaceableTextures\\Water\\Water31.blp")
call ce("ReplaceableTextures\\Water\\Water32.blp")
call ce("ReplaceableTextures\\Water\\Water33.blp")
call ce("ReplaceableTextures\\Water\\Water34.blp")
call ce("ReplaceableTextures\\Water\\Water35.blp")
call ce("ReplaceableTextures\\Water\\Water36.blp")
call ce("ReplaceableTextures\\Water\\Water37.blp")
call ce("ReplaceableTextures\\Water\\Water38.blp")
call ce("ReplaceableTextures\\Water\\Water39.blp")
call ce("ReplaceableTextures\\Water\\Water40.blp")
call ce("ReplaceableTextures\\Water\\Water41.blp")
call ce("ReplaceableTextures\\Water\\Water42.blp")
call ce("ReplaceableTextures\\Water\\Water43.blp")
call ce("ReplaceableTextures\\Water\\Water44.blp")
call ce("TerrainArt\\Blight\\Lords_Blight.blp")
call ce("war3map.shd")
call ce("ReplaceableTextures\\Cliff\\Cliff1.blp")
call ce("Units\\MiscGame.txt")
call ce("war3mapMisc.txt")
call ce("war3mapSkin.txt")
call ce("Units\\UpgradeData.slk")
call ce("Units\\AbilityData.slk")
call ce("war3map.w3h")
call ce("Units\\UnitUI.slk")
call ce("Units\\UnitData.slk")
call ce("Units\\UnitBalance.slk")
call ce("Units\\UnitAbilities.slk")
call ce("Units\\UnitWeapons.slk")
call ce("Units\\ItemData.slk")
call ce("units\\DestructableData.slk")
call ce("Units\\CampaignUnitStrings.txt")
call ce("Units\\HumanUnitStrings.txt")
call ce("Units\\NeutralUnitStrings.txt")
call ce("Units\\NightElfUnitStrings.txt")
call ce("Units\\OrcUnitStrings.txt")
call ce("Units\\UndeadUnitStrings.txt")
call ce("Units\\UnitGlobalStrings.txt")
call ce("Units\\CampaignUnitFunc.txt")
call ce("Units\\HumanUnitFunc.txt")
call ce("Units\\NeutralUnitFunc.txt")
call ce("Units\\NightElfUnitFunc.txt")
call ce("Units\\OrcUnitFunc.txt")
call ce("Units\\UndeadUnitFunc.txt")
call ce("Units\\CampaignAbilityStrings.txt")
call ce("Units\\CommonAbilityStrings.txt")
call ce("Units\\HumanAbilityStrings.txt")
call ce("Units\\NeutralAbilityStrings.txt")
call ce("Units\\NightElfAbilityStrings.txt")
call ce("Units\\OrcAbilityStrings.txt")
call ce("Units\\UndeadAbilityStrings.txt")
call ce("Units\\ItemAbilityStrings.txt")
call ce("Units\\CampaignAbilityFunc.txt")
call ce("Units\\CommonAbilityFunc.txt")
call ce("Units\\HumanAbilityFunc.txt")
call ce("Units\\NeutralAbilityFunc.txt")
call ce("Units\\NightElfAbilityFunc.txt")
call ce("Units\\OrcAbilityFunc.txt")
call ce("Units\\UndeadAbilityFunc.txt")
call ce("Units\\ItemAbilityFunc.txt")
call ce("Units\\CampaignUpgradeStrings.txt")
call ce("Units\\HumanUpgradeStrings.txt")
call ce("Units\\NightElfUpgradeStrings.txt")
call ce("Units\\OrcUpgradeStrings.txt")
call ce("Units\\UndeadUpgradeStrings.txt")
call ce("Units\\NeutralUpgradeStrings.txt")
call ce("Units\\CampaignUpgradeFunc.txt")
call ce("Units\\HumanUpgradeFunc.txt")
call ce("Units\\NightElfUpgradeFunc.txt")
call ce("Units\\OrcUpgradeFunc.txt")
call ce("Units\\UndeadUpgradeFunc.txt")
call ce("Units\\NeutralUpgradeFunc.txt")
call ce("Units\\CommandStrings.txt")
call ce("Units\\ItemStrings.txt")
call ce("Units\\Telemetry.txt")
call ce("Units\\CommandFunc.txt")
call ce("Units\\ItemFunc.txt")
call ce("ReplaceableTextures\\AshenvaleTree\\AshenTree.blp")
call ce("ReplaceableTextures\\AshenvaleTree\\AshenTreeBlight.blp")
call ce("ReplaceableTextures\\BarrensTree\\BarrensTree.blp")
call ce("ReplaceableTextures\\BarrensTree\\BarrensTreeBlight.blp")
call ce("ReplaceableTextures\\AshenvaleTree\\FelwoodTree.blp")
call ce("ReplaceableTextures\\AshenvaleTree\\FelwoodTreeBlight.blp")
call ce("ReplaceableTextures\\LordaeronTree\\LordaeronFallTree.blp")
call ce("ReplaceableTextures\\LordaeronTree\\LordaeronFallTreeBlight.blp")
call ce("ReplaceableTextures\\LordaeronTree\\LordaeronSummerTree.blp")
call ce("ReplaceableTextures\\LordaeronTree\\LordaeronSummerTreeBlight.blp")
call ce("ReplaceableTextures\\NorthrendTree\\NorthTree.blp")
call ce("ReplaceableTextures\\NorthrendTree\\NorthTreeBlight.blp")
call ce("ReplaceableTextures\\LordaeronTree\\LordaeronWinterTree.blp")
call ce("ReplaceableTextures\\LordaeronTree\\LordaeronWinterTreeBlight.blp")
call ce("ReplaceableTextures\\LordaeronTree\\LordaeronSnowTree.blp")
call ce("ReplaceableTextures\\LordaeronTree\\LordaeronSnowTreeBlight.blp")
call ce("ReplaceableTextures\\Cliff\\Cliff0.blp")
call ce("ReplaceableTextures\\Mushroom\\MushroomTree.blp")
call ce("ReplaceableTextures\\RuinsTree\\RuinsTree.blp")
call ce("ReplaceableTextures\\RuinsTree\\RuinsTreeBlight.blp")
call ce("ReplaceableTextures\\AshenvaleTree\\Ice_Tree.blp")
call ce("ReplaceableTextures\\AshenvaleTree\\Ice_TreeBlight.blp")
call ce("ReplaceableTextures\\AshenvaleTree\\AshenCanopyTree.blp")
call ce("ReplaceableTextures\\AshenvaleTree\\AshenCanopyTreeBlight.blp")
call ce("ReplaceableTextures\\OutlandMushroomTree\\MushroomTree.blp")
call ce("ReplaceableTextures\\OutlandMushroomTree\\MushroomTreeBlight.blp")
call ce("ReplaceableTextures\\DalaranRuinsTree\\DalaranRuinsTree.blp")
call ce("ReplaceableTextures\\DalaranRuinsTree\\DalaranRuinsTreeBlight.blp")
call ce("ReplaceableTextures\\UndergroundTree\\UnderMushroomTree.blp")
call ce("ReplaceableTextures\\UndergroundTree\\UnderMushroomTreeBlight.blp")
call ce("units\\UnitBalance.slk")
call ce("units\\UnitWeapons.slk")
call ce("units\\UnitAbilities.slk")
call ce("units\\unitUI.slk")
call ce("units\\UnitData.slk")
call ce("units\\ItemData.slk")
call ce("Environment\\DNC\\DNCLordaeron\\DNCLordaeronTerrain\\DNCLordaeronTerrain.mdx")
call ce("Environment\\DNC\\DNCLordaeron\\DNCLordaeronUnit\\DNCLordaeronUnit.mdx")
call ce("UI\\Feedback\\RallyPoint\\RallyPoint.mdx")
call ce("Textures\\RallPoint.blp")
call ce("UI\\Feedback\\Confirmation\\Confirmation.mdx")
call ce("Textures\\RallyArrow2.blp")
call ce("UI\\Feedback\\WaypointFlags\\WaypointFlag.mdx")
call ce("UI\\Cursor\\HumanCursor.mdx")
call ce("ui\\Cursor\\HumanCursor.blp")
call ce("UI\\Console\\Human\\HumanUITile01.blp")
call ce("UI\\Console\\Human\\HumanUITile02.blp")
call ce("UI\\Console\\Human\\HumanUITile03.blp")
call ce("UI\\Console\\Human\\HumanUITile04.blp")
call ce("UI\\Console\\Human\\HumanUI-TimeIndicator.mdx")
call ce("Textures\\GenericGlowFaded.blp")
call ce("Textures\\star3.blp")
call ce("Textures\\GenericGlow2_32.blp")
call ce("Textures\\HumanUITile-TimeIndicator.blp")
call ce("ui\\Console\\Human\\HumanUITile-TimeIndicatorFrame.blp")
call ce("Textures\\MagicGlow.blp")
call ce("UI\\Minimap\\Minimap-Ping.mdx")
call ce("ui\\MiniMap\\ping5.blp")
call ce("ui\\MiniMap\\ping2.blp")
call ce("ui\\MiniMap\\ping4.blp")
call ce("ui\\MiniMap\\ping6.blp")
call ce("UI\\Minimap\\Minimap-Waypoint.mdx")
call ce("ui\\Minimap\\Minimap-WaypointMarker.blp")
call ce("UI\\Minimap\\MiniMap-CreepLoc-Small.mdx")
call ce("ui\\Minimap\\MinimapIconCreepLoc.blp")
call ce("UI\\Minimap\\MiniMap-CreepLoc-Large.mdx")
call ce("ui\\Minimap\\MinimapIconCreepLoc2.blp")
call ce("UI\\Widgets\\Console\\Human\\human-minimap-ping-active.blp")
call ce("UI\\Widgets\\Console\\Human\\human-minimap-ping-active-down.blp")
call ce("UI\\Widgets\\Console\\Human\\human-minimap-ping-disabled.blp")
call ce("UI\\Widgets\\Console\\Human\\human-formation-on.blp")
call ce("UI\\Widgets\\Console\\Human\\human-formation-on-down.blp")
call ce("UI\\Widgets\\Console\\Human\\human-formation-on-disabled.blp")
call ce("UI\\Widgets\\Console\\Human\\human-minimap-terrain-active.blp")
call ce("UI\\Widgets\\Console\\Human\\human-minimap-terrain-active-down.blp")
call ce("UI\\Widgets\\Console\\Human\\human-minimap-terrain-active-disabled.blp")
call ce("UI\\Widgets\\Console\\Human\\human-minimap-ally-off.blp")
call ce("UI\\Widgets\\Console\\Human\\human-minimap-ally-off-down.blp")
call ce("UI\\Widgets\\Console\\Human\\human-minimap-ally-off-disabled.blp")
call ce("UI\\Widgets\\Console\\Human\\human-minimap-creep-inactive.blp")
call ce("UI\\Widgets\\Console\\Human\\human-minimap-creep-inactive-down.blp")
call ce("UI\\Widgets\\Console\\Human\\human-minimap-creep-inactive-disabled.blp")
call ce("UI\\Feedback\\XPBar\\human-bigbar-fill.blp")
call ce("UI\\Feedback\\XPBar\\human-xpbar-border.blp")
call ce("UI\\Widgets\\ToolTips\\Human\\human-tooltip-background.blp")
call ce("UI\\Widgets\\ToolTips\\Human\\human-tooltip-border.blp")
call ce("UI\\Feedback\\BuildProgressBar\\human-buildprogressbar-fill.blp")
call ce("UI\\Feedback\\BuildProgressBar\\human-buildprogressbar-border.blp")
call ce("UI\\Widgets\\Console\\Human\\infocard-supply.blp")
call ce("UI\\Widgets\\Console\\Human\\infocard-gold.blp")
call ce("UI\\Feedback\\Resources\\ResourceGold.blp")
call ce("UI\\Widgets\\BattleNet\\bnet-tournament-clock.blp")
call ce("ReplaceableTextures\\WorldEditUI\\Editor-MultipleUnits.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNTemp.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNTemp.blp")
call ce("UI\\Widgets\\Console\\Human\\human-unitqueue-border.blp")
call ce("UI\\Feedback\\HpBarConsole\\human-healthbar-fill.blp")
call ce("UI\\Widgets\\Console\\Human\\human-transport-slot.blp")
call ce("UI\\Widgets\\Console\\Human\\CommandButton\\human-subgroup-background.blp")
call ce("UI\\Console\\Human\\HumanUITile-InventoryCover.blp")
call ce("UI\\Widgets\\Console\\Human\\human-console-buttonstates2.blp")
call ce("UI\\Buttons\\HeroLevel\\HeroLevel.mdx")
call ce("Textures\\HeroLevel-Particle.blp")
call ce("ui\\Buttons\\HeroLevel\\HeroLevel-Border.blp")
call ce("ReplaceableTextures\\CommandButtons\\BTNWisp.blp")
call ce("ReplaceableTextures\\CommandButtonsDisabled\\DISBTNWisp.blp")
call ce("UI\\Widgets\\EscMenu\\Human\\human-cinematic-border.blp")
call ce("UI\\Widgets\\EscMenu\\Human\\blank-background.blp")
call ce("UI\\Widgets\\EscMenu\\Human\\human-options-menu-border.blp")
call ce("Sound\\Ambient\\LordaeronSummer\\LordaeronSummerNight.mid")
call ce("Sound\\Interface\\Warning.wav")
call ce("Sound\\Interface\\ItemReceived.wav")
call ce("Sound\\Interface\\Rescue.wav")
call ce("Sound\\Interface\\QuestNew.wav")
call ce("Sound\\Interface\\QuestLog.wav")
call ce("Sound\\Interface\\QuestCompleted.wav")
call ce("Sound\\Interface\\QuestFailed.wav")
call ce("Sound\\Interface\\Hint.wav")
call ce("Sound\\Interface\\SecretFound.wav")
call ce("Sound\\Time\\DaybreakRooster.wav")
call ce("Sound\\Time\\DuskWolf.wav")
call ce("Objects\\Spawnmodels\\Human\\HCancelDeath\\HCancelDeath.mdx")
call ce("Textures\\Clouds8x8Fire.blp")
call ce("Sound\\Buildings\\Death\\HCancelBuilding.wav")
call ce("Units\\NightElf\\Wisp\\WispWhat3.wav")
call ce("buildings\\orc\\TrollBurrow\\TrollBurrow.mdx")
call ce("Textures\\Bunker.blp")
call ce("Textures\\OrcBirth.blp")
call ce("buildings\\orc\\WatchTower\\WatchTower.mdx")
call ce("Textures\\Watchtower.blp")
call ce("ReplaceableTextures\\Weather\\Clouds8x8.blp")
call ce("Textures\\CloudSingleBlend.blp")
call ce("buildings\\other\\ElvenGuardFireTower\\ElvenGuardFireTower.mdx")
call ce("Textures\\ElvenGuardTower.blp")
call ce("buildings\\other\\ElvenGuardTower\\ElvenGuardTower.mdx")
call ce("buildings\\other\\DalaranGuardTower\\DalaranGuardTower.mdx")
call ce("buildings\\Other\\DalaranGuardTower\\DalaranGuardTower.blp")
call ce("buildings\\other\\TowerDefenseTower\\TowerDefenseTower.mdx")
call ce("buildings\\Other\\TowerDefenseTower\\TowerDSecondTower.blp")
call ce("buildings\\Other\\TowerDefenseTower\\TowerDFirstFourthTower.blp")
call ce("buildings\\Other\\TowerDefenseTower\\TowerDFifthTower.blp")
call ce("Textures\\Yellow_Glow3.blp")
call ce("buildings\\Other\\TowerDefenseTower\\TowerDThirdTower.blp")
call ce("Models\\Levelup.mdx")
call ce("LevelUp.blp")
call ce("Buildings\\Human\\HumanTower\\GuardTowerWhat1.wav")
call ce("Buildings\\Orc\\TrollBurrow\\OrcBurrowWhat1.wav")
call ce("Buildings\\Orc\\WatchTower\\OrcWatchTowerWhat1.wav")
call ce("Buildings\\Human\\HumanTower\\CannonTowerWhat1.wav")
call ce("Buildings\\Other\\ElvenGuardTower\\ElvenGuardTowerWhat1.wav")
call ce("Units\\NightElf\\Wisp\\WispWhat1.wav")
call ce("units\\human\\Militia\\Militia.mdx")
call ce("units\\Human\\Militia\\PeasantMilitia.blp")
call ce("Textures\\gutz.blp")
call ce("units\\human\\Militia\\Militia_portrait.mdx")
call ce("units\\human\\Footman\\Footman.mdx")
call ce("Textures\\Footman.blp")
call ce("units\\human\\Footman\\Footman_portrait.mdx")
call ce("units\\human\\Knight\\Knight.mdx")
call ce("Textures\\Knight.blp")
call ce("units\\human\\Knight\\Knight_portrait.mdx")
call ce("Sound\\Units\\Footsteps\\WaterStep4.wav")
call ce("Sound\\Units\\Footsteps\\WaterStep1.wav")
call ce("Sound\\Units\\Footsteps\\WaterStep2.wav")
call ce("Sound\\Units\\Footsteps\\WaterStep3.wav")
call ce("Abilities\\Weapons\\GuardTowerMissile\\GuardTowerMissile.mdx")
call ce("Textures\\ArrowMissile.blp")
call ce("Textures\\RibbonNE1_White.blp")
call ce("Abilities\\Weapons\\GuardTowerMissile\\GuardTowerMissileLaunch1.wav")
call ce("Abilities\\Weapons\\GuardTowerMissile\\GuardTowerMissileLaunch3.wav")
call ce("Abilities\\Weapons\\Arrow\\ArrowImpact.wav")
call ce("Abilities\\Spells\\Other\\HowlOfTerror\\HowlTarget.mdx")
call ce("abilities\\Spells\\Other\\HowlOfTerror\\Skull1.blp")
call ce("Abilities\\Spells\\Orc\\SpiritLink\\SpiritLink1.wav")
call ce("Abilities\\Weapons\\CannonTowerMissile\\CannonTowerMissile.mdx")
call ce("abilities\\Weapons\\CannonTowerMissile\\Bullet.blp")
call ce("Textures\\Clouds32_anim.blp")
call ce("Textures\\rock64.blp")
call ce("Textures\\GenericGlowX.blp")
call ce("Abilities\\Weapons\\CannonTowerMissile\\CannonTowerMissileLaunch1.wav")
call ce("Abilities\\Spells\\Other\\AcidBomb\\BottleImpact.mdx")
call ce("Textures\\WaterBlobs1.blp")
call ce("Abilities\\Weapons\\CannonTowerMissile\\CannonTowerMissile2.wav")
call ce("Abilities\\Weapons\\GuardTowerMissile\\GuardTowerMissileLaunch2.wav")
call ce("Models\\ArcaneMissile.mdx")
call ce("Textures\\Purple_Glow.blp")
call ce("Textures\\star4_32.blp")
call ce("Textures\\Star8b.blp")
call ce("Abilities\\Weapons\\KeeperGroveMissile\\KeeperOfTheGroveMissileLaunch3.wav")
call ce("Abilities\\Weapons\\KeeperGroveMissile\\KeeperOfTheGroveMissileHit1.wav")
call ce("Abilities\\Weapons\\KeeperGroveMissile\\KeeperOfTheGroveMissileHit3.wav")
call ce("Abilities\\Weapons\\CannonTowerMissile\\CannonTowerMissileLaunch2.wav")
call ce("Abilities\\Weapons\\CannonTowerMissile\\CannonTowerMissile3.wav")
call ce("Abilities\\Weapons\\KeeperGroveMissile\\KeeperOfTheGroveMissileLaunch1.wav")
call ce("Abilities\\Weapons\\KeeperGroveMissile\\KeeperOfTheGroveMissileLaunch2.wav")
call ce("Abilities\\Weapons\\KeeperGroveMissile\\KeeperOfTheGroveMissileHit2.wav")
call ce("Abilities\\Weapons\\CannonTowerMissile\\CannonTowerMissile1.wav")
call ce("Abilities\\Spells\\Human\\Defend\\DefendCaster.mdx")
call ce("Textures\\Yellow_Star_Dim.blp")
call ce("Abilities\\Spells\\Human\\Defend\\DefendCaster.wav")
call ce("Abilities\\Spells\\Human\\Thunderclap\\ThunderclapTarget.mdx")
call ce("Textures\\Magic1.blp")
call ce("Abilities\\Spells\\Demon\\RainOfFire\\RainOfFireTarget1.wav")
call ce("Models\\RainOfMagic.mdx")
call ce("Textures\\EQ_Rock2.blp")
call ce("Abilities\\Spells\\Demon\\RainOfFire\\RainOfFireLoop1.wav")
call ce("Models\\RainOfMagicTarget.mdx")
call ce("Abilities\\Weapons\\CannonTowerMissile\\CannonTowerMissileLaunch3.wav")
call ce("Abilities\\Spells\\Demon\\RainOfFire\\RainOfFireTarget2.wav")
call ce("Models\\RainOfFire.mdx")
call ce("Textures\\Red_Glow2.blp")
call ce("Models\\CloudOfDestruction.mdx")
call ce("Models\\CloudOfPoison.mdx")
call ce("Units\\Human\\Knight\\KnightDeath.wav")
call ce("Splats\\SpawnData.slk")
call ce("Objects\\Spawnmodels\\Human\\HumanBlood\\HumanBloodKnight.mdx")
call ce("Textures\\BloodWhiteSmall.blp")
call ce("Models\\CloudOfIce.mdx")
call ce("Models\\RainOfFireTarget.mdx")
call ce("Abilities\\Spells\\Demon\\RainOfFire\\RainOfFireTarget3.wav")
call ce("Models\\RainOfIce.mdx")
call ce("Units\\Human\\Peasant\\PeasantDeath.wav")
call ce("Objects\\Spawnmodels\\Human\\HumanBlood\\HumanBloodPeasant.mdx")
call ce("Abilities\\Spells\\Demon\\DemonBoltImpact\\DemonBoltImpact.mdx")
call ce("Textures\\Red_Glow1.blp")
call ce("Textures\\Zap1_Red.blp")
call ce("Splats\\LightningData.slk")
call ce("ReplaceableTextures\\Weather\\LightningRed.blp")
call ce("Abilities\\Spells\\Orc\\LightningBolt\\LightningBolt.wav")
call ce("Abilities\\Weapons\\Bolt\\BoltImpact.mdx")
call ce("Textures\\LightningBall.blp")
call ce("Textures\\Zap1.blp")
call ce("ReplaceableTextures\\Weather\\LightningForked.blp")
call ce("Units\\Human\\Footman\\FootmanDeath.wav")
call ce("Objects\\Spawnmodels\\Human\\HumanBlood\\HumanBloodFootman.mdx")
call ce("Abilities\\Spells\\Undead\\DarkRitual\\DarkRitualTarget.mdx")
call ce("Textures\\grad2b.blp")
call ce("Textures\\GenericGlow64.blp")
call ce("Units\\NightElf\\Wisp\\WispWhat2.wav")
call ce("Abilities\\Spells\\NightElf\\ManaBurn\\ManaBurnTarget.mdx")
call ce("Textures\\Green_Glow3.blp")
call ce("ReplaceableTextures\\Weather\\ManaBurnBeam.blp")
call ce("Abilities\\Spells\\NightElf\\ManaBurn\\ManaDrainTarget1.wav")
call ce("Abilities\\Spells\\Other\\Silence\\SilenceTarget.mdx")
call ce("abilities\\Spells\\Other\\Silence\\Silence.blp")
call ce("Abilities\\Spells\\Other\\Silence\\Silence1.wav")
call ce("units\\human\\Priest\\Priest_V1.mdx")
call ce("Textures\\BloodPriest.blp")
call ce("units\\human\\Priest\\Priest_V1_portrait.mdx")
call ce("units\\human\\Sorceress\\Sorceress_V1.mdx")
call ce("units\\Human\\Sorceress\\BloodElfSorceress.blp")
call ce("units\\human\\Sorceress\\Sorceress_V1_portrait.mdx")
call ce("units\\human\\BloodElfDragonHawk\\BloodElfDragonHawk.mdx")
call ce("units\\Human\\BloodElfDragonHawk\\BloodElfDragonhawk.blp")
call ce("units\\Human\\BloodElfDragonHawk\\Dragonhawk.blp")
call ce("Textures\\Blue_Star2.blp")
call ce("SharedModels\\Feather2.MDx")
call ce("SharedModels\\Feather1.blp")
call ce("SharedModels\\Bones1.MDx")
call ce("SharedModels\\Gutz1.MDx")
call ce("SharedModels\\Feather1.MDx")
call ce("ReplaceableTextures\\Shadows\\ShadowFlyer.blp")
call ce("units\\human\\BloodElfDragonHawk\\BloodElfDragonHawk_portrait.mdx")
call ce("Abilities\\Spells\\Human\\InnerFire\\InnerFireTarget.mdx")
call ce("abilities\\Spells\\Human\\InnerFire\\Crown1.blp")
call ce("Textures\\Yellow_Glow_Dim2.blp")
call ce("abilities\\Spells\\Human\\InnerFire\\Rune7.blp")
call ce("Textures\\Rune1d.blp")
call ce("Abilities\\Spells\\Human\\InnerFire\\InnerFireBirth.wav")
call ce("Abilities\\Spells\\Human\\CloudOfFog\\CloudOfFog.mdx")
call ce("Textures\\ShockwaveWater1.blp")
call ce("Abilities\\Spells\\Human\\CloudOfFog\\CloudOfFogLoop1.wav")
call ce("Units\\Human\\Sorceress\\SorceressDeath.wav")
call ce("Objects\\Spawnmodels\\Human\\HumanBlood\\HumanBloodSorceress.mdx")
call ce("Abilities\\Spells\\Undead\\DarkRitual\\DarkRitualTarget1.wav")
call ce("Units\\Human\\Priest\\PriestDeath.wav")
call ce("Objects\\Spawnmodels\\Human\\HumanBlood\\HumanBloodPriest.mdx")
call ce("units\\human\\BloodElfSpellThief\\BloodElfSpellThief.mdx")
call ce("Textures\\BloodElf-SpellThief.blp")
call ce("Textures\\star5tga.blp")
call ce("Textures\\Red_Glow3.blp")
call ce("Textures\\Knife_spinningBlade.blp")
call ce("units\\human\\BloodElfSpellThief\\BloodElfSpellThief_portrait.mdx")
call ce("UI\\Feedback\\Cooldown\\UI-Cooldown-Indicator.mdx")
call ce("ui\\Feedback\\Cooldown\\cooldown.blp")
call ce("ui\\Feedback\\Cooldown\\star4.blp")
call ce("Units\\Human\\BloodElfSpellThief\\BloodElfSpellThiefDeath1.wav")
call ce("units\\orc\\Grunt\\Grunt.mdx")
call ce("units\\Orc\\Grunt\\Grunt.blp")
call ce("Textures\\Shockwave_Ice1.blp")
call ce("units\\orc\\Grunt\\Grunt_portrait.mdx")
call ce("units\\orc\\HeadHunter\\HeadHunter_V1.mdx")
call ce("Textures\\Headhunter.blp")
call ce("Textures\\OrcBloodTailParticle0.blp")
call ce("units\\Orc\\HeadHunter\\OrcBloodTailParticle3.blp")
call ce("units\\Orc\\HeadHunter\\OrcBloodTailParticle1.blp")
call ce("units\\Orc\\HeadHunter\\OrcBloodTailParticle2.blp")
call ce("units\\Orc\\HeadHunter\\BeserkHeadHunter.blp")
call ce("units\\Orc\\HeadHunter\\ForestTroll.blp")
call ce("units\\orc\\HeadHunter\\HeadHunter_V1_portrait.mdx")
call ce("units\\orc\\WolfRider\\WolfRider.mdx")
call ce("units\\Orc\\Wolfrider\\Wolfrider.blp")
call ce("units\\orc\\WolfRider\\WolfRider_portrait.mdx")
call ce("Units\\Orc\\Grunt\\GruntDeath.wav")
call ce("Objects\\Spawnmodels\\Orc\\Orcblood\\OrcBloodGrunt.mdx")
call ce("Units\\Orc\\Wolfrider\\RaiderDeath.wav")
call ce("Objects\\Spawnmodels\\Orc\\Orcblood\\OrcBloodWolfrider.mdx")
call ce("Units\\Creeps\\ForestTroll\\ForestTrollDeath.wav")
call ce("units\\orc\\Shaman\\Shaman.mdx")
call ce("units\\Orc\\Shaman\\Shaman.blp")
call ce("units\\orc\\Shaman\\Shaman_portrait.mdx")
call ce("units\\orc\\BatTroll\\BatTroll.mdx")
call ce("Textures\\BatTroll.blp")
call ce("Textures\\GenericGlow2_mip1.blp")
call ce("Textures\\Dust6Color.blp")
call ce("Textures\\RingOFire.blp")
call ce("Textures\\grad6.blp")
call ce("Textures\\star32.blp")
call ce("units\\orc\\BatTroll\\BatTroll_portrait.mdx")
call ce("Abilities\\Spells\\Orc\\Bloodlust\\BloodlustSpecial.mdx")
call ce("Textures\\Shockwave4white.blp")
call ce("Textures\\Red_star2.blp")
call ce("Abilities\\Spells\\Orc\\Bloodlust\\BloodlustTarget.wav")
call ce("Units\\Orc\\Shaman\\ShamanDeath.wav")
call ce("Units\\Orc\\BatTroll\\TrollbatriderWhat3.wav")
call ce("Units\\Orc\\BatTroll\\TrollbatriderYes2.wav")
call ce("Units\\Orc\\BatTroll\\TrollbatriderWhat5.wav")
call ce("Units\\Orc\\BatTroll\\TrollbatriderYes1.wav")
call ce("Abilities\\Spells\\Orc\\Voodoo\\VoodooAuraTarget.mdx")
call ce("Units\\Orc\\BatTroll\\BatRiderDeath1.wav")
call ce("Objects\\Spawnmodels\\Orc\\Orcblood\\BattrollBlood.mdx")
call ce("units\\orc\\spiritwalker\\spiritwalker.mdx")
call ce("units\\Orc\\SpiritWalker\\TaurenSpiritWalker.blp")
call ce("Textures\\RibbonBlur1.blp")
call ce("units\\orc\\spiritwalker\\spiritwalker_portrait.mdx")
call ce("units\\orc\\Tauren\\Tauren.mdx")
call ce("units\\Orc\\Tauren\\Minotaur.blp")
call ce("units\\orc\\Tauren\\Tauren_portrait.mdx")
call ce("Sound\\Units\\Footsteps\\Step1.wav")
call ce("Sound\\Units\\Footsteps\\Step3.wav")
call ce("Sound\\Units\\Footsteps\\Step4.wav")
call ce("Sound\\Units\\Footsteps\\BigWaterStep4.wav")
call ce("Sound\\Units\\Footsteps\\Step2.wav")
call ce("Sound\\Units\\Footsteps\\BigWaterStep2.wav")
call ce("Sound\\Units\\Footsteps\\BigWaterStep1.wav")
call ce("Sound\\Units\\Footsteps\\BigWaterStep3.wav")
call ce("Abilities\\Spells\\Orc\\SpiritLink\\SpiritLinkZapTarget.mdx")
call ce("Textures\\Star8c.blp")
call ce("Textures\\Yellow_Glow.blp")
call ce("ReplaceableTextures\\Weather\\LightningSpirit.blp")
call ce("Abilities\\Spells\\Orc\\SpiritLink\\SpiritLinkTarget.mdx")
call ce("Textures\\Yellow_Glow2.blp")
call ce("Abilities\\Spells\\Orc\\WarStomp\\WarStompCaster.mdx")
call ce("Textures\\grad2d.blp")
call ce("Abilities\\Spells\\Human\\ThunderClap\\ThunderClapCaster.wav")
call ce("ReplaceableTextures\\Splats\\ThunderClapUbersplat.blp")
call ce("Units\\Orc\\HeroTaurenChieftain\\HeroTaurenChieftainDeath.wav")
call ce("Sound\\Units\\Orc\\OrcDissipate1.wav")
call ce("Objects\\Spawnmodels\\Orc\\Orcblood\\OrcBloodHeroTaurenChieftain.mdx")
call ce("Units\\Orc\\Tauren\\TaurenDeath1.wav")
call ce("Objects\\Spawnmodels\\Orc\\Orcblood\\OrcBloodTauren.mdx")
call ce("Units\\Human\\Peasant\\PeasantWhat4.wav")
call ce("Abilities\\Spells\\Other\\Transmute\\PileofGold.mdx")
call ce("abilities\\Spells\\Other\\Transmute\\Green_Firering2b.blp")
call ce("Textures\\Clouds8x8Fade.blp")
call ce("Textures\\star6.blp")
call ce("abilities\\Spells\\Other\\Transmute\\gold.blp")
call ce("Abilities\\Spells\\Human\\Polymorph\\PolymorphDone.wav")
call ce("Abilities\\Spells\\Other\\Transmute\\AlchemistTransmuteDeath1.wav")
call ce("units\\nightelf\\MountainGiant\\MountainGiant.mdx")
call ce("Textures\\MountainGiant.blp")
call ce("units\\nightelf\\MountainGiant\\MountainGiant_portrait.mdx")
call ce("Abilities\\Spells\\NightElf\\Taunt\\TauntCaster.mdx")
call ce("Textures\\firering1A.blp")
call ce("Abilities\\Spells\\NightElf\\Taunt\\Taunt.wav")
call ce("Units\\NightElf\\MountainGiant\\MountainGiantDeath1.wav")
call ce("ReplaceableTextures\\Selection\\SelectionCircleMed.blp")
call ce("Units\\Orc\\Wolfrider\\WolfriderWhat3.wav")
call ce("Units\\Orc\\Wolfrider\\WolfriderYes4.wav")
call ce("Units\\Orc\\Tauren\\TaurenWhat3.wav")
call ce("units\\undead\\Skeleton\\Skeleton.mdx")
call ce("Textures\\Skeleton.blp")
call ce("units\\undead\\Skeleton\\Skeleton_portrait.mdx")
call ce("units\\undead\\Ghoul\\Ghoul.mdx")
call ce("units\\Undead\\Ghoul\\Ghoul.blp")
call ce("Textures\\Peasant.blp")
call ce("units\\undead\\Ghoul\\Ghoul_portrait.mdx")
call ce("units\\undead\\Abomination\\Abomination.mdx")
call ce("Textures\\Abomination.blp")
call ce("Textures\\BloodSplash.blp")
call ce("Textures\\BloodSput.blp")
call ce("Textures\\OrcBloodTailParticle.blp")
call ce("units\\undead\\Abomination\\Abomination_portrait.mdx")
call ce("Abilities\\Spells\\Undead\\VampiricAura\\VampiricAuraTarget.mdx")
call ce("Abilities\\Spells\\Human\\Heal\\HealTarget.wav")
call ce("Units\\Undead\\Ghoul\\GhoulDeath.wav")
call ce("Objects\\Spawnmodels\\Undead\\UndeadBlood\\UndeadBloodAbomination.mdx")
call ce("Objects\\Spawnmodels\\Undead\\UndeadBlood\\UndeadBloodGhoul.mdx")
call ce("Units\\Undead\\Abomination\\AbominationDeath1.wav")
call ce("Units\\Undead\\Skeleton\\SkeletonDeath1.wav")
call ce("units\\undead\\Banshee\\Banshee.mdx")
call ce("Textures\\banshee.blp")
call ce("units\\undead\\Banshee\\Banshee_portrait.mdx")
call ce("units\\undead\\Necromancer\\Necromancer.mdx")
call ce("units\\Undead\\Necromancer\\Necromancer.blp")
call ce("units\\undead\\Necromancer\\Necromancer_portrait.mdx")
call ce("ReplaceableTextures\\TeamGlow\\TeamGlow04.blp")
call ce("units\\undead\\CryptFiend\\CryptFiend.mdx")
call ce("units\\Undead\\CryptFiend\\CryptFiend.blp")
call ce("ReplaceableTextures\\Splats\\BurrowSplat.blp")
call ce("units\\undead\\CryptFiend\\CryptFiend_portrait.mdx")
call ce("Textures\\CryptFiend.blp")
call ce("units\\undead\\FrostWyrm\\FrostWyrm.mdx")
call ce("units\\Undead\\FrostWyrm\\FrostWyrm.blp")
call ce("units\\undead\\FrostWyrm\\FrostWyrm_portrait.mdx")
call ce("units\\Undead\\FrostWyrm\\starfield.blp")
call ce("Sound\\Units\\Footsteps\\FiendStep2.wav")
call ce("Sound\\Units\\Footsteps\\FiendStep1.wav")
call ce("Sound\\Units\\Footsteps\\FiendStep3.wav")
call ce("Sound\\Units\\Footsteps\\FiendStep4.wav")
call ce("Abilities\\Spells\\Undead\\FreezingBreath\\FreezingBreathMissile.mdx")
call ce("Textures\\Dust6.blp")
call ce("Textures\\Shockwave_blue.blp")
call ce("Units\\Undead\\FrostWyrm\\FrostwyrmAttack1.wav")
call ce("Abilities\\Spells\\Undead\\FrostArmor\\FrostArmorTarget.mdx")
call ce("Textures\\Ghost2.blp")
call ce("Abilities\\Spells\\Undead\\FrostArmor\\FrostArmorTarget1.wav")
call ce("Abilities\\Spells\\Undead\\FreezingBreath\\FreezingBreathTargetArt.mdx")
call ce("Textures\\Ice3b.blp")
call ce("Textures\\snowflake.blp")
call ce("Abilities\\Spells\\Undead\\AntiMagicShell\\AntiMagicShell.mdx")
call ce("Textures\\Rune4b.blp")
call ce("Abilities\\Spells\\Undead\\AntiMagicShell\\AntiMagicShellBirth1.wav")
call ce("Abilities\\Spells\\Undead\\FreezingBreath\\FreezingBreathTarget1.wav")
call ce("Abilities\\Spells\\Undead\\AnimateDead\\AnimateDeadTarget.mdx")
call ce("Textures\\Green_Glow2.blp")
call ce("Textures\\Clouds8x8Mod.blp")
call ce("Abilities\\Spells\\Undead\\RaiseSkeletonWarrior\\RaiseSkeleton.wav")
call ce("Units\\Undead\\Banshee\\BansheeDeath.wav")
call ce("Units\\Undead\\CryptFiend\\PitFiendDeath1.wav")
call ce("Objects\\Spawnmodels\\Undead\\UndeadBlood\\UndeadBloodCryptFiend.mdx")
call ce("units\\nightelf\\Archer\\Archer.mdx")
call ce("Textures\\Ranger.blp")
call ce("units\\nightelf\\Archer\\Archer_portrait.mdx")
call ce("units\\NightElf\\Archer\\Ranger.blp")
call ce("units\\nightelf\\Ent\\Ent.mdx")
call ce("units\\NightElf\\Ent\\GoodEnt.blp")
call ce("SharedModels\\leaves.mdx")
call ce("SharedModels\\TjLeaves.blp")
call ce("units\\nightelf\\Ent\\Ent_portrait.mdx")
call ce("units\\nightelf\\Dryad\\Dryad.mdx")
call ce("units\\NightElf\\Dryad\\Dryad.blp")
call ce("units\\nightelf\\Dryad\\Dryad_portrait.mdx")
call ce("Textures\\Dryad.blp")
call ce("Units\\NightElf\\Archer\\ArcherDeath1.wav")
call ce("Objects\\Spawnmodels\\NightElf\\NightElfBlood\\NightElfBloodArcher.mdx")
call ce("Units\\NightElf\\Dryad\\DryadDeath1.wav")
call ce("Objects\\Spawnmodels\\NightElf\\NightElfBlood\\NightElfBloodDryad.mdx")
call ce("Units\\Creeps\\CorruptedEnt\\EntDeath1.wav")
call ce("units\\nightelf\\DruidoftheTalon\\DruidoftheTalon.mdx")
call ce("units\\NightElf\\DruidOfTheTalon\\DruidottheTalon.blp")
call ce("units\\NightElf\\DruidOfTheTalon\\Raven.blp")
call ce("units\\NightElf\\DruidOfTheTalon\\ButterFly.blp")
call ce("Textures\\Star7b.blp")
call ce("SharedModels\\FeatherPurple1.MDx")
call ce("SharedModels\\FeatherPurple.blp")
call ce("SharedModels\\FeatherPurple.MDx")
call ce("units\\nightelf\\DruidoftheTalon\\DruidoftheTalon_portrait.mdx")
call ce("units\\nightelf\\FaerieDragon\\FaerieDragon.mdx")
call ce("units\\NightElf\\FaerieDragon\\FaerieDragon.blp")
call ce("Textures\\lensflare1A.blp")
call ce("Textures\\CrystalBall.blp")
call ce("Textures\\Rune1.blp")
call ce("units\\nightelf\\FaerieDragon\\FaerieDragon_portrait.mdx")
call ce("units\\nightelf\\DruidoftheClaw\\DruidoftheClaw.mdx")
call ce("units\\NightElf\\DruidOfTheClaw\\Druidoftheclaw.blp")
call ce("units\\NightElf\\DruidOfTheClaw\\Bear.blp")
call ce("Textures\\DeathScream.blp")
call ce("Textures\\RibbonNE1.blp")
call ce("units\\NightElf\\DruidOfTheClaw\\Tj_leaf.blp")
call ce("units\\nightelf\\DruidoftheClaw\\DruidoftheClaw_portrait.mdx")
call ce("Abilities\\Spells\\NightElf\\Rejuvenation\\RejuvenationTarget.mdx")
call ce("Abilities\\Spells\\NightElf\\Rejuvenation\\RejuvenationTarget1.wav")
call ce("Abilities\\Spells\\NightElf\\BattleRoar\\RoarTarget.mdx")
call ce("abilities\\Spells\\NightElf\\BattleRoar\\BattleRoar.blp")
call ce("Abilities\\Spells\\NightElf\\BattleRoar\\RoarCaster.mdx")
call ce("Abilities\\Spells\\NightElf\\BattleRoar\\BattleRoar.wav")
call ce("Units\\NightElf\\DruidOfTheTalon\\DruidOfTheTalonDeath1.wav")
call ce("Objects\\Spawnmodels\\NightElf\\NightElfBlood\\NightElfBloodDruidoftheTalon.mdx")
call ce("Units\\NightElf\\DruidOfTheClaw\\DruidOfTheClawDeath1.wav")
call ce("Objects\\Spawnmodels\\NightElf\\NightElfBlood\\NightElfBloodDruidoftheClaw.mdx")
call ce("Units\\Undead\\Ghoul\\GhoulWhat2.wav")
call ce("Units\\Undead\\Ghoul\\GhoulWhat3.wav")
call ce("Units\\Undead\\Ghoul\\GhoulWhat1.wav")
call ce("Units\\Undead\\Ghoul\\GhoulYes4.wav")
call ce("Units\\Undead\\Ghoul\\GhoulYes1.wav")
call ce("Units\\Undead\\FrostWyrm\\FrostwyrmWhat1.wav")
call ce("Units\\Undead\\FrostWyrm\\FrostwyrmYes2.wav")
call ce("Units\\Undead\\FrostWyrm\\FrostwyrmYes3.wav")
call ce("Units\\Undead\\Necromancer\\NecromancerDeath.wav")
call ce("Objects\\Spawnmodels\\Undead\\UndeadBlood\\UndeadBloodNecromancer.mdx")
call ce("Units\\NightElf\\FaerieDragon\\FaerieDragonDeath1.wav")
endfunction
function Ge takes integer a returns nothing
local unit d=CreateUnit(Player($B),1685417264,0,0,0)
call UnitAddAbility(d,a)
call RemoveUnit(d)
set d=null
endfunction
function He takes nothing returns nothing
local integer A=0
set A=1093677136
call Ge(A)
set A=1093677125
call Ge(A)
set A=1093677137
call Ge(A)
set A=1093677111
call Ge(A)
set A=1093677126
call Ge(A)
set A=1093677131
call Ge(A)
set A=1093677139
call Ge(A)
set A=1093677127
call Ge(A)
set A=1093677399
call Ge(A)
set A=1093677385
call Ge(A)
set A=1093677365
call Ge(A)
set A=1093677107
call Ge(A)
set A=1093677393
call Ge(A)
set A=1093677396
call Ge(A)
set A=1093677389
call Ge(A)
set A=1093677384
call Ge(A)
set A=1093677391
call Ge(A)
set A=1093677378
call Ge(A)
set A=1093677363
call Ge(A)
set A=1093677106
call Ge(A)
set A=1093677379
call Ge(A)
set A=1093677108
call Ge(A)
set A=1093677633
call Ge(A)
set A=1093677621
call Ge(A)
set A=1093677619
call Ge(A)
set A=1093677623
call Ge(A)
set A=1093677637
call Ge(A)
set A=1093677635
call Ge(A)
set A=1098282348
call Ge(A)
set A=1093677658
call Ge(A)
set A=1093677140
call Ge(A)
set A=1093677361
call Ge(A)
set A=1093677146
call Ge(A)
set A=1093677648
call Ge(A)
set A=1093677657
call Ge(A)
set A=1093677382
call Ge(A)
set A=1093677145
call Ge(A)
set A=1093677128
call Ge(A)
set A=1093677656
call Ge(A)
set A=1093677142
call Ge(A)
set A=1093677872
call Ge(A)
set A=1093677143
call Ge(A)
set A=1093677655
call Ge(A)
set A=1093677649
call Ge(A)
set A=1093677875
call Ge(A)
set A=1093677878
call Ge(A)
set A=1093677876
call Ge(A)
set A=1093677877
call Ge(A)
set A=1093677879
call Ge(A)
set A=1093677368
call Ge(A)
set A=1093677104
call Ge(A)
set A=1093677141
call Ge(A)
set A=1093677395
call Ge(A)
set A=1093677109
call Ge(A)
set A=1093677624
call Ge(A)
set A=1093677380
call Ge(A)
set A=1093677124
call Ge(A)
set A=1093677638
call Ge(A)
set A=1093677367
call Ge(A)
set A=1093677366
call Ge(A)
set A=1093677110
call Ge(A)
set A=1093677105
call Ge(A)
set A=1093677113
call Ge(A)
set A=1093677381
call Ge(A)
set A=1110454320
call Ge(A)
set A=1110454321
call Ge(A)
set A=1110454324
call Ge(A)
set A=1110454325
call Ge(A)
set A=1110454326
call Ge(A)
endfunction
function le takes nothing returns nothing
local integer a=0
set y8[a]=o
set A8[a]=j
set a8[a]=h
set x8[a]=w
set z8[a]=z
set a=1
set y8[a]=B
set A8[a]=N
set a8[a]=Y
set x8[a]=m4
set z8[a]=w4
set a=2
set y8[a]=C
set A8[a]=O
set a8[a]=Z
set x8[a]=n4
set z8[a]=x4
set a=3
set y8[a]=D
set A8[a]=P
set a8[a]=d4
set x8[a]=o4
set z8[a]=y4
set a=4
set y8[a]=E
set A8[a]=Q
set a8[a]=e4
set x8[a]=p4
set z8[a]=z4
set a=5
set y8[a]=F
set A8[a]=R
set a8[a]=f4
set x8[a]=q4
set z8[a]=A4
set a=6
set y8[a]=G
set A8[a]=S
set a8[a]=g4
set x8[a]=r4
set z8[a]=a4
set a=7
set y8[a]=I
set A8[a]=U
set a8[a]=h4
set x8[a]=s4
set z8[a]=B4
set a=8
set y8[a]=J
set A8[a]=V
set a8[a]=i4
set x8[a]=t4
set z8[a]=b4
set a=9
set y8[a]=K
set A8[a]=W
set a8[a]=j4
set x8[a]=u4
set z8[a]=C4
set a=$A
set y8[a]=M
set A8[a]=X
set a8[a]=k4
set x8[a]=v4
set z8[a]=c4
set a=0
set B8=CreateRegion()
loop
exitwhen a>$A
call RegionAddRect(B8,x8[a])
set a=a+1
endloop
set a=0
set b8=CreateRegion()
loop
exitwhen a>$A
call RegionAddRect(b8,A8[a])
set a=a+1
endloop
call TriggerRegisterEnterRegion(m7,B8,null)
call TriggerRegisterLeaveRegion(n7,b8,null)
endfunction
function Ke takes nothing returns nothing
local integer n=-1
local integer T=1953462064
set n=n+1
set m8[n]=T
call SaveInteger(i8,T,1131377524,1)
set T=1953462065
set n=n+1
set m8[n]=T
call SaveInteger(i8,T,1131377524,4)
call SaveReal(i8,T,1148020545,4)
call SaveInteger(i8,T,1148020564,1349024115)
call SaveReal(i8,T,1265200194,.04)
call SaveInteger(i8,T,1096968497,1093677109)
call SaveInteger(i8,T,1096968498,1093677124)
call SaveInteger(i8,T,1165520945,30)
call SaveInteger(i8,T,1165520946,90)
call SaveReal(i8,T,1214605684,72.5)
set T=1953462066
set n=n+1
set m8[n]=T
call SaveInteger(i8,T,1131377524,4)
call SaveReal(i8,T,1148020545,1)
call SaveInteger(i8,T,1148020564,1349024115)
call SaveReal(i8,T,1265200194,.01)
call SaveInteger(i8,T,1096968497,1093677109)
call SaveInteger(i8,T,1096968498,1093677124)
call SaveInteger(i8,T,1165520945,30)
call SaveInteger(i8,T,1165520946,90)
call SaveReal(i8,T,1214605684,30)
set T=1953462067
set n=n+1
set m8[n]=T
call SaveInteger(i8,T,1131377524,4)
call SaveReal(i8,T,1148020545,1)
call SaveInteger(i8,T,1148020564,1349024115)
call SaveReal(i8,T,1265200194,.01)
call SaveInteger(i8,T,1096968497,1093677109)
call SaveInteger(i8,T,1096968498,1093677124)
call SaveInteger(i8,T,1165520945,30)
call SaveInteger(i8,T,1165520946,90)
call SaveReal(i8,T,1214605684,67.5)
set T=1953462068
set n=n+1
set m8[n]=T
call SaveInteger(i8,T,1131377524,4)
call SaveReal(i8,T,1148020545,6)
call SaveInteger(i8,T,1148020564,1298229091)
call SaveReal(i8,T,1265200194,.06)
call SaveInteger(i8,T,1096968497,1093677395)
call SaveInteger(i8,T,1096968498,1093677380)
call SaveInteger(i8,T,1165520945,30)
call SaveInteger(i8,T,1165520946,90)
call SaveReal(i8,T,1214605684,127.5)
set T=1953462069
set n=n+1
set m8[n]=T
call SaveInteger(i8,T,1131377524,4)
call SaveReal(i8,T,1148020545,2.25)
call SaveInteger(i8,T,1148020564,1298229091)
call SaveReal(i8,T,1265200194,.0225)
call SaveInteger(i8,T,1096968497,1093677395)
call SaveInteger(i8,T,1096968498,1093677380)
call SaveInteger(i8,T,1165520945,30)
call SaveInteger(i8,T,1165520946,90)
call SaveReal(i8,T,1214605684,127.5)
set T=1953462070
set n=n+1
set m8[n]=T
call SaveInteger(i8,T,1131377524,4)
call SaveReal(i8,T,1148020545,3.6)
call SaveInteger(i8,T,1148020564,1298229091)
call SaveReal(i8,T,1265200194,.036)
call SaveInteger(i8,T,1096968497,1093677395)
call SaveInteger(i8,T,1096968498,1093677380)
call SaveInteger(i8,T,1165520945,30)
call SaveInteger(i8,T,1165520946,90)
call SaveReal(i8,T,1214605684,127.5)
set T=1953462071
set n=n+1
set m8[n]=T
call SaveInteger(i8,T,1131377524,8)
call SaveReal(i8,T,1148020545,4)
call SaveInteger(i8,T,1148020564,1348825699)
call SaveReal(i8,T,1097811282,92)
call SaveReal(i8,T,1097811268,.5)
call SaveReal(i8,T,1265200194,.04)
call SaveInteger(i8,T,1096968497,1093677624)
call SaveInteger(i8,T,1096968498,1093677638)
call SaveInteger(i8,T,1165520945,30)
call SaveInteger(i8,T,1165520946,90)
call SaveReal(i8,T,1214605684,67.5)
set T=1953462072
set n=n+1
set m8[n]=T
call SaveInteger(i8,T,1131377524,6)
call SaveReal(i8,T,1148020545,6)
call SaveInteger(i8,T,1148020564,1349024115)
call SaveReal(i8,T,1097811282,92)
call SaveReal(i8,T,1097811268,.5)
call SaveReal(i8,T,1265200194,.06)
call SaveInteger(i8,T,1096968497,1093677624)
call SaveInteger(i8,T,1096968498,1093677638)
call SaveInteger(i8,T,1165520945,30)
call SaveInteger(i8,T,1165520946,90)
call SaveReal(i8,T,1214605684,117.5)
set T=1953462073
set n=n+1
set m8[n]=T
call SaveInteger(i8,T,1131377524,6)
call SaveReal(i8,T,1148020545,4)
call SaveInteger(i8,T,1148020564,1298229091)
call SaveReal(i8,T,1097811282,92)
call SaveReal(i8,T,1097811268,.5)
call SaveReal(i8,T,1265200194,.04)
call SaveInteger(i8,T,1096968497,1093677624)
call SaveInteger(i8,T,1096968498,1093677638)
call SaveInteger(i8,T,1165520945,30)
call SaveInteger(i8,T,1165520946,90)
call SaveReal(i8,T,1214605684,117.5)
set TT=n
endfunction
function Me takes nothing returns nothing
local integer n=-1
local integer T=1747988528
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,4)
call SaveInteger(i8,T,1197566318,5)
call SaveReal(i8,T,1214870608,40)
call SaveInteger(i8,T,1165520967,1)
call SaveInteger(i8,T,1281977716,$A)
call SaveInteger(i8,T,1399879534,1097347120)
set T=1747988529
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,$C)
call SaveInteger(i8,T,1197566318,$D)
call SaveReal(i8,T,1214870608,80)
call SaveInteger(i8,T,1165520967,1)
call SaveInteger(i8,T,1281977716,$A)
call SaveInteger(i8,T,1399879534,1097347121)
call SaveInteger(i8,T,1096968556,1093677146)
call SaveStr(i8,T,1330869362,"magicdefense")
set T=1747988530
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,20)
call SaveInteger(i8,T,1197566318,21)
call SaveReal(i8,T,1214870608,$A0)
call SaveInteger(i8,T,1346466413,75)
call SaveInteger(i8,T,1165520967,1)
call SaveInteger(i8,T,1281977716,$A)
call SaveInteger(i8,T,1399879534,1097347122)
set T=1747988531
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,16)
call SaveInteger(i8,T,1197566318,16)
call SaveReal(i8,T,1214870608,60)
call SaveInteger(i8,T,1296134765,20)
call SaveInteger(i8,T,1165520967,2)
call SaveInteger(i8,T,1281977716,5)
call SaveInteger(i8,T,1399879534,1097347123)
call SaveInteger(i8,T,1096968556,1093677648)
call SaveInteger(i8,T,1096968532,2)
call SaveStr(i8,T,1330869362,"innerfire")
set T=1747988532
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,36)
call SaveInteger(i8,T,1197566318,36)
call SaveReal(i8,T,1214870608,120)
call SaveInteger(i8,T,1165520967,2)
call SaveInteger(i8,T,1281977716,4)
call SaveInteger(i8,T,1399879534,1097347124)
call SaveInteger(i8,T,1096968556,1093677361)
call SaveInteger(i8,T,1096968532,1)
call SaveStr(i8,T,1330869362,"cloudoffog")
set T=1747988533
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,8)
call SaveInteger(i8,T,1197566318,8)
call SaveReal(i8,T,1214870608,50)
call SaveInteger(i8,T,1296134765,20)
call SaveInteger(i8,T,1165520967,2)
call SaveInteger(i8,T,1281977716,5)
call SaveInteger(i8,T,1399879534,1097347125)
call SaveInteger(i8,T,1096968556,1093677145)
call SaveInteger(i8,T,1096968532,2)
call SaveStr(i8,T,1330869362,"invisibility")
set T=1747988534
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,20)
call SaveInteger(i8,T,1197566318,21)
call SaveReal(i8,T,1214870608,125)
call SaveInteger(i8,T,1346466413,20)
call SaveInteger(i8,T,1165520967,1)
call SaveInteger(i8,T,1281977716,$A)
call SaveInteger(i8,T,1399879534,1097347126)
call SaveInteger(i8,T,1096968556,1093677128)
call SaveStr(i8,T,1330869362,"magicdefense")
set T=1865429040
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,4)
call SaveInteger(i8,T,1197566318,5)
call SaveReal(i8,T,1214870608,35)
call SaveInteger(i8,T,1346466413,45)
call SaveInteger(i8,T,1165520967,1)
call SaveInteger(i8,T,1281977716,$A)
call SaveInteger(i8,T,1399879534,1097805872)
set T=1865429041
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,$C)
call SaveInteger(i8,T,1197566318,$D)
call SaveReal(i8,T,1214870608,90)
call SaveInteger(i8,T,1346466413,20)
call SaveInteger(i8,T,1165520967,1)
call SaveInteger(i8,T,1281977716,$A)
call SaveInteger(i8,T,1399879534,1097805873)
set T=1865429042
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,16)
call SaveInteger(i8,T,1197566318,17)
call SaveReal(i8,T,1214870608,120)
call SaveInteger(i8,T,1346466413,20)
call SaveInteger(i8,T,1165520967,1)
call SaveInteger(i8,T,1281977716,$A)
call SaveInteger(i8,T,1399879534,1097805874)
set T=1865429043
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,$C)
call SaveInteger(i8,T,1197566318,$C)
call SaveReal(i8,T,1214870608,65)
call SaveInteger(i8,T,1296134765,20)
call SaveInteger(i8,T,1165520967,2)
call SaveInteger(i8,T,1281977716,5)
call SaveInteger(i8,T,1399879534,1097805875)
call SaveInteger(i8,T,1096968556,1093677140)
call SaveInteger(i8,T,1096968532,2)
call SaveStr(i8,T,1330869362,"bloodlust")
set T=1865429044
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,48)
call SaveInteger(i8,T,1197566318,48)
call SaveReal(i8,T,1214870608,90)
call SaveInteger(i8,T,1346466413,20)
call SaveInteger(i8,T,1165520967,2)
call SaveInteger(i8,T,1281977716,4)
call SaveInteger(i8,T,1399879534,1097805876)
set T=1865429045
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,28)
call SaveInteger(i8,T,1197566318,28)
call SaveReal(i8,T,1214870608,110)
call SaveBoolean(i8,T,1346466413,true)
call SaveInteger(i8,T,1165520967,2)
call SaveInteger(i8,T,1281977716,5)
call SaveInteger(i8,T,1399879534,1097805877)
call SaveInteger(i8,T,1096968556,1093677655)
call SaveInteger(i8,T,1096968532,2)
call SaveStr(i8,T,1330869362,"healingwave")
set T=1865429046
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,44)
call SaveInteger(i8,T,1197566318,44)
call SaveReal(i8,T,1214870608,260)
call SaveInteger(i8,T,1346466413,40)
call SaveInteger(i8,T,1165520967,2)
call SaveInteger(i8,T,1281977716,5)
call SaveInteger(i8,T,1399879534,1097805878)
call SaveInteger(i8,T,1096968556,1093677656)
call SaveStr(i8,T,1330869362,"stomp")
set T=1966092336
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,4)
call SaveInteger(i8,T,1197566318,5)
call SaveReal(i8,T,1214870608,35)
call SaveInteger(i8,T,1346466413,30)
call SaveInteger(i8,T,1296134765,30)
call SaveInteger(i8,T,1165520967,1)
call SaveInteger(i8,T,1281977716,$A)
call SaveInteger(i8,T,1399879534,1098199088)
set T=1966092337
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,$C)
call SaveInteger(i8,T,1197566318,$D)
call SaveReal(i8,T,1214870608,70)
call SaveInteger(i8,T,1346466413,20)
call SaveInteger(i8,T,1296134765,20)
call SaveInteger(i8,T,1165520967,1)
call SaveInteger(i8,T,1281977716,$A)
call SaveInteger(i8,T,1399879534,1098199089)
set T=1966092338
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,20)
call SaveInteger(i8,T,1197566318,20)
call SaveReal(i8,T,1214870608,110)
call SaveInteger(i8,T,1346466413,30)
call SaveInteger(i8,T,1296134765,30)
call SaveInteger(i8,T,1165520967,2)
call SaveInteger(i8,T,1281977716,5)
call SaveInteger(i8,T,1399879534,1098199090)
call SaveInteger(i8,T,1096968556,1093677382)
call SaveInteger(i8,T,1096968532,2)
call SaveStr(i8,T,1330869362,"frostarmor")
set T=1966092339
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,20)
call SaveInteger(i8,T,1197566318,20)
call SaveReal(i8,T,1214870608,60)
call SaveInteger(i8,T,1296134765,30)
call SaveInteger(i8,T,1165520967,2)
call SaveInteger(i8,T,1281977716,5)
call SaveInteger(i8,T,1399879534,1098199091)
call SaveInteger(i8,T,1096968556,1093677142)
call SaveStr(i8,T,1330869362,"raisedead")
set T=1966092340
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,72)
call SaveInteger(i8,T,1197566318,72)
call SaveReal(i8,T,1214870608,$F0)
call SaveInteger(i8,T,1165520967,2)
call SaveInteger(i8,T,1281977716,4)
call SaveInteger(i8,T,1399879534,1098199092)
call SaveInteger(i8,T,1096968556,1093677657)
call SaveInteger(i8,T,1096968532,2)
call SaveStr(i8,T,1330869362,"thunderbolt")
set T=1966092341
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,8)
call SaveInteger(i8,T,1197566318,8)
call SaveReal(i8,T,1214870608,50)
call SaveInteger(i8,T,1296134765,20)
call SaveInteger(i8,T,1165520967,2)
call SaveInteger(i8,T,1281977716,5)
call SaveInteger(i8,T,1399879534,1098199093)
call SaveInteger(i8,T,1096968556,1093677658)
call SaveInteger(i8,T,1096968532,2)
call SaveStr(i8,T,1330869362,"antimagicshell")
set T=1966092342
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,36)
call SaveInteger(i8,T,1197566318,37)
call SaveReal(i8,T,1214870608,$C8)
call SaveInteger(i8,T,1346466413,30)
call SaveInteger(i8,T,1296134765,30)
call SaveInteger(i8,T,1165520967,1)
call SaveInteger(i8,T,1281977716,$A)
call SaveInteger(i8,T,1399879534,1098199094)
set T=1697656880
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,4)
call SaveInteger(i8,T,1197566318,5)
call SaveReal(i8,T,1214870608,35)
call SaveInteger(i8,T,1296134765,45)
call SaveInteger(i8,T,1165520967,1)
call SaveInteger(i8,T,1281977716,$A)
call SaveInteger(i8,T,1399879534,1097150512)
set T=1697656881
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,$C)
call SaveInteger(i8,T,1197566318,$D)
call SaveReal(i8,T,1214870608,80)
call SaveInteger(i8,T,1296134765,60)
call SaveInteger(i8,T,1165520967,1)
call SaveInteger(i8,T,1281977716,$A)
call SaveInteger(i8,T,1399879534,1097150513)
set T=1697656882
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,16)
call SaveInteger(i8,T,1197566318,17)
call SaveReal(i8,T,1214870608,80)
call SaveBoolean(i8,T,1296134765,true)
call SaveInteger(i8,T,1165520967,1)
call SaveInteger(i8,T,1281977716,$A)
call SaveInteger(i8,T,1399879534,1097150514)
set T=1697656883
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,8)
call SaveInteger(i8,T,1197566318,8)
call SaveReal(i8,T,1214870608,50)
call SaveInteger(i8,T,1296134765,20)
call SaveInteger(i8,T,1165520967,2)
call SaveInteger(i8,T,1281977716,5)
call SaveInteger(i8,T,1399879534,1097150515)
call SaveInteger(i8,T,1096968556,1093677872)
call SaveInteger(i8,T,1096968532,2)
call SaveStr(i8,T,1330869362,"rejuvination")
set T=1697656884
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,48)
call SaveInteger(i8,T,1197566318,48)
call SaveReal(i8,T,1214870608,90)
call SaveInteger(i8,T,1296134765,20)
call SaveInteger(i8,T,1165520967,2)
call SaveInteger(i8,T,1281977716,4)
call SaveInteger(i8,T,1399879534,1097150516)
set T=1697656885
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,$C)
call SaveInteger(i8,T,1197566318,$C)
call SaveReal(i8,T,1214870608,80)
call SaveInteger(i8,T,1165520967,2)
call SaveInteger(i8,T,1281977716,5)
call SaveInteger(i8,T,1399879534,1097150517)
call SaveInteger(i8,T,1096968556,1093677143)
call SaveStr(i8,T,1330869362,"roar")
set T=1697656886
set n=n+1
set j8[n]=T
call SaveInteger(i8,T,1131377524,60)
call SaveInteger(i8,T,1197566318,60)
call SaveReal(i8,T,1214870608,280)
call SaveInteger(i8,T,1346466413,75)
call SaveInteger(i8,T,1296134765,55)
call SaveInteger(i8,T,1165520967,2)
call SaveInteger(i8,T,1281977716,5)
call SaveInteger(i8,T,1399879534,1097150518)
call SaveInteger(i8,T,1096968556,1093677649)
call SaveStr(i8,T,1330869362,"taunt")
set MT=n
endfunction
function Oe takes nothing returns nothing
set AT=AT+1
set X7[AT]=1953063787
set AT=AT+1
set X7[AT]=1651861094
set AT=AT+1
set X7[AT]=1093677125
set AT=AT+1
set X7[AT]=1093677126
set AT=AT+1
set X7[AT]=1400141166
set AT=AT+1
set X7[AT]=1093677106
set AT=AT+1
set X7[AT]=1865429041
set AT=AT+1
set X7[AT]=1865429044
set AT=AT+1
set X7[AT]=1093677655
set AT=AT+1
set X7[AT]=1697656881
set AT=AT+1
set X7[AT]=1697656884
set AT=AT+1
set X7[AT]=1093677146
set AT=AT+1
set X7[AT]=1093677648
set AT=AT+1
set X7[AT]=1093677145
set AT=AT+1
set X7[AT]=1093677128
set AT=AT+1
set X7[AT]=1093677140
set AT=AT+1
set X7[AT]=1093677382
set AT=AT+1
set X7[AT]=1093677658
set AT=AT+1
set X7[AT]=1093677872
set AT=AT+1
set X7[AT]=1093677143
endfunction
function Qe takes nothing returns nothing
local integer n=0
set g8[n]="|cffff0000"
set n=1
set g8[n]="|cff0000ff"
set n=2
set g8[n]="|cff00ffff"
set n=3
set g8[n]="|cff691e87"
set n=4
set g8[n]="|cffffff00"
set n=5
set g8[n]="|cffd25a1e"
set n=6
set g8[n]="|cff00ff00"
set n=7
set g8[n]="|cffff8080"
set n=8
set g8[n]="|cff808080"
set n=9
set g8[n]="|cff8080ff"
set n=$A
set g8[n]="|cff008080"
endfunction
function Se takes nothing returns nothing
local string s
local quest q=CreateQuest()
local questitem Te=QuestCreateItem(q)
set W7="Default game settings:
|cffcc8080"+I2S(O7)+"|r lives.
|cff0080ff1|r race per player.
"
set W7=W7+"|cffffff00"+I2S(60)+"|r starting gold.
All races are |cff00ff00allowed|r.
"
set W7=W7+"Players send their minions to random foe or themselves."
call QuestItemSetDescription(Te,"Choose race of your minions.")
set Te=QuestCreateItem(q)
call QuestItemSetDescription(Te,"Send minions to your foes to defeat them.")
set Te=QuestCreateItem(q)
call QuestItemSetDescription(Te,"Build towers to protect yourself from enemy minions.")
set Te=QuestCreateItem(q)
call QuestItemSetDescription(Te,"You will lose when amount of your lives becomes |cffcc80800|r.")
set s="Players should choose the race of their minions, then build a maze of towers, spawn some minions and press button 'Readiness' in their builder. "
set s=s+"When the wave starts, minions will be teleported to upper watery area of every player. "
set s=s+"If minion reaches bottom watery area, player will lose |cffcc80801|r life. If player loses all lives, he will be defeated. "
set s=s+"As soon as all minions will be killed, players should prepare and then confirm their readiness.
"
set s=s+"Players receive all gold when wave ends. This amount depends on type and number of minions that player has spawned before wave started. "
set s=s+"In addition, kills of minions increases income of player-killer by |cffff8000"+I2S(1)+"|r."
call QuestSetTitle(q,"How to play")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNSelectHeroOn.blp")
set s="Here is next types of damage:
"
set s=s+"|cffff8700Physical|r - deals |cffff8700Physical|r damage. Can be reduced by |cffff8700Physical|r armor and blocked by |cffff8700Immunity|r or |cffccccccProtection|r.
"
set s=s+"|cff8700ffMagical|r - deals |cff8700ffMagical|r damage. Can be reduced by |cff8700ffMagical|r armor and blocked by |cff8700ffImmunity|r or |cffccccccProtection|r.
"
set s=s+"|cffccccccPercetage|r - deals damage that equals some percents of target's maximum health. Cannot be reduced and can be blocked by |cffccccccProtection|r.

"
set s=s+"If minion has |cffff8700x|r\\|cff8700ffy|r armor, incoming |cffff8700Physical|r damage will be reduced by |cffff8700x%|r and |cff8700ffMagical|r by |cff8700ffy%|r. "
set s=s+"For example, if armor is |cffff870020|r\\|cff8700ff30|r and incoming |cffff8700Physical|r damage is |cffff00004|r, minion will receive only |cffff00003.2|r damage.
"
set s=s+"Upper bound of armor is |cffff870075|r\\|cff8700ff75|r and lower bound is |cffff8700-25|r\\|cff8700ff-25|r.
"
set s=s+"Some more words about |cffccccccPercetage|r damage. If incoming amount of damage is |cffff0000p|r of |cffccccccPercetage|r type, "
set s=s+"minion with |cff33cc33h|r maximum health will receive damage equals |cffff0000p%|r of |cff33cc33h|r. "
set s=s+"For example, if maximum health is |cff33cc33200|r and incoming |cffccccccPersentage|r damage is |cffff00008|r, minion will receive |cffff000016|r damage.
"
set q=CreateQuest()
call QuestSetTitle(q,"Armor & damage system")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNHelmutPurple.blp")
set s="Tower description:
"
set s=s+"Class - class of tower. Affects on tower's abilities.
"
set s=s+"Damage - default damage of tower.
"
set s=s+"Type - damage type.
"
set s=s+"Cooldown - time between two consistent attacks.
"
set s=s+"Range - attack range of tower (in cells).
"
set s=s+"AoE - AoE radius (in cells)\\AoE damage factor.
"
set s=s+"Targets - types of targets that tower can hit.
"
set s=s+"Kill Damage - amount of bonus damage per kill.
"
set s=s+"Levels - amount of experience required to reach 1st\\2nd level.
"
set s=s+"Ability - tower's passive ability.

"
set s=s+"Tower details:
"
set s=s+"Exp. - current amount of experience.
"
set s=s+"Kills - current amount of kills.
"
set s=s+"Damage - current damage of tower.
"
set s=s+"Type - current damage type.

"
set s=s+"Players can build towers only on grinded area and only between waves.
When tower kills minion, it and the nearest ones get experience. "
set s=s+"After reaching certain amount of experience towers get level. On every level player can choose only one ability to improve tower."
set q=CreateQuest()
call QuestSetTitle(q,"Towers")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNHumanWatchTower.blp")
set s="Minion description:
"
set s=s+"Gold - gold cost.
"
set s=s+"Income - income increment.
"
set s=s+"Armor - amount of |cffff8700Physical|r\\|cff8700ffMagical|r armor.
"
set s=s+"Movement - movement speed (cells per second)\\movement type.
"
set s=s+"Exp. - amount of experience gained when killed.
"
set s=s+"Limit - maximum amount of minions of this type that player can spawn.
"
set s=s+"Ability - description minion's ability.

"
set s=s+"When you spawn a minion, it appears in top enclosed watery area. When wave begins, minions will be teleported maintaining their positions relative to each other.
"
set s=s+"Players cannot control their minions. Minion casts their abilities automatically."
set q=CreateQuest()
call QuestSetTitle(q,"Minions")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\WorldEditUI\\Editor-MultipleUnits.blp")
set s="This is list of commands to modify game settings. Type them into chat to set up game.
# - integer number.

"
set s=s+"These commands can be used only by modemaker before game starts:
"
set s=s+"lif# - sets amount of lives (0 < #).
"
set s=s+"gol# - sets amount of starting gold ("+I2S(60)+" < #).
"
set s=s+"rac# - sets amount of races per player (0 < # < 5)
"
set s=s+"hum - allows to select Human race.
"
set s=s+"orc - allows to select Orc race.
"
set s=s+"und - allows to select Undead race.
"
set s=s+"elf - allows to select Night Elf race.
"
set s=s+"-hum - bans Human race.
"
set s=s+"-orc - bans Orc race.
"
set s=s+"-und - bans Undead race.
"
set s=s+"-elf - bans Night Elf race.
"
set s=s+"next - players send their minions to next player.
"
set s=s+"rand - players send their minions to random foe.
"
set s=s+"rans - players send their minions to random foe or themselves.
"
set s=s+"self - players send their minions to themselves.
"
set s=s+"start - starts game.
"
set q=CreateQuest()
call QuestSetTitle(q,"Mode commands")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNLesserRejuvScroll.blp")
set s="Special thanks for |cffccccccxgm.guru/p/wc3|r and |cffff3333youtube.com/2kxaoc|r.

"
set s=s+"Map created by |cff80ccffvk.com/prometheus3375|r.
All original icons and models created by |cff0080ffus.blizzard.com|r.

"
set s=s+"Additional contacts:
Twitter: |cff80ccfftwitter.com/prometheus3375|r.
Telegram: |cff80ccff@Prometheus3375|r.
E-mail: |cff80ccffprometheus3375@gmail.com|r."
set q=CreateQuest()
call QuestSetRequired(q,false)
call QuestSetTitle(q,"Credits")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNScrollOfHealing.blp")
call TriggerExecute(G7)
call TriggerExecute(H7)
call TriggerExecute(I7)
call TriggerExecute(l7)
call TriggerExecute(J7)
call TriggerExecute(K7)
call TriggerExecute(L7)
call TriggerExecute(M7)
set q=CreateQuest()
call QuestSetRequired(q,false)
call QuestSetTitle(q,"|cff909090version 1.0|r")
call QuestSetDescription(q,"Release.")
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNScroll.blp")
set q=null
set Te=null
endfunction
function Ve takes nothing returns nothing
local integer i=0
set h8=h8+1
loop
exitwhen i>$A
if v8[i]then
call SetPlayerState(Player(i),PLAYER_STATE_RESOURCE_LUMBER,h8)
endif
set i=i+1
endloop
endfunction
function We takes nothing returns nothing
local integer a=0
local player p
local string s="Welcome to WispTD!
You are a modemaker this game. You have 60 seconds to modify game settings. Type 'start' to start game immediately.
Click Help (|cffffcc00F9|r) to read tutorials, mode commands and tips."
call TimerStart(CreateTimer(),60,true,function Ve)
loop
exitwhen v8[a]
set a=a+1
endloop
set p=Player(a)
call DisplayTimedTextToPlayer(p,0,0,60,s+"

"+W7)
set s=Ae(a)
set s="Welcome to WispTD!
"+s+" is a modemaker this game. Wait 60 seconds or until "+s+" types 'start'.
Click Help (|cffffcc00F9|r) to read tutorials and tips."
set a=a+1
loop
exitwhen a>$A
if v8[a]then
set p=Player(a)
call DisplayTimedTextToPlayer(p,0,0,60,s+"

"+W7)
endif
set a=a+1
endloop
set C8=CreateTimerDialog(c8)
call TimerDialogSetTitle(C8,"Game starts in:")
call TimerDialogSetTimeColor(C8,$FF,$FF,$FF,$FF)
call TimerDialogSetTitleColor(C8,$FF,$FF,$FF,$FF)
call TimerStart(c8,60,false,null)
set p=null
call TriggerSleepAction(.4)
call TimerDialogDisplay(C8,true)
endfunction
function Ye takes nothing returns nothing
local string s=GetEventPlayerChatString()
local string De=SubString(s,3,4)
local string Ze
local integer l=StringLength(s)
local player p=GetTriggerPlayer()
local boolean b=true
if(De=="-")then
set b=false
endif
if b then
set Ze=" is |cff00ff00allowed|r."
else
set Ze=" is |cffff0000banned|r."
endif
set De=SubString(s,0,3)
if(De=="lif")then
set De=SubString(s,3,l)
set l=S2I(De)
if(l>0)then
set O7=l
call DisplayTimedTextToPlayer(p,0,0,4,"Amount of lives is set to |cffcc8080"+De+"|r.")
endif
elseif(De=="gol")then
set De=SubString(s,3,l)
set l=S2I(De)
if(l>=60)then
set P7=l
call DisplayTimedTextToPlayer(p,0,0,4,"Starting gold is set to |cffffff00"+De+"|r.")
endif
elseif(De=="rac")then
set De=SubString(s,3,4)
set l=S2I(De)
if(l>0)and(l<5)then
set Q7=l
call DisplayTimedTextToPlayer(p,0,0,4,"Amount of races per palyer is set to |cff0080ff"+De+"|r.")
endif
elseif(De=="hum")then
set R7=b
call DisplayTimedTextToPlayer(p,0,0,4,"Human race"+Ze)
elseif(De=="orc")then
set S7=b
call DisplayTimedTextToPlayer(p,0,0,4,"Orc race"+Ze)
elseif(De=="und")then
set T7=b
call DisplayTimedTextToPlayer(p,0,0,4,"Undead race"+Ze)
elseif(De=="elf")then
set U7=b
call DisplayTimedTextToPlayer(p,0,0,4,"Night Elf race"+Ze)
endif
if(s=="next")then
set V7=0
call DisplayTimedTextToPlayer(p,0,0,4,"Players send their minions to next player.")
elseif(s=="rand")then
set V7=1
call DisplayTimedTextToPlayer(p,0,0,4,"Players send their minions to random foe.")
elseif(s=="rans")then
set V7=2
call DisplayTimedTextToPlayer(p,0,0,4,"Players send their minions to random foe or themselves.")
elseif(s=="self")then
set V7=3
call DisplayTimedTextToPlayer(p,0,0,4,"Players send their minions to themselves.")
endif
set p=null
endfunction
function ef takes nothing returns nothing
local integer a=0
local integer T=0
local integer ff=0
local integer k=0
local real x1=0
local real x2=0
local real x=0
local real y=0
local real r1=0
local real r2=0
local player p=null
local string s="Current game settings:
|cffcc8080"
call DestroyTimerDialog(C8)
set C8=null
if(O7==1)then
set s=s+"1|r life."
else
set s=s+I2S(O7)+"|r lives."
endif
set s=s+"
|cffffff00"+I2S(P7)+"|r starting gold.
|cff0080ff"
if(Q7==1)then
set s=s+"1|r race per player.
Human race"
else
set s=s+I2S(Q7)+"|r races per player.
Human race"
endif
if R7 then
set s=s+" is |cff00ff00allowed|r.
Orc race"
else
set s=s+" is |cffff0000banned|r.
Orc race"
endif
if S7 then
set s=s+" is |cff00ff00allowed|r.
Undead race"
else
set s=s+" is |cffff0000banned|r.
Undead race"
endif
if T7 then
set s=s+" is |cff00ff00allowed|r.
Night Elf race"
else
set s=s+" is |cffff0000banned|r.
Night Elf race"
endif
if U7 then
set s=s+" is |cff00ff00allowed|r.
"
else
set s=s+" is |cffff0000banned|r.
"
endif
if bj_isSinglePlayer then
set s=s+"Singleplayer game. You send your minions to yourself."
elseif(V7==0)then
set s=s+"Players send their minions to next player."
elseif(V7==1)then
set s=s+"Players send their minions to random foe."
elseif(V7==2)then
set s=s+"Players send their minions to random foe or themselves."
elseif(V7==3)then
set s=s+"Players send their minions to themselves."
endif
call ClearTextMessages()
loop
exitwhen a>$A
if v8[a]then
set p=Player(a)
call DisplayTimedTextToPlayer(p,0,0,$F,s)
set u8[a]=O7
call n9(a,P7,true)
call SetPlayerState(p,PLAYER_STATE_RESOURCE_FOOD_CAP,40)
set k=0
loop
exitwhen k>$A
call SetPlayerAlliance(p,Player(k),ALLIANCE_SHARED_VISION,true)
set k=k+1
endloop
set I8[(a)*(2)+0]=CreateFogModifierRect(p,FOG_OF_WAR_VISIBLE,z8[a],true,true)
call FogModifierStart(I8[(a)*(2)+0])
set I8[(a)*(2)+1]=CreateFogModifierRect(p,FOG_OF_WAR_VISIBLE,y8[a],true,true)
call FogModifierStart(I8[(a)*(2)+1])
set k=0
set ff=0
set x1=GetRectMinX(a8[a])
set x2=GetRectMaxX(a8[a])
set x=GetRectMinX(a8[a])
set y=GetRectMaxY(a8[a])
call SaveBoolean(i8,a,1198680420,true)
loop
exitwhen k>55
set r1=k*.5
set r2=ff*.5
if(R2I(r1)<r1 and R2I(r2)==r2)or(R2I(r1)==r1 and R2I(r2)<r2)then
set T=1885435442
elseif(R2I(r1)<r1 and R2I(r2)<r2)or(R2I(r1)==r1 and R2I(r2)==r2)then
set T=1885435441
endif
set G8[(a)*(56)+k]=CreateUnit(p,T,x,y,0)
call SetUnitPathing(G8[(a)*(56)+k],false)
set x=x+$80
if(x>x2)then
set y=y-$80
set x=x1
set ff=ff+1
endif
set k=k+1
endloop
set x=GetRectCenterX(a8[a])
set y=GetRectCenterY(a8[a])
set p8[a]=CreateUnit(p,1651270770,x,y,0)
if R7 then
call UnitAddAbility(p8[a],1093677875)
endif
if S7 then
call UnitAddAbility(p8[a],1093677876)
endif
if T7 then
call UnitAddAbility(p8[a],1093677877)
endif
if U7 then
call UnitAddAbility(p8[a],1093677878)
endif
call SetUnitUserData(p8[a],Q7)
set q8[a]=GetHandleId(p8[a])
if h9(a)then
call PanCameraToTimed(x,y,0)
call SelectUnit(p8[a],true)
endif
call TriggerRegisterUnitEvent(f7,p8[a],EVENT_UNIT_ISSUED_POINT_ORDER)
call TriggerRegisterUnitEvent(g7,p8[a],EVENT_UNIT_SPELL_EFFECT)
call TriggerRegisterUnitEvent(h7,p8[a],EVENT_UNIT_SPELL_EFFECT)
call TriggerRegisterUnitEvent(j7,p8[a],EVENT_UNIT_SPELL_EFFECT)
call TriggerRegisterUnitEvent(k7,p8[a],EVENT_UNIT_SPELL_EFFECT)
endif
set a=a+1
endloop
set p=null
call TriggerExecute(U4)
set n8=n8+1
set k8=90
call MultiboardSetTitleText(Y7,"Wave "+I2S(n8)+" starts in "+I2S(k8)+" seconds")
call MultiboardDisplay(Y7,true)
call MultiboardMinimize(Y7,false)
call TimerStart(c8,1,true,function ue)
call DisableTrigger(T4)
call DisableTrigger(S4)
endfunction
function hf takes nothing returns nothing
local integer o9
local integer i=0
local multiboarditem t9
loop
exitwhen i>$A
if v8[i]then
set o9=GetPlayerState(Player(i),PLAYER_STATE_RESOURCE_GOLD)
set t9=MultiboardGetItem(Y7,w8[i],2)
call MultiboardSetItemValue(t9,I2S(o9))
call MultiboardReleaseItem(t9)
endif
set i=i+1
endloop
set t9=null
endfunction
function jf takes nothing returns nothing
local integer n=0
local integer a=0
local integer ff=1
local integer kf=0
local real L0=$D
local real L1=5
local real L2=5
local multiboarditem t9
set Y7=CreateMultiboard()
call MultiboardDisplay(Y7,false)
call MultiboardSetColumnCount(Y7,5)
call MultiboardSetTitleTextColor(Y7,$FF,$FF,$FF,$FF)
loop
exitwhen a>$A
if v8[a]then
set n=n+1
set w8[a]=n
set kf=StringLength(GetPlayerName(Player(a)))+3
call MultiboardSetRowCount(Y7,n+1)
set t9=MultiboardGetItem(Y7,n,0)
call MultiboardSetItemValue(t9,Ae(a))
call MultiboardReleaseItem(t9)
if L0<kf then
set L0=kf
endif
endif
set a=a+1
endloop
set L0=L0*.005
set kf=StringLength(I2S(O7))
if L1<kf then
set L1=kf
endif
set L1=L1*.005
set kf=StringLength(I2S(P7))
if L2<kf then
set L2=kf
endif
set L2=L2*.005
set t9=MultiboardGetItem(Y7,0,0)
call MultiboardSetItemValue(t9,"Status/Player")
call MultiboardSetItemValueColor(t9,$FF,$CC,$80,$FF)
call MultiboardSetItemWidth(t9,L0)
call MultiboardReleaseItem(t9)
set t9=MultiboardGetItem(Y7,0,1)
call MultiboardSetItemValue(t9,"Lives")
call MultiboardSetItemValueColor(t9,$FF,$CC,$80,$FF)
call MultiboardSetItemWidth(t9,L1)
call MultiboardReleaseItem(t9)
set t9=MultiboardGetItem(Y7,0,2)
call MultiboardSetItemValue(t9,"Gold")
call MultiboardSetItemValueColor(t9,$FF,$CC,$80,$FF)
call MultiboardSetItemWidth(t9,L2)
call MultiboardReleaseItem(t9)
set t9=MultiboardGetItem(Y7,0,3)
call MultiboardSetItemValue(t9,"Income")
call MultiboardSetItemValueColor(t9,$FF,$CC,$80,$FF)
call MultiboardSetItemWidth(t9,.035)
call MultiboardReleaseItem(t9)
set t9=MultiboardGetItem(Y7,0,4)
call MultiboardSetItemValue(t9,"Kills")
call MultiboardSetItemValueColor(t9,$FF,$CC,$80,$FF)
call MultiboardSetItemWidth(t9,.025)
call MultiboardReleaseItem(t9)
call MultiboardSetItemsStyle(Y7,true,false)
call MultiboardSetItemsIcon(Y7,"Icons\\PASPreparation.blp")
loop
exitwhen ff>n
set a=0
loop
exitwhen a>4
set t9=MultiboardGetItem(Y7,ff,a)
if(a==0)then
call MultiboardSetItemStyle(t9,true,true)
call MultiboardSetItemWidth(t9,L0)
elseif(a==1)then
call MultiboardSetItemValue(t9,I2S(O7))
call MultiboardSetItemValueColor(t9,$CC,$80,$80,$FF)
call MultiboardSetItemWidth(t9,L1)
elseif(a==2)then
call MultiboardSetItemValue(t9,I2S(P7))
call MultiboardSetItemValueColor(t9,$FF,$FF,0,$FF)
call MultiboardSetItemWidth(t9,L2)
elseif(a==3)then
call MultiboardSetItemValue(t9,"0")
call MultiboardSetItemValueColor(t9,$FF,$80,0,$FF)
call MultiboardSetItemWidth(t9,.035)
elseif(a==4)then
call MultiboardSetItemValue(t9,"0")
call MultiboardSetItemValueColor(t9,$FF,51,51,$FF)
call MultiboardSetItemWidth(t9,.025)
endif
call MultiboardReleaseItem(t9)
set a=a+1
endloop
set ff=ff+1
endloop
set t9=null
call TimerStart(CreateTimer(),.25,true,function hf)
endfunction
function nf takes unit t,unit m returns nothing
local integer i=(LoadInteger(i8,GetHandleId((m)),1333227088))
local integer G9=LoadInteger(i8,GetUnitTypeId(m),1096968556)
local integer tH=GetHandleId(t)
local integer tT=GetUnitTypeId(t)
local integer of=LoadInteger(i8,tH,1131574321)
local integer pf=LoadInteger(i8,tH,1131574322)
local real Zd=A9(t)
local integer qf=LoadInteger(i8,tH,1148020564)
local real rf=z9(t)
local real sf=Zd*LoadReal(i8,tT,1097811268)
local real hp=GetUnitState(m,UNIT_STATE_LIFE)
local real x=GetUnitX(m)
local real y=GetUnitY(m)
local real tf=hp-Zd
local integer n=0
local unit m0
call SaveInteger(i8,tH,1131574321,of+1)
call SaveInteger(i8,tH,1131574322,pf+1)
if(GetUnitAbilityLevel((t),(1093677125))>0)then
call Pd(m)
elseif(GetUnitAbilityLevel((t),(1093677126))>0)then
call Rd(m)
elseif(GetUnitAbilityLevel((t),(1093677131))>0)and(tf>=2)then
call E9(t,1685417264,1093677139,"acidbomb",2,0,0,m)
elseif(GetUnitAbilityLevel((t),(1093677389))>0)and(of==4)then
call SaveInteger(i8,tH,1131574321,1)
call E9(t,1685417265,1093677384,"rainoffire",1,x,y,null)
elseif(GetUnitAbilityLevel((t),(1093677391))>0)and(of==4)then
call SaveInteger(i8,tH,1131574321,1)
call E9(t,1685417265,1093677378,"rainoffire",1,x,y,null)
elseif(GetUnitAbilityLevel((t),(1093677393))>0)and(of==4)then
call SaveInteger(i8,tH,1131574321,1)
call E9(t,1685417265,1093677396,"rainoffire",1,x,y,null)
elseif(GetUnitAbilityLevel((t),(1093677619))>0)and(of==3)then
call SaveInteger(i8,tH,1131574321,1)
call c9(t,2003985456,x,y)
elseif(GetUnitAbilityLevel((t),(1093677621))>0)and(of==3)then
call SaveInteger(i8,tH,1131574321,1)
call c9(t,2003985457,x,y)
elseif(GetUnitAbilityLevel((t),(1093677623))>0)and(of==3)then
call SaveInteger(i8,tH,1131574321,1)
call c9(t,2003985458,x,y)
endif
if(GetUnitAbilityLevel((t),(1093677127))>0)then
call ed(t,m,1,1348825699)
elseif(GetUnitAbilityLevel((t),(1093677137))>0)and(of==4)then
call SaveInteger(i8,tH,1131574321,1)
set Zd=Zd+4
elseif(GetUnitAbilityLevel((t),(1093677136))>0)and(of==$A)then
call SaveInteger(i8,tH,1131574321,1)
call Td(m,1)
elseif(GetUnitAbilityLevel((t),(1093677363))>0)and(pf==$A)then
if(G9>0)and(tf>=2)then
call SaveInteger(i8,tH,1131574322,1)
call E9(t,1685417266,1093677106,"chainlightning",2,0,0,m)
else
call SaveInteger(i8,tH,1131574322,$A)
endif
elseif(GetUnitAbilityLevel((t),(1093677365))>0)and(pf==$A)then
if(tf>=2)then
call SaveInteger(i8,tH,1131574322,1)
call E9(t,1685417266,1093677107,"chainlightning",2,0,0,m)
else
call SaveInteger(i8,tH,1131574322,$A)
endif
elseif(GetUnitAbilityLevel((t),(1093677379))>0)and(pf==$A)then
if(tf>=2)then
call SaveInteger(i8,tH,1131574322,1)
call E9(t,1685417266,1093677108,"chainlightning",2,0,0,m)
else
call SaveInteger(i8,tH,1131574322,$A)
endif
endif
if(rf>0)then
loop
exitwhen n>FN[i]
set m0=E8[(i)*(50)+n]
if(i9(m,m0)<=rf)then
if(m0==m)then
call ed(t,m0,Zd,qf)
else
call ed(t,m0,sf,qf)
endif
endif
set n=n+1
endloop
else
call ed(t,m,Zd,qf)
endif
set m0=null
endfunction
function uf takes unit d,unit m,real Zd returns nothing
local integer dH=GetHandleId(d)
local unit t=LoadUnitHandle(i8,dH,0)
local integer tH=GetHandleId(t)
local integer G9=LoadInteger(i8,GetUnitTypeId(m),1096968556)
local real b=1
if(GetUnitAbilityLevel((t),(1093677385))>0)then
set b=2
elseif(GetUnitAbilityLevel((t),(1093677399))>0)then
set b=1+LoadInteger(i8,tH,1265200236)*.02
endif
if(GetUnitAbilityLevel((d),(1093677139))>0)then
call ed(t,m,3,1348825699)
elseif(GetUnitAbilityLevel((d),(1093677384))>0)then
call ed(t,m,2*b,1298229091)
elseif(GetUnitAbilityLevel((d),(1093677378))>0)then
call ed(t,m,2*b,1298229091)
elseif(GetUnitAbilityLevel((d),(1093677396))>0)and(Zd==.02)then
call ed(t,m,2*b,1298229091)
elseif(GetUnitAbilityLevel((d),(1093677396))>0)and(Zd==.01)then
call ed(t,m,1*b,1298229091)
elseif(GetUnitAbilityLevel((d),(1093677106))>0)and(G9>0)then
call Vd(m)
elseif(GetUnitAbilityLevel((d),(1093677107))>0)then
call ed(t,m,6*b,1298229091)
elseif(GetUnitAbilityLevel((d),(1093677108))>0)then
call Td(m,1)
endif
set t=null
endfunction
function vf takes nothing returns nothing
local unit wf=GetEventDamageSource()
local integer xf=GetUnitTypeId(wf)
local integer i=(GetPlayerId(GetOwningPlayer((wf))))
local unit m=GetTriggerUnit()
local integer T=GetUnitTypeId(m)
local real hp=GetUnitState(m,UNIT_STATE_LIFE)
local real Zd=GetEventDamage()
local integer qf=LoadInteger(i8,q8[i],1148020564)
if(xf!=1651270770)then
call SetUnitState(m,UNIT_STATE_LIFE,hp+Zd)
endif
if(xf==1651270770)and(hp-Zd<2)then
if(T==1865429044)then
call Yd(m,Zd)
elseif(T==1697656884)then
call je(m,Zd)
endif
elseif IsUnitType(wf,UNIT_TYPE_STRUCTURE)then
call nf(wf,m)
elseif IsUnitType(wf,UNIT_TYPE_ANCIENT)then
call uf(wf,m,Zd)
elseif(xf==1865429045)then
call ge(m)
endif
set wf=null
set m=null
endfunction
function yf takes nothing returns boolean
return GetEventDamage()>0
endfunction
function Af takes nothing returns nothing
local integer a=0
local integer array af
local integer n=-1
local integer u=-1
local integer array Bf
local integer s=-1
local integer array bf
local integer f=-1
local integer i=0
local integer r=0
loop
exitwhen a>$A
if v8[a]then
set n=n+1
set af[n]=a
endif
set a=a+1
endloop
if(n==0)or(V7==3)then
loop
exitwhen u==n
set u=u+1
set r8[af[u]]=af[u]
endloop
return
endif
if(V7==0)then
loop
exitwhen u==n
set u=u+1
if(u!=n)then
set r8[af[u]]=af[u+1]
else
set r8[af[n]]=af[0]
endif
endloop
return
endif
loop
exitwhen s==n
set s=s+1
set Bf[s]=af[s]
endloop
if(V7==2)then
loop
exitwhen u==n
set u=u+1
set r=GetRandomInt(0,s)
set r8[af[u]]=Bf[r]
set Bf[r]=Bf[s]
set s=s-1
endloop
return
endif
loop
exitwhen s==1
set u=u+1
set f=-1
loop
exitwhen f==s
set f=f+1
set bf[f]=Bf[f]
endloop
set i=0
loop
exitwhen(bf[i]==af[u])or(i>f)
set i=i+1
endloop
if(i<=f)then
set bf[i]=bf[f]
set f=f-1
endif
set r=GetRandomInt(0,f)
set r8[af[u]]=bf[r]
set i=0
loop
exitwhen Bf[i]==bf[r]
set i=i+1
endloop
set Bf[i]=Bf[s]
set s=s-1
endloop
set u=u+1
set i=0
loop
exitwhen(af[u]==Bf[i])or(i==2)
set i=i+1
endloop
if(i==0)then
set r8[af[u]]=Bf[1]
set r8[af[u+1]]=Bf[0]
return
elseif(i==1)then
set r8[af[u]]=Bf[0]
set r8[af[u+1]]=Bf[1]
return
endif
set i=0
loop
exitwhen(af[u+1]==Bf[i])or(i==2)
set i=i+1
endloop
if(i==0)then
set r8[af[u]]=Bf[0]
set r8[af[u+1]]=Bf[1]
return
elseif(i==1)then
set r8[af[u]]=Bf[1]
set r8[af[u+1]]=Bf[0]
return
endif
set r=GetRandomInt(0,1)
set r8[af[u]]=Bf[r]
if(r==0)then
set r=1
else
set r=0
endif
set r8[af[u+1]]=Bf[r]
endfunction
function cf takes nothing returns nothing
local unit t=GetKillingUnit()
local integer i=(GetPlayerId(GetOwningPlayer((t))))
local unit m=GetDyingUnit()
local integer mH=GetHandleId(m)
local integer T=GetUnitTypeId(m)
local integer b9=LoadInteger(i8,T,1165520967)
local unit Df=LoadUnitHandle(i8,mH,q8[i])
local integer Ef=GetHandleId(Df)
local integer Ff=LoadInteger(i8,Ef,1265200236)+1
local multiboarditem t9=MultiboardGetItem(Y7,w8[i],4)
local string s=""
local integer a=0
call SaveInteger(i8,Ef,1265200236,Ff)
set t8[i]=t8[i]+1
call MultiboardSetItemValue(t9,I2S(t8[i]))
call MultiboardReleaseItem(t9)
call r9(i,1)
if(b9>0)then
loop
exitwhen a>TN[i]
set t=H8[(i)*(56)+a]
set T=GetUnitTypeId(t)
if(t!=Df)and(T!=1953462064)and(i9(t,m)<=384)then
call u9("|cFF80CCFF+"+I2S(b9)+"|r",t,8.5,64,i,2,1)
call B9(t,b9)
endif
set a=a+1
endloop
call B9(Df,b9)
set s="|cFF80CCFF+"+I2S(b9)+"|r
"
endif
call u9(s+"|cFFFF3333+1|r",Df,8.5,64,i,2,1)
call oe(m)
set t=null
set m=null
set Df=null
set t9=null
endfunction
function Hf takes nothing returns nothing
local integer i=GetPlayerId(GetTriggerPlayer())
if v8[i]then
call Be(i)
endif
endfunction
function lf takes nothing returns nothing
set k8=k8-1
set Z7="Game will end in "+I2S(k8)+" second"
if k8!=1 then
set Z7=Z7+"s"
endif
if k8==0 then
call MultiboardSetTitleText(Y7,"Game is ending...")
call EndGame(true)
else
call MultiboardSetTitleText(Y7,Z7)
endif
endfunction
function Jf takes nothing returns nothing
local integer i=0
set N7=false
set k8=8
call ClearTextMessages()
loop
if v8[i]then
call Be(i)
endif
call DisplayTimedTextToPlayer(Player(i),.0,.0,7.,"Game will end in 7 seconds.")
set i=i+1
exitwhen i==$B
endloop
call lf()
call TimerStart(c8,1.,true,function lf)
endfunction
function Kf takes nothing returns nothing
local integer i=D8
local integer H=q8[i]
local integer a=0
local boolean Lf=true
local texttag y9
local effect e
local multiboarditem t9
set o8=o8-1
set v8[i]=false
call ae()
call FlushChildHashtable(i8,i)
call FlushChildHashtable(i8,H)
call RemoveUnit(p8[i])
set p8[i]=null
loop
if a<=FN[i]then
set H=GetHandleId(E8[(i)*(50)+a])
set y9=LoadTextTagHandle(i8,H,1415936116)
if(y9!=null)then
call DestroyTextTag(y9)
endif
call ne(H)
call FlushChildHashtable(i8,H)
call RemoveUnit(E8[(i)*(50)+a])
set E8[(i)*(50)+a]=null
if Lf then
set Lf=false
endif
endif
if a<=LN[i]then
set F8[(i)*(50)+a]=null
if Lf then
set Lf=false
endif
endif
if a<=55 then
call RemoveUnit(G8[(i)*(56)+a])
set G8[(i)*(56)+a]=null
if Lf then
set Lf=false
endif
endif
if a<=TN[i]then
set H=GetHandleId(H8[(i)*(56)+a])
set e=LoadEffectHandle(i8,H,1281717868)
if e!=null then
call DestroyEffect(e)
endif
call qe(H)
call ke(H)
call FlushChildHashtable(i8,H)
call RemoveUnit(H8[(i)*(56)+a])
set H8[(i)*(56)+a]=null
if Lf then
set Lf=false
endif
endif
if a<=MultiboardGetColumnCount(Y7)-1 then
set t9=MultiboardGetItem(Y7,w8[i],a)
call MultiboardSetItemValueColor(t9,51,51,51,$80)
call MultiboardReleaseItem(t9)
if Lf then
set Lf=false
endif
endif
set a=a+1
exitwhen Lf
set Lf=true
endloop
set t9=MultiboardGetItem(Y7,w8[i],0)
call MultiboardSetItemStyle(t9,true,false)
call MultiboardReleaseItem(t9)
set y9=null
set e=null
set t9=null
if o8<=1 and N7 then
call Jf()
endif
endfunction
function Nf takes unit m,integer i returns nothing
local integer Of=r8[i]
local integer H=GetHandleId(m)
local integer T=GetUnitTypeId(m)
local integer G9=LoadInteger(i8,T,1096968556)
local real x1=GetRectCenterX(y8[i])
local real y1=GetRectCenterY(y8[i])
local real x2=GetRectCenterX(A8[Of])
local real y2=GetRectCenterY(A8[Of])
local real x=GetUnitX(m)
local real y=GetUnitY(m)
local timer Pf=null
local integer Qf=0
call SetUnitX(m,x-x1+x2)
call SetUnitY(m,y-y1+y2)
call SetUnitFacing(m,270)
call SetUnitOwner(m,Player($B),false)
call Dd(m,Of)
set x=rX(x8[Of])
set y=GetRectMinY(x8[Of])
call UnitAddAbility(m,G9)
call SaveInteger(i8,H,1333227088,i)
call SaveInteger(i8,H,1181705552,Of)
if(T==1865429045)then
call SaveInteger(i8,H,1097623137,$80)
else
call SaveInteger(i8,H,1097623137,$FF)
endif
call SaveReal(i8,H,1131570264,x)
call SaveReal(i8,H,1131570265,y)
call TriggerRegisterUnitEvent(V4,m,EVENT_UNIT_DAMAGED)
call cd(m)
set Pf=LoadTimerHandle(i8,H,1651861094)
call TimerStart(Pf,.02,true,function Kd)
if(T==1865429041)then
set Pf=LoadTimerHandle(i8,H,1865429041)
call TimerStart(Pf,.02,true,function Wd)
elseif(T==1697656881)then
set Pf=LoadTimerHandle(i8,H,1697656881)
call TimerStart(Pf,.25,true,function he)
endif
call U9(m)
call IssuePointOrderById(m,$D0012,x,y)
set Pf=null
endfunction
function Rf takes nothing returns nothing
local integer i=0
local integer a=0
local boolean b=true
call TriggerExecute(W4)
loop
exitwhen i>$A
if v8[i]then
set a=0
loop
exitwhen a>FN[i]
call Nf(E8[(i)*(50)+a],i)
set a=a+1
endloop
call ve(i,false)
if LoadBoolean(i8,i,1198680420)then
call xe(i,false)
endif
if b and(FN[i]>-1)then
set b=false
endif
endif
set i=i+1
endloop
if b then
set n8=n8-1
call TriggerExecute(e7)
endif
endfunction
function Tf takes nothing returns nothing
local integer a=0
local boolean b
loop
exitwhen a>$A
if v8[a]then
call n9(a,s8[a],true)
call j9(a,F4,5,"You receive |cFFFFFF00"+I2S(s8[a])+"|r gold for surviving this wave.")
call r9(a,-s8[a])
set b=LoadBoolean(i8,a,1198680420)
call FlushChildHashtable(i8,a)
if b then
call SaveBoolean(i8,a,1198680420,true)
call xe(a,true)
endif
call ve(a,true)
endif
set a=a+1
endloop
set n8=n8+1
set k8=90
call MultiboardSetItemsIcon(Y7,"Icons\\PASPreparation.blp")
call MultiboardSetTitleText(Y7,"Wave "+I2S(n8)+" starts in "+I2S(k8)+" seconds")
call TimerStart(c8,1,true,function ue)
endfunction
function Uf takes nothing returns nothing
call TimerStart(c8,1.25,false,function Tf)
endfunction
function Wf takes nothing returns nothing
local unit b=GetOrderedUnit()
local real x0=GetOrderPointX()
local real y0=GetOrderPointY()
call SetUnitX(b,x0)
call SetUnitY(b,y0)
set b=null
endfunction
function Xf takes nothing returns boolean
local integer Cd=GetIssuedOrderId()
local integer D9=0
local integer n=0
loop
set D9=m8[n]
exitwhen(D9==Cd)or(n>TT)
set n=n+1
endloop
return n<=TT
endfunction
function Zf takes nothing returns nothing
local unit u=GetSpellAbilityUnit()
local integer T=GetSpellAbilityId()
local integer r=GetUnitUserData(u)-1
call UnitRemoveAbility(u,T)
call SetUnitUserData(u,r)
if(T==1093677875)then
call UnitAddAbility(u,1093677141)
elseif(T==1093677876)then
call UnitAddAbility(u,1093677366)
elseif(T==1093677877)then
call UnitAddAbility(u,1093677381)
elseif(T==1093677878)then
call UnitAddAbility(u,1093677367)
endif
if(r==0)then
call UnitRemoveAbility(u,1093677875)
call UnitRemoveAbility(u,1093677876)
call UnitRemoveAbility(u,1093677877)
call UnitRemoveAbility(u,1093677878)
endif
set u=null
endfunction
function dg takes nothing returns boolean
local integer T=GetSpellAbilityId()
local boolean b=(T==1093677875)or(T==1093677876)or(T==1093677877)or(T==1093677878)
return b
endfunction
function fg takes nothing returns nothing
local integer i=(GetPlayerId(GetOwningPlayer((GetSpellAbilityUnit()))))
local player p=Player(i)
local integer a=GetSpellAbilityId()
local integer o9=GetPlayerState(p,PLAYER_STATE_RESOURCE_GOLD)
local integer gg=GetPlayerState(p,PLAYER_STATE_RESOURCE_FOOD_USED)+1
local unit m
local integer Jd
local integer hg
local integer T
local integer c=0
loop
set T=LoadInteger(i8,j8[c],1399879534)
exitwhen a==T
set c=c+1
endloop
set T=j8[c]
set c=LoadInteger(i8,T,1131377524)
set Jd=LoadInteger(i8,i,T)+1
set hg=LoadInteger(i8,T,1281977716)
if(gg>40)then
call j9(i,D4,5,"|cffcc3333Error|r: you cannot spawn more minions.")
set p=null
return
endif
if(o9-c>=0)then
set m=CreateUnit(p,T,rX(y8[i]),rY(y8[i]),270)
set FN[i]=FN[i]+1
set E8[(i)*(50)+FN[i]]=m
call SaveInteger(i8,GetHandleId(m),1179535736,FN[i])
call N9(m,100)
call SetUnitInvulnerable(m,true)
call UnitAddAbility(m,1093677368)
call SetUnitMoveSpeed(m,512)
call SaveInteger(i8,i,T,Jd)
call n9(i,-c,false)
set c=LoadInteger(i8,T,1197566318)
call r9(i,c)
if(Jd==hg)then
call SetPlayerAbilityAvailable(p,a,false)
endif
set m=null
else
call j9(i,E4,5,"|cffcc3333Warning|r: not enough gold.")
endif
set p=null
endfunction
function ig takes nothing returns boolean
local integer a=GetSpellAbilityId()
local integer jg
local integer n=0
loop
set jg=LoadInteger(i8,j8[n],1399879534)
exitwhen(jg==a)or(n>MT)
set n=n+1
endloop
return n<=MT
endfunction
function mg takes nothing returns nothing
local unit m=GetSpellAbilityUnit()
local integer T=GetUnitTypeId(m)
local integer H=GetHandleId(m)
local integer c=LoadInteger(i8,T,1131377524)
local integer jg=LoadInteger(i8,T,1399879534)
local integer i=(GetPlayerId(GetOwningPlayer((m))))
local integer a=LoadInteger(i8,H,1179535736)
local integer Jd=LoadInteger(i8,i,T)
local effect e=AddSpecialEffect("Abilities\\Spells\\Other\\Transmute\\PileofGold.mdx",GetUnitX(m),GetUnitY(m))
call DestroyEffect(e)
call FlushChildHashtable(i8,H)
call me(i,a)
call SaveInteger(i8,i,T,Jd-1)
call SetPlayerAbilityAvailable(Player(i),jg,true)
call n9(i,c,false)
call u9("|cFFFFFF00+"+I2S(c)+"|r",m,$A,64,i,2,1)
set c=LoadInteger(i8,T,1197566318)
call r9(i,-c)
call RemoveUnit(m)
set e=null
set m=null
endfunction
function ng takes nothing returns boolean
return GetSpellAbilityId()==1093677368
endfunction
function pg takes nothing returns nothing
local integer i=(GetPlayerId(GetOwningPlayer((GetSpellAbilityUnit()))))
local boolean b=not LoadBoolean(i8,i,1198680420)
call xe(i,b)
call SaveBoolean(i8,i,1198680420,b)
endfunction
function qg takes nothing returns boolean
return GetSpellAbilityId()==1093677113
endfunction
function sg takes nothing returns nothing
local integer i=(GetPlayerId(GetOwningPlayer((GetSpellAbilityUnit()))))
local multiboarditem t9=MultiboardGetItem(Y7,w8[i],0)
call SetPlayerAbilityAvailable(Player(i),1093677110,false)
call SaveBoolean(i8,i,1382375780,true)
call MultiboardSetItemIcon(t9,"Icons\\PASReadiness.blp")
call MultiboardReleaseItem(t9)
set t9=null
call ae()
endfunction
function tg takes nothing returns boolean
return GetSpellAbilityId()==1093677110
endfunction
function vg takes integer i,integer wg returns nothing
local multiboarditem t9=MultiboardGetItem(Y7,w8[i],1)
set u8[i]=u8[i]+wg
if(u8[i]<0)then
set u8[i]=0
endif
call MultiboardSetItemValue(t9,I2S(u8[i]))
call MultiboardReleaseItem(t9)
set t9=null
endfunction
function xg takes nothing returns nothing
local unit m=GetEnteringUnit()
local integer T=GetUnitTypeId(m)
local integer H=GetHandleId(m)
local integer i=LoadInteger(i8,H,1333227088)
local integer f=LoadInteger(i8,H,1181705552)
local real x=GetUnitX(m)
local real y=GetUnitY(m)
local effect e=AddSpecialEffect("Abilities\\Spells\\Undead\\DarkRitual\\DarkRitualTarget.mdx",x,y)
call DestroyEffect(e)
if v8[f]then
call j9(f,H4,3,"|cffcc3333Warning|r: you have lost a life.")
call vg(f,-1)
endif
call oe(m)
call RemoveUnit(m)
set m=null
set e=null
if(u8[f]==0)then
call Be(f)
endif
endfunction
function yg takes nothing returns boolean
return GetOwningPlayer(GetEnteringUnit())==Player($B)
endfunction
function Ag takes nothing returns nothing
call SetUnitInvulnerable(GetLeavingUnit(),false)
endfunction
function ag takes nothing returns boolean
return GetOwningPlayer(GetLeavingUnit())==Player($B)
endfunction
function bg takes nothing returns nothing
local unit m=GetTriggerUnit()
local integer mH=GetHandleId(m)
local integer T=GetUnitTypeId(m)
local integer i=LoadInteger(i8,mH,1333227088)
local string Cd=LoadStr(i8,T,1330869362)
local timer Cg=LoadTimerHandle(i8,mH,1953063787)
local real cg=M9(m)
local unit t=GetAttacker()
local integer tH=GetHandleId(t)
local integer qf=LoadInteger(i8,tH,1148020564)
local boolean xd=(qf==1349024115)and(W9(m)!=1)
local boolean yd=(qf==1298229091)and(Y9(m)!=1)
local boolean zd=(qf==1348825699)
local boolean Dg=(T==1747988529)and(qf==1349024115)
local boolean Eg=(T==1747988534)and(qf==1298229091)
local boolean b=(xd or yd or zd)and not Z9(mH)
if b then
call TimerStart(Cg,.5,false,null)
set b=(Dg or Eg)
if b then
set b=IssueImmediateOrder(m,Cd)
endif
if not b then
set b=bd(i,qf,m)
endif
if not b and(cg<67)then
set b=bd(i,1214603628,m)
endif
if not b then
set b=bd(i,1416591218,t)
endif
endif
set m=null
set t=null
set Cg=null
endfunction
function Fg takes nothing returns boolean
return TimerGetRemaining(LoadTimerHandle(i8,GetHandleId(GetTriggerUnit()),1953063787))<=.0
endfunction
function Hg takes nothing returns nothing
local unit m=GetSpellAbilityUnit()
local integer mH=GetHandleId(m)
local real x=LoadReal(i8,mH,1131570264)
local real y=LoadReal(i8,mH,1131570265)
call IssuePointOrderById(m,$D0012,x,y)
set m=null
endfunction
function lg takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(i8,tH,0)
local integer mH=GetHandleId(m)
call RemoveSavedBoolean(i8,mH,1093677146)
call SetUnitAnimation(m,"walk")
set t=null
set m=null
endfunction
function Jg takes nothing returns nothing
local unit m=GetSpellAbilityUnit()
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(i8,mH,1093677146)
local integer tH=GetHandleId(t)
call SaveBoolean(i8,mH,1093677146,true)
call TimerStart(t,2,false,function lg)
set m=null
set t=null
endfunction
function Kg takes nothing returns boolean
return GetSpellAbilityId()==1093677146
endfunction
function Mg takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(i8,tH,0)
local integer mH=GetHandleId(m)
local effect e=LoadEffectHandle(i8,tH,1)
call RemoveSavedBoolean(i8,mH,1093677648)
call RemoveSavedHandle(i8,tH,1)
call DestroyEffect(e)
set t=null
set m=null
set e=null
endfunction
function Ng takes nothing returns nothing
local unit m=GetSpellTargetUnit()
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(i8,mH,1093677648)
local integer tH=GetHandleId(t)
local effect e
if not HaveSavedHandle(i8,tH,1)then
call SaveBoolean(i8,mH,1093677648,true)
set e=AddSpecialEffectTarget("Abilities\\Spells\\Human\\InnerFire\\InnerFireTarget.mdx",m,"overhead")
call SaveEffectHandle(i8,tH,1,e)
set e=null
endif
call TimerStart(t,2,false,function Mg)
set m=null
set t=null
endfunction
function Og takes nothing returns boolean
return GetSpellAbilityId()==1093677648
endfunction
function Qg takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(i8,tH,0)
local integer mH=GetHandleId(m)
call UnitRemoveAbility(m,1097295983)
set t=null
set m=null
endfunction
function Rg takes nothing returns nothing
local unit m=GetSpellTargetUnit()
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(i8,mH,1093677145)
local integer tH=GetHandleId(t)
if not(GetUnitAbilityLevel((m),(1097295983))>0)then
call UnitAddAbility(m,1097295983)
endif
call TimerStart(t,2,false,function Qg)
set m=null
set t=null
endfunction
function Sg takes nothing returns boolean
return GetSpellAbilityId()==1093677145
endfunction
function Ug takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(i8,tH,0)
local integer mH=GetHandleId(m)
call RemoveSavedBoolean(i8,mH,1093677128)
call SetUnitAnimation(m,"walk")
set t=null
set m=null
endfunction
function Vg takes nothing returns nothing
local unit m=GetSpellAbilityUnit()
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(i8,mH,1093677128)
local integer tH=GetHandleId(t)
call SaveBoolean(i8,mH,1093677128,true)
call TimerStart(t,2,false,function Ug)
set m=null
set t=null
endfunction
function Wg takes nothing returns boolean
return GetSpellAbilityId()==1093677128
endfunction
function Yg takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(i8,tH,0)
local integer mH=GetHandleId(m)
local effect e=LoadEffectHandle(i8,tH,1)
call DestroyEffect(e)
set e=LoadEffectHandle(i8,tH,2)
call DestroyEffect(e)
call RemoveSavedInteger(i8,mH,1093677140)
call RemoveSavedReal(i8,mH,1093677140)
call SetUnitScale(m,.7,.7,.7)
call RemoveSavedHandle(i8,tH,1)
call RemoveSavedHandle(i8,tH,2)
set t=null
set m=null
set e=null
endfunction
function Zg takes nothing returns nothing
local unit m=GetSpellTargetUnit()
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(i8,mH,1093677140)
local integer tH=GetHandleId(t)
local effect e
if not HaveSavedHandle(i8,tH,1)then
call SaveInteger(i8,mH,1093677140,30)
call SaveReal(i8,mH,1093677140,25)
call SetUnitScale(m,.9,.9,.9)
set e=AddSpecialEffectTarget("Abilities\\Spells\\Orc\\Bloodlust\\BloodlustSpecial.mdx",m,"hand left")
call SaveEffectHandle(i8,tH,1,e)
set e=AddSpecialEffectTarget("Abilities\\Spells\\Orc\\Bloodlust\\BloodlustSpecial.mdx",m,"hand right")
call SaveEffectHandle(i8,tH,2,e)
set e=null
endif
call TimerStart(t,2,false,function Yg)
set m=null
set t=null
endfunction
function dh takes nothing returns boolean
return GetSpellAbilityId()==1093677140
endfunction
function fh takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit D9=LoadUnitHandle(i8,tH,0)
local integer H=GetHandleId(D9)
local effect e=LoadEffectHandle(i8,tH,1)
call RemoveSavedBoolean(i8,H,1093677656)
call RemoveSavedHandle(i8,H,1093677656)
call PauseUnit(D9,false)
call FlushChildHashtable(i8,tH)
call DestroyEffect(e)
call DestroyTimer(t)
set t=null
set D9=null
set e=null
endfunction
function gh takes nothing returns nothing
local unit m=GetSpellAbilityUnit()
local integer n=GetHandleId(m)
local integer i=LoadInteger(i8,n,1181705552)
local unit D9
local integer T
local timer t
local effect e
local integer tH
local integer H
set n=0
loop
exitwhen n>TN[i]
set D9=H8[(i)*(56)+n]
set T=GetUnitTypeId(D9)
if(i9(m,D9)<=$C0)and(T!=1953462064)then
set H=GetHandleId(D9)
set t=LoadTimerHandle(i8,H,1093677656)
if(t==null)then
set t=CreateTimer()
set tH=GetHandleId(t)
call PauseUnit(D9,true)
call SaveBoolean(i8,H,1093677656,true)
call SaveTimerHandle(i8,H,1093677656,t)
call SaveUnitHandle(i8,tH,0,D9)
set e=AddSpecialEffectTarget("Abilities\\Spells\\Human\\Thunderclap\\ThunderclapTarget.mdx",D9,"overhead")
call SaveEffectHandle(i8,tH,1,e)
endif
call TimerStart(t,2,false,function fh)
endif
set n=n+1
endloop
set m=null
set D9=null
set t=null
set e=null
endfunction
function hh takes nothing returns boolean
return GetSpellAbilityId()==1093677656
endfunction
function jh takes nothing returns nothing
local unit m=GetDyingUnit()
local integer i=(LoadInteger(i8,GetHandleId((m)),1333227088))
local integer a=0
local unit c
local integer T
local effect e
loop
exitwhen a>FN[i]
set c=E8[(i)*(50)+a]
set T=GetUnitTypeId(c)
if(i9(m,c)<=512)and(M9(c)<100)and(T==1966092337 or T==1966092342)then
set e=AddSpecialEffectTarget("Abilities\\Spells\\Undead\\VampiricAura\\VampiricAuraTarget.mdx",c,"origin")
call R9(c,20)
call DestroyEffect(e)
endif
set a=a+1
endloop
set e=null
set m=null
set c=null
endfunction
function kh takes nothing returns boolean
local unit m=GetDyingUnit()
local integer T=GetUnitTypeId(m)
local boolean b=IsUnitType(m,UNIT_TYPE_GROUND)and(T!=1966092336)
set m=null
return b
endfunction
function nh takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(i8,tH,0)
local integer mH=GetHandleId(m)
local effect e=LoadEffectHandle(i8,tH,1)
call RemoveSavedBoolean(i8,mH,1093677658)
call RemoveSavedHandle(i8,tH,1)
call DestroyEffect(e)
set t=null
set m=null
set e=null
endfunction
function oh takes nothing returns nothing
local unit m=GetSpellTargetUnit()
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(i8,mH,1093677658)
local integer tH=GetHandleId(t)
local effect e
if not HaveSavedHandle(i8,tH,1)then
call SaveBoolean(i8,mH,1093677658,true)
set e=AddSpecialEffectTarget("Abilities\\Spells\\Undead\\AntiMagicShell\\AntiMagicShell.mdx",m,"overhead")
call SaveEffectHandle(i8,tH,1,e)
set e=null
endif
call TimerStart(t,2,false,function nh)
set m=null
set t=null
endfunction
function ph takes nothing returns boolean
return GetSpellAbilityId()==1093677658
endfunction
function rh takes real x,real y,real sh,integer i,integer Of returns nothing
local unit m=CreateUnit(Player($B),1966092336,x,y,sh)
local integer H=GetHandleId(m)
local timer Pf=CreateTimer()
local integer Qf=GetHandleId(Pf)
local effect e=AddSpecialEffectTarget("Abilities\\Spells\\Undead\\AnimateDead\\AnimateDeadTarget.mdx",m,"origin")
call DestroyEffect(e)
set FN[i]=FN[i]+1
set E8[(i)*(50)+FN[i]]=m
call SaveInteger(i8,H,1179535736,FN[i])
call N9(m,100)
call Dd(m,Of)
set x=rX(x8[Of])
set y=GetRectMinY(x8[Of])
call SaveInteger(i8,H,1333227088,i)
call SaveInteger(i8,H,1181705552,Of)
call SaveInteger(i8,H,1097623137,$FF)
call TriggerRegisterUnitEvent(V4,m,EVENT_UNIT_DAMAGED)
call SaveUnitHandle(i8,Qf,0,m)
call SaveTimerHandle(i8,H,1651861094,Pf)
call TimerStart(Pf,.02,true,function Kd)
call U9(m)
call IssuePointOrderById(m,$D0012,x,y)
set m=null
set e=null
set Pf=null
endfunction
function th takes nothing returns nothing
local unit m=GetDyingUnit()
local real x=GetUnitX(m)
local real y=GetUnitY(m)
local real sh=GetUnitFacing(m)
local integer H=GetHandleId(m)
local integer uh=LoadInteger(i8,H,1333227088)
local integer Of=LoadInteger(i8,H,1181705552)
local boolean b=bd(uh,1147494756,m)
if b then
call rh(x,y,sh,uh,Of)
call r9(uh,1)
endif
set m=null
endfunction
function vh takes nothing returns boolean
local unit m=GetDyingUnit()
local integer T=GetUnitTypeId(m)
local boolean b=IsUnitType(m,UNIT_TYPE_GROUND)and(T!=1966092336)
set m=null
return b
endfunction
function xh takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(i8,tH,0)
local integer mH=GetHandleId(m)
local effect e=LoadEffectHandle(i8,tH,1)
call RemoveSavedBoolean(i8,mH,1093677382)
call RemoveSavedHandle(i8,tH,1)
call DestroyEffect(e)
set t=null
set m=null
set e=null
endfunction
function yh takes nothing returns nothing
local unit m=GetSpellTargetUnit()
local integer mH=GetHandleId(m)
local timer t=LoadTimerHandle(i8,mH,1093677382)
local integer tH=GetHandleId(t)
local effect e
if not HaveSavedHandle(i8,tH,1)then
call SaveBoolean(i8,mH,1093677382,true)
set e=AddSpecialEffectTarget("Abilities\\Spells\\Undead\\FrostArmor\\FrostArmorTarget.mdx",m,"chest")
call SaveEffectHandle(i8,tH,1,e)
set e=null
endif
call TimerStart(t,2,false,function xh)
set m=null
set t=null
endfunction
function zh takes nothing returns boolean
return GetSpellAbilityId()==1093677382
endfunction
function ah takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(i8,tH,0)
local integer mH=GetHandleId(m)
local effect e=LoadEffectHandle(i8,tH,1)
local integer Bh=LoadInteger(i8,mH,1093677872)
call R9(m,25)
call SaveInteger(i8,mH,1093677872,Bh+1)
if(Bh==2)then
call RemoveSavedInteger(i8,mH,1093677872)
call RemoveSavedHandle(i8,tH,1)
call DestroyEffect(e)
call PauseTimer(t)
endif
set t=null
set m=null
set e=null
endfunction
function bh takes nothing returns nothing
local unit m=GetSpellTargetUnit()
local integer mH=GetHandleId(m)
local integer Bh=LoadInteger(i8,mH,1093677872)
local timer t=LoadTimerHandle(i8,mH,1093677872)
local integer tH=GetHandleId(t)
local effect e
if(Bh==0)then
set e=AddSpecialEffectTarget("Abilities\\Spells\\NightElf\\Rejuvenation\\RejuvenationTarget.mdx",m,"chest")
call SaveEffectHandle(i8,tH,1,e)
set e=null
call TimerStart(t,1,true,function ah)
endif
call R9(m,20)
call SaveInteger(i8,mH,1093677872,1)
set m=null
set t=null
endfunction
function Ch takes nothing returns boolean
return GetSpellAbilityId()==1093677872
endfunction
function Dh takes nothing returns nothing
local timer t=GetExpiredTimer()
local integer tH=GetHandleId(t)
local unit m=LoadUnitHandle(i8,tH,0)
local integer mH=GetHandleId(m)
local effect e=LoadEffectHandle(i8,tH,1)
local integer Bh=LoadInteger(i8,mH,1093677143)
call R9(m,$A)
call SaveInteger(i8,mH,1093677143,Bh+1)
if(Bh==4)then
call RemoveSavedInteger(i8,mH,1093677143)
call RemoveSavedHandle(i8,tH,1)
call DestroyEffect(e)
call PauseTimer(t)
endif
set t=null
set m=null
set e=null
endfunction
function Eh takes nothing returns nothing
local unit c=GetSpellAbilityUnit()
local integer H=GetHandleId(c)
local integer i=LoadInteger(i8,H,1333227088)
local unit m
local timer t
local effect e
local integer tH
local integer Bh
local integer n=0
loop
exitwhen n>FN[i]
set m=E8[(i)*(50)+n]
if(i9(c,m)<=512)then
set H=GetHandleId(m)
set Bh=LoadInteger(i8,H,1093677143)
set t=LoadTimerHandle(i8,H,1093677143)
set tH=GetHandleId(t)
if(Bh==0)then
set e=AddSpecialEffectTarget("Abilities\\Spells\\NightElf\\BattleRoar\\RoarTarget.mdx",m,"overhead")
call SaveEffectHandle(i8,tH,1,e)
call TimerStart(t,1,true,function Dh)
endif
call R9(m,$A)
call SaveInteger(i8,H,1093677143,1)
endif
set n=n+1
endloop
set c=null
set m=null
set t=null
set e=null
endfunction
function Fh takes nothing returns boolean
return GetSpellAbilityId()==1093677143
endfunction
function Hh takes nothing returns nothing
local unit m=GetSpellAbilityUnit()
local integer n=GetHandleId(m)
local integer i=LoadInteger(i8,n,1181705552)
local unit D9
local integer T
set n=0
loop
exitwhen n>TN[i]
set D9=H8[(i)*(56)+n]
set T=GetUnitTypeId(D9)
if(i9(m,D9)<=384)and(T!=1953462064)then
call IssueTargetOrderById(D9,$D000F,m)
endif
set n=n+1
endloop
set m=null
set D9=null
endfunction
function Ih takes nothing returns boolean
return GetSpellAbilityId()==1093677649
endfunction
function Jh takes integer i,real x0,real y0 returns boolean
local real Kh=GetRectMinX(a8[i])
local real Lh=GetRectMaxX(a8[i])
local real x=0
local real y=0
local real d=0
local real array Mh
local real array Nh
local real array Oh
local real array Ph
local integer n=-1
local integer a=0
local integer ns=0
local boolean Qh=false
local boolean Rh=false
loop
exitwhen n==TN[i]
set n=n+1
set Mh[n]=GetUnitX(H8[(i)*(56)+n])
set Nh[n]=GetUnitY(H8[(i)*(56)+n])
endloop
set Oh[0]=x0
set Ph[0]=y0
set i=0
loop
exitwhen(i>ns)
set x0=Oh[i]
set y0=Ph[i]
if(x0==Lh)then
set Qh=true
elseif(x0==Kh)then
set Rh=true
endif
exitwhen(Rh and Qh)
set a=0
loop
exitwhen a>n
set x=Mh[a]
set y=Nh[a]
set d=SquareRoot((x0-x)*(x0-x)+(y0-y)*(y0-y))
if(d<$C0)then
set ns=ns+1
set Oh[ns]=x
set Ph[ns]=y
set Mh[a]=Mh[n]
set Nh[a]=Nh[n]
set n=n-1
else
set a=a+1
endif
endloop
set i=i+1
endloop
return Rh and Qh
endfunction
function Sh takes nothing returns nothing
local unit t=GetConstructingStructure()
local real x=GetUnitX(t)
local real y=GetUnitY(t)
local integer T=GetUnitTypeId(t)
local integer H=GetHandleId(t)
local integer qf=LoadInteger(i8,T,1148020564)
local integer i=(GetPlayerId(GetOwningPlayer((t))))
local real x1=GetRectMinX(a8[i])
local real x2=GetRectMaxX(a8[i])
local real y1=GetRectMinY(a8[i])
local real y2=GetRectMaxY(a8[i])
if not(x>=x1 and x<=x2 and y>=y1 and y<=y2)then
call j9(i,D4,5,"|cffcc3333Error|r: you cannot build there.")
call se(t,false)
call RemoveUnit(t)
set t=null
return
endif
if Jh(i,x,y)then
call j9(i,D4,5,"|cffcc3333Error|r: this tower will block movement.")
call se(t,false)
call RemoveUnit(t)
set t=null
return
endif
if(T!=1953462064)then
call SaveInteger(i8,H,1148020564,qf)
endif
set TN[i]=TN[i]+1
set H8[(i)*(56)+TN[i]]=t
call SaveInteger(i8,GetHandleId(t),1414416760,TN[i])
call SetUnitFacing(t,270)
set t=null
endfunction
function Uh takes nothing returns nothing
local unit t=GetCancelledStructure()
call se(t,true)
call re(t)
set t=null
endfunction
function Wh takes nothing returns nothing
local unit t=GetSpellAbilityUnit()
call se(t,true)
call re(t)
set t=null
endfunction
function Xh takes nothing returns boolean
return GetSpellAbilityId()==1093677104
endfunction
function Zh takes nothing returns nothing
local unit D9=GetSpellAbilityUnit()
local integer H=GetHandleId(D9)
local integer i=(GetPlayerId(GetOwningPlayer((D9))))
local integer qf=LoadInteger(i8,H,1148020564)
local integer b9=LoadInteger(i8,H,1165520961)
local integer Ff=LoadInteger(i8,H,1265200236)
local real Zd=A9(D9)
local string s="Exp.: |cff80ccff"+I2S(b9)+"|r
Kills: |cffff3333"+I2S(Ff)+"|r
Dmg.: |cffff0000"
set s=s+R2SW(Zd,0,2)+"|r
Type: "
if(qf==1349024115)then
set s=s+"|cffff8700Phys.|r"
elseif(qf==1298229091)then
set s=s+"|cff8700ffMagc.|r"
elseif(qf==1348825699)then
set s=s+"|cffccccccPerc.|r"
endif
call u9(s,D9,8.5,0,i,4.5,4)
set D9=null
endfunction
function di takes nothing returns boolean
return GetSpellAbilityId()==1093677105
endfunction
function fi takes nothing returns nothing
local unit t=GetSpellAbilityUnit()
local integer a=GetSpellAbilityId()
local integer H=GetHandleId(t)
local integer T=GetUnitTypeId(t)
local integer a1=LoadInteger(i8,T,1096968497)
local integer a2=LoadInteger(i8,T,1096968498)
local effect e=LoadEffectHandle(i8,H,1281717868)
if(a==1093677121)then
call UnitAddAbility(t,1093677125)
call UnitRemoveAbility(t,a1)
elseif(a==1093677122)then
call UnitAddAbility(t,1093677126)
call UnitRemoveAbility(t,a1)
elseif(a==1093677130)then
call UnitAddAbility(t,1093677131)
call UnitRemoveAbility(t,a1)
elseif(a==1093677123)then
call UnitAddAbility(t,1093677127)
call UnitRemoveAbility(t,a2)
elseif(a==1093677133)then
call SaveInteger(i8,H,1131574321,1)
call UnitAddAbility(t,1093677136)
call UnitRemoveAbility(t,a2)
elseif(a==1093677134)then
call SaveInteger(i8,H,1131574321,1)
call UnitAddAbility(t,1093677137)
call UnitRemoveAbility(t,a2)
elseif(a==1093677390)then
call SaveInteger(i8,H,1131574321,1)
call UnitAddAbility(t,1093677389)
call UnitRemoveAbility(t,a1)
elseif(a==1093677392)then
call SaveInteger(i8,H,1131574321,1)
call UnitAddAbility(t,1093677391)
call UnitRemoveAbility(t,a1)
elseif(a==1093677394)then
call SaveInteger(i8,H,1131574321,1)
call UnitAddAbility(t,1093677393)
call UnitRemoveAbility(t,a1)
elseif(a==1093677360)then
call SaveInteger(i8,H,1131574322,1)
call UnitAddAbility(t,1093677363)
call UnitRemoveAbility(t,a2)
elseif(a==1093677364)then
call SaveInteger(i8,H,1131574322,1)
call UnitAddAbility(t,1093677365)
call UnitRemoveAbility(t,a2)
elseif(a==1093677377)then
call SaveInteger(i8,H,1131574322,1)
call UnitAddAbility(t,1093677379)
call UnitRemoveAbility(t,a2)
elseif(a==1093677618)then
call SaveInteger(i8,H,1131574321,1)
call UnitAddAbility(t,1093677619)
call UnitRemoveAbility(t,a1)
elseif(a==1093677620)then
call SaveInteger(i8,H,1131574321,1)
call UnitAddAbility(t,1093677621)
call UnitRemoveAbility(t,a1)
elseif(a==1093677622)then
call SaveInteger(i8,H,1131574321,1)
call UnitAddAbility(t,1093677623)
call UnitRemoveAbility(t,a1)
elseif(a==1093677625)then
call SaveReal(i8,H,1093677633,2)
call UnitAddAbility(t,1093677633)
call UnitRemoveAbility(t,a2)
elseif(a==1093677634)then
call SaveReal(i8,H,1093677635,64)
call UnitAddAbility(t,1093677635)
call UnitRemoveAbility(t,a2)
elseif(a==1093677636)then
call UnitAddAbility(t,1093677637)
call UnitRemoveAbility(t,a2)
endif
if not((GetUnitAbilityLevel((t),(a1))>0)or(GetUnitAbilityLevel((t),(a2))>0))then
call RemoveSavedHandle(i8,H,1281717868)
call DestroyEffect(e)
endif
set t=null
set e=null
endfunction
function gi takes nothing returns boolean
local integer a=GetSpellAbilityId()
local boolean b=(a!=1093677104)and(a!=1093677105)
return b
endfunction
function ii takes nothing returns nothing
local unit t=GetTriggerUnit()
local integer i=(GetPlayerId(GetOwningPlayer((t))))
local integer T=GetUnitTypeId(t)
local integer H=GetHandleId(t)
local integer qf=LoadInteger(i8,T,1148020564)
call SaveInteger(i8,H,1148020564,qf)
call n9(i,1,false)
call u9("|cFFFFFF00+1|r",t,8.5,64,i,2,1)
set t=null
endfunction
function ki takes nothing returns nothing
local quest q=CreateQuest()
local string s="General.
"
set s=s+" - Minor code improvements.
"
set s=s+" - Now game will end in 7 seconds when only one player stays alive.
"
set s=s+" - Fixed issue when \"Toggle Grid\" ability affects grid of other players.
"
set s=s+"Modes.
"
set s=s+" - Lifesteal mode removed.
"
set s=s+"    - When this mode is enabled, game can last for several hours without any hope to end.
"
set s=s+" - Default amount of lives increased from |cffcc808030|r to |cffcc808050|r.
"
set s=s+" - Now players send their minions to random foe or themselves by default.
"
call QuestSetRequired(q,false)
call QuestSetTitle(q,"|cff909090version 2.4.1|r")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNScroll.blp")
set q=null
endfunction
function ni takes nothing returns nothing
local quest q=CreateQuest()
local string s="General.
"
set s=s+" - Performance improvements.
"
set s=s+" - Preparation time increased to 90 seconds.
"
set s=s+"Towers.
"
set s=s+" - Level requirements set to |cff80ccff30|r\\|cff80ccff90|r.
"
set s=s+"Minions.
"
set s=s+" - Maximum number of minions set to 40.
"
set s=s+" - Movement speed of air minions set to |cff0080ff1.5|r cells per second.
"
set s=s+" - Shaman.
"
set s=s+"    - Updated ability.
"
set s=s+" - Troll Batrider.
"
set s=s+"    - Income increment reduced to |cffff800048|r.
"
set s=s+"    - Experience cost increased to |cff80ccff2|r.
"
set s=s+" - Spirit Walker.
"
set s=s+"    - Updated ability.
"
set s=s+" - Ghoul and Abomination.
"
set s=s+"    - Ability healing increased to |cff80ff0020%|r.
"
set s=s+" - Necromancer.
"
set s=s+"    - Ability cooldown increased to |cff00ff805|r seconds.
"
set s=s+" - Druid of the Talon.
"
set s=s+"    - Updated ability.
"
set s=s+" - Faerie Dragon.
"
set s=s+"    - Gold cost and income increment increased to |cffff800048|r.
"
set s=s+"    - Armor changed to |cffff87000|r\\|cff8700ff20|r.
"
set s=s+"    - Experience cost increased to |cff80ccff2|r.
"
set s=s+"Minor inprovements."
call QuestSetRequired(q,false)
call QuestSetTitle(q,"|cff909090version 2.4|r")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNScroll.blp")
set q=null
endfunction
function pi takes nothing returns nothing
local quest q=CreateQuest()
local string s="Towers.
"
set s=s+" - Damage and Kill Damage of all towers were reduced.
"
set s=s+" - Fixed issue when towers did not receive Kill Damage from kills.
"
set s=s+"Minions.
"
set s=s+" - Ghoul and Abomination.
"
set s=s+"    - Changed ability.
"
set s=s+" - Fixed issue when Necromancer did not cast his ability.
"
set s=s+"Minor inprovements."
call QuestSetRequired(q,false)
call QuestSetTitle(q,"|cff909090version 2.3|r")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNScroll.blp")
set q=null
endfunction
function ri takes nothing returns nothing
local quest q=CreateQuest()
local string s="General.
"
set s=s+" - Performance improvements.
"
set s=s+" - Fixed issue when player lost gold due to incorrect order.
"
set s=s+"Towers.
"
set s=s+" - Guard Tower.
"
set s=s+"    - Kill Damage increased to |cffff33330.09|r.
"
set s=s+" - Multishot Tower.
"
set s=s+"    - Kill Damage reduced to |cffff33330.025|r.
"
set s=s+" - Watch Tower.
"
set s=s+"    - Kill Damage reduced to |cffff33330.025|r.
"
set s=s+" - Magic Tower.
"
set s=s+"    - Kill Damage increased to |cffff33330.12|r.
"
set s=s+" - Blood Tower.
"
set s=s+"    - Kill Damage increased to |cffff33330.168|r.
"
set s=s+" - Boulder Tower.
"
set s=s+"    - Gold cost reduced to |cffffff008|r.
"
set s=s+"    - Kill Damage increased to |cffff33330.2|r.
"
set s=s+" - Artillery Tower.
"
set s=s+"    - Gold cost reduced to |cffffff006|r.
"
set s=s+"    - Kill Damage increased to |cffff33330.12|r.
"
set s=s+" - Energy Tower.
"
set s=s+"    - Gold cost reduced to |cffffff006|r.
"
set s=s+"    - Kill Damage increased to |cffff33330.08|r.
"
set s=s+" - Fixed issue when towers could be build on unbuildable area.
"
set s=s+"Minions.
"
set s=s+" - Necromancer.
"
set s=s+"    - Income increment from ability reduced to |cffff80001|r.
"
set s=s+"Minor inprovements."
call QuestSetRequired(q,false)
call QuestSetTitle(q,"|cff909090version 2.2|r")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNScroll.blp")
set q=null
endfunction
function ti takes nothing returns nothing
local quest q=CreateQuest()
local string s="General.
"
set s=s+" - Buildable space was reduced.
"
set s=s+" - Kills of minions increases income by |cffff80001|r.
"
set s=s+"Towers.
"
set s=s+" - Cannon class.
"
set s=s+"    - Heavy Impact.
"
set s=s+"       - AoE increment was changed to |cff80ff000.5|r cells.
"
set s=s+"Minions.
"
set s=s+" - Maximum number of minions is reduced to 30.
"
set s=s+" - Income increment of many minions was changed.
"
set s=s+" - Experience cost of many minions was reduced.
"
set s=s+" - Fixed issue when towers-affecting minions did not cast their abilities.
"
set s=s+"Minor inprovements."
call QuestSetRequired(q,false)
call QuestSetTitle(q,"|cff909090version 2.1|r")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNScroll.blp")
set q=null
endfunction
function vi takes nothing returns nothing
local quest q=CreateQuest()
local string s="General.
"
set s=s+" - Changed gold income system.
"
set s=s+"    - Every minion has its own number of income increment.
"
set s=s+"    - Players will not receive extra gold from kills and life absorption.
"
set s=s+" - Hotkeys of all basic and custom orders were updated.
"
set s=s+"Modes.
"
set s=s+" - Lifesteal is enabled as default.
"
set s=s+" - Default number of lives is |cffcc808030|r.
"
set s=s+"Towers.
"
set s=s+" - Level 3 of all towers was reassembled as individual towers.
"
set s=s+"    - New 9 towers.
"
set s=s+"    - Old towers were removed.
"
set s=s+" - Introducing tower classes.
"
set s=s+"    - 3 tower classes.
"
set s=s+"    - Each class has 2 levels and 3 passive abilities on both levels.
"
set s=s+"    - Classes' abilities based on abilities of old Arrow, Arcane and Cannon towers.
"
set s=s+"    - Some abilities were slightly changed.
"
set s=s+" - Fixed issue when level 2 abilities of Arcane tower did not bounce.
"
set s=s+" - Fixed issue when reduction of movement speed became permanent.
"
set s=s+"Minions.
"
set s=s+" - Movement speed of all minions was increased by |cff0080ff0.5|r cells per second.
"
set s=s+" - Troll Berserker and Raider.
"
set s=s+"    - Updated abilities due to overall movement speed increment.
"
set s=s+" - Troll Batrider.
"
set s=s+"    - Ability does not increases movement speed anymore.
"
set s=s+" - Necromancer.
"
set s=s+"    - Ability also increases income of player by |cffff80002|r.
"
set s=s+"Minor inprovements."
call QuestSetRequired(q,false)
call QuestSetTitle(q,"|cff909090version 2.0|r")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNScroll.blp")
set q=null
endfunction
function xi takes nothing returns nothing
local quest q=CreateQuest()
local string s="Unreleased version.

General.
"
set s=s+" - Now if players send minions to themselves, players will get only cost of minion if it absorbed life.
"
set s=s+" - Updated wave start.
"
set s=s+"    - Now after wave ends, new wave will start in 60 seconds.
"
set s=s+"    - Players can start wave earlier, if they all press button 'Readiness' in his builder.
"
set s=s+" - Fixed issue when wave could not start.
"
set s=s+"Minor updates and improvements."
call QuestSetRequired(q,false)
call QuestSetTitle(q,"|cff909090version 1.1.1|r")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNScroll.blp")
set q=null
endfunction
function zi takes nothing returns nothing
local quest q=CreateQuest()
local string s="General.
"
set s=s+" - Gold from kills increased to |cffffff002|r.
"
set s=s+" - Now players will not get gold from life absorbtion if they send minions to themselves.
"
set s=s+" - Updated wave start.
"
set s=s+"    - Player can prepare for wave until he press button 'Readiness' in his builder.
"
set s=s+"    - When all players are ready, wave starts after some seconds.
"
set s=s+"    - Readiness status added to board.
"
set s=s+"Modes.
"
set s=s+" - New mode Lifesteal.
"
set s=s+"    - When enabled, minions will give absorbed lives to their owner.
"
set s=s+"Towers.
"
set s=s+" - Experience requirements changed to |cff80ccff27|r\\|cff80ccff81|r\\|cff80ccff243|r.
"
set s=s+" - Minimum distance between towers reduced to |cff0080ff0.5|r cells.
"
set s=s+" - Watch Tower.
"
set s=s+"    - Gold cost increased to |cffffff001|r.
"
set s=s+" - Arcane Tower.
"
set s=s+"    - Abilities of level 1 have updated names, icons and effects.
"
set s=s+"    - Abilities of level 2 were fully changed.
"
set s=s+"    - Updated Blood Magic.
"
set s=s+" - Cannon Tower.
"
set s=s+"    - Changed Artillery and Refined Powder.
"
set s=s+" - New towers.
"
set s=s+"    - Arrow Tower I, Arrow Tower III, Arcane Tower I, Arcane Tower III and Cannon Tower I.
"
set s=s+"Minions.
"
set s=s+" - Experience cost of all minions increased by |cff80ccff1|r.
"
set s=s+" - Troll Berserker, Troll Batrider and Faerie Dragon.
"
set s=s+"    - Changed abilities.
"
set s=s+" - Tauren and Mountain Giant.
"
set s=s+"    - Limits increased to |cff0080ff5|r.
"
set s=s+"Minor updates and improvements."
call QuestSetRequired(q,false)
call QuestSetTitle(q,"|cff909090version 1.1|r")
call QuestSetDescription(q,s)
call QuestSetIconPath(q,"ReplaceableTextures\\CommandButtons\\BTNScroll.blp")
set q=null
endfunction
function RunInitializationTriggers takes nothing returns nothing
call ConditionalTriggerExecute(l4)
endfunction
function InitCustomPlayerSlots takes nothing returns nothing
call SetPlayerStartLocation(Player(0),0)
call ForcePlayerStartLocation(Player(0),0)
call SetPlayerColor(Player(0),ConvertPlayerColor(0))
call SetPlayerRacePreference(Player(0),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(0),false)
call SetPlayerController(Player(0),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(1),1)
call ForcePlayerStartLocation(Player(1),1)
call SetPlayerColor(Player(1),ConvertPlayerColor(1))
call SetPlayerRacePreference(Player(1),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(1),false)
call SetPlayerController(Player(1),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(2),2)
call ForcePlayerStartLocation(Player(2),2)
call SetPlayerColor(Player(2),ConvertPlayerColor(2))
call SetPlayerRacePreference(Player(2),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(2),false)
call SetPlayerController(Player(2),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(3),3)
call ForcePlayerStartLocation(Player(3),3)
call SetPlayerColor(Player(3),ConvertPlayerColor(3))
call SetPlayerRacePreference(Player(3),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(3),false)
call SetPlayerController(Player(3),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(4),4)
call ForcePlayerStartLocation(Player(4),4)
call SetPlayerColor(Player(4),ConvertPlayerColor(4))
call SetPlayerRacePreference(Player(4),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(4),false)
call SetPlayerController(Player(4),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(5),5)
call ForcePlayerStartLocation(Player(5),5)
call SetPlayerColor(Player(5),ConvertPlayerColor(5))
call SetPlayerRacePreference(Player(5),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(5),false)
call SetPlayerController(Player(5),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(6),6)
call ForcePlayerStartLocation(Player(6),6)
call SetPlayerColor(Player(6),ConvertPlayerColor(6))
call SetPlayerRacePreference(Player(6),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(6),false)
call SetPlayerController(Player(6),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(7),7)
call ForcePlayerStartLocation(Player(7),7)
call SetPlayerColor(Player(7),ConvertPlayerColor(7))
call SetPlayerRacePreference(Player(7),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(7),false)
call SetPlayerController(Player(7),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(8),8)
call ForcePlayerStartLocation(Player(8),8)
call SetPlayerColor(Player(8),ConvertPlayerColor(8))
call SetPlayerRacePreference(Player(8),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(8),false)
call SetPlayerController(Player(8),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player(9),9)
call ForcePlayerStartLocation(Player(9),9)
call SetPlayerColor(Player(9),ConvertPlayerColor(9))
call SetPlayerRacePreference(Player(9),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player(9),false)
call SetPlayerController(Player(9),MAP_CONTROL_USER)
call SetPlayerStartLocation(Player($A),$A)
call ForcePlayerStartLocation(Player($A),$A)
call SetPlayerColor(Player($A),ConvertPlayerColor($A))
call SetPlayerRacePreference(Player($A),RACE_PREF_HUMAN)
call SetPlayerRaceSelectable(Player($A),false)
call SetPlayerController(Player($A),MAP_CONTROL_USER)
endfunction
function InitCustomTeams takes nothing returns nothing
call SetPlayerTeam(Player(0),0)
call SetPlayerTeam(Player(1),0)
call SetPlayerTeam(Player(2),0)
call SetPlayerTeam(Player(3),0)
call SetPlayerTeam(Player(4),1)
call SetPlayerTeam(Player(5),1)
call SetPlayerTeam(Player(6),1)
call SetPlayerTeam(Player(7),1)
call SetPlayerTeam(Player(8),2)
call SetPlayerTeam(Player(9),2)
call SetPlayerTeam(Player($A),2)
endfunction
function InitAllyPriorities takes nothing returns nothing
call SetStartLocPrioCount(0,1)
call SetStartLocPrio(0,0,1,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(1,2)
call SetStartLocPrio(1,0,0,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(1,1,2,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(2,2)
call SetStartLocPrio(2,0,1,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(2,1,3,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(3,2)
call SetStartLocPrio(3,0,2,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(3,1,4,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(4,2)
call SetStartLocPrio(4,0,3,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(4,1,5,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(5,2)
call SetStartLocPrio(5,0,4,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(5,1,6,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(6,2)
call SetStartLocPrio(6,0,5,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(6,1,7,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(7,2)
call SetStartLocPrio(7,0,6,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(7,1,8,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(8,2)
call SetStartLocPrio(8,0,7,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(8,1,9,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount(9,2)
call SetStartLocPrio(9,0,8,MAP_LOC_PRIO_HIGH)
call SetStartLocPrio(9,1,$A,MAP_LOC_PRIO_HIGH)
call SetStartLocPrioCount($A,1)
call SetStartLocPrio($A,0,9,MAP_LOC_PRIO_HIGH)
endfunction
function main takes nothing returns nothing
local weathereffect we
local integer f9
local integer g9
local version v
local integer T8
call SetCameraBounds(-7936.+GetCameraMargin(CAMERA_MARGIN_LEFT),-896.+GetCameraMargin(CAMERA_MARGIN_BOTTOM),8448.-GetCameraMargin(CAMERA_MARGIN_RIGHT),1920.-GetCameraMargin(CAMERA_MARGIN_TOP),-7936.+GetCameraMargin(CAMERA_MARGIN_LEFT),1920.-GetCameraMargin(CAMERA_MARGIN_TOP),8448.-GetCameraMargin(CAMERA_MARGIN_RIGHT),-896.+GetCameraMargin(CAMERA_MARGIN_BOTTOM))
call SetDayNightModels("Environment\\DNC\\DNCLordaeron\\DNCLordaeronTerrain\\DNCLordaeronTerrain.mdl","Environment\\DNC\\DNCLordaeron\\DNCLordaeronUnit\\DNCLordaeronUnit.mdl")
call NewSoundEnvironment("Default")
call SetAmbientDaySound("LordaeronSummerDay")
call SetAmbientNightSound("LordaeronSummerNight")
call SetMapMusic("Music",true,0)
set D4=CreateSound("Sound\\Interface\\Error.wav",false,false,false,$A,$A,"")
call SetSoundParamsFromLabel(D4,"InterfaceError")
call SetSoundDuration(D4,614)
set E4=CreateSound("Sound\\Interface\\Warning\\Human\\KnightNoGold1.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(E4,"NoGoldHuman")
call SetSoundDuration(E4,$5CE)
set F4=CreateSound("Abilities\\Spells\\Items\\ResourceItems\\ReceiveGold.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(F4,"ReceiveGold")
call SetSoundDuration(F4,589)
call SetSoundChannel(F4,8)
call SetSoundVolume(F4,105)
set G4=CreateSound("Sound\\Interface\\BattleNetTick.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(G4,"ChatroomTimerTick")
call SetSoundDuration(G4,476)
call SetSoundVolume(G4,105)
set H4=CreateSound("Sound\\Interface\\Warning.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(H4,"Warning")
call SetSoundDuration(H4,$770)
call SetSoundVolume(H4,105)
set I4=CreateSound("Sound\\Interface\\ItemReceived.wav",false,false,false,$A,$A,"DefaultEAXON")
call SetSoundParamsFromLabel(I4,"ItemReward")
call SetSoundDuration(I4,$5CB)
set h=Rect(-7872.,-448.,-6976.,320.)
set j=Rect(-7936.,640.,-6912.,1152.)
set o=Rect(-7936.,1408.,-6912.,1920.)
set w=Rect(-7936.,-896.,-6912.,-768.)
set z=Rect(-7936.,-896.,-6912.,1152.)
set B=Rect(-6400.,1408.,-5376.,1920.)
set C=Rect(-4864.,1408.,-3840.,1920.)
set D=Rect(-3328.,1408.,-2304.,1920.)
set E=Rect(-1792.,1408.,-768.,1920.)
set F=Rect(-256.,1408.,768.,1920.)
set G=Rect(1280.,1408.,2304.,1920.)
set I=Rect(2816.,1408.,3840.,1920.)
set J=Rect(4352.,1408.,5376.,1920.)
set K=Rect(5888.,1408.,6912.,1920.)
set M=Rect(7424.,1408.,8448.,1920.)
set N=Rect(-6400.,640.,-5376.,1152.)
set O=Rect(-4864.,640.,-3840.,1152.)
set P=Rect(-3328.,640.,-2304.,1152.)
set Q=Rect(-1792.,640.,-768.,1152.)
set R=Rect(-256.,640.,768.,1152.)
set S=Rect(1280.,640.,2304.,1152.)
set U=Rect(2816.,640.,3840.,1152.)
set V=Rect(4352.,640.,5376.,1152.)
set W=Rect(5888.,640.,6912.,1152.)
set X=Rect(7424.,640.,8448.,1152.)
set Y=Rect(-6336.,-448.,-5440.,320.)
set Z=Rect(-4800.,-448.,-3904.,320.)
set d4=Rect(-3264.,-448.,-2368.,320.)
set e4=Rect(-1728.,-448.,-832.,320.)
set f4=Rect(-192.,-448.,704.,320.)
set g4=Rect(1344.,-448.,2240.,320.)
set h4=Rect(2880.,-448.,3776.,320.)
set i4=Rect(4416.,-448.,5312.,320.)
set j4=Rect(5952.,-448.,6848.,320.)
set k4=Rect(7488.,-448.,8384.,320.)
set m4=Rect(-6400.,-896.,-5376.,-768.)
set n4=Rect(-4864.,-896.,-3840.,-768.)
set o4=Rect(-3328.,-896.,-2304.,-768.)
set p4=Rect(-1792.,-896.,-768.,-768.)
set q4=Rect(-256.,-896.,768.,-768.)
set r4=Rect(1280.,-896.,2304.,-768.)
set s4=Rect(2816.,-896.,3840.,-768.)
set t4=Rect(4352.,-896.,5376.,-768.)
set u4=Rect(5888.,-896.,6912.,-768.)
set v4=Rect(7424.,-896.,8448.,-768.)
set w4=Rect(-6400.,-896.,-5376.,1152.)
set x4=Rect(-4864.,-896.,-3840.,1152.)
set y4=Rect(-3328.,-896.,-2304.,1152.)
set z4=Rect(-1792.,-896.,-768.,1152.)
set A4=Rect(-256.,-896.,768.,1152.)
set a4=Rect(1280.,-896.,2304.,1152.)
set B4=Rect(2816.,-896.,3840.,1152.)
set b4=Rect(4352.,-896.,5376.,1152.)
set C4=Rect(5888.,-896.,6912.,1152.)
set c4=Rect(7424.,-896.,8448.,1152.)
call ConfigureNeutralVictim()
set O8=Filter(function d9)
set filterIssueHauntOrderAtLocBJ=Filter(function IssueHauntOrderAtLocBJFilter)
set filterEnumDestructablesInCircleBJ=Filter(function Q8)
set filterGetUnitsInRectOfPlayer=Filter(function GetUnitsInRectOfPlayerFilter)
set filterGetUnitsOfTypeIdAll=Filter(function GetUnitsOfTypeIdAllFilter)
set filterGetUnitsOfPlayerAndTypeId=Filter(function GetUnitsOfPlayerAndTypeIdFilter)
set filterMeleeTrainedUnitIsHeroBJ=Filter(function MeleeTrainedUnitIsHeroBJFilter)
set filterLivingPlayerUnitsOfTypeId=Filter(function LivingPlayerUnitsOfTypeIdFilter)
set f9=0
loop
exitwhen f9==16
set bj_FORCE_PLAYER[f9]=CreateForce()
call ForceAddPlayer(bj_FORCE_PLAYER[f9],Player(f9))
set f9=f9+1
endloop
set bj_FORCE_ALL_PLAYERS=CreateForce()
call ForceEnumPlayers(bj_FORCE_ALL_PLAYERS,null)
set bj_cineModePriorSpeed=GetGameSpeed()
set bj_cineModePriorFogSetting=IsFogEnabled()
set bj_cineModePriorMaskSetting=IsFogMaskEnabled()
set f9=0
loop
exitwhen f9>=bj_MAX_QUEUED_TRIGGERS
set bj_queuedExecTriggers[f9]=null
set bj_queuedExecUseConds[f9]=false
set f9=f9+1
endloop
set bj_isSinglePlayer=false
set g9=0
set f9=0
loop
exitwhen f9>=$C
if(GetPlayerController(Player(f9))==MAP_CONTROL_USER and GetPlayerSlotState(Player(f9))==PLAYER_SLOT_STATE_PLAYING)then
set g9=g9+1
endif
set f9=f9+1
endloop
set bj_isSinglePlayer=(g9==1)
set bj_rescueSound=CreateSoundFromLabel("Rescue",false,false,false,$2710,$2710)
set bj_questDiscoveredSound=CreateSoundFromLabel("QuestNew",false,false,false,$2710,$2710)
set bj_questUpdatedSound=CreateSoundFromLabel("QuestUpdate",false,false,false,$2710,$2710)
set bj_questCompletedSound=CreateSoundFromLabel("QuestCompleted",false,false,false,$2710,$2710)
set bj_questFailedSound=CreateSoundFromLabel("QuestFailed",false,false,false,$2710,$2710)
set bj_questHintSound=CreateSoundFromLabel("Hint",false,false,false,$2710,$2710)
set bj_questSecretSound=CreateSoundFromLabel("SecretFound",false,false,false,$2710,$2710)
set bj_questItemAcquiredSound=CreateSoundFromLabel("ItemReward",false,false,false,$2710,$2710)
set bj_questWarningSound=CreateSoundFromLabel("Warning",false,false,false,$2710,$2710)
set bj_victoryDialogSound=CreateSoundFromLabel("QuestCompleted",false,false,false,$2710,$2710)
set bj_defeatDialogSound=CreateSoundFromLabel("QuestFailed",false,false,false,$2710,$2710)
call DelayedSuspendDecayCreate()
set v=VersionGet()
if(v==VERSION_REIGN_OF_CHAOS)then
set bj_MELEE_MAX_TWINKED_HEROES=bj_MELEE_MAX_TWINKED_HEROES_V0
else
set bj_MELEE_MAX_TWINKED_HEROES=bj_MELEE_MAX_TWINKED_HEROES_V1
endif
call InitQueuedTriggers()
call InitRescuableBehaviorBJ()
call InitDNCSounds()
call InitMapRects()
call InitSummonableCaps()
set T8=0
loop
set bj_stockAllowedPermanent[T8]=false
set bj_stockAllowedCharged[T8]=false
set bj_stockAllowedArtifact[T8]=false
set T8=T8+1
exitwhen T8>$A
endloop
call SetAllItemTypeSlots($B)
call SetAllUnitTypeSlots($B)
set bj_stockUpdateTimer=CreateTimer()
call TimerStart(bj_stockUpdateTimer,bj_STOCK_RESTOCK_INITIAL_DELAY,false,function Y8)
set bj_stockItemPurchased=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(bj_stockItemPurchased,Player($F),EVENT_PLAYER_UNIT_SELL_ITEM,null)
call TriggerAddAction(bj_stockItemPurchased,function RemovePurchasedItem)
call DetectGameStarted()
call ExecuteFunc("ai")
set l4=CreateTrigger()
call TriggerAddAction(l4,function be)
set J4=CreateTrigger()
call TriggerAddAction(J4,function Ee)
set K4=CreateTrigger()
call TriggerAddAction(K4,function He)
set L4=CreateTrigger()
call TriggerAddAction(L4,function le)
set M4=CreateTrigger()
call TriggerAddAction(M4,function Ke)
set N4=CreateTrigger()
call TriggerAddAction(N4,function Me)
set O4=CreateTrigger()
call TriggerAddAction(O4,function Oe)
set P4=CreateTrigger()
call TriggerAddAction(P4,function Qe)
set Q4=CreateTrigger()
call TriggerAddAction(Q4,function Se)
set R4=CreateTrigger()
call TriggerRegisterTimerEvent(R4,0,false)
call TriggerAddAction(R4,function We)
set S4=CreateTrigger()
call TriggerAddAction(S4,function Ye)
set T4=CreateTrigger()
call TriggerRegisterTimerExpireEvent(T4,c8)
call TriggerAddAction(T4,function ef)
set U4=CreateTrigger()
call TriggerAddAction(U4,function jf)
set V4=CreateTrigger()
call TriggerAddCondition(V4,Condition(function yf))
call TriggerAddAction(V4,function vf)
set W4=CreateTrigger()
call TriggerAddAction(W4,function Af)
set X4=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(X4,Player($B),EVENT_PLAYER_UNIT_DEATH,null)
call TriggerAddAction(X4,function cf)
set Y4=CreateTrigger()
call TriggerAddAction(Y4,function Hf)
set Z4=CreateTrigger()
call TriggerAddAction(Z4,function Kf)
set d7=CreateTrigger()
call TriggerAddAction(d7,function Rf)
set e7=CreateTrigger()
call TriggerAddAction(e7,function Uf)
set f7=CreateTrigger()
call TriggerAddCondition(f7,Condition(function Xf))
call TriggerAddAction(f7,function Wf)
set g7=CreateTrigger()
call TriggerAddCondition(g7,Condition(function dg))
call TriggerAddAction(g7,function Zf)
set h7=CreateTrigger()
call TriggerAddCondition(h7,Condition(function ig))
call TriggerAddAction(h7,function fg)
set i7=CreateTrigger()
call ze(i7,EVENT_PLAYER_UNIT_SPELL_EFFECT)
call TriggerAddCondition(i7,Condition(function ng))
call TriggerAddAction(i7,function mg)
set j7=CreateTrigger()
call TriggerAddCondition(j7,Condition(function qg))
call TriggerAddAction(j7,function pg)
set k7=CreateTrigger()
call TriggerAddAction(k7,function sg)
call TriggerAddCondition(k7,Condition(function tg))
set m7=CreateTrigger()
call TriggerAddCondition(m7,Condition(function yg))
call TriggerAddAction(m7,function xg)
set n7=CreateTrigger()
call TriggerAddCondition(n7,Condition(function ag))
call TriggerAddAction(n7,function Ag)
set o7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(o7,Player($B),EVENT_PLAYER_UNIT_ATTACKED,null)
call TriggerAddCondition(o7,Condition(function Fg))
call TriggerAddAction(o7,function bg)
set p7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(p7,Player($B),EVENT_PLAYER_UNIT_SPELL_ENDCAST,null)
call TriggerRegisterPlayerUnitEvent(p7,Player($B),EVENT_PLAYER_UNIT_SPELL_FINISH,null)
call TriggerAddAction(p7,function Hg)
set q7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(q7,Player($B),EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerAddCondition(q7,Condition(function Kg))
call TriggerAddAction(q7,function Jg)
set r7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(r7,Player($B),EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerAddCondition(r7,Condition(function Og))
call TriggerAddAction(r7,function Ng)
set s7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(s7,Player($B),EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerAddCondition(s7,Condition(function Sg))
call TriggerAddAction(s7,function Rg)
set t7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(t7,Player($B),EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerAddCondition(t7,Condition(function Wg))
call TriggerAddAction(t7,function Vg)
set u7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(u7,Player($B),EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerAddCondition(u7,Condition(function dh))
call TriggerAddAction(u7,function Zg)
set v7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(v7,Player($B),EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerAddCondition(v7,Condition(function hh))
call TriggerAddAction(v7,function gh)
set w7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(w7,Player($B),EVENT_PLAYER_UNIT_DEATH,null)
call TriggerAddCondition(w7,Condition(function kh))
call TriggerAddAction(w7,function jh)
set x7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(x7,Player($B),EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerAddCondition(x7,Condition(function ph))
call TriggerAddAction(x7,function oh)
set y7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(y7,Player($B),EVENT_PLAYER_UNIT_DEATH,null)
call TriggerAddCondition(y7,Condition(function vh))
call TriggerAddAction(y7,function th)
set z7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(z7,Player($B),EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerAddCondition(z7,Condition(function zh))
call TriggerAddAction(z7,function yh)
set A7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(A7,Player($B),EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerAddCondition(A7,Condition(function Ch))
call TriggerAddAction(A7,function bh)
set a7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(a7,Player($B),EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerAddCondition(a7,Condition(function Fh))
call TriggerAddAction(a7,function Eh)
set B7=CreateTrigger()
call TriggerRegisterPlayerUnitEvent(B7,Player($B),EVENT_PLAYER_UNIT_SPELL_EFFECT,null)
call TriggerAddCondition(B7,Condition(function Ih))
call TriggerAddAction(B7,function Hh)
set b7=CreateTrigger()
call ze(b7,EVENT_PLAYER_UNIT_CONSTRUCT_START)
call TriggerAddAction(b7,function Sh)
set C7=CreateTrigger()
call ze(C7,EVENT_PLAYER_UNIT_CONSTRUCT_CANCEL)
call TriggerAddAction(C7,function Uh)
set c7=CreateTrigger()
call ze(c7,EVENT_PLAYER_UNIT_SPELL_EFFECT)
call TriggerAddCondition(c7,Condition(function Xh))
call TriggerAddAction(c7,function Wh)
set D7=CreateTrigger()
call ze(D7,EVENT_PLAYER_UNIT_SPELL_EFFECT)
call TriggerAddCondition(D7,Condition(function di))
call TriggerAddAction(D7,function Zh)
set E7=CreateTrigger()
call ze(E7,EVENT_PLAYER_UNIT_SPELL_EFFECT)
call TriggerAddCondition(E7,Condition(function gi))
call TriggerAddAction(E7,function fi)
set F7=CreateTrigger()
call ze(F7,EVENT_PLAYER_UNIT_UPGRADE_FINISH)
call TriggerAddAction(F7,function ii)
set G7=CreateTrigger()
call TriggerAddAction(G7,function ki)
set H7=CreateTrigger()
call TriggerAddAction(H7,function ni)
set I7=CreateTrigger()
call TriggerAddAction(I7,function pi)
set l7=CreateTrigger()
call TriggerAddAction(l7,function ri)
set J7=CreateTrigger()
call TriggerAddAction(J7,function ti)
set K7=CreateTrigger()
call TriggerAddAction(K7,function vi)
set L7=CreateTrigger()
call TriggerAddAction(L7,function xi)
set M7=CreateTrigger()
call TriggerAddAction(M7,function zi)
call ConditionalTriggerExecute(l4)
endfunction
function config takes nothing returns nothing
local player p
call SetMapName("TRIGSTR_001")
call SetMapDescription("TRIGSTR_1020")
call SetPlayers($B)
call SetTeams(3)
set p=Player(0)
call DefineStartLocation(0,.0,1408.)
call SetPlayerStartLocation(p,0)
call SetPlayerColor(p,ConvertPlayerColor(0))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,0)
set p=Player(1)
call DefineStartLocation(1,.0,1408.)
call SetPlayerStartLocation(p,1)
call SetPlayerColor(p,ConvertPlayerColor(1))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,0)
set p=Player(2)
call DefineStartLocation(2,.0,1408.)
call SetPlayerStartLocation(p,2)
call SetPlayerColor(p,ConvertPlayerColor(2))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,0)
set p=Player(3)
call DefineStartLocation(3,.0,1408.)
call SetPlayerStartLocation(p,3)
call SetPlayerColor(p,ConvertPlayerColor(3))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,0)
set p=Player(4)
call DefineStartLocation(4,.0,1408.)
call SetPlayerStartLocation(p,4)
call SetPlayerColor(p,ConvertPlayerColor(4))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(5)
call DefineStartLocation(5,.0,1408.)
call SetPlayerStartLocation(p,5)
call SetPlayerColor(p,ConvertPlayerColor(5))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(6)
call DefineStartLocation(6,.0,1408.)
call SetPlayerStartLocation(p,6)
call SetPlayerColor(p,ConvertPlayerColor(6))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(7)
call DefineStartLocation(7,.0,1408.)
call SetPlayerStartLocation(p,7)
call SetPlayerColor(p,ConvertPlayerColor(7))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,1)
set p=Player(8)
call DefineStartLocation(8,.0,1408.)
call SetPlayerStartLocation(p,8)
call SetPlayerColor(p,ConvertPlayerColor(8))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,2)
set p=Player(9)
call DefineStartLocation(9,.0,1408.)
call SetPlayerStartLocation(p,9)
call SetPlayerColor(p,ConvertPlayerColor(9))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,2)
set p=Player($A)
call DefineStartLocation($A,.0,1408.)
call SetPlayerStartLocation(p,$A)
call SetPlayerColor(p,ConvertPlayerColor($A))
call SetPlayerRacePreference(p,RACE_PREF_RANDOM)
call SetPlayerRaceSelectable(p,true)
call SetPlayerController(p,MAP_CONTROL_USER)
call SetPlayerTeam(p,2)
set p=null
endfunction
function ai takes nothing returns nothing
endfunction
